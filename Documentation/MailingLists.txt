----------------------------
	Electronic Mailing Lists
----------------------------

	--------------------
	Introduction

	All mailing lists are hosted via the Mailbase service.  Mailbase's
	home page is

	--> http://www.mailbase.ac.uk/

	To join any of the lists, send the following command to
	"mailbase@mailbase.ac.uk" :

	> join <list-name> <your first name> <your last name>
	> stop

	--------------------
	NQS-Announce

	This is a moderated list, which carries announcements of new
	releases of Generic NQS.  I'm also happy to allow any announcements
	of other NQS products, or of any products related to NQS.

	Announcements of new releases of Generic NQS include full lists of
	what's changed since the last full release of Generic NQS.  This
	is normally the ONLY place where we announce new releases.

	I recommend that if you use Generic NQS, you should subscribe to
	this list.

	--------------------
	NQS-Support

	This is an unmoderated list.  If you have a problem with GNQS, and
	you need help, then this is the place to ask.  You're better off
	sending your email here rather than just to Stu!

	We recommend that all Generic NQS users should probably subscribe
	to this list, especially if you're in need of help.

	--------------------
	NQS-Developers

	This is an unmoderated list, which carries most of the discussion of
	the on-going development of Generic NQS.  It also carries
	announcements of pre-releases of Generic NQS - these are test
	versions that are released so that the NQS community can help
	ensure that they work, before they are announced properly.

	I recommend subscribing to this list if you want to keep an eye
	on where Generic NQS is going, and especially if you want to work
	on the Generic NQS source code yourself.
	
	--------------------
	Archives

	All of the electronic mailing lists are archived at

	--> http://www.mailbase.ac.uk
