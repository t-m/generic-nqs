----------------------------
	CVS Archives
----------------------------

	The Generic NQS project does not use CVS to store the source code,
	mainly because of bad experiences I've had with CVS the last time
	I tried to use it.

	However, we do have archives available online using the Perforce
	source control system.  Full details can be found in the
	SourceControl.txt file.

	Stu Herbert
	21st November 1999
