----------------------------
	Alternatives To NQS
----------------------------

	Generic NQS isn't for everyone, so I try to keep a list of
	alternative batch processing systems on the web site, so that
	you can make the right choice for you.

	--> http://www.gnqs.org/docs/

	Stu Herbert
	21st November 1999
