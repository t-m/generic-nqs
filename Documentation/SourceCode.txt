----------------------------
	Generic NQS Master Source Code
----------------------------

	As of November 1999, the master copy of the Generic NQS source code
	tree is now available online, via a source control system.

	--------------------
	Source Control System

	We use the Perforce source control system, in preference to the
	usual suspects such as CVS.  Perforce isn't open-sourced, but
	they were kind enough to give us a free license for the server,
	and to make the clients available for free download.

	--> http://www.perforce.com/

	By default, all users have read access to the Generic NQS source
	trees.

	--------------------
	Configuring Your Perforce Client

	o  Set the environment variable P4PORT to 'www.gnqs.org:1666'

	   export P4PORT=www.gnqs.org:1666

	o  Set the environment variable P4CLIENT to your hostname (e.g.
	   www.gnqs.org)

	o  Run the command 'p4 client'.  This will bring up a form in a
	   text editor for you to edit.

	   You'll need to edit the 'Root:' line to point to a directory
	   where your local copy of the GNQS source code will appear.
	   We'll call this your 'top level directory'.

	   Set the 'View:' lines to be

	   //GNQS_Stable/...  //<your hostname>/GNQS_Stable/...
	   //GNQS_Devel/...   //<your hostname>/GNQS_Devel/...

	   Save the file.

	o  Create your 'top level directory', and inside there, create
	   the directories 'GNQS_Stable' and 'GNQS_Devel'.

	   Go into the 'GNQS_Stable' and 'GNQS_Devel' directories in turn,
	   and run the command 'p4 sync'.  If nothing happens, try running
	   'p4 sync -f'.

	--------------------
	Obtaining Write Access

	If you want to be able to actually add code directly into the
	source trees, just email me with the following details:

	o  Your name
	o  Your email address
	o  Your company/organisation
	o  A contact telephone number

	and I'll email you back with a username and password.

	NOTE: Usernames and passwords will be valid within Perforce only;
	they will NOT let you log into www.gnqs.org itself!

	--------------------
	Rules For Developers

	Once you have write access, the rules are simple:

	o  Create a 'job' describing what you're doing before you do
	   anything else.

	   This makes sure that all the other developers know what is
	   happening, so that we don't all waste our time duplicating
	   effort.

	   This also allows us to track all bugs and new features.

	o  If you check a bug fix into the GNQS_Stable project, make sure
	   you add it into the GNQS_Devel project too if appropriate.

	   As the two projects diverge, bug fixes from GNQS_Stable will no
	   longer work against GNQS_Devel.

	o  In your changelog, please make a note of what platform you have
	   tested your new code on.

	o  Please make sure your changelog includes a meaningful
	   description.  The changelog will be shipped with all releases,
	   so that end-users can see what we've all been doing.

	o  Please try not to break code on other platforms.  If you're not
	   sure about the portability, drop me (stuart@gnqs.org) a line
	   about what you're doing, and I'll do my best to help out.

	o  If your submissions cause everyone else lots of unnecessary
	   work, I'll try and talk to you about it.  If I think you're
	   repeatedly causing a problem, I'll revoke your access.

	o  I reserve the right to remove anyone's access at any time,
	   without cause or explaination.
