----------------------------
	Generic NQS Credits
----------------------------

	This is a list of people who have contributed to Generic NQS since
	21st November 1999.

	If you want to be listed in this file, please send me a standard
	UNIX patch against this file (or patch the file in the Perforce
	source control server ;-).  All entries should be like this:

	o  Name:  <your name>
	o  Email: <your email address>
	o  Roll:  <what you do, if a regular contributor>
	o  Work:  <list of things you want to take credit for>
	o  Date:  <date the entry was added>

----------------------------
	  Generic NQS Contributors
----------------------------

	o  Name:  Stuart Herbert
	o  Email: stuart@gnqs.org, s.herbert@sheffield.ac.uk
	o  Role:  Monsanto NQS / Generic NQS Maintainer, Dec 1994 - Present
	o  Work:  Installation software, portability, fixes
	o  Date:  21st November 1999

