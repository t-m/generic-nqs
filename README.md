This repository contains the legacy code of The Generic NQS (Network Queue System).

More info:
- Introduction: http://gnqs.sourceforge.net/docs/starter_pack/introducing/index.html
- Sourceforge project (2 archived versions, no VCS(?)): https://sourceforge.net/projects/gnqs/
- Copyright: http://gnqs.sourceforge.net/docs/copyright/default.htm
