/*
 * qmgr/mgr_memdump.c
 * 
 * DESCRIPTION
 *
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <unistd.h>
#include <errno.h>
#include <libnqs/nqspacket.h>
#include <libnqs/nqsxvars.h>			/* NQS external vars */
#include <libnqs/transactcc.h>                 /* For TCML_COMPLETE */
#include <string.h>
#include <ctype.h>

int memdump (char *flags)
{
    int	dump_flag;
    char *cp;

    for (cp = flags; *cp != '\0'; cp++) {
	*cp = tolower(*cp);
    }
    
    if ( !(strncmp (flags, "load", 4)) ) {
	dump_flag = MEMDUMP_LOAD;
    } else if ( !strncmp (flags, "req", 3)) {
	dump_flag = MEMDUMP_REQ;
    } else dump_flag = 0;

    interclear();
    interw32i( (long) dump_flag );
    return( inter (PKT_MEMDUMP) );
}

