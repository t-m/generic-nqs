/*
 * qmgr/mgr_cmd.c
 * 
 * DESCRIPTION:
 *
 *	NQS manager command execution module.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 15, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <signal.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <libnqs/nqsxvars.h>	       	/* NQS external vars */
#include <libnqs/nqsmgr.h>	       	/* Token types and error messages */
#include <libnqs/transactcc.h>		/* Transaction completion codes */
#include <malloc.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <unistd.h>
#include <stdlib.h>

static int cpupreamble (char quename [MAX_QUEUENAME + 1], struct cpulimit *cpulimit, short *infinite);
static void hdl_reqset ( int oper );
static void kill_reqset ( int oper );
static int quotapreamble (char quename [MAX_QUEUENAME + 1], struct quotalimit *cpulimit, short *infinite);

extern int Mgr_priv;			/* Privilege bits for current */
					/* user of Qmgr as imported from */
					/* mgr_main.c */
extern int Qmgr_echo;
extern char *Nqsmgr_prefix;

/*** v_aboque
 *
 *
 *	void v_aboque():
 *	ABort Queue command.
 */
void v_aboque()
{
	char quename [MAX_QUEUENAME+1];	/* Name of queue to be aborted */
	char *cp;			/* Pointer to queue name */
	int wait_time;			/* Abort SIGKILL wait time */
	long long_int;

	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);				/* Save queue name */
	if (scan_opwait (&long_int, (long) ABORT_WAIT, 0L, 600L,
			 EM_INTWAITIMEXP, EM_WAITIMOUTOFBOU) == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	wait_time = long_int;
	diagnose (aboque (quename, wait_time));
}


/*** v_adddes
 *
 *
 *	void v_adddes():
 *	ADd DEStination command.
 */
void v_adddes()
{

	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	/*
	 *  Scan the set of specified destinations.
	 */
	if (scan_destset() == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsdes_set (quename, ADD_OP);	/* Add the destinations */
	fsizedb (Qmapfile);		/* Mappings file may have expanded */
	fsizedb (Pipeqfile);		/* Pipe queue destination file */
					/* may have expanded */
}


/*** v_adddev
 *
 *
 *	void v_adddev():
 *	ADd DEVice command.
 */
void v_adddev()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	/*
	 *  Scan the set of specified devices.
	 */
	if (scan_devset() == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsdev_set (quename, ADD_OP);	/* Add the queue/device mappings */
	fsizedb (Qmapfile);		/* Mappings file may have expanded */
}


/*** v_addfor
 *
 *
 *	void v_addfor():
 *	ADd Form command.
 */
void v_addfor()
{
	if (scan_forset() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsfor_set (ADD_OP);		/* Add the forms to the forms set */
	fsizedb (Formsfile);		/* Forms file may have expanded */
}


/*** v_addgid
 *
 *
 *	void v_addgid():
 *	ADd Groups command.
 */
void v_addgid()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	/*
	 *  Scan the set of specified groups.
	 */
	if (scan_groupset() == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsgid_set (quename, ADD_OP);	/* Add the access by group */
}


/*** v_addman
 *
 *
 *	void v_addman():
 *	ADd Manager command.
 */
void v_addman()
{
	if (scan_mgrset() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsmgr_set (ADD_OP);		/* Add the accounts to the mgr set */
	fsizedb (Mgrfile);		/* Manager file may have expanded */
}


/*** v_addque
 *
 *
 *	void v_addque():
 *	ADd Queue to complex command.
 */
void v_addque()
{
        char qcom_name [MAX_QCOMPLXNAME+1];     /* Queue complex name */
        char *cp;                       /* Pointer to queue name */

        if (scan_equals() == -1) return;
        /*
         *  Scan the set of specified queues.
         */
        if (scan_queset() == -1) return;
        if ((cp = scan_qcomname()) == NULL) return;
        strcpy (qcom_name, cp);         /* Save queue complex name */
        if (scan_end() == -1) return;
        if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		/* Manager privileges required */
		diagnose (TCML_INSUFFPRV);
		return;	
	}
        adsque_set (qcom_name, ADD_OP); /* Add the queues */
        fsizedb (Qmapfile);             /* Mappings file may have expanded */
}


/*** v_adduid
 *
 *
 *	void v_adduid():
 *	ADd Users command.
 */
void v_adduid()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	/*
	 *  Scan the set of specified users.
	 */
	if (scan_userset() == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsuid_set (quename, ADD_OP);	/* Add the access by user */
}


/*** v_crebatque
 *
 *
 *	void v_crebatque():
 *	Create Batch_queue command.
 */
void v_crebatque()
{
	static char *qualifier_set[] = {
		"PIpeonly",
		"PRiority",
		"Run_limit",
		"User_limit",
		NULL
	};

	long comcode;			/* Completion code */
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	long long_int;			/* Used for integer scanning */
	int priority;			/* Queue priority */
	int runlimit;			/* Queue run-limit */
	int usrlimit;			/* Queue user-limit */
	short pipeonly;			/* BOOLEAN Pipeonly attr */
	register char *cp;		/* Character pointer */
	register int token;		/* Scan token */
	/*
	 *  Boolean variables:
	 */
	short pr_seen;			/* PRiority seen */
	short rl_seen;			/* Run_limit seen */
	short ul_seen;			/* User_limit seen */

	/*
	 *  No attributes seen yet.
	 */
	pr_seen = 0;
	rl_seen = 0;
	ul_seen = 0;
	pipeonly = 0;
	runlimit = 1;			/* Default run limit */
	usrlimit = 1;			/* Default user limit */

	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	/*
	 *  Get the name of the queue to be created.
	 */
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	/*
	 *  Get the queue attributes.
	 */
	while ((token = scan_qualifier (qualifier_set)) > 0) {
		/*
		 *  A qualifier keyword has been recognized.
		 */
		switch (token) {
		case 1:			/* Pipeonly */
			pipeonly = 1;	/* Queue has the pipeonly attr */
			break;
		case 2:			/* Priority */
			if (pr_seen) {
				errormessage (EM_MULPRI);
				return;
			}
			pr_seen = 1;	/* Set the seen flag */
			if (scan_equals() == -1) return;
			if (scan_int (&long_int, 0L,
				     (long) MAX_QPRIORITY,
				      EM_INTPRIEXP,
				      EM_PRIOUTOFBOU) == -1) return;
			/*
			 *  Successful priority value scan.
			 */
			priority = long_int;
			break;
		case 3:			/* Run_limit */
			if (rl_seen) {
				errormessage (EM_MULRUNLIM);
				return;
			}
			rl_seen = 1;	/* Set the seen flag */
			if (scan_equals() == -1) return;
			if (scan_int (&long_int, 1L, (long) 1000000,
				      EM_INTRUNLIMEXP,
				      EM_RUNLIMOUTOFBOU) == -1) {
				return;
			}
			/*
			 *  Good run-limit value.
			 */
			runlimit = long_int;
			break;
		case 4:			/* User_limit */
			if (ul_seen) {
				errormessage (EM_MULUSRLIM);
				return;
			}
			ul_seen = 1;	/* Set the seen flag */
			if (scan_equals() == -1) return;
			if (scan_int (&long_int, 1L, (long) 1000000,
				      EM_INTUSRLIMEXP,
				      EM_USRLIMOUTOFBOU) == -1) {
				return;
			}
			/*
			 *  Good User-limit value.
			 */
			usrlimit = long_int;
			break;
		}
	}
	if (token != 0) return;			/* An error occurred */
	/*
	 *  The end of the command has been reached.
	 */
	if (!pr_seen) {
		priority = 16;                  /* Default priority */
	}
	/*
	 *  Create the new queue.
	 */
	if ((comcode = crebatque (quename, priority,
		runlimit, pipeonly, usrlimit)) == TCML_COMPLETE) {
		fsizedb (Queuefile);		/* Queue file size  */
						/* may have changed */
		printf( "Queue %s created.\n", quename);
		/* There is nothing more to add to a BATCH queue */
		diagnose (TCML_COMPLETE);
	}
	else diagnose (comcode);		/* Diagnose failure */
}


/*** v_crecom
 *
 *
 *	void v_crecom():
 *	Create Complex command.
 */
void v_crecom()
{
        char qcom_name [MAX_QCOMPLXNAME+1];     /* Queue complex name */
        char *cp;                       /* Pointer to queue name */

        if (scan_equals() == -1) return;
        /*
         *  Scan the set of specified queues.
         */
        if (scan_queset() == -1) return;
        if ((cp = scan_qcomname()) == NULL) return;
        strcpy (qcom_name, cp);         /* Save queue complex name */
        if (scan_end() == -1) return;
        if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
                diagnose (TCML_INSUFFPRV);
                return;                 /* Manager privileges required */
        }
        diagnose(crecom (qcom_name));   /* Create the queue complex */
        fsizedb (Qmapfile);             /* Mappings file may have expanded */
        adsque_set (qcom_name, ADD_OP); /* Add the queues */
        fsizedb (Qmapfile);             /* Mappings file may have expanded */
}


/*** v_credev
 *
 *
 *	void v_credev():
 *	Create DEVICE command.
 */
void v_credev()
{
	static char *qualifier_set[] = {
		"FOrms",
		"FUllname",
		"Server",
		NULL
	};

	char devname [MAX_DEVNAME+1];	/* New device name */
	char formsname [MAX_FORMNAME+1];/* Forms name for device */
	char fullname [MAX_PATHNAME+1];	/* Full device name */
	char srvname [MAX_SERVERNAME+1];/* Server name */
	register char *cp;		/* Character pointer */
	register int token;		/* Scan token */

	/*
	 *  Boolean variables:
	 */
	short fo_seen;			/* FOrms seen */
	short fu_seen;			/* FUllname seen */
	short se_seen;			/* Server seen */

	/*
	 *  No attributes seen yet.
	 */
	fo_seen = 0;
	fu_seen = 0;
	se_seen = 0;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	/*
	 *  Get the name of the device to be created.
	 */
	if ((cp = scan_dname()) == NULL) return;
	strcpy (devname, cp);			/* Save device name */
	/*
	 *  Get the device attributes.
	 */
	while ((token = scan_qualifier (qualifier_set)) > 0) {
		/*
		 *  A qualifier keyword has been recognized.
		 */
		switch (token) {
		case 1:				/* FOrms */
			if (fo_seen) {
				errormessage (EM_MULDEVFOR);
				return;
			}
			fo_seen = 1;		/* Set the seen flag */
			if (scan_equals() == -1) return;
			if ((cp = scan_fname()) == NULL) return;
			strcpy (formsname, cp);	/* Save forms name */
			break;
		case 2:				/* FUllname */
			if (fu_seen) {
				errormessage (EM_MULDEVFULNAM);
				return;
			}
			fu_seen = 1;		/* Set the seen flag */
			if (scan_equals() == -1) return;
			if ((cp = scan_dfname()) == NULL) return;
			strcpy (fullname, cp);	/* Save full device-name */
			break;
		case 3:
			if (se_seen) {
				errormessage (EM_MULSER);
				return;
			}
			se_seen = 1;		/* Set the seen flag */
			if (scan_equals() == -1) return;
			if ((cp = scan_allinparens()) == NULL) return;
			strcpy (srvname, cp);	/* Save server specification */
			break;
		}
	} 
	/*
	 *  The end of the command has been reached.
	 */
	if (!fo_seen) {
		errormessage (EM_NODEVFORSPE);	/* No device forms specified */
		return;
	}
	if (!fu_seen) {
		errormessage(EM_NODEVFULNAMSPE);/* No device fullname */
		return;				/* specified */
	}
	if (!se_seen) {
		errormessage (EM_NOSERSPE);	/* No server specified */
		return;
	}
	/*
	 *  Create the device.
	 */
	diagnose (credev (devname, formsname, fullname, srvname));
	fsizedb (Devicefile);			/* Device file size */
						/* may have changed */
}


/*** v_credevque
 *
 *
 *	void v_credevque():
 *	Create DEVICE_queue command.
 */
void v_credevque()
{
	static char *qualifier_set[] = {
		"Device",
		"PIpeonly",
		"PRiority",
		NULL
	};

	long comcode;			/* Completion code */
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	long long_int;			/* Used for integer scanning */
	int priority;			/* Queue priority */
	short pipeonly;			/* BOOLEAN Pipeonly attr */
	register char *cp;		/* Character pointer */
	register int token;		/* Scan token */
	/*
	 *  Boolean variables:
	 */
	short de_seen;			/* Device seen */
	short pr_seen;			/* PRiority seen */

	/*
	 *  No attributes seen yet.
	 */
	de_seen = 0;
	pr_seen = 0;
	pipeonly = 0;
	/*
	 *  Get the name of the queue to be created.
	 */
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	/*
	 *  Get the queue attributes.
	 */
	while ((token = scan_qualifier (qualifier_set)) > 0) {
		/*
		 *  A qualifier keyword has been recognized.
		 */
		switch (token) {
		case 1:			/* Device set */
			if (de_seen) {
				errormessage (EM_MULDEVSET);
				return;
			}
			de_seen = 1;	/* Set the seen flag */
			if (scan_equals() == -1) return;
			/*
			 *  Scan the set of specified devices.
			 */
			if (scan_devset() == -1) return;
			break;
		case 2:			/* Pipeonly */
			pipeonly = 1;	/* Queue has the pipeonly attr */
			break;
		case 3:			/* Priority */
			if (pr_seen) {
				errormessage (EM_MULPRI);
				return;
			}
			pr_seen = 1;	/* Set the seen flag */
			if (scan_equals() == -1) return;
			if (scan_int (&long_int, 0L,
				     (long) MAX_QPRIORITY,
				      EM_INTPRIEXP,
				      EM_PRIOUTOFBOU) == -1) return;
			/*
			 *  Successful priority value scan.
			 */
			priority = long_int;
			break;
		}
	}
	if (token != 0) return;			/* An error occurred */
	/*
	 *  The end of the command has been reached.
	 */
	if (!pr_seen) {
		errormessage (EM_NOPRISPE);	/* No priority specified */
		return;
	}
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	/*
	 *  Create the new queue.
	 */
	if ((comcode = credevque (quename, priority,
				  pipeonly)) == TCML_COMPLETE) {
		/*
		 *  We were successful in our efforts to create the
		 *  device queue.  Now add queue/device mappings
		 *  as necessary.
		 */
		fsizedb (Queuefile);		/* Queue file size may have */
						/* gotten bigger */
		printf ("Queue %s created.\n", quename);
		/*
		 *  Add any queue to device mappings as
		 *  specified.
		 */
		adsdev_set (quename, ADD_OP);	/* Add the queue/device */
						/* mappings as appropriate */
		fsizedb (Qmapfile);		/* Mappings file may have */
						/* expanded */
	}
	else diagnose (comcode);		/* Diagnose error */
}


/*** v_crepipque
 *
 *
 *	void v_crepipque():
 *	Create Pipe_queue command.
 */
void v_crepipque()
{
	static char *qualifier_set[] = {
		"Destination",
		"PIpeonly",
		"PRiority",
		"Run_limit",
		"Server",
		"LB_Out",
		"LB_In",
		NULL
	};

	long comcode;			/* Completion code */
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	long long_int;			/* Used for integer scanning */
	int priority;			/* Queue priority */
	int runlimit;			/* Queue run-limit */
	short pipeonly;			/* BOOLEAN Pipeonly attr */
	short ldb;                      /* Bit vector for load balanced attributes */
	char srvname [MAX_SERVERNAME+1];/* Server for queue */
	register char *cp;		/* Character pointer */
	register int token;		/* Scan token */
	struct stat statbuf;            /* Lets see if that file is there */
	int stat_stat;                  /* How did the stat do? */
	char *lib_dir;
	/*
	 *  Boolean variables:
	 */
	short de_seen;			/* Destination seen */
	short pr_seen;			/* PRiority seen */
	short rl_seen;			/* Run_limit seen */
	short se_seen;			/* Server seen */

	/*
	 *  No attributes seen yet.
	 */
	de_seen = 0;
	pr_seen = 0;
	rl_seen = 0;
	se_seen = 0;
	pipeonly = 0;
	ldb = 0;
	runlimit = 1;			/* Default run limit */

	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	/*
	 *  Get the name of the queue to be created.
	 */
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	/*
	 *  Get the queue attributes.
	 */
	while ((token = scan_qualifier (qualifier_set)) > 0) {
		/*
		 *  A qualifier keyword has been recognized.
		 */
		switch (token) {
		case 1:			/* Destination set */
			if (de_seen) {
				errormessage (EM_MULDESSET);
				return;
			}
			de_seen = 1;	/* Set the seen flag */
			if (scan_equals() == -1) return;
			/*
			 *  Scan the set of specified destinations.
			 */
			if (scan_destset() == -1) return;
			break;
		case 2:			/* Pipeonly */
			pipeonly = 1;	/* Queue has the pipeonly attr */
			break;
		case 3:			/* Priority */
			if (pr_seen) {
				errormessage (EM_MULPRI);
				return;
			}
			pr_seen = 1;	/* Set the seen flag */
			if (scan_equals() == -1) return;
			if (scan_int (&long_int, 0L,
				     (long) MAX_QPRIORITY,
				      EM_INTPRIEXP,
				      EM_PRIOUTOFBOU) == -1) return;
			/*
			 *  Successful priority value scan.
			 */
			priority = long_int;
			break;
		case 4:			/* Run_limit */
			if (rl_seen) {
				errormessage (EM_MULRUNLIM);
				return;
			}
			rl_seen = 1;	/* Set the seen flag */
			if (scan_equals() == -1) return;
			if (scan_int (&long_int, 1L, (long) 1000000,
				      EM_INTRUNLIMEXP,
				      EM_RUNLIMOUTOFBOU) == -1) {
				return;
			}
			/*
			 *  Good run-limit value.
			 */
			runlimit = long_int;
			break;
		case 5:			/* Server */
			if (se_seen) {
				errormessage (EM_MULSER);
				return;
			}
			se_seen = 1;	/* Set the seen flag */
			if (scan_equals() == -1) return;
			if ((cp = scan_allinparens()) == NULL) return;
			/*
			 *  A good server specification has been
			 *  scanned.
			 */
			strcpy (srvname, cp);	/* Save server spec */
			stat_stat = stat(srvname, &statbuf);
			if (stat_stat == -1) {
			    errormessage( EM_NOSUCHSERVER);
			    return;
			}
			if (! S_ISREG(statbuf.st_mode)){
			    errormessage( EM_NOSUCHSERVER);
			    return;
			} 
			break;
		case 6:                 /* LB out */
			ldb |= QUE_LDB_OUT;
			break;
		case 7:                 /* LB in */
			ldb |= QUE_LDB_IN;
			break;
		}
	}
	if (token != 0) return;			/* An error occurred */
	/*
	 *  The end of the command has been reached.
	 */
	if (!pr_seen) {
		priority = 16;	                /* Default priority */
	}
	if (!se_seen) {
		lib_dir = getfilnam( "", LIBDIR );
		if (lib_dir == (char *)NULL) {
		    fprintf (stderr, "%s(FATAL): Unable to ", Nqsmgr_prefix);
		    fprintf (stderr, "determine library directory name.\n");
		    exit(2);
		}
		/*
		 * getfilnam puts in the slash before pipeclient ...
		 */
		sprintf(srvname, "%spipeclient", lib_dir);
		relfilnam (lib_dir);
	}
	if ( (ldb & QUE_LDB_IN) && (ldb & QUE_LDB_OUT) ) {
	        errormessage (EM_INVLDBFLAGS);  /* Invalid load balancing flags */
		return;
	}
	/*
	 *  Create the new queue.
	 */
	if ((comcode = crepipque (quename, priority, runlimit,
				  pipeonly, srvname, ldb)) == TCML_COMPLETE) {
		/*
		 *  We were successful in our efforts to create the
		 *  pipe queue.  Now add any destinations as required.
		 */
		fsizedb (Queuefile);		/* Queue file size may have */
						/* gotten bigger */
		printf ("Queue %s created.\n", quename);
		adsdes_set (quename, ADD_OP);	/* Add the destinations */
		fsizedb (Qmapfile);		/* Mappings file may have */
						/* expanded */
		fsizedb (Pipeqfile);		/* Pipe queue destination */
						/* file may have expanded */
	}
	else diagnose (comcode);		/* Diagnose error */
}


/*** v_delcom
 *
 *
 *	void v_delcom():
 *	DElete Complex command.
 */
void v_delcom()
{
        char qcom_name [MAX_QCOMPLXNAME+1];     /* Queue complex name */
        char *cp;                       /* Pointer to queue name */

        if ((cp = scan_qcomname()) == NULL) return;
        strcpy (qcom_name, cp);         /* Save queue complex name */
        if (scan_end() == -1) return;
        if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
                diagnose (TCML_INSUFFPRV);
                return;                 /* Manager privileges required */
        }
        diagnose(delcom(qcom_name));    /* Delete queue complex */
        fsizedb (Qmapfile);             /* Mappings file may have contracted */
}


/*** v_deldes
 *
 *
 *	void v_deldes():
 *	DElete DEStination command.
 */
void v_deldes()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	/*
	 *  Scan the set of specified destinations.
	 */
	if (scan_destset() == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsdes_set (quename, DEL_OP);	/* Delete the destinations */
}


/*** v_deldev
 *
 *
 *	void v_deldev():
 *	DElete DEVice command.
 */
void v_deldev()
{
	char devname [MAX_DEVNAME+1];	/* Device name */
	char *cp;			/* Pointer to device name, '=', */
					/* or queue name */
	char quename [MAX_QUEUENAME+1];	/* Queue name */

	if ((cp = scan_dename()) == NULL) return;
	if (*cp != '=') {
		/*
		 *  Delete a device.
		 */
		strcpy (devname, cp);	/* Squirrel away the device name */
		if (scan_end() == -1) return;
		if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
			diagnose (TCML_INSUFFPRV);
			return;		/* Manager privileges required */
		}
		diagnose (deldev (devname));
	}
	else {
		/*
		 *  Delete queue/device mappings.
		 *  Scan the set of specified devices to delete
		 *  from the mappings for the named queue at the
		 *  end of the command.
		 *
		 *  Scan the set of specified devices.
		 */
		if (scan_devset() == -1) return;
		if ((cp = scan_qname()) == NULL) return;
		strcpy (quename, cp);		/* Save queue name */
		if (scan_end() == -1) return;
		if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
			diagnose (TCML_INSUFFPRV);
			return;		/* Manager privileges required */
		}
		adsdev_set (quename, DEL_OP);	/* Delete the queue/device */
	}					/* mappings */
}


/*** v_delfor
 *
 *
 *	void v_delfor():
 *	Delete Form command.
 */
void v_delfor()
{
	if (scan_forset() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsfor_set (DEL_OP);		/* Delete the forms from the */
}					/* forms set */


/*** v_delgid
 *
 *
 *	void v_delgid():
 *	DElete Groups command.
 */
void v_delgid()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	/*
	 *  Scan the set of specified groups.
	 */
	if (scan_groupset() == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsgid_set (quename, DEL_OP);	/* Delete the access by group */
}


/*** v_delman
 *
 *
 *	void v_delman():
 *	DElete Manager command.
 */
void v_delman()
{
	if (scan_mgrset() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsmgr_set (DEL_OP);		/* Delete accounts from the mgr set */
}


/*** v_delque
 *
 *
 *	void v_delque():
 *	DElete Queue command.
 */
void v_delque()
{
	char quename [MAX_QUEUENAME+1];	/* Name of queue to be deleted */
	char *cp;			/* Pointer to queue name */

	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (delque (quename));
}
/*** v_deluid
 *
 *
 *	void v_deluid():
 *	DElete Users command.
 */
void v_deluid()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	/*
	 *  Scan the set of specified users.
	 */
	if (scan_userset() == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsuid_set (quename, DEL_OP);	/* Delete the access by user */
}


/*** v_disdev
 *
 *
 *	void v_disdev():
 *	DIsable Device command.
 */
void v_disdev()
{
	char devname [MAX_DEVNAME+1];	/* Name of device to be disabled */
	char *cp;			/* Pointer to device name */

	if ((cp = scan_dname()) == NULL) return;
	strcpy (devname, cp);		/* Save device name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (disdev (devname));
}


/*** v_disque
 *
 *
 *	void v_disque():
 *	DIsable Queue command.
 */
void v_disque()
{
	char quename [MAX_QUEUENAME+1];	/* Name of queue to be disabled */
	char *cp;			/* Pointer to queue name */

	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (disque (quename));
}


/*** v_enadev
 *
 *
 *	void v_enadev():
 *	Enable Device command.
 */
void v_enadev()
{
	char devname [MAX_DEVNAME+1];	/* Name of device to be enabled */
	char *cp;			/* Pointer to device name */

	if ((cp = scan_dname()) == NULL) return;
	strcpy (devname, cp);		/* Save device name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (enadev (devname));
}


/*** v_enaque
 *
 *
 *	void v_enaque():
 *	Enable Queue command.
 */
void v_enaque()
{
	char quename [MAX_QUEUENAME+1];	/* Name of queue to be enabled */
	char *cp;			/* Pointer to queue name */

	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (enaque (quename));
}


/*** v_exi
 *
 *
 *	void v_exi():
 *	Exit command.
 */
void v_exi()
{
	exiting();			/* Relinquish connection to the */
	fflush (stdout);		/* local daemon, flush all */
	fflush (stderr);		/* output buffers, and exit. */
	exit (0);
}


/*** v_locdae
 *
 *
 *	void v_locdae():
 *	Lock command.
 *
 */
void v_locdae()
{
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (locdae ());
}

/*
 *	v_memdump():
 *	Memory Dump
 */
void v_memdump()
{
    char    *cp;
    char    flag_name[128];

    if ((cp = scan_aname()) == NULL) return;
    strcpy (flag_name, cp);		/* Save the flag's name */
    if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
	diagnose (TCML_INSUFFPRV);
	return;			/* Operator privileges required */
    }

    diagnose (memdump(flag_name) );
}
/*** v_modreq
 *
 *
 *	void v_modreq();
 *	MODify Request command.
 */
void v_modreq()
{
        long req_seqno;                         /* Request sequence number */
        Mid_t orig_mid;                         /* Originating machine ident */
        /*
         *      Get the request identifier
         */
        if (scan_reqidname (&req_seqno, &orig_mid) == -1) {
            errormessage (EM_REQIDEXP);
            return;
        }
        /*
         *      Scan the set of modifications
         */
        if (scan_reqmodset() == -1) return;

        if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
            diagnose (TCML_INSUFFPRV);
            return;                 /* Operator privileges required */
        }

        modreq_set (req_seqno, (Mid_t) orig_mid, (uid_t) 0); /* Mod requests */
}


/*** v_movque
 *
 *
 *	void v_movque();
 *	MOVe Queue command.
 */
void v_movque()
{
        char src_qname [MAX_QUEUENAME+1];       /* Name of source queue */
        char des_qname [MAX_QUEUENAME+1];       /* Name of destn queue */
        char *cp;                       /* Pointer to queue name */

        if ((cp = scan_qname()) == NULL) return;
        strcpy (src_qname, cp);         /* Save source queue name */
        if ((cp = scan_qname()) == NULL) return;
        strcpy (des_qname, cp);         /* Save destin queue name */
        if (scan_end() == -1) return;
        if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
                diagnose (TCML_INSUFFPRV);
                return;                 /* Operator privileges required */
        }
        diagnose(movque(src_qname,des_qname));
}


/*** v_movreq
 *
 *
 *	void v_movreq();
 *	MOVe Request command.
 */
void v_movreq()
{
        char quename [MAX_QUEUENAME+1]; /* Queue name */
        char *cp;                       /* Pointer to queue name */

        if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
                diagnose (TCML_INSUFFPRV);
                return;                 /* Operator privileges required */
        }
        /*
         *  Scan the parameter set
         */

        if (scan_equals() == -1) return;

        /*
         *      Scan the set of request identifiers
         */

        if (scan_reqset() == -1) return;
        if ((cp = scan_qname()) == NULL) return;
        strcpy (quename, cp);               /* Save queue name */
        if (scan_end() == -1) return;
        if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
            diagnose (TCML_INSUFFPRV);
            return;                 /* Operator privileges required */
        }
        movreq_set (quename);       /* Move the requests to the queue */

}

 
/*** v_purque
 *
 *
 *	void v_purque():
 *	Purge Queue command.
 */
void v_purque()
{
	char quename [MAX_QUEUENAME+1];	/* Name of queue to be purged */
	char *cp;			/* Pointer to queue name */

	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (purque (quename));
}

 
/*** v_remque
 *
 *
 *	void v_remque():
 *	Remove Queue from complex command.
 */
void v_remque()
{
        char qcom_name [MAX_QCOMPLXNAME+1];     /* Queue name */
        char *cp;                       /* Pointer to queue name */

        if (scan_equals() == -1) return;
        /*
         *  Scan the set of specified queues.
         */
        if (scan_queset() == -1) return;
        if ((cp = scan_qcomname()) == NULL) return;
        strcpy (qcom_name, cp);         /* Save queue complex name */
        if (scan_end() == -1) return;
        if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
                diagnose (TCML_INSUFFPRV);
                return;                 /* Manager privileges required */
        }
        adsque_set (qcom_name, DEL_OP); /* Delete the queues */

}


/*** v_setcomrunlim
 *
 *
 *	void v_setcomrunlim();
 *	SEt COMplex Run_limit command.
 */
void v_setcomrunlim()
{
        long run_limit;                 /* Run-limit being set */
        char qcom_name [MAX_QCOMPLXNAME+1];     /* Queue complex name */
        char *cp;                       /* Pointer to queue complex name */

        if (scan_equals() == -1) return;
        if (scan_int (&run_limit, 1L, (long) 1000000L,
                      EM_INTRUNLIMEXP, EM_RUNLIMOUTOFBOU) == -1) return;
        if ((cp = scan_qcomname()) == NULL) return;
        strcpy (qcom_name, cp);
        if (scan_end() == -1) return;
        if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
                diagnose (TCML_INSUFFPRV);
                return;                 /* Operator privileges required */
        }
        diagnose (setcomrun (qcom_name, (int) run_limit));
}

/*** v_setcomrunlim
 *
 *
 *	void v_setcomuserlim();
 *	SEt COMplex User_limit command.
 */
void v_setcomuserlim()
{
        long user_limit;                 /* User-limit being set */
        char qcom_name [MAX_QCOMPLXNAME+1];     /* Queue complex name */
        char *cp;                       /* Pointer to queue complex name */

        if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
                diagnose (TCML_INSUFFPRV);
                return;                 /* Operator privileges required */
        }
        if (scan_equals() == -1) return;
        if (scan_int (&user_limit, 1L, (long) 1000000L,
                      EM_INTUSRLIMEXP, EM_USRLIMOUTOFBOU) == -1) return;
        if ((cp = scan_qcomname()) == NULL) return;
        strcpy (qcom_name, cp);
        if (scan_end() == -1) return;
        diagnose (setcomuser (qcom_name, (int) user_limit));
}


/*** v_setgbatlim
 *
 *
 *	void v_setgbatlim():
 *	SEt Global_batch_limit command.
 */
void v_setgbatlim()
{
	long gbatlim;			/* batch limit being set */

	if (scan_equals() == -1) return;
	if (scan_int (&gbatlim, 0L, 65535L, EM_INTGBATLIMEXP,
		      EM_GBATLIMOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setgbatlim ((int) gbatlim));
}
/*** v_setdeb
 *
 *
 *	void v_setdeb():
 *	SEt DEBug command.
 */
void v_setdeb()
{
	long debug;			/* Debug level being set */

	if (scan_int (&debug, 0L, 65535L, EM_INTDEBVALEXP,
		      EM_DEBVALOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setdeblev ((int) debug));
}

/*** v_setecho
 *
 *
 *	void v_setecho():
 *	SEt Echo command
 */
void v_setecho()
{
	Qmgr_echo = 1;
}
/*** v_setdefbatque
 *
 *
 *	void v_setdefbatque():
 *	SEt DEFault Batch_request Queue command.
 */
void v_setdefbatque()
{
	char quename [MAX_QUEUENAME+1];	/* Name of default queue */
	char *cp;			/* Pointer to queue name */

	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setbatque (quename));
}


/*** v_setdefbatpri
 *
 *
 *	void v_setdefbatpri():
 *	SEt DEFault Batch_request Priority command.
 */
void v_setdefbatpri()
{
	long default_pri;		/* Default priority being set */

	if (scan_int (&default_pri, 0L, (long) MAX_RPRIORITY,
		      EM_INTPRIEXP, EM_PRIOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setbatpri ((int) default_pri));
}


/*** v_setdefdestim
 *
 *
 *	void v_setdefdestim():
 *	SEt DEFault DEStination_retry Time command.
 */
void v_setdefdestim()
{
	long retry_hours;		/* Destination retry time in hours */

	if (scan_int (&retry_hours, 0L, 10000L,
		      EM_INTDESTIMEXP, EM_DESTIMOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setdestim (retry_hours * 3600));
}


/*** v_setdefdeswai
 *
 *
 *	void v_setdefdeswai():
 *	SEt DEFault DEStination_retry Wait command.
 */
void v_setdefdeswai()
{
	long dest_wait;			/* Default destination retry wait */
					/* time in minutes */

	if (scan_int (&dest_wait, 1L, 100000L,
		      EM_INTDESWAIEXP, EM_DESWAIOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setdeswai ((dest_wait * 60L)));
}

/*** v_setdefloadint
 *
 *
 *	void v_setdefloadint():
 *	SEt DEFault Load Interval command.
 */
void v_setdefloadint()
{
	long load_int;			/* Default load interval */
					/* time in minutes */

	if (scan_int (&load_int, 1L, 100000L,
		      EM_INTLOADINTEXP, EM_LOADINTOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setdefloadint ((load_int * 60L)));

}


/*** v_setdefdevpri
 *
 *
 *	void v_setdefdevpri():
 *	SEt DEFault DEVice_request Priority command.
 */
void v_setdefdevpri()
{
	long default_pri;		/* Default priority being set */

	if (scan_int (&default_pri, 0L, (long) MAX_RPRIORITY,
		      EM_INTPRIEXP, EM_PRIOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setdevpri ((int) default_pri));
}


/*** v_setdefprifor
 *
 *
 *	void v_setdefprifor():
 *	SEt DEFault Print_request Forms command.
 */
void v_setdefprifor()
{
	char form [MAX_FORMNAME+1];	/* Form name */
	char *cp;			/* Pointer to form name */

	if ((cp = scan_fname()) == NULL) return;
	strcpy (form, cp);		/* Save forms name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setprifor (form));
}


/*** v_setdefprique
 *
 *
 *	void v_setdefprique():
 *	SEt DEFault Print_request Queue command.
 */
void v_setdefprique()
{
	char quename [MAX_QUEUENAME+1];	/* Name of default queue */
	char *cp;			/* Pointer to queue name */

	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setprique (quename));
}


/*** v_setdes
 *
 *
 *	void v_setdes():
 *	SEt DEStination command.
 */
void v_setdes()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	/*
	 *  Scan the set of specified destinations.
	 */
	if (scan_destset() == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsdes_set (quename, SET_OP);	/* Set the destinations */
	fsizedb (Qmapfile);		/* Mappings file may have expanded */
	fsizedb (Pipeqfile);		/* Pipe queue destination file */
					/* may have expanded */
}


/*** v_setdev
 *
 *
 *	void v_setdev():
 *	SEt DEVICE command.
 */
void v_setdev()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	/*
	 *  Scan the set of specified devices.
	 */
	if (scan_devset() == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsdev_set (quename, SET_OP);	/* Set the queue/device mappings */
	fsizedb (Qmapfile);		/* Mappings file may have expanded */
}


/*** v_setdevser
 *
 *
 *	void v_setdevser():
 *	SEt DEVICE_server command.
 */
void v_setdevser()
{
	char devname [MAX_DEVNAME+1];	/* Device name */
	char srvname [MAX_SERVERNAME+1];/* Server for device */
	char *cp;			/* Pointer to name */

	if (scan_equals() == -1) return;
	if ((cp = scan_allinparens()) == NULL) return;
	strcpy (srvname, cp);		/* Save server spec */
	if ((cp = scan_dname()) == NULL) return;
	strcpy (devname, cp);		/* Save device name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setdevser (devname, srvname));
}


/*** v_setfor
 *
 *
 *	void v_setfor():
 *	SEt Forms command.
 */
void v_setfor()
{
	char formname [MAX_FORMNAME+1];
	char devname [MAX_DEVNAME+1];
	char *cp;

	switch(scan_forset()) {
	
	case -2:	/* it was "set forms = <forms-name> <device-name>" */
			/* not "set forms <forms-name> ..." */
		
		if ((cp = scan_fname()) == NULL) return;
		strcpy (formname, cp);
		if ((cp = scan_dname()) == NULL) return;
		strcpy (devname, cp);
		if (scan_end() == -1) return;
		if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
			diagnose (TCML_INSUFFPRV);
			return;		/* operator privileges required.*/
		}
		diagnose (setdevfor (devname, formname));
		break;
	
	case -1:			/* parse error */
		return;
	
	case 0:		/* it was "set forms <forms-name> ..." */

		if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
			diagnose (TCML_INSUFFPRV);
			return;		/* Manager privileges required */
		}
		adsfor_set (SET_OP);	/* Set the forms to the forms set */
		fsizedb (Formsfile);	/* Forms file may have expanded */
		
	default:
		return;
	}
}


/*** v_setliftim
 *
 *
 *	void v_setliftim():
 *	SEt LIfetime command.
 */
void v_setliftim()
{
	long lifetime;			/* Pipe queue req lifetime in hours */

	if (scan_int (&lifetime, 1L, 100000L,
		      EM_INTLIFTIMEXP, EM_LIFTIMOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setlife ((lifetime * 3600L)));
}


/*** v_setmai
 *
 *
 *	void v_setmai():
 *	SEt MAIl command.
 */
void v_setmai()
{
	char acct [MAX_ACCOUNTNAME+1];	/* Account name */
	char *cp;			/* Pointer to queue name */
	register struct passwd *passwd;	/* Password file entry */

	if ((cp = scan_aname()) == NULL) return;
	strcpy (acct, cp);		/* Save account name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	if ((passwd = sal_fetchpwnam (acct)) == NULL) diagnose (TCML_NOSUCHACC);
	else {
		sal_closepwdb();		/* Close account/password database */
		diagnose (setnqsmai ((uid_t) passwd->pw_uid));
	}
}


/*** v_setman
 *
 *
 *	void v_setman():
 *	SEt MANager command.
 */
void v_setman()
{
	if (scan_mgrset() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	adsmgr_set (SET_OP);		/* Set the accounts to the mgr set */
	fsizedb (Mgrfile);		/* Manager file may have expanded */
}


/*** v_setmaxcop
 *
 *
 *	void v_setmaxcop():
 *	SEt MAXimum Copies command.
 */
void v_setmaxcop()
{
	long max_copies;		/* Maximum copies on device req. */

	if (scan_int (&max_copies, 1L, 1000L,
		      EM_INTMAXCOPEXP, EM_MAXCOPOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setmaxcop (max_copies));
}


/*** v_setmaxoperet
 *
 *
 *	void v_setmaxoperet():
 *	SEt MAXimum Open_retries command.
 */
void v_setmaxoperet()
{
	long retry_limit;		/* Failed device open retry limit */

	if (scan_int (&retry_limit, 0L, 1000000L,
		      EM_INTMAXOPERETEXP, EM_MAXOPERETOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setmaxopr (retry_limit));
}


/*** v_setmaxprisiz
 *
 *
 *	void v_setmaxprisiz():
 *	SEt MAXimum Print_size command.
 */
void v_setmaxprisiz()
{
	long max_printsize;		/* Maximum print size */

	if (scan_int (&max_printsize, 1L, 100000000L,
		      EM_INTMAXPRISIZEXP, EM_MAXPRISIZOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setmaxpsz (max_printsize));
}


/*** v_setndfbatque
 *
 *
 *	void v_setndfbatque():
 *	SEt NO_Default Batch_request Queue command.
 */
void v_setndfbatque()
{
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setndfbat());
}


/*** v_setndfprifor
 *
 *
 *	void v_setndfprifor():
 *	SEt NO_Default Print_request Forms command.
 */
void v_setndfprifor()
{
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setndffor());
}


/*** v_setndfprique
 *
 *
 *	void v_setndfprique():
 *	SEt NO_Default Print_request Queue command.
 */
void v_setndfprique()
{
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setndfpri());
}


/*** v_setnetcli
 *
 *
 *	void v_setnetcli();
 * 	SEt NEtwork Client command.
 */
void v_setnetcli()
{
	char srvname[MAX_SERVERNAME+1];
	char *cp;

	if (scan_equals() == -1) return;
	if ((cp = scan_allinparens()) == NULL) return;
	strcpy (srvname, cp);
	if (scan_end() == -1) return;
	if (( Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setnetcli (srvname));
}


/*** v_setnloaddae
 *
 *
 *	void v_setnloaddae();
 * 	SEt NO_LOad_Daemon command.
 */
void v_setnloaddae()
{
	if (scan_end() == -1) return;
	if (( Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setloaddae ("no-load-daemon"));
}
/*** v_setloaddae
 *
 *
 *	void v_setloaddae();
 * 	SEt Load Daemon command.
 */
void v_setloaddae()
{
	char srvname[MAX_SERVERNAME+1];
	char *cp;

	if (scan_equals() == -1) return;
	if ((cp = scan_allinparens()) == NULL) return;
	strcpy (srvname, cp);
	if (scan_end() == -1) return;
	if (( Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setloaddae (srvname));
}
/*** v_setnetdae
 *
 *
 *	void v_setnetdae();
 * 	SEt NEtwork Daemon command.
 */
void v_setnetdae()
{
	char srvname[MAX_SERVERNAME+1];
	char *cp;

	if (scan_equals() == -1) return;
	if ((cp = scan_allinparens()) == NULL) return;
	strcpy (srvname, cp);
	if (scan_end() == -1) return;
	if (( Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setnetdae (srvname));
}


/*** v_setnetser
 *
 *
 *	void v_setnetser();
 * 	SEt NEtwork Server command.
 */
void v_setnetser()
{
	char srvname[MAX_SERVERNAME+1];
	char *cp;

	if (scan_equals() == -1) return;
	if ((cp = scan_allinparens()) == NULL) return;
	strcpy (srvname, cp);
	if (scan_end() == -1) return;
	if (( Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setnetser (srvname));
}


/*** v_setnnedae
 *
 *
 *	void v_setnnedae();
 *	SEt NO_network_daemon command.
 */
void v_setnnedae()
{
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setnnedae ());
}


/*** v_setnoacc
 *
 *
 *	void v_setnoacc():
 *	SEt NO_Access command.
 */
void v_setnoacc()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */
	
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setnoqueacc (quename));
}

/*** v_setlbin
 *
 *
 *	void v_setlbin():
 *	SEt_LB_In command.
 */
void v_setlbin()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */
	
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setquechar (quename, QUE_LDB_IN));
}
/*** v_setlbout
 *
 *
 *	void v_setlbout():
 *	SEt_LB_Out command.
 */
void v_setlbout()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */
	
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setquechar (quename, QUE_LDB_OUT));
}
/*** v_setpipeonly
 *
 *
 *	void v_setpipeonly():
 *	SEt_pipeonly command.
 */
void v_setpipeonly()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */
	
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setquechar (quename, QUE_PIPEONLY));
}
/*** v_setnolbin
 *
 *
 *	void v_setnolbin():
 *	SEt NO_LB_In command.
 */
void v_setnolbin()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */
	
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setnoquechar (quename, QUE_LDB_IN));
}

/*** v_setnolbout
 *
 *
 *	void v_setnolbout():
 *	SEt NO_LB_Out command.
 */
void v_setnolbout()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */
	
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setnoquechar (quename, QUE_LDB_OUT));
}

/*** v_setnopipeonly
 *
 *
 *	void v_setnopipeonly():
 *	SEt NO_PIPEOnly command.
 */
void v_setnopipeonly()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */
	
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setnoquechar (quename, QUE_PIPEONLY));
}


/*** v_setopewai
 *
 *
 *	void v_setopewai():
 *	SEt Open_wait command.
 */
void v_setopewai()
{
	long retry_wait;		/* Time to wait between failed */
					/* device open retry attempts */

	if (scan_int (&retry_wait, 1L, 1000L,
		      EM_INTOPEWAIEXP, EM_OPEWAIOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setopewai (retry_wait));
}

/*** v_setpipcli
 *
 *
 *	void v_setpipcli():
 *	SEt PIpe_client command.
 */
void v_setpipcli()
{
	struct stat statbuf;            /* Lets see if that file is there */
	int stat_stat;                  /* How did the stat do? */
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char srvname [MAX_SERVERNAME+1];/* The pipe client */
	char *cp;			/* Pointer to name */

	if (scan_equals() == -1) return;
	if ((cp = scan_allinparens()) == NULL) return;
	strcpy (srvname, cp);		/* Save client specification */
	stat_stat = stat(srvname, &statbuf);
	if (stat_stat == -1) {
	    errormessage( EM_NOSUCHSERVER );
	    return;
	}
	if (! S_ISREG(statbuf.st_mode)){
	    errormessage( EM_NOSUCHSERVER);
	    return;
	} 
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
	    diagnose (TCML_INSUFFPRV);
	    return;			/* Manager privileges required */
	}
	diagnose (setpipcli (quename, srvname));
}


/*** v_setppcore
 *
 *
 *	void v_setppcore():
 *	SEt CORefile_limit command.
 */
void v_setppcore()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setppcore (quename, quotalimit.max_quota,
				     quotalimit.max_units, infinite));
	}
}


/*** v_setppcput
 *
 *
 *	void v_setppcput():
 *	SEt PER_Process Cpu_limit command.
 */
void v_setppcput()
{
	struct cpulimit cpulimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (cpupreamble (quename, &cpulimit, &infinite) != -1) {
		diagnose (setppcput (quename, cpulimit.max_seconds,
				    cpulimit.max_ms, infinite));
	}
}


/*** v_setppdata
 *
 *
 *	void v_setppdata():
 *	SEt DAta_limit command.
 */
void v_setppdata()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setppdata (quename, quotalimit.max_quota,
				     quotalimit.max_units, infinite));
	}
}


/*** v_setppmem
 *
 *
 *	void v_setppmem():
 *	SEt PER_Process Memory_limit command.
 */
void v_setppmem()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setppmem (quename, quotalimit.max_quota,
				    quotalimit.max_units, infinite));
	}
}


/*** v_setndp
 *
 *
 *      void v_setndp():
 *      SEt NDP command.
 */
void v_setndp()
{
        long ndp_value;
        char quename [MAX_QUEUENAME + 1];
        register char *cp;

        if (scan_equals() == -1) return;
        if (scan_int (&ndp_value, (long) MIN_QUENDP, (long) MAX_QUENDP,
                EM_INTNDPEXP, EM_NDPOUTOFBOU) == -1) return;
        if ((cp = scan_qname()) == NULL) return;
        strcpy (quename, cp);
        if (scan_end() == -1) return;
        if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
                diagnose (TCML_INSUFFPRV);
                return;                 /* Manager privileges required */
        }
        diagnose (setndp (quename, (int) ndp_value));
}

/*** v_setppnice
 *
 *
 *	void v_setppnice():
 *	SEt NIce_value_limit command.
 */
void v_setppnice()
{
	long nice_value;
	char quename [MAX_QUEUENAME + 1];
	register char *cp;

	if (scan_equals() == -1) return;
	if (scan_int (&nice_value, (long) MIN_REQNICE, (long) MAX_REQNICE,
		EM_INTNICEEXP, EM_NICEOUTOFBOU) == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setppnice (quename, (int) nice_value));
}


/*** v_setpppfile
 *
 *
 *	void v_setpppfile():
 *	SEt PER_Process Permfile_limit command.
 */
void v_setpppfile()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setpppfile (quename, quotalimit.max_quota,
				      quotalimit.max_units, infinite));
	}
}


/*** v_setppstack
 *
 *
 *	void v_setppstack():
 *	SEt STack_limit command.
 */
void v_setppstack()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setppstack (quename, quotalimit.max_quota,
				      quotalimit.max_units, infinite));
	}
}


/*** v_setpptfile
 *
 *
 *	void v_setpptfile():
 *	SEt PER_Process Tempfile_limit command.
 */
void v_setpptfile()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setpptfile (quename, quotalimit.max_quota,
				      quotalimit.max_units, infinite));
	}
}


/*** v_setppwork
 *
 *
 *	void v_setppwork():
 *	SEt Working_set_limit command.
 */
void v_setppwork()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setppwork (quename, quotalimit.max_quota,
				     quotalimit.max_units, infinite));
	}
}


/*** v_setprcput
 *
 *
 *	void v_setprcput():
 *	SEt PER_Request Cpu_limit command.
 */
void v_setprcput()
{
	struct cpulimit cpulimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (cpupreamble (quename, &cpulimit, &infinite) != -1) {
		diagnose (setprcput (quename, cpulimit.max_seconds,
				    cpulimit.max_ms, infinite));
	}
}


/*** v_setpri
 *
 *
 *	void v_setpri():
 *	SEt PRiority command.
 */
void v_setpri()
{
	long priority;			/* Priority being set */
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	if (scan_int (&priority, 0L, (long) MAX_QPRIORITY,
		      EM_INTPRIEXP, EM_PRIOUTOFBOU) == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setquepri (quename, (int) priority));
}


/*** v_setprmem
 *
 *
 *	void v_setprmem():
 *	SEt PER_Request Memory_limit command.
 */
void v_setprmem()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setprmem (quename, quotalimit.max_quota,
				    quotalimit.max_units, infinite));
	}
}


/*** v_setprpfile
 *
 *
 *	void v_setprpfile():
 *	SEt PER_Request Permfile_limit command.
 */
void v_setprpfile()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setprpfile (quename, quotalimit.max_quota,
				      quotalimit.max_units, infinite));
	}
}


/*** v_setprtfile
 *
 *
 *	void v_setprtfile():
 *	SEt PER_Request Tempfile_limit command.
 */
void v_setprtfile()
{
	struct quotalimit quotalimit;
	char quename [MAX_QUEUENAME + 1];
	short infinite;

	if (quotapreamble (quename, &quotalimit, &infinite) != -1) {
		diagnose (setprtfile (quename, quotalimit.max_quota,
				      quotalimit.max_units, infinite));
	}
}

/*** v_setnoscheduler
 * 
 *
 *	void v_setnoscheduler():
 *	SEt NO_SCheduler command.
 */
void v_setnoscheduler()
{
        if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
                diagnose (TCML_INSUFFPRV);
                return;                 /* Manager privileges required */

	}
	diagnose (setnqssched ( 0 ) );
}

/*** v_setscheduler
 * 
 *
 *	void v_setscheduler():
 *	SEt SCheduler command.
 */
void v_setscheduler()
{
	Mid_t scheduler_mid;
	char scheduler_name [256];	/* The scheduler's name */
	char *cp;			/* Pointer to queue name */
        struct hostent *ent;


	if ((cp = scan_aname()) == NULL) return;
	strcpy (scheduler_name, cp);		/* Save scheduler's name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
        if ((ent = gethostbyname (scheduler_name)) == (struct hostent *) 0) {
		diagnose (TCML_NOSUCHMAC);
		return;
                                                /* This machine is not */
                                                /* known to us */
        }
        switch(nmap_get_mid (ent, &scheduler_mid)) {
        case NMAP_ENOMAP:               /* What?  No local mid defined! */
		diagnose (TCML_NOSUCHMAC);
		return;
        case NMAP_ENOPRIV:              /* No privilege */
		diagnose (TCML_INSUFFPRV);
		return;
        }
	diagnose (setnqssched (scheduler_mid) );
}

/*** v_setusrlim
 *
 *
 *	void v_setusrlim():
 *	SEt USer_limit command.
 */
void v_setusrlim()
{
	long usr_limit;			/* Run-limit being set */
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	if (scan_int (&usr_limit, 1L, (long) 1000000,
		      EM_INTUSRLIMEXP, EM_USRLIMOUTOFBOU) == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (setqueusr (quename, (int) usr_limit));
}


/*** v_setrunlim
 *
 *
 *	void v_setrunlim():
 *	SEt Run_limit command.
 */
void v_setrunlim()
{
	long run_limit;			/* Run-limit being set */
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */

	if (scan_equals() == -1) return;
	if (scan_int (&run_limit, 1L, (long) 1000000L,
		      EM_INTRUNLIMEXP, EM_RUNLIMOUTOFBOU) == -1) return;
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (setquerun (quename, (int) run_limit));
}

/*** v_setservavail
 *
 *
 *	void v_setservavail():
 *	SEt SErver Available command.
 */
void v_setservavail()
{
	char servername [MAX_SERVERNAME+1];/* Server name */
	Mid_t servermid;		/* The server's mid */
	char *cp;			/* Pointer to server name */
	struct hostent *ent;

	if (scan_equals() == -1) return;
	if ((cp = scan_servername()) == NULL) return;
	strcpy (servername, cp);
	if (scan_end() == -1) return;
        if ((ent = gethostbyname (servername)) == (struct hostent *) 0) {
		diagnose (TCML_NOSUCHMAC);
		return;
        }
        switch(nmap_get_mid (ent, &servermid)) {
            case NMAP_ENOMAP:               /* What?  No local mid defined! */
                diagnose (TCML_NETDBERR);
                return;
            case NMAP_ENOPRIV:              /* No privilege */
                diagnose (TCML_INSUFFPRV);
                return;
        }
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setservavail (servermid));
}

/*** v_setservnoperf
 *
 *
 *	void v_setservnoperf():
 *	SEt SErver Noperformance command.
 */
void v_setservnoperf()
{
	char servername [MAX_SERVERNAME+1];/* Server name */
	Mid_t servermid;		/* The server's mid */
	char *cp;			/* Pointer to server name */
	struct hostent *ent;

	if ((cp = scan_servername()) == NULL) return;
	strcpy (servername, cp);
	if (scan_end() == -1) return;
        if ((ent = gethostbyname (servername)) == (struct hostent *) 0) {
		diagnose (TCML_NOSUCHMAC);
		return;
        }
        switch(nmap_get_mid (ent, &servermid)) {
            case NMAP_ENOMAP:               /* What?  No local mid defined! */
                diagnose (TCML_NETDBERR);
                return;
            case NMAP_ENOPRIV:              /* No privilege */
                diagnose (TCML_INSUFFPRV);
                return;
        }
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setservperf (servermid, (int) 0));
}

/*** v_setservperf
 *
 *
 *	void v_setservperf():
 *	SEt SErver Performance command.
 */
void v_setservperf()
{
	long performance;		/* Performance being set */
	Mid_t servermid;		/* The server's mid */
	char servername [MAX_SERVERNAME+1]; /* Server name */
	char *cp;			/* Pointer to server name */
	struct hostent *ent;

	if (scan_equals() == -1) return;
	if (scan_int (&performance, 1L, (long) MAX_SERVERPERF,
		      EM_INTPERFEXP, EM_PERFOUTOFBOU) == -1) return;
	if ((cp = scan_servername()) == NULL) return;
	strcpy (servername, cp);
	if (scan_end() == -1) return;
        if ((ent = gethostbyname (servername)) == (struct hostent *) 0) {
		diagnose (TCML_NOSUCHMAC);
		return;
        }
        switch(nmap_get_mid (ent, &servermid)) {
            case NMAP_ENOMAP:               /* What?  No local mid defined! */
                diagnose (TCML_NETDBERR);
                return;
            case NMAP_ENOPRIV:              /* No privilege */
                diagnose (TCML_INSUFFPRV);
                return;
        }

	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setservperf (servermid, (int) performance));
}

/*** v_setshsfix
 *
 *
 *	void v_setshsfix():
 *	SEt SHell_strategy FIxed command.
 */
void v_setshsfix()
{
	struct stat statbuf;            /* Lets see if that file is there */
	int stat_stat;                  /* How did the stat do? */
	char shname [MAX_SERVERNAME+1];
	char *cp;

	if (scan_equals() == -1) return;
	if ((cp = scan_allinparens()) == NULL) return;
	strcpy (shname, cp);
	if (scan_end() == -1) return;
	stat_stat = stat(shname, &statbuf);
	if (stat_stat == -1) {
	    errormessage( EM_NOSUCHSHELL);
	    return;
	}
	if (! S_ISREG(statbuf.st_mode)){
	    errormessage( EM_NOSUCHSHELL);
	    return;
	}
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}
	diagnose (setshsfix (shname));
}


/*** v_setshsfre
 *
 *
 *	void v_setshsfre():
 *	SEt SHell_strategy FRee command.
 */
void v_setshsfre()
{
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setshsfre());
}


/*** v_setshslog
 *
 *
 *	void v_setshslog():
 *	SEt SHell_strategy Login command.
 */
void v_setshslog()
{
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setshslog());
}


/*** v_setunracc
 *
 *
 *	void v_setunracc():
 *	SEt UNrestricted_access command.
 */
void v_setunracc()
{
	char quename [MAX_QUEUENAME+1];	/* Queue name */
	char *cp;			/* Pointer to queue name */
	
	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);		/* Save queue name */
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;
	}
	diagnose (setunrqueacc (quename));
}


/*** v_shutdown
 *
 *
 *	void v_shutdown():
 *	SHUtdown command.
 */
void v_shutdown()
{
	int wait_time;			/* Shutdown SIGKILL wait time */
	long long_int;

	if (scan_opwait (&long_int, (long) SHUTDOWN_WAIT, 0L, 600L,
			 EM_INTWAITIMEXP, EM_WAITIMOUTOFBOU) == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	wait_time = long_int;
	diagnose (nqsshutdn (wait_time));
}

/*** v_snap
 *
 *     void v_snap();
 *     SNap command.
 *
 */
void v_snap()
{
    static char *qualifier_set[] = {
	      "File",
	      NULL
    };
    char snap_file [MAX_PATHNAME+1];
    register char *cp;
    register int token;

    short fi_seen;                     /* File seen */

    if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
	diagnose (TCML_INSUFFPRV);
	return;	  		       /* Manager privileges required */
    }
    fi_seen = 0;
    /*
     *  Get the snap file name.
     */
    while ((token = scan_qualifier (qualifier_set)) > 0) {
	/*
	 *  A qualifier keyword has been recognized.
	 */
	switch (token) {
	    case 1:			/* Destination set */
		if (fi_seen) {
		    errormessage (EM_MULSNAPFILE);
		    return;
		}
		fi_seen = 1;	/* Set the seen flag */
		if (scan_equals() == -1) return;
		if ((cp = scan_allinparens()) == NULL) return;
		/*
		 *  Scan for the name of the snap file.
		 */
		strcpy (snap_file, cp);
		break;
	    }
    }
    if (token != 0) return;			/* An error occurred */
    /*
     *  The end of the command has been reached.
     */
    if (!fi_seen) {
	errormessage (EM_NOSNAPFILE);	/* No file specified */
	return;
    } 
    diagnose (snap(snap_file) );
}

/*** v_startnqs
 *
 *
 *	void v_startnqs():
 *	STArt Nqs command.
 */
void v_startnqs()
{
	int status;
	uid_t myuid;
	int daepresent;
	char start_command[512];
	char *lib_dir;

	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Manager privileges required */
	}

#if	HAS_BSD_PIPE
	daepresent = daepres (Queuefile);
#else
	daepresent = daepres();
#endif
        if (daepresent) {
	    errormessage (EM_NQSRUNNING);
	    return;
	}
	myuid = getuid();
	status = setuid(0);
	if (status) {
	    errormessage (EM_NOGOROOT);
	    return;
	}
	lib_dir = getfilnam( "", LIBDIR );
	if (lib_dir == (char *)NULL) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Nqsmgr_prefix);
	    fprintf (stderr, "determine library directory name.\n");
	    exit(2);
	}
	/*
	 * getfilnam puts in the slash before nqsdaemon ...
	*/
	sprintf(start_command, "%snqsdaemon > /dev/null &", lib_dir);
	relfilnam (lib_dir);
	status = system(start_command);
/*	setuid(myuid); */
	if (status < 0) {
	    errormessage(EM_NQSNOTSTART);
	    return;
	}
	else diagnose (TCML_COMPLETE);
}

/*** v_staque
 *
 *
 *	void v_staque():
 *	STArt Queue command.
 */
void v_staque()
{
	char quename [MAX_QUEUENAME+1];	/* Name of queue to be started */
	char *cp;			/* Pointer to queue name */

	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);				/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (staque (quename));
}


/*** v_stoque
 *
 *
 *	void v_stoque():
 *	STOp Queue command.
 */
void v_stoque()
{
	char quename [MAX_QUEUENAME+1];	/* Name of queue to be stopped */
	char *cp;			/* Pointer to queue name */

	if ((cp = scan_qname()) == NULL) return;
	strcpy (quename, cp);				/* Save queue name */
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (stoque (quename));
}


/*** v_startload
 *
 *
 *	void v_startload():
 *	STart Load_daemon command.
 */
void v_startload()
{
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (control_daemon(START_DAEMON, LOAD_DAEMON) );
}
/*** v_startnetdae
 *
 *
 *	void v_startnetdae():
 *	STart Network_daemon command.
 */
void v_startnetdae()
{
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (control_daemon(START_DAEMON, NET_DAEMON) );
}
/*** v_stoload
 *
 *
 *	void v_stoload():
 *	STOp Load_daemon command.
 */
void v_stoload()
{
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (control_daemon(STOP_DAEMON, LOAD_DAEMON) );
}
/*** v_stonetdae
 *
 *
 *	void v_stonetdae():
 *	STOp Network_daemon command.
 */
void v_stonetdae()
{
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (control_daemon(STOP_DAEMON, NET_DAEMON) );
}

/*** v_unldae
 *
 *
 *	void v_unldae():
 *	Unlock Local_daemon command.
 */
void v_unldae()
{
	if (scan_end () == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (unldae ());
}



/*** cpupreamble
 *
 *
 *	int cpupreamble ():
 *
 *	Perform all of the necessary preamble work for a command that
 *	sets a CPU time quota limit.
 *
 *	Returns:
 *		 0: if scanning was successful, and privileges are ok;
 *		-1: otherwise.
 */
static int cpupreamble
(
  char quename [MAX_QUEUENAME + 1],
  struct cpulimit *cpulimit,
  short *infinite
)
{
	register char *cp;

	if (scan_equals() == -1) return (-1);
	if ((cp = scan_allinparens()) == (char *) 0) return (-1);
	*infinite = 0;
	if (*cp == 'u') {
		cpulimit->max_seconds = 0;		/* Give these fields */
		cpulimit->max_ms = 0;			/* values that will */
		cpulimit->warn_seconds = 0;		/* pass integrity */
		cpulimit->warn_ms = 0;			/* checks */
		*infinite = 1;
	}
	else {
		switch (scancpulim (cp, cpulimit, 1)) {
		case 0:
			break;
		case -1:
			errormessage (EM_INVCPULIMSYN);
			return (-1);
		case -2:
			errormessage (EM_BADCPULIMVAL);
			return (-1);
		case -3:
			errormessage (EM_CPULIMEXP);
			return (-1);
		}
	}
#if	IS_SGI	
	if (cpulimit->max_seconds > 21474836) {
		errormessage (EM_BADCPULIMVAL);
		return (-1);
	}
#endif
	if ((cp = scan_qname()) == (char *) 0) return (-1);
	strcpy (quename, cp);
	if (scan_end() == -1) return (-1);
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return (-1);		/* Manager privileges required */
	}
	return (0);			/* Scanning successful, and */
}					/* privileges are sufficient */


/*** quotapreamble
 *
 *
 *	int quotapreamble():
 *
 *	Perform all of the necessary preamble work for a command that
 *	sets a quota limit having nothing to do with "nice" or cpu limits.
 *
 *	Returns:
 *		 0: if scanning was successful, and privileges are ok;
 *		-1: otherwise.
 */
static int quotapreamble 
(
  char quename [MAX_QUEUENAME + 1], 
  struct quotalimit *quotalimit,
  short *infinite
)
{
	register char *cp;

	if (scan_equals() == -1) return (-1);
	if ((cp = scan_allinparens()) == (char *) 0) return (-1);
	*infinite = 0;
	if (*cp == 'u') {
		quotalimit->max_quota = 0;		/* Give these fields */
		quotalimit->max_units = QLM_BYTES;	/* values that will */
		quotalimit->warn_quota = 0;		/* pass integrity */
		quotalimit->warn_units = QLM_BYTES;	/* checks */
		*infinite = 1;
	}
	else {
		switch (scanquolim (cp, quotalimit, 1)) {
		case 0:
			break;
		case -1:
			errormessage (EM_INVQUOLIMSYN);
			return (-1);
		case -2:
			errormessage (EM_BADQUOLIMVAL);
			return (-1);
		case -3:
			errormessage (EM_QUOLIMEXP);
			return (-1);
		}
	}
	if ((cp = scan_qname()) == (char *) 0) return (-1);
	strcpy (quename, cp);
	if (scan_end() == -1) return (-1);
	if ((Mgr_priv & QMGR_MGR_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return (-1);		/* Manager privileges required */
	}
	return (0);			/* Scanning successful, and */
}					/* privileges are sufficient */

/*** v_holdreq
 *
 *
 *	void v_holdreq():
 *	HOld Request command.
 */
void v_holdreq()
{
	/*
	 *  Scan the set of request identifiers
	 */
	if (scan_reqset() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	hdl_reqset (0);			/* Hold the requests */
}


/*** v_relreq
 *
 *
 *	void v_relreq():
 *	RELease Request command.
 */
void v_relreq()
{

	/*
	 *  Scan the set of request identifiers.
	 */
	if (scan_reqset() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	hdl_reqset(1);			/* Release the requests */
}


/*** hdl_reqset
 *
 *
 *	void hdl_reqset()
 *
 *	Handle (hold or release) a request set. The set of request-ids to
 *	handle is defined by the scan_reqset() function which is used to
 *	scan/parse a reqid set specification.
 */
static void hdl_reqset (oper)
int oper;				/* Valid operations are: */
					/* 0 - hold requests */
					/* 1 - release requests */
{
	long comcode;			/* Completion code */
	register short exitflag;	/* Exit flag */
	register short mssgflag;	/* BOOLEAN TRUE if a message has */
					/* been delivered to the user */
	Mid_t orig_hostid;		/* Host machine-id of request */
	long seq_num;			/* Sequence number of request */

	/*
	 *  Loop to handle specific requests
	 */
	mssgflag = 0;
	exitflag = 0;
	while (deq_set() != NULL && !exitflag) {
		/*
		 *  Perform the operation
		 */
		seq_num = deq_uid_gid();	/* Get sequence no. */
		if ((orig_hostid = deq_mid()) == (unsigned) -1)
			localmid(&orig_hostid); /* Get host id */
		/*
		 *  The request exists, as far as we can tell ...
		 */
		if ((comcode = hdlreq (oper, (uid_t) 0, seq_num, orig_hostid))
							!= TCML_COMPLETE) {
			show_failed_prefix();
			printf ("Problem holding or releasing request: %ld.%s.\n",
		 		seq_num, getmacnam (orig_hostid));
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;			/* Exit */
		}
	}
	if (!mssgflag) diagnose (TCML_COMPLETE);
}


/*** kill_reqset
 *
 *
 *	void abo_reqset()
 *
 *	Kill (abort or delete) a request set. The set of request-ids
 *	to abort is defined by the scan_reqset() function which is used
 *	to scan/parse a	reqid set specification.
 */
static void kill_reqset (oper)
int oper;				/* Valid operations are: */
					/* 0 - delete requests */
					/* 1 - abort requests */
{
	long comcode;			/* Completion code */
	register short exitflag;	/* Exit flag */
	register short mssgflag;	/* BOOLEAN TRUE if a message has */
					/* been delivered to the user */
	Mid_t orig_hostid;		/* original machine-id of request */
	Mid_t target_mid;		/* Target machine-id of request */
	long seq_num;			/* Sequence number of request */
	int state;			/* Request queue state: RQS_ */

	/*
	 *  Loop to kill specific requests
	 */
	mssgflag = 0;
	exitflag = 0;
	localmid(&target_mid);
	while (deq_set() != NULL && !exitflag) {
		/*
		 *  Perform the operation
		 */
		seq_num = deq_uid_gid();		/* Get sequence no. */
		if ((orig_hostid = deq_mid()) == (unsigned)-1)
			localmid(&orig_hostid);         /* Get host id */
		/*
		 *  The request exists, as far as we can tell ...
		 */
		state = (oper ? RQS_RUNNING : RQS_DEPARTING | RQS_RUNNING |
			RQS_STAGING | RQS_QUEUED | RQS_WAITING | RQS_HOLDING |
			RQS_ARRIVING);
		comcode = delreq ((uid_t) 0, seq_num, orig_hostid, target_mid,
				       SIGINT, state);
		if (comcode != TCML_REQSIGNAL && comcode != TCML_REQDELETE) {
			show_failed_prefix();
			printf ("Problem killing request: %ld.%s.\n",
		 		seq_num, getmacnam (orig_hostid));
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;			/* Exit */
		}
	}
	if (!mssgflag) diagnose (TCML_COMPLETE);
}
/*** v_setgblpiplim
 *
 *
 *	void v_setgblpiplim();
 *	SEt Global Pipe_limit command.
 */
void v_setgblpiplim()
{
	long run_limit;			/* Run limit being set */

	if (scan_equals() == -1) return;
	if (scan_int (&run_limit, 1L, 1000000L,
		EM_INTRUNLIMEXP, EM_RUNLIMOUTOFBOU) == -1) return;
	if (scan_end() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	diagnose (setgblpip ( (int) run_limit));
}

/*** v_aboreq
 *
 *
 *	void v_aboreq():
 *	ABort Request command.
 */
void v_aboreq()
{

	/*
	 *  Scan the set of request identifiers
	 */
	if (scan_reqset() == -1) return;
	if ((Mgr_priv & QMGR_OPER_PRIV) == 0) {
		diagnose (TCML_INSUFFPRV);
		return;			/* Operator privileges required */
	}
	/*
	 *  Abort the requests
	 */
	kill_reqset (1);
}

