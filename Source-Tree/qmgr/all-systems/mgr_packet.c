/*
 * qmgr/mgr_packet.c
 * 
 * DESCRIPTION:
 *
 *	NQS manager program packet sending module.
 *
 *	Each of the functions defined below, returns the transaction
 *	completion codes as listed for each function.  In addition, all
 *	functions can return the transaction codes of:
 *
 *
 *		TCML_INTERNERR:	 if maximum packet size exceeded.
 *		TCML_NOESTABLSH: if unable to establish inter-
 *				 process communication.
 *		TCML_NOLOCALDAE: if the NQS daemon is not running.
 *		TCML_PROTOFAIL:	 if a protocol failure occurred.
 *
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	January 3, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnmap/nmap.h>		/* Mid_t (all OS's) */
#include <libnqs/nqspacket.h>		/* NQS local message packets */
#include <libnqs/transactcc.h>
#include <libsal/license.h>
#include <libsal/proto.h>

/*** aboque
 *
 *
 *	long aboque():
 *
 *	Send to the NQS daemon a packet which will abort all running
 *	requests in the named queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long aboque (char *que_name, int wait_time)
{
	interclear();
	interwstr (que_name);
	interw32i ((long) wait_time);
	return (inter (PKT_ABOQUE));
}


/*** addfor
 *
 *
 *	long addfor():
 *
 *	Send to the NQS daemon a packet which will add a form
 *	to the NQS forms set.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:	 if the form already exists.
 */
long addfor (char *form)
{
	interclear();
	interwstr (form);
	return (inter (PKT_ADDFOR));
}


/*** addnqsman
 *
 *
 *	long addnqsman():
 *
 *	Send to the NQS daemon a packet which will add an account
 *	to the NQS manager access set.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:  if the specified account is already
 *				 an NQS manager.
 */
long addnqsman (
	uid_t mgr_uid,			/* User-id of manager account */
	Mid_t machine_id,		/* at Machine-id of the machine */
					/* for the named account */
	int privilege_bits)		/* Manager privilege bits */
{
	interclear();
	interw32i ((long) mgr_uid);
	interw32u ( machine_id);
	interw32i ((long) privilege_bits);
	return (inter (PKT_ADDNQSMAN));
}

/*** addqcomque
 *
 *
 *      long addqcomque():
 *
 *      Send to the NQS daemon a packet which will add a queue
 *      to the queue set of a queue complex.
 *
 *      Returns:
 *              TCML_COMPLETE:   if successful;
 *              TCML_ALREADEXI:  if the specified queue already
 *                               exists in the queue complex.
 *              TCML_INSUFFMEM:  if there was not sufficient heap
 *                               space available to the NQS daemon
 *                               to create the new destination.
 *              TCML_NOSUCHQUE:  if the named queue does not
 *                               exist.
 *              TCML_NOSUCHCOM:  if the named queue complex does
 *                               not exist.
 *              TCML_WROQUETYP:  if the specified local queue is
 *                               not a batch queue.
 */
long addqcomque (char *que_name, char *qcom_name)
{
        interclear();
        interwstr (que_name);
        interwstr (qcom_name);
        return (inter (PKT_ADDQUECOM));
}



/*** addquedes
 *
 *
 *	long addquedes():
 *
 *	Send to the NQS daemon a packet which will add a queue
 *	to the destination set for a pipe queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:	 if the specified destination for
 *				 the local queue already existed.
 *		TCML_INSUFFMEM:  if there was not sufficient heap
 *				 space available to the NQS daemon
 *				 to create the new destination.
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_SELREFDES:	 if the destination set for the pipe
 *				 queue would have been made self-
 *				 referential by the completion of
 *				 the transaction.
 *		TCML_WROQUETYP:	 if the specified local queue is
 *				 not a pipe queue.
 */
long addquedes (
	char *local_queue,		/* Name of local queue */
	char *dest_queue,		/* Name of destination queue */
	Mid_t dest_mid)			/* Machine-id of destination */
{
	interclear();
	interwstr (local_queue);
	interwstr (dest_queue);
	interw32u (dest_mid);
	return (inter (PKT_ADDQUEDES));
}


/*** addquedev
 *
 *
 *	long addquedev():
 *
 *	Send to the NQS daemon a packet which will add a device
 *	to the device set for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:	 if the mapping already exists.
 *		TCML_INSUFFMEM:	 if there was not sufficient memory
 *				 available to the NQS daemon to
 *				 establish the mapping.
 *		TCML_NOSUCHDEV:	 if the named device does not
 *				 exist.
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_WROQUETYP:	 if the named queue is not a
 *				 device queue.
 */
long addquedev (char *queue_name, char *device_name)
{
	interclear();
	interwstr (queue_name);
	interwstr (device_name);
	return (inter (PKT_ADDQUEDEV));
}

 
/*** addquegid
 *
 *
 *	long addquegid():
 *
 *	Send to the NQS daemon a packet which will add a gid
 *	to the access set for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADACC:	 if the group already has access
 *		TCML_INSUFFMEM:	 if there was not sufficient memory
 *				 available to the NQS daemon to
 *				 establish the access.
 *		TCML_NOSUCHGRP:	 if there is no such group on this
 *				 machine
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_UNRESTR:	 if access to this queue is unrestricted.
 */
long addquegid (char *queue_name, gid_t gid)
{
	interclear();
	interwstr (queue_name);
	interw32i ((long) gid);
	return (inter (PKT_ADDQUEGID));
}

 
/*** addqueuid
 *
 *
 *	long addqueuid():
 *
 *	Send to the NQS daemon a packet which will add a uid
 *	to the access set for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADACC:	 if the user already has access
 *		TCML_INSUFFMEM:	 if there was not sufficient memory
 *				 available to the NQS daemon to
 *				 establish the mapping.
 *		TCML_NOSUCHACC:	 if there is no such account on this
 *				 machine
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_UNRESTR:	 if access to this queue is unrestricted.
 */
long addqueuid (char *queue_name, uid_t uid)
{
	interclear();
	interwstr (queue_name);
	interw32i ((long) uid);
	return (inter (PKT_ADDQUEUID));
}

 
/*** crebatque
 *
 *
 *	long crebatque():
 *
 *	Send to the NQS daemon a packet which will create
 *	a batch queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:	 if the named queue already exists.
 *		TCML_INSUFFMEM:	 if the NQS daemon did not have
 *				 sufficient memory to create the
 *				 new queue.
 */
long crebatque (
	char *que_name,			/* Name of queue to be created */
	int que_priority,		/* Queue priority [0..32767] */
	int que_runlimit,		/* Queue run-limit */
	int que_pipeonly,		/* Boolean pipeonly attr */
	int que_usrlimit)		/* Queue user run-limit */
{
	interclear();
	interwstr (que_name);
	interw32i ((long) que_priority);
	interw32i ((long) que_runlimit);
	interw32i ((long) que_pipeonly);
	interw32i ((long) que_usrlimit);
	return (inter (PKT_CREBATQUE));
}

/*** crecom
 *
 *
 *      long crecom():
 *
 *      Send to the NQS daemon a packet which will create a queue complex.
 *
 *      Returns:
 *              TCML_COMPLETE:   if successful;
 *              TCML_ALREADEXI:  if the named queue complex already exists.
 *              TCML_INSUFFMEM:  if the NQS daemon did not have
 *                               sufficient memory to create the
 *                               new queue.
 */
long crecom (char *qcom_name)
{
        interclear();
        interwstr (qcom_name);
        return (inter (PKT_CRECOM));
}



/*** credev
 *
 *
 *	long credev():
 *
 *	Send to the NQS daemon a packet which will create a device.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:	 if the named device already exists.
 *		TCML_INSUFFMEM:	 if the NQS daemon did not have
 *				 sufficient memory to create the
 *				 new device.
 *		TCML_TOOMANDEV:	 if too many devices would exist,
 *				 if this device were successfully
 *				 created.
 */
long credev (
	char *dev_name,			/* Name of device to be created */
	char *dev_forms,		/* Name of forms to be loaded */
					/* in the device */
	char *dev_fullname,		/* Full pathname of device */
	char *dev_server)		/* Server for device */
{
	interclear();
	interwstr (dev_name);
	interwstr (dev_forms);
	interwstr (dev_fullname);
	interwstr (dev_server);
	return (inter (PKT_CREDEV));
}


/*** credevque
 *
 *
 *	long credevque():
 *
 *	Send to the NQS daemon a packet which will create a device queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:	 if the named queue already exists.
 *		TCML_INSUFFMEM:	 if the NQS daemon did not have
 *				 sufficient memory to create the
 *				 new queue.
 */
long credevque (
	char *que_name,			/* Name of queue to be created */
	int que_priority,		/* Queue priority [0..32767] */
	int que_pipeonly)		/* Boolean pipeonly attr */
{
	interclear();
	interwstr (que_name);
	interw32i ((long) que_priority);
	interw32i ((long) que_pipeonly);
	return (inter (PKT_CREDEVQUE));
}


/*** crepipque
 *
 *
 *	long crepipque():
 *
 *	Send to the NQS daemon a packet which will create a pipe queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:	 if the named queue already exists.
 *		TCML_INSUFFMEM:	 if the NQS daemon did not have
 *				 sufficient memory to create the
 *				 new queue.
 */
long crepipque (
	char *que_name,		/* Name of queue to be created */
	int que_priority,	/* Queue priority [0..32767] */
	int que_runlimit,	/* Queue run-limit */
	int que_pipeonly,	/* Boolean pipeonly attr */
	char *que_server,	/* Server for queue */
	int que_ldb)            /* Bit vector for load balance attributes */
{
	interclear();
	interwstr (que_name);
	interw32i ((long) que_priority);
	interw32i ((long) que_runlimit);
	interw32i ((long) que_pipeonly);
	interw32i ((long) que_ldb);
	interwstr (que_server);
	return (inter (PKT_CREPIPQUE));
} 


/*** delcom
 *
 *
 *      long delcom():
 *
 *      Send to the NQS daemon a packet which will delete a complex.
 *
 *      Returns:
 *              TCML_COMPLETE:   if successful;
 *              TCML_NOSUCHCOM   if no such complex exists;
 */
long delcom (char *qcom_name)
{
        interclear();
        interwstr (qcom_name);
        return (inter (PKT_DELCOM));
}


/*** deldev
 *
 *
 *	long deldev():
 *
 *	Send to the NQS daemon a packet which will delete a device.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_DEVACTIVE:	 if the named device is active.
 *		TCML_DEVENABLE:	 if the named device is enabled.
 *		TCML_NOSUCHDEV:	 if the named device does not
 *				 exist.
 */
long deldev (char *dev_name)
{
	interclear();
	interwstr (dev_name);
	return (inter (PKT_DELDEV));
}


/*** delfor
 *
 *
 *	long delfor():
 *
 *	Send to the NQS daemon a packet which will delete a form
 *	from the NQS forms set.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHFORM: if the named form does not
 *				 exist.
 */
long delfor (char *form)
{
	interclear();
	interwstr (form);
	return (inter (PKT_DELFOR));
}


/*** delnqsman
 *
 *
 *	long delnqsman():
 *
 *	Send to the NQS daemon a packet which will delete an account
 *	from the NQS manager access set.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHMAN:	 if the named manager account did
 *				 not exist.
 *		TCML_ROOTINDEL:	 if an attempt was made to delete
 *				 the system "root" account from the
 *				 NQS manager set.
 */
long delnqsman (
	uid_t mgr_uid,			/* User-id of manager */
	Mid_t machine_id,		/* at this machine */
	int privilege_bits)		/* Manager privilege bits */
{
	interclear();
	interw32i ((long) mgr_uid);
	interw32u (machine_id);
	interw32i ((long) privilege_bits);
	return (inter (PKT_DELNQSMAN));
}


/*** delque
 *
 *
 *	long delque():
 *
 *	Send to the NQS daemon a packet which will delete a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_QUEENABLE:	 if the specified queue is enabled.
 *		TCML_QUEHASREQ:	 if the specified queue has reqs
 *				 in it.
 */
long delque (char *que_name)
{
	interclear();
	interwstr (que_name);
	return (inter (PKT_DELQUE));
}


/*** delquedes
 *
 *
 *	long delquedes():
 *
 *	Send to the NQS daemon a packet which will delete a
 *	destination from destination set for the specified pipe queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHDES:   if the specified destination does
 *				 not exist for the local queue.
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_WROQUETYP:	 if the type of the local queue is
 *				 not a pipe queue.
 */
long delquedes (
	char *local_queue,		/* Name of local queue */
	char *dest_queue,		/* Name of destination queue */
	Mid_t dest_mid)			/* Destination machine-id */
{
	interclear();
	interwstr (local_queue);
	interwstr (dest_queue);
	interw32u ((long) dest_mid);
	return (inter (PKT_DELQUEDES));
}


/*** delquedev
 *
 *
 *	long delquedev():
 *
 *	Send to the NQS daemon a packet which will delete a device
 *	from the device set for the specified queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHMAP:	 if the specified queue-to-device
 *				 mapping does not exist.
 *		TCML_NOSUCHQUE:	 if the specified queue does not
 *				 exist.
 *		TCML_WROQUETYP:	 if the specified queue is not a
 *				 device queue.
 */
long delquedev (char *queue_name, char *device_name)
{
	interclear();
	interwstr (queue_name);
	interwstr (device_name);
	return (inter (PKT_DELQUEDEV));
}


/*** delquegid
 *
 *
 *	long delquegid():
 *
 *	Send to the NQS daemon a packet which will delete a gid
 *	from the access set for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOACCNOW:	 if the group does not have access now
 *		TCML_NOSUCHGRP:	 if there is no such group on this
 *				 machine
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_UNRESTR:	 if access to this queue is unrestricted.
 */
long delquegid (char *queue_name, gid_t gid)
{
	interclear();
	interwstr (queue_name);
	interw32i ((long) gid);
	return (inter (PKT_DELQUEGID));
}

 
/*** delqueuid
 *
 *
 *	long delqueuid():
 *
 *	Send to the NQS daemon a packet which will delete a uid
 *	from the access set for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOACCNOW:	 if the user does not have access now
 *		TCML_NOSUCHACC:	 if there is no such account on this
 *				 machine
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_UNRESTR:	 if access to this queue is unrestricted.
 */
long delqueuid (char *queue_name, uid_t uid)
{
	interclear();
	interwstr (queue_name);
	interw32i ((long) uid);
	return (inter (PKT_DELQUEUID));
}

 
/*** disdev
 *
 *
 *	long disdev():
 *
 *	Send to the NQS daemon a packet which will disable a device.,
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHDEV:	 if the named device does not
 *				 exist.
 */
long disdev (char *device_name)
{
	interclear();
	interwstr (device_name);
	return (inter (PKT_DISDEV));
}


/*** disque
 *
 *
 *	long disque():
 *
 *	Send to the NQS daemon a packet which will disable a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long disque (char *queue_name)
{
	interclear();
	interwstr (queue_name);
	return (inter (PKT_DISQUE));
}


/*** enadev
 *
 *
 *	long enadev():
 *
 *	Send to the NQS daemon a packet which will enable a device.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHDEV:	 if the named device does not
 *				 exist.
 */
long enadev (char *device_name)
{
	interclear();
	interwstr (device_name);
	return (inter (PKT_ENADEV));
}


/*** enaque
 *
 *
 *	long enaque():
 *
 *	Send to the NQS daemon a packet which will enable a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long enaque (char *queue_name)
{
	interclear();
	interwstr (queue_name);
	return (inter (PKT_ENAQUE));
}


/*** locdae
 *
 *
 *	long locdae():
 *
 *	Send to the NQS daemon a packet which will lock
 *	the NQS daemon in memory making the daemon immune
 *	to swapping activity.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_PLOCKFAIL:	 if the daemon call to plock()
 *				 failed.
 */
long locdae()
{
	interclear();
	return (inter (PKT_LOCDAE));
}


/*** modreq
 *
 *
 *      long modreq():
 *
 *      Send to the NQS daemon a packet which will modify
 *      the parameters of a request.
 *
 *      Returns:
 *              TCML_COMPLETE:  if successful;
 *              TCML_NOSUCHREQ: if request does not exist;
 *              TCML_NOSUCHQUO: if this limit is meaningless
 *                              for this operating system;
 *              TCML_FIXBYNQS:  if successful, but NQS had to set the
 *                              limit to a value other than requested;
 */
long modreq (
	uid_t orig_uid,         /* Originator's user-id */
	int req_seqno,          /* Request sequence number */
	Mid_t req_mid,          /* Request machine id */
	int limit_type,         /* Limit type */
	long param_value1,      /* First part of parameter value */
	long param_value2,      /* Second part of parameter value */
	short param_infinite)   /* Infinite paramater value indicator */
{
        interclear();
        interw32i ((long) orig_uid);
        interw32i ((long) req_seqno);
        interw32u (req_mid);
        interw32i ((long) limit_type);
        interw32i (param_value1);
        interw32i (param_value2);
        interw32i ((long) param_infinite);
        return (inter (PKT_MODREQ));
}

/*** movque
 *
 *
 *      long movque():
 *
 *      Send to the NQS daemon a packet which will move a queue.
 *
 *      Returns:
 *              TCML_COMPLETE:   if successful;
 *              TCML_NOSUCHQUE:  if a named queue does not
 *                               exist.
 */
long movque (char *src_qname, char *des_qname)
{
        interclear();
        interwstr (src_qname);
        interwstr (des_qname);
        return (inter (PKT_MOVQUE));
}


/*** nqsshutdn
 *
 *
 *	long nqsshutdn():
 *
 *	Send to the NQS daemon a packet which will shutdown the NQS daemon.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_SHUTDNERR:  if unsuccessful;
 */
long nqsshutdn (int wait_time)
{
	interclear();
	interw32i ((long) wait_time);
	return (inter (PKT_SHUTDOWN));
}


/*** purque
 *
 *
 *	long purque():
 *
 *	Send to the NQS daemon a packet which will purge a specified queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long purque (char *que_name)
{
	interclear();
	interwstr (que_name);
	return (inter (PKT_PURQUE));
}

/*** remqcomque
 *
 *
 *      long remqcomque():
 *
 *      Send to the NQS daemon a packet which will remove a queue
 *      from the queue set of a queue complex.
 *
 *      Returns:
 *              TCML_COMPLETE:   if successful;
 *              TCML_NOSUCHCOM:  if the named queue complex does
 *                               not exist.
 *              TCML_NOSUCHQUE:  if the named queue does not
 *                               exist in the queue complex.
 */
long remqcomque (char *que_name, char *qcom_name)
{
        interclear();
        interwstr (que_name);
        interwstr (qcom_name);
        return (inter (PKT_REMQUECOM));
}


/*** setbatpri
 *
 *
 *	long setbatpri():
 *
 *	Send to the NQS daemon a packet which will set a default
 *	intra-queue batch request priority.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setbatpri (int newpri)
{
	interclear();
	interw32i ((long) newpri);
	return (inter (PKT_SETDEFBATPR));
}


/*** setbatque
 *
 *
 *	long setbatque():
 *
 *	Send to the NQS daemon a packet which will set
 *	a default batch queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 */
long setbatque (char *que_name)
{
	interclear();
	interwstr (que_name);
	return (inter (PKT_SETDEFBATQU));
}

/*** setcomrun
 *
 *
 *      long setcomrun():
 *
 *      Send to the NQS daemon a packet which will set a queue complex
 *      run-limit.
 *
 *      Returns:
 *              TCML_COMPLETE:   if successful;
 *              TCML_NOSUCHCOM:  if the named complex does not
 *                               exist.
 */
long setcomrun (char *qcom_name, int run_limit)
{
        interclear();
        interwstr (qcom_name);
        interw32i ((long) run_limit);
        return (inter (PKT_SETCOMLIM));
}
/*** setcomuser
 *
 *
 *      long setcomuser():
 *
 *      Send to the NQS daemon a packet which will set a queue complex
 *      user-limit.
 *
 *      Returns:
 *              TCML_COMPLETE:   if successful;
 *              TCML_NOSUCHCOM:  if the named complex does not
 *                               exist.
 */
long setcomuser (char *qcom_name, int user_limit)
{
        interclear();
        interwstr (qcom_name);
        interw32i ((long) user_limit);
        return (inter (PKT_SETCOMUSERLIM));
}

/*** setdeblev
 *
 *
 *	long setdeblev():
 *
 *	Send to the NQS daemon a packet which will set the debug level.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setdeblev (int debug_level)
{
  	long ulResult;
  
	interclear();
	interw32i ((long) debug_level);
	ulResult = inter (PKT_SETDEB);
  
  	if (ulResult == TCML_COMPLETE)
    		sal_debug_SetLevel(debug_level);
  
  	return ulResult;
}

/*** setgbatlim
 *
 *
 *	long setgbatlim():
 *
 *	Send to the NQS daemon a packet which will set the global batch limit.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setgbatlim (int limit)
{
	interclear();
	interw32i ((long) limit);
	return (inter (PKT_SETGBATLIM));
}

/*** setgbpip
 *
 *
 *
 *	long setgblpip():
 *
 *	Send to the NQS daemon a packet which will set the global
 *	pipe limit.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful;
 */

long setgblpip (int pip_limit)
{
	interclear();
	interw32i ((long) pip_limit);
	return (inter (PKT_SETPIPLIM));
}



/*** setdestim
 *
 *
 *	long setdestim():
 *
 *	Send to the NQS daemon a packet which will set the default
 *	number of seconds that can elapse during which time a pipe
 *	queue destination can be unreachable, before being marked
 *	as completely failed.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setdestim (long retrydelta)
{
	interclear();
	interw32i (retrydelta);
	return (inter (PKT_SETDEFDESTI));
}


/*** setdeswai
 *
 *
 *	long setdeswai():
 *
 *	Send to the NQS daemon a packet which will set
 *	the default destination wait time in seconds between
 *	failed destination connection attempts.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setdeswai (long destwait)
{			
	interclear();
	interw32i (destwait);
	return (inter (PKT_SETDEFDESWA));
}


/*** setdevfor
 *
 *
 *	long setdevfor():
 *
 *	Send to the NQS daemon a packet which will set
 *	the device forms for a device.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHDEV:	 if the named device does not
 *				 exist.
 *		TCML_NOSUCHFORM: if the specified form is not
 *				 defined in the local NQS forms
 *				 set.
 */
long setdevfor (char *dev_name, char *forms_name)
{
	interclear();
	interwstr (dev_name);
	interwstr (forms_name);
	return (inter (PKT_SETDEVFOR));
}


/*** setdevpri
 *
 *
 *	long setdevpri():
 *
 *	Send to the NQS daemon a packet which will set
 *	the default intra-queue device request priority.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setdevpri (int newpri)
{
	interclear();
	interw32i ((long) newpri);
	return (inter (PKT_SETDEFDEVPR));
}


/*** setdevser
 *
 *
 *	long setdevser():
 *
 *	Send to the NQS daemon a packet which will set
 *	the device server for a device.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHDEV:	 if the named device does not
 *				 exist.
 */
long setdevser (char *dev_name, char *server_name)
{
	interclear();
	interwstr (dev_name);
	interwstr (server_name);
	return (inter (PKT_SETDEVSER));
}


/*** setfor
 *
 *
 *	long setfor():
 *
 *	Send to the NQS daemon a packet which will set
 *	the NQS forms set to the single specified form.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setfor (char *form)
{
	interclear();
	interwstr (form);
	return (inter (PKT_SETFOR));
}


/*** setlife
 *
 *
 *	long setlife():
 *
 *	Send to the NQS daemon a packet which will set
 *	the pipe queue request lifetime.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setlife (long lifetime)
{
	interclear();
	interw32i (lifetime);
	return (inter (PKT_SETLIFE));
}


/*** setlogfil
 *
 *
 *	long setlogfil():
 *
 *	Send to the NQS daemon a packet which will instruct
 *	the NQS daemon to use a different file in the recording
 *	of errors and messages.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_LOGFILERR:	 if the NQS daemon was unable to
 *				 open or create the specified
 *				 logfile.
 */
long setlogfil (char *logfile_name)
{
	interclear();
	interwstr (logfile_name);
	return (inter (PKT_SETLOGFIL));
}


/*** setmaxcop
 *
 *
 *	long setmaxcop():
 *
 *	Send to the NQS daemon a packet which will set a maximum
 *	on the number of print copies.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setmaxcop (long max_copies)
{
	interclear();
	interw32i (max_copies);
	return (inter (PKT_SETMAXCOP));
}


/*** setmaxopr
 *
 *
 *	long setmaxopr():
 *
 *	Send to the NQS daemon a packet which will set a maximum
 *	failed device open retry count.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setmaxopr (long maxretries)
{
	interclear();
	interw32i (maxretries);
	return (inter (PKT_SETMAXOPERE));
}


/*** setmaxpsz
 *
 *
 *	long setmaxpsz():
 *
 *	Send to the NQS daemon a packet which will set a maximum
 *	on print file size.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setmaxpsz (long maxprintsize)
{
	interclear();
	interw32i (maxprintsize);
	return (inter (PKT_SETMAXPRISI));
}


/*** setndfbat
 *
 *
 *	long setndfbat():
 *
 *	Send to the NQS daemon a packet which will cause there to be
 *	no default batch queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setndfbat ()
{
	interclear();
	return (inter (PKT_SETNDFBATQU));
}


/*** setndffor
 *
 *
 *	long setndffor():
 *
 *	Send to the NQS daemon a packet which will cause there to be
 *	no default print form.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setndffor ()
{
	interclear();
	return (inter (PKT_SETNDFPRIFO));
}


/*** setndfpri
 *
 *
 *	long setndfpri():
 *
 *	Send to the NQS daemon a packet which will cause there to be
 *	no default print queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setndfpri ()
{
	interclear();
	return (inter (PKT_SETNDFPRIQU));
}


/*** setnetcli
 *
 *
 *	long setnetcli():
 *
 *	Send to the NQS local daemon a packet which will set
 *	the NQS network client.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setnetcli (char *network_client_name)
{
	interclear();
	interwstr (network_client_name);
	return (inter (PKT_SETNETCLI));
}


/*** setnetdae
 *
 *
 *	long setnetdae():
 *
 *	Send to the NQS local daemon a packet which will set
 *	the NQS network daemon.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setnetdae (char *network_daemon_name)
{
	interclear();
	interwstr (network_daemon_name);
	return (inter (PKT_SETNETDAE));
}

/*** setloaddae
 *
 *
 *	long setloaddae():
 *
 *	Send to the NQS local daemon a packet which will set
 *	the NQS load daemon.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setloaddae (char *load_daemon_name)
{
	interclear();
	interwstr (load_daemon_name);
	return (inter (PKT_SETLOADDAE));
}

/*** control_daemon
 *
 *
 *	long control_daemon():
 *
 *	Send to the NQS local daemon a packet which will stop
 *	or start one of the daemons.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long control_daemon (long action, long daemon)
{
	interclear();
	interw32i (action);
	interw32i (daemon);
	return (inter (PKT_CTLDAE));
}

/*** setnetser
 *
 *
 *	long setnetser():
 *
 *	Send to the NQS local daemon a packet which will set
 *	the NQS network server.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setnetser (char *network_server_name)
{
	interclear();
	interwstr (network_server_name);
	return (inter (PKT_SETNETSER));
}


/*** setnnedae
 *
 *
 *	long setnnedae():
 *
 *	Send to the NQS daemon a packet which will cause there to be
 *	no network daemon.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setnnedae ()
{
	interclear();
	return (inter (PKT_SETNONETDAE));
}

/*** setnoqueacc
 *
 *
 *	long setnoqueacc ():
 *
 *	Send to the NQS daemon a packet which will set the
 *	access set for a queue to none.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long setnoqueacc (char *queue_name)
{
	interclear();
	interwstr (queue_name);
	return (inter (PKT_SETNOQUEACC));
}

/*** setnoquechar
 *
 *
 *	long setnoquechar ():
 *
 *	Send to the NQS daemon a packet which will clear the
 *	appropriate bits for a queue
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long setnoquechar (char *queue_name, int item)
{
	interclear();
	interwstr (queue_name);
	interw32i(item);
	return (inter (PKT_SETNOQUECHAR));
}

/*** setquechar
 *
 *
 *	long setnoqueacc ():
 *
 *	Send to the NQS daemon a packet which will set the
 *	appropriate status bits for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long setquechar (char *queue_name, int item)
{
	interclear();
	interwstr (queue_name);
	interw32i (item);
	return (inter (PKT_SETQUECHAR));
}

/*** setnqsmai
 *
 *
 *	long setnqsmai():
 *
 *	Send to the NQS daemon a packet which will set
 *	the NQS mail account to the specified account user-id.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHACC:	 if the specified account does
 *				 not exist.
 */
long setnqsmai (uid_t nqs_mail_account)
{
	interclear();
	interw32i ((long) nqs_mail_account);
	return (inter (PKT_SETNQSMAI));
}


/*** setnqsman
 *
 *
 *	long setnqsman():
 *
 *	Send to the NQS daemon a packet which will set the
 *	NQS manager access set to the single account
 *	of root, with all privileges.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setnqsman ()
{
	interclear();
	return (inter (PKT_SETNQSMAN));
}


/*** setopewai
 *
 *
 *	long setopewai():
 *
 *	Send to the NQS daemon a packet which will set the open
 *	wait time in seconds between failed device open attempts.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setopewai (long openwait)
{
	interclear();
	interw32i (openwait);
	return (inter (PKT_SETOPEWAI));
}


/*** setpipcli
 *
 *
 *	long setpipcli():
 *
 *	Send to the NQS daemon a packet which will set
 *	the pipe client for a pipe queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_WROQUETYP:	 if the named queue is not a pipe queue.
 */
long setpipcli (char *que_name, char *client_name)
{
	interclear();
	interwstr (que_name);
	interwstr (client_name);
	return (inter (PKT_SETPIPCLI));
}


/*** setpiponl
 *
 *
 *	long setpiponl():
 *
 *	Send to the NQS daemon a packet which will set
 *	the pipeonly entry attribute for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long setpiponl (char *queue_name)
{
	interclear();
	interwstr (queue_name);
	return (inter (PKT_SETPIPONL));
}


/*** setppcore
 *
 *
 *	long setppcore():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per process core file size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setppcore (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPPCORE));
}


/*** setppcput
 *
 *
 *	long setppcput():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per process cpu time limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setppcput (
	char *queue_name,		/* Name of the queue */
	unsigned long new_seconds,	/* Integer part */
	short new_ms,			/* Milliseconds */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_seconds);
	interw32i ((long) new_ms);
	interw32i ((long) infinite);
	return (inter (PKT_SETPPCPUT));
}


/*** setppdata
 *
 *
 *	long setppdata():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per process data segment size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setppdata (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPPDATA));
}


/*** setppmem
 *
 *
 *	long setppmem():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per process memory size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setppmem (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPPMEM));
}


/*** setndp
 *
 *
 *      long setndp():
 *
 *      Send to the NQS daemon a packet which will set
 *      the non-degrading priority value for a queue.
 *
 *      Returns:
 *              TCML_COMPLETE:   if successful;
 *              TCML_FIXBYNQS:   if sucessful, but NQS had to set the
 *                               limit to a value other than that
 *                               requested;
 *              TCML_GRANFATHER: if successful, but a queued request
 *                               will be given a grandfather clause.
 *              TCML_NOSUCHQUE:  if the named queue does not exist.
 *              TCML_NOSUCHQUO:  if this limit is meaningless
 *                               for this operating system;
 *              TCML_WROQUETYP:  if this limit cannot be set for
 *                               this queue type.
 */
long setndp (char *queue_name, int new_ndp)
{
        interclear();
        interwstr (queue_name);
        interw32i ((long) new_ndp);
        return (inter (PKT_SETQUENDP));
}

/*** setppnice
 *
 *
 *	long setppnice():
 *
 *	Send to the NQS daemon a packet which will set
 *	the nice value for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system;
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setppnice (char *queue_name, int new_nice)
{
	interclear();
	interwstr (queue_name);
	interw32i ((long) new_nice);
	return (inter (PKT_SETPPNICE));
}


/*** setpppfile
 *
 *
 *	long setpppfile():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per process permanent file size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setpppfile (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPPPFILE));
}


/*** setppstack
 *
 *
 *	long setppstack():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per process stack size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setppstack (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPPSTACK));
}


/*** setpptfile
 *
 *
 *	long setpptfile():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per process temporary file size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setpptfile (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPPTFILE));
}


/*** setppwork
 *
 *
 *	long setppwork():
 *
 *	Send to the NQS daemon a packet which will set
 *	the working set size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setppwork (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPPWORK));
}


/*** setprcput
 *
 *
 *	long setprcput():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per request cpu time limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setprcput (
	char *queue_name,		/* Name of the queue */
	unsigned long new_seconds,	/* Integer part */
	short new_ms,			/* Milliseconds */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_seconds);
	interw32i ((long) new_ms);
	interw32i ((long) infinite);
	return (inter (PKT_SETPRCPUT));
}


/*** setprifor
 *
 *
 *	long setprifor():
 *
 *	Send to the NQS daemon a packet which will set
 *	the default print forms.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHFORM: if the specified form is not
 *				 defined in the forms database.
 */
long setprifor (char *formsname)
{
	interclear();
	interwstr (formsname);
	return (inter (PKT_SETDEFPRIFO));
}


/*** setprique
 *
 *
 *	long setprique():
 *
 *	Send to the NQS daemon a packet which will set
 *	the default print queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 */
long setprique (char *que_name)
{
	interclear();
	interwstr (que_name);
	return (inter (PKT_SETDEFPRIQU));
}


/*** setprmem
 *
 *
 *	long setprmem():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per request memory size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setprmem (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPRMEM));
}


/*** setprpfile
 *
 *
 *	long setprpfile():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per request permanent file size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setprpfile (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPRPFILE));
}


/*** setprtfile
 *
 *
 *	long setprtfile():
 *
 *	Send to the NQS daemon a packet which will set
 *	the per request temporary file size limit for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_FIXBYNQS:	 if sucessful, but NQS had to set the
 *				 limit to a value other than that
 *				 requested;
 *		TCML_GRANFATHER: if successful, but a queued request
 *				 will be given a grandfather clause.
 *		TCML_NOSUCHQUE:	 if the named queue does not exist.
 *		TCML_NOSUCHQUO:	 if this limit is meaningless
 *				 for this operating system.
 *		TCML_WROQUETYP:	 if this limit cannot be set for
 *				 this queue type.
 */
long setprtfile (
	char *queue_name,
	unsigned long new_quota,
	short new_units,		/* QLM_??? */
	short infinite)			/* Non-zero if infinite */
{
	interclear();
	interwstr (queue_name);
	interw32u (new_quota);
	interw32i ((long) new_units);
	interw32i ((long) infinite);
	return (inter (PKT_SETPRTFILE));
}


/*** setquedes
 *
 *
 *	long setquedes():
 *
 *	Send to the NQS daemon a packet which will set
 *	the queue to destination set for a pipe queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:	 if the specified destination for
 *				 the local queue already existed.
 *		TCML_INSUFFMEM:  if there was not sufficient heap
 *				 space available to the NQS daemon
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_SELREFDES:	 if the destination set for the pipe
 *				 queue would have been made self-
 *				 referential by the completion of
 *				 the transaction.
 *		TCML_WROQUETYP:	 if the specified local queue is
 *				 not a pipe queue.
 */
long setquedes (
	char *local_queue,	/* Name of local queue */
	char *dest_queue,	/* Name of destination queue */
	Mid_t dest_mid)		/* Machine-id of destination */
{
	interclear();
	interwstr (local_queue);
	interwstr (dest_queue);
	interw32u (dest_mid);
	return (inter (PKT_SETQUEDES));
}


/*** setquedev
 *
 *
 *	long setquedev():
 *
 *	Send to the NQS daemon a packet which will set
 *	the device set for a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_ALREADEXI:	 if the mapping already exists.
 *		TCML_INSUFFMEM:	 if there was not sufficient memory
 *				 available to the NQS daemon to
 *				 establish the mapping.
 *		TCML_NOSUCHDEV:	 if the named device does not
 *				 exist.
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_WROQUETYP:	 if the named queue is not a
 *				 device queue.
 */
long setquedev (char *queue_name, char *device_name)
{
	interclear();
	interwstr (queue_name);
	interwstr (device_name);
	return (inter (PKT_SETQUEDEV));
}

 
/*** setquepri
 *
 *
 *	long setquepri():
 *
 *	Send to the NQS daemon a packet which will set a queue priority.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long setquepri (char *que_name, int new_priority)
{
	interclear();
	interwstr (que_name);
	interw32i ((long) new_priority);
	return (inter (PKT_SETQUEPRI));
}


/*** setqueusr
 *
 *
 *	long setqueusr():
 *
 *	Send to the NQS daemon a packet which will set a queue usr-limit.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_WROQUETYP:	 if the named queue is a device-
 *				 queue.
 */
long setqueusr (char *que_name, int usr_limit)
{
	interclear();
	interwstr (que_name);
	interw32i ((long) usr_limit);
	return (inter (PKT_SETQUEUSR));
}

/*** setquerun
 *
 *
 *	long setquerun():
 *
 *	Send to the NQS daemon a packet which will set a queue run-limit.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 *		TCML_WROQUETYP:	 if the named queue is a device-
 *				 queue.
 */
long setquerun (char *que_name, int run_limit)
{
	interclear();
	interwstr (que_name);
	interw32i ((long) run_limit);
	return (inter (PKT_SETQUERUN));
}

/*** setnqssched
 *
 *
 *	long setnqssched():
 *
 *	Send to the NQS daemon a packet which will set the scheduler.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setnqssched (Mid_t scheduler)
{
	interclear();
	interw32u (scheduler);
	return ( inter (PKT_SETNQSSCHED) );
}


/*** setservavail
 *
 *
 *	long setservavail():
 *
 *	Send to the NQS daemon a packet which will set
 *	a server available.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setservavail (Mid_t server)
{
	interclear();
	interw32u (server);
	return (inter (PKT_SETSERVAVAIL));
}

/*** setservperf
 *
 *
 *	long setservperf():
 *
 *	Send to the NQS daemon a packet which will set
 *	a server relative performance.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setservperf (Mid_t server, int performance)
{
	interclear();
	interw32u (server);
	interw32i ((long) performance);
	return (inter (PKT_SETSERVPERF));
}

/*** setshsfix
 *
 *
 *	long setshsfix():
 *
 *	Send to the NQS daemon a packet which will set
 *	a fixed shell strategy.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setshsfix (char *shell_name)
{
	interclear();
	interwstr (shell_name);
	return (inter (PKT_SETSHSFIX));
}


/*** setshsfre
 *
 *
 *	long setshsfre():
 *
 *	Send to the NQS daemon a packet which will set
 *	a free shell strategy.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setshsfre()
{
	interclear();
	return (inter (PKT_SETSHSFRE));
}


/*** setshslog
 *
 *
 *	long setshslog():
 *
 *	Send to the NQS daemon a packet which will set
 *	a login shell strategy.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setshslog()
{
	interclear();
	return (inter (PKT_SETSHSLOG));
}


/*** setunrqueacc
 *
 *
 *	long setunrqueacc():
 *
 *	Send to the NQS daemon a packet which will set the
 *	access set for a queue to unrestricted.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long setunrqueacc (char *queue_name)
{
	interclear();
	interwstr (queue_name);
	return (inter (PKT_SETUNRQUEAC));
}

 
/*** staque
 *
 *
 *	long staque():
 *
 *	Send to the NQS daemon a packet which will start a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long staque (char *que_name)
{
	interclear();
	interwstr (que_name);
	return (inter (PKT_STAQUE));
}


/*** stoque
 *
 *
 *	long stoque():
 *
 *	Send to the NQS daemon a packet which will stop a queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHQUE:	 if the named queue does not
 *				 exist.
 */
long stoque (char *que_name)
{
	interclear();
	interwstr (que_name);
	return (inter (PKT_STOQUE));
}

/*** setdefloadint
 *
 *
 *	long setdefloadint():
 * 
 *	Send to the NQS daemon a packet which will set the load interval
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 */
long setdefloadint(int interval)
{
	interclear();
	interw32i (interval);
	return (inter (PKT_SETDEFLOADINT));
}


/*** unldae
 *
 *
 *	long unldae():
 *
 *	Send to the NQS daemon a packet which will unlock
 *	the NQS daemon from memory making it subject to swapping.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_PLOCKFAIL:	 if the daemon call to plock()
 *				 failed.
 */
long unldae ()
{
	interclear();
	return (inter (PKT_UNLDAE));
}
