/*
 * qmgr/mgr_adsset.c
 * 
 * DESCRIPTION:
 *
 *	NQS manager "add set", "delete set", and "set set" execution module.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 15, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsmgr.h>		/* Token types and error messages */
#include <libnqs/transactcc.h>		/* Transaction completion codes */

static void adsdes_problem ( int opcode, char *quename, char *destname );
static void adsdev_problem ( int opcode, char *quename, char *devname );
static void adsfor_problem ( int opcode, char *formname );
static void adsgid_problem ( int opcode, char *quename, char *group_name );
static void adsmgr_problem ( int opcode, char *mgr_name );
static void adsque_problem ( int opcode, char *que_name, char *qcom_name );
static void adsuid_problem ( int opcode, char *quename, char *user_name );

/*** adsdes_set
 *
 *
 *	void adsdes_set():
 *
 *	Add/delete/set queue destinations set procedure.  The set of
 *	destinations to add/delete/set is defined by the scan_destset()
 *	function which is used to parse a destination set specification.
 */
void adsdes_set (char *quename, int opcode)
{					/* DEL_OP, or SET_OP. */

	long comcode;			/* Completion code */
	register char *destq;		/* Destination queue name */
	register Mid_t destmid;		/* Destination machine-id */
	register short exitflag;	/* Exit flag */
	register short opcode2;		/* Internal opcode */
	short mssgflag;			/* BOOLEAN TRUE if a message has */
					/* been delivered to the user */

	/*
	 *  Loop to add/delete/set queue destinations.
	 */
	mssgflag = 0;
	exitflag = 0;
	opcode2 = opcode;
	while ((destq = deq_set()) != NULL && !exitflag) {
		/*
		 *  Perform the operation for pair.
		 */
		destmid = deq_mid();	/* Get destination machine-id */
		switch (opcode2) {
		case ADD_OP:
			comcode = addquedes (quename, destqueue (destq),
					     destmid);
			break;
		case DEL_OP:
			comcode = delquedes (quename, destqueue (destq),
					     destmid);
			break;
		case SET_OP:
			comcode = setquedes (quename, destqueue (destq),
					     destmid);
			opcode2 = ADD_OP;
			break;
		}
		switch (comcode) {
		case TCML_INTERNERR:
		case TCML_NOESTABLSH:
		case TCML_NOLOCALDAE:
		case TCML_PROTOFAIL:
			adsdes_problem (opcode, quename, destq);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		case TCML_COMPLETE:
			break;
		case TCML_ALREADEXI:
			adsdes_problem (opcode, quename, destq);
			diagnose (comcode);
			mssgflag = 1;
			break;
		case TCML_INSUFFMEM:
		case TCML_MAXQDESTS:
			adsdes_problem (opcode, quename, destq);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		case TCML_NOSUCHDES:
			adsdes_problem (opcode, quename, destq);
			diagnose (comcode);
			mssgflag = 1;
			break;
		case TCML_NOSUCHQUE:
			adsdes_problem (opcode, quename, destq);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		case TCML_SELREFDES:
			adsdes_problem (opcode, quename, destq);
			diagnose (comcode);
			mssgflag = 1;
			break;
		case TCML_WROQUETYP:
			adsdes_problem (opcode, quename, destq);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		default:
			adsdes_problem (opcode, quename, destq);
			diagnose (TCML_UNDEFINED);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
		}
	}
	if (!mssgflag) diagnose (TCML_COMPLETE);
}


/*** adsdes_problem
 *
 *
 *	void adsdes_problem():
 *	Indicate problem adding/deleting/setting queue destination.
 */
static void adsdes_problem (int opcode, char * quename, char * destname)
{
	show_failed_prefix();
	switch (opcode) {
	case ADD_OP:
		printf ("Problem adding queue destination:\n");
		show_failed_prefix();
		printf ("%s -> %s.\n", quename, destname);
		break;
	case DEL_OP:
		printf ("Problem deleting queue destination:\n");
		show_failed_prefix();
		printf ("%s -> %s.\n", quename, destname);
		break;
	case SET_OP:
		printf ("Problem setting queue destination:\n");
		show_failed_prefix();
		printf ("%s -> %s.\n", quename, destname);
		break;
	}
}


/*** adsdev_set
 *
 *
 *	void adsdev_set():
 *
 *	Add/delete/set queue to device mappings procedure.  The set
 *	of devices to add/delete/set is defined by the scan_devset()
 *	function which is used to parse a device set specification.
 */
void adsdev_set (char *quename, int opcode)
{				

	long comcode;			/* Completion code */
	register char *devname;		/* Device name */
	register short exitflag;	/* Exit flag */
	register int opcode2;		/* Internal opcode */
	short mssgflag;			/* BOOLEAN TRUE if a message has */
					/* been delivered to the user */

	/*
	 *  Loop to add/delete/set queue to device mappings.
	 */
	mssgflag = 0;
	exitflag = 0;
	opcode2 = opcode;
	while ((devname = deq_set()) != NULL && !exitflag) {
		/*
		 *  Perform the operation.
		 */
		switch (opcode2) {
		case ADD_OP:
			comcode = addquedev (quename, devname);
			break;
		case DEL_OP:
			comcode = delquedev (quename, devname);
			break;
		case SET_OP:
			comcode = setquedev (quename, devname);
			opcode2 = ADD_OP;
			break;
		}
		switch (comcode) {
		case TCML_INTERNERR:
		case TCML_NOESTABLSH:
		case TCML_NOLOCALDAE:
		case TCML_PROTOFAIL:
			adsdev_problem (opcode, quename, devname);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		case TCML_COMPLETE:
			break;
		case TCML_ALREADEXI:
			adsdev_problem (opcode, quename, devname);
			diagnose (comcode);
			mssgflag = 1;
			break;
		case TCML_INSUFFMEM:
			adsdev_problem (opcode, quename, devname);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		case TCML_NOSUCHDEV:
		case TCML_NOSUCHMAP:
			adsdev_problem (opcode, quename, devname);
			diagnose (comcode);
			mssgflag = 1;
			break;
		case TCML_NOSUCHQUE:
		case TCML_WROQUETYP:
			adsdev_problem (opcode, quename, devname);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		default:
			adsdev_problem (opcode, quename, devname);
			diagnose (TCML_UNDEFINED);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
		}
	}
	if (!mssgflag) diagnose (TCML_COMPLETE);
}


/*** adsdev_problem
 *
 *
 *	void adsdev_problem():
 *	Indicate problem adding/deleting/setting queue/device mapping.
 */
static void adsdev_problem (int opcode, char *quename, char *devname)
{
	show_failed_prefix();
	switch (opcode) {
	case ADD_OP:
		printf ("Problem adding queue->device mapping:\n");
		show_failed_prefix();
		printf ("%s -> %s.\n", quename, devname);
		break;
	case DEL_OP:
		printf ("Problem deleting queue->device mapping:\n");
		show_failed_prefix();
		printf ("%s -> %s.\n", quename, devname);
		break;
	case SET_OP:
		printf ("Problem setting queue->device mapping:\n");
		show_failed_prefix();
		printf ("%s -> %s.\n", quename, devname);
		break;
	}
}


/*** adsfor_set
 *
 *
 *	void adsfor_set():
 *
 *	Add/delete/set forms set.  The set of forms to add/delete/set
 *	is defined by the scan_forset() function which is used to
 *	scan/parse a forms set specification.
 */
void adsfor_set (int opcode)
{

	long comcode;			/* Completion code */
	register char *formname;	/* Form name */
	register short exitflag;	/* Exit flag */
	register int opcode2;		/* Internal opcode */
	short mssgflag;			/* BOOLEAN TRUE if a message has */
					/* been delivered to the user */

	/*
	 *  Loop to add/delete/set forms set.
	 */
	mssgflag = 0;
	exitflag = 0;
	opcode2 = opcode;
	while ((formname = deq_set()) != NULL && !exitflag) {
		/*
		 *  Perform the operation.
		 */
		switch (opcode2) {
		case ADD_OP:
			comcode = addfor (formname);
			break;
		case DEL_OP:
			comcode = delfor (formname);
			break;
		case SET_OP:
			comcode = setfor (formname);
			opcode2 = ADD_OP;
			break;
		}
		switch (comcode) {
		case TCML_INTERNERR:
		case TCML_NOESTABLSH:
		case TCML_NOLOCALDAE:
		case TCML_PROTOFAIL:
			adsfor_problem (opcode, formname);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		case TCML_COMPLETE:
			break;
		case TCML_ALREADEXI:
			adsfor_problem (opcode, formname);
			diagnose (comcode);
			mssgflag = 1;
			break;
		case TCML_NOSUCHFORM:
			adsfor_problem (opcode, formname);
			diagnose (comcode);
			mssgflag = 1;
			break;
		default:
			adsfor_problem (opcode, formname);
			diagnose (TCML_UNDEFINED);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
		}
	}
	if (!mssgflag) diagnose (TCML_COMPLETE);
}


/*** adsfor_problem
 *
 *
 *	void adsfor_problem():
 *	Indicate problem adding/deleting/setting forms set.
 */
static void adsfor_problem (int opcode, char *formname)
{
	show_failed_prefix();
	switch (opcode) {
	case ADD_OP:
		printf ("Problem adding form: %s to forms set.\n", formname);
		break;
	case DEL_OP:
		printf ("Problem deleting form: %s from forms set.\n",
			formname);
		break;
	case SET_OP:
		printf ("Problem setting forms set to include: %s.\n",
			formname);
		break;
	}
}


/*** adsgid_set
 *
 *
 *	void adsgid_set():
 *
 *	Add/delete/set gidset.  The set of gids (group id's) to
 *	add/delete/set is defined by the scan_groupset() function
 *	which is used to scan/parse a gid set specification.
 *	Setting is currently impossible.
 */
void adsgid_set (char *quename, int opcode)
{
	long comcode;			/* Completion code */
	register short exitflag;	/* Exit flag */
	char *group_name;		/* Group as specified by qmgr user */
	long gid;			/* Group-id */
	short mssgflag;			/* BOOLEAN TRUE if a message has */
					/* been delivered to the user */

	/*
	 *  Loop to add/delete/set gid set.
	 */
	mssgflag = 0;
	exitflag = 0;
	while ((group_name = deq_set()) != NULL && !exitflag) {
		/*
		 *  Perform the operation.
		 */
		gid = deq_uid_gid(); /* Get user-id */
		if (gid != -1) {
			/*
			 *  The group exists, as far as we can tell....
			 */
			switch (opcode) {
			case ADD_OP:
				comcode = addquegid (quename, (gid_t) gid);
				break;
			case DEL_OP:
				comcode = delquegid (quename, (gid_t) gid);
				break;
			}
		}
		else comcode = TCML_NOSUCHGRP;	/* No such group */
		switch (comcode) {
		case TCML_INTERNERR:
		case TCML_NOESTABLSH:
		case TCML_NOLOCALDAE:
		case TCML_PROTOFAIL:
			adsgid_problem (opcode, quename, group_name);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		case TCML_COMPLETE:
			break;
		case TCML_ALREADACC:
		case TCML_NOACCNOW:
		case TCML_NOSUCHGRP:
		case TCML_NOSUCHQUE:
		case TCML_UNRESTR:
			adsgid_problem (opcode, quename, group_name);
			diagnose (comcode);
			mssgflag = 1;
			break;
		default:
			adsgid_problem (opcode, quename, group_name);
			diagnose (TCML_UNDEFINED);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
		}
	}
	if (!mssgflag) diagnose (TCML_COMPLETE);
}


/*** adsgid_problem
 *
 *
 *	void adsgid_problem():
 *	Indicate problem adding/deleting/setting gid set.
 */
static void adsgid_problem (int opcode, char *quename, char *group_name)
{
	show_failed_prefix();
	switch (opcode) {
	case ADD_OP:
		printf ("Problem adding group: %s; queue = %s.\n",
			group_name, quename);
		break;
	case DEL_OP:
		printf ("Problem deleting group: %s; queue = %s.\n",
			group_name, quename);
		break;
	}
}


/*** adsmgr_set
 *
 *
 *	void adsmgr_set():
 *
 *	Add/delete/set manager set.  The set of manager accounts to
 *	add/delete/set is defined by the scan_mgrset() function which
 *	is used to scan/parse a manager account set specification.
 */
void adsmgr_set (int opcode)
{

	long comcode;			/* Completion code */
	register short exitflag;	/* Exit flag */
	register int opcode2;		/* Internal opcode */
	char *mgr_name;			/* Manager account name */
	Mid_t mgr_mid;			/* Manager machine-id */
	long mgr_uid;			/* Manager user-id */
	short privileges;		/* Privileges mask */
	short mssgflag;			/* BOOLEAN TRUE if a message has */
					/* been delivered to the user */

	/*
	 *  Loop to add/delete/set manager set.
	 */
	mssgflag = 0;
	exitflag = 0;
	if (opcode == SET_OP) {
		setnqsman();		/* Reduce set to only "root" */
		opcode2 = ADD_OP;	/* SET becomes ADD */
	}
	else opcode2 = opcode;
	while ((mgr_name = deq_set()) != NULL && !exitflag) {
		/*
		 *  Perform the operation.
		 */
		mgr_mid = deq_mid();	/* Get manager machine-id */
		mgr_uid = deq_uid_gid(); /* Get manager user-id */
		privileges = deq_priv();/* Get manager privilege mask */
		if (mgr_uid != -1) {
			/*
			 *  The account exists, as far as we can tell....
			 */
			switch (opcode2) {
			case ADD_OP:
				comcode = addnqsman ((uid_t) mgr_uid, mgr_mid,
						     privileges);
				break;
			case DEL_OP:
				comcode = delnqsman ((uid_t) mgr_uid, mgr_mid,
						     privileges);
				break;
			}
		}
		else comcode = TCML_NOSUCHACC;	/* No such account */
		switch (comcode) {
		case TCML_INTERNERR:
		case TCML_NOESTABLSH:
		case TCML_NOLOCALDAE:
		case TCML_PROTOFAIL:
			adsmgr_problem (opcode, mgr_name);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		case TCML_COMPLETE:
			break;
		case TCML_ALREADEXI:
		case TCML_NOSUCHACC:
		case TCML_NOSUCHMAN:
		case TCML_ROOTINDEL:
			adsmgr_problem (opcode, mgr_name);
			diagnose (comcode);
			mssgflag = 1;
			break;
		default:
			adsmgr_problem (opcode, mgr_name);
			diagnose (TCML_UNDEFINED);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
		}
	}
	if (!mssgflag) diagnose (TCML_COMPLETE);
}


/*** adsmgr_problem
 *
 *
 *	void adsmgr_problem():
 *	Indicate problem adding/deleting/setting manager set.
 */
static void adsmgr_problem (int opcode, char *mgr_name)
{
	show_failed_prefix();
	switch (opcode) {
	case ADD_OP:
		printf ("Problem adding account: %s to manager set.\n",
			mgr_name);
		break;
	case DEL_OP:
		printf ("Problem deleting account: %s from manager set.\n",
			mgr_name);
		break;
	case SET_OP:
		printf ("Problem setting manager set to include: %s.\n",
			mgr_name);
		break;
	}
}


/*** adsque_set
 *
 *
 *      void adsque_set():
 *
 *      Add/remove/set queue set.  The set of scheduler queues to
 *      add/remove/set is defined by the scan_queset() function
 *      which is used to scan/parse a queue set specification.
 *      Setting is currently impossible.
 */
void adsque_set (qcom_name, opcode)
char *qcom_name;                        /* Queue complex name */
int opcode;                             /* Operation code = ADD_OP or DEL_OP */{

        long comcode;                   /* Completion code */
        register short exitflag;        /* Exit flag */
        char *que_name;                 /* Queue complex name */
        short mssgflag;                 /* BOOLEAN TRUE if a message has */
                                        /* been delivered to the user */

        /*
         *  Loop to add/remove/set queue set.
         */
        mssgflag = 0;
        exitflag = 0;
        while ((que_name = deq_set()) != NULL && !exitflag) {
                /*
                 *  Perform the operation.
                 */
                switch (opcode) {
                case ADD_OP:
                        comcode = addqcomque (que_name, qcom_name);
                        break;
                case DEL_OP:
                        comcode = remqcomque (que_name, qcom_name);
                        break;
                }
                switch (comcode) {
                case TCML_INSUFFMEM:
                case TCML_INTERNERR:
                case TCML_NOESTABLSH:
                case TCML_NOLOCALDAE:
                case TCML_PROTOFAIL:
                        adsque_problem (opcode, que_name, qcom_name);
                        diagnose (comcode);
                        mssgflag = 1;
                        exitflag = 1;           /* Exit */
                        break;
                case TCML_COMPLETE:
                        break;
                case TCML_ALREADEXI:
                case TCML_NOSUCHCOM:
                case TCML_NOSUCHQUE:
                case TCML_WROQUETYP:
                        adsque_problem (opcode, que_name, qcom_name);
                        diagnose (comcode);
                        mssgflag = 1;
                        break;
                default:
                        adsque_problem (opcode, que_name, qcom_name);
                        diagnose (TCML_UNDEFINED);
                        mssgflag = 1;
                        exitflag = 1;           /* Exit */
                }
        }
        if (!mssgflag) diagnose (TCML_COMPLETE);
}


/*** adsque_problem
 *
 *
 *      void adsque_problem():
 *      Indicate problem adding/removing/setting queue set.
 */
static void adsque_problem (
	int opcode,                     /* Opcode = ADD_OP or DEL_OP */
	char *que_name,                 /* Queue name */
	char *qcom_name)                /* Queue complex name */
{
        show_failed_prefix();
        switch (opcode) {
        case ADD_OP:
                printf ("Problem adding queue: %s; queue complex = %s.\n",
                        que_name, qcom_name);
                break;
        case DEL_OP:
                printf ("Problem removing queue: %s; queue complex = %s.\n",
                        que_name, qcom_name);
                break;
        }
}

/*** adsuid_set
 *
 *
 *	void adsuid_set():
 *
 *	Add/delete/set uidset.  The set of uids (user id's) to
 *	add/delete/set is defined by the scan_userset() function
 *	which is used to scan/parse a uid set specification.
 *	Setting is currently impossible.
 */
void adsuid_set (char *quename, int opcode)
{

	long comcode;			/* Completion code */
	register short exitflag;	/* Exit flag */
	char *user_name;		/* User account name */
	long uid;			/* User-id */
	short mssgflag;			/* BOOLEAN TRUE if a message has */
					/* been delivered to the user */

	/*
	 *  Loop to add/delete/set uid set.
	 */
	mssgflag = 0;
	exitflag = 0;
	while ((user_name = deq_set()) != NULL && !exitflag) {
		/*
		 *  Perform the operation.
		 */
		uid = deq_uid_gid(); /* Get user-id */
		if (uid != -1) {
			/*
			 *  The account exists, as far as we can tell....
			 */
			switch (opcode) {
			case ADD_OP:
				comcode = addqueuid (quename, (uid_t) uid);
				break;
			case DEL_OP:
				comcode = delqueuid (quename, (uid_t) uid);
				break;
			}
		}
		else comcode = TCML_NOSUCHACC;	/* No such account */
		switch (comcode) {
		case TCML_INTERNERR:
		case TCML_NOESTABLSH:
		case TCML_NOLOCALDAE:
		case TCML_PROTOFAIL:
			adsuid_problem (opcode, quename, user_name);
			diagnose (comcode);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
			break;
		case TCML_COMPLETE:
			break;
		case TCML_ALREADACC:
		case TCML_NOACCNOW:
		case TCML_NOSUCHACC:
		case TCML_NOSUCHQUE:
		case TCML_UNRESTR:
			adsuid_problem (opcode, quename, user_name);
			diagnose (comcode);
			mssgflag = 1;
			break;
		default:
			adsuid_problem (opcode, quename, user_name);
			diagnose (TCML_UNDEFINED);
			mssgflag = 1;
			exitflag = 1;		/* Exit */
		}
	}
	if (!mssgflag) diagnose (TCML_COMPLETE);
}


/*** adsuid_problem
 *
 *
 *	void adsuid_problem():
 *	Indicate problem adding/deleting/setting uid set.
 */
static void adsuid_problem (
	int opcode,		/* Opcode = ADD_OP or DEL_OP */
	char *quename,		/* Queue name */
	char *user_name)	/* Account name */
{
	show_failed_prefix();
	switch (opcode) {
	case ADD_OP:
		printf ("Problem adding user: %s; queue = %s.\n",
			user_name, quename);
		break;
	case DEL_OP:
		printf ("Problem deleting user: %s; queue = %s.\n",
			user_name, quename);
		break;
	}
}
