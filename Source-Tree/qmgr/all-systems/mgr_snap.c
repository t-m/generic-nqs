/*
 * qmgr/mgr_snap.c
 * 
 * DESCRIPTION
 *
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <libnqs/nqsxvars.h>		/* NQS external vars */
#include <libnqs/transactcc.h>          /* For TCML_COMPLETE */
					/* from a database file */
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <SETUP/autoconf.h>

static char *get_snap_units ( short units );
static void snap_batch ( struct gendescr *descr );
static void snap_complex ( struct gendescr *descr );
static void snap_dest ( struct gendescr *que_descr );
static void snap_global ( void );
static void snap_managers ( void );
static void snap_param ( void );
static void snap_pipe ( struct gendescr *descr );
static void snap_queacc ( struct gendescr *descr );
static void snap_servers ( void );

extern gid_t cgid;			/* Caller's gid */
extern Mid_t Local_mid;
extern int Init_mgr;
extern char *our_cwd;

FILE *genfile;				/* Qmgr command generation file */

int snap (char *filename)
{

	struct gendescr *descr;
	int newfd;		/* File descriptor of commands file */
	time_t nownow;
	uid_t ouruid;
	char filename_path [MAX_PATHNAME+1];
	struct passwd * pstPasswd = NULL;
	
	seekdb (Paramfile, 0L);
	ldparam ();

	ouruid = getuid();
	if ( setuid(ouruid) != 0) {
	    show_failed_prefix();
	    fprintf(stderr, "Unable to setuid to %d, errno = %d\n",
			ouruid, errno);
	    fflush(stderr);
	    return(TCML_EPERM);
	}
	/*
	 * If an absolute pathname,  use as is.  Otherwise, prepend with the
	 * current working directory.
	 */
	if (*filename == '/') {
	    strcpy (filename_path, filename);
	} else {
	    strcpy (filename_path, our_cwd);
	    strcat (filename_path, "/");
	    strcat (filename_path, filename);
	}
	if ((newfd = open (filename_path, O_CREAT | O_WRONLY | O_TRUNC, 0644)) == -1) {
	    show_failed_prefix();
	    fprintf (stderr, "Unable to open %s for writing qmgr", filename);
	    fprintf (stderr, " configuration commands.\n");
	    fflush (stderr);
	    return( errnototcm() );
	}

	genfile = fdopen (newfd, "w");

	if (telldb (Queuefile) != -1) {
	    /*
	     * We are not necessarily at the start of the database file
	     */
	    seekdb (Queuefile, 0L);		/* Seek to the beginning */
	}
	time (&nownow);

	pstPasswd = sal_fetchpwuid(ouruid);
	if (pstPasswd == NULL)
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Unable to retrieve password record for uid %ld\n", ouruid);

	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Snap file created for %s by %s on %s", 
		 getmacnam (Local_mid), pstPasswd->pw_name, ctime( &nownow) );
	fprintf (genfile, "#\n");
	fprintf (genfile, "#    NQS Version %s\n",  NQS_VERSION);
	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Create the batch queues and set the defined");
	fprintf (genfile, " attributes\n");
	fprintf (genfile, "#\n");

	while ((descr = nextdb (Queuefile)) != (struct gendescr *) 0)  {
	    if (descr->v.que.type == QUE_BATCH) snap_batch (descr);
	}
	if (telldb (Queuefile) != -1) {
	    /*
	     * We are not necessarily at the start of the database file
	     */
	    seekdb (Queuefile, 0L);		/* Seek to the beginning */
	}
	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Create the pipe queues and set the defined");
	fprintf (genfile, " attributes\n");
	fprintf (genfile, "#\n");

	while ((descr = nextdb (Queuefile)) != (struct gendescr *) 0)  {
	    if (descr->v.que.type == QUE_PIPE) snap_pipe (descr);
	}
	fflush (genfile);
	if (telldb (Queuefile) != -1) {
	    /*
	     * We are not necessarily at the start of the database file
	     */
	    seekdb (Queuefile, 0L);		/* Seek to the beginning */
	}
	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Add the destinations to the pipe queues.\n");
	fprintf (genfile, "#\n");

	while ((descr = nextdb (Queuefile)) != (struct gendescr *) 0)
	    if (descr->v.que.type == QUE_PIPE) snap_dest (descr);

	if (telldb (Qcomplexfile) != -1) {
	    /*
	     * We are not necessarily at the start of the database file
	     */
	    seekdb (Qcomplexfile, 0L);	/* Seek to the beginning */
	}
	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Create and set up queue complexes.\n");
	fprintf (genfile, "#\n");

	while ((descr = nextdb (Qcomplexfile)) != (struct gendescr *) 0)  {
		snap_complex (descr);
	}
	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Add all managers to the list.\n");
	fprintf (genfile, "#\n");
	snap_managers ();

	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Set general parameters.\n");
	fprintf (genfile, "#\n");
	snap_param ();
 
	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Set global parameters.\n");
	fprintf (genfile, "#\n");
	snap_global ();

	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Set compute servers.\n");
	fprintf (genfile, "#\n");
	snap_servers ();

	fclose (genfile);
	return (TCML_COMPLETE);
}


static void snap_batch (struct gendescr *descr)
{
	char *qname;

	qname = descr->v.que.namev.name;
	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Creating and defining batch queue %s\n", qname);
	fprintf (genfile, "#\n");
	fprintf (genfile, "create batch_queue %s priority=%d",  qname,
		descr->v.que.priority);
	if (descr->v.que.status & QUE_PIPEONLY) fprintf (genfile, " pipeonly");
	fprintf (genfile, "\n");
#if     IS_SGI
	if (descr->v.que.ndp != 0) 
	    fprintf(genfile, "set ndp=%d %s\n", descr->v.que.ndp, qname);
#endif
	/*
	 *  Set the run limits.
	 */
	fprintf (genfile, "set run_limit=%d %s\n", 
		descr->v.que.v1.batch.runlimit, qname);
	fprintf (genfile, "set user_limit=");
	fprintf (genfile, "%d %s\n",  descr->v.que.v1.batch.userlimit, qname);
	/*
	 *  Set per_process and per_request limits.
	 */
	fprintf (genfile, "set corefile_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PPCORE)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.ppcorecoeff, 
			 get_snap_units (descr->v.que.v1.batch.ppcoreunits),
			 qname);
	fprintf (genfile, "set data_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PPDATA)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.ppdatacoeff, 
			 get_snap_units (descr->v.que.v1.batch.ppdataunits),
			 qname);
	fprintf (genfile, "set per_process permfile_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PPPFILE)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.pppfilecoeff, 
			 get_snap_units (descr->v.que.v1.batch.pppfileunits),
			 qname);
	fprintf (genfile, "set per_request permfile_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PRPFILE)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.prpfilecoeff, 
			 get_snap_units (descr->v.que.v1.batch.prpfileunits),
			 qname);
	fprintf (genfile, "set per_process tempfile_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PPTFILE)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.pptfilecoeff, 
			 get_snap_units (descr->v.que.v1.batch.pptfileunits),
			 qname);
	fprintf (genfile, "set per_request tempfile_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PRTFILE)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.prtfilecoeff, 
			 get_snap_units (descr->v.que.v1.batch.prtfileunits),
			 qname);
	fprintf (genfile, "set per_process memory_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PPMEM)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.ppmemcoeff, 
			 get_snap_units (descr->v.que.v1.batch.ppmemunits),
			 qname);
	fprintf (genfile, "set per_request memory_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PRMEM)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.prmemcoeff, 
			 get_snap_units (descr->v.que.v1.batch.prmemunits),
			 qname);
	fprintf (genfile, "set stack_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PPSTACK)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.ppstackcoeff, 
			 get_snap_units (descr->v.que.v1.batch.ppstackunits),
			 qname);
	fprintf (genfile, "set working_set_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PPWORK)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu %s) %s\n",
			 descr->v.que.v1.batch.ppworkcoeff, 
			 get_snap_units (descr->v.que.v1.batch.ppworkunits),
			 qname);
	fprintf (genfile, "set per_process cpu_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PPCPUT)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, " %lu ) %s\n",
			 descr->v.que.v1.batch.ppcpusecs, qname);
	fprintf (genfile, "set per_request cpu_limit = (");
	if (descr->v.que.v1.batch.infinite & LIM_PRCPUT)
	    fprintf (genfile, "unlimited) %s\n", qname);
	else
	    fprintf (genfile, "%lu) %s\n",
			 descr->v.que.v1.batch.prcpusecs, qname);
	fprintf (genfile, "set nice_value_limit=%d %s\n", 
		 descr->v.que.v1.batch.ppnice, qname);
	/*
	 *  Set up the queue's access restrictions
	 */
	if (descr->v.que.status & QUE_BYGIDUID) {
	    fprintf (genfile, "set no_access %s\n", qname);
	    snap_queacc (descr);
	}
	if (descr->v.que.status & QUE_RUNNING)  
		fprintf (genfile, "start queue %s\n", qname);
	if (descr->v.que.status & QUE_ENABLED)
		fprintf (genfile, "enable queue %s\n", qname);
	fflush (genfile);
}


static void snap_pipe (struct gendescr *descr)
{
	char *qname;

	qname = descr->v.que.namev.name;
	fprintf (genfile, "#\n");
	fprintf (genfile, "#    Creating and defining pipe queue %s\n", qname);
	fprintf (genfile, "#\n");
	fprintf (genfile, "create pipe_queue %s priority=%d server=(%s)\n", qname, 
		 descr->v.que.priority, 
		 descr->v.que.v1.pipe.server);
	if (descr->v.que.status & QUE_PIPEONLY)
		fprintf (genfile, "set pipeonly %s\n", qname);
	if (descr->v.que.status & QUE_LDB_IN)
		fprintf (genfile, "set lb_in %s\n", qname);
	if (descr->v.que.status & QUE_LDB_OUT)
		fprintf (genfile, "set lb_out %s\n", qname);
	/*
	 *  Set the run limits.
	 */
	fprintf (genfile, "set run_limit=%d %s\n", 
		descr->v.que.v1.pipe.runlimit, qname);
	/*
	 *  Set up the queue's access restrictions
	 */
	if (descr->v.que.status & QUE_BYGIDUID) {
	    fprintf (genfile, "set no_access %s\n", qname);
	    snap_queacc (descr);
	}
	if (descr->v.que.status & QUE_RUNNING)
		fprintf (genfile, "start queue %s\n", qname);
	if (descr->v.que.status & QUE_ENABLED)
		fprintf (genfile, "enable queue %s\n", qname);
	fflush (genfile);
}


static void snap_queacc (struct gendescr *descr)
{
	
	int fd;				/* File descriptor */
	unsigned long buffer [QAFILE_CACHESIZ];
	int done;			/* Boolean */
	int cachebytes;			/* Try to read this much */
	int bytes;			/* Did read this much */
	int entries;			/* Did read this many */
	int i;				/* Loop variable */


	fd = openqacc (Queuefile, descr);
	/*
	 * Openqacc returns 0 if the queue is restricted and the file
	 * is open; -1 if the queue is unrestricted; -2 if the queue
	 * queue is restricted but the file is not open, and -3
	 * if the queue appears to have been deleted.
	 */
	if (fd < -1) return;
	done = 0;
	cachebytes = QAFILE_CACHESIZ * sizeof (unsigned long);
	while ((bytes = read (fd, (char *) buffer, cachebytes)) > 0) {
	    entries = bytes / sizeof (unsigned long);
	    for (i = 0; i < entries; i++) {
		/* Zero comes only at the end */
		if (buffer [i] == 0) {
		    done = 1;
		    break;
		}
		if (buffer [i] & MAKEGID) 	/* If a group */
			fprintf (genfile, "add groups = %s %s\n",
				getgrpnam ((int) buffer [i] & ~MAKEGID),
				descr->v.que.namev.name);
		else				/* A user */
			fprintf (genfile, "add users = %s %s\n",
						getusenam ((int) buffer [i]), 
						descr->v.que.namev.name);
	    }
	    if (done) break;
	}
	fflush (genfile);
}


static void snap_dest (struct gendescr *que_descr)
{
	register struct gendescr *dbds1;/* NQS database file generic descr */
	register struct gendescr *dbds2;/* NQS database file generic descr */
	char *rhostname;		/* Remote host destination name */
	
	if (telldb (Qmapfile) != -1) {
	    /*
	     *  We are not necessarily at the start of the
	     *  queue/device/destination descriptor database
	     *  file....
	     */
	    seekdbb (Qmapfile, 0L);	/* Seek to the beginning */
	}
	dbds1 = nextdb (Qmapfile);
	while (dbds1 != (struct gendescr *) 0) {
	    if (!dbds1->v.map.qtodevmap &&
		strcmp (dbds1->v.map.v.qdestmap.lqueue,
		    que_descr->v.que.namev.name) == 0) {
		/*
		 *  We have located a pipe-queue/destination mapping
		 *  for the pipe queue.
		 *
		 *  Now, locate the database description for the
		 *  pipe queue destination referenced in the mapping.
		 */
		if (telldb (Pipeqfile) != -1) {
		    /*
		     *  We are not necessarily at the start of the
		     *  pipe queue descriptor database file....
		     */
		    seekdbb (Pipeqfile, 0L);/* Seek to start */
		}
		dbds2 = nextdb (Pipeqfile);
		while (dbds2 != (struct gendescr *) 0 &&
		      (strcmp (dbds1->v.map.v.qdestmap.rqueue,
		       dbds2->v.dest.rqueue) ||
		       dbds1->v.map.v.qdestmap.rhost_mid !=
		       dbds2->v.dest.rhost_mid)) {
			/*
			 *  We have not yet located the pipe queue
			 *  destination referred to in the mapping
			 *  descriptor.
			 */
			dbds2 = nextdb (Pipeqfile);
		}
		if (dbds2 != (struct gendescr *) 0) {
		    /*
		     *  We have located the pipe-queue
		     *  descriptor for the mapping.
		     */
		    rhostname = getmacnam (dbds2->v.dest.rhost_mid);
		    fprintf (genfile, "add destination=");
		    if (dbds2->v.dest.rhost_mid == Local_mid)
		        fprintf (genfile, "%s %s\n", dbds2->v.dest.rqueue,
				que_descr->v.que.namev.name);
		    else
		        fprintf (genfile, "%s@%s %s\n",
				 dbds2->v.dest.rqueue,
				 getmacnam(dbds2->v.dest.rhost_mid),
				 que_descr->v.que.namev.name);
		}
	    }
	    dbds1 = nextdb (Qmapfile);
	}
}

static void snap_complex (struct gendescr *descr)
{
	register int i;
	register int count;

	count = 0;
	for (i = 0; i < MAX_QSPERCOMPLX; i++) {
	    if (descr->v.qcom.queues[i][0] != (char)NULL) {
		if (count) {
		    fprintf (genfile, "add queue=(%s) %s\n",
					 descr->v.qcom.queues[i],
					 descr->v.qcom.name);
		} else {
		    fprintf (genfile, "create complex=(%s) %s\n",
					 descr->v.qcom.queues[i],
					 descr->v.qcom.name);
		    count++;
		}
	    }
	}
	if (!count) return;
	/*
	 *  Produce the qmgr commands to set the complex limits.
	 */
	fprintf (genfile, "set complex run_limit=%d %s\n", 
		descr->v.qcom.runlimit, descr->v.qcom.name);
	fprintf (genfile, "set complex user_limit=%d %s\n",  
		 descr->v.qcom.userlimit, 
		 descr->v.qcom.name);
	fflush (genfile);
}


static void snap_managers(void)
{
	register struct gendescr *descr;  /* NQS database file queue descr*/

	seekdb (Mgrfile, 0L);
	descr = nextdb (Mgrfile);
	while ((descr = nextdb (Mgrfile)) != (struct gendescr *) 0) {
	    fprintf (genfile, "add managers ");
	    if (descr->v.mgr.manager_mid != Local_mid) 
		    fprintf (genfile, "[%d]@%s", (int) descr->v.mgr.manager_uid,
			     getmacnam (descr->v.mgr.manager_mid));
	    else fprintf (genfile, "%s", getusenam (descr->v.mgr.manager_uid));
	    if (descr->v.mgr.privileges & QMGR_MGR_PRIV)
		    fprintf (genfile, ":m\n");
	    else fprintf (genfile, ":o\n");
	}
	fflush (genfile);
}
 
static void snap_servers(void)
{
        register struct gendescr *descr;  /* NQS database file queue descr*/

        seekdb (Serverfile, 0L);
        /* descr = nextdb (Serverfile); */
        while ((descr = nextdb (Serverfile)) != (struct gendescr *) 0) {
            fprintf (genfile, "set server performance=");
	    fprintf (genfile, "%ld %s\n", descr->v.cserver.rel_perf,
			getmacnam (descr->v.cserver.server_mid) );
        }
        fflush (genfile);
}


static void snap_param (void)
{

	fprintf (genfile, "set debug %d\n", sal_debug_GetLevel());
	fprintf (genfile, "set default batch_request priority %d\n", Defbatpri);
	if (Defbatque [0] != '\0')
		fprintf (genfile, "set default batch_request queue %s\n", Defbatque);
	fprintf (genfile, "set default destination_retry time %ld\n",  
		 (long) Defdesrettim / 3600);
	fprintf (genfile, "set default destination_retry wait %ld\n", 
		 (long) Defdesretwai / 60);
	fprintf (genfile, "set lifetime %ld\n", Lifetime / 3600);
	if (Logfilename [0] != '\0')
		fprintf (genfile, "set log_file  %s\n",  Logfilename);
	fprintf (genfile, "set mail %s\n", getusenam (Mail_uid));
	if (Netclient [0] != '\0')
		fprintf (genfile, "set network client=(%s)\n", Netclient);
	if (Netdaemon [0] != '\0')
		fprintf (genfile, "set network daemon=(%s)\n", Netdaemon);
	if (Netserver [0] != '\0')
		fprintf (genfile, "set network server=(%s)\n", Netserver);
	if (Loaddaemon [0] != '\0')
		fprintf (genfile, "set load_daemon=(%s)\n", Loaddaemon);
	fprintf (genfile, "set shell_strategy ");
	if (Shell_strategy == SHSTRAT_FREE)
		fprintf (genfile, "free\n");
	else if (Shell_strategy == SHSTRAT_LOGIN)
		fprintf (genfile, "login\n");
	else
		fprintf (genfile, "fixed=(%s)\n",Fixed_shell);
	if (Plockdae)
		fprintf (genfile, "lock local_daemon\n");
	if (LB_Scheduler)
		fprintf (genfile, "set scheduler %s\n", fmtmidname(LB_Scheduler));
	else
		fprintf (genfile, "set no_scheduler\n");
	fprintf(genfile,  "set default load_interval %ld\n", Defloadint/60);
	fflush (genfile);
}  



static void snap_global (void)
{
	fprintf (genfile, "set global batch_limit=%d\n",  Maxgblbatlimit);
	fprintf (genfile, "set global pipe_limit=%d\n",  Maxgblpiplimit);
	fflush (genfile);
}

static char *get_snap_units (short units)
{
	switch (units) {
	case QLM_BYTES:
		return ("b");
	case QLM_WORDS:
		return ("w");
	case QLM_KBYTES:
		return ("kb");
	case QLM_KWORDS:
		return ("kw");
	case QLM_MBYTES:
		return ("mb");
	case QLM_MWORDS:
		return ("mw");
	case QLM_GBYTES:
		return ("gb");
	case QLM_GWORDS:
		return ("gw");
	}
	return ("");		/* Unknown units! */
}

