/*
 * nmap/nmap.h
 *
 * DESCRIPTION:
 *	Nmap.h:  This header file contains the definitions of the
 *		 data types and return codes used in the Network
 *		 Mapping procedures in the Nmap library.
 */


#ifndef _NQS_NMAPH_
#define _NQS_NMAPH_

#include <libsal/types.h>
#include <sys/types.h>

/*
 *
 *	Data types:
 */

/* 
 * Mid_t has to be a 32-bit unsigned number.  'unsigned long' on some
 * architectures happens to be a 64-bit number, so we add support for
 * this ...
 * 
 * Long term, we really have to replace these binary quantities once
 * and for all ...
 */
#ifdef	HAS_32BIT_LONG
typedef unsigned long  Mid_t;
#else
typedef	unsigned int Mid_t;	/* Machine-id type.		*/
#endif

/*
 *
 *	Return codes:
 */
#define	NMAP_SUCCESS	 0	/* Success!			*/
#define	NMAP_DEFMAP	 1	/* No uid(gid) mapping existed.	*/
				/* The default uid(gid) mapping	*/
				/* for the specified machine has*/
				/* been returned.		*/
#define	NMAP_EUNEXPECT	-1	/* Serious major error; like	*/
				/* EMFILE or ENFILE, or one of	*/
				/* mapping files is screwed up;	*/
				/* Something that should have	*/
				/* worked, did not.		*/
#define NMAP_ENOPRIV	-2	/* Insufficient privilege to	*/
				/* access or update mapping	*/
				/* database.			*/
#define	NMAP_ECONFLICT	-3	/* Machine already exists or an	*/
				/* attempt was made to add a	*/
				/* uid(gid) mapping which	*/
				/* already existed on the target*/
				/* machine and the new mapping	*/
				/* differed from the existing	*/
				/* mapping.			*/
#define	NMAP_ENOMAP	-4	/* No mapping exists.		*/
#define	NMAP_ENOMID	-5	/* No machine as specified by	*/
				/* the mid parameter in the call*/
				/* exists.			*/
#define	NMAP_EBADNAME	-6	/* Bad name specified.		*/
/*
 *
 *	nmap_ctl opcodes:
 */
#define	NMAP_OPNOFD	0	/* Set number of file descriptors */
#define	NMAP_OPUIDC	1	/* Set uid cache size */
#define	NMAP_OPGIDC	2	/* Set gid cache size */
#define	NMAP_OPNAMC	3	/* Set name cache size */

#include "nmapdb.h"
#include "nmap_proto.h"
#endif  /* _NQS_NMAPH_ */
