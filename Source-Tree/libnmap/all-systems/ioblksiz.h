/*
 *	Network Queueing System (NQS)
 *  This version of NQS is Copyright (C) 1992  John Roman
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 1, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
*  PROJECT:     Network Queueing System
*  AUTHOR:      John Roman
*
*  Modification history:
*
*       Version Who     When            Description
*       -------+-------+---------------+-------------------------
*       V01.10  JRR                     Initial version.
*       V01.20  JRR     16-Jan-1992	Added support for RS6000.
*	V01.3	JRR	17-Jun-1992	Added header.
*	V01.4	JRR	11-Nov-1992	Added HPUX.
*	V01.5	JRR	06-Apr-1993	Added support for DECOSF.
*	V01.6	JRR	02-Mar-1994	Added support for SOLARIS.
*/
/*++ ioblksiz.h - optimal block size for indivisible file system I/O operations
 *
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/libnmap/all-systems/ioblksiz.h,v $
 *
 * DESCRIPTION:
 *
 *	This header file defines the optimal block size for indivisible
 *	file system I/O operations.  This value defines the largest
 *	number of bytes that can be read/written from/to a file in a
 *	single indivisible I/O operation (ATOMICBLKSIZ).  It is also
 *	permissible for this value to be an exact divisor of the true
 *	"atomic block size."  In either case, this value MUST satisfy
 *	these constraints for the supporting system.
 *
 *
 *	WARNING:
 *	  Eight system types are defined:  BSD43, SGI, SYS52, IBMRS, SOLARIS
 *	  HPUX, ULTRIX, & DECOSF. Accordingly, the flag: SYSTEM_TYPE
 *	  in the Makefile must be appropriately defined.
 *
 *	  The definition of ATOMICBLKSIZ must NOT be changed without
 *	  careful forethought.  If the value of ATOMICBLKSIZ is changed,
 *	  then the database software that is dependent upon the correct
 *	  definition of ATOMICBLKSIZ can be easily broken.  Examples of
 *	  such software include the nmap (network mapping) database
 *	  routines, and the Network Queueing System (NQS).
 *
 *	  Be careful.
 *
 *
 *	Author(s):
 *	----------
 *	Brent A. Kingsbury, and Kate R. Rosenbloom.
 *	Sterling Software Incorporated.
 *
 *
 * STANDARDS VIOLATIONS:
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: ioblksiz.h,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:11  nqs
 * Imported sources
 *
 * Revision 1.6  1994/03/30  20:35:37  jrroma
 * Version 3.35.6
 *
 * Revision 1.5  93/07/13  21:33:14  jrroma
 * Version 3.34
 * 
 * Revision 1.4  92/12/22  15:51:37  jrroma
 * Version 3.30
 * 
 * Revision 1.3  92/06/18  14:39:16  jrroma
 * Added gnu header
 * 
 * Revision 1.2  92/01/17  12:47:55  jrroma
 * Added support for RS6000.
 * 
 * Revision 1.1  92/01/17  12:47:10  jrroma
 * Initial revision
 * 
 *
 */
#ifndef _NQS_IOBLKSIZH_
#define _NQS_IOBLKSIZH_

#if	IS_BSD | IS_ULTRIX | IS_DECOSF | IS_UNICOS	/* Berkeley UNIX */
#define	ATOMICBLKSIZ	4096		/* (On some Berkeley systems, it */
					/* might be 8192) */
#else
#if	IS_LINUX | IS_SYSVr4 | IS_POSIX_1
#define	ATOMICBLKSIZ	1024		/* This is as good a default size */
					/* as any ... 	*/
#else
BAD SYSTEM TYPE
#endif
#endif

#endif

