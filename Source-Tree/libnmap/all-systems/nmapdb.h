/*
 *	Network Queueing System (NQS)
 *  This version of NQS is Copyright (C) 1992  John Roman
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 1, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
*  PROJECT:     Network Queueing System
*  AUTHOR:      John Roman
*
*  Modification history:
*
*       Version Who     When            Description
*       -------+-------+---------------+-------------------------
*       V01.10  JRR                     Initial version.
*       V01.20  JRR     16-Jan-1992	Added support for RS6000.
*       V01.3   JRR     28-Feb-1992	Added Cosmic V2 changes.
*       V01.4   JRR     07-Apr-1992     Added CERN enhancements.
*	V01.5	JRR	17-Jun-1992	Added header.
*	V01.6	JRR	11-Nov-1992	Tolerate included > 1x.
*	V01.7	JRR			Version 3.30
*	V01.8	JRR	05-Mar-1993	Added Boeing enhancement for files.
*	V01.9	JRR	02-Mar-1994	Fixed up IOBLKSIZ.
*/
/*++ nmapdb.h - nmap database internal structure
 *
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/libnmap/all-systems/nmapdb.h,v $
 *
 * DESCRIPTION:
 *
 * STANDARDS VIOLATIONS:
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: nmapdb.h,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:11  nqs
 * Imported sources
 *
 * Revision 1.9  1994/03/30  20:35:39  jrroma
 * Version 3.35.6
 *
 * Revision 1.8  93/07/13  21:33:16  jrroma
 * Version 3.34
 * 
 * Revision 1.7  92/12/22  15:52:02  jrroma
 * Version 3.30
 * 
 * Revision 1.6  92/06/18  14:39:20  jrroma
 * Added gnu header
 * 
 * Revision 1.5  92/05/06  10:25:50  jrroma
 * Version 3.20
 * 
 * Revision 1.4  92/03/20  10:56:57  jrroma
 * *** empty log message ***
 * 
 * Revision 1.3  92/02/28  13:20:40  jrroma
 * Added Cosmic V2 changes.
 * 
 * Revision 1.2  92/01/17  12:52:02  jrroma
 * Added support for RS6000.
 * 
 * Revision 1.1  92/01/17  12:51:27  jrroma
 * Initial revision
 * 
 *
 */

#ifndef _NQS_NMAPDBH_
#define _NQS_NMAPDBH_

#include <sys/types.h>
#include "ioblksiz.h"
/*
 *	Configurable #defs that can be tailored for an individual system.
 */
#define	MAX_PATHLENGTH	1024	/* The maximum length of a file pathname */

#define MACHINES	"machines"
				/* Name of machine to mid mapping file */

#define	MAX_NAMELENGTH	255	/* Maximum length of a machine name */
				/* The maximum length of a machine name */
				/* plus the associated structure overhead */
				/* (see name_map struct defn below) must */
				/* NOT exceed ATOMICBLKSIZ bytes in length! */

#ifndef ALIGNTYPE
#define	ALIGNTYPE	double	/* Alignment type */
#endif

#define	HASHNAMENTS	256	/* Number of machine-name to mid hash chains */

#define	HASHENTSPERBLK	(ATOMICBLKSIZ / sizeof (long))
				/* Number of entries per block for a hash */
				/* table. */

#define	HASHNAMBLKS	((HASHNAMENTS - 1) / HASHENTSPERBLK + 1)
				/* Number of blocks that must be allocated */
				/* for the machine-name to mid hash table */
#define MAX_MIDS        1000     /* Maximum number of machine id's in the */
                                /* network.  Used by nmap_show(). */
/*
 *	WARNING:
 *
 *	These relations may have to be changed for some compiler/machine
 *	implementations based upon any padding algorithms the compiler
 *	follows when declaring structures.
 */
#define	GIDHDRSIZ	(sizeof (struct gidmap8) - 8 * sizeof (gid_t))

#define	UIDHDRSIZ	(sizeof (struct uidmap8) - 8 * sizeof (uid_t))

#define	GIDSPERBLK	((ATOMICBLKSIZ - GIDHDRSIZ) / (2 * sizeof (gid_t)))
				/* The number of group-id mappings per block */

#define	UIDSPERBLK	((ATOMICBLKSIZ - UIDHDRSIZ) / (2 * sizeof (uid_t)))
				/* The number of user-id mappings per block */

struct xidparm {
	union {
		struct {
			uid_t remote_uid;
			uid_t local_uid;
		} u;
		struct {
			gid_t remote_gid;
			gid_t local_gid;
		} g;
	} u;
};


/* Data structures for database */

struct m_header {
	char name [MAX_NAMELENGTH+1];
				/* Null terminated machine name.	   */
	char defuid_enabled;	/* Default user-id mapping enabled.	   */
	char defgid_enabled;	/* Default group-id mapping enabled.	   */
	uid_t defuid;		/* Default user-id mapping value.	   */
	gid_t defgid;		/* Default group-id mapping value.	   */
	time_t enatime;		/* When to reenable mapping, 0=never, 1=on */
};

struct gidmap8 {		/* Only used for GIDSPERBLK computation	*/
	long next;		/* Addr of next block in hash chain	*/
	unsigned n_pairs;	/* There are this many gid to gid map	*/
				/* pairs in this block.			*/
	gid_t pairs [8];	/* Gid to gid pairs......		*/
};

struct gidmap {
	long next;		/* Addr of next block in hash chain	*/
	unsigned n_pairs;	/* There are this many gid to gid map	*/
				/* pairs in this block.			*/
	gid_t pairs [GIDSPERBLK];
};				/* Gid to gid pairs......		*/

struct uidmap8 {		/* Only used for UIDSPERBLK computation	*/
	long next;		/* Addr of next block in hash chain	*/
	unsigned n_pairs;	/* There are this many uid to uid map	*/
				/* pairs in this block.			*/
	uid_t pairs [8];	/* Uid to uid pairs......		*/
};

struct uidmap {
	long next;		/* Addr of next block in hash chain	*/
	unsigned n_pairs;	/* There are this many uid to uid map	*/
				/* pairs in this block.			*/
	uid_t pairs [UIDSPERBLK];
};				/* Uid to uid pairs......		*/

struct hashblk {
	long chain [HASHENTSPERBLK];	/* Hash chain pointers		*/
};

struct nammap {
	Mid_t	 mid;		/* Machine-id				*/
	unsigned short size;	/* Number of bytes contained in this	*/
				/* entry (even if NOT allocated!)	*/
	unsigned short namelen;	/* Length of machine-name.		*/
				/* Zero implies this entire entry is	*/
				/* not used at all.			*/
	char	 name [MAX_NAMELENGTH];
};				/* Name of machine.			*/

struct n_header {
	long nexthash;		/* Address of next block in hash chain	*/
	struct nammap map;	/* First mapping entry in the block	*/
};

struct nmap_rawblock {		/* Raw map file block properly aligned	*/
	union {
		ALIGNTYPE alignment;		/* Align the block	*/
		struct m_header mhdr;		/* Machine file hdr blk	*/
		struct uidmap umap;		/* User-id mapping blk	*/
		struct gidmap gmap;		/* Group-id mapping blk	*/
		struct hashblk hash;		/* Hash ptr block	*/
		struct n_header nhdr;		/* First nammap entry	*/
		char bytes [ATOMICBLKSIZ];	/* Raw block contents	*/
	} u;
};
#endif
