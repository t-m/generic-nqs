/*
 * libnmap/nmapcommon.h
 * 
 * DESCRIPTION:
 *	Nmapcommon.h:  Header file containing definitions for use with nmap
 */

#if	IS_POSIX_1 | IS_SYSVr4
#include <fcntl.h>
#else
#if	IS_BSD
#include <sys/file.h>
#else
BAD SYSTEM TYPE
#endif
#endif
#include <errno.h>
#include <netdb.h>		/* Berkeley network database */
#include <sys/types.h>
#include <libnmap/nmap.h>		/* Network mapping headers */
#include "ioblksiz.h"
#include <libnmap/nmapdb.h>
#define	CLOSE	close
#define	LSEEK	lseek
#define	OPEN	open
#define	READ	read
#define	WRITE	write

/*
 *	Mapping modes:
 */
#define	BY_NAM	0001		/* Request by name */
#define	BY_UID	0002		/* Request by user-id */
#define	BY_GID	0004		/* Request by group-id */
#define	ADD_OP	0010		/* Add mapping */
#define	DEL_OP	0020		/* Delete mapping */
#define	GET_OP	0040		/* New map request */
#define	ADD_UID	(BY_UID|ADD_OP)
#define	ADD_GID	(BY_GID|ADD_OP)
#define	ADD_NAM	(BY_NAM|ADD_OP)
#define	DEL_UID	(BY_UID|DEL_OP)
#define	DEL_GID	(BY_GID|DEL_OP)
#define	DEL_NAM	(BY_NAM|DEL_OP)
#define	GET_UID	(BY_UID|GET_OP)
#define	GET_GID	(BY_GID|GET_OP)
#define	GET_MID	(BY_NAM|GET_OP)

extern int name_mapfile;	/* File descriptor for machines		*/
				/* database: -1 if closed, > -1 if open.*/
extern int machine_file;	/* File descriptor for machine file	*/
				/* -1 if closed, > -1 if open.		*/
extern int nfds;		/* This variable determines the number	*/
				/* file descriptors that the network	*/
				/* mapping software will be allowed to	*/
				/* keep open between calls (0, 1, 2).	*/
