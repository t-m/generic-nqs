/*
 * qrls/qrls.c
 * 
 * DESCRIPTION:
 *
 *	Release a held request.
 *
 *	Original Author:
 *	-------
 *	Christian Boissat, CERN, Geneva, Switzerland.
 *	January 6, 1992.
 */

#define	MAX_REQS	100		/* Maximum number of reqs that */
					/* can be released at a time. */
#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <signal.h>			/* Signal definitions */
#include <netdb.h>			/* Network database header file; */
#include <libnqs/nqsdirs.h>		/* NQS files and directories */
#include <libnqs/transactcc.h>		/* Transaction completion codes */
#include <libsal/license.h>
#include <libsal/proto.h>
#include <unistd.h>
#include <SETUP/autoconf.h>

/*
 *	Global variables:
 */
char *Qrls_prefix = "Qrls";

/*** qrls_cleanup
 *
 *
 *	Catch certain signals, and delete the inter-process
 *	communication file we have been using.
 */
static void qrls_cleanup (int sig)
{
	signal (sig, SIG_IGN);		/* Ignore multiple signals */
	exiting();			/* Delete our comm. file */
}


/*** qrls_diagrel
 *
 *
 *	qrls_diagrel():
 *	Diagnose hdlreq() completion code.
 */
static void qrls_diagrel (long code, char *reqid)
{
	switch (code) {
	case TCML_INTERNERR:
	    printf ("Internal error.\n");
	    exiting();		/* Delete communication file */
	    exit (1);
	case TCML_NOESTABLSH:
	    printf ("Unable to establish inter-process communications ");
	    printf ("with NQS daemon.\n");
	    printf ("Seek staff support.\n");
	    exiting();		/* Delete communication file */
	    exit (1);
	case TCML_NOLOCALDAE:
	    printf ("The NQS daemon is not running.\n");
	    printf ("Seek staff support.\n");
	    exiting();		/* Delete communication file */
	    exit (1);
	case TCML_NOSUCHREQ:
	    printf ("Request %s does not exist.\n", reqid);
	    break;
	case TCML_NOTREQOWN:
	    printf ("Not owner of request %s.\n", reqid);
	    break;
	case TCML_PEERDEPART:
	    printf ("Request %s is presently being routed by a ", reqid);
	    printf ("pipe queue,\n");
	    printf ("and this NQS implementation does not support the\n");
	    printf ("handling of a request under such conditions.\n");
	    break;
	case TCML_PROTOFAIL:
	    printf ("Protocol failure in inter-process communications ");
	    printf ("with NQS daemon.\n");
	    printf ("Seek staff support.\n");
	    exiting();		/* Delete communication file */
	    exit (1);
	case TCML_COMPLETE:
	    printf ("Request %s has been released.\n", reqid);
	    break;
	case TCML_REQRUNNING:
	    printf ("Request %s is running.\n", reqid);
	    break;
	case TCML_REQNOTHELD:
	    printf ("Request %s is not on hold.\n", reqid);
	    break;
	default:
	    printf ("Unexpected completion code from NQS daemon.\n");
	    exiting();		/* Delete communication file */
	    exit (1);
    }
}


/*** qrls_showhow
 *
 *
 *	qrls_showhow():
 *	Show how to use this command.
 */
static void qrls_showhow(void)
{
    fprintf (stderr, "qrls -- release held NQS requests\n");
    fprintf (stderr, "usage:    qrls [-u username] [-v] <request-id(s)>\n");
    fprintf (stderr, " -u username   username of request owner (if not yourself)\n");
    fprintf (stderr, " -v            print version information\n");
    fprintf (stderr, " <request-id>  NQS request identifier\n");
    exit (0);
}

static void show_version(void)
{
    fprintf (stderr, "NQS version is %s.\n", NQS_VERSION);
}

/*** main
 *
 *
 *	qrls [ -u <username> ] <request-id(s)>
 */
int main (int argc, char *argv[])
{

	int n_reqs;			/* Number of reqs to release. */
	struct {
		long orig_seqno;	/* Sequence# for req */
		Mid_t machine_id;	/* Machine-id of originating machine */
		Mid_t target_mid;       /* target mid */
	} reqs [MAX_REQS];		/* Reqs to release */
	struct passwd *passwd;		/* Password structure ptr */
	char **scan_reqs;		/* Scan reqs */
	char *argument;			/* Ptr to cmd line arg text */
	uid_t real_uid;			/* Real user-id */
	Mid_t local_mid;		/* local machine-id */
	char *root_dir;                 /* Fully qualified file name */

  	sal_debug_InteractiveInit(1, NULL);
  
	/*
	 *  Catch 4 common household signals:  SIGINT, SIGQUIT,SIGHUP, 
	 *  and SIGTERM.  This is quite important, because we do not want
	 *  to leave useless inter-process communication files hanging
	 *  around in the NQS directory hierarchy.  However, if we do,
	 *  it is not fatal, it will eventually be cleaned up by NQS.
	 */
	signal (SIGINT, qrls_cleanup);
	signal (SIGQUIT, qrls_cleanup);
	signal (SIGHUP, qrls_cleanup);
	signal (SIGTERM, qrls_cleanup);
	if ( ! buildenv()) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qrls_prefix);
	    fprintf (stderr, "establish directory independent ");
	    fprintf (stderr, "environment.\n");
	    exit (1);
	}
	root_dir = getfilnam (Nqs_root, SPOOLDIR);
	if (root_dir == (char *)NULL) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qrls_prefix);
	    fprintf (stderr, "determine root directory name.\n");
	    exit (1);
	}
	if (chdir (root_dir) == -1) {
	    fprintf (stderr, "%s(FATAL): Unable to chdir() to the NQS ",
                 Qrls_prefix);
	    fprintf (stderr, "root directory.\n");
	    relfilnam (root_dir);
	    exit (1);
	}
	relfilnam (root_dir);
	/*
	 *  On systems with named pipes, we get a pipe to the local
	 *  daemon automatically the first time we call inter().
	 */
#if	HAS_BSD_PIPE
	if (interconn () < 0) {
	    fprintf (stderr, "%s(FATAL): Unable to get ", Qrls_prefix);
	    fprintf (stderr, "a pipe to the local daemon.\n");
	    exit (1);
	}
#endif
	passwd = NULL;			/* No -u flag seen */
	while (*++argv != NULL && **argv == '-') {
	    argument = *argv;
	    switch (*++argument) {
	        case 'u':		/* User-name specification */
		    if (*++argv == NULL) {
			fprintf (stderr, "Missing username.\n");
			exit (1);
		    }
		    if (passwd != NULL) {
			fprintf (stderr, "Multiple -u specifications.\n");
			exit (1);
		    }
		    if ((passwd = sal_fetchpwnam (*argv)) == NULL) {
			fprintf (stderr, "No such user on this ");
			fprintf (stderr, "machine.\n");
			exit (1);
		    }
		    if (localmid (&local_mid) != 0) {
			fprintf (stderr, "%s(FATAL): ", Qrls_prefix);
			fprintf (stderr, "Unable to get machine-id of local host.\n");
			exit(1);
		    }
		    if ((nqspriv (getuid(), local_mid, local_mid)
				& QMGR_OPER_PRIV) ||
			passwd->pw_uid == getuid()) {
			    /*
			     *  We have NQS operator privileges, or we
			     *  are just going after our own requests.
			     */
			    real_uid = passwd->pw_uid;
		    }
		    else {
			fprintf (stderr, "Insufficient privilege for -u specification.\n");
			exit (1);
		    }
		    break;
		case 'v':
		    show_version();
		    break;
		default:
		    fprintf (stderr, "Invalid option flag specified.\n");
		    qrls_showhow();
		    exit(1);
	     }
	}
	if (passwd == NULL) {
	    /*
	     *  No username specified.  We assume the invoker.
	     */
	    real_uid = getuid();		/* Get real user-id */
	}
	else sal_closepwdb();		/* Close account/password database */
	/*
	 *  Build the set of reqs to be released.
	 */
	if (*argv == NULL) {
	    /*
	     *  No request-ids were specified.
	     */
	    fprintf (stderr, "No request-id(s) specified.\n");
	    qrls_showhow();
	}
	else {
	    n_reqs = 0;			/* #of reqs to release */
	    scan_reqs = argv;		/* Set req scan pointer */
	    while (*scan_reqs != NULL && n_reqs < MAX_REQS) {
		switch (reqspec (*scan_reqs, &reqs [n_reqs].orig_seqno,
			 &reqs [n_reqs].machine_id,  &reqs [n_reqs].target_mid)) {
		case -1:
		    fprintf (stderr, "Invalid request-id syntax ");
		    fprintf (stderr, "for request-id: %s.\n", *scan_reqs);
		    exit (1);
		case -2:
		    fprintf (stderr, "Unknown machine for");
		    fprintf (stderr, "request-id: %s.\n", *scan_reqs);
		    exit (1);
		case -3:
		    fprintf (stderr, "Network mapping database ");
		    fprintf (stderr, "inaccessible.  Seek staff support.\n");
		    exit (1);
		case -4:
		    fprintf (stderr, "Network mapping database ");
		    fprintf (stderr, "error when parsing ");
		    fprintf (stderr, "request-id: %s.\n", *scan_reqs);
		    fprintf (stderr, "Seek staff support.\n");
		    exit (1);
	        }
	        /* If reqspec returns null in machine id, force to
	         * local machine id.
	         */
	        if (reqs[n_reqs].machine_id == 0) 
				localmid(&reqs [n_reqs].machine_id);
	        scan_reqs++;		/* One more req */
	        n_reqs++;
	    }
	    if (*scan_reqs != NULL) {
		/*
		 *  Too many reqs were specified to be released.
		 */
		fprintf (stderr, "Too many requests given to release.\n");
		exit (1);
	    }
	    /*
	     *  Now that everything has been parsed and legitimized,
	     *  release the specified set of requests.
	     */
	    n_reqs = 0;
	    while (*argv != NULL) {		/* Loop to release reqs */
		qrls_diagrel (hdlreq (1, real_uid, reqs [n_reqs].orig_seqno,
				 reqs [n_reqs].machine_id), *argv);
		argv++;			/* One more req */
		n_reqs++;
	    }
	}
	exiting();			/* Delete our comm. file */
	exit (0);
}

