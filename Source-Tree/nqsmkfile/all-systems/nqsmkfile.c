/*
 * nqsmkfile.c
 *		Helper utility to test whether a specified user can
 *		create a named file or not.
 *
 *		Part of Generic NQS v3.50.5 and later.
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1999 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Parameters:
 *
 *	$1	- uid to become
 *	$2	- gid to become
 *      $3      - operation to perform (mk or rm)
 *	$4	- file to try and create or delete
 *
 * Returns 0 on success, 1 otherwise.
 *
 * On success, the file $3 will exist, and will be empty.
 */

int main (int argc, char * argv[])
{
	FILE *fp = NULL;

	if (argc != 5)
	{
		exit(2);
	}

	if (setgid(atoi(argv[2])) != 0)
		exit(4);
	if (setuid(atoi(argv[1])) != 0)
		exit(3);

	if (strcmp(argv[3],"mk") == 0) {
	    if ((fp = fopen(argv[4], "a+")) == NULL)
		return 1;
	} else if (strcmp(argv[3],"rm") == 0) {
	    if (unlink(argv[4]) < 0)
		return 1;
	}
	return 0;
}
