/*
 * qdev/qdev.c
 * 
 * DESCRIPTION:
 *
 *	Display device status information.
 *	This program must run as a setuid root program.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 24, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsdirs.h>		/* NQS files and directories */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <SETUP/autoconf.h>

/*
 *	Global variables:
 */
char *Qdev_prefix = "Qdev";

/*** main
 *
 *
 *	qdev [ <device-name(s)> ]
 */
int main (int argc, char *argv[])
{
	struct confd *devicefile;
        int exitcode = 0;                       /* Function exit code */
	char *root_dir;                 /* Fully qualified file name */

  	sal_debug_InteractiveInit(1, NULL);
  
	if ( ! buildenv()) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qdev_prefix);
	    fprintf (stderr, "establish directory independent ");
	    fprintf (stderr, "environment.\n");
	    exit (1);
	}

	root_dir = getfilnam (Nqs_root, SPOOLDIR);
	if (root_dir == (char *)NULL) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qdev_prefix);
	    fprintf (stderr, "determine root directory name.\n");
	    exit (1);
	}
	if (chdir (root_dir) == -1) {
	    fprintf (stderr, "%s(FATAL): Unable to chdir() to the NQS ",
                 Qdev_prefix);
	    fprintf (stderr, "root directory.\n");
	    relfilnam (root_dir);
	    exit (1);
	}
	relfilnam (root_dir);
	if ((devicefile = opendb (Nqs_devices, O_RDONLY)) == NULL) {
		fprintf (stderr, "%s(FATAL): Unable to open the NQS device ",
			 Qdev_prefix);
		fprintf (stderr, "definition database file.\n");
		exit (1);
	}
	/*
	 *  Block buffer stdout for efficiency.
	 */
	bufstdout();
        if (*++argv == NULL)  exitcode |= shoalldev (devicefile);
	else {
		while (*argv != NULL) {		/* Show info on a device */
                        exitcode |= shodbyname (devicefile, *argv);
			argv++;
		}
	}
	/*
	 *  Flush output buffers and exit.
	 */
	fflush (stdout);
	fflush (stderr);
	exit (exitcode);
}
