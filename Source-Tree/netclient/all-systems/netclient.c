/*
 * netclient/netclient.c
 * 
 * DESCRIPTION:
 *
 *	Default NQS network client.  Empties network queues.
 *	Returns stdout, stderr, and stage-out files.
 *
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	April 22, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdlib.h>
#include <grp.h>
#include <limits.h>
#include <signal.h>			/* For SIGPIPE, SIGTERM */
#include <string.h>
#include <libnqs/netpacket.h>			/* Network packet types */
#include <libnqs/informcc.h>			/* Information completion codes */
#include <libnqs/requestcc.h>			/* Request completion codes */
#include <libnqs/transactcc.h>			/* Transaction completon codes */
#include <libnqs/nqsdirs.h>			/* For nqslib library functions */
#include <errno.h>			/* Error numbers */
#if	IS_IBMRS
#include <sys/mode.h>
#endif

#include <unistd.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <SETUP/General.h>

static int copy ( char *temp, char *dest, int mode );
static void descravail ( void );
static long do_delivery ( struct rawreq *rawreq, uid_t cuid, char *cusername );
static long do_stageout ( struct rawreq *rawreq, uid_t cuid, char *cusername );
static long do_stderr ( struct rawreq *rawreq, struct passwd *passwd );
static long do_stdout ( struct rawreq *rawreq, struct passwd *passwd );
static void inodeavail ( void );
static void lostsink ( int );
static long rightrcm ( long remotercm, long localrcm );
static int robustopen ( char *path, int flags, int mode );
static long tryhere ( char *temp, char *dest, struct stat *statbuf, int mode );
static long trythere ( char *tempname, char *dest, Mid_t dest_mid, struct stat *statbuf, struct passwd *passwd );
static void waitdescr ( void );
static void waitinode ( void );

/*
 * Variables global to this module.
 */
static Mid_t Locmid; 

/*** main
 *
 *
 *	main():
 *
 *	This process is exec'd by the child of a shepherd process (child of
 *	the NQS daemon) with the following file descriptors open as described:
 *
 *		File descriptor 0: Control file (O_RDWR).
 *		File descriptor 1: syslog debugging using libsal
 *		File descriptor 2: Unused.
 *		File descriptor 3: Write file descriptor for serexit()
 *				   back to the shepherd process.
 *		File descriptor 4: Connected to the local NQS daemon
 *				   request FIFO pipe.
 *		File descriptors [5.._NFILE] are closed.
 *
 *
 *	At this time, this process is running with a real AND effective
 *	user-id of root!
 *
 *
 *	The current working directory of this process is the NQS
 *	root directory.
 *
 *
 *	All signal actions are set to SIG_DFL.
 *
 *
 *	The environment of this process contains the environment string:
 *
 *		DEBUG=n
 *
 *	where n specifies (in integer ASCII decimal format), the debug
 *	level in effect when this client was exec'd over the child of
 *	an NQS shepherd.
 *
 *
 *	The user upon whose behalf this work is being performed (the
 *	transaction owner), is identified by the environment variables:
 *
 *		UID=n
 *		USERNAME=username-of-transaction-owner
 *
 *	where n specifies (in integer ASCII decimal format), the integer
 *	user-id of the transaction owner.
 * 
 *
 *	For System V based implementations of NQS, the environment
 *	variable:
 *
 *		TZ=timezonename
 *
 *	will also be present.  Berkeley based implementations of NQS will
 *	not contain this environment variable.
 *
 *
 *	The environment of this process also contains the default retry
 *	state tolerance time, the default retry wait time, and the amount
 *	of time that the network destination has been in a retry mode
 *	(all times are in seconds):
 *
 *		DEFAULT_RETRYWAIT=n
 *		DEFAULT_RETRYTIME=n
 *		ELAPSED_RETRYTIME=n
 *
 *	where n is an ASCII decimal format long integer number.
 *
 *
 *	The environment variable of:
 *
 *		OP=opstring
 *
 *	defines the operation that is to be performed by this instance of
 *	the network queue server.
 *
 *		OP=D	-- deliver this request to its destination;
 *		OP=O	-- perform a stage-out file hierarchy event;
 *
 *	In the OP=O case, the additional environment variable of:
 *
 *		EVENT=n
 *
 *	is also present with n indicating the file staging event
 *	number [0..31] (see ../lib/transact.c).
 *
 */
int main (int argc, char *argv[], char *envp[])
{

	struct rawreq rawreq;	/* To hold the control file header */
	char *cp;		/* Ascii value of environmental var */
	int cuid;		/* Client's user id */
	char *cusername;	/* Client's username */

  	/*
	 * initialiase the debugging support.  On a fatal message, we
	 * rely on the libsal to provide default behaviour.
	 */
  
  	sal_debug_Init(1, "NQS Network Client", NULL);
  
	/*
	 *  On some UNIX implementations, stdio functions alter errno to
	 *  ENOTTY when they first do something where the file descriptor
	 *  corresponding to the stream happens to be a pipe (which is
	 *  the case here).
	 *
	 *  The fflush() calls are added to get this behavior out of the
	 *  way, since bugs have occurred in the past when a server ran
	 *  into difficultly, and printed out an errno value in situations
	 *  where the diagnostic printf() displaying errno occurred AFTER
	 *  the first stdio function call invoked.
	 */
	if (getuid() != 0 || geteuid() != 0) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netclient: uid or euid not root.\n");
		exit (1);
	}
	/*
	 * SIGTERM is sent to all NQS processes upon NQS shutdown.
	 * Ignore it.
	 */
	signal (SIGTERM, SIG_IGN);
	if ((cp = getenv ("DEBUG")) != (char *) 0)
  	  sal_debug_SetLevel(atoi(cp));
  
	interset (4);		/* Use this file descriptor to communicate */
				/* with the local NQS daemon */
	
	while (*envp != NULL) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Testing netclient envp: %s\n", *envp);
		envp++;
	}
	if (readreq (0, &rawreq) != 0) {
		serexit (RCM_BADCDTFIL, (char *) 0);
	}
	if ((cp = getenv ("UID")) == (char *) 0) {
		serexit (RCM_BADSRVARG, (char *) 0);
	}
	cuid = atoi (cp); 
	if ((cusername = getenv ("USERNAME")) == (char *) 0) {
		serexit (RCM_BADSRVARG, (char *) 0);
	}
	/*
	 * Which operation are we to perform?
	 */
	if ((cp = getenv ("OP")) == (char *) 0) {
		serexit (RCM_BADSRVARG, (char *) 0);
	}
	switch (*cp) {
	case 'D':
		if (localmid (&Locmid) != 0) {
			serexit (RCM_UNAFAILURE, (char *) 0);
		}
		serexit (do_delivery (&rawreq, cuid, cusername), (char *) 0);
		break;
	case 'O':
		if (localmid (&Locmid) != 0) {
			serexit (RCM_UNAFAILURE, (char *) 0);
		}
		serexit (do_stageout (&rawreq, cuid, cusername), (char *) 0);
		break;
	default:
		serexit (RCM_BADSRVARG, (char *) 0);/* Exit */
		break;
	}
	return 0;  
}


/*** do_delivery
 *
 *
 *	long do_delivery():
 *
 *	Delivery of requests across machines will soon be done using
 *	the one-two punch of pipe queues and network queues.
 *	This routine empties network queues.
 */
static long do_delivery (
	struct rawreq *rawreq,
	uid_t cuid,
	char *cusername)
{
	return (mergertcm (RCM_DELIVERFAI, TCML_FATALABORT));
}


/*** do_stageout
 *
 *
 *	long do_stageout():
 *
 *	A batch request has spooled an output file.  Move that file
 *	from the output spooling area to a reasonable final location,
 *	possibly on another machine.
 */
static long do_stageout (
	struct rawreq *rawreq,
	uid_t cuid,
	char *cusername)
{

	char *asciievent;		/* Ascii val of env var */
	struct passwd *passwd;		/* Pointer to password info */

	signal (SIGPIPE, lostsink);
	if ((asciievent = getenv ("EVENT")) == (char *) 0) {
		return (RCM_BADSRVARG);
	}
	/*
	 * It will be nice to know the user's home directory
	 */
	if ((passwd = sal_fetchpwuid (cuid)) == (struct passwd *) 0) {
		return (mergertcm (RCM_STAGEOUTFAI, TCML_UNAFAILURE));
	}
	switch (atoi (asciievent)) {
	case 30:
		return (do_stdout (rawreq, passwd));
	case 31:
		return (do_stderr (rawreq, passwd));
	default:
		/*
		 * Stage-out events 0-29 are not supported yet.
		 */
		return (mergertcm (RCM_STAGEOUTFAI, TCML_FATALABORT));
	}
}


/*** do_stdout
 *
 *
 *	long do_stdout():
 *
 *	Stdout has already been spooled to a regular file.  Move that
 *	file from the output spooling area to a reasonable final
 *	location, possibly on another machine.
 *	Returns: RCM_STAGEOUT (might have information bits)
 *		 RCM_STAGEOUTFAI (will have information bits)
 */
static long do_stdout (
	struct rawreq *rawreq,
	struct passwd *passwd)
{
	char *cp;				/* Character pointer */
	char *tempname;				/* Spooled file pathname */
	struct stat statbuf;			/* To hold stat() output */
	char path [MAX_REQPATH + 256 + 1];	/* 256 for home dir */
	char last_chance_path [MAX_REQPATH + 256 + 1];	/* 256 for home dir */
	long primaryrcm;			/* Rcm from first try */
	long secondaryrcm;			/* Rcm from second try */
	int ngroups;				/* size of group set */
	gid_t gidset[NGROUPS_MAX];		/* group set */

	if (rawreq->v.bat.stdout_acc & OMD_M_KEEP) {
		rawreq->v.bat.stdout_mid = Locmid;
	}
	tempname = namstdout (rawreq->orig_seqno, rawreq->orig_mid);
	if (stat (tempname, &statbuf) == -1) {
		/*
		 * Assume that a previous attempt worked.
		 */
		return (RCM_STAGEOUT);
	}
	/*
	 * Make sure last chance directory exists for this user.
	 * The directory is: /usr/spool/nqs/dump/<username>
	 */
	sprintf(path, "%s/%s", NQS_SPOOL, Nqs_dump);
	mkdir (path, 0777);
	sprintf(last_chance_path, "%s/%s", path, passwd->pw_name);
	if ( mkdir (last_chance_path, 0700) == -1) {
		if (errno != EEXIST) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netclient: unable to make directory %s for stdout, errno = %d\n", last_chance_path, errno); 
		}
	}
	if (chown (last_chance_path, passwd->pw_uid, passwd->pw_gid) == -1) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netclient: unable to change owner of %s for stdout, errno = %d\n", last_chance_path, errno);
		return (mergertcm (RCM_STAGEOUTFAI, TCML_UNAFAILURE));
	}
	/*
	 * Assume that any previous attempts failed.
	 */
	if (rawreq->v.bat.stdout_mid == Locmid) {
		/*
		 * Lower privileges.
		 */
		initgroups (passwd->pw_name, passwd->pw_gid);
		ngroups = getgroups (NGROUPS_MAX, gidset);
		setgroups (ngroups, gidset);
		setgid (passwd->pw_gid);
		setuid (passwd->pw_uid);
		if (rawreq->v.bat.stdout_name [0] != '/') {
			sprintf (path, "%s/%s", passwd->pw_dir,
				rawreq->v.bat.stdout_name);
		}
		else strcpy (path, rawreq->v.bat.stdout_name);
		/*
		 * The following check handles the case of
		 * a bad password file home directory.
		 */
		if (path [0] != '/') {
			return (mergertcm (RCM_STAGEOUTFAI, TCML_UNAFAILURE));
		}
		primaryrcm = tryhere (tempname, path, &statbuf,
			(~rawreq->v.bat.umask & 0666));
		if ((primaryrcm & XCI_REASON_MASK) == RCM_STAGEOUTFAI) {
			/*
			 * We cannot return it to the primary location.
			 * Try to return it to a backup location
			 * on the execution machine.
			 */
			mkdefault (path, passwd->pw_dir, rawreq->reqname,
				rawreq->orig_seqno, 'o');
			/*
			 * Try the secondary location.  If we can't get it
			 * there, then try the last chance location.
			 */
			secondaryrcm =	tryhere (tempname, path, &statbuf,
					(~rawreq->v.bat.umask & 0666));
			if ((secondaryrcm & XCI_REASON_MASK) 
							!= RCM_STAGEOUTFAI)
				return (rightrcm (primaryrcm,secondaryrcm));
			/*
			 * Hmm, neither of those worked, lets try the last
			 * chance location:  /usr/spool/nqs/dump/<username>
			 */
			cp = strrchr(path, '/');
			cp++;
			sprintf(last_chance_path, "%s/%s/%s/%s", 
				NQS_SPOOL, Nqs_dump, passwd->pw_name, cp);
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netclient: last chance trying %s\n", last_chance_path);
			secondaryrcm = tryhere (tempname, last_chance_path, 
					&statbuf,
					(~rawreq->v.bat.umask & 0666));
			return (mergertcm (RCM_STAGEOUTLCH, TCML_UNAFAILURE));

		}
		else return (primaryrcm);
	}
	else {
		/*
		 * Try to return this file across the net.
		 */
		primaryrcm = trythere (tempname, rawreq->v.bat.stdout_name,
			rawreq->v.bat.stdout_mid, &statbuf, passwd);
		if ( (primaryrcm & XCI_REASON_MASK) == RCM_STAGEOUT) {
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM,"Netclient: trythere(stdout) succeeded\n");
		}
		else  tcmmsgs (primaryrcm, SAL_DEBUG_MSG_WARNING,
				"Netclient: trythere(stdout) ");
		if ((primaryrcm & XCI_REASON_MASK) == RCM_STAGEOUTFAI) {
			/*
			 * We cannot return it across the net.
			 * Try to return it to a backup location
			 * on the execution machine.
			 * First, lower privileges.
			 */
			initgroups (passwd->pw_name, passwd->pw_gid);
			ngroups = getgroups (NGROUPS_MAX, gidset);
			setgroups (ngroups, gidset);
			setgid (passwd->pw_gid);
			setuid (passwd->pw_uid);
			mkdefault (path, passwd->pw_dir,
				rawreq->reqname, rawreq->orig_seqno, 'o');
			/*
			 * Try the secondary location.  If we can't get it
			 * there, then try the last chance location.
			 */
			secondaryrcm =	tryhere (tempname, path, &statbuf,
					(~rawreq->v.bat.umask & 0666));
			if ((secondaryrcm & XCI_REASON_MASK) 
							!= RCM_STAGEOUTFAI)
				return (rightrcm (primaryrcm,secondaryrcm));
			/*
			 * Hmm, neither of those worked, lets try the last
			 * chance location:  /usr/spool/nqs/dump/<username>
			 */
			cp = strrchr(path, '/');
			cp++;
			sprintf(last_chance_path, "%s/%s/%s/%s", 
				NQS_SPOOL, Nqs_dump, passwd->pw_name, cp);
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING,"Netclient: last chance trying %s\n", last_chance_path);
			secondaryrcm = tryhere (tempname, last_chance_path, 
					&statbuf,
					(~rawreq->v.bat.umask & 0666));
			return (mergertcm (RCM_STAGEOUTLCH, TCML_UNAFAILURE));

		}
		else return (primaryrcm);
	}
}


/*** do_stderr
 *
 *
 *	long do_stderr():
 *
 *	Stderr has already been spooled to a regular file.  Move that
 *	file from the output spooling area to a reasonable final
 *	location, possibly on another machine.
 *	Returns: RCM_STAGEOUT (might have information bits)
 *		 RCM_STAGEOUTFAI (will have information bits)
 *
 */
static long do_stderr (
	struct rawreq *rawreq,
	struct passwd *passwd)
{
	
	char *cp;				/* Character pointer */
	char *tempname;				/* Spooled file pathname */
	struct stat statbuf;			/* To hold stat() output */
	char path [MAX_REQPATH + 256 + 1];	/* 256 for home dir */
	char last_chance_path [MAX_REQPATH + 256 + 1];	/* 256 for home dir */
	long primaryrcm;			/* Rcm from first try */
	long secondaryrcm;			/* Rcm from second try */
	int ngroups;				/* size of group set */
	gid_t gidset[NGROUPS_MAX];		/* group set */

	if (rawreq->v.bat.stderr_acc & OMD_M_KEEP) {
		rawreq->v.bat.stderr_mid = Locmid;
	}
	tempname = namstderr (rawreq->orig_seqno, rawreq->orig_mid);
	if (stat (tempname, &statbuf) == -1) {
		/*
		 * Assume that a previous attempt worked.
		 */
		return (RCM_STAGEOUT);
	}
	sprintf (path, "%s/%s", NQS_SPOOL, Nqs_dump);
	mkdir (path, 0777);
	sprintf(last_chance_path, "%s/%s", path, passwd->pw_name);
	if (mkdir(last_chance_path, 0700) == -1) {
		if (errno != EEXIST) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netclient: unable to create directory %s for stderr, errno = %d\n", last_chance_path, errno);
			return (mergertcm (RCM_STAGEOUTFAI, TCML_UNAFAILURE));
		}
	}
	if (chown (last_chance_path, passwd->pw_uid, passwd->pw_gid) == -1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netclient: unable to change owner of %s for stderr, errno = %d\n", last_chance_path, errno);
		return (mergertcm (RCM_STAGEOUTFAI, TCML_UNAFAILURE));
	}
	/*
	 * Assume that any previous attempts failed.
	 */
	if (rawreq->v.bat.stderr_mid == Locmid) {
		/*
		 * Lower privileges.
		 */
		initgroups (passwd->pw_name, passwd->pw_gid);
		ngroups = getgroups (NGROUPS_MAX, gidset);
		setgroups (ngroups, gidset);
		setgid (passwd->pw_gid);
		setuid (passwd->pw_uid);
		if (rawreq->v.bat.stderr_name [0] != '/') {
			sprintf (path, "%s/%s", passwd->pw_dir,
				rawreq->v.bat.stderr_name);
		}
		else strcpy (path, rawreq->v.bat.stderr_name);
		/*
		 * The following check handles the case of
		 * a bad password file home directory.
		 */
		if (path [0] != '/') {
			return (mergertcm (RCM_STAGEOUTFAI, TCML_UNAFAILURE));
		}
		primaryrcm = tryhere (tempname, path, &statbuf,
			(~rawreq->v.bat.umask & 0666));
		if ((primaryrcm & XCI_REASON_MASK) == RCM_STAGEOUTFAI) {
			/*
			 * We cannot return it to the primary location.
			 * Try to return it to a backup location
			 * on the execution machine.
			 */
			mkdefault (path, passwd->pw_dir, rawreq->reqname,
				rawreq->orig_seqno, 'e');
			/*
			 * Try the secondary location.  If we can't get it
			 * there, then try the last chance location.
			 */
			secondaryrcm =	tryhere (tempname, path, &statbuf,
					(~rawreq->v.bat.umask & 0666));
			if ((secondaryrcm & XCI_REASON_MASK) 
							!= RCM_STAGEOUTFAI)
				return (rightrcm (primaryrcm,secondaryrcm));
			/*
			 * Hmm, neither of those worked, lets try the last
			 * chance location:  /usr/spool/nqs/dump/<username>
			 */
			cp = strrchr(path, '/');
			cp++;
			sprintf(last_chance_path, "%s/%s/%s/%s", 
				NQS_SPOOL, Nqs_dump, passwd->pw_name, cp);
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING,"Netclient: last chance trying %s\n", last_chance_path);
			secondaryrcm = tryhere (tempname, last_chance_path, 
					&statbuf,
					(~rawreq->v.bat.umask & 0666));
			return (mergertcm (RCM_STAGEOUTLCH, TCML_UNAFAILURE));

		}
		else return (primaryrcm);
	}
	else {
		/*
		 * Try to return this file across the net.
		 */
		primaryrcm = trythere (tempname, rawreq->v.bat.stderr_name,
			rawreq->v.bat.stderr_mid, &statbuf, passwd);
		if ( (primaryrcm & XCI_REASON_MASK) == RCM_STAGEOUT) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netclient: trythere(stderr) succeeded\n");
		} else  tcmmsgs( primaryrcm, SAL_DEBUG_MSG_WARNING, "Netclient: trythere(stderr) ");
		if ((primaryrcm & XCI_REASON_MASK) == RCM_STAGEOUTFAI) {
			/*
			 * We cannot return it across the net.
			 * Try to return it to a backup location
			 * on the execution machine.
			 * First, lower privileges.
			 */
			initgroups (passwd->pw_name, passwd->pw_gid);
			ngroups = getgroups (NGROUPS_MAX, gidset);
			setgroups (ngroups, gidset);
			setgid (passwd->pw_gid);
			setuid (passwd->pw_uid);
			mkdefault (path, passwd->pw_dir,
				rawreq->reqname, rawreq->orig_seqno, 'e');
			/*
			 * Try the secondary location.  If we can't get it
			 * there, then try the last chance location.
			 */
			secondaryrcm =	tryhere (tempname, path, &statbuf,
					(~rawreq->v.bat.umask & 0666));
			if ((secondaryrcm & XCI_REASON_MASK) 
							!= RCM_STAGEOUTFAI)
				return (rightrcm (primaryrcm,secondaryrcm));
			/*
			 * Hmm, neither of those worked, lets try the last
			 * chance location:  $(NQS_SPOOL)/dump/<username>
			 */
			cp = strrchr(path, '/');
			cp++;
			sprintf(last_chance_path, "%s/%s/%s/%s", 
				NQS_SPOOL, Nqs_dump, passwd->pw_name, cp);
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netclient: last chance trying %s\n", last_chance_path);
			secondaryrcm = tryhere (tempname, last_chance_path, 
					&statbuf,
					(~rawreq->v.bat.umask & 0666));
			return (mergertcm (RCM_STAGEOUTLCH, TCML_UNAFAILURE));

		}
		else return (primaryrcm);
	}
}


/*** rightrcm
 *
 *
 *	long rightrcm ():
 *	Return the correct request completion message.
 *
 *	Given that return to the primary location failed,
 *	this function allows us to report one to the following:
 *
 *	1) Successful return to the backup location,
 *		 and why the primary location failed, or
 *	2) Unsuccessful, and why the primary location failed
 *
 *	instead of one of the following:
 *
 *	1) Successful return (neglecting to mention that
 *		it was to the backup location), or
 *	2) Unsuccessful, and why the backup location failed
 *		(never mentioning why return to the primary location failed)
 *
 */
static long rightrcm (long remotercm, long localrcm)
{
	if ((localrcm & XCI_REASON_MASK) == RCM_STAGEOUT) {
		return (RCM_STAGEOUTBAK | (remotercm & XCI_INFORM_MASK));
	}
	else return remotercm;
}

/*** trythere
 *
 *
 *	long trythere ():
 *
 *	Return an output file to a remote machine.
 *	If successful, return RCM_STAGEOUT and unlink tempname.
 *	If unsuccessful, return RCM_STAGEOUTFAI and do not unlink.
 */
static long trythere (
	char *tempname,			/* Where we spooled it */
	char *dest,			/* Path on remote machine */
	Mid_t dest_mid,			/* Nmap mid for remote machine */
	struct stat *statbuf,		/* Stat of spooled copy */
	struct passwd *passwd)		/* Password on our machine */
{
	
	int fd;
	int sd;
	long transactcc;
	short timeout;			/* Seconds in between tries */
	int integers;
	int strings;
	char packet [MAX_PACKET];
	int packetsize;
	long tcm;

	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netclient: trythere tempname: %s\n", tempname);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netclient: trythere dest: %s\n", dest);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netclient: trythere dest_mid: %lu\n", dest_mid);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netclient: trythere size: %d\n", statbuf->st_size);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netclient: trythere user: %s\n", passwd->pw_name);
	if ((fd = robustopen (tempname, O_RDONLY, 0)) == -1) {
		return (mergertcm (RCM_STAGEOUTFAI, errnototcm ()));
	}
	interclear ();
	sd = establish (NPK_MVOUTFILE, dest_mid, passwd->pw_uid,
		passwd->pw_name, &transactcc);
	/*
	 * Establish has just told the network server
	 * on the remote machine what we want.
	 */
	if (sd < 0) {
	    if (sd == -2) {
		/*
		 * Retry is in order.
		 */
		timeout = 1;
		do {
		    nqssleep (timeout);
		    interclear ();
		    sd = establish (NPK_MVOUTFILE, dest_mid,
			passwd->pw_uid, passwd->pw_name,
			&transactcc);
		    timeout *= 2;
		} while (sd == -2 && timeout <= 16);
		/*
		 * Beyond this point, give up on retry.
		 */
		if (sd < 0) {
		    return (mergertcm (RCM_STAGEOUTFAI,	transactcc));
		}
	    }
	    else {
		/*
		 * Retry would surely fail.
		 */
		return (mergertcm (RCM_STAGEOUTFAI, transactcc));
	    }
	}
	/*
	 * The server liked us.
	 */
	interclear();
	interw32i ((long) NPK_MVOUTFILE);
	interw32i ((long) statbuf->st_mode);
	interw32i ((long) statbuf->st_size);
	interwstr (dest);
	if ((packetsize = interfmt (packet)) == -1) {
	    return (mergertcm (RCM_STAGEOUTFAI, TCML_INTERNERR));
	}
	if (write (sd, packet, packetsize) != packetsize) {
	    return (mergertcm (RCM_STAGEOUTFAI, TCMP_CONNBROKEN));
	}
	setsockfd (sd);
	switch (interread (getsockch)) {
	case 0:
	    break;
	case -1:
	    return (mergertcm (RCM_STAGEOUTFAI, TCMP_CONNBROKEN));
	case -2:
	    return (mergertcm (RCM_STAGEOUTFAI, TCML_PROTOFAIL));
	}
	integers = intern32i();
	strings = internstr();
	if (integers == 1 && strings == 0) {
	    if ((tcm = interr32i (1)) != TCMP_CONTINUE) {
		return (mergertcm (RCM_STAGEOUTFAI, tcm));
	    }
	}
	else return (mergertcm (RCM_STAGEOUTFAI, TCML_PROTOFAIL));
	if (filecopyentire (fd, sd) != statbuf->st_size) {
	    return (mergertcm (RCM_STAGEOUTFAI, TCMP_CONNBROKEN));
	}
	interclear();
	interw32i ((long) NPK_DONE);
	interw32i ((long) 0);
	interw32i ((long) 0);
	interwstr ("");
	if ((packetsize = interfmt (packet)) == -1) {
	    return (mergertcm (RCM_STAGEOUTFAI, TCML_INTERNERR));
	}
	if (write (sd, packet, packetsize) != packetsize) {
	    return (mergertcm (RCM_STAGEOUTFAI, TCMP_CONNBROKEN));
	}
	/*
	 * We think the transfer was successful.
	 */
	switch (interread (getsockch)) {
	case 0:
		break;
	case -1:
		return (mergertcm (RCM_STAGEOUTFAI, TCMP_CONNBROKEN));
	case -2:
		return (mergertcm (RCM_STAGEOUTFAI, TCML_PROTOFAIL));
	}
	integers = intern32i();
	strings = internstr();
	if (integers == 1 && strings == 0) {
	    if ((tcm = interr32i(1)) == TCMP_COMPLETE) {
		unlink (tempname);
		return (RCM_STAGEOUT);
	    } else {
		fflush(stdout);
	        return (mergertcm (RCM_STAGEOUTFAI, tcm));
	    }
	}
	else return (mergertcm (RCM_STAGEOUTFAI, TCML_PROTOFAIL));
}


/*** tryhere
 *
 *
 *	long tryhere ():
 *
 *	Return an output file to the local machine.
 *	If successful, return RCM_STAGEOUT and unlink tempname.
 *	If unsuccessful, return RCM_STAGEOUTFAI and do not unlink.
 */
static long tryhere (
	char *temp,			/* Temporary name */
	char *dest,			/* Destination name */
	struct stat *statbuf,		/* Result of stat() on temp */
	int mode)			/* Destination file mode */
{

	short waitmsg;			/* Wait message flag */
	short linkstatus;		/* Result of link() call */

	if (statbuf->st_nlink != 1) {
		/*
		 *  The output process has already been completed by
		 *  linking the final file to the temporary.  We just
		 *  have not gotten around to deleting the temporary
		 *  file just yet.
		 */
		unlink (temp);
		return (RCM_STAGEOUT);
	}
	waitmsg = 0;				/* No lack of inodes yet */
	while ((linkstatus = link (temp, dest)) == -1 && errno == ENOSPC) {
		if (!waitmsg) {
			waitmsg = 1;
			waitinode();		/* Write log mssg that we are */
		}				/* waiting.... */
		nqssleep (RESOURCE_WAIT);	/* Sleep for a while */
	}
	if (waitmsg) inodeavail();		/* I-node became available */
	if (linkstatus == 0) {
		/*
		 *  The simple link case succeeded.
		 */
		chmod (dest, mode);		/* Update mode */
		unlink (temp);			/* Remove temp file */
		return (RCM_STAGEOUT);		/* Complete success */
	}
	/*
	 *  The link failed.  Try copying the file.
	 */
	if (copy (temp, dest, mode) == 0) {
		/*
		 *  File copy successful.
		 */
		unlink (temp);			/* Remove temp file */
		return (RCM_STAGEOUT);		/* Complete success */
	}
	return (mergertcm (RCM_STAGEOUTFAI, errnototcm ()));
}


/*** copy
 *
 *
 *	int copy():
 *	Copy temp output file to specified destination file.
 *
 *	Returns:
 *		0: if successful.
 *	       -1: if unsuccessful.
 */
static int copy (
	char *temp,		/* Name of temporary file */
	char *dest,		/* Name of destination file */
	int mode)		/* File creation mode */
{

	register int srcfd;
	register int desfd;

	if ((srcfd = robustopen (temp, O_RDONLY, 0)) == -1) {
		return (-1);
	}
	if ((desfd = robustopen (dest, O_WRONLY|O_CREAT|O_TRUNC, mode)) == -1) {
		return (-1);
	}
	/*
	 *  Copy the output file.
	 */
	if (filecopyentire (srcfd, desfd) == -1) {
		/*
		 *  The file copy failed.
		 */
		unlink (dest);		/* Unlink destination */
		return (-1);
	}
	/*
	 *  The file copy succeeded!
	 */
	return (0);			/* Success */
}


/*** robustopen
 *
 *
 *	int robustopen ():
 *
 *	Behave just as the open(2) system call does, only
 *	try to turn failure into success.
 */
static int robustopen (
	char *path,		/* Path name */
	int flags,		/* O_??? */
	int mode)		/* Rwx permissions, not */
				/* used if we only read */
{
	
	short descr_wait;			/* Boolean */
	short inode_wait;			/* Boolean */
	int fd;					/* File descriptor */

	/*
	 *  If the system file table is full, or no free i-nodes exist,
	 *  then we wait, and wait, and wait...........
	 */
	descr_wait = 0;
	inode_wait = 0;
	/*
	 * C allows us to pass the wrong number of args to open().
	 */
	while (
		((fd = open (path, flags, mode)) == -1) &&
		(errno == ENFILE || errno == EINTR || errno == ENOSPC)
		) {
		if (errno != EINTR) {
			if (errno == ENFILE) {
				if (inode_wait) {
					inode_wait = 0;
					inodeavail ();
				}
				if (!descr_wait) {
					descr_wait = 1;
					waitdescr ();
				}
			}
			else {
				if (descr_wait) {
					descr_wait = 0;
					descravail ();
				}
				if (!inode_wait) {
					inode_wait = 1;
					waitinode ();
				}
			}
			nqssleep (RESOURCE_WAIT);
		}
	}
	return (fd);
}


/*** descravail
 *
 *
 *	void descravail():
 *	Write-out file-descriptor has become available message.
 */
static void descravail()
{
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "File descriptor has become available.\nProcess %1d is no longer blocked.\n", getpid());
}


/*** inodeavail
 *
 *
 *	void inodeavail():
 *	Write-out i-node has become available message.
 */
static void inodeavail()
{
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Inode has become available.\nProcess %1d is no longer blocked.\n", getpid());
}


/*** waitdescr
 *
 *
 *	void waitdescr():
 *	Write-out waiting for file descriptor message.
 */
static void waitdescr()
{
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Process %1d waiting for system file table overflow\nactivity to cease in order to return output file.\n", getpid());
}


/*** waitinode
 *
 *
 *	void waitinode():
 *	Write-out waiting for available i-node message.
 */
static void waitinode()
{
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Process %1d waiting for free i-node to become available in order to return output file.\n", getpid());
}


/*** lostsink
 *
 *
 *	void lostsink():
 *	Called upon receiving SIGPIPE.
 */
static void lostsink( int unused )
{
	signal (SIGPIPE, lostsink);
	serexit (mergertcm (RCM_STAGEOUTFAI, errnototcm ()), (char *) 0);
				/* was EPIPE */
}

