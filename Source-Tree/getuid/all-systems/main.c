/*
 *
 * getuid/all-systems/main.c
 * Echo current user-id value to stdout
 *
 * Author:
 * Stuart Herbert
 * S.Herbert@sheffield.ac.uk
 */

#include <stdio.h>
#include <unistd.h>

int main(void)
{
  printf("%lu\n", (unsigned long) getuid());
  return 0;
}
