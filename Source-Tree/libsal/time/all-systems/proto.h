#ifndef __SAL_TIME_HEADER
#define __SAL_TIME_HEADER

#if	IS_BSD | HAS_BSD_TIME
#include <sys/time.h>
#else
#include <time.h>
#endif

#if	HAS_NOT_TIMET
typedef long time_t;
#endif

#endif /* __SAL_TIME_HEADER */
