/*
 * libsal/skiplists/newlist.c
 * Create a new skiplist
 * 
 * For copyright details, see libsal/license.h
 */

#include <libsal/skiplists/internal.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libsal/types.h>

sal_skiplist_skiplist sal_skiplist_NewList(int (*compare_func)(void *, void *))
{
  sal_skiplist_skiplist pstList;
  int i;
  
  pstList = (sal_skiplist_skiplist) malloc (sizeof (struct TAG_sal_skiplist_skiplist));
  pstList->iLevel = 0;
  pstList->pstHeader = sal_skiplist_NewNodeOfLevel(SAL_SKIPLIST_MAXNOOFLEVELS);
  pstList->compare_func = compare_func;
  
  for (i = 0; i < SAL_SKIPLIST_MAXNOOFLEVELS; i++)
    pstList->pstHeader->pstForward[i] = sal_skiplist_GetEndNode();
  
  return pstList;
}

