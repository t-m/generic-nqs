#ifndef __SAL_SKIPLISTS_PROTO_HEADER
#define __SAL_SKIPLISTS_PROTO_HEADER

/*
 * libsal/skiplists/proto.h
 * Prototypes for skip list code
 * 
 * See libsal/license.h for copyright information
 */

void sal_skiplist_Init(void);
int sal_skiplist_Insert(sal_skiplist_skiplist, void *);
int sal_skiplist_Search(sal_skiplist_skiplist, void *, void **);
int sal_skiplist_Delete(sal_skiplist_skiplist, void *);

sal_skiplist_skiplist sal_skiplist_NewList  (int (*compare_func)(void *, void *));
void                  sal_skiplist_FreeList (sal_skiplist_skiplist);

#ifdef __SAL_SKIPLISTS_INTERNAL_HEADER
/* put prototypes for routines internal to the skip list code HERE */

sal_skiplist_node sal_skiplist_Find          (sal_skiplist_skiplist, void *);
sal_skiplist_node sal_skiplist_FindForUpdate (sal_skiplist_skiplist, void *, sal_skiplist_node *);

int  sal_skiplist_GetRandomLevel(void);
void sal_skiplist_SetRandomLevel(void);

sal_skiplist_node sal_skiplist_GetEndNode(void);
void              sal_skiplist_SetEndNode(sal_skiplist_node);

#endif /* __SAL_SKIPLISTS_INTERNAL_HEADER */

#endif /* __SAL_SKIPLISTS_PROTO_HEADER */
