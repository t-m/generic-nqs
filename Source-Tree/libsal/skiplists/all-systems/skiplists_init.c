/*
 * libsal/skiplists/init.c
 * Initialise the skip list support
 * 
 * See libsal/license.h for copyright information
 */

#include <libsal/skiplists/internal.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libsal/types.h>

#include <stdio.h>
#include <stdlib.h>

void sal_skiplist_Init(void)
{
  /* 
   * the purpose of this first piece of code is to ensure
   * that this can be called often, but is only executed
   * the once.
   */
  static unsigned int iLock = 0;
  
  if (iLock++)
  {
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "sal_skiplist_Init() locked out %u time(s)", iLock);
    return;
  }
  
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Initialised subsystem: skiplists");
  
  sal_skiplist_SetEndNode(sal_skiplist_NewNodeOfLevel(0));
  sal_skiplist_GetEndNode()->szData = NULL;
  sal_skiplist_SetRandomLevel();
}

