/*
 * libsal/skiplists/search.c
 * Search for a node in a skip list
 * 
 * For copyright details, see libsal/license.h
 */

#include <libsal/skiplists/internal.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libsal/types.h>

int sal_skiplist_Search (sal_skiplist_skiplist pstList, void *szKey, void ** szData)
{
  sal_skiplist_node q = sal_skiplist_Find(pstList, szKey);
  
  if ((*pstList->compare_func)(q->szData, szKey) == 0)
  {
    *szData = q->szData;
    return 1;
  }
  else
    return 0;
}
