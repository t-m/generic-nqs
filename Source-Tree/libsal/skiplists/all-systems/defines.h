#ifndef __SAL_SKIPLISTS_DEFINES_HEADER
#define __SAL_SKIPLISTS_DEFINES_HEADER

/*
 * libsal/skiplists/defines.h
 * Definitions for use with skip lists
 * 
 * See libsal/license.h for copyright details.
 */

#include <SETUP/autoconf.h>

#define SAL_SKIPLIST_MAXNOOFLEVELS 16
#define SAL_SKIPLIST_MAXLEVEL (SAL_SKIPLIST_MAXNOOFLEVELS - 1)

/* 
 * this is naughty of us, because in the .c files it'll look like
 * a real function; but it's not!
 */
#include <stdlib.h>

#define sal_skiplist_NewNodeOfLevel(x) (sal_skiplist_node) malloc (sizeof(struct TAG_sal_skiplist_node) + (x)*(sizeof(sal_skiplist_node)))

#endif /* __SAL_SKIPLISTS_DEFINES_HEADER */

