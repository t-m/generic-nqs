#ifndef __SAL_SKIPLISTS_TYPES_HEADER
#define __SAL_SKIPLISTS_TYPES_HEADER

/*
 * libsal/skiplists/types.h
 * Data types for use with skip lists data structure
 * 
 * See libsal/license.h for copyright details
 */

struct TAG_sal_skiplist_node
{
  void * szData;
  struct TAG_sal_skiplist_node ** pstForward;
};

typedef struct TAG_sal_skiplist_node * sal_skiplist_node;

struct TAG_sal_skiplist_skiplist
{
  int               iLevel;
  sal_skiplist_node pstHeader;
  int (*compare_func)(void *, void *);
};

typedef struct TAG_sal_skiplist_skiplist * sal_skiplist_skiplist;

#endif /* __SAL_SKIPLISTS_TYPES_HEADER */

