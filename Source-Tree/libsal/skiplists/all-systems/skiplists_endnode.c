/*
 * libsal/skiplist/endnode.c
 * Support for skip list data structures
 * 
 * See libsal/license.h for copyright information
 */

#include <libsal/skiplists/internal.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libsal/types.h>

static sal_skiplist_node pstEndNode;

sal_skiplist_node sal_skiplist_GetEndNode(void)
{
  assert(pstEndNode != NULL);
  return pstEndNode;
}

void sal_skiplist_SetEndNode(sal_skiplist_node pstNode)
{
  assert(pstNode != NULL);
  pstEndNode = pstNode;
}
