/*
 * libsal/skiplists/freelist.c
 * Routine to delete a skiplist
 * 
 * See libsal/license.h for copyright details
 */

#include <libsal/skiplists/internal.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libsal/types.h>

void sal_skiplist_FreeList(sal_skiplist_skiplist pstList)
{
  sal_skiplist_node p,q,r ;
  
  p = pstList->pstHeader;
  
  /* much faster if we use a temporary variable */
  r = sal_skiplist_GetEndNode();
  
  do
  {
    q = p->pstForward[0];
    free(p);
    p = q;
  } while (p != r);
  
  free (pstList);
}

