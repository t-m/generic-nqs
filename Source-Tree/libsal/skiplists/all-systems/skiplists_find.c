/*
 * libsal/skiplists/find.c
 * Find a value inside a skip list
 * 
 * for copyright details, see libsal/license.h
 */

#include <libsal/skiplists/internal.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libsal/types.h>

sal_skiplist_node sal_skiplist_FindForUpdate(sal_skiplist_skiplist pstList, void *szKey, sal_skiplist_node * pstUpdate)
{
  int k;
  sal_skiplist_node p, q;
  
  k = pstList->iLevel;
  p = pstList->pstHeader;
  
  do
  {
    while (q = p->pstForward[k], (*pstList->compare_func)(q->szData, szKey) == -1)
      p = q;
    pstUpdate[k] = p;
  } while (--k > 0);
  
  return q;
}

sal_skiplist_node sal_skiplist_Find(sal_skiplist_skiplist pstList, void *szKey)
{
  sal_skiplist_node pstUpdate[SAL_SKIPLIST_MAXNOOFLEVELS];
  
  return sal_skiplist_FindForUpdate(pstList, szKey, pstUpdate);
}
