/*
 * libsal/machine/gethostname.c
 */

#include <libsal/license.h>
#include <libsal/proto.h>

#if	IS_POSIX_1 | IS_SYSVr4
#include <sys/utsname.h>
#else /* BSD */
#include <unistd.h>
#endif

/*
 * !! FUNCTION:	sal_gethostname
 * !! PURPOSE:	Call this function to obtain the name of the local
 * !! PURPOSE:	machine.
 * 
 * !! A_NAME:	szBuffer
 * !! A_TYPE:	char *
 * !! A_DESC:	Pointer to a string in which to put the machine name.
 * !! A_NAME:	iLen
 * !! A_TYPE:	int
 * !! A_DESC:	Length of the string pointer to by szBuffer.
 * 
 * !! R_TYPE:	int
 * !! R_DESC:	Returns 0 for success, and -1 on error.
 * 
 * !! API_LIST:	POSIX BSD SYSV
 * !! API_DESC:	Should work on all versions of UNIX.
 * !! END_FUNCTION:
 */

int sal_gethostname(char *szBuffer, int iLen)
{
#if	IS_POSIX_1 | IS_SYSVr4
  /*
   * POSIX.1 compliant platforms use 
   * 
   * 	#include <sys/utsname.h>
   *    int uname(struct utsname *);
   * 
   */
  struct utsname stName;
  
  if (uname(&stName) >= 0)
  {
    if (iLen < (strlen(stName.nodename) -1))
    {
      return -1;
    }
    else
    {
      strcpy(szBuffer, stName.nodename);
      return 0;
    }
  }
  else
  {
    return -1;
  }
#else	/* BSD */
  
  /*
   * BSD systems use
   * 
   * 	#include <unistd.h>
   * 	int gethostname (char *, int)
   * 
   */
  return gethostname (szBuffer, iLen);
#endif
}

      
