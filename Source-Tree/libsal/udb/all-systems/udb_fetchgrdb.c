/*
 * libsal/udb/fetchgrdb.c
 * Routines to interface with the group database
 * 
 * Originally taken from the Network Queueing System
 */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <grp.h>			/* Password file structures */
#include <string.h>

/*** fetchgrgid
 *
 *
 *	struct group *fetchgrgid():
 *	Return struct group structure for the specified local group-id.
 *
 *	NOTE:	This function does not open and close the
 *		group-database on successive invocations.
 *		It is up to the caller to invoke closegrdb()
 *		when access is no longer required to the
 *		group-database.
 */
struct group *sal_fetchgrgid (int gid)
{
	register struct group *p;

	setgrent();			/* Seek to start of group file */
	while ((p = getgrent()) != (struct group *) 0 && p->gr_gid != gid)
		;
	return (p);
}


/*** fetchgrnam
 *
 *
 *	struct group *fetchgrnam():
 *	Return struct group structure for the specified group-name.
 *
 *	NOTE:	This function does not open and close the
 *		group-database on successive invocations.
 *		It is up to the caller to invoke closegrdb()
 *		when access is no longer required to the
 *		group-database.
 */
struct group *sal_fetchgrnam (char *name)
{
	register struct group *p;

  	assert(name != NULL);
  
	setgrent();			/* Seek to start of group file */
	while ((p = getgrent()) && strcmp (p->gr_name, name))
		;
	return(p);
}


/*** closegrdb
 *
 *
 *	void closegrdb():
 *
 *	Close the local group-database opened by fetchgrgid(), or
 *	by fetchgrnam().
 */
void sal_closegrdb(void)
{
	endgrent();			/* Close the local group file */
}
