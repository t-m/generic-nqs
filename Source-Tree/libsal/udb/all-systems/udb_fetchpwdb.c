/*
 * libsal/udb/fetchpwdb.c
 * Routines to access the passwords database.
 */

#include <libsal/license.h>
#include <libsal/proto.h>

#include <stdio.h>			/* for definition of L_cuserid */
#include <pwd.h>			/* Password file structures */


/*** fetchpwuid
 *
 *
 *	struct passwd *fetchpwuid():
 *	Return struct passwd structure for the specified local user-id.
 *
 *	NOTE:	This function does not open and close the
 *		account/password database on successive
 *		invocations.  It is up to the caller to
 *		invoke closepwdb() when access to the
 *		account/password database is no longer
 *		required.
 */
struct passwd *sal_fetchpwuid (int uid)
{
    return ( getpwuid (uid) );
}


/*** fetchpwnam
 *
 *
 *	struct passwd *fetchpwnam():
 *	Return struct passwd structure for the specified local username.
 *
 *	NOTE:	This function does not open and close the
 *		account/password database on successive
 *		invocations.  It is up to the caller to
 *		invoke closepwdb() when access to the
 *		account/password database is no longer
 *		required.
 */
struct passwd *sal_fetchpwnam (char *name)
{
    return ( getpwnam (name) );
}


/*** closepwdb
 *
 *
 *	void closepwdb():
 *
 *	Close the local account/password database opened by fetchpwuid(),
 *	or by fetchpwnam().
 */
void sal_closepwdb()
{
	endpwent();			/* Close the local password file */
}
