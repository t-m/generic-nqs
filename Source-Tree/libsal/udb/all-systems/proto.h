#ifndef __SAL_UDB_PROTO_HEADER
#define __SAL_UDB_PROTO_HEADER

#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

void            sal_closegrdb  (void);
struct group *  sal_fetchgrgid (int gid);
struct group *	sal_fetchgrnam (char *name);
struct group *  sal_getchgrnam (char *name);

void            sal_closepwdb   (void);
struct passwd * sal_fetchpwnam  (char *name);
struct passwd * sal_fetchpwuid  (int uid);

#endif /* __SAL_UDB_PROTO_HEADER */
