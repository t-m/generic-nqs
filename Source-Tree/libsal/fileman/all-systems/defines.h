#ifndef __SAL_FILEMAN_DEFINES_HEADER
#define __SAL_FILEMAN_DEFINES_HEADER

/*
 * libsal/fileman/all-systems/defines.h
 * Definitions for managed file support
 *
 * Part of the System Abstraction Layer
 */

#define SAL_FILEMAN_FLAG_WRITE			0x00000001
#define SAL_FILEMAN_FLAG_FAST			0x00000002
#define SAL_FILEMAN_FLAG_CLEARONRELEASE		0x00000004
#define SAL_FILEMAN_FLAG_DIDCLOSE		0x00000008

#define SAL_FILEMAN_ISFLAG(x,y)			((y) & SAL_FILEMAN_FLAG_ ## x)
#define SAL_FILEMAN_SETFLAG(x,y)		(y) = (y) & SAL_FILEMAN_FLAG_ ## x
#define SAL_FILEMAN_CLEARFLAG(x,y)		(y) = (y) & ~SAL_FILEMAN_FLAG_ ## x

#define SAL_FILEMAN
#endif /* SAL_FILEMAN_DEFINES_HEADER */
