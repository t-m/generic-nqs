#ifndef __SAL_FILEMAN_TYPES_HEADER
#define __SAL_FILEMAN_TYPES_HEADER

/*
 * libsal/fileman/all-systems/types.h
 * Type definitions for managed files
 * 
 * Part of the System Abstraction Layer
 */

#include <libsal/skiplist/types.h>

struct TAG_sal_fileman_bitmap
{
  int iNumber;
  /* which bitmap is this? */
  void * pvoBitmap;
  /* the actual bitmap */
};

typedef struct TAG_sal_fileman_bitmap sal_fileman_bitmap;

struct TAG_sal_fileman
{
  int iDesc;
  /* file descriptor */
  char * szName;
  /* name of the file */
  int iErrno;
  /* error number for this file */
  unsigned long ulFlags;
  /* flags for this file */
  
  /* the following is information we get from the file */
  unsigned long ulPages;
  /* number of pages */
  unsigned long ulPageSize;
  /* size of each page */
  
  sal_skiplist_skiplist stBitmapCache;
  /* cache of loaded bitmaps */
};

typedef struct TAG_sal_fileman sal_fileman;

#endif /* __SAL_FILEMAN_TYPES_HEADER */
