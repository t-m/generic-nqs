/*
 * libsal/fileman/open.c
 * Routine to open a managed file.
 * 
 * Part of the System Abstraction Layer
 */

#include <libsal/license.h>
#include <libsal/defines.h>
#include <libsal/types.h>

sal_fileman sal_fileman_open (const char * szFilename, int mode, int role)
{
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "Entered sal_fileman_open");
  
  TEST_ARG(szFilename != NULL, 1);
  TEST_ARG(role == SAL_FILEMAN_ROLE_READER || role == SAL_FILEMAN_ROLE_WRITER);
  /* we don't test mode, because it's just a standard UNIX mode */
  
  
