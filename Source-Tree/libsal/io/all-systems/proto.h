#ifndef __SAL_IO_PROTO_HEADER
#define __SAL_IO_PROTO_HEADER

/*
 * libsal/io/proto.h
 * ANSI C prototypes for i/o interface of libsal
 */

#include <stdarg.h>

int sal_io_vsnprintf(char *, int, const char *, va_list);

#endif /* __SAL_IO_PROTO_HEADER */
