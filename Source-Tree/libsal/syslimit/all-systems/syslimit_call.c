/*
 * libsal/syslimit/syslimit_call.c
 * Build a system limits table + routines to manage it
 */

/*
 * Author(s)
 * Stuart Herbert (S.Herbert@sheffield.ac.uk)
 * Academic Computing Services
 * University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/syslimit/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*
 * this is a table of calls to set, and get a limit via a supported
 * interface.
 * The first entry in the table is NULL to ensure that stray '0' values,
 * which may be propagating from some untrapped error elsewhere don't
 * work here.
 */

static sal_syslimit_interface sal_syslimit_interfaces [] =
{
  { NULL,			NULL				},
  { sal_syslimit_getrlimit,	sal_syslimit_setrlimit		},
  { NULL,			NULL,				},
  { NULL,			sal_syslimit_setllimit   	},
  { sal_syslimit_getnice,	sal_syslimit_setnice		},
  { sal_syslimit_getpriority,	sal_syslimit_setpriority	},
  { NULL,			NULL				}
};

/*
 * !! FUNCTION:	sal_syslimit_call
 * !! PURPOSE:	Call this function to obtain a pointer to the
 * !! PURPOSE:	correct API to call to enforce a particular limit.
 * 
 * !! A_NAME:	iInterface
 * !! A_TYPE:	int
 * !! A_DESC:	Contains a magic value, representing which interface
 * !! A_DESC:	we want the API of.  This value MUST be less than
 * !! A_DESC:	SAL_LIMIT_USES_END.
 * 
 * !! R_TYPE:	const sal_syslimit_entry *
 * !! R_DESC:	Pointer to the API published by the specified interface.
 * 
 * !! END_FUNCTION:
 */

const sal_syslimit_interface * sal_syslimit_call(int iInterface)
{
  /*
   * if this assertion fails, the user has passed us a value which would
   * go beyond the end of the interface array.
   */
  
  TEST_ARG(iInterface < SAL_LIMIT_USES_END, 1);
  
  /*
   * if this assertion fails, the user has passed us a value which would
   * go beyond the other end of the interface array.
   */
  
  TEST_ARG(iInterface > SAL_LIMIT_USES_START, 1);
  
  return &sal_syslimit_interfaces[iInterface];
}
