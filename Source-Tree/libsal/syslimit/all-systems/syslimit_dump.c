/*
 * libsal/syslimit/syslimit_dump.c
 * Dump contents of a syslimit structure to the debugging output
 */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

void sal_syslimit_dump(sal_syslimit *pstLimitArg)
{
  TEST_ARG(pstLimitArg != NULL, 1);
  
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "Contents of syslimit are as follows :\n");
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitInfo.szName         = %s\n", pstLimitArg->stLimitInfo.szName);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitInfo.iName          = %d\n", pstLimitArg->stLimitInfo.iName);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitInfo.iLimitType     = %d\n", pstLimitArg->stLimitInfo.iLimitType);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitInfo.iInterface     = %d\n", pstLimitArg->stLimitInfo.iInterface);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitInfo.iID1           = %d\n", pstLimitArg->stLimitInfo.iID1);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitInfo.iID2           = %d\n", pstLimitArg->stLimitInfo.iID2);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitInfo.boSupported    = %d\n", pstLimitArg->stLimitInfo.boSupported);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitInfo.boAffectsChild = %d\n", pstLimitArg->stLimitInfo.boAffectsChild);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitInfo.szDesc         = %s\n", pstLimitArg->stLimitInfo.szDesc);
  
  if (pstLimitArg->stLimitInfo.iName == SAL_LIMIT_NICE)
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitValue.iValue        = %lu\n", (unsigned long) pstLimitArg->stLimitValue.iValue);
  else
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitValue.iValue        = %ld\n", (signed long) pstLimitArg->stLimitValue.iValue);
  
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "stLimitValue.boUnlimited   = %d\n", pstLimitArg->stLimitValue.boUnlimited);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "--- End of syslimits contents ---\n");
}
