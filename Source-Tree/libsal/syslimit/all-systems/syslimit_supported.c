/*
 * libsal/syslimit/syslimit_supported.c
 * Determine whether a limit is supported or not
 */

/*
 * Author(s)
 * Stuart Herbert (S.Herbert@sheffield.ac.uk)
 * Academic Computing Services
 * University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/syslimit/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*
 * !! FUNCTION:	sal_syslimit_supported
 * !! PURPOSE:	Call this function to determine whether a limit, listed
 * !! PURPOSE:	in the syslimits table, is supported or not.
 * 
 * !! A_NAME:	stLimitArg
 * !! A_TYPE:	sal_syslimit *
 * !! A_DESC:	This is a pointer to a entry in the syslimits table.
 *
 * !! R_TYPE:	int
 * !! R_DESC:	Returns TRUE (1) if the limit is supported on this
 * !! R_DESC:	version of UNIX; FALSE (0) otherwise.
 * 
 * !! END_FUNCTION:
 */

int sal_syslimit_supported(const sal_syslimit *stLimitArg)
{
  assert(stLimitArg != NULL);
  assert(stLimitArg->stLimitInfo.iName > SAL_LIMIT_START);
  assert(stLimitArg->stLimitInfo.iName < SAL_LIMIT_END);
  
  return (sal_syslimit_supportedentry(&(stLimitArg->stLimitInfo)));
}
