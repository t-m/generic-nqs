/*
 * libsal/syslimit/nice.c
 * Get/set nice value
 */

/*
 * Author(s):
 * Stuart Herbert
 * Academic Computing Services, The University of Sheffield
 * Email: S.Herbert@sheffield.ac.uk
 */

#include <libsal/license.h>
#include <libsal/syslimit/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <unistd.h>
#include <errno.h>

int sal_syslimit_getnice(sal_syslimit *pstLimitArg)
{
#if	HAS_NICE_LIMITS
  int iNice;
  
  TEST_ARG(pstLimitArg != NULL, 1);
  errno = 0;

  iNice = nice(0);
  if (errno != 0)
    return 0;
  else
  {
    pstLimitArg->stLimitValue.iValue = iNice;
    return 1;
  }
#else
  return 0;
#endif
}

int sal_syslimit_setnice(sal_syslimit *pstLimitArg)
{
#if	HAS_NICE_LIMITS
  TEST_ARG(pstLimitArg != NULL, 1);
  
  errno = 0;
  nice(pstLimitArg->stLimitValue.iValue);
  if (errno == 0)
    return 1;
  else
    return 0;
#else
  return 0;
#endif
}
