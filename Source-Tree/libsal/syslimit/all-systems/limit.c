/*
 * libsal/syslimit/limit.c
 * Support for the UNICOS 'limit' API
 */

/*
 * Author(s):
 * Stuart Herbert
 * Academic Computing Services,
 * The University of Sheffield
 * Email: S.Herbert@sheffield.ac.uk
 * 
 * Based on UNICOS support for Generic NQS by Dave Safford, saff@tamu.edu
 */

#include <libsal/license.h>
#include <libsal/syslimit/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#if HAS_LIMIT_LIMITS | IS_UNICOS
#include <time.h>
#include <sys/category.h>
#include <sys/resource.h>
#include <unistd.h>
#endif

/* 
 * FIXME:
 * Someone please add support for sal_syslimit_getllimit ...
 */

int sal_syslimit_setllimit(sal_syslimit *pstLimitArg)
{
#if	HAS_LIMIT_LIMITS | IS_UNICOS
  TEST_ARG(pstLimitArg != NULL, 1);

  /* Generic NQS 3.50.0.5
   * 
   * We must adjust the value of the limit before we attempt to feed it to
   * the UNICOS interface.
   * 
   * For times, the formula is ( seconds * CLK_TCK )
   * For sizes, the formula is ( bytes   / 4096    )
   */
  
  switch (pstLimitArg->stLimitInfo.iID2)
  {
   case L_CPU:
    pstLimitArg->stLimitValue.iValue *= CLK_TCK;
    break;
   default:
    pstLimitArg->stLimitValue.iValue /= 4096;
    break;
  }
  
  limit(pstLimitArg->stLimitInfo.iID1, 0, pstLimitArg->stLimitInfo.iID2, pstLimitArg->stLimitValue.iValue);
  return 1;
#else
  return 0;
#endif
}
