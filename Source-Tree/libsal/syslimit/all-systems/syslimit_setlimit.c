/*
 * libsal/syslimit/syslimit_setlimit.c
 * Set a limit
 */

/*
 * Author(s)
 * Stuart Herbert (S.Herbert@sheffield.ac.uk)
 * Academic Computing Services
 * University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/syslimit/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*
 * !! FUNCTION:	sal_syslimit_setlimit
 * !! PURPOSE:	Call this function to set a system-supported limit to
 * !! PURPOSE:	a new value.
 * 
 * !! A_NAME:	stLimitArg
 * !! A_TYPE:	sal_syslimit *
 * !! A_DESC:	Pointer to the system limit structure which contains the
 * !! A_DESC:	new value for the limit you wish to change.
 * 
 * !! R_TYPE:	int
 * !! R_DESC:	Returns TRUE (1) if the limit has been changed, or FALSE
 * !! R_DESC:	(0) if not.
 * 
 * !! END_FUNCTION:
 */

int sal_syslimit_setlimit(sal_syslimit *stLimitArg)
{
  TEST_ARG(stLimitArg != NULL,                              1);
  TEST_ARG(stLimitArg->stLimitInfo.iName > SAL_LIMIT_START, 1);
  TEST_ARG(stLimitArg->stLimitInfo.iName < SAL_LIMIT_END,   1);

  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Entered sal_syslimit_setlimit() for limit %s\n", stLimitArg->stLimitInfo.szName);
  sal_syslimit_dump(stLimitArg);
  
  if (sal_syslimit_find(stLimitArg) && (sal_syslimit_supported(stLimitArg)))
  {
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Found limit %s\n", stLimitArg->stLimitInfo.szName);
    sal_syslimit_dump(stLimitArg);
    
    if (sal_syslimit_call(stLimitArg->stLimitInfo.iInterface)->fSetLimit(stLimitArg))
    {
      sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Limit enforced successfully\n");
      sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Exited sal_syslimit_setlimit for limit %s\n", stLimitArg->stLimitInfo.szName);
      return 1;
    }
    if (stLimitArg->stLimitValue.boUnlimited)
      sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to enforce the %s limit with value 'unlimited'\n", stLimitArg->stLimitInfo.szName);
    else
      sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to enforce the %s limit with value %lu\n", stLimitArg->stLimitInfo.szName, stLimitArg->stLimitValue.iValue);
  }
  else
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Limit %s not supported\n", stLimitArg->stLimitInfo.szName);
  
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Exited sal_syslimit_setlimit for limit %s\n", stLimitArg->stLimitInfo.szName);
  return 0;
}
