#ifndef __SAL_SYSLIMIT_PROTO_HEADER
#define __SAL_SYSLIMIT_PROTO_HEADER

/*
 * libsal/syslimit/proto.h
 * ANSI C Prototypes
 */

#include <libsal/types.h>

void				sal_syslimit_dump	(sal_syslimit *);
int				sal_syslimit_find	(sal_syslimit *);
int				sal_syslimit_getlimit	(sal_syslimit *);
int				sal_syslimit_iterate	(sal_syslimit *);
int				sal_syslimit_setlimit	(sal_syslimit *);
int				sal_syslimit_supported	(const sal_syslimit *);

#ifdef __SAL_SYSLIMIT_INTERNAL_HEADER
/*
 * These are functions only for use within libsal's syslimit support,
 * and not to be called from outside libsal.
 */
const sal_syslimit_interface *	sal_syslimit_call		(int);
const sal_syslimit_entry *	sal_syslimit_gettable		(void);
int				sal_syslimit_supportedentry 	(const sal_syslimit_entry *);

int				sal_syslimit_getrlimit		(sal_syslimit *);
int				sal_syslimit_setrlimit		(sal_syslimit *);
int				sal_syslimit_setllimit		(sal_syslimit *);
int				sal_syslimit_getnice		(sal_syslimit *);
int				sal_syslimit_setnice		(sal_syslimit *);
int				sal_syslimit_getpriority	(sal_syslimit *);
int				sal_syslimit_setpriority	(sal_syslimit *);
#endif /* __SAL_SYSLIMIT_INTERNAL_HEADER */

#endif /* __SAL_SYSLIMIT_PROTO_HEADER */
