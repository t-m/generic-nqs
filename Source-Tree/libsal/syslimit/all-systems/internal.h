#ifndef __SAL_SYSLIMIT_INTERNAL_HEADER
#define __SAL_SYSLIMIT_INTERNAL_HEADER

/*
 * libsal/syslimit/internal.h
 * Internal function support
 * 
 * DESCRIPTION
 * 
 * 	Include this header file, before libsal/proto.h, to ensure that
 * 	prototypes for functions internal to libsal's syslimit interface
 * 	are available to your source code.
 */

#endif /* __SAL_SYSLIMIT_INTERNAL_HEADER */
