#ifndef __SAL_SYSLIMIT_TYPES_HEADER
#define __SAL_SYSLIMIT_TYPES_HEADER

/*
 * libsal/syslimit/types.h
 * Structures etc used in the system limits support
 */

#include <libsal/syslimit/defines.h>

/* ***********************************************************************
 * SYSTEM LIMIT SUPPORT
 */

struct TAG_sal_syslimit_entry
{
  char *szName;
  int iName;
    /* which limit entry is this for ? */
  int iLimitType;
    /* what type is this limit?  is it a time limit, or a size limit? */
  int iInterface;
    /* which interface do we use for this limit ? */
  int iID1;
  int iID2;
    /* what is the numeric ID of this interface ? */
  int boSupported;
    /* is this limit supported or not ? */
  int boAffectsChild;
    /* is this limit inherited by child processes ? */
  char *szDesc;
    /* what do we think this limit is for? */
};

typedef struct TAG_sal_syslimit_entry sal_syslimit_entry;

#define SAL_LIMIT_START		0
#define SAL_LIMIT_CPU		1
#define SAL_LIMIT_FSIZE		2
#define SAL_LIMIT_DATA		3
#define SAL_LIMIT_STACK		4
#define SAL_LIMIT_CORE		5
#define SAL_LIMIT_RSS		6
#define SAL_LIMIT_MEMLOCK	7
#define SAL_LIMIT_NPROC		8
#define SAL_LIMIT_OFILE		9
#define SAL_LIMIT_NOFILE	10
#define SAL_LIMIT_VMEM		11
#define SAL_LIMIT_REGFILESIZE	12
#define SAL_LIMIT_RCPU		13
#define SAL_LIMIT_RFSIZE	14
#define SAL_LIMIT_RVMEM		15
#define SAL_LIMIT_RCORE		16
#define SAL_LIMIT_NICE		17
#define SAL_LIMIT_END		18

#define SAL_LIMIT_BY_SIZE	1
#define SAL_LIMIT_BY_TIME	2
#define SAL_LIMIT_BY_NO		3

/* one entry for each interface supported by SAL */

#define SAL_LIMIT_USES_START		0
#define SAL_LIMIT_USES_RLIMIT		1
#define SAL_LIMIT_USES_ULIMIT		2
#define SAL_LIMIT_USES_LIMIT		3
#define SAL_LIMIT_USES_NICE		4
#define SAL_LIMIT_USES_SETPRIORITY	5
#define SAL_LIMIT_USES_END		6

#ifndef HAS_RLIM_T
  typedef int rlim_t;
#endif

typedef rlim_t sal_syslimit_t;

struct TAG_sal_syslimit_value
{
  sal_syslimit_t iValue;
    /* what is the value of this limit ? */
  int boUnlimited;
    /* is there a limit at all ? */
};

typedef struct TAG_sal_syslimit_value sal_syslimit_value;

struct TAG_sal_syslimit
{
  sal_syslimit_entry stLimitInfo;
  sal_syslimit_value stLimitValue;
};

typedef struct TAG_sal_syslimit sal_syslimit;

struct TAG_sal_syslimit_interface
{
  int (*fGetLimit)(sal_syslimit *);
  int (*fSetLimit)(sal_syslimit *);
};

typedef struct TAG_sal_syslimit_interface sal_syslimit_interface;

#endif /* __SAL_SYSLIMIT_TYPES_HEADER */
