#ifndef __SAL_SYSLIMIT_HPUX_HEADER
#define __SAL_SYSLIMIT_HPUX_HEADER

/*
 * libsal/syslimit/hpux.h
 * Header file for HP-UX systems and syslimit support
 */

#ifdef IS_HPUX

#ifndef RLIM_INFINITY
#define RLIM_INFINITY	0x7fffffff
#endif 

#ifndef RLIMIT_CPU
#define RLIMIT_CPU	0
#endif

#ifndef RLIMIT_FSIZE
#define RLIMIT_FSIZE	1
#endif

#ifndef RLIMIT_DATA
#define RLIMIT_DATA	2
#endif

#ifndef RLIMIT_STACK
#define RLIMIT_STACK	3
#endif

#ifndef RLIMIT_CORE
#define RLIMIT_CORE	4
#endif

#ifndef RLIMIT_RSS
#define RLIMIT_RSS	5
#endif

#endif /* IS_HPUX */
#endif /* __SAL_SYSLIMIT_HPUX_HEADER */
