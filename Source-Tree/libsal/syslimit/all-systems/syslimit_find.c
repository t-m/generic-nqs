/*
 * libsal/syslimit/syslimit_find.c
 * Find information about a particular system limit
 */

/*
 * Author(s)
 * Stuart Herbert (S.Herbert@sheffield.ac.uk)
 * Academic Computing Services
 * University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/syslimit/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*
 * !! FUNCTION:	sal_syslimit_find
 * !! PURPOSE:	Call this function, with stLimitArg->iName set to the
 * !! PURPOSE:	name of the limit you want to find, and this function
 * !! PURPOSE:	will copy the full details into stLimitArg.
 * 
 * !! A_NAME:	stLimitArg
 * !! A_TYPE:	sal_syslimit_entry *
 * !! A_DESC:	Pointer to an sal_syslimit_entry structure.  The structure's
 * !! A_DESC:	iName field will contain the name of the limit to locate -
 * !! A_DESC:	all other fields will be replaced by a successful find.
 * 
 * !! R_TYPE:	int
 * !! R_DESC:	TRUE (1) if the find was successful, FALSE (0) otherwise.
 * 
 * !! END_FUNCTION:
 */

int sal_syslimit_find(sal_syslimit * stLimitArg)
{
  const sal_syslimit_entry *pstWalker = sal_syslimit_gettable();

  /*
   * if this assertion fails, the user has passed us garbage
   */
  
  TEST_ARG(stLimitArg != NULL, 1);
  TEST_ARG(stLimitArg->stLimitInfo.iName > SAL_LIMIT_START, 1);
  TEST_ARG(stLimitArg->stLimitInfo.iName < SAL_LIMIT_END, 1);
  
  /*
   * if this assertion fails, the internal table has been corrupted
   */
  
  ENSURE_DATA(pstWalker != NULL, "Internal syslimits table is corrupt!");
  
  while ((pstWalker != NULL) && (pstWalker->iName != SAL_LIMIT_END))
  {
    if ((pstWalker->iName == stLimitArg->stLimitInfo.iName) &&
	(sal_syslimit_supportedentry(pstWalker)))
    {
      stLimitArg->stLimitInfo = *pstWalker;
        /* structure copy */
      return 1;
    }
    pstWalker++;
  }
  return 0;
}
