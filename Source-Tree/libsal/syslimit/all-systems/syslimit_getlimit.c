/*
 * libsal/syslimit/syslimit_getlimit.c
 * Routine to obtain the value of a system limit
 */

/*
 * Author(s)
 * Stuart Herbert (S.Herbert@sheffield.ac.uk)
 * Academic Computing Services
 * University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/syslimit/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*
 * !! FUNCTION:	sal_syslimit_getlimit
 * !! PURPOSE:	Call this routine to fill in the stLimitArg->stLimitValue
 * !! PURPOSE:	member structure.
 * 
 * !! A_NAME:	stLimitArg
 * !! A_TYPE:	sal_syslimit *
 * !! A_DESC:	Pointer to the structure to fill in.
 * 
 * !! R_TYPE:	int
 * !! R_DESC:	Returns TRUE (1) if the limit has been obtained, FALSE
 * !! R_DESC:	otherwise (ie, the limit is not supported).
 * 
 * !! END_FUNCTION:
 */

int sal_syslimit_getlimit(sal_syslimit *stLimitArg)
{
  /*
   * if this assertion fails, then we have been passed garbage
   */
  
  TEST_ARG(stLimitArg != NULL, 1);

  /*
   * we use sal_syslimit_find here in two ways :
   * 
   * 1) it allows the user to pass a minimal sal_syslimit entry, and
   *    we can fill out the details
   * 
   * 2) if the user has passed a full entry, we can verify it against
   *    the master copy in the table.
   */
  
  if ((sal_syslimit_find(stLimitArg)) &&
      (sal_syslimit_supported(stLimitArg)))
  {
    if (sal_syslimit_call(stLimitArg->stLimitInfo.iInterface)->fGetLimit(stLimitArg))
    {
      return 1;
    }
  }
  return 0;
}

