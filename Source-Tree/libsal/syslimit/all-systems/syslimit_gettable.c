/*
 * libsal/syslimit/syslimit_gettable.c
 * Build a system limits table + routine to obtain a pointer to it
 */

/*
 * Author(s)
 * Stuart Herbert (S.Herbert@sheffield.ac.uk)
 * Academic Computing Services
 * University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/syslimit/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

static sal_syslimit_entry sal_syslimits [] =
{
  START_LIMTBL,
  /* -------------- name -------------- internal symbol ------- time or size? --------- interface to use ------ ID1 ---	ID2 ------------------- affects child - description --- */
  MAKE_LIMTBL_ENTRY("cputime         ", SAL_LIMIT_CPU,		SAL_LIMIT_BY_TIME,	RLIMIT,			0,	RLIMIT_CPU,		0,		"CPU time limit"),
  MAKE_LIMTBL_ENTRY("cputime         ",	SAL_LIMIT_CPU,		SAL_LIMIT_BY_TIME,	LIMIT,			C_PROC,	L_CPU,			0,		"Per-process CPU time limit"),
  MAKE_LIMTBL_ENTRY("job cputime     ",	SAL_LIMIT_RCPU,		SAL_LIMIT_BY_TIME,	LIMIT,			C_JOB,	L_CPU,			0,		"Per-job CPU time limit"),
  MAKE_LIMTBL_ENTRY("filesize        ",	SAL_LIMIT_FSIZE,	SAL_LIMIT_BY_SIZE,	RLIMIT,			0,	RLIMIT_FSIZE,		0,		"Regular file size limit"),
  MAKE_LIMTBL_ENTRY("filesize        ",	SAL_LIMIT_FSIZE,	SAL_LIMIT_BY_SIZE,	LIMIT,			C_PROC,	L_FSBLK,		0,		"Per-process regular file size limit"),
  MAKE_LIMTBL_ENTRY("job filesize    ",	SAL_LIMIT_RFSIZE,	SAL_LIMIT_BY_SIZE,	LIMIT,			C_JOB,	L_FSBLK,		0,		"Per-job regular file size limit"),
  MAKE_LIMTBL_ENTRY("datasize        ", SAL_LIMIT_DATA,		SAL_LIMIT_BY_SIZE,	RLIMIT,			0,	RLIMIT_DATA,		0,		"Heap size limit"),
  MAKE_LIMTBL_ENTRY("stacksize       ",	SAL_LIMIT_STACK,	SAL_LIMIT_BY_SIZE,	RLIMIT,			0,	RLIMIT_STACK,		0,		"Stack size limit"),
  MAKE_LIMTBL_ENTRY("coredumpsize    ",	SAL_LIMIT_CORE,		SAL_LIMIT_BY_SIZE,	RLIMIT,			0,	RLIMIT_CORE,		0,		"Core dump size limit"),
  MAKE_LIMTBL_ENTRY("coredumpsize    ",	SAL_LIMIT_CORE,		SAL_LIMIT_BY_SIZE,	LIMIT,			C_PROC, L_CORE,			0,		"Per-process core dump size limit"),
  MAKE_LIMTBL_ENTRY("job coredumpsize", SAL_LIMIT_RCORE,	SAL_LIMIT_BY_SIZE,	LIMIT,			C_JOB,  L_CORE,			0,		"Per-job core dump size limit"),
  MAKE_LIMTBL_ENTRY("memoryuse       ",	SAL_LIMIT_RSS,		SAL_LIMIT_BY_SIZE,	RLIMIT,			0,	RLIMIT_RSS,		0,		"Resident set size limit"),
  MAKE_LIMTBL_ENTRY("memoryuse       ",	SAL_LIMIT_VMEM,		SAL_LIMIT_BY_SIZE,	LIMIT,			C_PROC,	L_MEM,			0,		"Per-process resident set size limit"),
  MAKE_LIMTBL_ENTRY("job memoryuse   ", SAL_LIMIT_RVMEM,	SAL_LIMIT_BY_SIZE,	LIMIT,			C_JOB,	L_MEM,			0,		"Per-job resident set size limit"),
  MAKE_LIMTBL_ENTRY("lockedmem       ",	SAL_LIMIT_MEMLOCK,	SAL_LIMIT_BY_SIZE,	RLIMIT,			0,	RLIMIT_MEMLOCK,		0,		"No of pages which can be locked limit"),
  MAKE_LIMTBL_ENTRY("maxproc         ",	SAL_LIMIT_NPROC,	SAL_LIMIT_BY_NO,	RLIMIT,			0,	RLIMIT_NPROC,		0,		"No of simultaneous processes limit"),
  MAKE_LIMTBL_ENTRY("descriptors     ",	SAL_LIMIT_OFILE,	SAL_LIMIT_BY_NO,	RLIMIT,			0,	RLIMIT_OFILE,		0,		"Number of open files limit"),
  MAKE_LIMTBL_ENTRY("descriptors     ",	SAL_LIMIT_NOFILE,	SAL_LIMIT_BY_NO,	RLIMIT,			0,	RLIMIT_NOFILE,		0,		"Number of open files limit"),
  MAKE_LIMTBL_ENTRY("vmemoryuse      ",	SAL_LIMIT_VMEM,		SAL_LIMIT_BY_SIZE,	RLIMIT,			0,	RLIMIT_VMEM,		0,		"Memory usage limit"),
  MAKE_LIMTBL_ENTRY("nice            ", SAL_LIMIT_NICE,		SAL_LIMIT_BY_NO,	NICE,			0,	NICE_NICE,		0,		"Process nice value"),
  MAKE_LIMTBL_ENTRY("nice            ", SAL_LIMIT_NICE,		SAL_LIMIT_BY_NO,	SETPRIORITY,		0,	SETPRIORITY_NICE,	0,		"Process nice value"),
  END_LIMTBL
};

/*
 * !! FUNCTION:	sal_syslimit_gettable
 * !! PURPOSE:	Call this function to obtain a pointer to the first
 * !! PURPOSE:	entry in the system limits table.
 * !! PURPOSE:
 * !! PURPOSE:	This routine can only be called internally.
 * 
 * !! NO_ARGS:
 * 
 * !! R_TYPE:	const sal_syslimit_entry *
 * !! R_DESC:	Pointer to the first entry in the syslimits table.
 * 
 * !! END_FUNCTION:
 */

const sal_syslimit_entry * sal_syslimit_gettable(void)
{
  return & sal_syslimits[0];
}

