/*
 * libsal/syslimit/syslimit_supportedentry.c
 * 
 */

/*
 * Author(s)
 * Stuart Herbert (S.Herbert@sheffield.ac.uk)
 * Academic Computing Services
 * University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/syslimit/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*
 * !! FUNCTION: sal_syslimit_supportedentry
 * !! PURPOSE:	Internal function used to check whether a table entry
 * !! PURPOSE:	represents a supported limit or not.  This is used
 * !! PURPOSE:	by sal_syslimit_iterate(), to save the trouble of
 * !! PURPOSE:	having to attempt the iteration using a full
 * !! PURPOSE:	sal_syslimit structure.
 * 
 * !! A_NAME:	pstListArg
 * !! A_TYPE:	const sal_syslimit_entry *
 * !! A_DESC:	Pointer to a table entry to test.
 * 
 * !! R_TYPE:	int
 * !! R_DESC:	Returns TRUE(1) if stLimitArg points to a limit which
 * !! R_DESC:	is supported by the host operating system, FALSE(0)
 * !! R_DESC:	otherwise.
 * 
 * !! END_FUNCTION:
 */

int sal_syslimit_supportedentry (const sal_syslimit_entry *stLimitArg)
{
  TEST_ARG(stLimitArg != NULL, 			1);
  TEST_ARG(stLimitArg->iName > SAL_LIMIT_START, 1);
  TEST_ARG(stLimitArg->iName < SAL_LIMIT_END,	1);
  
  return (stLimitArg->boSupported);
}
