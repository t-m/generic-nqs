/*
 * libsal/memory/bytecopy.c
 * Copy N bytes
 */

#include <libsal/license.h>
#include <libsal/proto.h>

/*** bytecopy
 *
 *
 *	bytecopy():
 *	Copy exactly N bytes.
 */
void sal_bytecopy (void *to, void *from, size_t n)
{
#if 	IS_POSIX_1 | IS_SYSVr4
  memcpy(to, from, n);
#else
  bcopy (from, to, (int) n);
#endif
}
