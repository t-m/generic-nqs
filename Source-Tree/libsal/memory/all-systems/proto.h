#ifndef __SAL_MEMORY_PROTO_HEADER
#define __SAL_MEMORY_PROTO_HEADER

#ifndef __SAL_LICENSE_HEADER
PLEASE ADD #include <libsal/license.h> TO THIS SOURCE CODE!!!
#endif

/*
 * libsal/memory/proto.h
 * Prototype support for the system-independant memory interface
 */

#include <sys/types.h>

void sal_bytecopy (void *to, void *from, size_t n);
void sal_bytezero (void *from, size_t n);

int		sal_gethostname (char *, int);

void		sal_closegrdb  (void);
struct group *	sal_fetchgrgid (int gid);
struct group *	sal_getchgrnam (char *name);

void		sal_closepwdb	(void);
struct passwd *	sal_fetchpwnam	(char *name);
struct passwd * sal_fetchpwuid	(int uid);

#endif /* __SAL_MEMORY_PROTO_HEADER */
