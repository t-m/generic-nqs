/*
 * libsal/memory/bytezero.c
 * Zero a specified range of memory.
 * 
 * Original author:
 * Stuart Herbert
 */

#include <libsal/license.h>
#include <libsal/proto.h>

#include <string.h>

/*** bytezero
 *
 *
 *	bytezero():
 *	Zero-out exactly N bytes.
 */
void sal_bytezero (void *to, size_t n)
{
#if	IS_POSIX_1 | IS_SYSVr4
  memset(to, (int) '\0', n);
#else
  bzero(to, (int) n);
#endif
}
