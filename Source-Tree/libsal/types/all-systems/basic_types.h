/*
 * libsal/types/all-systems/basic_types.h
 * Define fundamental data types for portability purposes
 * 
 * Part of the System Abstraction Layer
 * Copyright (c) 1996 Stuart Herbert
 * Released under v2 of the GNU General Public License
 */

/*
 * Possible definitions :
 * 
 * ulong32 		- guarenteed 32-bit unsigned value
 * 			- (I *hate* the NQS networking stuff!)
 * HAS_32BIT_INT	- printf() using shorts to avoid warnings
 * HAS_32BIT_LONG	- printf() using longs to avoid warnings
 */

#include <SETUP/autoconf.h>

#if GPORT_SIZEOF_UNSIGNED_LONG == 4
typedef unsigned long ulong32;
#undef  HAS_32BIT_LONG
#define HAS_32BIT_LONG
#else
#if GPORT_SIZEOF_INT == 4
typedef unsigned int ulong32;
#undef  HAS_32BIT_INT
#define HAS_32BIT_INT
#else
#error
/* if this error ever fires, please send urgent email to the current
 * Generic NQS maintainer to let him or her know!!! */
#endif /* SIZEOF_UNSIGNED_INT  */
#endif /* SIZEOF_UNSIGNED_LONG */

#define PRECISION_INT ( GPORT_SIZEOF_INT * 8 )
#define PRECISION_ULONG ( GPORT_SIZEOF_UNSIGNED_LONG * 8 )
