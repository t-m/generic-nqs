/*
 * libsal/error/all-systems/posix.c
 * POSIX error table, taken from POSIX 1003.1-1990
 * 
 * Part of the System Abstraction Layer
 */

#include <errno.h>

#include <libsal/error/internal.h>
#include <libsal/license.h>
#include <libsal/libsal.h>

static sal_error_table stPosixErrors [] =
{
  /* code - 			type - 		facility desc */
  ERROR_TABLE(0,		INFO,		0,	"No error reported"),
  ERROR_TABLE(E2BIG, 		WARNING, 	0, 	"Arg list too long"),
  ERROR_TABLE(EACCES,		WARNING, 	0, 	"Permission denied"),
  ERROR_TABLE(EAGAIN,		WARNING,	0,	"Resource temporarily unavailable"),
  ERROR_TABLE(EBADF,		WARNING,	0,	"Bad file descriptor"),
  ERROR_TABLE(EBUSY,		WARNING,	0,	"Resource busy"),
  ERROR_TABLE(ECHILD,		WARNING,	0,	"No child processes"),
  ERROR_TABLE(EDEADLK,		WARNING,	0,	"Resource deadlock avoided"),
  ERROR_TABLE(EDOM,		WARNING,	0,	"Domain error"),
  ERROR_TABLE(EEXIST,		WARNING,	0,	"File exists"),
  ERROR_TABLE(EFAULT,		WARNING,	0,	"Bad address"),
  ERROR_TABLE(EFBIG,		WARNING,	0,	"File too large"),
  ERROR_TABLE(EINTR,		WARNING,	0,	"Interrupted function call"),
  ERROR_TABLE(EINVAL,		WARNING,	0,	"Invalid argument"),
  ERROR_TABLE(EIO,		WARNING,	0,	"Input/output error"),
  ERROR_TABLE(EISDIR,		WARNING,	0,	"Is a directory"),
  ERROR_TABLE(EMFILE,		WARNING,	0,	"Too many open files"),
  ERROR_TABLE(EMLINK,		WARNING,	0,	"Too many links"),
  ERROR_TABLE(ENAMETOOLONG,	WARNING,	0,	"Filename too long"),
  ERROR_TABLE(ENFILE,		WARNING,	0,	"Too many open files in system"),
  ERROR_TABLE(ENODEV,		WARNING,	0,	"No such device"),
  ERROR_TABLE(ENOENT,		WARNING,	0,	"No such file or directory"),
  ERROR_TABLE(ENOEXEC,		WARNING,	0,	"Exec format error"),
  ERROR_TABLE(ENOLCK,		WARNING,	0,	"No locks available"),
  ERROR_TABLE(ENOMEM,		WARNING,	0,	"Not enough memory space"),
  ERROR_TABLE(ENOSPC,		WARNING,	0,	"No space left on device"),
  ERROR_TABLE(ENOSYS,		WARNING,	0,	"Function not implemented"),
  ERROR_TABLE(ENOTDIR,		WARNING,	0,	"Not a directory"),
  ERROR_TABLE(ENOTEMPTY,	WARNING,	0,	"Directory not empty"),
  ERROR_TABLE(ENOTTY,		WARNING,	0,	"Inappropriate I/O control operation"),
  ERROR_TABLE(ENXIO,		WARNING,	0,	"No such device or address"),
  ERROR_TABLE(EPERM,		WARNING,	0,	"Operation not permitted"),
  ERROR_TABLE(EPIPE,		WARNING,	0,	"Broken pipe"),
  ERROR_TABLE(ERANGE,		WARNING,	0,	"Result too large"),
  ERROR_TABLE(EROFS,		WARNING,	0,	"Read-only filing system"),
  ERROR_TABLE(ESPIPE,		WARNING,	0,	"Invalid seek"),
  ERROR_TABLE(ESRCH,		WARNING,	0,	"No such process"),
  ERROR_TABLE(EXDEV,		WARNING,	0,	"Improper link"),
  END_ERROR_TABLE
};

void sal_error_RegisterPOSIX(void)
{
  ENTER_FUNCTION("sal_error_RegisterPOSIX");
  ERROR_API(SAL_ERROR_SYSTEM, "POSIX.1 Error Codes", stPosixErrors);
  EXIT_FUNCTION;
}
