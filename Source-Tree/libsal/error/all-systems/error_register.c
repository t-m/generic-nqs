/*
 * libsal/error/register.c
 * Register an error API with the library
 * 
 * Part of the System Abstraction Layer
 * Released under v2 of the GNU GPL
 */

#include <libsal/error/internal.h>
#include <libsal/license.h>
#include <libsal/libsal.h>

void sal_error_RegisterApi(sal_error_api * pstErrorApi)
{
  sal_error_api * pstErrorFind;
  
  ENTER_FUNCTION("sal_error_api");
  
  TEST_ARG(pstErrorApi != NULL,                1);
  TEST_ARG(pstErrorApi->szDesc != NULL,        1);
  TEST_ARG(pstErrorApi->pstErrorTable != NULL, 1);
  
  pstErrorFind = sal_error_FindFromList(pstErrorApi->ulErrorApi);
  
  if (pstErrorFind != NULL)
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORAUTHOR, "Error: error API %d has same ID as API %d - not registered", pstErrorApi->szDesc, pstErrorFind->szDesc);
  
  sal_error_AddToList(pstErrorApi);
  
  EXIT_FUNCTION;
}
