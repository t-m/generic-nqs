#ifndef __SAL_ERROR_INTERNAL_HEADER
#define __SAL_ERROR_INTERNAL_HEADER

/*
 * libsal/error/internal.h
 * Include this to get access to internal routines.
 * 
 * Part of the System Abstraction Layer
 * Released under v2 of the GNU GPL
 */

#endif /* __SAL_ERROR_INTERNAL_HEADER */
