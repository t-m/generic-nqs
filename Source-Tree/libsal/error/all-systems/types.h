#ifndef __SAL_ERROR_TYPES_HEADER
#define __SAL_ERROR_TYPES_HEADER

/*
 * libsal/error/all-systems/types.h
 * error-code handling support
 * 
 * Part of the System Abstraction Layer
 */

#include <SETUP/autoconf.h>

#if GPORT_HAS_ERRNO_STRERROR
#if GPORT_HAS_ERRNO_STRERROR_STRING
#include <string.h>
#elif GPORT_HAS_ERRNO_STRERROR_ERRNO
#include <errno.h>
#endif /* GPORT_HAS_ERRNO_STRERROR_STRING */

#define libsal_err_strerror(x) strerror(x)

#elif GPORT_HAS_ERRNO_ERRLIST
#if GPORT_HAS_ERRNO_ERRLIST_STDIO
#include <stdio.h>
#elif GPORT_HAS_ERRNO_ERRLIST_ERRNO
#include <errno.h>
#elif GPORT_HAS_ERRNO_ERRLIST
extern GPORT_ERRLIST_TYPE;
#endif /* GPORT_HAS_ERRNO_ERRLIST */

#define libsal_err_strerror(x) sys_errlist[x]
#endif

struct TAG_sal_error
{
  unsigned long ulErrorApi;
  unsigned long ulErrorCode;
  char * szFile;
  unsigned long ulLine;
  BOOLEAN boCaught;
};

typedef struct TAG_sal_error sal_error;

struct TAG_sal_error_table
{
  unsigned long ulErrorCode;
  /* what is this error? */
  unsigned long ulErrorType;
  /* how should we call sal_dprintf() to announce this error? */
  unsigned long ulErrorFacility;
  /* ** not yet supported ** bitmap mask to turn on/off certain errors/warnings */
  const char * szName;
  const char * szDesc;
};

typedef struct TAG_sal_error_table sal_error_table;

struct TAG_sal_error_api
{
  unsigned long ulErrorApi;
  /* unique ID for this class of error */
  const char * szDesc;
  /* string to say where this error came from */
  sal_error_table * pstErrorTable;
  /* pointer to the error table proper */
};

typedef struct TAG_sal_error_api sal_error_api;
  
#endif /* __SAL_ERROR_TYPES_HEADER */
