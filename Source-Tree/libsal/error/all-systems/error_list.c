/*
 * libsal/error/all-systems/list.c
 * Maintain list of error APIs supported
 * 
 * Part of the System Abstraction Layer
 * Released under v2 of GNU GPL
 */

#include <libsal/error/internal.h>
#include <libsal/license.h>
#include <libsal/libsal.h>

static sal_skiplist_skiplist stList;

void sal_error_InitList(void)
{
  RUN_ONCE_ONLY_VOID;
  ENTER_FUNCTION("sal_error_InitList");
  
  sal_skiplist_Init();
  stList = sal_skiplist_NewList(sal_error_compare);
  
  EXIT_FUNCTION;
}

void sal_error_AddToList(sal_error_api * pstErrorApi)
{
  ENTER_FUNCTION("sal_error_api");
  
  TEST_ARG(pstErrorApi != NULL,                1);
  TEST_ARG(pstErrorApi->szDesc != NULL,        1);
  TEST_ARG(pstErrorApi->pstErrorTable != NULL, 1);
  
  sal_skiplist_Insert(stList, (void *) pstErrorApi);
  
  EXIT_FUNCTION;
}

sal_error_api * sal_error_FindFromList(unsigned long ulErrorApi)
{
  sal_error_api stErrorApi;
  sal_error_api * pstResult;
  
  ENTER_FUNCTION("sal_error_FindFromList");
  
  stErrorApi.ulErrorApi = ulErrorApi;
  
  if (sal_skiplist_Search(stList, (void *) &stErrorApi, (void **) &pstResult))
  {
    EXIT_FUNCTION;
    return pstResult;
  }
  else
  {
    EXIT_FUNCTION;
    return NULL;
  }
}

int sal_error_compare(void * pstError1, void * pstError2)
{
  sal_error_api * pstApi1 = (sal_error_api *) pstError1;
  sal_error_api * pstApi2 = (sal_error_api *) pstError2;
  
  TEST_ARG(pstApi1 != NULL, 1);
  TEST_ARG(pstApi2 != NULL, 2);
  
  if (pstApi1->ulErrorApi < pstApi2->ulErrorApi)
    return -1;
  else if (pstApi1->ulErrorApi > pstApi2->ulErrorApi)
    return 1;
  else
    return 0;
}
