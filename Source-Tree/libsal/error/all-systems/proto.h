#ifndef __SAL_ERROR_PROTO_HEADER
#define __SAL_ERROR_PROTO_HEADER

/*
 * libsal/error/proto.h
 * ANSI prototypes for error handling library
 * 
 * Part of the System Abstraction Layer
 * Released under v2 of the GNU GPL
 */

#include <libsal/types.h>
#include <libsal/defines.h>

void sal_error_Init        (void);
void sal_error_Handle      (sal_error);
void sal_error_RegisterApi (sal_error_api *);

#ifdef __SAL_ERROR_INTERNAL_HEADER

/*
 * these functions are intended only for use inside the error
 * handling library - don't call them from anywhere else!
 */

void              sal_error_InitList      (void);
void              sal_error_AddToList     (sal_error_api *);
sal_error_api   * sal_error_FindFromList  (unsigned long);
sal_error_table * sal_error_FindFromTable (sal_error_api *, unsigned long);
void              sal_error_RegisterPOSIX (void);
int               sal_error_compare       (void *, void *);

#endif /* __SAL_ERROR_INTERNAL_HEADER */
#endif /* __SAL_ERROR_PROTO_HEADER */
