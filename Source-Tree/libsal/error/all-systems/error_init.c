/*
 * libsal/error/init.c
 * Initialise the error-handling system
 * 
 * Part of the System Abstraction Layer
 * Released under v2 of GNU GPL
 */

#include <libsal/error/internal.h>
#include <libsal/license.h>
#include <libsal/libsal.h>

void sal_error_Init(void)
{
  ENTER_FUNCTION("sal_error_Init");
  
  sal_error_InitList();
  sal_error_RegisterPOSIX();
  
  EXIT_FUNCTION;
}
