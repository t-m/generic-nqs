/*
 * libsal/error/all-systems/handle.c
 * Handle a reported error
 * 
 * Part of the System Abstraction Layer
 */

#include <libsal/error/internal.h>
#include <libsal/license.h>
#include <libsal/libsal.h>

void sal_error_Handle (sal_error stError)
{
  sal_error_table * pstErrorTable;
  sal_error_api   * pstErrorApi;
  
  ENTER_FUNCTION("sal_error_Handle");

  if ((pstErrorApi = sal_error_FindFromList(stError.ulErrorApi)) != NULL)
  {
    if ((pstErrorTable = sal_error_FindFromTable(pstErrorApi, stError.ulErrorCode)) != NULL)
    {
      /* we have found the error information */
      sal_dprintf(stError.szFile, stError.ulLine, pstErrorTable->ulErrorType, "%8d-%8d: %s", stError.ulErrorApi, stError.ulErrorCode, pstErrorTable->szDesc);
      /* sal_dprintf() will not return for fatal errors ... */
      EXIT_FUNCTION;
      return;
    }
  }
  
  /*
   * at this point, we have an error which we don't know about.
   * 
   * Our strategy is simple :
   * 
   * 	1) If it's a system error, tell the user we've had an
   * 	   unknown system error as an error.
   * 
   * 	2) Otherwise, complain that we've received an unknown
   * 	   error from another source.
   */
  
  if (pstErrorApi != NULL)
    sal_dprintf(stError.szFile, stError.ulLine, SAL_DEBUG_MSG_ERRORAUTHOR, "Received unknown error code %d supposedly from API %s\n", stError.ulErrorCode, pstErrorApi->szDesc);
  else
    sal_dprintf(stError.szFile, stError.ulLine, SAL_DEBUG_MSG_ERRORAUTHOR, "Received unknown error code %d supposedly from unknown API %d\n", stError.ulErrorCode, stError.ulErrorApi);
}
