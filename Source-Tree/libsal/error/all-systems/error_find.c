/*
 * libsal/error/error_find.c
 * Find an error in the logged tables.
 * 
 * Part of the Stsrem Abstraction Layer
 * Released under v2 of GNU GPL
 */

#include <libsal/error/internal.h>
#include <libsal/license.h>
#include <libsal/libsal.h>

sal_error_table * sal_error_FindFromTable(sal_error_api * pstErrorApi, unsigned long ulErrorCode)
{
  sal_error_table * pstErrorTable;
  
  ENTER_FUNCTION("sal_error_FindFromTable");
  
  TEST_ARG(pstErrorApi != NULL, 1);
  
  pstErrorTable = pstErrorApi->pstErrorTable;
  while (pstErrorTable != NULL)
  {
    if (pstErrorTable->ulErrorCode == ulErrorCode)
    {
      EXIT_FUNCTION;
      return pstErrorTable;
    }
    pstErrorTable++;
  }
  
  EXIT_FUNCTION;
  return NULL;
}
