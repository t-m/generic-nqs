#ifndef __SAL_DIRENT_PROTO_HEADER
#define __SAL_DIRENT_PROTO_HEADER

/*
 * libsal/dirent/all-systems/proto.h
 * Prototypes for directory support
 * 
 * Part of the System Abstraction Layer
 */

/* NOTE
 * 
 * We use #def's here because we are trying to *exactly* duplicate the
 * existing API used by Generic NQS.
 * 
 * This should work for all APIs where sal_dirent is typedef'd as the
 * native directory structure type for the underlying operating system.
 */

#define sal_dirent_readdir(x)   readdir(x)
#define sal_dirent_rewinddir(x) rewinddir(x)

#endif /* __SAL_DIRENT_PROTO_HEADER */
