#ifndef __SAL_DIRENT_TYPES_HEADER
#define __SAL_DIRENT_TYPES_HEADER

/*
 * libsal/dirent/all-systems/types.h
 * Types for the portable directory handling support.
 * 
 * Part of the System Abstraction Layer
 */

#include <dirent.h>

typedef struct dirent sal_dirent;

#endif /* __SAL_DIRENT_TYPES_HEADER */
