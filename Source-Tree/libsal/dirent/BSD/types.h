#ifndef __SAL_DIRENT_TYPES_HEADER
#define __SAL_DIRENT_TYPES_HEADER

/*
 * libsal/dirent/BSD/types.h
 * Types for directory support
 * 
 * Part of the System Abstraction Layer
 */

#include <sys/dir.h>

typedef struct direct sal_dirent;

#endif /* __SAL_DIRENT_TYPES_HEADER */
