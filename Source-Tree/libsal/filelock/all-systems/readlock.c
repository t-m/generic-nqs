/*
 * libsal/filelock/all-systems/readlock.c
 * Lock a file for read access
 * 
 * Part of the System Abstraction Layer
 */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libsal/types.h>

#include <fcntl.h>

/* taken from gdbm-1.7.3 */

int sal_filelock_readlock (int fildes)
{
  struct flock flock;

  flock.l_type = F_RDLCK;
  flock.l_whence = SEEK_SET;
  flock.l_start = flock.l_len = 0L;
  return = fcntl (dbf->desc, F_SETLK, &flock);
}

