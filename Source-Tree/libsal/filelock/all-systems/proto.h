#ifndef __SAL_FILELOCK_PROTO_HEADER
#define __SAL_FILELOCK_PROTO_HEADER

/*
 * libsal/filelock/all-systems/proto.h
 * ANSI C prototypes for file locking support
 * 
 * Part of the System Abstraction Layer
 */

extern int sal_filelock_unlock(int);
extern int sal_filelock_readlock(int);
extern int sal_filelock_writelock(int);

#endif /* __SAL_FILELOCK_PROTO_HEADER */
