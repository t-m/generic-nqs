/*
 * libsal/filelock/all-systems/writelock.c
 * Support for write-locking a file
 * 
 * Part of the System Abstraction Layer
 */

#include <fcntl.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libsal/types.h>

/* taken from gdbm-1.7.3 */

int sal_filelock_writelock(int fildes)
{
  struct flock flock;
  
  flock.l_type = F_WRLCK;
  flock.l_whence = SEEK_SET;
  flock.l_start = flock.l_len = 0L;
  return fcntl (dbf->desc, F_SETLK, &flock);
}

