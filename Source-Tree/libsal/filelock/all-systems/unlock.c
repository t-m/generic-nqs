/*
 * libsal/filelock/all-systems/unlock.c
 * Unlock a locked file.
 * 
 * Part of the System Abstraction Layer
 */

/* Taken from gdbm-1.7.3 */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libsal/types.h>

#include <fcntl.h>

int sal_filelock_unlock(int fildes)
{
  struct flock flock;
  
  flock.l_type = F_UNLCK;
  flock.l_whence = SEEK_SET;
  flock.l_start = flock.l_len = 0L;
  return fcntl (dbf->desc, F_SETLK, &flock);
}
  
  
