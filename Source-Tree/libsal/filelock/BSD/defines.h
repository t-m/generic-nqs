#ifndef __SAL_FILELOCK_DEFINES_HEADER
#define __SAL_FILELOCK_DEFINES_HEADER

/*
 * libsal/filelock/BSD/defines.h
 * Definitions for file locking for BSD UNIX
 * 
 * Part of the System Abstraction Layer
 */

/* This code is based on that found within gdbm-1.7.3 */

#include <sys/file.h>

/* in case they are missing */

#ifndef LOCK_SH
#define LOCK_SH 1
#endif

#ifndef LOCK_EX
#define LOCK_EX 2
#endif

#ifndef LOCK_NB
#define LOCK_NB 4
#endif

#ifndef LOCK_UN
#define LOCK_UN 8
#endif

#define sal_filelock_unlock(x) flock(x, LOCK_UN)
#define sal_filelock_readlock(x) flock(x, LOCK_SH + LOCK_NB)
#define sal_filelock_writelock(x) flock(x, LOCK_EX + LOCK_NB)

#endif /* __SAL_FILELOCK_DEFINES_HEADER */
