#ifndef __SAL_LICENSE_HEADER
#define __SAL_LICENSE_HEADER

/*
 * libsal/license.h
 * License information for libsal
 * 
 * All source code supplied as part of libsal are subject to this license.
 */

/*
 * libsal
 * The System Abstraction Library
 * Originally devised for use with Generic NQS.
 * 
 * Copyright (c) 1995 The University of Sheffield.
 * Copyright (c) 1996 Stuart Herbert
 * 
 * Portions are the copyright of individual contributors.
 *
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * 
 * Work done by the University of Sheffield is funded by the Joint
 * Information Services Committee New Technologies Initiative grant
 * NTI/48.2, on behalf of all UK Higher Education sites.  More
 * information is available from :
 *
 *      http://www.shef.ac.uk/~nqs/
 *
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * 
 * Skip list support is adapted from code by William Pugh, Associate
 * Professor, Department of Computer Science, University of Maryland,
 * College Park.  More information is available from :
 * 
 * 	ftp://ftp.cs.umd.edu/pub/skipLists
 * 
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This license is to be found in the file COPYING.libsal, which is in the
 * top directory of the source code distribution.
 */

#endif /* __SAL_LICENSE_HEADER */
