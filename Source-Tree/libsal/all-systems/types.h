#ifndef __SAL_TYPES_HEADER
#define __SAL_TYPES_HEADER

#ifndef BOOLEAN
#define BOOLEAN int
#define TRUE    1
#define FALSE   0
#endif /* BOOLEAN */

#include <libsal/types/basic_types.h>
#include <libsal/debug/types.h>
#include <libsal/error/types.h>
#include <libsal/syslimit/types.h>
#include <libsal/skiplists/types.h>

#endif /* __SAL_TYPES_HEADER */
