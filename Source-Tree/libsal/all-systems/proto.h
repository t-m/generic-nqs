#ifndef __SAL_PROTO_HEADER
#define __SAL_PROTO_HEADER

/*
 * libsal/proto.h
 * Prototypes for the system abstraction library
 */

#ifndef __SAL_LICENSE_HEADER
#error YOU MUST INCLUDE <libsal/license.h> FIRST.
#endif

#include <stdio.h>
#include <libsal/time/proto.h>

#include <libsal/debug/proto.h>
#include <libsal/error/proto.h>
#include <libsal/io/proto.h>
#include <libsal/machine/proto.h>
#include <libsal/memory/proto.h>
#include <libsal/syslimit/proto.h>
#include <libsal/udb/proto.h>
#include <libsal/skiplists/proto.h>

#endif /* __SAL_PROTO_HEADER */
