/*
 * libsal/libsal.h
 * To save typing, this acts as a catch-all include file.
 * 
 * Part of the System Abstraction Layer
 * Released under v2 of the GNU GPL
 */

#include <libsal/defines.h>
#include <libsal/types.h>
#include <libsal/proto.h>
