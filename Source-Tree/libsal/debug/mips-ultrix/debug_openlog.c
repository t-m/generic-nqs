/*
 * libsal/debug/mips-ultrix/debug_openlog.c
 * Open the NQS logfile
 * 
 * Part of the System Abstraction Layer
 * Released under v2 of the GNU GPL
 */

#include <libsal/debug/internal.h>
#include <libsal/license.h>
#include <libsal/libsal.h>
#include <syslog.h>

/*
 * ULTRIX doesn't support most of openlog(), so we have to throw
 * some parameters away, and add our own ...
 */

void sal_debug_openlog(const char *szIdent, int iOption, int iFacility)
{
  openlog(szIdent, LOG_PID);
}

