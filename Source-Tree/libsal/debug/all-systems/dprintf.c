/*
 * libsal/debug/dprintf.c
 * Debugging output for Generic NQS
 * 
 * DESCRIPTION:
 * 
 * 	dprintf() provides a printf-like statement for use in
 * 	debugging statements.  The caller does not have to be
 * 	concerned about whether the output is required at the
 * 	current debugging level - the caller merely has to
 * 	provide enough information about the nature of the
 * 	statement so that dprintf() can determine whether to
 * 	output or not.
 * 
 * 	Original Author
 * 	===============
 * 	Stuart Herbert,
 * 	Academic Computing Services, University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/debug/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <stdlib.h>

int sal_dprintf
(
  const char      *szFile,
  unsigned long    ulLine, 
  sal_debug_msg_t  gMessType, 
  const char      *szFormat, 
  ...
)
{
  int		  iTmp;
    /* holds the return value from sal_debug_vdprintf() */
  va_list         vTmp;
  
  /* test arguments, to ensure we're happy with them */
  
  TEST_ARG(szFile != NULL, 1);
  TEST_ARG(gMessType > SAL_DEBUG_MSG_START, 3);
  TEST_ARG(gMessType < SAL_DEBUG_MSG_END,   3);
  TEST_ARG(szFormat != NULL, 4);
  
  /* if we don't want to show this type of message right now, silently
   * return ... */
  
  if (!sal_debug_QueryShowMessage(gMessType))
    return 0;
  
  /* Preparation for handling ... parameter */
  
  va_start(vTmp, szFormat);

  /* Show any message prefix information ... the actual information
   * output depends upon the current debugging level ... */
  
  sal_debug_ShowMessagePrefix(szFile, ulLine, gMessType);
  
  /* print the actual message */
  iTmp = sal_debug_vdprintf(gMessType, szFormat, vTmp);
  va_end(vTmp);
  
  /* 
   * finally, show any last details about this message.  NOTE that it
   * is possible for this function to never return, in which case a
   * core dump will have occured!!! 
   *
   * No, this is not a bug - there is a deliberate call to abort() in
   * there to produce a core dump if the message type is flagged as
   * being fatal.
   */
  
  sal_debug_ShowMessageSuffix(gMessType);
  
  return iTmp;
}
