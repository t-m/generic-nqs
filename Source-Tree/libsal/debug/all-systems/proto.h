#ifndef __SAL_DEBUG_PROTO_HEADER
#define __SAL_DEBUG_PROTO_HEADER

/*
 * libsal/debug/proto.h
 * Prototypes for libsal's debugging support
 */

#include <libsal/types.h>

/* 
 * these routines may be called from outside libsal's debugging support
 * code.
 * 
 * sal_debug_CallStack_Push() and sal_debug_CallStack_Pop() are called
 * by using the ENTER_FUNCTION and EXIT_FUNCTION macros; their use otherwise
 * is most definitely NOT supported.
 */

int sal_dprintf (const char *, unsigned long, sal_debug_msg_t, const char *, ... );

int sal_debug_GetLevel	(void);
int sal_debug_SetLevel	(int);

void sal_debug_Init (int, const char *, sal_debug_fatalfunc_t);
void sal_debug_InteractiveInit(int, sal_debug_fatalfunc_t);

void sal_debug_CallStack_Push(const char *, unsigned long, const char *);
void sal_debug_CallStack_Pop (const char *, unsigned long);

#ifdef	__SAL_DEBUG_INTERNAL_HEADER

/*
 * these are routines which are internal to the debugging support of libsal.
 * Their prototypes are not exported outside of the debugging support, and
 * you should NOT call them directly.
 */

#include <stdarg.h>

int sal_debug_dprintf  (sal_debug_msg_t, const char *, ...);
int sal_debug_vdprintf (sal_debug_msg_t, const char *, va_list);

const char * sal_debug_GetName		(sal_debug_msg_t);
int 	     sal_debug_GetSyslogAction	(sal_debug_msg_t);
BOOLEAN      sal_debug_QueryShowTrace	(sal_debug_msg_t);
BOOLEAN      sal_debug_QueryShowMessage	(sal_debug_msg_t);
BOOLEAN      sal_debug_QueryShowAuthor	(sal_debug_msg_t);
BOOLEAN      sal_debug_QueryFatal	(sal_debug_msg_t);
BOOLEAN      sal_debug_QueryShowWhere	(sal_debug_msg_t);

void sal_debug_SetInitLevel		(int);

void sal_debug_SetUseStderr		(void);
void sal_debug_SetDontUseStderr		(void);
int  sal_debug_UseStderr		(void);

void sal_debug_ShowAuthorDetails	(sal_debug_msg_t);
void sal_debug_ShowFatalDetails		(sal_debug_msg_t);
void sal_debug_ShowStackTrace		(sal_debug_msg_t);

void sal_debug_ShowMessagePrefix	(const char *, unsigned long, sal_debug_msg_t);
void sal_debug_ShowMessageSuffix	(sal_debug_msg_t);

const sal_debug_func_t * sal_debug_CallStack_Peep(const sal_debug_func_t *);

void sal_debug_SetOnFatal		(sal_debug_fatalfunc_t);
void sal_debug_OnFatal			(void);

void sal_debug_openlog			(const char *, int, int);

#endif /* __SAL_DEBUG_INTERNAL_HEADER */

#endif /* __SAL_DEBUG_PROTO_HEADER */
