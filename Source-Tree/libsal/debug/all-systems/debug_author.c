/*
 * libsal/debug/debug_author.c
 * Show author details
 * 
 * DESCRIPTION
 * 
 * 	This routine is called by sal_debug_ShowMessageSuffix() to output
 * 	details on how to contact the author, if appropriate.
 * 
 * 	Original Author
 * 	===============
 * 	Stuart Herbert
 * 	Academic Computing Services, The University of Sheffield
 */

/*
 * !! FIXME:	Modifications for SETUP support
 * !! DESC:	This file will require modification in the future so that
 * !! DESC:	the author's details can be changed using 'make config'.
 * !! END_FIXME:
 */

#include <libsal/license.h>
#include <libsal/debug/internal.h>
#include <libsal/proto.h>
#include <libsal/debug/defines.h>

#define AUTHOR_NAME	"Stuart Herbert"
#define AUTHOR_EMAIL	"S.Herbert@sheffield.ac.uk"

void sal_debug_ShowAuthorDetails
(
  sal_debug_msg_t gMessType
)
{
  /* first, ensure we're happy with the argument */
  TEST_ARG(gMessType > SAL_DEBUG_MSG_START, 1);
  TEST_ARG(gMessType < SAL_DEBUG_MSG_END,   1);
  
  /* if we don't want author output, return now */
  if (!sal_debug_QueryShowAuthor(gMessType))
    return;
  
  sal_debug_dprintf(gMessType, "Please report this problem to the author, %s (%s)\n", AUTHOR_NAME, AUTHOR_EMAIL);
}
