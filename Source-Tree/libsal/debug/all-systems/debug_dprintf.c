/*
 * libsal/debug/debug_dprintf.c
 * Debugging output routine for internal use
 * 
 * DESCRIPTION
 * 
 * 	sal_debug_dprintf(), like sal_dprintf(), is a wrapper around the
 * 	internal function sal_debug_vdprintf().  sal_dprintf() is called
 * 	from either elsewhere in libsal, or from outside of libsal,
 * 	while sal_debug_printf() is called *ONLY* from the routines
 * 	which sal_dprintf() calls.
 * 
 * 	sal_debug_dprintf() is protected as an internal function - no
 * 	prototype is available outside of libsal's debugging support.
 * 
 * 	Original Author
 * 	===============
 * 	Stuart Herbert
 * 	Academic Computing Services, The University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/debug/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*
 * !! FUNCTION:	sal_debug_dprintf
 * !! PURPOSE:	Functions called by sal_dprintf(), and ONLY those functions,
 * !! PURPOSE:	can call this function in order to output extra debugging
 * !! PURPOSE:	messages (eg, informing the user where the sal_dprintf()
 * !! PURPOSE:	call originated from ...).
 * 
 * !! A_NAME:	gMessType
 * !! A_TYPE:	sal_debug_msg_t
 * !! A_DESC:	Debugging message type to output
 * 
 * !! A_NAME:	szFormat
 * !! A_TYPE:	const char *
 * !! A_DESC:	Format string to print
 * 
 * !! A_NAME:	...
 * !! A_DESC:	Arguments for the format string
 * 
 * !! R_TYPE:	int
 * !! R_DESC:	Number of characters output
 * !! END_FUNCTION:
 */

int sal_debug_dprintf
(
  sal_debug_msg_t	 gMessType,
  const char 		*szFormat,
  ...
)
{
  int     iTmp;
    /* holds return value from sal_debug_vdprintf() */
  va_list vTmp;
    /* holds pointer to the variable argument list */
  
  TEST_ARG(gMessType > SAL_DEBUG_MSG_START, 1);
  TEST_ARG(gMessType < SAL_DEBUG_MSG_END,   1);
  TEST_ARG(szFormat != NULL, 2);
  
  va_start(vTmp, szFormat);
  
  iTmp = sal_debug_vdprintf(gMessType, szFormat, vTmp);
  
  va_end(vTmp);
  
  return iTmp;
}
