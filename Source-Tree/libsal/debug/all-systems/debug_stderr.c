/*
 * libsal/debug/debug_stderr.c
 * Stderr support for debugging software
 * 
 * DESCRIPTION
 * 
 * 	This routines are used internally by libsal's debugging support
 * 	to set, and determine, whether debugging output should go to
 * 	stderr or to syslog.
 * 
 * 	To use any of these routines, your code must include 
 *	libsal/debug/internal.h to get the prototypes.
 * 
 *  	Original Author
 * 	===============
 * 	Stuart Herbert
 * 	Academic Computing Services, University of Sheffield
 */

#include <libsal/debug/internal.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

static int s_iUseStderr = FALSE;

/* call this to send all debugging output to stderr */
void sal_debug_SetUseStderr(void)
{
  s_iUseStderr = TRUE;
}

/* call this to send all debugging output to syslog */
void sal_debug_SetDontUseStderr(void)
{
  s_iUseStderr = FALSE;
}

/* call this to determine whether to print to stderr (TRUE) or 
 * syslog (FALSE) */
int sal_debug_UseStderr(void)
{
  return s_iUseStderr;
}
