/*
 * libsal/debug/debug_init.c
 * Initialise the debugging support
 * 
 * DESCRIPTION
 * 
 * 	Call this routine to initialise the debugging support, and enable
 * 	output of messages.
 * 
 * 	NOTE that calling this routine opens a single file descriptor
 * 	before the routine returns.  This file descriptor is used for
 * 	communication with the syslog daemon.
 * 
 * 	Original Author
 * 	===============
 * 	Stuart Herbert
 * 	Academic Computing Services, University of Sheffield
 */

#include <libsal/debug/internal.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <libsal/General.h>

#include <syslog.h>
#include <stdlib.h>

/*
 * as we are initialising the debugging support, if anything goes wrong,
 * we cannot use it to report errors, so instead we fall back on good
 * old assert.h
 */

#ifdef assert
#undef assert
#include <assert.h>
#endif /* assert */

void sal_debug_Init 
(
  int                    iLevel,
  const char            *szName,
  sal_debug_fatalfunc_t  pfFunc
)
{
  time_t gTime;
  
  assert(szName != NULL);
  
  sal_debug_SetDontUseStderr();
  
  sal_debug_openlog(szName, LOG_CONS | LOG_NDELAY | LOG_PID, CONFIG_SAL_LOGFACILITY);
  sal_debug_SetInitLevel (iLevel);
  
  if (pfFunc != NULL)
    sal_debug_SetOnFatal(pfFunc);
  
  time(&gTime);
  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Process logging started at %s", asctime(localtime(&gTime)));
}

void sal_debug_InteractiveInit
(
  int			iLevel,
  sal_debug_fatalfunc_t	pfFunc
)
{
  sal_debug_SetUseStderr();
  sal_debug_SetInitLevel(iLevel);
  if (pfFunc != NULL)
    sal_debug_SetOnFatal(pfFunc);
}
