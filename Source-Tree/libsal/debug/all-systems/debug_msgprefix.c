/*
 * libsal/debug/debug_msgprefix.c
 * Show message prefix
 * 
 * DESCRIPTION
 * 
 * 	sal_debug_ShowMessagePrefix outputs a message showing where the
 * 	message is buried in the source code.  This is useful for finding
 * 	out which code triggered the message, so debugging (if required)
 * 	can begin.
 *
 * 	Original Author
 * 	===============
 * 	Stuart Herbert,
 * 	Academic Computing Services, The University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/debug/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

void sal_debug_ShowMessagePrefix
(
  const char 		* szFile,
  unsigned long		  ulLine,
  sal_debug_msg_t	  gMessType
)
{
  /* first, we test the arguments */
  
  TEST_ARG(szFile != NULL, 1);
  TEST_ARG(gMessType > SAL_DEBUG_MSG_START, 3);
  TEST_ARG(gMessType < SAL_DEBUG_MSG_END,   3);
  
  /* possible prefix: do we want to show where in the source code
   * the error message resides? */
  
  if (sal_debug_QueryShowWhere(gMessType))
    sal_debug_dprintf(gMessType, "In file %s at line %lu:\n", szFile, ulLine);
  
  /* no more prefixes supported atm.  Add more prefixes here ... */
}
