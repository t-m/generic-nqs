/*
 * libsal/debug/debug_fatal.c
 * Handle fatal debugging messages
 * 
 * DESCRIPTION
 * 
 * 	This function is called from sal_debug_ShowMessageSuffix(), to
 * 	attempt to tell the user that this process is about to core dump.
 * 	
 * 	Original Author
 * 	===============
 * 	Stuart Herbert
 * 	Academic Computing Services, The University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/debug/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

void sal_debug_ShowFatalDetails
(
  sal_debug_msg_t gMessType
)
{
  /* first, we test the args */
  TEST_ARG(gMessType > SAL_DEBUG_MSG_START, 1);
  TEST_ARG(gMessType < SAL_DEBUG_MSG_END,   1);
  
  /* if this isn't a fatal error, silently return */
  if (!sal_debug_QueryFatal(gMessType))
    return;

  /* call the fatal error handler - should be bye bye process */
  sal_debug_OnFatal();
  
  /* I'd love to see this ever get executed ;-) */
  NEVER;
}
