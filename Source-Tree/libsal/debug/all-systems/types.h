#ifndef __SAL_DEBUG_TYPES_HEADER
#define __SAL_DEBUG_TYPES_HEADER

/*
 * libsal/debug/types.h
 * Structures used by libsal's debugging support
 */

typedef int sal_debug_msg_t;
typedef void (*sal_debug_fatalfunc_t)(void);

#ifdef __SAL_DEBUG_INTERNAL_HEADER

/*
 * this is used by the function call trace stack
 */

struct sal_debug_func_t
{
  const char    *szFile;
  unsigned long  ulLine;
  const char    *szFunction;
    
  struct sal_debug_func_t *pstNext;
};

typedef struct sal_debug_func_t sal_debug_func_t;

#endif /* __SAL_DEBUG_INTERNAL_HEADER */
#endif /* __SAL_DEBUG_TYPES_HEADER */
