/*
 * libsal/debug/debug_level.c
 * Routines to set/get the current debugging level
 * 
 * DESCRIPTION:
 * 	
 * 	The routines in here can be used to specify the new level
 * 	of debugging support required, and to obtain the current level
 * 	of debugging support.
 * 
 * 	This scheme means that there is no longer any need to define
 * 	global variables just to hold the debugging level.
 *
 * 	Original Author
 * 	===============
 * 	Stuart Herbert
 * 	Academic Computing Services, University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/debug/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

static int iDebug = 1;

/*
 * !! FUNCTION:	sal_debug_GetLevel
 * !! PURPOSE:	Call this function to obtain the current debugging level.
 * 
 * !! NO_ARGS:
 * 
 * !! R_TYPE:	int
 * !! R_DESC:	The current debugging level.
 * !! END_FUNCTION:
 */

int sal_debug_GetLevel(void)
{
  return iDebug;
}

/*
 * !! FUNCTION:	sal_debug_SetLevel
 * !! PURPOSE:	Call this function to change the current debugging level.
 * 
 * !! A_NAME:	iNewDebug
 * !! A_TYPE:	int
 * !! A_DESC:	Contains the new debugging level.
 * 
 * !! R_TYPE:	int
 * !! R_DESC:	Returns the old debugging level.
 * 
 * !! END_FUNCTION:
 */

int sal_debug_SetLevel(int iNewDebug)
{
  int iTmp = iDebug;
  iDebug = iNewDebug;

  if (iTmp != iNewDebug)
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Changing debugging level from %d to %d\n", iTmp, iNewDebug);

  return iTmp;
}

void sal_debug_SetInitLevel(int iNewDebug)
{
  iDebug = iNewDebug;
}
