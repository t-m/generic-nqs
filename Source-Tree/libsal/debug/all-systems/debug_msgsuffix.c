/*
 * libsal/debug/debug_msgsuffix.c
 * Show supplimental debugging output after the main message
 * 
 * DESCRIPTION
 * 
 * 	sal_debug_ShowMessageSuffix() is called by sal_dprintf() after
 * 	sal_dprintf() has output the main message from the user.
 * 
 * 	sal_debug_ShowMessageSuffix() is a place-holder for calling
 * 	separate routines to output each type of suffix message.  The
 * 	only code to be added to this routine is calls to more routines.
 * 
 * 	Original Author
 * 	===============
 * 	Stuart Herbert
 * 	Academic Computing Services, The University of Sheffield
 */

#include <libsal/license.h>
#include <libsal/debug/internal.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*
 * pfMessage is a local type used to simplify the declaration of the
 * table of suffix messages below
 */

typedef void (*pfMessage)(sal_debug_msg_t);

/*
 * stMessage is the table of suffix messages; it is an array of functions
 * to be called, in order, to attempt to output supplimental messages.
 * 
 * This table is terminated by NULL.
 */

static pfMessage stMessage[] =
{
  sal_debug_ShowStackTrace,
  sal_debug_ShowAuthorDetails,
  sal_debug_ShowFatalDetails,
  NULL
};

void sal_debug_ShowMessageSuffix
(
  sal_debug_msg_t gMessType
)
{
  int iIndex;
  
  TEST_ARG(gMessType > SAL_DEBUG_MSG_START, 1);
  TEST_ARG(gMessType < SAL_DEBUG_MSG_END,   1);

  /* iterate through the table of suffix messages */
  for (iIndex = 0; stMessage[iIndex] != NULL; iIndex++)
    (*stMessage[iIndex])(gMessType);
  
}
