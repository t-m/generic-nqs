/*
 * netdaemon/netdaemon.c
 * 
 * DESCRIPTION:
 *
 *	This module is the main NQS network daemon module and processes
 *	requests from remote NQS client processes.
 *
 *	The NQS network daemon is a child of the local NQS daemon, and
 *	is in the same process group as the local NQS daemon.
 *
 *	When exec'ed from the local NQS daemon, the following file
 *	descriptors and file streams are open:
 *
 *
 *	  Stream stdout (fd 1):	writes to the NQS log
 *				process. 
 *
 *	  Stream stderr (fd 2):	writes to the NQS log
 *				process.
 *
 *	  File descriptor #3:	writes to the named request
 *				FIFO pipe of the local NQS daemon.
 *
 *
 *	The NQS network daemon CANNOT and MUST NOT be executed
 *	independently of the local NQS daemon.  The NQS network
 *	daemon must only be started up by a startup of the local
 *	NQS daemon.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Robert W. Sandstrom.
 *	Sterling Software Incorporated.
 *	December 19, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
					/* (includes <sys/types.h>) */
#include <stdlib.h>
#include <unistd.h>
#define	WRITE_FIFO	3		/* Write pipe to daemon is on #3 */
#include <libnqs/nqsvars.h>		/* NQS global vars */
#include <libnqs/nqspacket.h>		/* NQS packet types */
#include <libnqs/netpacket.h>		/* NQS network packet types */
#include <libnqs/transactcc.h>		/* NQS transaction completion codes */
#include <netdb.h>			/* 4.2 BSD network database; */
#include <errno.h>			/* Error number */
#include <pwd.h>			/* Password stuff */
#include <signal.h>			/* SIGINT, etc. */
#include <sys/socket.h>			/* 4.2 BSD structs */
#include <netinet/in.h>			/* Internet architecture specific */
#include <arpa/inet.h>
#include <grp.h>
#include <limits.h>
#include <sys/wait.h>			/* For WNOHANG, union wait */
#if     IS_POSIX_1 | IS_SYSVr4
#include <sys/time.h>
#include <sys/resource.h>
#endif

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <SETUP/General.h>

static void badpacket ( void );
static void delreqserver ( int sd, struct passwd *pw, Mid_t client_mid, char *chostname, uid_t whomuid, char *whomname, long orig_seqno, Mid_t orig_mid, int sig, int states );
static void execnetserver ( int newsd, struct passwd *pw, Mid_t client_mid, char *op );
static void loadserver ( int sd, Mid_t client_mid, int version, int nqs_jobs, char *string );
static char *netpacket ( long index );
static void net_abort ( void );
static void net_daeshutdown ( int );
static void outanderrto ( int sd );
static void pre_server ( int newsd, struct sockaddr_in *from );
static void qlimitserver ( int sd, Mid_t client_mid, char *chostname, int whomuid, int flags, char *whomname );
static void qstatserver ( int sd, struct passwd *whompw, Mid_t client_mid, char *chostname, int flags, int whomuid, char *queuename, char *whomname );
static void reload ( int );
static void rreqcomserver ( int sd, Mid_t client_mid, int req_seqno, Mid_t req_mid, int no_jobs );
static void sendandabort ( int sd, long tcm );
static void server_abort ( long tcm );
static void showbytes ( char *bytes, unsigned length );

#if	IS_BSD | HAS_BSD_PIPE
static void relaypackets (void);
#endif

/*
 * Variables exported from this module.
 */
int monsanto_header = 0;
/*
 * Variables local to this module.
 */
 
static void reapchild( int signum );	/* Wait for one or more kids */

/*** main
 *
 *
 *	int main():
 *	Start of the NQS network daemon.
 */
int main (int argc, char *argv[])
{
	struct sockaddr_in sin;		/* Internet socket */
	struct sockaddr_in from;	/* Socket-addr from accept() */
	struct servent *servent;	/* Server entry structure */
	struct stat stat_buf;		/* Stat() buffer */
	int sd;				/* Socket descriptor */
	int new_socket;			/* New socket from accept() */
	int from_addrlen;		/* Address length of client */
	char *root_dir;                 /* Fully qualified file name */

  	/* Generic NQS 3.50.0-pre9
	 * 
	 * We close file descriptors 1 & 2, and re-open them using
	 * sal_debug_Init, and dup2, to ensure that they correctly
	 * point to the right place
	 */
  	close(1);
  	close(2);
  	sal_debug_Init(1, "NQS Netdaemon", net_abort);
  	dup2(1, 2);
  
	if (getuid () != 0 || geteuid () != 0) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon uid or euid wrong.\n");
	}
	Argv0 = argv[0];		/* Save pointer to daemon argv[0] */
	Argv0size = strlen (Argv0);	/* and size for later use */
	/*
	 *  Set the name of this process to a more appropriate string.
	 *
	 *  We use sprintf() to pad with spaces rather than null
	 *  characters so that slimy programs like ps(1) which
	 *  walk the stack will not get confused.
	 */
#if	TEST
	sprintf (Argv0, "%-*s", Argv0size, "xNQS netdaemon");
#else
	sprintf (Argv0, "%-*s", Argv0size, "NQS netdaemon");
#endif
	/*
	 *  Initialize the in-core copy of the parameters.
	 *  The NQS local daemon uses SIGHUP to let us know
	 *  when the parameters have changed.  Reload() assumes
	 *  that the variable "Paramfile" has been set.
	 */
	if ((Paramfile = opendb (Nqs_params, O_RDONLY)) == NULL) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon: unable to open general parameters file.\n");
	}
	signal (SIGHUP, reload);
	ldparam ();
	/*
	 *  Disable certain signals.
	 */
	signal (SIGINT, SIG_IGN);
	signal (SIGQUIT, SIG_IGN);
	signal (SIGPIPE, SIG_IGN);
#if	( IS_POSIX_1 | IS_SYSVr4 )
	signal (SIGUSR1, SIG_IGN);
	signal (SIGUSR2, SIG_IGN);
#endif
#if defined(SIGCLD)
	signal (SIGCLD, reapchild);
#endif
#if defined(SIGCHLD)
	signal (SIGCHLD, reapchild);
#endif
	signal (SIGTERM, net_daeshutdown); /* Shutdown on receipt of SIGTERM */
	/*
	 *  All files are to be created with the stated permissions.
	 *  No additional permission bits are to be turned off.
	 */
	umask (0);

	if ( ! buildenv()) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon: Unable to establish directory independent environment.\n");
	}

	/*
	 *  Change directory to the NQS "root" directory.
	 */
	root_dir = getfilnam (Nqs_root, SPOOLDIR);
	if (root_dir == (char *)NULL) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon: Unable to determine root directory name.\n");
	}
	if (chdir (root_dir) == -1) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon: Unable to chdir() to the NQS root directory.\n");
	}
	relfilnam (root_dir);
	/*
	 *  Determine the machine-id of the local host.
	 */
	if (localmid (&Locmid) != 0) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon: unable to determine machine-id of local host.\n");
	}
	/*
	 *  Build the address that we will bind() to.
	 */
	servent = getservbyname (NQS_SERVICE_NAME, "tcp");
	if (servent == (struct servent *) 0) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "F$Netdaemon: Service [%s] is unknown.\n", NQS_SERVICE_NAME);
	}
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = servent->s_port;
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: Service on port %d\n",  sin.sin_port);
	sd = socket (AF_INET, SOCK_STREAM, 0);
	if (sd == -1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon: Socket() error.\n");
	}
	/*
	 *  Test to see that file descr #WRITE_FIFO is really open on the
	 *  local NQS daemon FIFO request pipe.  This partially tests whether
	 *  or not the NQS network daemon has been properly exec'ed from
	 *  the local NQS daemon.
	 */
	if (fstat (WRITE_FIFO, &stat_buf) == -1) {
		/*
		 *  Fstat can only fail by EBADF or EFAULT.
		 *  Since stat_buf is ok, the error is EBADF.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Netdaemon: Improper invocation.\n");
	}
	/*
	 *  Inform inter() that file descr #WRITE_FIFO is connected to the
	 *  local NQS daemon.
	 */
	interset (WRITE_FIFO);
	/*
	 *  Prepare to listen for connections.
	 */
	if (bind (sd, (struct sockaddr *) &sin, sizeof (sin)) == -1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon: Bind() error - possible attempt to execute multiple Netdaemons.\n");
	}
	if (listen (sd, 10) == -1) {	/* Queue up to 10 connections */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon: Listen() error.\n");
	}
	from_addrlen = sizeof (from);
	/*
	 *  Loop forever handling NQS network requests, until a SIGTERM
	 *  shutdown signal is received from the local NQS daemon.
	 */
	for (;;) {
		new_socket = accept (sd, (struct sockaddr *)&from, &from_addrlen);
		if (getppid() <= 1) net_abort();
		if (new_socket == -1) {
			if (errno != EINTR) {
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Netdaemon: accept() error, errno=%d\n",errno);
			}
		}
		else {
			/*
			 *  Make sure to flush any diagnostic messages
			 *  PRIOR to the fork!
			 */
			switch (fork()) {
			case -1:
				/*
				 *  The fork() failed, due to a lack of
				 *  available processes.
				 */
				close (new_socket);	/* Break connection */
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: fork failed - net request aborted.\n");
				break;
			case 0:
				/*
				 *  The fork() succeeded.  We are the
				 *  child server process.
				 *
				 *  Close the original socket before handling
				 *  the new request.
				 *
				 */
				signal (SIGTERM, SIG_DFL);
				signal (SIGHUP, SIG_IGN);
				close (sd);
				closedb (Paramfile);	/* Child opens fresh */
							/* copy */
				pre_server (new_socket, &from);
				break;
			default:
				/*
				 *  The fork() succeeded.  We are the
				 *  NQS network daemon.  Close new socket
				 *  and loop to handle another request.
				 */
				close (new_socket);
				break;
			}
		}
	}
}


/*** pre_server
 *
 *
 *	void pre_server():
 *
 *	Handle NQS network requests from a remote NQS client process.
 *	We are a server process for an NQS network client. If there
 *	is a lot of work to do, we will exec the Netdaemon code.
 *
 *	We are running with the effective and real user-id of root.
 */
static void pre_server (
	int newsd,			/* Socket-descr */
	struct sockaddr_in *from)	/* Address of the client */
{
	struct hostent *chostent;	/* Client hostent using OUR file */
	Mid_t client_mid;		/* Client machine-id using OUR nmap */
	struct passwd *pw = NULL;	/* Passwd entry */
	int integers;			/* Number of 32-bit precision signed-*/
					/* integer values present in the */
					/* received message packet */
	int strings;			/* Number of character/byte strings */
					/* in the received message packet */
	int i;				/* Loop index var */
	
	/*
	 *  Set the name of this process to a more appropriate string.
	 *  We use sprintf() to pad with spaces rather than null
	 *  characters so that disgusting programs like ps(1) which
	 *  walk the stack will not get confused.
	 */
#if	TEST
	sprintf (Argv0, "%-*s", Argv0size, "NQS Netdaemon");
#else
	sprintf (Argv0, "%-*s", Argv0size, "xNQS Netdaemon");
#endif
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netdaemon: born by fork.\n");
  
	/*
	 *  True NQS clients always bind to a port < IPPORT_RESERVED.
	 */
	if (htons ((u_short) from->sin_port) >= IPPORT_RESERVED) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Client on non-secure port (client port >= %1d).\n", IPPORT_RESERVED);
		sendandabort (newsd, TCMP_NONSECPORT);
	}
	/*
	 *  Determine the machine-id of the client.
	 */

	chostent = gethostbyaddr ((char *) &from->sin_addr,
				  sizeof (struct in_addr), AF_INET);

	if (chostent == (struct hostent *) 0) {
	    /*
	     *  The client host address is not recognized on this
	     *  system!
	     */
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING,"Netdaemon: client internet addr unknown to local host.\n");
	    sendandabort (newsd, TCMP_NETDBERR);
	}
  	else
  	{
	     sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Netdaemon: Connection from %s(%s)\n", chostent->h_name, inet_ntoa(from->sin_addr));
	}
  
	switch (nmap_get_mid (chostent, &client_mid)) {
	case NMAP_ENOMAP:
	case NMAP_ENOPRIV:
	case NMAP_EUNEXPECT:
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: client hostname %s unknown to local host.\n", chostent->h_name);
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: mid is %u.\n", client_mid);
	    sendandabort (newsd, TCMP_CLIMIDUNKN);
	case NMAP_SUCCESS:
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH,"Netdaemon: client %s has machine id %ud\n", chostent->h_name, client_mid);
	    break;
	}
	/*
	 *  Perform NQS connection protocol preamble.
	 */
	setsockfd (newsd);		/* Teach getsockch() */
	switch (interread (getsockch)) {
	case 0: 
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: got 1st packet.\n");
		break;
	case -1:
		/*
		 *  The remote client has closed the connection.
		 *  This network request session is over.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netdaemon: session concluded, exiting.\n");
		exit (0);
	case -2:			/* Bad packet received */
					/* Abort execution */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon received bad packet.\n");
		sendandabort (newsd, TCMP_PROTOFAIL);
	}
	/*
	 *  Otherwise, the packet is self-consistent.
	 *  We have a request to process.
	 */
	integers = intern32i();		/* Get number of 32-bit precision */
					/* integers in message packet */
	strings = internstr();		/* Get number of strings in packet */
#if	HAS_BSD_PIPE
	if (integers == 1 && strings == 0) {
		/*
		 *  The first message packet contains only a single integer.
		 *  The message packet must therefore be an NPK_SERVERCONN
		 *  packet, requesting the creation of a message packet relay
		 *  process.  NPK_SERVERCONN packets can ONLY come from
		 *  processes on the SAME machine.
		 */
		if (Locmid != client_mid) {
			/*
			 *  Client process is not local.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: NPK_SERVERCONN received from remote client.\n");
			sendandabort (newsd, TCMP_PROTOFAIL);
		}
		if (interr32i (1) != NPK_SERVERCONN) {
			/*
			 *  Bad packet type or protocol error.
			 */
			sendandabort (newsd, TCMP_PROTOFAIL);
		}
		/*
		 *  The client process is running on the local machine.
		 *  Become a local message packet relay process to the
		 *  local NQS daemon.
		 */
		relaypackets ();	/* Relay message packets to the */
	}				/* local NQS daemon */
#endif
	/*
	 *  The message packet is of the ordinary variety, originating from
	 *  a remote client machine.
	 */
	if (integers < 6 || strings < 3) {
		/*
		 *  The first message packet at a minimum must contain
		 *  these integers and strings.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon received message packet lacking appended info.\n");
		sendandabort (newsd, TCMP_PROTOFAIL);
	}
	integers -= 6;			/* Focus on non-appended integers */
	strings -= 3;			/* Focus on non-appended strings */
	if (interr32u (integers + 1) != NPK_MAGIC1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: client has bad magic.\n");
		sendandabort (newsd, TCMP_PROTOFAIL);
	}
	if ( ((int) interr32i (integers + 2) != NPK_LOAD) &&
	         ((int) interr32i (integers + 2) != NPK_RREQCOM) ) {
	    /* The 0 means map by username, not by nmap */
	    if ((pw = mapuser (interr32i (integers + 5), interrstr (strings + 2),
		client_mid, (char *) chostent->h_name, 0 ))
					== (struct passwd *) 0) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: mapuser() denies access.\n");
		sendandabort (newsd, TCMP_NOACCAUTH);
	    }
	    else
	    {
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Netdaemon: mapuser() mapped to user %s\n", pw->pw_name);
	    }
	  
	    
	}
	if (interr32u (integers + 3) != client_mid) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: client/local mid conflict.\n");
		sendandabort (newsd, TCMP_MIDCONFLCT);
	}
	if (interr32u (integers + 4) != Locmid) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: local/client mid conflict.\n");
		sendandabort (newsd, TCMP_MIDCONFLCT);
	}
	/*
	 *  Reopen the general parameters file.  In the future, we
	 *  will check the NQS network password for the client machine.
	 *  This password will be stored in the corresponding network
	 *  peer record stored in the parameters file.
	 *
	 *  The NQS parameters file must also be opened for the benefit
	 *  of execnetserver() (see below).
	 */
	if ((Paramfile = opendb (Nqs_params, O_RDONLY)) == NULL) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: unable to open general parameters file.\n");
		sendandabort (newsd, TCMP_INTERNERR);
	}
	/*
	 *  The packet has been minimally authenticated.
	 */
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netdaemon: Received packet from remote client.\n");
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netdaemon: Packet type is: %s\n",
		 netpacket(interr32i (integers + 2)) );
	if (integers != 0 || strings != 0) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netdaemon: Packet contents are as follows:\n");
		for (i = 1; i <= strings; i++) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netdaemon: Str [%1d] = ", i);
			showbytes (interrstr (i), interlstr (i));
		}
		for (i = 1; i <= integers; i++) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "D$Netdaemon: Integer [%1d] = %1ld\n", i, interr32i (i));
		}
	}
	/*
	 *  Based upon the network packet type, we perform a case
	 *  statement.  Each case exits.  Note that we ignore SIGTERM
	 *  beyond this point.
	 */
	signal (SIGTERM, SIG_IGN);
	switch ((int) interr32i (integers + 2)) {
	/*
	 *  Batch requests and device requests.
	 */
	case NPK_QUEREQ:
		if (strings == 0 && integers == 0) {
		  	assert(pw != NULL);
			execnetserver (newsd, pw, client_mid, "NPK_QUEREQ");
		}
		else badpacket ();
		break;
	case NPK_DELREQ:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netdaemon: got delreq\n");
		if (strings == 1 && integers == 5 ) {
		  	assert(pw != NULL);
			delreqserver (newsd, pw, client_mid, (char *)chostent->h_name,
				interr32i (1), interrstr (1), interr32i (2),
				interr32u (3), interr32i (4), interr32i (5));
		}
		else badpacket ();
		break;
	case NPK_COMMIT:
		/*
		 * NPK_COMMIT should never be used to start a conversation.
		 */
		badpacket ();
		break;
	/*
	 * File system work.
	 */
	case NPK_DONE:
		/*
		 * NPK_DONE should never be used to start a conversation.
		 */
		badpacket ();
		break;
	case NPK_MKDIR:
		if (strings == 0 && integers == 0 ) {
			badpacket ();
		}
		else badpacket ();
		break;
	case NPK_REQFILE:
		if (strings == 0 && integers == 0 ) {
		  	assert(pw != NULL);
			execnetserver (newsd, pw, client_mid, "NPK_REQFILE");
		}
		else badpacket ();
		break;
	case NPK_CPINFILE:
	case NPK_CPOUTFILE:
	case NPK_MVINFILE:
		if (strings == 0 && integers == 0 ) {
			badpacket ();
		}
		else badpacket ();
		break;
	case NPK_MVOUTFILE:
		if (integers == 0 && strings == 0) {
		  	assert(pw != NULL);
			execnetserver (newsd, pw, client_mid, "NPK_MVOUTFILE");
		}
		else badpacket ();
		break;
	/*
	 * Status
	 */
	case NPK_QLIMIT:
		if (integers == 2 && strings == 1) {
			qlimitserver (newsd, client_mid, (char *) chostent->h_name,
				interr32i (1), interr32i (2), interrstr (1));
		}
		else badpacket ();
		break;
	case NPK_QDEV:
	case NPK_QMGR:
		if (strings == 0 && integers == 0 ) {
			badpacket ();
		}
		else badpacket ();
		break;
	case NPK_QSTAT:
		if (integers == 2 && strings == 2) {
		  	assert(pw != NULL);
			qstatserver (newsd, pw, client_mid, (char *)chostent->h_name,
				interr32i (1), interr32i (2), interrstr (1),
				interrstr (2));
		}
		else badpacket ();
		break;
	case NPK_LOAD:
		if (integers == 2 && strings == 1) {
			loadserver (newsd, client_mid,
				interr32i (1), interr32i (2),  interrstr (1));
		}
		else badpacket ();
		break;
	case NPK_RREQCOM:
		if (integers == 4 && strings == 0) { 
			rreqcomserver (newsd, client_mid,
				 interr32i (2), interr32i (3), interr32i (4));
		} 
		else badpacket ();
		break;

	default:
		badpacket ();
		break;
	}
	exit (0);			/* This line not reached */
}


/*** execnetserver
 *
 *
 *	void execnetserver():
 *
 */
static void execnetserver (
	int newsd,		/* Socket descriptor */
	struct passwd *pw,	/* Whom we are acting on behalf of */
	Mid_t client_mid,	/* The client's machine id */
	char *op)		/* The operation */
{
	
	struct gendescr *descr;		/* NQS database information */
	char *argv [MAX_SERVERARGS + 1];/* Construct argv for Netdaemon here */
	char *environment;
	int envpsize;			/* First free byte in environment */
	int envpsizemax;
	char **envp;			/* Construct env for Netdaemon here */
	int envpcount;			/* First free slot in envp */
	int envpcountmax;
	long i;				/* Loop variable */
	int ngroups;			/* size of group set */
	gid_t gidset[NGROUPS_MAX];	/* group set */
#if 0
	char ** env_walker = NULL;	/* walk the environment */
#endif

	/*
	 * Lose our privileges.
	 */
	initgroups (pw->pw_name, pw->pw_gid);
	ngroups = getgroups (NGROUPS_MAX, gidset);
	setgroups (ngroups, gidset);
	setgid (pw->pw_gid);
	setuid (pw->pw_uid);
	seekdb (Paramfile, Udbnetprocs);
	descr = nextdb (Paramfile);
	if (descr->v.par.paramtype != PRM_NETPROCS) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Bad parameters file.\n");
		sendandabort (newsd, TCMP_INTERNERR);
	}
	/*
	 *  Enforce file descriptor associations:
	 *
	 *	0: new socket;
	 *	1,2,4: Log process pipe;
	 *	WRITE_FIFO: Local daemon FIFO;
	 *	remaining: closed
	 */
	if (newsd != 0) {
		close (0);
		fcntl (newsd, F_DUPFD, 0);
		close (newsd);
	}
	i = sysconf ( _SC_OPEN_MAX );		/* #of file descrs per proc */
	while (--i >= 5) close (i);
	if (parseserv (descr->v.par.v.netprocs.netserver,
			argv) == -1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Parse of server path and args failed.\n");
		sendandabort (newsd, TCMP_INTERNERR);
	}
 	
 	/* Dynamic allocation of environment */
 	envpcountmax = 4;
 	envpsizemax = 9 + 4 + strlen(op) + 21;
 	envp = (char **)malloc(envpcountmax*sizeof(char *));
 	if (envp == NULL) {
 	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Memory allocation error #1\n");
 	    sendandabort (newsd, TCMP_INTERNERR);
 	}
 	environment = (char *)malloc(envpsizemax*sizeof(char));
 	if (environment == NULL) {
 	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Memory allocation error #2\n");
 	    sendandabort (newsd, TCMP_INTERNERR);
 	}
 	
	envpcount = 0;
	envpsize = 0;
	envp [envpcount++] = environment;
	sprintf (environment, "DEBUG=%1d", sal_debug_GetLevel());
	envpsize += strlen (environment) + 1;
	envp [envpcount++] = environment + envpsize;
	sprintf (environment + envpsize, "OP=%s", op);
	envpsize += strlen (environment + envpsize) + 1;
	envp [envpcount++] = environment + envpsize;
	sprintf (environment + envpsize, "CLIENTMID=%lu", (unsigned long) client_mid);
	envpsize += strlen (environment + envpsize) + 1;
	/*
	 * Mark the end of the array of character pointers.
	 */
	envp [envpcount] = (char *) 0;

#if 0
	/* output the environment */
	env_walker = envp;

	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "netdaemon: environment for netserver is\n");
	while (*env_walker != NULL) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "netdaemon: %s\n", *env_walker);
		env_walker++;
	}
#endif
	/* MOREHERE: set up a SIGPIPE handler in the exec'd process */
	execve (argv [0], argv, envp);
	free(environment);
	free(envp);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Exec error.  Errno=%d\n", errno);
	sendandabort (newsd, TCMP_INTERNERR);
}


/*** delreqserver
 *
 *
 *	void delreqserver():
 *
 */
static void
delreqserver (
	int sd,			/* Socket connection */
	struct passwd *pw,	/* Password entry on this machine */
	Mid_t client_mid,	/* Our guess as to their mid */
	char *chostname,	/* Our guess as to their principal name */
	uid_t whomuid,		/* The request belongs to this user */
	char *whomname,		/* The request belongs to this user */
	long orig_seqno,	/* Original sequence number */
	Mid_t orig_mid,		/* Original machine id */
	int sig,		/* Which signal to send */
	int states)		/* Only proceed if in these states */
{

	char packet [MAX_PACKET];	/* To hold our reply */
	int packetsize;			/* Bytes in our reply */
	long tcm;			/* Transaction completion code */
	int ngroups;			/* size of group set */
	gid_t gidset[NGROUPS_MAX];	/* Group set */
	
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: NPK_DELREQ.\n");
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: whomuid = %d.\n", whomuid);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: whomname = %s.\n", whomname);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: client_mid = %u.\n", client_mid);
        sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: chostname = %s.\n", chostname);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: uid = %d.\n", pw->pw_uid);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: seqno = %d.\n", orig_seqno);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: orig_mid = %u.\n", orig_mid);
	/*
	 * Lose our privileges.
	 */
	initgroups (pw->pw_name, pw->pw_gid);
	ngroups = getgroups (NGROUPS_MAX, gidset);
	setgroups (ngroups, gidset);
        setgid (pw->pw_gid);
        setuid (pw->pw_uid);

	tcm = delreq (pw->pw_uid, orig_seqno, orig_mid, Locmid, sig, states);
	interclear ();
	interw32i (tcm);
	if ((packetsize = interfmt (packet)) == -1) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Packet would be too large.\n");
	    errno = 0;		/* Not a system call error */
	    server_abort (tcm);	/* Exit nicely */
	}
	write (sd, packet, packetsize);	/* Write response */
        close (sd);
	exiting();
	exit (0);
}
/*** rreqcomserver
 *
 *
 *	void rreqcomserver():
 *
 */
static void
rreqcomserver (
	int sd,			/* Socket connection */
	Mid_t client_mid,	/* Our guess as to their mid */
	int req_seqno,		/* Sequence number of request that completed */
	Mid_t req_mid,		/* Mid of the request that completed */
	int no_jobs)		/* Number of jobs remaining on that mid */
{


    char packet [MAX_PACKET];	/* To hold our reply */
    int packetsize;		/* Bytes in our reply */
    long tcm;			/* Transaction completion code */

    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: NQK_RREQCOM.\n");
    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: Client mid = %u.\n",  client_mid);
    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: Request mid = %u.\n", req_mid);
    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: Request seqno = %d.\n", req_seqno);
    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: # jobs = %d.\n", no_jobs);
  
    interclear();
    interw32u(client_mid);
    interw32u(req_mid);
    interw32i( (long) req_seqno);
    interw32i( (long) no_jobs);
    tcm = inter( PKT_RREQCOM);
    interclear ();
    tcm = TCMP_CONTINUE;		/* Tell them what we think */
    interw32i (tcm);
    if ((packetsize = interfmt (packet)) == -1) {
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Packet would be too large.\n");
	errno = 0;		/* Not a system call error */
	server_abort (tcm);	/* Exit nicely */
    }
    write (sd, packet, packetsize);	/* Write response */
    close (sd);
    exiting();
    exit (0);
    
}
/*** loadserver
 *
 *
 *	void loadserver():
 *
 */
static void
loadserver (
	int sd,			/* Socket connection */
	Mid_t client_mid,	/* Our guess as to their mid */
	int version,		/* Version of this packet */
	int nqs_jobs,	/* Number of current NQS jobs running on that node */
	char *string)
{

    char packet [MAX_PACKET];	/* To hold our reply */
    int packetsize;		/* Bytes in our reply */
    long tcm;			/* Transaction completion code */

    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: NPK_LOAD.\n");
    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: Client mid = %u.\n",  client_mid);
    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: version = %d\n",  version);
    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: NQS jobs = %d\n",  nqs_jobs);
    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: string = %s\n",  string);
  
    interclear();
    interw32u( client_mid);
    interw32i( (long) version);
    interw32i( (long) nqs_jobs);
    interwstr( string );
    tcm = inter( PKT_LOAD);
    if ((tcm == TCML_COMPLETE)  || (tcm == TCMP_COMPLETE) )
        tcm = TCMP_CONTINUE;
    interclear ();
    interw32i (tcm);
    if ((packetsize = interfmt (packet)) == -1) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Packet would be too large.\n");
	    errno = 0;		/* Not a system call error */
	    server_abort (tcm);	/* Exit nicely */
    }
    write (sd, packet, packetsize);	/* Write response */
    close (sd);
    exiting();				/* Close pipe to nqsdaemon */
    exit (0);
    
}
/*** qlimitserver
 *
 *
 *	void qlimitserver():
 *
 */
static void
qlimitserver (
	int sd,			/* Socket connection */
	Mid_t client_mid,	/* Our guess as to their mid */
	char *chostname,	/* Our guess as to their principal name */
	int whomuid,		/* mapped uid */
	int flags,		/* Display flags */
	char *whomname)		/* mapped name */
{
	
	char packet [MAX_PACKET];	/* To hold our reply */
	int packetsize;			/* Bytes in our reply */
	
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: NPK_QLIMIT.\n");
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: whomuid = %d.\n", whomuid);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: whomname = %s.\n", whomname);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: client_mid = %u.\n", client_mid);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: chostname = %s.\n", chostname);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: flags = %o.\n", flags);
  
	interclear ();
	interw32i (TCMP_CONTINUE);
	if ((packetsize = interfmt (packet)) == -1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Packet would be too large.\n");
		errno = 0;		/* Not a system call error */
		server_abort (TCML_INTERNERR);	/* Exit nicely */
	}
	write (sd, packet, packetsize);	/* Write response */
	/*
	 *  Cause file descriptors 1 and 2 to point to the socket.
	 */
	outanderrto (sd);
	if ((Paramfile = opendb (Nqs_params, O_RDONLY)) == NULL) {
	     printf ("Qlimit (FATAL): Unable to open the NQS parameters database file.\n");
	     exit (1);
	}
	shoalllim(Paramfile,  flags);
	exit (0);
}
/*** qstatserver
 *
 *
 *	void qstatserver():
 *
 */
static void
qstatserver (
	int sd,			/* Socket connection */
	struct passwd *whompw,	/* Whom wants to know */
	Mid_t client_mid,	/* Our guess as to their mid */
	char *chostname,	/* Our guess as to their principal name */
	int flags,		/* Display flags */
	int whomuid,		/* -u mapped uid */
	char *queuename,	/* If 0 length, then show all queues */
	char *whomname)		/* -u mapped name */
{
	
	char packet [MAX_PACKET];	/* To hold our reply */
	int packetsize;			/* Bytes in our reply */
        struct reqset *reqset;          /* request set we are interested in */

        reqset = NULL;                  /* assume no request set */
	
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netdaemon: NPK_QSTAT.\n");
	/*
	 *  Open the non-network queue descriptor file.
	 */
	if ((Queuefile = opendb (Nqs_queues, O_RDONLY)) == NULL) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: unable to open Queuefile.\n");
		sendandabort (sd, TCMP_INTERNERR);
	}
        if ((Qcomplexfile = opendb (Nqs_qcomplex, O_RDONLY)) == NULL) {
                sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: unable to open Qcomplexfile.\n");
		sendandabort (sd, TCMP_INTERNERR);
        }
	/*
	 *  Open the queue/device/destination mapping descriptor file.
	 */
	if ((Qmapfile = opendb (Nqs_qmaps, O_RDONLY)) == NULL) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: unable to open Qmapfile.\n");
		sendandabort (sd, TCMP_INTERNERR);
	}
	/*
	 *  Open the pipe queue destination descriptor file.
	 */
	if ((Pipeqfile = opendb (Nqs_pipeto, O_RDONLY)) == NULL) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: unable to open Pipeqfile.\n");
		sendandabort (sd, TCMP_INTERNERR);
	}
	interclear ();
	interw32i (TCMP_CONTINUE);
	if ((packetsize = interfmt (packet)) == -1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Packet would be too large.\n");
		errno = 0;		/* Not a system call error */
		server_abort (TCML_INTERNERR);	/* Exit nicely */
	}
	write (sd, packet, packetsize);	/* Write response */
	/*
	 *  Cause file descriptors 1 and 2 to point to the socket.
	 */
	outanderrto (sd);
        /*
         *      This is kludge to allow old NQS versions to network
         *      to this machine.  We can tell as an old style NQS
         *      will not have SHO_BATCH SHO_DEVICE SHO_PIPE
         */
        /* if ( (!(flags & SHO_PIPE )) && (!(flags & SHO_BATCH)) &&
         *       (!(flags & SHO_DEVICE)))
         *       flags |= (SHO_PIPE | SHO_BATCH | SHO_DEVICE);
	 */

	/*
	 * If the user wants to see jobs originating on her machine,
	 * set up the reqset data structure here.
	 */
	if (flags & SHO_R_ORIGIN) {
        	reqset = (struct reqset *) malloc (sizeof(reqset) );
                if (reqset == NULL) {
                    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon:  Error allocating memory.\n");
                    errno = 0;
		    server_abort ( TCML_ENOMEM );
                }
                reqset->v.reqid.orig_mid = client_mid;
                reqset->v.reqid.orig_seqno = 0;
                reqset->selecttype = SEL_REQID;
                reqset->next = NULL;
	}

	/*
	 *  To deny a user the right to learn about another user's
	 *  requests, add code in shoqbyname.c or qstat.c, not here.
	 */
	/* 
	 * Cosmic V2 code:
         * if (queuename [0] == '\0') {
         *       list(Queuefile,Qcomplexfile,NULL,flags,NULL,
	 *			Qmapfile,Pipeqfile);
	 *        }
         * else {
         *       list(Queuefile,Qcomplexfile,queuename,flags,NULL,
	 *			Qmapfile,Pipeqfile);
         * }
	 */

        if ( flags & SHO_R_COMPLEX) {
#if		HAS_BSD_PIPE
            showcomplex( Queuefile, queuename, flags, whompw->pw_uid, 
			reqset, daepres(Queuefile), Qmapfile, Pipeqfile, 
			Qcomplexfile);
#else
            showcomplex( Queuefile, queuename, flags, whompw->pw_uid, 
			reqset, daepres(), Qmapfile, Pipeqfile, 
			Qcomplexfile);
#endif
	} else {			/* get info about a queue */
	    if (queuename [0] == '\0' ) {
		/*
		 *  Information should be shown about all queues.
		 */
#if		HAS_BSD_PIPE
		shoallque (Queuefile, flags, whompw->pw_uid,
			  reqset, daepres (Queuefile),
			   Qmapfile, Pipeqfile, 1, Qcomplexfile);
#else
		shoallque (Queuefile, flags, whompw->pw_uid,
			  reqset, daepres (),
			   Qmapfile, Pipeqfile, 1, Qcomplexfile);
#endif
	    } else {
#if		HAS_BSD_PIPE
		shoqbyname (Queuefile, queuename, flags, whompw->pw_uid,
			   reqset, daepres (Queuefile),
			    Qmapfile, Pipeqfile, Qcomplexfile);
#else
		shoqbyname (Queuefile, queuename, flags, whompw->pw_uid,
			   reqset, daepres (),
			    Qmapfile, Pipeqfile, Qcomplexfile);
#endif
	    }
	}
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "qstatserver: done\n");
	exit (0);
}


#if	HAS_BSD_PIPE
/*** relaypackets
 *
 *
 *	void relaypackets():
 *
 *	The network server process is a "relay message packet" server,
 *	passing message packets from client processes to the local NQS
 *	daemon.
 *
 *	This function exists because Berkeley style UNIX does not support
 *	named-pipes.  This little function, and the additional network
 *	packet of: NPK_SERVERCONN, allow us to get by on Berkeley style UNIX
 *	systems.
 */
  
static void relaypackets (void)
{
	char packet [MAX_PACKET];	/* Relay packet */
	register int n_integers;	/* Number of integers in packet */
	register int n_strings;		/* Number of strings in packet */
	register int i;

	while (1) {
		/*
		 *  Loop until EOF from client.
		 */
		switch (interread (getsockch)) {
		case 0:	/*
			 *  A message packet has been received from the
			 *  local client process.  Forward the message
			 *  packet in a single chunk to the local NQS
			 *  daemon.
			 */
			interclear();	/* Clear outgoing message packet */
			n_integers = intern32i();
			n_strings = internstr();
			for (i = 1; i <= n_integers; i++) {
				/*
				 *  Forward the integer data.
				 */
				if (interr32sign (i)) {
					interw32i (interr32i (i));
				}
				else interw32u (interr32u (i));
			}
			for (i = 1; i <= n_strings; i++) {
				/*
				 *  Forward the byte/string data.
				 */
				interwbytes (interrstr (i),
					    (unsigned) interlstr (i));
			}
			i = interfmt (packet);	/* Format message packet */
			/*
			 *  Now, send the message packet to the local
			 *  NQS daemon.
			 */
			if (write (WRITE_FIFO, packet, i) != i) {
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Relay packet write error.\n");
				server_abort ( NULL );	/* Abort */
			}
			break;
		case -1:
			/*
			 *  The local client process has closed the
			 *  connection, or has exited.
			 */
			exit (0);
		case -2:
			/*
			 *  A bad message packet was received from the
			 *  local client process.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "E$Netdaemon: Bad message packet received.\n");
			errno = 0;		/* Not a system call error */
			server_abort( NULL );		/* Abort */
		}
	}
}
#endif


/*** badpacket
 *
 *
 *	void badpacket():
 *
 *	Write a message to the NQS log process concerning the bad packet
 *	received.
 */
static void badpacket ()
{

	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: bad network packet received.\n");
	errno = 0;			/* Not a system call error */
	server_abort (TCML_INTERNERR);		/* Abort execution */
}


/*** net_abort
 *
 *
 *	void net_abort():
 *
 *	Abort Network daemon execution, writing a message to
 *	the NQS log process.
 */
static void net_abort(void)
{
	if (errno) 
          sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "%s.\n", asciierrno());
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon aborted.\n");
	exit (1);
}


/*** net_daeshutdown
 *
 *
 *	void net_daeshutdown():
 *	Catch SIGTERM signal to shutdown.
 */
static void net_daeshutdown( int unused )
{
	signal (SIGTERM, SIG_IGN);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon shutting down.\n");
	exit (0);
}


/*** outanderrto
 *
 *
 *	void outanderrto():
 *
 *	Force STDOUT and STDERR to be directed at the socket
 *	connection established between the remote client and
 *	this server.  The streams stdout and stderr stay open.
 */
static void outanderrto (int sd)
{
	static char stdout_buffer [SOCKBUFSIZ];	/* Stdout output buffer */
	static char stderr_buffer [SOCKBUFSIZ];	/* Stderr output buffer */
	
	fflush (stdout);
	fflush (stderr);
	close (STDOUT);			/* Close original STDOUT */
	close (STDERR);			/* Close original STDERR */
	if (fcntl (sd, F_DUPFD, STDOUT) != STDOUT ) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon cannot replicate socket descriptor.\n");
		sendandabort (sd, errnototcm ());
	}
	if (fcntl (sd, F_DUPFD, STDERR) != STDERR) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon cannot replicate socket descriptor.\n");
		sendandabort (sd, errnototcm ());
	}
	/*
	 *  Make sure that formatted output returned to the remote
	 *  client process is transmitted in blocks for greater
	 *  efficiency.
	 */
#if	IS_POSIX_1 | IS_SYSVr4
  	setvbuf (stdout, stdout_buffer, _IOFBF, SOCKBUFSIZ);
	setvbuf (stderr, stderr_buffer, _IOFBF, SOCKBUFSIZ);
#else
#if	IS_BSD
	setbuffer (stdout, stdout_buffer, SOCKBUFSIZ);
	setbuffer (stderr, stderr_buffer, SOCKBUFSIZ);
#else
BAD SYSTEM TYPE
#endif
#endif
}

/*** reapchild
 *
 *	void reapchild ( int ):	
 *	SIGCHLD handler.
 *
 *  Everyone now catches SIGCHLD and/or SIGCLD
 *
 */
static void reapchild ( int signum )
{
    int status;
    int errno_save;

    errno_save = errno;
    while ( waitpid(-1,&status,WNOHANG) > 0) {
    }
    errno = errno_save;
    /* Reinstall yourself */
    signal (signum, reapchild);
}

/*** reload
 *
 *
 *	void reload ():	
 *	When the local daemon updates a parameter,
 *	it will send a SIGHUP to the net daemon.  The net daemon
 *	should then update its copy of the parameters off the disk.
 *
 */
static void reload ( int unused )
{
	signal (SIGHUP, reload);
	seekdb (Paramfile, 0);
	ldparam ();
}


/*** sendandabort
 *
 *
 *	void sendandabort ():
 *
 *	Abort Network server execution, sending a TCM code
 *	to the client and writing a message to the NQS log.
 */
static void sendandabort (int sd, long tcm)
{
	char packet [MAX_PACKET];
	int packetsize;
	
	interclear ();
	interw32i (tcm);
	if ((packetsize = interfmt (packet)) == -1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon: Packet would be too large.\n");
		errno = 0;		/* Not a system call error */
		server_abort ( TCML_INTERNERR );	/* Exit nicely */
	}
	write (sd, packet, packetsize);	/* Write response */
	errno = 0;			/* Not a system call error */
	server_abort ( tcm );
}


/*** server_abort
 *
 *
 *	void server_abort():
 *
 *	Abort Network server execution, writing a message to
 *	the NQS log process.
 */
static void server_abort (long tcm )
{
	if (tcm != TCMP_NOACCAUTH) {
	    if (errno) 
	      sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "%s.\n", asciierrno());
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netdaemon aborting.\n");
	}
	exit (1);
}


/*** showbytes
 *
 *
 *	void showbytes():
 *	Show byte/string datum as received in message packet.
 */
static void showbytes (char *bytes, unsigned int length)
{
  #if 0
	register short ch;		/* Byte/character */
	register int col;		/* Column counter */
	short truncate_flag;		/* String truncated flag */

	col = 0;			/* # of columns written */
	truncate_flag = 0;
	if (length > 300) {
		truncate_flag = 1;	/* Truncate to 300 chars on */
		length = 300;		/* display */
	}
	while (length--) {
		if (col >= 24) {
			fputc ('\n', logfile);
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_, "D$Netdaemon: ");
			col = 0;
		}
		ch = (*bytes & 0377);	/* Mask to 8 bits */
		bytes++;		/* Increment to next byte */
		if (ch < ' ' || ch > 127) {
			/*
			 *  This character is unprintable, or is a tab
			 *  character.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_, "[\\%03o]", ch);
			col += 6;
		}
		else {
			fputc (ch, logfile);
			col += 1;
		}
	}
	fputc ('\n', logfile);
	if (truncate_flag) {
		/*
		 *  String truncated.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_, "D$Netdaemon: String truncated.\n");
	}
  #endif
}

/*** netpacket
 *
 *   char *netpacket():
 *   Return string of type of packet
 *
 */
static char *netpacket(long index)
{
    static char *quereq = "NPK_QUEREQ";
    static char *delreq = "NPK_DELREQ";
    static char *commit = "NPK_COMMIT";
    static char *done   = "NPK_DONE";
    static char *mkdir  = "NPK_MKDIR";
    static char *reqfile = "NPK_REQFILE";
    static char *cpinfile = "NPK_CPINFILE";
    static char *cpoutfile = "NPK_CPOUTFILE";
    static char *mvinfile = "NPK_MVINFILE";
    static char *mvoutfile = "NPK_MVOUTFILE";
    static char *suspendreq = "NPK_SUSPENDREQ";
    static char *qdev = "NPK_QDEV";
    static char *qlimit = "NPK_QLIMIT";
    static char *qmgr = "NPK_QMGR";
    static char *qstat = "NPK_QSTAT";
    static char *load = "NPK_LOAD";
    static char *rreqcom = "NPK_RREQCOM";
    static char *unknown = "Packet type is unknown";

    switch (index) {
      case NPK_QUEREQ:
	return(quereq);
      case NPK_DELREQ:
	return(delreq);
      case NPK_COMMIT:
	return(commit);
      case NPK_DONE:
	return(done);
      case NPK_MKDIR:
	return(mkdir);
      case NPK_REQFILE:
	return(reqfile);
      case NPK_CPINFILE:
	return(cpinfile);
      case NPK_CPOUTFILE:
	return(cpoutfile);
      case NPK_MVINFILE:
	return(mvinfile);
      case NPK_MVOUTFILE:
	return(mvoutfile);
      case NPK_SUSPENDREQ:
        return (suspendreq);
      case NPK_QDEV:
        return(qdev);
      case NPK_QLIMIT:
        return(qlimit);
      case NPK_QMGR:
        return (qmgr);
      case NPK_QSTAT:
        return(qstat);
      case NPK_LOAD:
        return(load);
      case NPK_RREQCOM:
        return(rreqcom);
      default:
        return(unknown);
    }
}
