/*
 * libnqs/misc/getfilnam.c
 * 
 * DESCRIPTION:
 *
 *	Function for building file names in directory independent installation.
 *	Function getfilnam checks the environment from specific variables.
 *	If set, the fully qualified name returned becomes the concatenation
 *	of the string defined by the environment variable and a suffix
 *	supplied by the calling function.  If the environment is not set,
 *	the fully qualified name returned becomes the compiled in default
 *	string concatenated with the suffix.
 *
 *	CAUTION:  This function allocates memory for the fully qualified
 *		  file name string.  It is the responsibility of the calling
 *		  function to release this memory when no longer needed.
 *
 *	Author:
 *	-------
 *	Chuck Keagle, Boeing Computer Services
 *	August 11, 1992
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 */

#include <sys/param.h>

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <stdlib.h>
#include <string.h>
#if TEST_GETFILNAM | TEST_GETENVIRON | TEST_BUILDENV
#include <libnqs/nqsdirs.h>
#else
#include <libnqs/nqsxdirs.h>
#endif
#include <SETUP/General.h>

char *getfilnam (char *suffix, short dirtype)
{
    char *homedir;
    char *name;
    char *prefix;

    assert (suffix != NULL);
    /*
     *  Find the appropriate environment variable.  Also determine the
     *  compiled in default which is used if the environment variable is
     *  undefined.
     */
    switch (dirtype)
    {
    case LIBDIR:
	homedir = getenv (Nqs_libdir);
	prefix = NQS_LIBEXE;
	break;
    case MAPDIR:
	homedir = getenv (Nqs_nmapdir);
	prefix = NQS_NMAP;
	break;
    case SPOOLDIR:
	homedir = getenv (Nqs_spooldir);
	prefix = NQS_SPOOL;
	break;
    default:
	return (char *)NULL;
    }
    /*
     *  If homedir is non-null, concatenate it to the supplied suffix.
     *  If it is null, use the compiled in prefix.  Return a pointer to
     *  the fully qualified name.
     */
    if (homedir == (char *)NULL)
    {
	name = (char *)malloc((unsigned)(strlen(prefix) + strlen(suffix) + 2));
	sprintf (name, "%s/%s", prefix, suffix);
    }
    else
    {
	name = (char *)malloc ((unsigned)(strlen(homedir) + strlen(suffix) + 2));
	sprintf (name, "%s/%s", homedir, suffix);
    }
    return (name);
}

/*++ relfilnam
 *
 * DESCRIPTION:
 *
 *	Function relfilnam frees the memory that was allocated by getfilnam.
 *	Author:
 *	-------
 *	Chuck Keagle, Boeing Computer Services
 *	August 11, 1992
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 */
void relfilnam (char *string)
{
    assert(string != NULL);
  
    free (string);
    return;
}

/*++ getenviron
 *
 * DESCRIPTION:
 *
 *	Function getenviron obtains a pointer to the supplied environment
 *	variable.  The pointer points to the beginning of the name=value
 *	string.  By contrast, getenv returns a pointer to the value
 *	portion of the string.  If the environment does not exist,
 *	getenviron returns a null pointer.
 *
 *	Author:
 *	-------
 *	Chuck Keagle, Boeing Computer Services
 *	August 11, 1992
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 */
char *getenviron (char *name)
{
    extern char **environ;
    char **table;

    /*
     *  Find the specified variable (Uh-huh).
     *	Environment table is terminated by a null pointer.
     *  Be careful not to smash the current environment.
     */
  
    assert (name != NULL);
    assert (environ != NULL);
  
    table = environ;
    while ((*table != (char *)NULL) && strncmp (name, *table, strlen (name)))
    	table++;
    return (*table);
}

/*++ buildenv
 *
 * DESCRIPTION:
 *
 *	Function buildenv builds the NQS directory independent environment
 *	from the contents of the nqs.config file in the directory pointed
 *	to by the NQS_HOME environment variable.  If the putenv call fails,
 *	buildenv returns an error.  It is not an error for the NQS_HOME
 *	environment variable to be undefined.
 *
 *	Author:
 *	-------
 *	Chuck Keagle, Boeing Computer Services
 *	August 11, 1992
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 */
int buildenv()
{
    FILE *fp;
    char *envp;
    char *homedir;
#ifdef IS_UNICOS
    char string [PATH_MAX];
#else
    char string [MAXPATHLEN];
#endif

    /*
     *  Find the nqs.config file in the directory pointed to by the
     *  $NQS_HOME environment variable.  If the $NQS_HOME environment
     *  variable does not exist, there is no error.  The administrator
     *  has installed NQS in the default directories.
     */
    if ((homedir = getenv ("NQS_HOME")) != (char *)NULL)
    {
	sprintf (string, "%s/%s", homedir, "nqs.config");
	if ((fp = fopen (string, "r")) != (FILE *)NULL)
	{
	    /*
	     *  When reading the config file, ignore lines that begin
	     *  with either a # sign or a newline character.  All other
	     *  lines are placed into the environment after the
	     *  newline character is deleted.
	     */
	    while (fgets (string, sizeof (string), fp) != (char *)NULL)
	    {
		if (string [0] != '#' && string [0] != '\n')
		{
		    envp = (char *)malloc (strlen (string));
		    strncpy (envp, string, strlen (string));
		    envp [strlen (string) - 1] = '\0';
		    if (putenv (envp)) return (0);
		}
	    }
	    fclose (fp);
	}
    }
    return (~0);
}

#ifdef TEST_GETFILNAM

main()
{
    char *name;
    char *getfilnam();
    void relfilnam();

    name = getfilnam ("root", SPOOLDIR);
    printf ("file name = %s\n", name);
    relfilnam (name);
    exit (0);
}
#endif

#ifdef TEST_GETENVIRON

main()
{
    char *name;
    char *getenviron();

    name = getenviron ("Z99_FOO");
    printf ("getenviron for Z99_Foo returned %s\n", name);
    putenv ("Z99_FOO=Bar");
    printf ("Environment Variable Z99_FOO set to Bar\n");
    name = getenviron ("Z99_FOO");
    printf ("getenviron for Z99_FOO returned %s\n", name);
    exit (0);
}
#endif

#ifdef TEST_BUILDENV

main()
{
    int buildenv();

    if (buildenv())
	system ("env");
    else
	printf ("putenv failed\n");
    exit (0);
}
#endif
