/*
 * libnqs/queue/qdel_by_req.c
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/transactcc.h>		/* Transaction completion codes */
#include <libnqs/nqsxdirs.h>
#include <unistd.h>
#include <ctype.h>

extern char *Qdel_prefix;

static int qdel_selrequest ( struct qentry *qentry, short sig, uid_t whomuid, char *req_pat, int confirm );

int qdel_by_req(
	char	*req_pat,		/* Request pattern */
	uid_t	real_uid,		/* user id to check */
	short	sig,			/* signal to send */
	int	confirm)		/* true if confirm deletion */
{
	struct confd *queuefile;       /* MUST be the NQS queue file */
	short daepresent;	       	/* is NQS daemon present? */
        register struct gendescr *descr;
        int fd;                         /* Queue ordering file descriptor */
        int i;                          /* Loop var */
        struct qentry cache [QOFILE_CACHESIZ];
                                        /* Queue ordering file cache buffer */
        int cacheindex;                 /* Current buffer cache index */
        int cachesize;                  /* Number of queue entries in read */
                                        /* cache buffer */
	int	tot_requests;		/* total number of requests */


	/* debugging
	 * printf("Request pattern is %s\n",req_pat);
	 * printf("Uid is %d\n", real_uid);
	 * printf("Signal is %d\n", sig);
	 */

        if ((queuefile = opendb (Nqs_queues, O_RDONLY)) == NULL) {
                fprintf (stderr, "%s(FATAL): Unable to open the NQS queue ",
                         Qdel_prefix);
                fprintf (stderr, "definition database file.\n");
                exit (2);
        }
        seekdb (queuefile, 0L); /* Seek to the beginning */
        descr = nextdb (queuefile);
        if (opendb (Nqs_qmaps, O_RDONLY) == NULL) {
                fprintf (stderr, "%s(FATAL): Unable to open the NQS queue/",
                         Qdel_prefix);
                fprintf (stderr, "device/destination\n");
                fprintf (stderr, "%s(FATAL): mapping database file.\n",
                         Qdel_prefix);
                exit (3);
        }
        if (opendb (Nqs_pipeto, O_RDONLY) == NULL) {
                fprintf (stderr, "%s(FATAL): Unable to open the NQS pipe-",
                         Qdel_prefix);
                fprintf (stderr, "queue destination database file.\n");
                exit (4);
        }
#if     HAS_BSD_PIPE
        daepresent = daepres (queuefile);
#else
        daepresent = daepres();
#endif

        while (descr != (struct gendescr *)0) {

        	fd = openqord (queuefile, descr);
        	/*
         	 *  fd >= 0 if the queue has requests, and we successfully
         	 *          opened the queue ordering file.
         	 *  fd = -1 if the queue has no requests.
         	 *  fd = -2 if the queue has requests, but an error prevented
         	 *          us from opening the queue ordering file for
         	 *          the queue.
         	 *  fd = -3 if the queue was deleted.
         	 */
        	if (fd < -1) return (fd);       /* Error or queue was deleted */

		tot_requests = descr->v.que.departcount +
			descr->v.que.runcount +
			descr->v.que.stagecount +
			descr->v.que.queuedcount +
			descr->v.que.waitcount +
			descr->v.que.holdcount +
			descr->v.que.arrivecount;
		cacheindex = 0;
		cachesize = 0;
        	for (i = 0; i < tot_requests; i++) {
        		if (cacheindex >= cachesize) {
                        	cachesize = read (fd, (char *) cache,
                                                  sizeof (cache));
                        	cachesize /= sizeof (struct qentry);
                        	cacheindex = 0;
                	}
                	qdel_selrequest (&cache [cacheindex], sig,
                                        real_uid, req_pat,confirm);
                	cacheindex++;
        	}
        descr = nextdb (queuefile);  /* Get the next queue */
        }
  	return 0;

}
static int qdel_selrequest (
	struct qentry *qentry, 		/* Request queue entry */
	short  sig,                     /* signal */
	uid_t  whomuid,                 /* Whom we are asking about */
	char   *req_pat,	        /* compiled request pattern */
	int    confirm)			/* true if confirm deletion */
{
	char	line[128];

	/* printf ("Whomuid = %d\n", whomuid); */
        if (whomuid != 0) {
		if  (qentry->uid != whomuid) 
                                return (0);     /* Request will not be */
						/* deleted */
	}
	qentry->reqname_v[MAX_REQNAME] = '\0';
	/* printf("Request name is %s\n", qentry->reqname_v ); */
	if (matche (req_pat, qentry->reqname_v) == 1) {
		/* printf("Found a live one.\n"); */
		if (confirm) {
			printf("Delete request %s [Y/N/Q]? ", 
						qentry->reqname_v);
			(void) fflush (stdout);
			(void) fgets(line, 128, stdin);
			line[0] = toupper(line[0]);
			if (line[0] == 'Q') exit(0);
			if (line[0] != 'Y') return (1);
		}
                qdel_diagdel (delreq (whomuid, qentry->orig_seqno,
                                 qentry->orig_mid, 0, sig,
                                 RQS_DEPARTING | RQS_RUNNING | RQS_STAGING |
                                 RQS_QUEUED | RQS_WAITING | RQS_HOLDING |
                                 RQS_ARRIVING), qentry->reqname_v);

	}
        return (0);                     /* It wasn't one of the requests */
                                        /* we are looking for. */
}

/*** qdel_diagdel
 *
 *
 *      qdel_diagdel():
 *      Diagnose delreq() completion code.
 */
void qdel_diagdel (long code, char *reqid)
{
        switch (code) {
        case TCML_INTERNERR:
                printf ("Internal error.\n");
                exiting();              /* Delete communication file */
                exit (1);
        case TCML_NOESTABLSH:
                printf ("Unable to establish inter-process communications ");
                printf ("with NQS daemon.\n");
                printf ("Seek staff support.\n");
                exiting();              /* Delete communication file */
                exit (1);
        case TCML_NOLOCALDAE:
                printf ("The NQS daemon is not running.\n");
                printf ("Seek staff support.\n");
                exiting();              /* Delete communication file */
                exit (1);
        case TCML_NOSUCHREQ:
                printf ("Request %s does not exist.\n", reqid);
                break;
        case TCML_NOSUCHSIG:
                printf ("Request %s is running, and the ", reqid);
                printf ("specified signal is\n");
                printf ("not recognized by the execution machine.\n");
                break;
        case TCML_NOTREQOWN:
                printf ("Not owner of request %s.\n", reqid);
                break;
        case TCML_PEERDEPART:
                printf ("Request %s is presently being routed by a ", reqid);
                printf ("pipe queue,\n");
                printf ("and this NQS implementation does not support the ");
                printf ("deletion of a\n");
                printf ("request under such conditions.\n");
                break;
        case TCML_PROTOFAIL:
                printf ("Protocol failure in inter-process communications ");
                printf ("with NQS daemon.\n");
                printf ("Seek staff support.\n");
                exiting();              /* Delete communication file */
                exit (1);
        case TCML_REQDELETE:
                printf ("Request %s has been deleted.\n", reqid);
                break;
        case TCML_REQRUNNING:
                printf ("Request %s is running.\n", reqid);
                break;
        case TCML_REQSIGNAL:
                printf ("Request %s is running, and has ", reqid);
                printf ("been signalled.\n");
                break;
        default:
                printf ("Unexpected completion code from NQS daemon.\n");
                exiting();              /* Delete communication file */
                exit (1);
        }
}

