/*
 * libnqs/queue/destqueue.c
 * 
 * DESCRIPTION:
 *
 *	Return a pointer to the queue-name portion of a remote
 *	queue specification.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 15, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

/*** destqueue
 *
 *
 *	char *destqueue():
 *
 *	Return a pointer to the queue-name portion of a remote
 *	queue specification.
 */
char *destqueue (char *remote_queue)
{
	static char quename [MAX_QUEUENAME+2];
					/* Remote queue name of one */
					/* extra byte so that mgr_scan.c */
					/* can tell a remote queue */
					/* name that is too long. */
	register char *copyto;		/* Copyto pointer */
	register unsigned size;		/* Copy size */

  	assert(remote_queue != NULL);
  
	size = 0;			/* Nothing copied yet */
	copyto = quename;		/* Where to copy */
	while (*remote_queue && *remote_queue != '@' &&
	       size < MAX_QUEUENAME+1) {
		*copyto++ = *remote_queue++;
		size++;			/* One more character copied */
	}				/* specification */
	*copyto = '\0';			/* Null terminate */
	return (quename);		/* Return pointer to queue name */
}
