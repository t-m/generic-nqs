/*
 * libnqs/queue/shoqbyname.c
 * 
 * DESCRIPTION:
 *
 *	Print on stdout information about a queue.
 *
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	December 18, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/netpacket.h>			/* NQS networking */
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <unistd.h>
#include <string.h>

/*** shoqbyname
 *
 *
 *	int shoqbyname():
 *
 *	Print on stdout information about the named queue.
 *	Returns: 0 if output was produced.
 *		 1 if no output was produced.
 */
int shoqbyname (
	struct confd *queuefile,	/* NQS queue file */
	char *fullname,			/* Name with possible '@' */
	long flags,			/* SHO_??? */
	uid_t whomuid,			/* Whom we are interested in */
	struct reqset *reqs,		/* Looking for these requests only */
	short dae_present,		/* NQS local daemon is present */
	struct confd *qmapfile,		/* NQS queue/device/destination */
					/* mappings file */
	struct confd *pipeqfile,	/* NQS pipe queue destinations file */
	struct confd *qcomplexfile)     /* NQS queue complex definition file */
{
	uid_t myuid;			/* Local user-id */
	char myname [MAX_ACCOUNTNAME+1];/* Local account name */
	struct gendescr *descr;		/* Pointer to general descriptor */
	int found;			/* Boolean */
	Mid_t itsmid;			/* The machine id of the queue */
	Mid_t mymid;			/* The local machine id */
	char *localname;		/* The queue name without "@host" */
	struct passwd *passwd;		/* Password file entry */
	int sd;				/* Socket descriptor */
	int extrach;			/* Number of chs already read */
	short output;			/* Boolean */
	int timeout;			/* Timeout in between tries */
	long transactcc;		/* Transaction completion code */

  	assert (queuefile != NULL);
  	assert (fullname  != NULL);
  	assert (qmapfile  != NULL);
  
	switch (machspec (fullname, &itsmid)) {
		case 0:
			break;
		case -1:
		case -2:
			fprintf (stderr, "Invalid hostname specification ");
			fprintf (stderr, "for queue %s.\n", fullname);
			return (1);
		case -3:
		case -4:
			fprintf (stderr, "Unexpected error in machspec(), ");
			fprintf (stderr, "queue = %s.\n", fullname);
			return (1);
	}
	if (localmid (&mymid) != 0) {
		fprintf (stderr, "Unexpected error in localmid().\n");
		return (0);
	}
	localname = destqueue (fullname);
	if (itsmid == mymid) {
		if (fullname[0] == '@') {
		    shoallque (queuefile,  flags, whomuid, reqs, dae_present, 
			    qmapfile, pipeqfile, 0, qcomplexfile);
		    return (0);
		}
		found = 0;
		if (telldb (queuefile) != -1) {
			/*
			 *  We are not necessarily at the start of the
			 *  database file....
			 */
			seekdb (queuefile, 0L);	/* Seek to the beginning */
		}
		descr = nextdb (queuefile);
		while (descr != NULL && !found) {
			if (strcmp (descr->v.que.namev.name, localname) == 0) {
				found = 1;
			}
			else descr = nextdb (queuefile);
		}
		if (found) {
			return (shoqbydesc (queuefile, descr, flags, whomuid,
					    reqs, dae_present, qmapfile,
					    pipeqfile, qcomplexfile));
		}
		else {
			fprintf (stderr, "Queue: %s does not exist.\n",
				 fullname);
			return (1);
		}
	}
	else {
		myuid = getuid();
		if ((passwd = sal_fetchpwuid (myuid)) == (struct passwd *) 0) {
			fprintf (stderr, "Who are you?\n");
			return (1);
		}
		strncpy (myname, passwd->pw_name, MAX_ACCOUNTNAME+1);
					/* Save account name */
		if ((passwd = sal_fetchpwuid (whomuid)) == (struct passwd *) 0) {
			fprintf (stderr, "Who is user %d?\n", whomuid);
			return (1);
		}
		interclear ();
		/* flags &= ~SHO_R_ALLUID; */			/* jrr	*/
		interw32i (flags);	/* SHO_??? */
		interwstr (localname);	/* Queue name without @machine */
		/*
		 *  This implementation of shoqbyname() uses "whomuid" to
		 *  determine, for local queues, whose requests should be
		 *  highlighted.  We presume that the remote implementation
		 *  does the same.  However, we do not assume that nmap is
		 *  used to come up with the remote whomuid.  Pass both the
		 *  client's whomuid and the client's "whom-username".
		 *  This lets the netserver decide how to come up with
		 *  whomuid on the server machine.
		 */
		interw32i (whomuid);
		interwstr (passwd->pw_name);
		sd = establish (NPK_QSTAT, itsmid, myuid, myname,
				&transactcc);
		/*
		 *  Establish has just told the network server
		 *  on the remote machine what it is that we want.
		 */
		if (sd < 0) {
			if (sd == -2) {
				/*
				 * Retry is in order.
				 */
				timeout = 1;
				do {
					nqssleep (timeout);
					interclear ();
					interw32i (flags);
					interwstr (localname);
					interw32i (whomuid);
					interwstr (passwd->pw_name);
					sd = establish (NPK_QSTAT, itsmid,
							myuid, myname,
							&transactcc);
					timeout *= 2;
				} while (sd == -2 && timeout <= 16);
				/*
				 *  Beyond this point, give up on retry.
				 */
				if (sd < 0) {
					analyzetcm (transactcc, SAL_DEBUG_MSG_WARNING, "");
					return(1);
				}
			}
			else {
				/*
				 *  Retry would surely fail.
				 */
				analyzetcm (transactcc, SAL_DEBUG_MSG_WARNING, "");
				return(1);
			}
		}
		/*
		 *  The server liked us.
		 */
		output = 1;
		/*
		 *  First, display any characters that
		 *  were already read inside establish().
		 */
		if ((extrach = extrasockch ())) {
			output = 0;
			fflush (stdout);
			write (STDOUT, bufsockch (), extrach);
		}
		/*
		 *  Then display any characters that the server
		 *  may still want to send us.
		 */
		fflush (stdout);
		if (filecopyentire (sd, STDOUT)) return (0);
		else return (output);
	}
}
