/*
 * libnqs/queue/showcomplex.c
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <ctype.h>
#include <libnqs/nqsxdirs.h>		/*  NQS directories */
#include <libnqs/nqsxvars.h>		/* NQS directories */
#include <libsal/license.h>
#include <libsal/proto.h>

static void print_complex ( struct gendescr *descr, long flags );
static void show_qcmplx_hdr ( void );

/*** showcomplex
 * 
 */
int showcomplex( 
	register struct confd *qfile,   /* MUST be the NQS queue file */
	char * name,			/* complex name  */
	register long flags,            /* Display flags */
	uid_t whomuid,			/* Uid of whom we are interested in */
	struct reqset *reqset_ptr,     	/* pointer to the request set */
	short daepresent,
	struct confd *qmapfile,		/*NQS qmap definition file */
	struct confd *pipeqfile,	/*NQS qmap definition file */
	struct confd *qcomplexfile)	/*NQS queue complex definition file */
{

	register struct gendescr *descr;
	int shown_header = 0;		/* True if have shown header */

	/* read first record */
	descr = nextdb(qcomplexfile);
	if (descr == NULL) {
	    printf("\t\t No complexes on host. \n");
	    if (name != NULL) return (1);
	    return (0);
	}
	while (descr != NULL) {
	    if (name != NULL)   {
		if (strcmp(descr->v.qcom.name, name) == 0) {
		    if (!shown_header) {
			show_qcmplx_hdr();
			shown_header++;
		    }
		    print_complex(descr, flags);
		    return(0);
		}
	   } else { 
	 	/* name == NULL, so print all we can find.  */
		if (!shown_header) {
		    show_qcmplx_hdr();
		    shown_header++;
		}
		print_complex(descr, flags);
	   }
	   descr = nextdb(qcomplexfile);
	}
	/*
	 * If we were looking for a specific complex and did not find it, 
	 * return 1.  O/W return 0.
	 */
	if (!shown_header && name != NULL) return (1);
	return (0);
}
static void print_complex(
	struct gendescr *descr,
	long flags)
{
	int	i;

	printf("%s \n",descr->v.qcom.name);
	if (flags & SHO_R_BRIEF) return;
        printf("  Complex run limit: %d;  ", descr->v.qcom.runlimit);
        printf("  Complex user limit: %d\n", descr->v.qcom.userlimit);
	if ( ! (flags & SHO_R_LONG) ) return;
        printf("  Queues for complex:\n");

        for(i=0; i< MAX_QSPERCOMPLX; i++) {
	    if (descr->v.qcom.queues[i][0] != (char)NULL) 
				printf("    %s\n", descr->v.qcom.queues[i]);
        }
}
static void show_qcmplx_hdr()
{
        char hostname[256];

        sal_gethostname(hostname,255);
        hostname[255] = '\0';

        printf("Queue Complexes:\n");
}

