/*
 * libnqs/queue/list_nqs_queues.c
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libnqs/nqsxdirs.h>	       	/* NQS files and directories */
#include <unistd.h>

/*
 *	Global variables:
 */
int DEBUG = 0;
/*
 *  int list_nqs_queues ( char * queptr, int count)
 *
 *  This routine will return the names of all the NQS queues on the 
 *  system in a character buffer.
 *
 */
int
list_nqs_queues(char *queptr,  int count)
{
	struct confd *queuefile;	/* Queue description file */
	register struct gendescr *descr;
	char	*cptr;			/* Pointer to buffer entries */
	int	i;			/* Counter of entries put into buffer */

	if (chdir (Nqs_root) == -1) {
		return(-1);
	}
#ifndef		NULL
#define		NULL	0
#endif
	if ((queuefile = opendb (Nqs_queues, O_RDONLY)) == NULL) {
		return(-2);
	}
	cptr = queptr;
	i = 0;
	descr = nextdb(queuefile);
	while (descr != (struct gendescr *)0) {
	    if ( (i+1) > count) break;	/* If will overrun, leave the loop */
	    i++;			/* Another entry in the table */
	    strcpy(cptr,  descr->v.que.namev.name);
	    cptr += MAX_QUEUENAME+1;	/* Advance to the next entry */
            descr = nextdb (queuefile);  /* Get the next queue */
        }
	return(i);
}
