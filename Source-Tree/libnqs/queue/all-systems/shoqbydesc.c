/*
 * libnqs/queue/shoqbydesc.c
 * 
 * NOTE:
 * 	The routines in this file are to be split up into something a
 * 	little more sensible, and merged into libsal at some date.
 * 
 * DESCRIPTION:
 *
 *	This module contains the function shoqbydesc().
 *	Shoqbydesc() is responsible for displaying information
 *	about the local queue whose descriptor it is called with.
 *
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxdirs.h>
#include <libsal/license.h>
#include <libsal/proto.h>

#include <string.h>
#include <errno.h>
#include <unistd.h>

#define DESTSET_MARGIN  13      /* Used in shoquedesmap() */
#define DEVSET_MARGIN   12      /* Used in shoquedevmap() */
#define OUTPUT_WIDTH    78      /* Number of output columns - 1 */

#if	IS_SGI | IS_IBMRS | IS_SOLARIS | IS_HPUX | IS_DECOSF
#include <stdlib.h>
#include <time.h>
#include <sys/param.h>
#ifndef IS_HPUX
#include <sys/proc.h>
#endif
#include <fcntl.h>
#include <unistd.h>
int	memfd;
int	procsize;
int	do_count;
long	total_time;	/* total cpu time */
time_t	start_time;
#define	    KMEM	"/dev/kmem"

#if	IS_SGI	
#include <sys/syssgi.h>
#include <sys/sysmp.h>
#include <sys/var.h>
#if   IS_IRIX5 | IS_IRIX6
#include <sys/fault.h>
#include <sys/syscall.h>
#include <sys/procfs.h>
#endif
#endif
#if	IS_IBMRS
#define	    MAX_PROC	1000
#include <nlist.h>
#include <procinfo.h>	
struct nlist anlist;
#endif
#if IS_SOLARIS | IS_DECOSF
#if REV_26
#define USE_NEW_PROC
#define _STRUCTURED_PROC 1
#define prpsinfo psinfo
#endif /* REV_26 */
#include <sys/procfs.h>
#include <dirent.h>
#endif

#if	IS_HPUX
#define	    MAX_PROC	1000
#include <nlist.h>
#include <sys/pstat.h>
struct nlist anlist;
#define X_AVENRUN       0
#define X_CCPU          1
#define X_NPROC         2
#define X_PROC          3
#define X_TOTAL         4
#define X_CP_TIME       5
#define X_MPID          6

#ifndef __hp9000s300
#define X_HZ            7
#endif


static struct nlist nlst[] = {
    { "_avenrun" },             /* 0 */
    { "_ccpu" },                /* 1 */
    { "_nproc" },               /* 2 */
    { "_proc" },                /* 3 */
    { "_total" },               /* 4 */
    { "_cp_time" },             /* 5 */
    { "_mpid" },                /* 6 */
#ifdef X_HZ
    { "_hz" },                  /* 7 */
#endif
    { 0 }
};
unsigned long proc;
int  nproc;
long hz;
static int bytes;
static struct proc *pbase;
static union pstun *pst;

#endif
#endif

#if IS_UNICOS
#include <sys/param.h>
#include <sys/types.h>
#include <sys/sema.h>
#include <sys/signal.h>
#include <sys/fs/prfcntl.h>
#include <sys/session.h>
#include <sys/user.h>
#include <sys/fcntl.h>
time_t        total_time;
time_t        start_time;
pid_t   jobid;
#endif


#define	DESTSET_MARGIN	13	/* Used in shoquedesmap() */
#define	DEVSET_MARGIN	12	/* Used in shoquedevmap() */
#define	OUTPUT_WIDTH	78	/* Number of output columns - 1 */

#if	IS_SGI
static int get_node ( struct proc *pointer, pid_t pid_oi, pid_t proc_family );
#else
#if	!IS_LINUX
static int get_node ( int pointer, pid_t pid_oi, pid_t proc_family );
#endif
#endif
static int selrequest ( struct qentry *qentry, long flags, uid_t whomuid, struct reqset *requests );
static void shogap ( int gaplow, int gaphigh, char * state, int flags );
static void shogrp ( unsigned long gid );
static void shokey ( struct gendescr *que_descr );
static void shoomd ( char mode );
static void shoqueacc ( struct confd *file, struct gendescr *que_descr );
static void shoquedesmap ( struct gendescr *que_descr, struct confd *qmapfile, struct confd *pipeqfile );
static void shoquedevmap ( struct gendescr *que_descr, struct confd *qmapfile );
static void shoquehdr ( struct confd *file, struct gendescr *que_descr, short dae_present, long flags, char *hostname, struct confd *qmapfile, struct confd *pipeqfile, struct confd *qcomplexfile );
static void shoquelims ( struct gendescr * que_descr );
static void shoquetime ( struct gendescr *que_descr );
static void shoreqlims ( struct gendescr *que_descr, struct rawreq *rawreqp );
static void shorequest ( struct qentry *qentry, int reqstate, struct gendescr *que_descr, int nth_request, long flags, char *hostname );
static void shousr ( unsigned long uid );
static void sho_mon_hdr ( void );
#if !IS_LINUX
static void summary ( pid_t proc_family, struct rawreq *rawreqp );
#endif

struct qcount {
        int arrive;
        int depart;
        int hold;
        int queued;
        int run;
        int stage;
        int wait;
};

extern int monsanto_header;

static int acccolumn = 0;	/* Used by shoqueacc(), shogrp(), shousr() */

/*** shoqbydesc
 *
 *
 *	int shoqbydesc():
 *
 *	Display information about a local NQS queue.  The caller
 *	of this function must be running with the current directory
 *	of:  Nqs_root.
 *
 *	Returns:
 *		0: If information about the queue and zero or more
 *		   requests residing in the queue was displayed.
 *	       -1: If no output was produced.
 *	       -2: If we were unable to open the queue request-ordering
 *		   file for the queue; no output was produced.
 *	       -3: If the queue was deleted while we were trying
 *		   to open the queue-ordering file.
 */
int shoqbydesc (
	struct confd *file,		/* NQS queue file containing.... */
	struct gendescr *que_descr,	/* the queue descriptor */
	long flags,			/* Display flags */
	uid_t whomuid,			/* Zero in on this user */
	struct reqset *requests,	/* Optional request selection set */
	short dae_present,		/* Boolean non-zero if the local NQS */
					/* daemon is present and running */
	struct confd *qmapfile,		/* NQS queue/device/destination */
					/* mapping database file */
	struct confd *pipeqfile,	/* NQS pipe-queue destination */
					/* database file */
	struct confd *qcomplexfile)     /* NQS queue complex definition file */
{

	char hostname [256];		/* Name of local host */
	struct qentry cache [QOFILE_CACHESIZ];
					/* Queue ordering file cache buffer */
	int cacheindex;			/* Current buffer cache index */
	int cachesize;			/* Number of queue entries in read */
					/* cache buffer */
	short header;			/* ~0 if queue header displayed */
	int nth_request;		/* Rank of this request */
	int gaplow;			/* Lowest rank among unshown reqs */
	int gaphigh;			/* Highest rank among unshown reqs */
	int fd;				/* Queue ordering file descriptor */
	int i;				/* Loop var */

	sal_gethostname (hostname, 255);	/* Get name of local host and make */
	hostname [255] = '\0';		/* sure that it is null-terminated */
	header = 0;			/* Queue header has not been shown */
	nth_request = 0;		/* No requests yet displayed */
	gaplow = 1;			/* Set up boundary conditions */
	gaphigh = 0;			/* Set up boundary conditions */
	if (flags & (SHO_RS_EXIT | SHO_RS_RUN | SHO_RS_STAGE | SHO_RS_QUEUED |
			SHO_RS_WAIT | SHO_RS_HOLD | SHO_RS_ARRIVE)) {
		/*
		 *  Open the request queue ordering file.
		 */
		if (!dae_present) {
			/*
			 *  Override database if daemon not present.
			 */
			que_descr->v.que.queuedcount
				+= que_descr->v.que.runcount;
			que_descr->v.que.runcount = 0;
		}
		fd = openqord (file, que_descr);
		/*
		 *  fd >= 0 if the queue has requests, and we successfully
		 *	    opened the queue ordering file.
		 *  fd = -1 if the queue has no requests.
		 *  fd = -2 if the queue has requests, but an error prevented
		 *	    us from opening the queue ordering file for
		 *	    the queue.
		 *  fd = -3 if the queue was deleted.
		 */
		if (fd < -1) return (fd);	/* Error or queue was deleted */
	}
	else {
		/*
		 *  We are only interested in the queue header.
		 */
		if (!dae_present) {
			/*
			 *  Override database if daemon not present.
			 */
			que_descr->v.que.queuedcount
				+= que_descr->v.que.runcount;
			que_descr->v.que.runcount = 0;
		}
		shoquehdr (file, que_descr, dae_present, flags, hostname,
			   qmapfile, pipeqfile, qcomplexfile);
						/* Display queue header only */
		return (0);			/* Output was produced */
	}
        if ( (!monsanto_header ) && (flags & SHO_R_MONSANTO) &&
                                (flags & SHO_R_HEADER) ) {
                monsanto_header++;
                sho_mon_hdr();
        }
	/*
	 *  The queue ordering file does exist.
	 *  We use it to try to find requests that match our requirements.
	 */
	if (requests == (struct reqset *) 0) {
		/*
		 *  Definitely display queue header no matter what.
		 */
		header = 1;			/* Mark header as displayed */
		shoquehdr (file, que_descr, dae_present, flags, hostname,
			   qmapfile, pipeqfile, qcomplexfile);
	}
	if (fd == -1) {
		/*
		 *  No requests are in the queue.
		 */
		if (header) return (0);		/* Output was produced */
		return (-1);			/* No output produced */
	}
	cachesize = 0;				/* Mark read cache as invalid*/
	cacheindex = 0;
	if (requests == (struct reqset *) 0) {
		if (flags & SHO_R_STANDARD) shokey (que_descr);
	}
	if ((flags & SHO_RS_EXIT) && que_descr->v.que.type == QUE_BATCH) {
		/*
		 *  Show completed requests staging output files.
		 */
		for (i = 0; i < que_descr->v.que.departcount; i++) {
			nth_request++;
			if (cacheindex >= cachesize) {
				cachesize = read (fd, (char *) cache,
						  sizeof (cache));
				cachesize /= sizeof (struct qentry);
				cacheindex = 0;
			}
			if (selrequest (&cache [cacheindex], flags,
					whomuid, requests)) {
				if (!header) {
					/*
					 *  Display the queue header.
					 *  Mark header as displayed.
					 */
					header = 1;
					shoquehdr (file, que_descr, dae_present,
						   flags, hostname, qmapfile,
						   pipeqfile, qcomplexfile);
					if (flags & SHO_R_STANDARD) {
						shokey (que_descr);
					}
				}
				shogap (gaplow, gaphigh, "EXITING", flags);
				shorequest (&cache [cacheindex],
					    SHO_RS_EXIT, que_descr,
					    nth_request, flags, hostname);
				gaphigh = nth_request;
				gaplow = nth_request + 1;
			}
			else gaphigh++;
			cacheindex++;
		}
		shogap (gaplow, gaphigh, "EXITING", flags);
		gaphigh = nth_request;
		gaplow = nth_request + 1;
	}
	else if ((flags & SHO_RS_EXIT) && que_descr->v.que.type==QUE_PIPE) {
		/*
		 *  Show departing requests.
		 */
		for (i = 0; i < que_descr->v.que.departcount; i++) {
			nth_request++;
			if (cacheindex >= cachesize) {
				cachesize = read (fd, (char *) cache,
						  sizeof (cache));
				cachesize /= sizeof (struct qentry);
				cacheindex = 0;
			}
			if (selrequest (&cache [cacheindex], flags,
					whomuid, requests)) {
				if (!header) {
					/*
					 *  Display the queue header.
					 *  Mark header as displayed.
					 */
					header = 1;
					shoquehdr (file, que_descr, dae_present,
						   flags, hostname, qmapfile,
						   pipeqfile,qcomplexfile);
					if (flags & SHO_R_STANDARD) {
						shokey (que_descr);
					}
				}
				shogap (gaplow, gaphigh, "DEPARTING", flags);
				shorequest (&cache [cacheindex],
					    SHO_RS_EXIT, que_descr,
					    nth_request, flags, hostname);
				gaphigh = nth_request;
				gaplow = nth_request + 1;
			}
			else gaphigh++;
			cacheindex++;
		}
		shogap (gaplow, gaphigh, "DEPARTING", flags);
		gaphigh = nth_request;
		gaplow = nth_request + 1;
	}
	else lseek (fd, (long) (que_descr->v.que.departcount *
			sizeof (struct qentry)), 0);
	if ((flags & SHO_RS_RUN) && que_descr->v.que.type != QUE_PIPE) {
		/*
		 * Show running requests.
		 */
		for (i = 0; i < que_descr->v.que.runcount; i++) {
			nth_request++;
			if (cacheindex >= cachesize) {
				cachesize = read (fd, (char *) cache,
						  sizeof (cache));
				cachesize /= sizeof (struct qentry);
				cacheindex = 0;
			}
			if (selrequest (&cache [cacheindex], flags,
					whomuid, requests)) {
				if (!header) {
					/*
					 *  Display the queue header.
					 *  Mark header as displayed.
					 */
					header = 1;
					shoquehdr (file, que_descr, dae_present,
						   flags, hostname, qmapfile,
						   pipeqfile,qcomplexfile);
					if (flags & SHO_R_STANDARD) {
						shokey (que_descr);
					}
				}
				shogap (gaplow, gaphigh, "RUNNING", flags);
				shorequest (&cache [cacheindex], SHO_RS_RUN,
					    que_descr, nth_request, flags,
					    hostname);
				gaphigh = nth_request;
				gaplow = nth_request + 1;
			}
			else gaphigh++;
			cacheindex++;
		}
		shogap (gaplow, gaphigh, "RUNNING", flags);
		gaphigh = nth_request;
		gaplow = nth_request + 1;
	}
	else if ((flags & SHO_RS_RUN) && que_descr->v.que.type == QUE_PIPE) {
		for (i = 0; i < que_descr->v.que.runcount; i++) {
			nth_request++;
			if (cacheindex >= cachesize) {
				cachesize = read (fd, (char *) cache,
						  sizeof (cache));
				cachesize /= sizeof (struct qentry);
				cacheindex = 0;
			}
			if (selrequest (&cache [cacheindex], flags,
					whomuid, requests)) {
				if (!header) {
					/*
					 *  Display the queue header.
					 *  Mark header as displayed.
					 */
					header = 1;
					shoquehdr (file, que_descr, dae_present,
						   flags, hostname, qmapfile,
						   pipeqfile,qcomplexfile);
					if (flags & SHO_R_STANDARD) {
						shokey (que_descr);
					}
				}
				shogap (gaplow, gaphigh, "ROUTING", flags);
				shorequest (&cache [cacheindex], SHO_RS_RUN,
					    que_descr, nth_request, flags,
					    hostname);
				gaphigh = nth_request;
				gaplow = nth_request + 1;
			}
			else gaphigh++;
			cacheindex++;
		}
		shogap (gaplow, gaphigh, "ROUTING", flags);
		gaphigh = nth_request;
		gaplow = nth_request + 1;
	}
	else {
		cacheindex += que_descr->v.que.runcount;
		if (cacheindex > cachesize) {
			lseek (fd, (long) (cacheindex - cachesize)
					 * sizeof (struct qentry), 1);
		}
	}
	if ((flags & SHO_RS_STAGE) && que_descr->v.que.type == QUE_BATCH) {
		/*
		 * Show staging requests.
		 */
		for (i = 0; i < que_descr->v.que.stagecount; i++) {
			nth_request++;
			if (cacheindex >= cachesize) {
				cachesize = read (fd, (char *) cache,
						  sizeof (cache));
				cachesize /= sizeof (struct qentry);
				cacheindex = 0;
			}
			if (selrequest (&cache [cacheindex], flags,
					whomuid, requests)) {
				if (!header) {
					/*
					 *  Display the queue header.
					 *  Mark header as displayed.
					 */
					header = 1;
					shoquehdr (file, que_descr, dae_present,
						   flags, hostname, qmapfile,
						   pipeqfile,qcomplexfile);
					if (flags & SHO_R_STANDARD) {
						shokey (que_descr);
					}
				}
				shogap (gaplow, gaphigh, "STAGING", flags);
				shorequest (&cache [cacheindex],
					    SHO_RS_STAGE, que_descr,
					    nth_request, flags, hostname);
				gaphigh = nth_request;
				gaplow = nth_request + 1;
			}
			else gaphigh++;
			cacheindex++;
		}
		shogap (gaplow, gaphigh, "STAGING", flags);
		gaphigh = nth_request;
		gaplow = nth_request + 1;
	}
	else {
		cacheindex += que_descr->v.que.stagecount;
		if (cacheindex > cachesize) {
			lseek (fd, (long) (cacheindex - cachesize)
					 * sizeof (struct qentry), 1);
		}
	}
	if (flags & SHO_RS_QUEUED) {	/* Show queued requests */
		for (i = 0; i < que_descr->v.que.queuedcount; i++) {
			nth_request++;
			if (cacheindex >= cachesize) {
				cachesize = read (fd, (char *) cache,
						  sizeof (cache));
				cachesize /= sizeof (struct qentry);
				cacheindex = 0;
			}
			if (selrequest (&cache [cacheindex], flags,
					whomuid, requests)) {
				if (!header) {
					/*
					 *  Display the queue header.
					 *  Mark header as displayed.
					 */
					header = 1;
					shoquehdr (file, que_descr, dae_present,
						   flags, hostname, qmapfile,
						   pipeqfile, qcomplexfile);
					if (flags & SHO_R_STANDARD) {
						shokey (que_descr);
					}
				}
				shogap (gaplow, gaphigh, "QUEUED", flags);
				shorequest (&cache [cacheindex],
					    SHO_RS_QUEUED, que_descr,
					    nth_request, flags, hostname);
				gaphigh = nth_request;
				gaplow = nth_request + 1;
			}
			else gaphigh++;
			cacheindex++;
		}
		shogap (gaplow, gaphigh, "QUEUED", flags);
		gaphigh = nth_request;
		gaplow = nth_request + 1;
	}
	else {
		cacheindex += que_descr->v.que.queuedcount;
		if (cacheindex > cachesize) {
			lseek (fd, (long) (cacheindex - cachesize)
					 * sizeof (struct qentry), 1);
		}
	}
	if (flags & SHO_RS_WAIT) {	/* Show waiting requests */
		for (i = 0; i < que_descr->v.que.waitcount; i++) {
			nth_request++;
			if (cacheindex >= cachesize) {
				cachesize = read (fd, (char *) cache,
						  sizeof (cache));
				cachesize /= sizeof (struct qentry);
				cacheindex = 0;
			}
			if (selrequest (&cache [cacheindex], flags,
					whomuid, requests)) {
				if (!header) {
					/*
					 *  Display the queue header.
					 *  Mark header as displayed.
					 */
					header = 1;
					shoquehdr (file, que_descr, dae_present,
						   flags, hostname, qmapfile,
						   pipeqfile, qcomplexfile);
					if (flags & SHO_R_STANDARD) {
						shokey (que_descr);
					}
				}
				shogap (gaplow, gaphigh, "WAITING", flags);
				shorequest (&cache [cacheindex],
					    SHO_RS_WAIT, que_descr,
					    nth_request, flags, hostname);
				gaphigh = nth_request;
				gaplow = nth_request + 1;
			}
			else gaphigh++;
			cacheindex++;
		}
		shogap (gaplow, gaphigh, "WAITING", flags);
		gaphigh = nth_request;
		gaplow = nth_request + 1;
	}
	else {
		cacheindex += que_descr->v.que.waitcount;
		if (cacheindex > cachesize) {
			lseek (fd, (long) (cacheindex - cachesize)
					 * sizeof (struct qentry), 1);
		}
	}
	if (flags & SHO_RS_HOLD) {	/* Show requests holding */
		for (i = 0; i < que_descr->v.que.holdcount; i++) {
			nth_request++;
			if (cacheindex >= cachesize) {
				cachesize = read (fd, (char *) cache,
						  sizeof (cache));
				cachesize /= sizeof (struct qentry);
				cacheindex = 0;
			}
			if (selrequest (&cache [cacheindex], flags,
					whomuid, requests)) {
				if (!header) {
					/*
					 *  Display the queue header.
					 *  Mark header as displayed.
					 */
					header = 1;
					shoquehdr (file, que_descr, dae_present,
						   flags, hostname, qmapfile,
						   pipeqfile,qcomplexfile);
					if (flags & SHO_R_STANDARD) {
						shokey (que_descr);
					}
				}
				shogap (gaplow, gaphigh, "HOLDING", flags);
				shorequest (&cache [cacheindex],
					    SHO_RS_HOLD, que_descr,
					    nth_request, flags, hostname);
				gaphigh = nth_request;
				gaplow = nth_request + 1;
			}
			else gaphigh++;
			cacheindex++;
		}
		shogap (gaplow, gaphigh, "HOLDING", flags);
		gaphigh = nth_request;
		gaplow = nth_request + 1;
	}
	else {
		cacheindex += que_descr->v.que.holdcount;
		if (cacheindex > cachesize) {
			lseek (fd, (long) (cacheindex - cachesize)
					 * sizeof (struct qentry), 1);
		}
	}
	if (flags & SHO_RS_ARRIVE) {	/* Show arriving requests */
		for (i = 0; i < que_descr->v.que.arrivecount; i++) {
			nth_request++;
			if (cacheindex >= cachesize) {
				cachesize = read (fd, (char *) cache,
						  sizeof (cache));
				cachesize /= sizeof (struct qentry);
				cacheindex = 0;
			}
			if (selrequest (&cache [cacheindex], flags,
					whomuid, requests)) {
				if (!header) {
					/*
					 *  Display the queue header.
					 *  Mark header as displayed.
					 */
					header = 1;
					shoquehdr (file, que_descr, dae_present,
						   flags, hostname, qmapfile,
						   pipeqfile,qcomplexfile);
					if (flags & SHO_R_STANDARD) {
						shokey (que_descr);
					}
				}
				shogap (gaplow, gaphigh, "ARRIVING", flags);
				shorequest (&cache [cacheindex],
					    SHO_RS_ARRIVE, que_descr,
					    nth_request, flags, hostname);
				gaphigh = nth_request;
				gaplow = nth_request + 1;
			}
			else gaphigh++;
			cacheindex++;
		}
		shogap (gaplow, gaphigh, "ARRIVING", flags);
		gaphigh = nth_request;
		gaplow = nth_request + 1;
	}
	close (fd);			/* Close queue ordering file */
	if (header) return (0);		/* There was output */
	return (-1);			/* No output at all */
}

/*** selrequest
 *
 *
 *	int selrequest():
 *	Select a request.
 *
 *	Returns:
 *		1: if the request was selected for display;
 *		0: if not.
 */
static int selrequest (
	register struct qentry *qentry,	/* Request queue entry */
	long flags,			/* Display mode flags */
	uid_t whomuid,			/* Whom we are asking about */
	register struct reqset *requests)/* Specific request select list */
{
	if (requests == (struct reqset *) 0 ) {	
		if (flags & SHO_R_ALLUID) return (1);
		else {
			if (qentry->uid == whomuid) {
				return (1);	/* Request will be shown */
			}
			return (0);		/* Request will not be shown */
		}
	}
	/*
	 *  Scan specific request set.
	 */
	do {
		if ((flags & SHO_R_ALLUID) || qentry->uid == whomuid) {
			/*
			 *  Might be one of the requests we are looking for.
			 */
			if (requests->selecttype == SEL_REQNAME) {
				/*
				 *  Selection is by request name.
				 */
				if (strncmp (requests->v.reqname,
					     qentry->reqname_v,
					     MAX_REQNAME) == 0) {
					return (1);	/* A match */
				}
			}
			else {
				/*
				 *  Selection is by request-id.
				 */
                                /*
                                 * If the seqno is 0, we are looking for
                                 * requests that originated on the machine.
                                 */
                                if (requests->v.reqid.orig_mid
                                        == qentry->orig_mid &&
                                    requests->v.reqid.orig_seqno
                                        == 0) {
                                        return (1);     /* A match */
                                }
                                if (requests->v.reqid.orig_mid
                                        == qentry->orig_mid &&
                                    requests->v.reqid.orig_seqno
                                        == qentry->orig_seqno) {
                                        return (1);     /* A match */
				}
			}
		}
		requests = requests->next;	/* Get next request */
						/* in selection set */
	} while (requests != (struct reqset *) 0);	/* until end of set */
	return (0);			/* It wasn't one of the requests */
					/* we are looking for. */
}

/*** shogap
 *
 *
 *	void shogap():
 *	Display a gap in between requests we are interested in.
 */
static void shogap (
	int gaplow,
	int gaphigh,
	char * state,
	int flags)
{
        if (flags & SHO_R_MONSANTO) return;
	if (gaplow > gaphigh) return;
	if (gaplow == gaphigh) printf ("<1 request %s>\n", state);
	else printf ("<%d requests %s>\n", gaphigh - gaplow + 1, state);
}


/*** shogrp
 *
 *
 *	void shogrp():
 *	Display a group name.
 */
static void shogrp (unsigned long gid)
{
	if (acccolumn >= 4) {
		acccolumn = 0;
		printf ("\n    ");
	}
	else printf ("%s\t", getgrpnam ((int) gid));
	acccolumn++;
}


/*** shokey
 *
 *
 *	void shokey():
 *	Display column headings.
 */
static void shokey (struct gendescr *que_descr)
{
        if (que_descr->v.que.type == QUE_DEVICE) {
                printf ("         REQUEST NAME        REQUEST ID            ");
                printf ("USER  PRI    STATE     SIZE\n");
                return;
        }
        printf ("         REQUEST NAME        REQUEST ID            ");
        printf ("USER  PRI    STATE     PGRP\n");
}


/*** shoomd
 *
 *
 *	void shoomd():
 *	Display an output mode.
 */
static void shoomd (char mode)
{
	if (mode & OMD_SPOOL) {
		printf ("SPOOL\n");
	}
	else if (mode & OMD_EO) {
		printf ("EO\n");
	}
	else printf ("NOSPOOL\n");
}


/*** shoqueacc
 *
 *
 *	void shoqueacc():
 *	Display the groups and users allowed to access a queue.
 */
static void shoqueacc (
	struct confd *file,
	struct gendescr *que_descr)
{
	int fd;				/* File descriptor */
	unsigned long buffer [QAFILE_CACHESIZ];
	int done;			/* Boolean */
	int cachebytes;			/* Try to read this much */
	int bytes;			/* Did read this much */
	int entries;			/* Did read this many */
	int i;				/* Loop variable */


	fd = openqacc (file, que_descr);
	/*
	 * Openqacc returns 0 if the queue is restricted and the file
	 * is open; -1 if the queue is unrestricted; -2 if the queue
	 * queue is restricted but the file is not open, and -3
	 * if the queue appears to have been deleted.
	 */
	if (fd < -1) {
		printf ("  Failed to open queue access file.\n");
		return;
	}
	if (fd == -1) {
		printf ("  Unrestricted access\n");
		return;
	}
	printf ("  Groups with access:  ");
	acccolumn = 2;
	done = 0;
	cachebytes = QAFILE_CACHESIZ * sizeof (unsigned long);
	while ((bytes = read (fd, (char *) buffer, cachebytes)) > 0) {
		entries = bytes / sizeof (unsigned long);
		for (i = 0; i < entries; i++) {
			/* Zero comes only at the end */
			if (buffer [i] == 0) {
				done = 1;
				break;
			}
			if ((buffer [i] & MAKEGID) != 0) /* A group */
				shogrp (buffer [i] & ~MAKEGID);
		}
		if (done) break;
	}
	putchar ('\n');
	lseek (fd, 0, 0);			/* Back to the beginning */
	printf ("  Users with access:  ");
	acccolumn = 2;
	done = 0;
	shousr ((unsigned long) 0);		/* Root always has access */
	while ((bytes = read (fd, (char *) buffer, cachebytes)) > 0) {
		entries = bytes / sizeof (unsigned long);
		for (i = 0; i < entries; i++) {
			/* Zero comes only at the end */
			if (buffer [i] == 0) {
				done = 1;
				break;
			}
			if ((buffer [i] & MAKEGID) == 0) /* Not a group */
				shousr (buffer [i]);
		}
		if (done) break;
	}
	putchar ('\n');
}


/*** shoquedesmap
 *
 *
 *	void shoquedesmap():
 *	Display the destination set for a pipe queue.
 */
static void shoquedesmap (
	struct gendescr *que_descr,	/* The queue descriptor */
	struct confd *qmapfile,		/* Queue/device/destination mapping */
					/* database file */
	struct confd *pipeqfile)	/* Pipe-destination database file */
{
	register struct gendescr *dbds1;/* NQS database file generic descr */
	register struct gendescr *dbds2;/* NQS database file generic descr */
	register short column;		/* Current output column */
	register short fldsiz;		/* Size of output field */
	short preamble;			/* Display flag */
	short first;			/* Display flag */
	char *rhostname;		/* Remote host destination name */
	
	if (telldb (qmapfile) != -1) {
		/*
		 *  We are not necessarily at the start of the
		 *  queue/device/destination descriptor database
		 *  file....
		 */
		seekdbb (qmapfile, 0L);	/* Seek to the beginning */
	}
	dbds1 = nextdb (qmapfile);
	preamble = 0;		/* "Destset = [" not displayed yet */
	first = 1;		/* If destination displayed, it will */
				/* be the first in the set */
	while (dbds1 != (struct gendescr *) 0) {
		if (!dbds1->v.map.qtodevmap &&
		    strcmp (dbds1->v.map.v.qdestmap.lqueue,
			    que_descr->v.que.namev.name) == 0) {
			/*
			 *  We have located a pipe-queue/destination mapping
			 *  for the pipe queue.
			 *
			 *  Now, locate the database description for the
			 *  pipe queue destination referenced in the mapping.
			 */
			if (telldb (pipeqfile) != -1) {
				/*
				 *  We are not necessarily at the start of the
				 *  pipe queue descriptor database file....
				 */
				seekdbb (pipeqfile, 0L);/* Seek to start */
			}
			dbds2 = nextdb (pipeqfile);
			while (dbds2 != (struct gendescr *) 0 &&
			      (strcmp (dbds1->v.map.v.qdestmap.rqueue,
				       dbds2->v.dest.rqueue) ||
			       dbds1->v.map.v.qdestmap.rhost_mid !=
			       dbds2->v.dest.rhost_mid)) {
				/*
				 *  We have not yet located the pipe queue
				 *  destination referred to in the mapping
				 *  descriptor.
				 */
				dbds2 = nextdb (pipeqfile);
			}
			if (dbds2 != (struct gendescr *) 0) {
			    /*
			     *  We have located the pipe-queue
			     *  descriptor for the mapping.
			     */
			    if (!preamble) {
				/*
				 *  Display destination set
				 *  header.
				 */
				preamble = 1;
				printf ("  Destset = {");
				column = DESTSET_MARGIN; /* From 0 */
			    }
			    rhostname = getmacnam (dbds2->v.dest.rhost_mid);
			    if (!(dbds2->v.dest.status & DEST_ENABLED) &&
				 (dbds2->v.dest.status & DEST_RETRY)) {
				/*
				 *  The destination is in the retry
				 *  state.  Show the time of the
				 *  next earliest retry, and the
				 *  time of the first failure.
				 */
				if (column != DESTSET_MARGIN) {
					printf (",\n%*s", DESTSET_MARGIN, "");
				}
				printf ("%s@%s [RETRY]\n",
					dbds2->v.dest.rqueue, rhostname);
				printf ("%*s<Unreachable since: %s>\n",
					DESTSET_MARGIN+4, "",
					fmttime (&dbds2->v.dest.retrytime));
				printf ("%*s<Next retry at: %s>\n%*s",
					DESTSET_MARGIN+4, "",
					fmttime (&dbds2->v.dest.retry_at),
					DESTSET_MARGIN, "");
				column = DESTSET_MARGIN;
			    }
			    else {
				/*
				 *  The destination is either enabled,
				 *  or has been marked as failed.
				 */
				fldsiz = strlen (dbds2->v.dest.rqueue)
				       + strlen (rhostname) + 1;
				if (dbds2->v.dest.status & DEST_FAILED) {
					/*
					 *  This destination has been
					 *  marked as failed.
					 */
					fldsiz += 9;	/* _[FAILED] */
				}
				if (!first) {
					putchar (',');
					fldsiz += 2;	/*+2 for ', '*/
				}
				if (column + fldsiz >= OUTPUT_WIDTH) {
					printf ("\n%*s", DESTSET_MARGIN, "");
					column = fldsiz + DESTSET_MARGIN;
					if (!first) column -= 2;
				}
				else {
					if (!first) putchar (' ');
					column += fldsiz;
				}
				printf ("%s@%s", dbds2->v.dest.rqueue,
					rhostname);
				if (dbds2->v.dest.status & DEST_FAILED) {
					printf (" [FAILED]");
				}
			    }
			    first = 0;		/* Not the first */
			}
		}
		dbds1 = nextdb (qmapfile);
	}
	if (preamble) printf ("};\n");
}


/*** shoquehdr
 *
 *
 *	void shoquehdr():
 *	Display the queue header.
 */
static void shoquehdr (
	struct confd *file,		/* NQS queue file containing.... */
	struct gendescr *que_descr,	/* The Queue-descriptor */
	short dae_present,		/* Non-zero if the local NQS daemon */
					/* is present and running */
	long flags,			/* Display mode flags */
	char *hostname,			/* Name of local host */
	struct confd *qmapfile,		/* Queue/device/destination mapping */
					/* database file */
	struct confd *pipeqfile,	/* Pipe-destination database file */
	struct confd *qcomplexfile)     /* Queue complex definition file */
{
        struct gendescr *qcom_descr;    /* A queue-complex-descriptor */
        register char *namep, *qname;
        register int i, found;


	if (flags & SHO_R_MONSANTO ) return;
	/* ---------------------------------------------------------------
	 * Here is where we print the queue name -- it can be long enough
	 * to wrap around ....
	 */

	printf ("%s@%s;  type=", que_descr->v.que.namev.name, hostname);
	switch (que_descr->v.que.type) {
	case QUE_BATCH:
		printf ("BATCH");
		break;
	case QUE_DEVICE:
		printf ("DEVICE");
		break;
	case QUE_PIPE:
		printf ("PIPE");
		break;
	case QUE_NET:
		printf ("NETWORK");
		break;
	default:
		printf ("UNKNOWN");
		break;
	}
	printf (";  [");
	if (que_descr->v.que.status & QUE_ENABLED) {
		if (dae_present) printf ("ENABLED, ");
		else printf ("CLOSED, ");
	}
	else printf ("DISABLED, ");
	if (que_descr->v.que.status & QUE_RUNNING) {
		if (!dae_present) printf ("SHUTDOWN");
		else if (que_descr->v.que.runcount) printf ("RUNNING");
		else printf ("INACTIVE");
	}
	else if (que_descr->v.que.runcount && dae_present) printf ("STOPPING");
	else printf ("STOPPED");
	fputs ("];", stdout);
	if (que_descr->v.que.status & QUE_PIPEONLY) {
		fputs ("  PIPEONLY;", stdout);
	}
	printf ("  pri=%1d", que_descr->v.que.priority);
        printf ("  lim=");
        switch (que_descr->v.que.type) {
        case QUE_BATCH:
                printf ("%1d\n", que_descr->v.que.v1.batch.runlimit);
                printf (" %2d exit;  ", que_descr->v.que.departcount);
                printf ("%2d run;  ", que_descr->v.que.runcount);
                printf ("%2d stage;  ", que_descr->v.que.stagecount);
                break;
        case QUE_DEVICE:
                printf ("1\n");
                break;
        case QUE_PIPE:
                printf ("%1d\n", que_descr->v.que.v1.pipe.runlimit);
                printf (" %2d depart;  ", que_descr->v.que.departcount);
                printf ("%2d route;  ", que_descr->v.que.runcount);
                break;
        case QUE_NET:
                printf ("%1d\n", que_descr->v.que.v1.network.runlimit);
                printf (" %2d run;  ", que_descr->v.que.runcount);
                break;
        }
	printf ("%2d queued;  ", que_descr->v.que.queuedcount);
	printf ("%2d wait;  ", que_descr->v.que.waitcount);
	printf ("%2d hold;  ", que_descr->v.que.holdcount);
	printf ("%2d arrive;\n", que_descr->v.que.arrivecount);
        if (flags & SHO_H_DEST) {
	    if (que_descr->v.que.type == QUE_BATCH) 
	        printf("  User run limit= %1d\n", 
			que_descr->v.que.v1.batch.userlimit);
	}
	if (flags & SHO_H_DEST) {
            if (que_descr->v.que.type == QUE_PIPE) {
	        if (que_descr->v.que.status & QUE_LDB_IN )
	             fputs ("  Load balanced inbound.\n", stdout);
	        else if (que_descr->v.que.status & QUE_LDB_OUT)
	             fputs ("  Load balanced outbound.\n", stdout);
	    }
	}
	if (flags & SHO_H_TIME) {
		shoquetime (que_descr);
	}
	if (flags & SHO_H_ACCESS && que_descr->v.que.type != QUE_NET) {
		shoqueacc (file, que_descr);
	}
	if (flags & SHO_H_SERV && que_descr->v.que.type == QUE_PIPE) {
		printf ("  Queue server: %s\n",
			que_descr->v.que.v1.pipe.server);
	}
	if (flags & SHO_H_MAP && que_descr->v.que.type == QUE_DEVICE) {
		shoquedevmap (que_descr, qmapfile);
	}
	else if (flags & SHO_H_DEST && que_descr->v.que.type == QUE_PIPE) {
		shoquedesmap (que_descr, qmapfile, pipeqfile);
	}
        if (que_descr->v.que.type == QUE_BATCH) {
                found = 0;
                if (telldb (qcomplexfile) != -1) {
                        seekdbb (qcomplexfile, 0L);     /* Seek to beginning */
                }
                qname = que_descr->v.que.namev.name;
                while ((qcom_descr = nextdb( qcomplexfile )) !=
                      (struct gendescr *)0) {
                        for (i = 0; i < MAX_QSPERCOMPLX; i++) {
                                namep = qcom_descr->v.qcom.queues[i];
                                if (*namep == '\0') continue;
                                if (strcmp(namep, qname) != 0) continue;
                                if (!found) {
                                        printf("  Queue Complex Membership:");
                                }
                                found = 1;
                                printf("  %s;", qcom_descr->v.qcom.name);
                        }
                }
                if (found) putchar('\n');
                if (flags & SHO_H_LIM) {
                        shoquelims (que_descr);
                }
        }
	putchar ('\n');				/* Leave one blank line */
}


/*** shoquelims
 *
 *
 *	void shoquelims():
 *	Display the limits associated with a queue.
 */
static void shoquelims (struct gendescr *que_descr)
{
  shoqlims(que_descr);
}


/*** shoquedevmap
 *
 *
 *	void shoquedevmap():
 *	Display the queue-to-device mappings for a queue.
 */
static void shoquedevmap (
	struct gendescr *que_descr,	/* The queue descriptor */
	struct confd *qmapfile)		/* Queue/device/destination mapping */
					/* database file */
{
	register struct gendescr *dbdsc;/* NQS database file generic descr */
	register short column;		/* Current output column */
	register short fldsiz;		/* Current field size */
	short preamble;			/* Display flag */
	short first;			/* Display flag */
	
	if (telldb (qmapfile) != -1) {
		/*
		 *  We are not necessarily at the start of the database file.
		 */
		seekdbb (qmapfile, 0L);	/* Seek to the beginning */
	}
	dbdsc = nextdb (qmapfile);
	preamble = 0;		/* "Device set = [" not displayed */
	first = 1;		/* If device displayed, it will be */
				/* the first in the set */
	while (dbdsc != (struct gendescr *) 0) {
		if (dbdsc->v.map.qtodevmap) {
			if (strcmp (dbdsc->v.map.v.qdevmap.qname,
				    que_descr->v.que.namev.name) == 0) {
				if (!preamble) {
					/*
					 *  Display device set header.
					 */
					preamble = 1;
					printf ("  Devset = {");
					column = DEVSET_MARGIN;	/* From 0 */
				}
				fldsiz = strlen (dbdsc->v.map.v.qdevmap.dname);
				if (!first) {
					putchar (',');
					fldsiz += 2;	/* +2 for ', '*/
				}
				if (column + fldsiz >= OUTPUT_WIDTH) {
					printf ("\n%*s", DEVSET_MARGIN, "");
					column = fldsiz + DEVSET_MARGIN;
					if (!first) column -= 2;
				}
				else {
					if (!first) putchar (' ');
					column += fldsiz;
				}
				fputs (dbdsc->v.map.v.qdevmap.dname, stdout);
				first = 0;	/* Not the first */
			}
		}
		dbdsc = nextdb (qmapfile);
	}
	if (preamble) printf ("};\n");
}


/*** shoquetime
 *
 *
 *	void shoquetime():
 *	Display the cumulative cpu time used by requests in a queue.
 */
static void shoquetime (struct gendescr *que_descr)
{
	printf ("  Cumulative system space time = ");
#if	IS_POSIX_1 | IS_SYSVr4
	printf ("%ld.%02d seconds\n", (que_descr->v.que.ru_stime / CLK_TCK),
		(int) (((que_descr->v.que.ru_stime % CLK_TCK) * 100) /
		CLK_TCK));
#else
#if	IS_BSD
	printf ("%ld.%06ld seconds\n", que_descr->v.que.ru_stime,
		que_descr->v.que.ru_stime_usec);
#else
BAD SYSTEM TYPE
#endif
#endif
	printf ("  Cumulative user space time = ");
#if	IS_POSIX_1 | IS_SYSVr4
	printf ("%ld.%02d seconds\n", (que_descr->v.que.ru_utime / CLK_TCK),
		(int) (((que_descr->v.que.ru_utime % CLK_TCK) * 100) /
		CLK_TCK));
#else
#if	IS_BSD
	printf ("%ld.%06ld seconds\n", que_descr->v.que.ru_utime,
		que_descr->v.que.ru_utime_usec);
#else
BAD SYSTEM TYPE
#endif
#endif
}

/*** shoreqlims
 *
 *
 *	void shoreqlims():
 *	Display the limits associated with a request.
 */
static void shoreqlims (
	struct gendescr *que_descr,	/* Needed to detect pipe queues */
	struct rawreq *rawreqp)		/* The request as a rawreq */
{
  shoreql(que_descr, rawreqp);
}

/*** shorequest
 *
 *
 *	void shorequest():
 *	Display a request.
 */
static void shorequest (
	struct qentry *qentry,		/* Queue entry describing request */
	int reqstate,			/* Req state=[HOLDING,RUNNING,QUEUED] */
	struct gendescr *que_descr,	/* Queue file queue-descriptor */
	int nth_request,		/* N-th request number in the queue */
	long flags,			/* Display mode flags */
	char *hostname)			/* Local host name */
{
	char reqname [MAX_REQNAME+1];	/* Name of request */
	char *status;			/* Request status */
	char *machinename;		/* Name of originating machine */
	char *username;			/* User name of request owner */
	struct rawreq rawreq;		/* Control file raw request */
	int cfd;			/* Control file file-descr */
	int chars;			/* String length */
/*	register int i;			   apparently unused	*/
	char *loadlname;
	char *dotptr;
	
	machinename = getmacnam (qentry->orig_mid);
	username = getusenam (qentry->uid);
	switch (reqstate) {
	case SHO_RS_EXIT:
		if (que_descr->v.que.type == QUE_BATCH) {
			status = "EXITING";
		}
		else if (que_descr->v.que.type == QUE_PIPE) {
			status = "DEPARTING";
		}
		else status = "UNKNOWN";
		break;
	case SHO_RS_RUN:
		if (que_descr->v.que.type == QUE_PIPE) {
			status = "ROUTING";
		} else { 
		    status = "RUNNING";
		    if ((cfd = getreq ((long)qentry->orig_seqno,
				qentry->orig_mid, &rawreq)) != -1) {
			if (rawreq.flags & RQF_SUSPENDED) status = "SUSPENDED";
		    }
		}
		break;
	case SHO_RS_STAGE:
		if (que_descr->v.que.type == QUE_BATCH) {
			status = "STAGING";
		}
		else status = "UNKNOWN";
		break;
	case SHO_RS_QUEUED:
		status = "QUEUED";
		break;
	case SHO_RS_WAIT:
		status = "WAITING";
		break;
	case SHO_RS_HOLD:
		status = "HOLDING";
		break;
	case SHO_RS_ARRIVE:
		status = "ARRIVING";
		break;
	default:
		status = "UNKNOWN";	/* We should never get here */
		break;
	}
	strncpy (reqname, qentry->reqname_v, MAX_REQNAME);
	reqname [MAX_REQNAME] = '\0';	/* Force null termination */
        if (flags & SHO_R_MONSANTO) {
                if (!monsanto_header) {
                        sho_mon_hdr();
                        monsanto_header++;
                }
		if (strlen(reqname) > (size_t) 14) {
			printf("%s\n", reqname);
			printf("%-14s", " ");
		} else 
			printf ("%-14s", reqname);
                printf ("  %5ld", (long) qentry->orig_seqno);
#if IS_UNICOS
                usummary(qentry->v.process_family);
                printf (" %7d",jobid);
#endif
                chars = strlen (username);
                if (chars > 8) username [8] = '\0';
                printf ("  %-8s", username);
#if IS_SGI | IS_IBMRS | IS_SOLARIS | IS_HPUX | IS_UNICOS | IS_DECOSF
		if (reqstate == SHO_RS_RUN) {
		    if (strlen(que_descr->v.que.namev.name) > (size_t) 8) {
			printf(" %s\n", que_descr->v.que.namev.name);
			printf(" %-38s", " ");
		    } else 
			printf(" %-8s", que_descr->v.que.namev.name);
		    if ((cfd = getreq ((long)qentry->orig_seqno,
				qentry->orig_mid, &rawreq)) != -1) {
		    	/*
		     	 *  We have a good control file.
		     	 */
			summary(qentry->v.process_family, &rawreq);
		    }
		} else {
		    printf(" %-36s", que_descr->v.que.namev.name);
		    printf("        ");
		}
#else
		printf(" %-36s", que_descr->v.que.namev.name);
		printf("        ");
#endif
                printf (" %1.1s", status);
                putchar ('\n');
        }

	if (flags & SHO_R_STANDARD) {
		if (strlen(reqname) > (size_t) 15) {
			printf (" %4d:%s\n", nth_request, reqname);
			printf (" %20s", " ");
		} else
			printf (" %4d:%15s", nth_request, reqname);
		chars = strlen (machinename);
		if (chars > 15) machinename [15] = '\0';
		printf (" %9ld.%s", (long) qentry->orig_seqno, machinename);
		for (; chars < 15; chars++) {
			putchar (' ');
		}
		chars = strlen (username);
		if (chars > 8) username [8] = '\0';
		printf (" %8s", username);
		printf (" %3d", qentry->priority);
		printf (" %8s", status);
		if (que_descr->v.que.type == QUE_DEVICE) {
			/*
			 *  For device queues we show the request size, and
			 *  not the process-family.
			 */
			printf ("%9lu", qentry->size);
		}
		else if (reqstate == SHO_RS_RUN &&
			 qentry->reqname_v [MAX_REQNAME]) {
			/*
			 *  For non-device queues, we show the process-family,
			 *  and not the request size.
			 *
			 *  Qentry->reqname_v [MAX_REQNAME] is used as a flag,
			 *  telling whether the process family field is valid
			 *  yet.
			 */
			printf ("%9ld", qentry->v.process_family);
		}
		putchar ('\n');
	}
	else if ( flags & SHO_R_LONG) { /* qstat -m or -l */
		printf ("  Rank %4d:", nth_request);
		printf ("  Name=%s\n", reqname);
		
		/*
		 *  Make sure that we only return the machine name,
		 *  as LoadL_starter expects this.
		 */
		loadlname = strdup (machinename);
		dotptr = strchr (loadlname, '.');
		if (dotptr != NULL) *dotptr = '\0';
		
		printf ("  REQUEST: %1ld.%s", (long) qentry->orig_seqno,
			loadlname);
		printf ("     Owner=%s  Priority=%1d  State: %s  ", username,
			qentry->priority, status);
		if (reqstate == SHO_RS_RUN) {
			/*
		 	 *  The request is running or is being spawned.  Try
		 	 *  to show the process family (group).
		 	 */
			if (qentry->reqname_v [MAX_REQNAME]) {
				/*
				 *  The process-family field validity byte is
				 *  set.  Show the process-family.
				 */
				printf ("Pgrp=%ld  ",
					qentry->v.process_family);
			}
		}
		else if (reqstate == SHO_RS_WAIT) {
			/*
			 *  Show the -a time.
			 */
			printf ("%s  ", fmttime (&qentry->v.start_time));
		}
		if (que_descr->v.que.type == QUE_DEVICE) {
			printf ("Size=%lu", qentry->size);
		}
		putchar ('\n');
	}
	if (flags & SHO_R_LONG) {
		if ((cfd = getreq ((long)qentry->orig_seqno, qentry->orig_mid,
				    &rawreq)) != -1) {
		    /*
		     *  We have a good control file.
		     */
		    printf ("  Created at %s\n",
			    fmttime (&rawreq.create_time));
		    if (que_descr->v.que.type == QUE_PIPE) {
			if (rawreq.start_time > 0) {
			    printf ("  Execute after %s\n",
				    fmttime (&rawreq.start_time));
			}
		    }
		    fputs ("  Mail = [", stdout);
		    if (rawreq.flags & RQF_BEGINMAIL) {
			if (rawreq.flags & RQF_ENDMAIL)
				fputs ("BEGIN, END]\n", stdout);
			else fputs ("BEGIN]\n", stdout);
		    }
		    else if (rawreq.flags & RQF_ENDMAIL) {
			fputs ("END]\n", stdout);
		    }
		    else fputs ("NONE]\n", stdout);
		    /*
		     *  Show where the mail will be sent.
		     */
		    printf ("  Mail address = %s@%s\n", rawreq.mail_name,
			    getmacnam (rawreq.mail_mid));
		    printf ("  Owner user name at originating ");
		    printf ("machine = %s\n", rawreq.username);
		    /*
		     * Show restart information.
		     */
		    fputs ("  Request is ", stdout);
		    if ( !(rawreq.flags & RQF_RESTARTABLE))
			    fputs ("not", stdout);
		    fputs("restartable, ", stdout);
		    if ( !(rawreq.flags & RQF_RECOVERABLE))
			    fputs ("not",  stdout);
		    fputs ("recoverable.\n", stdout);
		    /*
		     * Show broadcast information.
		     */
		    fputs ("  Broadcast = [", stdout);
		    if (rawreq.flags & RQF_BEGINBCST) {
			if (rawreq.flags & RQF_ENDBCST)
				fputs ("BEGIN, END]\n", stdout);
			else fputs ("BEGIN]\n", stdout);
		    }
		    else if (rawreq.flags & RQF_ENDBCST) {
			fputs ("END]\n", stdout);
		    }
		    else fputs ("NONE]\n", stdout);
		    /*
		     *  Show request-type dependent information.
		     */
		    if (rawreq.type == RTYPE_DEVICE) {
			/*
			 *  The request is a device-oriented request.
			 */
			printf ("  Forms = ");
			if (rawreq.v.dev.forms [0] == '\0') printf ("DEFAULT");
			else fputs (rawreq.v.dev.forms, stdout);
			printf (";  Copies = %1d\n", rawreq.v.dev.copies);
		    }
		    else {
			/*
			 *  The request is not device-oriented.
			 *  The request is a batch request requiring
			 *  only the resource of a CPU.
			 */
			shoreqlims (que_descr, &rawreq);
			printf ("  Standard-error access mode = ");
			shoomd (rawreq.v.bat.stderr_acc);
			if ((rawreq.v.bat.stderr_acc & OMD_EO) == 0) {
			    if (rawreq.v.bat.stderr_acc & OMD_M_KEEP) {
				if (que_descr->v.que.type == QUE_BATCH) {
				    /*
				     *  Use the name of the local host.
				     */
				    machinename = hostname;
				}
				else machinename = "<execution-host>";
			    }
			    else {
				machinename=getmacnam(rawreq.v.bat.stderr_mid);
			    }
			    printf ("  Standard-error name = %s:%s\n",
				    machinename, rawreq.v.bat.stderr_name);
			}
			printf ("  Standard-output access mode = ");
			shoomd (rawreq.v.bat.stdout_acc);
			if (rawreq.v.bat.stdout_acc & OMD_M_KEEP) {
			    if (que_descr->v.que.type==QUE_BATCH) {
				/*
				 *  Use the name of the local
				 *  host.
				 */
				machinename = hostname;
			    }
			    else machinename = "<execution-host>";
			}
			else {
			    machinename = getmacnam (rawreq.v.bat.stdout_mid);
			}
			printf ("  Standard-output name = %s:%s\n",
				machinename, rawreq.v.bat.stdout_name);
			printf ("  Shell = ");
			if (rawreq.v.bat.shell_name [0] == '\0') {
			    printf ("DEFAULT\n");
			}
			else {	/* An explicit shell spec is given */
			    fputs (rawreq.v.bat.shell_name, stdout);
			    putchar ('\n');
			}
			printf ("  Umask = %3o\n", rawreq.v.bat.umask);
			putchar ('\n');	/* Blank line separator */
		    }
		    close (cfd);		/* Close the control file */
		}
		else if (reqstate == SHO_RS_RUN) {
		    printf ("\n  <exiting>\n\n");
		}
		else {
		    printf ("\n  update in progress");
		    printf (" for sequence number: %ld, Machine id %lu errno returned: %d\n",
			(long)qentry->orig_seqno, (unsigned long) qentry->orig_mid, errno);
		}
	}
}


/*** shousr
 *
 *
 *	void shousr():
 *	Display a user name.
 */
static void shousr (unsigned long uid)
{
	if (acccolumn >= 4) {
		acccolumn = 0;
		printf ("\n    ");
	}
	printf ("%s\t", getusenam ((int) uid));
	acccolumn++;
}

#if 	IS_IRIX6 | IS_DECOSF
#include <sys/types.h>
#include <sys/dir.h>
	/* for IRIX 6, avoid 64 bit KMEM , and go straight to /proc */
static void summary(
	pid_t	proc_family,		/* process family */
	struct  rawreq	*rawreqp)
{
	struct	tm  *start_tm;
	int	days, hours, minutes;
	int	max_time;
        int     fd;
	prpsinfo_t prpsinfo;
        char char_pid[32];
	pid_t	sesid;
	DIR 	*dirp;
#if IS_IRIX6
	struct direct *dir;
#else
	struct dirent *dir;
#endif
  	int	retval;

	total_time = start_time = 0;

	/* use /proc to read leader info directly */
#if	IS_IRIX6
        sprintf(char_pid, "/proc/pinfo/%05d", proc_family);
#else
	sprintf(char_pid, "/proc/%05d", proc_family);
#endif
        fd = open (char_pid, O_RDONLY);
        if (fd != -1) {
	    retval = ioctl(fd, PIOCPSINFO, &prpsinfo);
	    start_time = prpsinfo.pr_start.tv_sec;
	    sesid = prpsinfo.pr_sid;
            close (fd);
        }

	/* walk /proc, accumulating time for all procs in sesid */
#if	IS_IRIX6
	dirp = opendir("/proc/pinfo");
	while ((dir = readdir(dirp)) != NULL) {
		sprintf(char_pid, "/proc/pinfo/%s",dir->d_name);
#else
	dirp = opendir("/proc");
	while ((dir = readdir(dirp)) != NULL) {
		sprintf(char_pid, "/proc/%s", dir->d_name);
#endif
		fd = open(char_pid,O_RDONLY);
		if (fd != -1) {
			retval = ioctl(fd,PIOCPSINFO, &prpsinfo);
			if (prpsinfo.pr_sid == sesid) {
#if IS_IRIX6
				total_time += prpsinfo.pr_time.tv_sec
					   +  prpsinfo.pr_ctime.tv_sec;
#else
				total_time += prpsinfo.pr_time.tv_sec;
#endif
			}
			close(fd);
		}
	}

	if ( start_time ) {
	    start_tm = localtime(&start_time);
	    printf(" %2d/%2.2d %2.2d:%2.2d", start_tm->tm_mon+1,
			start_tm->tm_mday,
			start_tm->tm_hour,
			start_tm->tm_min);
	} else {
	    printf("            ");
	}
	/*
	 * Calculate and print the per process max time.
	 */
	max_time = rawreqp->v.bat.ppcputime.max_seconds;
	days = max_time / (24 * 60 * 60);
	max_time -= days * 24 * 60 * 60;
	hours = max_time / (60 * 60);
	max_time -= hours * 60 * 60;
	minutes = max_time / 60;
	max_time -= minutes * 60;
	if (days > 9)
		printf("  %3d %2d:%2.2d", days, hours, minutes);
	else
		printf("  %d %2d:%2.2d:%2.2d", days, hours, minutes, max_time);
	/*
	 * Calculate and print the total time used by all processes.
	 */
	if ( total_time ) {
	    days = total_time / (24 * 60 * 60);
	    total_time -= days * 24 * 60 * 60;
	    hours = total_time / (60 * 60);
	    total_time -= hours * 60 * 60;
	    minutes = total_time / 60;
	    total_time -= minutes * 60;
	    if (days > 9)
	        printf("  %3d %2d:%2.2d ", days, hours, minutes);
	    else
	        printf("  %d %2d:%2.2d:%2.2d", days, hours, minutes, total_time);
	} else {
	    printf("             ");
	}
}


#else
#if	IS_SGI

static void summary(
	pid_t	proc_family,		/* process family */
	struct  rawreq	*rawreqp)
{

	struct	tm  *start_tm;
	int	status;
	struct proc *poffset;
	pid_t	pid_oi;
	struct	proc   proc_buff;
	int	days, hours, minutes;
	int	max_time;

	total_time = start_time = 0;
	pid_oi = proc_family;

#if	DEBUG_QSTAT
	printf( "pid is %d\n", pid_oi);
#endif
	procsize = sysmp(MP_KERNADDR,  MPKA_PROCSIZE);
#if	DEBUG_QSTAT
	printf ("Procsize is %d\n", procsize);
#endif
	poffset = (struct proc *)sysmp(MP_KERNADDR,  MPKA_PROC);
#if	DEBUG_QSTAT
	printf ("pointer is %x (or %d)\n", poffset, poffset);
#endif
	memfd = open (KMEM, O_RDONLY);
	if (memfd == -1) {
		perror("Open");
		exit(1);
	}
	poffset = (struct proc *) ( (int) poffset & ~0x80000000);
#if	DEBUG_QSTAT
	printf ("pointer is %x (or %d)\n", poffset, poffset);
#endif
	status = lseek(memfd, (int) poffset, SEEK_SET);
	if (status != (int) poffset) {
		perror("Lseek");
		exit(1);
	}
	status = read (memfd, &proc_buff, procsize);
	if (status != procsize) {
		perror ("Read");
		exit(1);
	}
	poffset =  proc_buff.p_child;
	do_count = 0;
	get_node(poffset, pid_oi, proc_family);
  
	if ( start_time ) {
	    start_tm = localtime(&start_time);
	    printf(" %2d/%2.2d %2.2d:%2.2d", start_tm->tm_mon+1,
			start_tm->tm_mday,
			start_tm->tm_hour,
			start_tm->tm_min);
	} else {
	    printf("            ");
	}
	/*
	 * Calculate and print the per process max time.
	 */
	max_time = rawreqp->v.bat.ppcputime.max_seconds;
	days = max_time / (24 * 60 * 60);
	max_time -= days * 24 * 60 * 60;
	hours = max_time / (60 * 60);
	max_time -= hours * 60 * 60;
	minutes = max_time / 60;
	max_time -= minutes * 60;
	printf("  %3d %2d:%2.2d", days, hours, minutes);
	/*
	 * Calculate and print the total time used by all processes.
	 */
	if ( total_time ) {
#if	!IS_IRIX5
	    total_time = total_time/100;
#endif
	    days = total_time / (24 * 60 * 60);
	    total_time -= days * 24 * 60 * 60;
	    hours = total_time / (60 * 60);
	    total_time -= hours * 60 * 60;
	    minutes = total_time / 60;
	    total_time -= minutes * 60;
	    if (days > 9)
	        printf("   %3d %2d:%2.2d ", days, hours, minutes);
	    else
	        printf("   %d %2d:%2.2d:%2.2d", days, hours, minutes, total_time);
	} else {
	    printf("             ");
	}
}
static get_node(
	struct proc *pointer,
	pid_t	pid_oi,
	pid_t	proc_family)
{
	int	status;
	struct  user  *uptr;
	struct user   auser;
	struct	proc   proc_buff;
#if   IS_IRIX5
        int     fd;
        int     retval;
        prstatus_t prstatus;
        char char_pid[16];
#endif


	if (pointer == 0) return(0);
        pointer = (struct proc *) ((int) pointer & ~0x80000000);
        status = lseek(memfd, (int) pointer, SEEK_SET);
        if (status != (int) pointer) {
            perror("Lseek");
            exit(1);
        }
        status = read (memfd, &proc_buff, procsize);
        if (status != procsize) {
            perror ("Read");
	    exit(1);
        }
	if (proc_buff.p_pid == pid_oi) do_count++;
#if	DEBUG_QSTAT
        if (do_count) printf(">>>>>>>>Pid is %d\n", proc_buff.p_pid);
#endif
	uptr = &auser;
	status = syssgi (SGI_RDUBLK, proc_buff.p_pid, uptr, sizeof(auser) );
#if   IS_IRIX5
        if (do_count) {
            sprintf(char_pid, "/proc/%05d", proc_buff.p_pid);
            fd = open (char_pid, O_RDONLY);
            if (fd != -1) {
                retval = ioctl(fd, PIOCSTATUS, &prstatus);
                total_time += prstatus.pr_cutime.tv_sec+
                                prstatus.pr_cstime.tv_sec +
                                prstatus.pr_utime.tv_sec +
                                prstatus.pr_stime.tv_sec;
#if   DEBUG_QSTAT
                printf(">>>>>>>>total_time: %d\n", total_time);
#endif
                close (fd);
            }
        }
#else

	if (do_count) total_time += uptr->u_utime + uptr->u_stime
			+ uptr->u_cutime + uptr->u_cstime;
#endif
	if (proc_buff.p_pid == proc_family) start_time = uptr->u_start;
        status = get_node (proc_buff.p_child, pid_oi, proc_family);
	if (proc_buff.p_pid == pid_oi) do_count = 0;
	status = get_node(proc_buff.p_sibling, pid_oi, proc_family);
}
#endif
#endif


#if	IS_IBMRS
static void summary(
	pid_t	proc_family,	    /* process family */
	struct  rawreq	*rawreqp)
{

	struct	tm  *start_tm;
	int	status;
	int	poffset;
	pid_t	pid_oi;
	unsigned long	procstart;
	struct	proc   myproc;
	int	days, hours, minutes;
	int	max_time;
	
	total_time = start_time = 0;
	pid_oi = proc_family;
#if	DEBUG_QSTAT
	printf( "pid is %d\n", pid_oi);
#endif
	memfd = open (KMEM, O_RDONLY);
	if (memfd == -1) {
		perror("Open");
		exit(1);
	}
	anlist.n_name = "proc";
	knlist(&anlist, 1, sizeof(struct nlist));
	procstart = anlist.n_value;
#if	DEBUG_QSTAT
	printf ("pointer is %x (or %d)\n", procstart, procstart); 
#endif
	status = findproc( procstart,  &myproc);
	poffset = (int) myproc.p_child;
	do_count = 0;
	get_node(poffset, pid_oi, proc_family);

	if ( start_time ) {
	    start_tm = localtime(&start_time);
	    printf(" %2d/%2.2d %2.2d:%2.2d", start_tm->tm_mon+1,
			start_tm->tm_mday,
			start_tm->tm_hour,
			start_tm->tm_min);
	} else {
	    printf("            ");
	}
	/*
	 * Calculate and print the per process max time.
	 */
	max_time = rawreqp->v.bat.ppcputime.max_seconds;
	days = max_time / (24 * 60 * 60);
	max_time -= days * 24 * 60 * 60;
	hours = max_time / (60 * 60);
	max_time -= hours * 60 * 60;
	minutes = max_time / 60;
	max_time -= minutes * 60;
	printf("  %3d %2d:%2.2d", days, hours, minutes);
	/*
	 * Calculate and print the total time used by all processes.
	 */
	if ( total_time ) {
	    days = total_time / (24 * 60 * 60);
	    total_time -= days * 24 * 60 * 60;
	    hours = total_time / (60 * 60);
	    total_time -= hours * 60 * 60;
	    minutes = total_time / 60;
	    total_time -= minutes * 60;
	    if (days > 9)
	        printf("   %3d %2d:%2.2d ", days, hours, minutes);
	    else
	        printf("   %d %2d:%2.2d:%2.2d", days, hours, minutes, total_time);
	} else {
	    printf("             ");
	}
}
static get_node(
	int	pointer,
	pid_t	pid_oi,
	pid_t	proc_family)
{
	int	status;
	struct  user  myuser;
	struct	proc   myproc;

	if (pointer == 0) return(0);
	status = findproc(pointer,  &myproc);
	if (myproc.p_pid == pid_oi) do_count++;
#if	DEBUG_QSTAT
        if (do_count) printf(">>>>>>>>Pid is %d\n", myproc.p_pid);
#endif
	status = finduser(myproc.p_pid,  &myuser);
#if	IS_AIX4
	if (do_count) total_time += myuser.U_utime+myuser.U_stime
			+myuser.U_cutime+myuser.U_cstime;
#else
	if (do_count) total_time += myuser.u_utime+myuser.u_stime
			+myuser.u_cutime+myuser.u_cstime;
#endif
#if	IS_DEBUG_QSTAT
	printf("Total time is %d\n",  total_time);
#endif
#if	IS_AIX4
	if (myproc.p_pid == proc_family) start_time = myuser.U_start;
#else
	if (myproc.p_pid == proc_family) start_time = myuser.u_start;
#endif
        status = get_node((int)myproc.p_child, pid_oi, proc_family);
	if (myproc.p_pid == pid_oi) do_count = 0;
	status = get_node((int)myproc.p_siblings, pid_oi, proc_family);
}
static findproc(
	unsigned long address,
	struct proc *myproc)
{
    int	    rtn;
    unsigned long   offset;
    offset = address & 0x7fffffff;
    rtn = lseek(memfd,  offset,  0);
    if (rtn == -1) return (0);
    rtn = readx(memfd, (char *)myproc, sizeof(struct proc),  1);
    if (rtn == -1) return (0);
    return(1);
}

finduser(long pid,  struct user *user)
{
    struct procinfo pinfo[MAX_PROC];
    int	proc_no,  nproc;
    int status;
    
    nproc = getproc(&pinfo[0],  MAX_PROC,  sizeof(struct procinfo) );
#if DEBUG_QSTAT
    printf("Nproc = %d\n",  nproc);
#endif
    for (proc_no = 0; proc_no < nproc; proc_no++) {
	if (pid == pinfo[proc_no].pi_pid) break;
    }
    status = getuser(&pinfo[proc_no],  sizeof(struct procinfo),  user, 
		    sizeof(struct user) );
#if DEBUG_QSTAT
    printf("Getuser status is %d\n",  status);
#endif
    return (status);
}
#endif

#if	IS_HPUX & ! IS_HPUX10

static void summary(
	pid_t	proc_family,	    /* process family */
	struct  rawreq	*rawreqp)
{

    struct	tm  *start_tm;
    int		status;
    static int nlist_flag = 0;
    int		i;
    pid_t	pid_oi;
    int		days, hours, minutes;
    int		max_time;
	
    total_time = start_time = 0;
    pid_oi = proc_family;
#if	DEBUG_QSTAT
    printf( "pid is %d\n", pid_oi);
#endif
    memfd = open (KMEM, O_RDONLY);
    if (memfd == -1) {
       perror("Open");
       exit(1);
    }
    if (!nlist_flag) {
#ifdef __hp9000s800
        /* 800 names don't have leading underscores */
        for (i = 0; nlst[i].n_name; nlst[i++].n_name++)
            continue;
#endif

        status =  nlist("/hp-ux", nlst);

        if (nlst[0].n_type == 0) {
            fprintf(stderr, "psx: nlist failed\n");
        }
	nlist_flag = 1;
    }
    (void) getkval(nlst[X_PROC].n_value,   (int *)(&proc),      sizeof(proc),
            nlst[X_PROC].n_name);
    (void) getkval(nlst[X_NPROC].n_value,  &nproc,              sizeof(nproc),
            nlst[X_NPROC].n_name);
#ifdef X_HZ
    (void) getkval(nlst[X_HZ].n_value,     (int *)(&hz),        sizeof(hz),
            nlst[X_HZ].n_name);
#else
    hz = HZ;
#endif

    bytes = nproc * sizeof(struct proc);
    pbase = (struct proc *)malloc(bytes);
    if (pbase == NULL ) printf("Error allocating pbase.\n");
    pst   = (union pstun *)malloc(nproc * sizeof(union pstun));

    (void) getkval(proc, (int *)pbase, bytes, "proc array");
    for (i = 0; i < nproc; ++i) {
        pst[i].pst_status=(struct pst_status *)malloc(sizeof(struct pst_status));
        if (pstat(PSTAT_PROC, pst[i], sizeof(struct pst_status), 0, pbase[i].p_pid) != 1)
	           pbase[i].p_upreg = (preg_t *) 0;
        else
            pbase[i].p_upreg = (preg_t *) pst[i].pst_status;
        pbase[i].p_nice = pst[i].pst_status->pst_nice;
        pbase[i].p_cpticks = pst[i].pst_status->pst_cpticks;
    }
    /*
     * Implemntation note:  It looks like the p_sid field can be used.  If
     * the p_sid is the same as the process family,  then we can count it.
     */
    get_process_info (pid_oi, proc_family);
    if ( start_time ) {
       start_tm = localtime(&start_time);
       printf(" %2d/%2.2d %2.2d:%2.2d", start_tm->tm_mon+1,
			start_tm->tm_mday,
			start_tm->tm_hour,
			start_tm->tm_min);
   }  else {
       printf("            ");
    }
    /*
     * Calculate and print the per process max time.
     */
    max_time = rawreqp->v.bat.ppcputime.max_seconds;
    days = max_time / (24 * 60 * 60);
    max_time -= days * 24 * 60 * 60;
    hours = max_time / (60 * 60);
    max_time -= hours * 60 * 60;
    minutes = max_time / 60;
    max_time -= minutes * 60;
    printf("  %3d %2d:%2.2d", days, hours, minutes);
    /*
     * Calculate and print the total time used by all processes.
     */
    if ( total_time ) {
        days = total_time / (24 * 60 * 60);
        total_time -= days * 24 * 60 * 60;
        hours = total_time / (60 * 60);
	total_time -= hours * 60 * 60;
        minutes = total_time / 60;
        total_time -= minutes * 60;
        if (days > 9)
            printf("   %3d %2d:%2.2d ", days, hours, minutes);
        else
	    printf("   %d %2d:%2.2d:%2.2d", days, hours, minutes, total_time);
    } else {
        printf("             ");
    }
  
  /* Generic NQS 3.50.0-pre7
   * 
   * Release the memory we malloc'd above.
   * Thanks to Michael Andrews for pointing this out
   */

  for (i=0; i< nproc; i++)
    free(pst[i].pst_status);
	 
  free(pst);
  free(pbase);
}

get_process_info(
	pid_t pid,
	pid_t proc_family)
{
    int i;
    struct proc *pp;
    struct pst_status *ps;

    for (pp = pbase , i = 0; i< nproc; i++, pp++) {
	if (pp->p_pid == proc_family) start_time = pp->p_start;
        if (pp->p_pid == pid) {
            if ((ps = (struct pst_status *) pp->p_upreg) != NULL) {
                total_time += (ps->pst_cptickstotal)/hz;
            }
        }
        if (pp->p_ppid == pid) get_process_info(pp->p_pid, proc_family);
     }
}

/*
 *  getkval(offset, ptr, size, refstr) - get a value out of the kernel.
 *      "offset" is the byte offset into the kernel for the desired value,
 *      "ptr" points to a buffer into which the value is retrieved,
 *      "size" is the size of the buffer (and the object to retrieve),
 *      "refstr" is a reference string used when printing error meessages,
 *
 */

getkval(
	unsigned long offset,
	int *ptr,
	int size,
	char *refstr)

{
    if (lseek(memfd, (long)offset, L_SET) == -1) {
        (void) fprintf(stderr, "%s: lseek to %s: %s\n", "/dev/kmem",
                       refstr, libsal_err_strerror(errno));
        exit(23);
    }
    if (read(memfd, (char *) ptr, size) == -1) {
            (void) fprintf(stderr, "%s: reading %s: %s\n", "/dev/kmem",
                           refstr, libsal_err_strerror(errno));
            exit(23);
    }
    return(1);
}

#endif

#if IS_SOLARIS | IS_HPUX10
 
static void summary(
        pid_t   proc_family,            /* process family */
        struct  rawreq  *rawreqp)

{
 
    struct      tm  *start_tm;
    int         days, hours, minutes;
    int         max_time;
    pid_t	pid;
 
    total_time = start_time = 0;
 
    pid = proc_family;
    get_process_info (pid, proc_family);
    if ( start_time ) {
       start_tm = localtime(&start_time);
       printf(" %2d/%2.2d %2.2d:%2.2d", start_tm->tm_mon+1,
                        start_tm->tm_mday,
                        start_tm->tm_hour,
                        start_tm->tm_min);
   }  else {
       printf("            ");
    }
    /*
     * Calculate and print the per process max time.
     */
    max_time = rawreqp->v.bat.ppcputime.max_seconds;
    days = max_time / (24 * 60 * 60);
    max_time -= days * 24 * 60 * 60;
    hours = max_time / (60 * 60);
    max_time -= hours * 60 * 60;
    minutes = max_time / 60;
    max_time -= minutes * 60;
    printf("  %3d %2d:%2.2d", days, hours, minutes);
    /*
     * Calculate and print the total time used by all processes.
     */
    if ( total_time ) {
        days = total_time / (24 * 60 * 60);
        total_time -= days * 24 * 60 * 60;
        hours = total_time / (60 * 60);
        total_time -= hours * 60 * 60;
        minutes = total_time / 60;
        total_time -= minutes * 60;
        if (days > 9)
            printf("   %3d %2d:%2.2d ", days, hours, minutes);
        else
            printf("   %d %2d:%2.2d:%2.2ld", days, hours, minutes, total_time);
    } else {
        printf("             ");
    }
}

#if  IS_SOLARIS
#define PROCFS "/proc"
 
int get_process_info(pid_t pid, pid_t proc_family)
{
    char current_dir[255];
    static DIR *procdir;
    struct dirent *direntp;
    struct prpsinfo *ps, ps_struct;
 
    (void) getcwd (current_dir, 255);
    procdir = opendir (PROCFS);
 
    chdir (PROCFS);
 
    ps=&ps_struct;
    for (rewinddir (procdir); (direntp = readdir (procdir));)
    {
        int fd;

#ifdef USE_NEW_PROC
	char buf[30];

	sprintf(buf, "%s/psinfo", direntp->d_name);
	if ((fd = open (buf, O_RDONLY)) < 0)
		continue;
	if (read(fd, ps, sizeof(psinfo_t)) != sizeof(psinfo_t)) {
		(void) close(fd);
		continue;
	}
#else 
        if ((fd = open (direntp->d_name, O_RDONLY)) < 0)
                continue;
 
        if (ioctl (fd, PIOCPSINFO, ps) < 0) {
                (void) close (fd);
                continue;
        }
#endif /* USE_NEW_PROC */ 
	close (fd);

        if (ps->pr_pid == proc_family) 
		start_time = ps->pr_start.tv_sec;

	if (ps->pr_pid == pid || ps->pr_sid == pid) 
        	total_time += ps->pr_time.tv_sec;
	if (ps->pr_ppid == pid)
		get_process_info(ps->pr_pid, proc_family);
     }
     closedir(procdir);
     chdir(current_dir);
}
#else	/* IS_HPUX10 */

#define BULK 32
int get_process_info(pid_t pid, pid_t proc_family)
{
	struct	pst_status	pst[BULK];
	int			i, count, index = 0;

	/* read bulk of data */
	while ((count = pstat_getproc(pst, sizeof(pst[0]), BULK, index)) > 0)
	{
		for (i = 0; i < count; i++)
		{
			if (pst[i].pst_pid == proc_family)
				start_time = pst[i].pst_start;
			if (pst[i].pst_sid == proc_family)
				total_time += (pst[i].pst_utime + 
				               pst[i].pst_stime);
		}
		/* update bulk index */
		index = pst[count-1].pst_idx +1;
	}
}

#endif 
#endif

#if IS_UNICOS

static void summary(
	       pid_t	proc_family,
	       struct rawreq * rawreqp)
{
  struct	tm *start_tm;
  int		days, hours, minutes;
  int		max_time;
  
  if ( start_time ) 
  {
    start_tm = localtime(&start_time);
    printf(" %2d%2.2d %2.2d:%2.2d", start_tm->tm_mon+1,
	   start_tm->tm_mday, start_tm->tm_hour, start_tm->tm_min);
  }
  else
  {
    printf("            ");
  }
     /*
      * Calculate and print the per process max time.
      */
     max_time = rawreqp->v.bat.prcputime.max_seconds;
     days = max_time / (24 * 60 * 60);
     max_time -= days * 24 * 60 * 60;
     hours = max_time / (60 * 60);
     max_time -= hours * 60 * 60;
     minutes = max_time / 60;
     max_time -= minutes * 60;
     printf("  %3d %2d:%2.2d", days, hours, minutes);
     /*
      * Calculate and print the total time used by all processes.
      */
     if ( total_time ) {
         days = total_time / (24 * 60 * 60);
         total_time -= days * 24 * 60 * 60;
         hours = total_time / (60 * 60);
         total_time -= hours * 60 * 60;
         minutes = total_time / 60;
         total_time -= minutes * 60;
         if (days > 9)
             printf("   %3d %2d:%2.2d ", days, hours, minutes);
         else
             printf("   %d %2d:%2.2d:%2.2ld", days, hours, minutes, total_time)

     } else {
         printf("             ");
     }
 }


/*  under unicos, job information (used for per request limits)
 *  is now merged into an extended session structure. Therefore
 *  the session structure for a job has cumulative cpu times
 *  for all processes in the job. This means we don't have to
 *  walk all processes to accumulate times for a process group:
 *  simply open the /proc entry for the process group leader,
 *  which is also the job leader, and read the cpu times
 *  directly in the session structure.
 */
int usummary(pid_t pgrp)
{
        struct ucomm u;
        struct sess s;
        int pfd,r;
        char proc[32];
  	sprintf(proc,"/proc/%d",pgrp);
        pfd = open(proc,O_RDONLY);
        if (pfd == -1)
                return(-1);

        lseek(pfd,PRFS_UCOMM,0);
  	r = read(pfd,(char *)&u,sizeof(u));
        if (r != sizeof(u))
                return(-1);

        lseek(pfd,PRFS_SESS,0);
        r = read(pfd,(char *)&s,sizeof(s));
        if (r != sizeof(s))
                return(-1);

        start_time = u.uc_start;
        total_time = s.s_ucputime/CLK_TCK + s.s_scputime/CLK_TCK;
        jobid = s.s_sid;
}

#endif



static void sho_mon_hdr(void)
{
#if IS_UNICOS
        printf("Request         I.D.   Jobid   Owner    Queue    ");
#else
        printf("Request         I.D.   Owner    Queue    ");
#endif
#if IS_SGI | IS_IBMRS | IS_SOLARIS | IS_HPUX | IS_UNICOS | IS_DECOSF
        printf("Start Time   Time Limit  Time Used  ");
#else
        printf("                                    ");
#endif
        printf("St\n");
#ifdef IS_UNICOS
        printf("-------------- ------- ------- -------- -------- ");
#else
        printf("-------------- ------- -------- -------- ");
#endif
  
#if IS_SGI | IS_IBMRS | IS_SOLARIS | IS_HPUX | IS_UNICOS | IS_DECOSF
	printf(          "-----------  ----------  ---------- ");
#else
	printf(          "                                    ");
#endif
	printf(                                              "--\n");
}

