/*
 * libnqs/queue/shoallque.c
 * 
 * DESCRIPTION:
 *
 *	Show information about ALL queues on the local machine as
 *	selected by the request selection and display criteria.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <unistd.h>			/* For getuid */
#include <string.h>
#include <SETUP/General.h>

/*** shoallque
 *
 *
 *	int shoallque():
 *	Show status information on ALL of the local NQS queues.
 *
 *	WARNING:
 *		It is assumed that the current position of
 *		the queue definitions file is 0.
 *
 *	Returns:
 *		0: If output was produced.
 *	       -1: If no output was produced.
 *
 */
int shoallque (
	struct confd *file,		/* MUST be the NQS queue file */
	long flags,			/* Display flags */
	uid_t whomuid,			/* Whom we are interested in */
	struct reqset *reqs,		/* Req select set */
	short daepresent,		/* Boolean non-zero if the local */
					/* NQS daemon is present and running */
	struct confd *qmapfile,		/* Queue/device/destination mapping */
					/* file */
	struct confd *pipeqfile,	/* Pipe-queue destination file */
	int	xrmt,			/* 1 if remote, 0 if not     */
	struct confd *qcomplexfile)     /* Queue complex definition file */
{
	int result;
        Mid_t itsmid;                   /* The machine id of the queue */
        Mid_t mymid;                    /* The local machine id */
	register struct gendescr *descr;
	char *cp;
	FILE *domainfile;
	char domainfile_name[64];
	char hostname[256];
	char athostname[256];
	uid_t myuid;
	struct passwd *my_passwd;

	if (xrmt != 0) xrmt = 1;        /* basic programming paranoia */
	result = -1;			/* No output produced */
	descr = nextdb (file);

	while (descr != (struct gendescr *)0) {
		if (shoqbydesc (file, descr, flags, whomuid, reqs,
			daepresent, qmapfile, pipeqfile,
			qcomplexfile) == 0) {
			/*
			 *  Output was produced.
			 */
			result = 0;
                }
                descr = nextdb (file);  /* Get the next queue */
        }
        if ( flags & SHO_R_DEST) {
	    myuid = getuid();
	    my_passwd = sal_fetchpwuid(myuid);
	    sprintf(domainfile_name,  "%s/.qstat", my_passwd->pw_dir);
	    domainfile = fopen (domainfile_name, "r");
	    if (domainfile == (FILE *) 0) {
	        sprintf(domainfile_name, "%s/nqs-domain", NQS_LIBEXE);
	        domainfile = fopen (domainfile_name, "r");
                if (domainfile == (FILE *) 0) {
		    /*
                     *  The domain file was not successfully opened.
                     */
		     fprintf(stderr,"Cannot open NQS domain file %s.\n",
				domainfile_name);
		     return (0);
	         }
	     }
	    if (localmid (&mymid) != 0) {
                fprintf (stderr, "Unexpected error in localmid().\n");
                return (0);
	    }
	    while ( (fgets(hostname, sizeof(hostname), domainfile) ) != 
			(char *) 0 ) { 
		cp = strchr(hostname, '\n');
		if (cp != (char *) 0 ) *cp = '\0';
		if (hostname[0] == '#') continue;
                /*
                 *  Output was produced.
                 */
                result = 0;
	        strcpy(athostname,  "@");
		strcat(athostname,  hostname);
		machspec (athostname, &itsmid);
        	if ((itsmid != mymid) && (xrmt == 0)) {
		    printf("Destination machine: %s\n", hostname);
		    shoqbyname (file, athostname, flags, 
			     whomuid, reqs, daepresent,qmapfile, pipeqfile,
			     qcomplexfile);

		}
	    }
	}
	return (result);
}
