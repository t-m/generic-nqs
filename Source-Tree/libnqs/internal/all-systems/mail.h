#ifndef __NQS_INTERNAL_MAIL_HEADER
#define __NQS_INTERNAL_MAIL_HEADER

/*
 * h/nqs/internal/mail.h
 *
 * DESCRIPTION:
 *
 *	NQS mail suffix codes and mail file definitions.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 26, 1986.
 */

/*
 *	Mail suffix codes.
 */
#define	MSX_ABORTED	0	/* "aborted" */
#define	MSX_ABORTSHUTDN	1	/* "aborted for NQS shutdown" */
#define	MSX_BEGINNING	2	/* "beginning" */
#define	MSX_DELETED	3	/* "deleted" */
#define	MSX_DELIVERED	4	/* "delivered" */
#define	MSX_ENDED	5	/* "ended */
#define	MSX_FAILED	6	/* "files being placed in NQS failed */
				/*  directory" */
#define	MSX_REQUEUED	7	/* "requeued" */
#define	MSX_RESTARTING	8	/* "restarting" */
#define	MSX_MAXMSX	MSX_RESTARTING
				/* Used in ../src/nqs_mai.c */


/*
 *	NQS mail requestlog structure.
 *
 *	This structure is passed to nqs_mai() for both batch and device
 *	requests, to report completion information.  However, this
 *	structure is not stored in permanent memory (i.e. disk), except
 *	for batch requests, which use this structure to preserve information
 *	across crashes and system shutdowns.
 *
 *	WARNING:
 *	    This structure must NOT be larger than ATOMICBLKSIZ bytes
 *	    in size (see ../h/nqs.h) for the crash recovery mechanisms
 *	    of NQS to properly work.  This restriction is verified in
 *	    ../src/nqs_ldconf.c.
 */
struct requestlog {
	short reqrcmknown;		/* Boolean request RCM code known */
	struct servermssg svm;		/* Request completion code and */
					/* server message */
	long outrcmknown;		/* Bit mask of known RCM_ */
					/* stageout event completion codes */
	long outrcm [MAX_OUSTAPERREQ+3];/* RCM_ codes for stageout events */
};

#endif /* __NQS_INTERNAL_MAIL_HEADER */
