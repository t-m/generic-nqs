/*
 * libnqs/comms/setpeertcm.c
 * 
 * DESCRIPTION:
 *
 *	Return TCMx_ code with peer origin bit set.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	May 21, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/informcc.h>		/* NQS information completion codes */

/*** setpeertcm
 *
 *
 *	long setpeertcm():
 *	Return TCMx_ code with peer origin bit set.
 */
long setpeertcm (long code)
{
	return (code | XCI_PEER_MASK);	/* Return TCMx_ code with peer bit */
}
