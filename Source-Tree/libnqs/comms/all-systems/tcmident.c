/*
 * libnqs/comms/tcmident.c
 * 
 * DESCRIPTION:
 *
 *	Return a character string pointer to the identifier
 *	associated with a specific transaction reason code.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	May 22, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/informcc.h>		/* NQS information completion codes */
#include <libnqs/transactcc.h>		/* NQS transaction completion codes */
#include <string.h>

#define	LOCAL_ORIGIN	001	/* Code can originate locally */
#define	PEER_ORIGIN	002	/* Code can originate at peer */



/*** tcmident
 *
 *
 *	char *tcmident():
 *
 *	Return a character string pointer to the identifier
 *	associated with a specific transaction reason code.
 */
char *tcmident (long code)
{
	static char identbuffer [32];	/* Identifier name buffer for return */
	static struct {
		char *generic_name;	/* Generic name of transaction code */
		short origin;		/* Possibilities of origin */
	} identifiers [] = {
		{ "ACCESSDEN",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ALREADACC",	LOCAL_ORIGIN			},
		{ "ALREADEXI",	LOCAL_ORIGIN			},
		{ "BADCDTFIL",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "CLIMIDUNKN",			PEER_ORIGIN	},
		{ "COMPLETE",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "CONNBROKEN",			PEER_ORIGIN	},
		{ "CONNTIMOUT",			PEER_ORIGIN	},
		{ "CONTINUE",			PEER_ORIGIN	},
		{ "CPUALRESVD",	LOCAL_ORIGIN			},
		{ "DEVACTIVE",	LOCAL_ORIGIN			},
		{ "DEVENABLE",	LOCAL_ORIGIN			},
		{ "EACCESS",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "EFBIG",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "EISDIR",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ELOOP",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ENFILE",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ENOBUFS",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ENOENT",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ENOMEM",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ENOSPC",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ENOTDIR",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ENXIO",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "EPERM",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "EPIPE",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "EROFS",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ERRORRETRY",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ETIMEDOUT",	LOCAL_ORIGIN			},
		{ "ETXTBSY",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "FATALABORT",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "FIXBYNQS",	LOCAL_ORIGIN			},
		{ "GRANFATHER",	LOCAL_ORIGIN			},
		{ "INSHUTDOWN",	LOCAL_ORIGIN			},
		{ "INSQUESPA",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "INSUFFMEM",	LOCAL_ORIGIN			},
		{ "INSUFFPRV",	LOCAL_ORIGIN			},
		{ "INTERNERR",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "LOGFILERR",	LOCAL_ORIGIN			},
		{ "MAXNETCONN",			PEER_ORIGIN	},
		{ "MAXQDESTS",	LOCAL_ORIGIN			},
		{ "MIDCONFLCT",			PEER_ORIGIN	},
		{ "NETDBERR",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "NETNOTSUPP",	LOCAL_ORIGIN			},
		{ "NETPASSWD",			PEER_ORIGIN	},
		{ "NOACCAUTH",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "NOACCNOW",	LOCAL_ORIGIN			},
		{ "NOESTABLSH",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "NOLOCALDAE",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "NOMOREPROC",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "NONETDAE",			PEER_ORIGIN	},
		{ "NONSECPORT",			PEER_ORIGIN	},
		{ "NOPORTAVAI",	LOCAL_ORIGIN			},
		{ "NOSUCHACC",	LOCAL_ORIGIN			},
		{ "NOSUCHCPU",	LOCAL_ORIGIN			},
		{ "NOSUCHDES",	LOCAL_ORIGIN			},
		{ "NOSUCHDEV",	LOCAL_ORIGIN			},
		{ "NOSUCHFORM",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "NOSUCHGRP",	LOCAL_ORIGIN			},
		{ "NOSUCHMAC",	LOCAL_ORIGIN			},
		{ "NOSUCHMAN",	LOCAL_ORIGIN			},
		{ "NOSUCHMAP",	LOCAL_ORIGIN			},
		{ "NOSUCHQUE",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "NOSUCHQUO",	LOCAL_ORIGIN			},
		{ "NOSUCHREQ",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "NOSUCHSIG",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "NOTREQOWN",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "PATHLEN",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "PEERARRIVE",	LOCAL_ORIGIN			},
		{ "PEERDEPART",	LOCAL_ORIGIN			},
		{ "PLOCKFAIL",	LOCAL_ORIGIN			},
		{ "PROTOFAIL",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "QUEDISABL",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "QUEENABLE",	LOCAL_ORIGIN			},
		{ "QUEHASREQ",	LOCAL_ORIGIN			},
		{ "QUOTALIMIT",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "REQCOLLIDE",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "REQDELETE",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "REQRUNNING",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "REQSIGNAL",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "ROOTINDEL",	LOCAL_ORIGIN			},
		{ "RRFUNKNMID",			PEER_ORIGIN	},
		{ "SELMIDUNKN",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "SELREFDES",	LOCAL_ORIGIN			},
		{ "SHUTERROR",	LOCAL_ORIGIN			},
		{ "SUBMITTED",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "TOOMANDEV",	LOCAL_ORIGIN			},
		{ "UNAFAILURE",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "UNDEFINED",	LOCAL_ORIGIN |	PEER_ORIGIN	},
		{ "UNRESTR",	LOCAL_ORIGIN			},
		{ "WROQUETYP",	LOCAL_ORIGIN |	PEER_ORIGIN	},
                { "NOSUCHCOM",  LOCAL_ORIGIN                    },
                { "TOOMANQUE",  LOCAL_ORIGIN                    },
                { "TOOMANCOM",  LOCAL_ORIGIN                    },
                { "ALREADCOM",  LOCAL_ORIGIN                    },
                { "NOTMEMCOM",  LOCAL_ORIGIN                    },
                { "REQRESUMED", LOCAL_ORIGIN | PEER_ORIGIN      },
                { "REQSUSPENDED",  LOCAL_ORIGIN | PEER_ORIGIN   },
                { "REQWASRUNNING",  LOCAL_ORIGIN | PEER_ORIGIN  },
                { "REQWASSUSPENDED",  LOCAL_ORIGIN | PEER_ORIGIN},
		{ "QUEBUSY",    LOCAL_ORIGIN | PEER_ORIGIN      },
		{ "NOMOREMEM",  LOCAL_ORIGIN                    },
		{ "REQNOTHELD", LOCAL_ORIGIN                    },
		{ "INVLDLBFLAGS", LOCAL_ORIGIN                  },
		{ "DAEALRUN", LOCAL_ORIGIN                      },
		{ "DAENOTRUN", LOCAL_ORIGIN                     }
		
	};

	register short reason;		/* Reason bits of completion code */

	reason = (code & (XCI_REASON_MASK & ~XCI_PEER_MASK));
	if (reason > (TCM_MAXTCM & (XCI_REASON_MASK & ~XCI_PEER_MASK)) ||
	   !(code & XCI_TRANSA_MASK)) {
		/*
		 *  The transaction code is invalid.
		 */
		reason = (TCML_UNDEFINED & XCI_REASON_MASK);
	}
	if (reason & XCI_PEER_MASK) {
		/*
		 *  The transaction code originated at a transaction
		 *  peer.
		 */
		strcpy (identbuffer, "TCMP_");
		if (identifiers [reason].origin & PEER_ORIGIN) {
			strcat (identbuffer, identifiers[reason].generic_name);
		}
		else strcat (identbuffer, "badpeercod");
	}
	else {	/* The transaction code originated at the local host */
		strcpy (identbuffer, "TCML_");
		if (identifiers [reason].origin & LOCAL_ORIGIN) {
			strcat (identbuffer, identifiers[reason].generic_name);
		}
		else strcat (identbuffer, "badlocalcd");
	}
	return (identbuffer);
}
