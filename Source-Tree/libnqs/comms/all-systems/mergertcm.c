/*
 * libnqs/comms/mergertcm.c
 *
 * DESCRIPTION:
 *
 *	Merge a TCMx_ code with an RCM_ code.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 28, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/informcc.h>	/* Completion code information bits */

/*** mergertcm
 *
 *
 *	long mergertcm():
 *	Merge TCMx_ code with RCM_code.
 */
long mergertcm (long rcm_original, long tcm_original)
{
	return (rcm_original | ((tcm_original & XCI_FULREA_MASK) << 10));
}
