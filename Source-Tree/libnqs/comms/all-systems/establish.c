/*
 * libnqs/comms/establish.c
 * 
 * DESCRIPTION:
 *
 *	Establish a network connection with a remote NQS network
 *	daemon. Give our caller a SIGPIPE handler.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Robert W. Sandstrom.
 *	Sterling Software Incorporated.
 *	April 21, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>			/* NQS types and definitions */
					/* + sys/types.h + nmap.h + quolim.h */
#include <libnqs/informcc.h>		/* Information completion codes */
#include <libnqs/transactcc.h>		/* Transaction completion codes */
#include <libnqs/netpacket.h>		/* Network packet types */
#include <libnmap/nmap.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <SETUP/General.h>
#include <netdb.h>			/* Network database */
#include <signal.h>			/* For SIGPIPE */
#include <errno.h>			/* System call failure errnos */
#include <sys/socket.h>			/* Socket stuff */
#include <netinet/in.h>			/* Internet stuff */
#include <unistd.h>

static void brokenconnection ( int );

/*** establish
 *
 *
 *	int establish():
 *
 *	Establish a network connection with a remote NQS network
 *	daemon.
 *
 *	Returns:
 *		>= 0: if successful; the value returned is the
 *		      "file" descriptor associated with the connection.
 *
 *		-1:   if the connection failed, and we should NOT retry.
 *
 *		-2:   if the connection failed, and we should retry
 *			after some seconds or minutes.
 */
int establish (
	int packettype,
	Mid_t targetmid,
	uid_t cuid,
	char *cusername,
	long *transactcc)
{
	
#if	GPORT_HAS_BSD_TIME
	struct timeval tv;
	struct timezone tz;
#endif
	Mid_t hostmid;			/* Local host machine-id */
	char *targetname;		/* Hostname of target machine */
	time_t elapsed;			/* Seconds since 1970 */
	char *tzn;			/* Time zone name */
	struct tm *tp;			/* Time structure pointer */
	char netpassword [MAX_NETPASSWORD+1];
					/* Network password */
	struct sockaddr_in rsin;	/* Remote Internet socket address */
	struct sockaddr_in lsin;	/* Local Internet socket address */
	struct servent *servent;	/* NQS service entry */
	struct hostent *hostent;	/* Host entry for target */
	struct passwd *passwd;		/* Client's password */
	int socket_descr;		/* Socket descriptor */
	char packet [MAX_PACKET];	/* Network message packet */
	int packetsize;			/* Size of network packet */
  	int wSize;
	short tryport;			/* Try to bind to this port */
	short retry;			/* Boolean retry flag */
	int timeout;			/* Seconds before next connect() */
	int integers;			/* # of 32 bit integers in packet */
	int strings;			/* # of strings in packet */

  	assert(cusername != NULL);
  	assert(transactcc != NULL);
  
	/*
	 *  Get the principal name of the target machine.
	 */
	targetname = nmap_get_nam (targetmid);
	if (targetname == (char *) 0) {
		/*
		 *  Cannot determine hostname for mid.
		 */
		*transactcc = TCML_NETDBERR;
		return (-1);
	}
	if (localmid (&hostmid) != 0) {
		/*
		 *  Our attempt at determining the machine-id of the local
		 *  host was unsuccessful.
		 */
		*transactcc = TCML_SELMIDUNKN;
		return (-1);
	}
	servent = getservbyname (NQS_SERVICE_NAME, "tcp");
	if (servent == (struct servent *) 0) return (-1);
	/*
	 *  Determine the address of the host that we are trying
	 *  to reach.
	 */
	hostent = gethostbyname (targetname);
	if (hostent == (struct hostent *) 0) {
		*transactcc = TCML_NETDBERR;
		return (-1);
	}
	/*
	 * Verify the existence of the client we are claiming to be.
	 */
  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "Doing the fetchpwuid for cuid %d.\n", cuid);
	if ((passwd = sal_fetchpwuid (cuid)) == (struct passwd *) 0) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "fetchpwuid not return structure.\n");
		*transactcc = TCML_NOACCAUTH;
		return (-1);
	}
	if (strcmp (passwd->pw_name, cusername)) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Password name = %s, cusername = %s\n", passwd->pw_name, cusername);
		*transactcc = TCML_NOACCAUTH;
		return (-1);
	}
#if	GPORT_HAS_SYSV_TIME
	tzset();			/* Get local timezone */
	time (&elapsed);		/* Fill in elapsed */
	tp = localtime (&elapsed);	/* Convert to struct tm */
	tzn = tzname [tp->tm_isdst];	/* Tm_isdst is 0 or 1 */
#else
	gettimeofday (&tv, &tz);	/* Elapsed secs in tv, zone in tz */
	{
	  time_t tv_sec = tv.tv_sec;
	  tp = localtime (&tv_sec);	/* Convert to struct tm */
	}
	tzn = timezone (tz.tz_minuteswest, tp->tm_isdst);
#endif
	if (tzn == (char *) 0) tzn = "";
	/*
	 *  Obtain the network password to access the remote NQS machine.
	 */
	netpassword [0] = '\0';		/* NQS password access is not yet */
					/* implemented */
	/*
	 * Clobber whatever SIGPIPE handler our caller has,
	 * and put in one that always returns errno == EPIPE.
	 */
	signal (SIGPIPE, brokenconnection);
	/*
	 *  Append the necessary trailer information to the network
	 *  transaction packet, that is common to ALL NQS network
	 *  transaction packets.
	 */
	interw32u ((unsigned long) NPK_MAGIC1);
	interw32i ((long) packettype);	/* Write packet transaction type */
	interwstr (netpassword);	/* Attach network password to packet */
	interw32u (hostmid);		/* Attach "from-mid" to packet */
	interw32u (targetmid);		/* Attach "to-mid" to packet */
	interw32i ((long) cuid);	/* Attach client's uid to packet */
	interwstr (cusername);		/* Attach client's username to packet */
#if	GPORT_HAS_SYSV_TIME
	/* interw32i ((long) timezone);*/ /* Write timezone difference from */
					/* GMT in seconds */
	interw32i ((long) 0); 		/* Write timezone difference from */
#else
	interw32i ((long) tz.tz_minuteswest * 60L);
					/* Write our timezone difference */
					/* from GMT in seconds */
#endif
	interwstr (tzn);		/* Write our timezone name */
	/*
	 *  Now that we have defined the network transaction packet
	 *  contents, format the packet.
	 */
	if ((packetsize = interfmt (packet)) == -1) {
		/*
		 *  The packet contents create a packet that is too
		 *  large.  The packet cannot be sent.
		 */
		*transactcc = TCML_INTERNERR;
		return (-1);
	}
	/*
	 *  Store the address of the host we are trying to reach
	 *  in the remote socket address structure.
	 */
#ifdef IS_UNICOS
        /* hmm... s_addr is a bitfield, so forgive the following...*/
        sal_bytecopy ((char *) &rsin.sin_addr, (char *) hostent->h_addr,
                hostent->h_length);
#else
	sal_bytecopy ((char *) &rsin.sin_addr.s_addr, (char *) hostent->h_addr,
		hostent->h_length);
#endif
	rsin.sin_family = hostent->h_addrtype;
	rsin.sin_port = servent->s_port;
	
	
	/*
	 *  Store the address of the local socket we will be using
	 *  in the local socket address structure.
	 */
	tryport = IPPORT_RESERVED -1;
	timeout = 1;
	lsin.sin_family = AF_INET;
	lsin.sin_addr.s_addr = 0;
	/*
	 *  Try to connect to the remote machine.
	 *  If it is likely that an immediate retry would succeed, go
	 *  ahead and do the retry right now, rather than returning -2.
	 */
	do {
		retry = 0;		/* Adopt an optimistic attitude */
		socket_descr = socket (AF_INET, SOCK_STREAM, 0);
		if (socket_descr == -1) {
			*transactcc = errnototcm ();
			if (errno == ENOBUFS) return (-2);
			else return (-1);
		}
		/*
		 *  Try to bind this socket to a reserved port.
		 *  Without a reserved port, the NQS netserver on a
		 *  remote machine would suspect that we were trying
		 *  to pull a fast one.
		 */
		lsin.sin_port = htons (tryport);
		while (bind (socket_descr, (struct sockaddr *) &lsin, sizeof (lsin)) < 0) {
			/*
			 *  The bind() failed.
			 *  Try another port.
			 */
			if (errno != EADDRINUSE && errno != EADDRNOTAVAIL) {
				close (socket_descr);
				*transactcc = errnototcm ();
				return (-1);
			}
			tryport--;
			if (tryport <= IPPORT_RESERVED/2) {
				close (socket_descr);
				*transactcc = TCML_NOPORTAVAI;
				return (-2);
			}
			lsin.sin_port = htons(tryport);
		}
		/*
		 *  Connect to the NQS network daemon on the remote machine.
		 */
		if (connect (socket_descr, (struct sockaddr *) &rsin,
			     sizeof (rsin)) == -1) {
			/*
			 *  We were unable to connect to the remote NQS
			 *  network daemon.
			 */
			switch (errno) {
			case EADDRINUSE:
				close (socket_descr);
				tryport--;
				retry = 1;	/* Retry */
				break;
			case ECONNREFUSED:
				if (timeout <= 16) {
					close (socket_descr);
					sleep (timeout);
					timeout *= 2;
					retry = 1;	/* Retry */
				}
				else {
					close (socket_descr);
					*transactcc = TCMP_NONETDAE;
					return (-1);
				}
				break;
			case ETIMEDOUT:
				close (socket_descr);
				*transactcc = TCML_ETIMEDOUT;
				return (-1);
			default:
				close (socket_descr);
				*transactcc = TCMP_NOESTABLSH;
				return (-1);
			}
		}
	} while (retry);
	if ((wSize = write (socket_descr, &packet[0], packetsize)) != packetsize) {
		close (socket_descr);
		*transactcc = TCMP_CONNBROKEN;
	  	sal_dprintf(SAL_DEBUG_INFO,SAL_DEBUG_MSG_WARNING,"Write on pipe socket returned %d; should have bee %d\n", wSize, packetsize);
	  	fflush(stderr);
		return (-1);
	}
	/*
	 * Before returning, check that the server liked us.
	 */
	setsockfd (socket_descr);
	switch (interread (getsockch)) {
	case 0:
		break;
	case -1:
		close (socket_descr);
	  	sal_dprintf(SAL_DEBUG_INFO,SAL_DEBUG_MSG_WARNING, "Unable to read reply from server\n");
	  	fflush(stderr);
		*transactcc = TCMP_CONNBROKEN;
		return (-1);
	case -2:
		close (socket_descr);
		*transactcc = TCMP_PROTOFAIL;
		return (-1);
	}
	integers = intern32i();			
	if (integers < 1) {
		close (socket_descr);
		*transactcc = TCMP_PROTOFAIL;
		return (-1);
	}
	*transactcc = interr32i (1);
	switch (*transactcc & XCI_FULREA_MASK) {
	/*
	 * Note that code outside the switch passes the TCM to our caller.
	 */
	    case TCMP_CONTINUE:
		return (socket_descr);
	    case TCMP_ERRORRETRY:
		/*
		 * We might have remote servers send strings
		 * as well as TCM's.
		 */
		strings = internstr();
		if (strings < 1) {
		}
		close (socket_descr);
		return (-1);
	    case TCMP_FATALABORT:
		strings = internstr();
		if (strings < 1) {
		}
		close (socket_descr);
		return (-1);
	    case TCMP_CLIMIDUNKN:
	    case TCMP_MAXNETCONN:
	    case TCMP_MIDCONFLCT:
	    case TCMP_NETPASSWD:
	    case TCMP_NOACCAUTH:
	    case TCMP_NOMOREPROC:
	    case TCMP_NONSECPORT:
	    case TCMP_PROTOFAIL:
	    case TCMP_UNAFAILURE:
		close (socket_descr);
		return (-1);
	    default:
		close (socket_descr);
		return (-1);
	}
}


/*** brokenconnection
 *
 *	void brokenconnection():
 *	Allow writes on broken connections to return -1 
 *	instead of exiting.
 */
static void brokenconnection ( int unused )
{
	signal (SIGPIPE, brokenconnection);
	errno = EPIPE;
}
