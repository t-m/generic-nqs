/*
 * libnqs/comms/rcmmsgs.c
 * 
 * DESCRIPTION:
 *
 *	Diagnose a request completion code WITHOUT
 *	diagnosing any RCI_ information bits that might
 *	be present in the request completion code.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 28, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdio.h>		/* FILE definition */
#include <libnqs/informcc.h>	/* NQS information completion codes */
#include <libnqs/requestcc.h>	/* NQS request completion codes */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*** rcmmsgs
 *
 *	void rcmmsgs():
 *
 *	Diagnose a request completion code WITHOUT
 *	diagnosing any RCI_ information bits that might
 *	be present in the request completion code.
 */
void rcmmsgs (long code, sal_debug_msg_t gMessType, char *prefix)
{
	static char
	shutdown[] ="Request aborted for NQS shutdown.\n";
	static char
	deleted[]  ="Request deleted.\n";
	static char
	failed[]   ="Request files placed in NQS failed directory.\n";
	static char
	norun[]    ="Request not executed.\n";
	static char
	reque[]    ="Request requeued.\n";
	static char
	retryexc[] ="Retry limit exceeded.\n";
	static char
	shlbrkpnt[]="Unsupported shell breakpoint encountered.\n";
	static char
	shlexefai[]="Request shell execve() failed.\n";

	static char *messages[] = {
		"Too many environment variables to run batch request.\n",
		norun,
		deleted,
		(char *) 0,		/* RCM_2MANYENVARS */
		"Too many server arguments on server execve().\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_2MANYSVARGS */
		"Request aborted via a signal.\n",
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_ABORTED */
		"Corrupted request control and/or data files.\n",
		norun,
		failed,
		(char *) 0,		/* RCM_BADCDTFIL */
		"Bad argument passed to request server.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_BADSRVARG */
		"Request has been successfully to delivered to destination.\n",
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_DELIVERED */
		"Request delivery time expired.\n",
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_DELIVEREXP */
		"Request could not be delivered.\n",
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_DELIVERFAI */
		"Request was not delivered.\n",
		retryexc,
		deleted,
		(char *) 0,		/* RCM_DELIVERRETX */
		"Device open failed.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_DEVOPEFAI */
		"Unable to successfully start request because of a\n",
		"file descriptor shortage.\n",
		reque,
		(char *) 0,		/* RCM_ENFILERUN */
		"Unable to successfully start request because of a\n",
		"file system resource shortage.\n",
		reque,
		(char *) 0,		/* RCM_ENOSPCRUN */
		"Request executing.\n",
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_EXECUTING */
		"Request exited normally.\n",
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_EXITED */
		"Insufficient memory to start request.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_INSUFFMEM */
		"Server transaction processing aborted for NQS shutdown.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_INTERRUPTED */
		"Machine-id of request owner is no longer recognized\n",
		"by the execution machine.\n",
		norun,
		deleted,		/* RCM_MIDUNKNOWN */
		"Request could not be successfully delivered.  Previously\n",
		"routed request expired or was deleted at destination.\n",
		deleted,
		(char *) 0,		/* RCM_NETREQDEL */
		"No account authorization on execution machine for mapped\n",
		"request owner user-id.\n",
		norun,
		deleted,		/* RCM_NOACCAUTH */
		"Insufficient processes available to spawn request.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_NOMOREPROC */
		"Server bind() error.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_NONSECPORT */
		"Interrupted request prohibits restart on NQS rebuild.\n",
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_NORESTART */
		"Server for request did not return a completion code.\n",
		"Request failed.\n",
		failed,
		(char *) 0,		/* RCM_NOSVRETCODE */
		"Resolved stdout or stderr pathname of batch request\n",
		"at destination exceeds the maximum supported length.\n",
		deleted,
		(char *) 0,		/* RCM_PATHLEN */
		deleted,
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_PIPREQDEL */
		"NQS rebuild failure.  Request could not be requeued.\n",
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_REBUILDFAI */
		"Request collided with another previously existing\n",
		"request with the same request-id.  The newer request\n",
		"has been deleted.  Seek staff support.\n",
		(char *) 0,		/* RCM_REQCOLLIDE */
		"Request transaction failed.  Retry scheduled.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_RETRYLATER */
		"Request successfully routed for delivery to destination.\n",
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_ROUTED */
		"Request sent to local queue destination.\n",
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_ROUTEDLOC */
		"Request routing time expired.\n",
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_ROUTEEXP */
		"Request could not be routed.\n",
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_ROUTEFAI */
		"Request was not routed.\n",
		retryexc,
		deleted,
		(char *) 0,		/* RCM_ROUTERETX */
		"Unsupported server breakpoint encountered.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_SERBRKPNT */
		"Request server execve() failed.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_SEREXEFAI */
		"Server killed by unanticipated signal.\n",
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_SERVESIGERR */
		"Too many environment variables to run request.\n",
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_SHEXEF2BIG */
		shutdown,
		"The request was defined as unrestartable, and so the\n",
		"request has been deleted.\n",
		(char *) 0,		/* RCM_SHUTDNABORT */
		shutdown,
		"The request has been requeued for later restart.\n",
		(char *) 0,
		(char *) 0,		/* RCM_SHUTDNREQUE */
		shlbrkpnt,
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_SSHBRKPNT */
		shlexefai,
		reque,
		(char *) 0,
		(char *) 0,		/* RCM_SSHEXEFAI */
		"Output file successfully returned to destination.\n",
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_STAGEOUT */
		"Output file could not be returned to primary destination.\n",
		"Output file successfully returned to backup destination\n",
		"in user home directory on the execution machine.\n",
		(char *) 0,		/* RCM_STAGEOUTBAK */
		"Output file could not be returned to primary or backup\n",
		"destination.\n",
		(char *) 0,
		(char *) 0,		/* RCM_STAGEOUTFAI */
		"Unable to execute request.\n",
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_UNABLETOEXE */
		"Request failed.\n",
		failed,
		(char *) 0,
		(char *) 0,		/* RCM_UNAFAILURE */
		"Unable to create directly accessed (-re) stderr file.\n",
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_UNCRESTDERR */
		"Unable to create directly accessed (-ro) stdout file.\n",
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_UNCRESTDOUT */
		"Undefined RCM_ completion code.\n",
		(char *) 0,
		(char *) 0,
		(char *) 0,		/* RCM_UNDEFINED */
		shlbrkpnt,
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_USHBRKPNT */
		shlexefai,
		deleted,
		(char *) 0,
		(char *) 0,		/* RCM_USHEXEFAI */
		"Output file could not be returned to primary destination.\n",
		"Output file successfully returned to last chance destination\n",
	        /* ANSI C concatenates the following line into one string */
		"in dump/<username> on the execution machine's spool space.\n",
		(char *) 0		/* RCM_STAGEOUTLCH */
	};

	register short reason;		/* Reason bits of completion code */

  	assert (prefix != NULL);
  
	reason = (code & XCI_REASON_MASK);
	if (reason < 0 || reason > (RCM_MAXRCM & XCI_REASON_MASK) ||
	   (code & XCI_TRANSA_MASK)) {
		/*
		 *  The request completion code is invalid.
		 */
		reason = (RCM_UNDEFINED & XCI_REASON_MASK);
	}
	reason *= 4;
	sal_dprintf(SAL_DEBUG_INFO, gMessType, "%s%s", prefix, messages[reason++]);
	if (messages [reason] != NULL) {
	  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%s%s", prefix, messages[reason++]);
	}
	if (messages [reason] != NULL) {
	  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%s%s", prefix, messages[reason++]);
	}
	if (messages [reason] != NULL) {
	  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%s%s", prefix, messages[reason++]);
	}
}
