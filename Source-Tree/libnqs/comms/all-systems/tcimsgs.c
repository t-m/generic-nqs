/*
 * libnqs/comms/tcimsgs.c
 * 
 * DESCRIPTION:
 *
 *	Diagnose transaction code information bits.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 28, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdio.h>			/* FILE definition */
#include <libnqs/informcc.h>		/* NQS information completion codes */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

struct nqs_tcimsg_t
{
  unsigned long ulMessType;
  const char * szMessText;
};

static struct nqs_tcimsg_t s_stMessage [] =
{
  { TCI_COPLIMEXC,	"  Copy limit\n" },
  { TCI_PP_CFLEXC,	"  Per-process core-file size limit\n" },
  { TCI_PP_CTLEXC,	"  Per-process CPU time limit\n" },
  { TCI_PP_DSLEXC,	"  Per-process data-segment limit\n" },
  { TCI_PP_MSLEXC,	"  Per-process memory size limit\n" },
  { TCI_PP_NELEXC,	"  Per-process nice execution priority limit\n" },
  { TCI_PP_PFLEXC,	"  Per-process permanent file size limit\n" },
  { TCI_PP_QFLEXC,	"  Per-process quick file size quota limit\n" },
  { TCI_PP_SSLEXC,	"  Per-process stack segment limit\n" },
  { TCI_PP_TFLEXC,	"  Per-process temporary file size limit\n" },
  { TCI_PP_WSLEXC,	"  Per-process working set quota limit\n" },
  { TCI_PRILIMEXC,	"  Print/output size limit\n" },
  { TCI_PR_CTLEXC,	"  Per-request CPU time limit\n" },
  { TCI_PR_DRIEXC,	"  Per-request tape drive quota limit\n" },
  { TCI_PR_MSLEXC,	"  Per-request memory space limit\n" },
  { TCI_PR_NCPEXC,	"  Per-request CPU quota limit\n" },
  { TCI_PR_PFLEXC,	"  Per-request permanent file space limit\n" },
  { TCI_PR_QFLEXC,	"  Per-request quck file space quota limit\n" },
  { TCI_PR_TFLEXC,	"  Per-request temporary file space limit\n" },
  { 0,			NULL }
};

/*** tcimsgs
 *
 *
 *	void tcimsgs():
 *	Diagnose transaction code information bits.
 */
void tcimsgs (
	long code,					/* Completion code */
	sal_debug_msg_t gMessType,			/* Output message type */
	char *prefix,					/* Output prefix */
	void (*headerfn)(sal_debug_msg_t, char *))		/* Header display function */
{
	register short preamble;	/* Preamble seen flag */
	struct nqs_tcimsg_t *pstWalker;
  
	preamble = 0;			/* No preamble yet */
	pstWalker = &s_stMessage[0];
  	
  	while (pstWalker->szMessText != NULL)
  	{
	  if (code & pstWalker->ulMessType)
	  {
	    if (0 == preamble)
	    {
	      preamble++;
	      (*headerfn)(gMessType, prefix);
	    }
	    sal_dprintf(SAL_DEBUG_INFO, gMessType, "%s%s", prefix, pstWalker->szMessText);
	  }
	  pstWalker++;
	}
}
