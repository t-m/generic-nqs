/*
 * libnqs/comms/rcimsgs.c
 * 
 * DESCRIPTION:
 *
 *	Diagnose request code information bits.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 28, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdio.h>		/* FILE definition */
#include <libnqs/informcc.h>	/* NQS information completion codes */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

struct nqs_rcimsg_t
{
  unsigned long ulMessType;
  const char * szMessText;
};

static struct nqs_rcimsg_t s_stMessage [] =
{
  { RCI_ACCESSDEN,	"  Access denied\n" },
  { RCI_CLIMIDUNKN,	"  Client machine-id is unknown at transaction peer\n" },
  { RCI_EFBIG,		"  File size limit exceeded\n" },
  { RCI_FATALABORT,	"  Non-recoverable transaction failure\n" },
  { RCI_MIDCONFLICT,	"  Machine-id conflict between client and destination\n" },
  { RCI_NETNOTSUPP,	"  Networking not supported at NQS site\n" },
  { RCI_NETPASSWD,	"  Network password verification error\n" },
  { RCI_NOSUCHFORM,	"  No such device forms\n" },
  { RCI_NOSUCHQUE,	"  No such queue\n" },
  { RCI_PEERINTERR,	"  NQS internal error at transaction peer\n" },
  { RCI_PEERMIDUNKN,	"  Local machine-id is unknown at transaction peer\n" },
  { RCI_PEERNETDB,	"  Network database error at transaction peer\n" },
  { RCI_PEERNOACATH,	"  No account authorization at transaction peer\n" },
  { RCI_PROTOFAIL,	"  NQS protocol failure\n" },
  { RCI_QUOTALIMIT,	"  Explicit request quota limits exceed maximums\n" },
  { RCI_RRFUNKNMID,	"  Request refers to mids unknown at transaction peer\n" },
  { RCI_WROQUETYP,	"  Wrong queue type for request\n" },
  { RCI_UNAFAILURE,	"  Unanticipated transaction failure\n" },
  { 0,			NULL }
};

/*** rcimsgs
 *
 *
 *	void rcimsgs():
 *	Diagnose request code information bits.
 */
void rcimsgs (
	long code,				/* Completion code */
	sal_debug_msg_t gMessType,		/* Output message type */
	char *prefix,				/* Output prefix */
	void (*headerfn)(sal_debug_msg_t, char *))	/* Header display function */
{
	register short preamble;		/* Preamble seen flag */
  	struct nqs_rcimsg_t *pstWalker;

  	assert (prefix != NULL);
  	assert (headerfn != NULL);

  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "rcm code is %ld\n", code);

	preamble = 0;			/* No preamble yet */
  
  	pstWalker = &s_stMessage[0];
  	while (pstWalker->szMessText != NULL)
  	{
    	  if (code & pstWalker->ulMessType)
	  {
	    if (0 == preamble)
	    {
	      preamble++;
	      (*headerfn)(gMessType, prefix);
	    }
	    sal_dprintf(SAL_DEBUG_INFO, gMessType, "%s%s", prefix, pstWalker->szMessText);
	  }
	  pstWalker++;
	}
}
