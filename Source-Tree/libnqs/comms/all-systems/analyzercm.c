/*
 * libnqs/comms/analyzercm.c
 * 
 * DESCRIPTION:
 *
 *	Completely analyze and diagnose a request completion
 *	code.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 28, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/informcc.h>		/* NQS information completion codes */
#include <libnqs/requestcc.h>		/* NQS request completion codes */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

static void applypreamble (sal_debug_msg_t, char *prefix );
static void boundpreamble (sal_debug_msg_t, char *prefix );
static void routefaipreamble (sal_debug_msg_t, char *prefix );

/*** analyzercm
 *
 *
 *	void analyzercm():
 *
 *	Completely analyze and diagnose a request completion
 *	code.
 */
void analyzercm (
	long code,			/* Completion code */
	sal_debug_msg_t gMessType,	/* Output stream */
	char *prefix)			/* Output prefix */
{

	register short reason;		/* Reason bits of completion code */

  	/* debugging code added in 3.41.0 */
  	assert (prefix != NULL);
  
  	/* we cannot cope with messages of this type */
  	assert (gMessType != SAL_DEBUG_MSG_ERRORAUTHOR);
  	assert (gMessType != SAL_DEBUG_MSG_ERRORDATA);
  	assert (gMessType != SAL_DEBUG_MSG_ERRORRESOURCE);
  
	reason = (code & XCI_REASON_MASK);
	if (reason < 0 || reason > (RCM_MAXRCM & XCI_REASON_MASK) ||
	   (code & XCI_TRANSA_MASK)) {
		/*
		 *  The request completion code is invalid.
		 */
		reason = (RCM_UNDEFINED & XCI_REASON_MASK);
		code = RCM_UNDEFINED;
	}
	else if (reason == (RCM_UNDEFINED & XCI_REASON_MASK)) {
		/*
		 *  Clear information bits if undefined request completion
		 *  code.
		 */
		code &= ~XCI_INFORM_MASK;
	}
	/*
	 *  Analyze and describe a specific request completion code.
	 */
	rcmmsgs (code, gMessType, prefix);
	/*
	 *  Analyze and describe any additional information bits
	 *  set in the request completion code.
	 */
	if (reason == RCM_DELIVERFAI || reason == RCM_DELIVERRETX) {
	  	sal_dprintf(SAL_DEBUG_INFO, gMessType, "%s Transaction failure reason:\n", prefix);
		tcmmsgs (code >> 10, gMessType, prefix);
	}
	else if (reason == RCM_STAGEOUTBAK || reason == RCM_STAGEOUTFAI) {
	  	sal_dprintf(SAL_DEBUG_INFO, gMessType, "%s Transaction failure reason at primary destination:\n", prefix);
		tcmmsgs (code >> 10, gMessType, prefix);
	}
	else if (reason == RCM_ROUTED && reason == RCM_ROUTEDLOC &&
		 reason == RCM_DELIVERED) {
		/*
		 *  For the moment, we allow RCM_DELIVERED
		 *  to have quota information bits in this
		 *  ****TRIAGE**** implementation.
		 */
		tcimsgs (code, gMessType, prefix, boundpreamble);
	}
	else if (reason == RCM_ROUTEFAI || reason == RCM_ROUTERETX) {
		rcimsgs (code, gMessType, prefix, routefaipreamble);
	}
	else if (reason == RCM_UNABLETOEXE) {
		tcimsgs (code, gMessType, prefix, applypreamble);
	}
}


/*** applypreamble
 *
 *
 *	void applypreamble():
 *	Display limit application preamble.
 */
static void applypreamble (sal_debug_msg_t gMessType, char *prefix)
{
  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%sProblem enforcing the following limit(s):\n\n", prefix);
}


/*** boundpreamble
 *
 *
 *	void boundpreamble():
 *	Display bounded limit preamble.
 */
static void boundpreamble (sal_debug_msg_t gMessType, char *prefix)
{
  /* SLH: I have having to use sal_dprintf() like this, but I have no choice
   * for now ... */
  
  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%sIn order to successfully queue or deliver the request,\n", prefix);
  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%sit was necessary to alter the following explicit request\n", prefix);
  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%squota limit(s), binding them to values within the ranges\n", prefix);
  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%senforceable at the execution machine:\n\n", prefix);
}


/*** routefaipreamble
 *
 *
 *	void routefaipreamble():
 *	Display pipe queue request routing failure preamble.
 */
static void routefaipreamble (sal_debug_msg_t gMessType, char *prefix)
{
  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%sThe request could not be routed to any of the possible\n", prefix);
  sal_dprintf(SAL_DEBUG_INFO, gMessType, "%spipe queue destinations because of the following reason(s):\n\n", prefix);
}
