/*
 * libnqs/req/fopendata.c
 * 
 * DESCRIPTION:
 *
 *	Open an NQS req data file as called from a server returning
 *	the opened file stream.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdio.h>
#include <sys/types.h>
#include <libnmap/nmap.h>			/* Get definition of mid_t */

/*** fopendata
 *
 *
 *	FILE *fopendata():
 *
 *	Open an NQS req data file as called from a server returning
 *	the opened file stream.
 *
 *	Note that the current directory of the caller must be the
 *	NQS root directory (which is the directory at the time that
 *	the server is exec'd from the NQS daemon).
 *
 *	Returns:
 *		The opened file stream if successful, otherwise
 *		NULL is returned.
 */
FILE *fopendata (
	long orig_seqno,	/* Sequence number for req */
	Mid_t orig_mid,		/* Machine-id for req */
	int datano)		/* Data file number */
{
	int fd;				/* Opened file descr */

	if ((fd = opendata (orig_seqno, orig_mid, datano)) != -1) {
		return (fdopen (fd, "r"));
	}
	return (NULL);
}
