/*
 * libnqs/req/verifyhdr.c
 * 
 * DESCRIPTION:
 *
 *	Verify the common portion of a rawreq structure.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	March 28, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

/*** verifyhdr
 *
 *
 *	int verifyhdr():
 *	Verify the common portion of a rawreq structure.
 *
 *	Returns:
 *		 0: if common header is valid.
 *		-1: otherwise.
 */
int verifyhdr (struct rawreq *rawreq)
{
  	assert(rawreq != NULL);
	/*
	 * "rawreq->ndatafiles > 32767" will be an invariant comparison
	 * when compiled on a machine with 16 bit shorts.
	 */
	if (rawreq->magic1 != REQ_MAGIC1 ||
	    rawreq->trans_id < 0 || rawreq->trans_id > MAX_TRANSACTS ||
	    rawreq->trans_quename [MAX_QUEUENAME] != '\0' ||
	    rawreq->quename [0] == '\0' ||
	    rawreq->quename [MAX_QUEUENAME] != '\0' ||
	    (rawreq->type != RTYPE_BATCH && rawreq->type != RTYPE_DEVICE) ||
	    rawreq->orig_seqno < -1 || rawreq->orig_seqno > MAX_SEQNO_USER ||
	    rawreq->rpriority < -1 || rawreq->rpriority > MAX_POSIX_RPRIORITY ||
	    rawreq->ndatafiles < 0 || rawreq->ndatafiles > 32766 ||
	    rawreq->reqname [0] == '\0' ||
	    rawreq->reqname [MAX_REQNAME] != '\0' ||
	    rawreq->username [0] == '\0' ||
	    rawreq->username [MAX_ACCOUNTNAME] != '\0' ||
	    rawreq->mail_name [0] == '\0' ||
	    rawreq->mail_name [MAX_ACCOUNTNAME] != '\0') {
		/*
		 *  Values out of bounds.
		 */
		return (-1);
	}
	/*
	 *  No errors detected in the common portion of the rawreq.
	 */
	return (0);
}
