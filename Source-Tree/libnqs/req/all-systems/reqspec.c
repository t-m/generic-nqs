/*
 * libnqs/req/reqspec.c
 * 
 * DESCRIPTION:
 *
 *	Return the sequence number, and explicit or implied machine
 *	specification as given in the request-id text parameter.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 11, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnmap/nmap.h>
#include <string.h>
#include <netdb.h>			/* Network database header file; */

static char *reqspec_machine ( char *text, Mid_t *machine_id, int *status );

/*** reqspec
 *
 *
 *	int reqspec():
 *
 *	Return the sequence number, and explicit or implied machine
 *	specifications as given in the request-id text parameter.
 *
 *	A request-id is always of the form:
 *
 *		<seqno>		OR
 *		<seqno>.<machine-specification> OR
 *		<seqno>.<machine-specification>@<remote-machine-specification>
 *
 *	If no <machine-specification> is given, then the request is
 *	assumed to have been submitted from the local machine.
 *
 *	If no <remote-machine.specification> is given, then the request
 *	is assumed to exist on the local machine.
 *
 *	The form of an explicit machine specification (when present
 *	in a request-id specification), is:
 *
 *		.machine-name	OR
 *		.[integer#]
 *
 *	where the "machine-name" form represents the principal name
 *	or alias for the machine from whence the request was submitted,
 *	and the "[integer#]" form is supported so that explicit
 *	machine-ids can be specified in place of a name or alias.
 *
 *	Returns:
 *		0: if successful, in which case the orig_seqno,
 *		   and machine-id parameters are properly updated.
 *
 *	       -1: if an invalid request-id syntax is encountered.
 *
 *	       -2: if the explicit or implied machine-specification
 *		   is not recognized by the local system (NMAP_ENOMAP).
 *
 *	       -3: if the Network Mapping Procedures (NMAP_)
 *		   deny access to the caller (NMAP_ENOPRIV).
 *
 *	       -4: if some other general NMAP_ error occurs.
 */
int reqspec (
	char *text,		/* Literal text */
	long *orig_seqno,	/* Req sequence number */
	Mid_t *machine_id,	/* Pointer of where to return */
				/* the proper machine-id */
	Mid_t *target_mid)	/* Pointer of where to return */
				/* the target machine-id */
{
	register long seqno;		/* Sequence# of request */
	int	status;			/* status integer */

  	assert (text       != NULL);
  	assert (orig_seqno != NULL);
  	assert (machine_id != NULL);
  	assert (target_mid != NULL);
  
	seqno = 0;
	while (*text >= '0' && *text <= '9') {
		seqno *= 10;
		seqno += *text - '0';
		text++;			/* Advance to the next character. */
	}
	if (*text == '\0') {
		/*
		 *  No explicit machine-specification is present.
		 *  We assume the local machine in this case.
		 */
		*orig_seqno = seqno;
		if ( (status = localmid (machine_id)) ) return ( status );
		return (localmid (target_mid));
	}
	else if ((*text != '.') && (*text != '@') ) {
		/*
		 *  Invalid request-id syntax.
		 */
		return (-1);		/* Report bad request-id syntax */
	}
	if (*text == '.') {
	    text++;				/* Scan past '.' */
	    /*
 	     *  A machine-specification is present.
	     */
	    text = reqspec_machine(text, machine_id, &status);
	    if (status) {
	      return (status);
            }
        } else {
            /* 
	     * No machine spec before the target spec should use local mach 
             */
            if ( (status = localmid (machine_id)) ) return ( status );
        }
	if (*text == '\0') {		/* if we have no target_mid ... */
		*orig_seqno = seqno;
		return (localmid (target_mid));
	}
	if ( *text != '@') {
		/*
		 *  Invalid request-id syntax.
		 */
		return (-1);
	}
	text++;
	text = reqspec_machine(text, target_mid, &status);
	if (status) return (status);
	*orig_seqno = seqno;	/* Return request sequence# */
	return (0);
}
	
/*
 *  This routine takes a pointer to a machine specification string and
 *  returns the machine_id and the status.  If successful, status is
 *  0 and it returns a pointer the character after the specification string.
 *  On failure, status is negative and it returns 0.
 */
static char *reqspec_machine(
	char	*text,			/* buffer holding machine spec */
	Mid_t	*machine_id,		/* return the machine id here */
	int	*status)		/* return the status here */
{
	register Mid_t mid_spec;	/* Machine-id specification */
	struct hostent *ent;		/* Host table entry structure */
	char   buffer[64];		/* temporary buffer */
	char   *cp;

  	assert (text       != NULL);
  	assert (machine_id != NULL);
  	assert (status     != NULL);
  
	if (*text == '\0') {
		/*
		 *  Missing machine-specification.
		 */
		*status = -1;
		return (0);		/* Bad request-id syntax */
	}
	/*
	 *  Determine the machine-id of the machine specification
	 *  as present in the request-id text.
	 */
	if (*text == '[') {
		/*
		 *  Could be an explicit machine specification by machine-id.
		 */
		text++;
		mid_spec = 0;
		while (*text >= '0' && *text <= '9') {
			mid_spec *= 10;
			mid_spec += *text - '0';
			text++;
		}
		if (*text == ']') {
			/*
			 *	[ <digit-sequence> ]
			 */
			if (*++text) {		/* Invalid machine-id spec */
				*status = -1;
				return (0);	/* Bad syntax */
			}
			*machine_id = mid_spec;
			*status = 0;
			return (text);		/* Success */
		}
		*status = -1;
		return (0);			/* Invalid syntax */
	}
	/*
	 *  The machine-specification portion of the request-id has been
	 *  specified as a name.
	 *
	 *  First remove any extra stuff at the end, if present.
	 */
	strcpy( buffer, text);
	if (  (cp = strchr(buffer, '@'))  ) {
		*cp = '\0';
		text = strchr(text, '@');
	} else {
	        text = strchr(text, '\0');
	}
	if ((ent = gethostbyname (buffer)) == (struct hostent *) 0) {
		*status = -2;
		return (0);			/* This machine is not */
						/* known to us */
	}
	switch (nmap_get_mid (ent, machine_id)) {
	case NMAP_SUCCESS:		/* Successfully got local machine-id */
		*status = 0;
		return (text);
	case NMAP_ENOMAP:		/* What?  No local mid defined! */
		*status = -2;
		return (0);
	case NMAP_ENOPRIV:		/* No privilege */
		*status = -3;
		return (0);
	}
	*status = -4;
	return (0);			/* General NMAP_ error */
}
