/*
 * libnqs/req/quereqvlpq.c
 * 
 * DESCRIPTION:
 *
 *	Send a "queue request in a local queue via a local
 *	pipe queue" packet to the NQS daemon.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	December 18, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnmap/nmap.h>
#include <libnqs/nqspacket.h>			/* NQS local message packets */

/*** quereqvlpq
 *
 *
 *	long quereqvlpq():
 *
 *	Send a "queue request in a local queue via a local
 *	pipe queue" packet to the NQS daemon.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if the specified request was
 *				 successfully queued in the local
 *				 destination queue.
 *		TCML_INTERNERR:	 if maximum packet size exceeded.
 *		TCML_NOESTABLSH: if unable to establish inter-
 *				 process communication.
 *		TCML_NOLOCALDAE: if the NQS daemon is not running.
 *		TCML_NOSUCHQUE:	 if the specified destination
 *				 queue does not exist.
 *		TCML_NOSUCHREQ:	 if the specified request does not
 *				 exist.
 *		TCML_PROTOFAIL:	 if a protocol failure occurred.
 */
long quereqvlpq (
	long orig_seqno,	/* Original sequence# */
	Mid_t orig_mid,		/* Original machine-id of req */
	char *destqueue)	/* Name of local queue where */
{				/* request should be queued */
  
  	assert (destqueue != NULL);
  
	interclear();
	interw32i ((long) orig_seqno);
	interw32u (orig_mid);
	interwstr (destqueue);
	return (inter (PKT_QUEREQVLPQ));
}
