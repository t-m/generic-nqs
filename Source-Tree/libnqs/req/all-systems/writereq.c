/*
 * libnqs/req/writereq.c
 * 
 * DESCRIPTION:
 *
 *	[Re]write a control file request header.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <errno.h>
#include <unistd.h>

/*** writereq
 *
 *
 *	int writereq():
 *
 *	[Re]write the request header portion of the control file
 *	for an NQS request.
 *
 *	Unless errno is returned non-zero, the lseek() position
 *	of the control file descriptor upon return, is ALWAYS at
 * 	the first byte of the varying portion of the control file.
 *
 *
 *	Returns:
 *		0: if successful;
 *	       -1: otherwise.
 *
 *			Note:	errno will be returned 0 by this function
 *				unless a system call error has occurred.
 */
int writereq (int fd, struct rawreq *rawreq)
{
	register int size;		/* Header size */

	errno = 0;			/* Clear errno */
	if (lseek (fd, 0L, 0) == -1) {	/* Seek to start of control file */
		return (-1);		/* Return EBADF */
	}
	/*
	 *  [Re]write the header portion of an NQS control file, in one
	 *  single write() system call.
	 *
	 *  We do this for reasons of efficiency, and also so that the first
	 *  block of the control file is [re]written in a single atomic
	 *  I/O operation so that Qmod updates, and multiple readers of
	 *  the control file (who are also carefully reading the first
	 *  block of the control in an indivisible fashion), will not
	 *  step on each other.
	 */
	size = ((char *) &rawreq->v) - ((char *) rawreq);
	if (rawreq->type == RTYPE_DEVICE) {
		/*
		 *  We are [re]writing the control file header of a
		 *  device-oriented request.
		 */
		size += sizeof (struct rawdevreq);
	}
	else {
		/*
		 *  We are [re]writing the control file header of a
		 *  non device-oriented request.
		 */
		size += sizeof (struct rawbatreq);
	}
	if (write (fd, (char *) rawreq, size) != size) {
		if (errno == 0) {
			/*
			 *  We presume that the receiving device has been
			 *  completely filled-up, and artifically set errno
			 *  to ENOSPC, for the benefit of the caller.
			 */
			errno = ENOSPC;	/* Insufficient space */
		}
		return (-1);		/* Write() failed */
	}
	return (0);
}
