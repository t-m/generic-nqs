/*
 * libnqs/req/nextseqno.c
 * 
 * DESCRIPTION:
 *
 *	Return the next available NQS request sequence number.
 *	The current directory of the caller MUST be the NQS
 *	root directory.
 *
 *	For the reasons behind the peculiar storage of the
 *	NQS request sequence number, see ../src/nqs_ldconf.c.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	March 20, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxdirs.h>			/* Get Nqs_seqno definition */

/*** nextseqno
 *
 *	long nextseqno():
 *
 *	Return the next available NQS request sequence number.
 *	The current directory of the caller MUST be the NQS
 *	root directory.
 */
long nextseqno (void)
{
	struct stat stat_buf;		/* Stat() buffer */

	if (stat (Nqs_seqno, &stat_buf) == -1) return (0);
	if ((stat_buf.st_mode & 0777) == 0) return (0);
	return ((long) stat_buf.st_mtime);
}
