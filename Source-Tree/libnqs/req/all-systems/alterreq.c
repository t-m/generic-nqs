/*
 * libnqs/req/alterreq.c
 * 
 * DESCRIPTION:
 *
 *	Alter a non-running NQS request.
 *
 *	Original Author:
 *	-------
 *	John Roman,  Monsanto Company
 *	October 21, 1993.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqspacket.h>
#include <libnmap/nmap.h>

/*** alterreq
 *
 *
 *	long alterreq():
 *
 *	Send to the NQS daemon a packet which will alter a
 *	request.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHREQ:	 if request does not exist.
 *		TCML_REQRUNNING: if the request is running.
 */
long alterreq (
	uid_t whomuid,		/* Calling user, else zero for */
				/* NQS operator/manager call  */
	int req_seqno,		/* Request sequence number */
	Mid_t req_mid,		/* Request machine id */
	int priority)		/* Priority to change request to */
{
	interclear();
	interw32i ((long) whomuid);
	interw32i ((long) req_seqno);
	interw32u ((Mid_t) req_mid);
	interw32i ((long) priority);
	return (inter (PKT_ALTERREQ));
}
