/*
 * libnqs/req/movreq.c
 * 
 * DESCRIPTION:
 *
 *	Send a move request packet to the NQS daemon.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	December 2, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqspacket.h>	/* NQS local message packets */

/*** movreq
 *
 *
 *	long movreq():
 *	Send a move request packet to the NQS daemon.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if the specified request was
 *				 successfully placed in the specified
 *				 queue.
 *		TCML_INTERNERR:	 if maximum packet size exceeded.
 *		TCML_NOESTABLSH: if unable to establish inter-
 *				 process communication.
 *		TCML_NOLOCALDAE: if the NQS daemon is not running.
 *		TCML_NOSUCHQUE:	 if the specified from or destination
 *				 queue does not exist.
 *		TCML_NOSUCHREQ:	 if the specified request does not
 *				 exist.
 *		TCML_NOTREQOWN:	 if the specified request is not
 *				 owned by the client.
 *		TCML_PROTOFAIL:	 if a protocol failure occurred.
 */
long movreq (
	long orig_seqno,		/* Original sequence# */
	Mid_t orig_mid,			/* Original machine-id of req */
	char *dest_queue_name)		/* Name of destination queue for */
{					/* the request */
  	assert (dest_queue_name != NULL);
  
	interclear();
	interw32i ((long) orig_seqno);
	interw32u (orig_mid);
	interwstr (dest_queue_name);
	return (inter (PKT_MOVREQ));
}
