/*
 * libnqs/req/opendata.c
 * 
 * DESCRIPTION:
 *
 *	Open an NQS request data file as called from a server returning
 *	the opened file descriptor number.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxdirs.h>		/* NQS directory definitions */

/*** opendata
 *
 *
 *	int opendata():
 *
 *	Open an NQS req data file as called from a server returning
 *	the opened file descriptor number.
 *
 *	Note that the current directory of the caller must be the
 *	NQS root directory (which is the directory at the time that
 *	the server is exec'd from the NQS daemon).
 *
 *	Returns:
 *	      >=0: as the file descriptor of the opened file if
 *		   successful;
 *	       -1: if an error occurs in which case errno is set.
 */
int opendata (long orig_seqno, Mid_t orig_mid, int datano)
{
	char path [MAX_PATHNAME+1];	/* Pathname for data file */

	pack6name (path, Nqs_data, (int) (orig_seqno % MAX_OUTPSUBDIRS),
		  (char *) 0, (long) orig_seqno, 5, (long) orig_mid, 6,
		   datano-1, 3);
	/*printf("D$Attempting to open file %s\n", path);*/
	/*fflush(stdout);*/
	return (open (path, O_RDONLY));
}
