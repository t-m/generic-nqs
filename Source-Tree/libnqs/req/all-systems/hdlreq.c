/*
 * libnqs/req/hdlreq.c
 * 
 * DESCRIPTION:
 *
 *	Handle (hold or release) a NQS request.
 *
 *	Original Author:
 *	-------
 *	Christian Boissat, CERN, Geneva, Switzerland.
 *	January 6, 1992.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqspacket.h>			/* NQS local message packets */
#include <libnmap/nmap.h>			/* Mid_t (all OS's) */
					/* Uid_t and gid_t (if not BSD43) */

/*** hdlreq
 *
 *
 *	long hdlreq():
 *
 *	Send to the NQS daemon a packet which will hold or release a
 *	request.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHREQ:	 if request does not exist.
 *		TCML_REQRUNNING: if the request is running, and has
 *				 not been signalled.
 */
long hdlreq (
	int oper,		/* Type of operation (hold or release) */
	uid_t whomuid,		/* Calling user, else zero for */
				/* NQS operator/manager call  */
	int req_seqno,		/* Request sequence number */
	Mid_t req_mid)		/* Request machine id */
{
	interclear();
	interw32i ((long) whomuid);
	interw32i ((long) req_seqno);
	interw32u ((Mid_t) req_mid);
	return (inter (oper == 0 ? PKT_HOLDREQ : PKT_RELREQ));
}
