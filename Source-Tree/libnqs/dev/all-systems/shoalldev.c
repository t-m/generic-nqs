/*
 * libnqs/dev/shoalldev.c
 * 
 * DESCRIPTION:
 *
 *	Show information about ALL devices on the local machine.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

/*** shoalldev
 *
 *
 *	int shoalldev():
 *	Show status information on ALL of the local NQS devices.
 *
 *	WARNING:
 *		It is assumed that the current position
 *		of the device definitions file is 0.
 *
 *	Returns:
 *		0: If information about one or more devices
 *		   was displayed.
 *	       -1: If no output was produced.
 *
 */
int shoalldev (struct confd *file)
{
	int result;
	register struct gendescr *descr;

  	assert (file != NULL);
	result = -1;			/* No output produced */
	descr = nextdb (file);
	while (descr != (struct gendescr *)0) {
		if (shodbydesc (descr) == 0) {
			/*
			 *  Output was produced.
			 */
			result = 0;
			putchar ('\n');	/* Intervening space */
		}
		descr = nextdb (file);	/* Get the next device */
	}
	return (result);
}
