/*
 * libnqs/dev/destdev.c
 * 
 * DESCRIPTION:
 *
 *	Return a pointer to the device-name portion of a remote
 *	device specification.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 15, 1985.
 */

#include <libnqs/license.h>			/* NQS types and definitions */
#include <libnqs/proto.h>

/*** destdev
 *
 *
 *	char *destdev():
 *
 *	Return a pointer to the device-name portion of a remote
 *	device specification.
 */
char *destdev (char *remote_device)
{
	static char devname [MAX_DEVNAME+2];
					/* Remote device name of one */
					/* extra byte so that mgr_scan.c */
					/* can tell a remote device */
					/* name that is too long. */
	register char *copyto;		/* Copyto pointer */
	register unsigned size;		/* Copy size */

  	assert(remote_device != NULL);
  
	size = 0;			/* Nothing copied yet */
	copyto = devname;		/* Where to copy */
	while (*remote_device && *remote_device != '@' &&
	       size < MAX_DEVNAME+1) {
		*copyto++ = *remote_device++;
		size++;			/* One more character copied */
	}				/* specification */
	*copyto = '\0';			/* Null terminate */
	return (devname);		/* Return pointer to device name */
}
