/*
 * libnqs/dev/shodbydesc.c
 * 
 * DESCRIPTION:
 *
 *	Show status information for the specified local NQS device.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>

/*** shodbydesc
 *
 *
 *	int shodbydesc():
 *	Show status information for the specified local NQS device.
 *
 *	Returns:
 *		This function always returns 0.
 *
 */
int shodbydesc (struct gendescr *descr)
{
	char hostname [256];		/* 255-char host names */

  	assert (descr != NULL);
  
	sal_gethostname (hostname, 255);	/* Get the name of the local host */
	hostname [255] = '\0';		/* Make absolutely sure that it is */
					/* null terminated */
	printf ("  %s@%s\n", descr->v.dev.dname, hostname);
	printf ("  Fullname: %s\n", descr->v.dev.fullname);
	printf ("  Server: %s\n", descr->v.dev.server);
	printf ("  Forms: %s\n", descr->v.dev.forms);
	printf ("  Status = [");
	if (descr->v.dev.status & DEV_ENABLED) printf ("ENABLED, ");
	else if (descr->v.dev.status & DEV_ACTIVE) {
		printf ("ENABLED/CLOSED, ");
	}
	else printf ("DISABLED, ");
	if (descr->v.dev.status & DEV_FAILED) printf ("FAILED");
	else if (descr->v.dev.status & DEV_ACTIVE) printf ("ACTIVE");
	else printf ("INACTIVE");
	if (descr->v.dev.statmsg [0] != '\0') {
		/*
		 *  Device status message text is defined for this device.
		 *  Display it.
		 */
		printf (", %s", descr->v.dev.statmsg);
	}
	printf ("];\n");
	if (descr->v.dev.status & DEV_ACTIVE) {
		/*
		 *  The device is active and handling a request.
		 *  Display a certain amount of information about
		 *  the current request being handled.
		 */
		printf ("  Request name=%s;  id=%1ld.%s\n",
			descr->v.dev.reqname, descr->v.dev.orig_seqno,
			getmacnam (descr->v.dev.orig_mid));
		printf ("  Owner=%8s;\n", getusenam (descr->v.dev.uid));
	}
	return (0);			/* Output produced */
}
