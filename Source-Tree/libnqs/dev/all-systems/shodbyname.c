/*
 * libnqs/dev/shodbyname.c
 * 
 * DESCRIPTION:
 *
 *	Print on stdout information about a device.
 *
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	December 18, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*** shodbyname
 *
 *
 *	int shodbyname():
 *
 *	Print on stdout information about the named device.
 *	Returns: 0 if output was produced.
 *		-1 if no output was produced.
 */
int shodbyname (struct confd *devicefile, char *fullname)
{
	struct gendescr *descr;
	int found;
	Mid_t itsmid;
	Mid_t mymid;
	char *localname;

  	assert (devicefile != NULL);
  	assert (fullname   != NULL);
  
	switch (machspec (fullname, &itsmid)) {
		case 0:
			break;
		case -1:
		case -2:
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Invalid hostname specification for device %s\n", fullname);
			return (0);
		case -3:
		case -4:
	  		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unexpected error in machspec(), device = %s.\n", fullname);
			return (0);
	}
	if (localmid (&mymid) != 0) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unexpected error in localmid().\n");
		return (0);
	}
	if (itsmid == mymid) {
		found = 0;
		if (telldb (devicefile) != -1) {
			/*
			 *  We are not necessarily at the start of the
			 *  database file....
			 */
			seekdb (devicefile, 0L);/* Seek to the beginning */
		}
		descr = nextdb (devicefile);
		localname = destdev (fullname);
		while (descr != NULL && !found) {
			if (strcmp (descr->v.dev.dname, localname) == 0) {
				found = 1;
			}
			else descr = nextdb (devicefile);
		}
		if (found) return (shodbydesc (descr));
		else {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Device: %s does not exist.\n", fullname);
			return (0);
		}
	}
	else {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "You are asking about a remote device.  Networking is not supported in this implementation.\n");
		return (0);
	}
}
