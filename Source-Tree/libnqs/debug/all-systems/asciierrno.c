/*
 * libnqs/debug/asciierrno.c
 * 
 * DESCRIPTION:
 *
 *	Format present errno value in ASCII diagnostic string.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 27, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdio.h>
#include <errno.h>

/*** asciierrno
 *
 *
 *	char *asciierrno():
 *	Return formatted ASCII value of present errno.
 */
char *asciierrno(void)
{
	static char formatbuffer [20];

	sprintf (formatbuffer, "Errno value: %1d", errno);
	return (formatbuffer);
}
