/*
 * libnqs/debug/errnototcm.c
 * 
 * DESCRIPTION:
 *
 *	Convert a file access related errno code to its corresponding
 *	transaction code.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 27, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <errno.h>			/* Errno definitions */
#include <libnqs/transactcc.h>		/* NQS transaction completion codes */

#include <libsal/license.h>
#include <libsal/libsal.h>

/*** errnototcm
 *
 *
 *	long errnototcm():
 *	Convert errno code to transaction completion code.
 *
 *	Returns:
 *		The transaction code corresponding to the
 *		errno condition.
 */
long errnototcm (void)
{
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "errnototcm: errno is '%d':'%s'\n", errno, libsal_err_strerror(errno));

	switch (errno) {
	case EACCES:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_EACCESS");
		return (TCML_EACCESS);
	case EFBIG:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_EFBIG");
		return (TCML_EFBIG);
	case EISDIR:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_EISDIR");
		return (TCML_EISDIR);
#if	BSD
	case ELOOP:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ELOOP");
		return (TCML_ELOOP);
#endif
	case ENFILE:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ENFILE");
		return (TCML_ENFILE);
	case ENOBUFS:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ENOBUFS");
		return (TCML_ENOBUFS);
	case ENOENT:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ENOENT");
		return (TCML_ENOENT);
	case ENOMEM:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ENOMEM");
		return (TCML_ENOMEM);
	case ENOSPC:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ENOSPC");
		return (TCML_ENOSPC);
	case ENOTDIR:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ENOTDIR");
		return (TCML_ENOTDIR);
	case ENXIO:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ENXIO");
		return (TCML_ENXIO);
	case EPERM:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_EPERM");
		return (TCML_EPERM);
	case EPIPE:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_EPIPE");
		return (TCML_EPIPE);
	case EROFS:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_EROFS");
		return (TCML_EROFS);
	case ETIMEDOUT:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ETIMEDOUT");
		return (TCML_ETIMEDOUT);
	case ETXTBSY:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_ETXTBSY");
		return (TCML_ETXTBSY);
	}
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "errnototcm(): returning TCML_FATALABORT");
	return (TCML_FATALABORT);
}
