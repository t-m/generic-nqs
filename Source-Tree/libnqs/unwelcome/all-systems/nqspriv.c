/*
 * libnqs/unwelcome/nqspriv.c
 * 
 * DESCRIPTION:
 *
 *	Determine the Qmgr privilege bits for the user running the
 *	NQS Qmgr program.  The value returned is the privilege mask
 *	for the user.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxdirs.h>			/* NQS external directories */

/*** nqspriv
 *
 *
 *	nqspriv():
 *	Returns:
 *		The Qmgr privilege bits for the user running the
 *		NQS Qmgr program.  The value returned is the
 *		privilege mask for the user.
 */
int nqspriv (
	uid_t real_uid,		/* REAL user-id of user at */
	Mid_t user_mid,		/* machine: machine_id. */
	Mid_t local_mid)	/* Machine-id of local machine */
{
	register struct confd *file;	/* NQS database file descriptor */
	register struct gendescr *descr;/* Generic descriptor */
	char path [MAX_PATHNAME+1];	/* Pathname of NQS mgr acct file */
	char *root_dir;

	if (real_uid == 0 && user_mid == local_mid) {
		/*
		 *  The caller is running as root on the local machine,
		 *  and thus has all possible NQS Qmgr privileges.
		 */
		return (~0);
	}
        root_dir = getfilnam (Nqs_root, SPOOLDIR);
        if (root_dir == (char *)NULL) return (0);
        sprintf (path, "%s/%s", root_dir, Nqs_mgracct);
	relfilnam (root_dir);
	if ((file = opendb (path, O_RDONLY)) == (struct confd *) 0) return (0);
	descr = nextdb (file);		/* Get next allocated record */
	while (descr != (struct gendescr *)0) {
		if (descr->v.mgr.manager_uid == real_uid &&
		    descr->v.mgr.manager_mid == user_mid) {
			closedb (file);
			return (descr->v.mgr.privileges);
					/* Access permitted */
		}
		descr = nextdb (file);
	}
	closedb (file);			/* Access denied */
	return (0);			/* NO NQS privileges */
}
