/*
 * libnqs/unwelcome/parseserv.c
 * 
 * DESCRIPTION:
 *
 *	This module parses a server invocation command line, placing
 *	the broken-out arguments into the specified argv[] array.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 31, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <string.h>

static char *scanwhite ( char *ch );

/*** parseserv
 *
 *
 *	int parseserv ():
 *	Parse server command line into argv[].
 *
 *	Returns:
 *		0: if successful.
 *	       -1: if there are too many arguments, or the server
 *		   specification is too long.
 */
int parseserv (char *servercmd, char *argv[MAX_SERVERARGS+1])
{

	static char serverspec [MAX_SERVERNAME+1];
					/* Server command and arguments */
	register int i;			/* Index */
	register char *cp;		/* Character pointer */

	if (strlen (servercmd) > (size_t) MAX_SERVERNAME) 
           return (-1);
  
	strcpy (serverspec, servercmd);	/* Copy the server command into */
	i = 0;				/* the static buffer area */
	cp = serverspec;
	do {
		cp = scanwhite (cp);	/* Scan white space */
		if (*cp) {
			/*
			 *  We have an argument.
			 */
			argv [i] = cp;	/* Record start of argument */
			do {		/* Scan argument */
				cp++;
			} while (*cp > ' ');
			if (*cp) {
				/*
				 *  Null terminate argument.
				 */
				*cp = '\0';
				cp++;
			}
		}
		i++;
	} while (*cp && i < MAX_SERVERARGS);
	argv [i] = (char *) 0;		/* Null terminate argv[] list */
	cp = scanwhite (cp);
	if (*cp != '\0') {
		/*
		 *  Too many server arguments in server command line.
		 */
		return (-1);
	}
	return (0);			/* Parse was successful */
}


/*** scanwhite
 *
 *
 *	char *scanwhite():
 *
 *	Scan white space returning pointer to first non-white character
 *	or null character.  This function only works for 7-bit ASCII
 *	characters.
 */
static char *scanwhite (char *ch)
{
	while (*ch && *ch <= ' ') ch++;
	return (ch);
}
