/*
 * libnqs/unwelcome/setmtime.c
 * 
 * WARNING:
 * 	This code will disappear once the transaction information is
 * 	reworked to use an alternative storage mechanism.
 * 
 * DESCRIPTION:
 *
 *	Please read the DESCRIPTION section of ../lib/transact.c
 *	to understand why this module exists.  This module allows
 *	us to implement a minimum transaction mechanism on top
 *	of UNIX implementations that do not support synchronous
 *	write file I/O (where when you return from the system call,
 *	the permanent memory associated with the write operation
 *	HAS ACTUALLY BEEN UPDATED--as opposed to sitting in some
 *	buffer cache....).
 *
 *	Again, see the comments in ../lib/transact.c.  There
 *	are also explanatory comments in ../src/nqs_ldconf.c.
 *
 *	The transaction implementation for UNIX implementations
 *	of the variety mentioned in the first paragraph, requires
 *	a procedure to synchronously update a file inode.  That
 *	is to say, upon successful return from this procedure,
 *	the inode of the named file will have been PHYSICALLY
 *	updated in the file system.
 *
 *	All UNIX implementations known to the author (Silicon
 *	Graphics, Berkeley, & System V) perform
 *	a synchronous update of the target file-inode (the file
 *	inode to link count is being incremented) referenced
 *	by a link(2) system call.
 *
 *	We exploit this property of the link(2) system call
 *	to great advantage in some of the transaction mecha-
 *	nisms implemented within NQS.
 *
 *	I really hate this function, but I need it until UNIX
 *	gets smarter.  Sorry.
 *
 *	This procedure is used by ../lib/transact.c, ../src/nqs_udb.c,
 *	and by ../src/nqs_ldconf.c.
 *
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	March 20, 1986.
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: setmtime.c,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:14  nqs
 * Imported sources
 *
 * Revision 1.7  1994/03/30  20:32:43  jrroma
 * Version 3.35.6
 *
 * Revision 1.6  93/07/13  21:31:35  jrroma
 * Version 3.34
 * 
 * Revision 1.5  92/11/06  13:57:46  jrroma
 * Added support for HPUX.
 * 
 * Revision 1.4  92/06/18  13:24:42  jrroma
 * Added gnu header
 * 
 * Revision 1.3  92/05/06  10:19:26  jrroma
 * Version 3.20
 * 
 * Revision 1.2  92/01/16  17:13:10  jrroma
 * Added support for RS6000.
 * 
 * Revision 1.1  92/01/16  17:12:13  jrroma
 * Initial revision
 * 
 *
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
					/* (and also sys/types.h) */
#if	HAS_BSD_UTIMES
#include <sys/time.h>
#else
#include <time.h>
#include <utime.h>
#include <unistd.h>
#if 0
struct utimbuf {
	time_t acctime;			/* Access time */
	time_t modtime;			/* Modification time */
};
#endif
#endif
#include <string.h>


/*** setmtime
 *
 *
 *	int setmtime():
 *
 *	Set the modification time of the specified file using a synchronous
 *	algorithm.  (The access time of the file is set to the current time.)
 *	Upon successful return, the image of the inode will have been
 *	physically updated in the file system.
 *
 *	The total length of the file pathname must be LESS than MAX_PATHNAME,
 *	and the filename at the end of the pathname must be less than or equal
 *	to 9 characters in length, so that the linkname used to perform the
 *	synchronous update can be created by appending "_LINK" to the end
 *	of pathname (without overflowing the 14-character limit on System V
 *	based UNIX implementations).
 *
 *	Furthermore, the linkname must not conflict with some other file
 *	already existing in the same directory.
 *
 *	Returns:
 *		 0: if successful.
 *		-1: if unsuccessful (errno has reason).
 */
int setmtime (char *path, unsigned long ulongval)
{
#if	HAS_BSD_UTIMES
	struct timeval utimbuf [2];	/* Utimes() buffer */
#else
	struct utimbuf utimbuf;		/* utime() buffer */
#endif
	char linkpath [MAX_PATHNAME+1];	/* Link name for ../database/seqno */

  	assert (path != NULL);
	/*
	 *  Remove any left-over synchronous link path.
	 */
	strcpy (linkpath, path);
	strcat (linkpath, "_LINK");
#if	HAS_BSD_UTIMES
        {
          time_t t;
	  time (&t);	
          utimbuf[0].tv_sec = t; /* Access time = current time */
	}
	utimbuf [0].tv_usec = 0;
	utimbuf [1].tv_sec = ulongval;
	utimbuf [1].tv_usec = 0;
	if (utimes (path, utimbuf) == -1) return (-1);
#else
	time (&utimbuf.actime);	/* Access time = current time */
	utimbuf.modtime = ulongval;
	if (utime (path, &utimbuf) == -1) return (-1);
#endif
	/*
	 *  Now, make a link to the target inode to force the synchronous
	 *  writing of the inode to disk.
	 */
	if (link (path, linkpath) == -1) {
		/*
		 *  The link failed.  It may have failed because the
		 *  synchronous link-name was left over from a previous
		 *  system crash.  Try unlinking the link name, and
		 *  then try again....
		 */
		unlink (linkpath);
		if (link (path, linkpath) == -1) {
			/*
			 *  The link still fails.
			 */
			return (-1);
		}
	}
	/*
	 *  Now, dispose of the synchronous link name path, to avoid
	 *  cluttering up the transaction database subdirectory with
	 *  extraneous names (which would otherwise extend the size
	 *  of the directory unnecessarily--slowing everyone down).
	 */
	unlink (linkpath);		/* Unlink synchronous link */
	return (0);			/* Success */
}
