/*
 * libnqs/unwelcome/shoalllim.c
 * 
 * DESCRIPTION:
 *
 *	Show information about 
 *	1) all limits on the local machine and
 *	2) the shell strategy on the local machine,
 *	in both cases as selected by the display criteria flags.
 *
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	January 13, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*** shoalllim
 *
 *
 *	int shoalllim():
 *	Show status information on all local limits
 *	and the local shell strategy.
 *
 *	WARNING:
 *		It is assumed that the current position of
 *		the parameter definitions file is 0.
 *
 *	Returns:
 *		Always returns 0. Shoalllim always produces output.
 *
 */
int shoalllim (struct confd *file, long flags)
{
    register struct gendescr *descr;	/* pointer into param file */
    int found;		/* boolean: found the correct param descriptor? */

    sal_syslimit stWalker;
    stWalker.stLimitInfo.iName = SAL_LIMIT_START;

    printf("  QSub Switch	Name            Description\n");
    while (sal_syslimit_iterate(&stWalker))
    {
      switch(stWalker.stLimitInfo.iName)
      {
       case SAL_LIMIT_CORE:
	printf ("  -lc\t\t%sCore file size limit\n", stWalker.stLimitInfo.szName);
	break;
       case SAL_LIMIT_DATA:
	printf ("  -ld\t\t%sData segment size limit\n", stWalker.stLimitInfo.szName);
	break;
       case SAL_LIMIT_FSIZE:
	printf ("  -lf\t\t%sPer-process permanent file size limit\n", stWalker.stLimitInfo.szName);
	break;
       case SAL_LIMIT_RFSIZE:
	printf ("  -lF\t\t%sPer-request permanent file size limit\n", stWalker.stLimitInfo.szName);
	break;
       case SAL_LIMIT_VMEM:
	printf ("  -lm\t\t%sPer-process memory size limit\n", stWalker.stLimitInfo.szName);
	break;
       case SAL_LIMIT_NICE:
	printf ("  -ln\t\t%sNice value\n", stWalker.stLimitInfo.szName);
	break;
       case SAL_LIMIT_STACK:
	printf ("  -ls\t\t%sStack segment size limit\n", stWalker.stLimitInfo.szName);
	break;
       case SAL_LIMIT_CPU:
	printf ("  -lt\t\t%sPer-process cpu time limit\n", stWalker.stLimitInfo.szName);
	break;
       case SAL_LIMIT_RCPU:
	printf ("  -lT\t\t%sPer-request cpu time limit\n", stWalker.stLimitInfo.szName);
	break;
       case SAL_LIMIT_RSS:
	printf ("  -lw\t\t%sWorking set limit\n", stWalker.stLimitInfo.szName);
	break;
       default:
	/* printf ("  (none)\t\t%s%s\n", stWalker.stLimitInfo.szName, stWalker.stLimitInfo.szDesc); */
	break;
      }
    }
  
    putchar ('\n');
    if (flags & SHO_SHS) {		/* Show shell strategy */
	descr = nextdb (file);
	found = 0;
	while (descr != (struct gendescr *)0 && !found) {
	    if (descr->v.par.paramtype == PRM_GENPARAMS) {
		found = 1;
		printf ("  Shell strategy = ");
		switch (descr->v.par.v.genparams.shell_strategy) {
		case SHSTRAT_FREE:
		    printf ("FREE\n");
		    break;
		case SHSTRAT_LOGIN:
		    printf ("LOGIN\n");
		    break;
		case SHSTRAT_FIXED:
		    printf ("FIXED: %s\n",
			    descr->v.par.v.genparams.fixed_shell);
		    break;
		}
	    }
	    descr = nextdb (file);	/* Get the next param */
	}
	if (found == 0) {
	    printf ("  No shell strategy.\n");	
	}
	putchar ('\n');
    }
    return (0);
}
