/*
 * libnqs/nqsxdirs.h
 * 
 * DESCRIPTION:
 *
 *	NQS daemon external directory and file definitions.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 2, 1985.
 */

#if	!HAS_BSD_PIPE
extern char *Nqs_fifo;			/* NQS interprocess FIFO pipe */
					/* relative pathname */
extern char *Nqs_ffifo;			/* NQS interprocess FIFO pipe */
					/* full pathname */
#endif
extern char *Nqs_requests;		/* NQS new request directory */
extern char *Nqs_root;			/* NQS root directory */
extern char *Nqs_scripts;		/* NQS shell script links directory */
extern char *Nqs_dump;			/* NQS dump directory if all else */
					/*   fails */
/*
 *	All of the following directories/files are relative to Nqs_root.
 */
extern char *Nqs_control;		/* NQS control directory */
extern char *Nqs_data;			/* NQS data directory */
extern char *Nqs_devices;		/* NQS device status file */
extern char *Nqs_forms;			/* NQS forms file */
extern char *Nqs_netqueues;		/* NQS network queue/status file */
extern char *Nqs_qcomplex;              /* NQS queue complex status file */
extern char *Nqs_queues;		/* NQS non-network queue/status file */
extern char *Nqs_qmaps;			/* NQS queue/device/destination file */
extern char *Nqs_pipeto;		/* NQS pipe destination queues */
extern char *Nqs_params;		/* NQS general parameters file */
extern char *Nqs_mgracct;		/* NQS manager account name file */
extern char *Nqs_servers;		/* NQS compute servers file */
extern char *Nqs_seqno;			/* NQS request seq# file */
extern char *Nqs_qaccess;		/* NQS queue access directory */
extern char *Nqs_qorder;		/* NQS queue ordering directory */
extern char *Nqs_failed;		/* NQS failed directory */
extern char *Nqs_inter;			/* NQS inter-process comm. dir. */
extern char *Nqs_output;		/* NQS temporary output file dir */
extern char *Nqs_transact;		/* NQS transaction directory */
extern char *Nqs_libdir;	       	/* NQS daemons directory env variable */
extern char *Nqs_nmapdir;		/* NQS map directory env variable */
extern char *Nqs_spooldir;		/* NQS spool directory env variable */
