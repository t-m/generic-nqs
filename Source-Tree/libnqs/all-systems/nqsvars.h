/*
 *	Network Queueing System (NQS)
 *  This version of NQS is Copyright (C) 1992  John Roman
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 1, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
*  PROJECT:     Network Queueing System
*  AUTHOR:      John Roman
*
*  Modification history:
*
*       Version Who     When            Description
*       -------+-------+---------------+-------------------------
*       V01.10  JRR                     Initial version.
*       V01.20  JRR     16-Jan-1992	Added support for RS6000.
*       V01.3   JRR     28-Feb-1992	Added Cosmic V2 changes.
*       V01.4   JRR     07-Apr-1992     Added CERN enhancments.
*	V01.5	JRR	17-Jun-1992	Added header.
*	V01.6	JRR	22-Sep-1992	Added LB_Scheduler.
*	V01.7	JRR	01-Mar-1994	Added support for SOLARIS.
*	V01.8	JRR	06-Apr-1994	Ranking compute servers.
*/
/*++ nqsvars.h - Network Queueing System
 *
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/libnqs/all-systems/nqsvars.h,v $
 *
 * DESCRIPTION:
 *
 *
 *	NQS global variable definitions file.
 *
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: nqsvars.h,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:13  nqs
 * Imported sources
 *
 * Revision 1.8  1994/09/02  17:37:23  jrroma
 * Version 3.36
 *
 * Revision 1.7  94/03/30  20:31:41  jrroma
 * Version 3.35.6
 * 
 * Revision 1.6  92/12/22  15:45:24  jrroma
 * Version 3.30
 * 
 * Revision 1.5  92/06/18  09:46:26  jrroma
 * Added gnu header
 * 
 * Revision 1.4  92/05/06  10:06:22  jrroma
 * Version 3.20
 * 
 * Revision 1.3  92/02/28  11:10:55  jrroma
 * Added Cosmic V2 changes.
 * 
 * Revision 1.2  92/01/17  11:48:23  jrroma
 * Added support for RS6000.
 * 
 * Revision 1.1  92/01/17  11:47:37  jrroma
 * Initial revision
 * 
 *
 */

#include "nqsdirs.h"			/* NQS global directory defintions */

char *Argv0;				/* Pointer to argv[0] string for */
					/* the NQS daemon */
int Argv0size;				/* Size of the argv[0] space for */
					/* the NQS daemon */
short Booted;				/* Boolean NQS booted flag.  If */
					/* non-zero, then reqs can be */
					/* spawned, etc.  Otherwise, NQS */
					/* is in the process of initializing */
					/* itself. */
int Debug;				/* NQS debug level */
int Defbatpri;				/* Default intra-queue batch-req */
					/* priority */
char Defbatque [MAX_QUEUENAME+1];	/* Default batch queue name */
long Defdesrettim;			/* Default maximum number of seconds */
					/* that a destination can exist in a */
					/* retry state without being marked */
					/* as failed */
long Defdesretwai;			/* Default number of seconds to wait */
					/* between failed destination */
					/* connection attempts (cannot be 0) */
int Defdevpri;				/* Default intra-queue device-req */
					/* priority */
long Defnetrettim;			/* Default maximum number of seconds */
					/* that a network queue can exist in */
					/* a failed retry state without being*/
					/* marked as failed */
long Defnetretwai;			/* Default number of seconds to wait */
					/* between failed network host */
					/* connection attempts (cannot be 0) */
long Defloadint;			/* Default time to wait between */
					/* reports to scheduler on our load */
char Defprifor [MAX_FORMNAME+1];	/* Default print form name */
char Defprique [MAX_QUEUENAME+1];	/* Default print queue name */
int Extreqcount;			/* Number of requests that have */
					/* arrived at this machine from */
					/* other remote machines */
char Fixed_shell [MAX_SERVERNAME+1];	/* Name of shell to be used if the */
					/* shell strategy is fixed */
int Fromlog;				/* Pipe from NQS log message process */
short Gblbatcount;			/* Total number of batch requests */
					/* running at the moment */
short Gblnetcount;			/* Total number network requests */
					/* running at the moment */
short Gblpipcount;			/* Total number of pipe requests */
					/* running at the moment */
long Lifetime;				/* Lifetime in seconds of requests */
					/* in pipe queues.  Also the */
					/* lifetime of requests in the */
					/* arriving state */
Mid_t Locmid;				/* Machine-id of local host */
char Logfilename [MAX_PATHNAME+1];	/* Logfile name */
uid_t Mail_uid;				/* User-id of NQS mail account */
int Maxcopies;				/* Maximum number of print copies */
int Maxextrequests;			/* Maximum number of requests */
					/* that arrived from remote machines */
					/* that can be simultaneously queued */
					/* on this machine */
short Maxgblacclimit;			/* Maximum number of simultaneous */
					/* connections that will be */
					/* accepted by the local NQS network */
					/* daemon from the progeny of remote */
					/* NQS daemons */
short Maxgblbatlimit;			/* Maximum number of batch requests */
					/* that can simultaneously run on */
					/* the local host */
short Maxgblnetlimit;			/* Maximum number of network requests */
					/* that can simultaneously run on */
					/* the local host */
short Maxgblpiplimit;			/* Maximum number of pipe requests */
					/* that can simultaneously run on */
					/* the local host */
short Maxgblrunlimit;                   /* Maximum number of all requests */
                                        /* that can simultaneously run on */
                                        /* the local host */
int Maxoperet;				/* Maximum device-open retries */
int Maxprint;				/* Maximum number of bytes in a */
					/* print file */
char Netclient [MAX_SERVERNAME+1];	/* Empties network queues */
char Netdaemon [MAX_SERVERNAME+1];	/* Spawns copies of Netserver */
char Loaddaemon [MAX_SERVERNAME+1];	/* Sends load information to NQS */
int Netdaepid;				/* Process-id of NQS network daemon */
					/* (0 if no daemon present) */
char Netserver [MAX_SERVERNAME+1];	/* Exec'd over child of Netdaemon */
struct nqsqueue *Net_queueset;		/* Network queue set */
struct qcomplex *New_qcomplex;          /* Most recently created queue */
                                        /* complex during the NQS boot */
                                        /* rebuild sequence.  (See */
                                        /* nqs_ldconf.c and nqs_updcom.c.) */
struct device *New_device;		/* Most recently created device      */
					/* during NQS boot rebuild sequence. */
					/* See nqs_ldconf.c and nqs_upddev.c.*/
struct qdestmap *New_qdestmap;		/* Most recently created queue/dest */
					/* mapping during the NQS boot */
					/* rebuild sequence.  (See */
					/* nqs_ldconf.c and nqs_upddev.c.) */
struct qdevmap *New_qdevmap;		/* Most recently created queue/device*/
					/* mapping during the NQS boot */
					/* rebuild sequence.  (See */
					/* nqs_ldconf.c and nqs_upddev.c.) */
struct nqsqueue *New_queue;		/* Most recently created queue  */
					/* during NQS boot rebuild sequence. */
					/* See nqs_ldconf.c and nqs_updque.c.*/
struct nqsqueue *Nonnet_queueset;	/* Non-networked queue set */
short Noofdevices;			/* Number of devices presently in */
					/* existence */
int Opewai;				/* Number of seconds to wait between */
					/* failed device opens */
struct pipeto *Pipetoset;		/* Destination set for all local */
					/* pipe queues */
short Plockdae;				/* Plock() status of daemon */
struct nqsqueue *Pribatqueset;		/* Priority ordered batch queue set */
struct nqsqueue *Prinetqueset;		/* Priority ordered network queue set*/
struct nqsqueue *Pripipqueset;		/* Priority ordered pipe queue set */
struct qcomplex *Qcomplexset;           /* Queue complex set */
struct running *Runvars;                /* Variables for running requests */
int Runvars_size;                       /* Number of entries in the Runvars */
                                        /* structure */
long Seqno_user;			/* Next available seq# for batch and */
					/* device requests */
short Shell_strategy;			/* Batch request shell script choice */
					/* strategy = [SHSTRAT_FREE, */
					/* SHSTRAT_FIXED] */
short Shutdown;				/* Non-zero if shutdown in progress */
short Termsignal;			/* Terminating signal sent to the */
					/* process group of an exiting */
					/* request */
long Udbgenparams;			/* Offset of general parameters */
					/* record in the params file */
long Udblogfile;			/* Offset of the logfile record */
					/* in the params file */
long Udbnetprocs;			/* Offset of the network processes */
					/* record in the params file */
struct confd *Devicefile;		/* Device descriptor file */
struct confd *Netqueuefile;		/* Network queue descriptor file */
struct confd *Qcomplexfile;             /* Queue complex descriptor file */
struct confd *Queuefile;		/* Queue descriptor file */
struct confd *Qmapfile;			/* Queue/device/destination mapping */
					/* file */
struct confd *Pipeqfile;		/* Pipe queue destination file */
struct confd *Paramfile;		/* General parameters file */
struct confd *Mgrfile;			/* NQS manager access list file */
struct confd *Formsfile;		/* NQS forms list file */
struct confd *Serverfile;		/* NQS compute server list file */
struct device *Devset;			/* Device set */
int Read_fifo;				/* Read FIFO descriptor used to */
					/* receive new request packets */
int Write_fifo;				/* Write FIFO descriptor.  This file */
					/* descriptor has the FIFO pipe */
					/* open for writing to prevent the */
					/* NQS daemon from exiting when	*/
					/* no processes have the request */
					/* pipe open.  IT IS CLOSED when */
					/* a shutdown request is received */
					/* so that the daemon can shutdown */
					/* when the last requesting proc */
					/* closes the FIFO request pipe */
					/* or exits.  Furthermore, this file */
					/* descriptor is used by pipe queue */
					/* servers and shepherd processes to */
					/* send appropriate message packets */
					/* to the local NQS daemon */
Mid_t LB_Scheduler;			/* Mid of system that does the load */
					/* scheduling */
int Loaddaepid;				/* Process-id of NQS load daemon */
					/* (0 if no daemon present) */
struct loadinfo Loadinfo_head;		/* Structure to hold pointer to load info */					
