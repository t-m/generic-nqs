/*
 *	Network Queueing System (NQS)
 *  This version of NQS is Copyright (C) 1992  John Roman
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 1, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
*  PROJECT:     Network Queueing System
*  AUTHOR:      John Roman
*
*  Modification history:
*
*       Version Who     When            Description
*       -------+-------+---------------+-------------------------
*       V01.10  JRR                     Initial version.
*       V01.20  JRR     16-Jan-1992	Added support for RS6000.
*       V01.3   JRR     28-Feb-1992	Added Cosmic V2 changes.
*	V01.4	JRR	17-Jun-1992	Added header.
*	V01.5	JRR	30-Mar-1994	Ranking Compute Servers
*/
/*++ netxvars.h - Network Queueing System
 *
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/libnqs/all-systems/netxvars.h,v $
 *
 * DESCRIPTION:
 *
 *
 *	NQS network daemon global external variable definitions file.
 *
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	December 23, 1985.
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: netxvars.h,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:12  nqs
 * Imported sources
 *
 * Revision 1.5  1994/09/02  17:37:18  jrroma
 * Version 3.36
 *
 * Revision 1.4  92/06/18  09:46:19  jrroma
 * Added gnu header
 * 
 * Revision 1.3  92/02/28  10:11:34  jrroma
 * Added Cosmic V2 changes.
 * 
 * Revision 1.2  92/01/17  11:37:37  jrroma
 * Added support for RS6000.
 * 
 * Revision 1.1  92/01/17  11:36:55  jrroma
 * Initial revision
 * 
 *
 */

#include "nqsxdirs.h"			/* NQS global directory defintions */

extern char *Argv0;			/* Pointer to argv[0] string for */
					/* the NQS daemon */
extern int Argv0size;			/* Size of the argv[0] space for */
					/* the NQS daemon */
extern int Debug;			/* NQS debug level */
extern Mid_t Locmid;			/* Machine-id of local host */
extern FILE *NetLogfile;		/* Log process stream */
extern struct confd *Devicefile;	/* Device descriptor file */
extern struct confd *Netqueuefile;	/* Network queue descriptor file */
extern struct confd *Queuefile;		/* Non-network queue descriptor file */
extern struct confd *Qmapfile;		/* Queue-device mapping file */
extern struct confd *Pipeqfile;		/* Pipe queue destination file */
extern struct confd *Paramfile;		/* General parameters file */
extern struct confd *Mgrfile;		/* NQS manager access list file */
extern struct confd *Formsfile;		/* NQS forms list file */
extern struct confd *Qcomplexfile;      /* Queue complex descriptor file */
extern struct confd *Serverfile;	/* NQS compute server list file */
