/*
 *	Network Queueing System (NQS)
 *  This version of NQS is Copyright (C) 1992  John Roman
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 1, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
*  PROJECT:     Network Queueing System
*  AUTHOR:      John Roman
*
*  Modification history:
*
*       Version Who     When            Description
*       -------+-------+---------------+-------------------------
*       V01.10  JRR                     Initial version.
*       V01.20  JRR     20-Mar-1992	Added NPK_SUSPENDREQ.
*	V01.3	JRR	17-Jun-1992	Added header.
*	V01.4	JRR	21-Sep-1992	Added load and request completion
*					packets.
*/
/*++ netpacket.h - Network Queueing System
 *
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/libnqs/all-systems/netpacket.h,v $
 *
 * DESCRIPTION:
 *
 *	NQS network packet definitions file.
 *
 *
 *	WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING
 *	WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING
 *	WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING
 *	WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING
 *	WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING
 *	WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING
 *
 *
 *	    ALL symbols starting with the characters:  NPK_
 *	    as defined in this file are TOXIC, and should be
 *	    left completely alone.
 *
 *	    Raise your right paw, and repeat after me:
 *
 *		I, <your-name>, will NOT change the
 *		numerical value of any symbol defined
 *		in this file starting with the
 *		characters:
 *
 *		    NPK_
 *
 *		I understand that all such symbols
 *		defined in this file denote NQS
 *		network packet types, or related
 *		network packet constants.
 *
 *		I furthermore promise NEVER to change
 *		the format of an NQS network packet.
 *
 *	    It is critical to understand that network packets
 *	    are exactly what they sound like.  They are
 *	    message packets which transmit information from
 *	    one NQS process to another NQS process on another
 *	    completely DIFFERENT machine.
 *
 *	    To be even more precise, network packets of the
 *	    NPK_ form are always sent FROM NQS client processes,
 *	    and tell remote NQS server processes what to do.
 *	    NQS remote server processes then return transaction
 *	    completion codes (see ../h/transactcc.h) telling
 *	    the client process the result of their orders.
 *
 *	    As time passes, the probability increases that
 *	    there will be DIFFERENT versions of NQS running
 *	    at different installations.
 *
 *	    In all cases however, EVERY NQS implementation
 *	    MUST use the SAME network packet types, and the
 *	    SAME network packet formats so that other remote
 *	    machines running other implementations of NQS
 *	    will be able to understand each other.  Otherwise,
 *	    NQS will have to be renamed 'QS' with the 'N' for
 *	    networking removed.
 *
 *	    Note however that the brave software developer
 *	    MAY elect to ADD new network packet types, which
 *	    other unmodified versions of NQS will not understand.
 *
 *	    Furthermore, if you ever add a network packet
 *	    type, you can never delete it.  Adding a new
 *	    network packet type is a lot like adding a new
 *	    system call to an operating system.  Once added,
 *	    it cannot be removed.
 *
 *	    Don't add new network packet types recklessly.
 *	    You'll have to live with your decisions forever.
 *
 *
 *
 *	END OF WARNING *** END OF WARNING *** END OF WARNING *** END OF WARNING
 *	END OF WARNING *** END OF WARNING *** END OF WARNING *** END OF WARNING
 *	END OF WARNING *** END OF WARNING *** END OF WARNING *** END OF WARNING
 *	END OF WARNING *** END OF WARNING *** END OF WARNING *** END OF WARNING
 *	END OF WARNING *** END OF WARNING *** END OF WARNING *** END OF WARNING
 *	END OF WARNING *** END OF WARNING *** END OF WARNING *** END OF WARNING
 *	    
 *
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 22, 1986.
 *
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: netpacket.h,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:12  nqs
 * Imported sources
 *
 * Revision 1.5  1992/12/22  15:45:05  jrroma
 * Version 3.30
 *
 * Revision 1.4  92/06/18  09:46:17  jrroma
 * Added gnu header
 * 
 * Revision 1.3  92/04/07  09:31:47  jrroma
 * *** empty log message ***
 * 
 * Revision 1.2  92/04/07  09:31:27  jrroma
 * *** empty log message ***
 * 
 * Revision 1.1  92/03/20  16:42:01  jrroma
 * Initial revision
 * 
 *
 */


#define	NPK_MAGIC1	0x4E515301	/* Magic# used in establish() */
					/* connect protocol */


/*
 *	NQS network packet types.
 *
 *	Batch request and device request packets.
 */
#define	NPK_QUEREQ	0		/* Remotely queue a request */
#define	NPK_DELREQ	1		/* Remotely delete a request */
					/* RESERVED for future use */
#define	NPK_COMMIT	2		/* Remotely commit some action, */
					/* such as the queueing of a request */

/*
 *	NQS local server connection packet.
 *
 *	This packet is sent by a local NQS client process to the local
 *	NQS network daemon, whenever a connection must be established
 *	from a local client process to the local NQS daemon.
 *
 *	This packet is used ONLY for UNIX implementations that do NOT
 *	support named-pipes (i.e. Berkeley 4.2 and 4.3).
 */
#define	NPK_SERVERCONN	90		/* Local server connect packet */

/*
 *	File system packets.
 *
 *	The following actions involving the file system are
 *	undertaken in such a way as to insure their failure
 *	if the user on whose behalf they are being executed
 *	lacks the necessary permissions.
 */
#define NPK_DONE	100		/* Marks the end of an iterative */
					/* conversation */
#define NPK_MKDIR	101		/* Make a directory */
					/* RESERVED for future use */
#define	NPK_REQFILE	102		/* Deliver a file for a previously */
					/* queued request */
#define	NPK_CPINFILE	103		/* Copy-in a file */
					/* RESERVED for future use */
#define	NPK_CPOUTFILE	104		/* Copy-out a file */
					/* RESERVED for future use */
#define	NPK_MVINFILE	105		/* Move-in a file */
					/* RESERVED for future use */
#define	NPK_MVOUTFILE	106		/* Move-out a file */
#define NPK_SUSPENDREQ  207		/* Remote suspend / resume  */


/*
 *	Status packets.
 */
#define	NPK_QDEV	200		/* Remote Qdev startup packet*/
					/* RESERVED for future use */
#define	NPK_QLIMIT	201		/* Remote Qlimit startup packet */
					/* RESERVED for future use */
#define	NPK_QMGR	202		/* Remote Qmgr startup packet */
					/* RESERVED for future use */
#define	NPK_QSTAT	203		/* Remote Qstat startup packet */
#define NPK_LOAD	204		/* Load status packet */
#define NPK_RREQCOM	205		/* Request completion packet */
