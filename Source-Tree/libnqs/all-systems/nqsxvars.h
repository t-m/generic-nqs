/*
 * libnqs/nqsxvars.h
 * 
 * DESCRIPTION:
 *
 *	NQS global (and external) variable definitions file.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/nqsxdirs.h>		/* NQS global directory defintions */

extern char *Argv0;			/* Pointer to argv[0] string for */
					/* the NQS daemon */
extern int Argv0size;			/* Size of the argv[0] space for */
					/* the NQS daemon */
extern short Booted;			/* Boolean NQS booted flag.  If */
					/* non-zero, then reqs can be */
					/* spawned, etc.  Otherwise, NQS */
					/* is in the process of initializing */
					/* itself. */
extern int Defbatpri;			/* Default intra-queue batch-req */
					/* priority */
extern char Defbatque [MAX_QUEUENAME+1];/* Default batch queue name */
extern long Defdesrettim;		/* Default maximum number of seconds */
					/* that a destination can exist in a */
					/* retry state without being marked */
					/* as failed */
extern long Defdesretwai;		/* Default number of seconds to wait */
					/* between failed destination */
					/* connection attempts (cannot be 0) */
extern int Defdevpri;			/* Default intra-queue device-request*/
					/* priority */
extern long Defnetrettim;		/* Default maximum number of seconds */
					/* that a network queue can exist in */
					/* a failed retry state without being*/
					/* marked as failed */
extern long Defnetretwai;		/* Default number of seconds to wait */
					/* between failed network host */
					/* connection attempts (cannot be 0) */
extern long Defloadint;			/* Default number of seconds to wait */
					/* between reports to scheduler */
extern char Defprifor [MAX_FORMNAME+1];	/* Default print form name */
extern char Defprique [MAX_QUEUENAME+1];/* Default print queue name */
extern int Extreqcount;			/* Number of requests that have */
					/* arrived at this machine from */
					/* other remote machines */
extern char Fixed_shell [MAX_SERVERNAME+1];
					/* Name of shell to be used if the */
					/* shell strategy is fixed */
extern int Fromlog;			/* Pipe from NQS log message process */
extern short Gblbatcount;		/* Total number of batch requests */
					/* running at the moment */
extern short Gblnetcount;		/* Total number network requests */
					/* running at the moment */
extern short Gblpipcount;		/* Total number of pipe requests */
					/* running at the moment */
extern long Lifetime;			/* Lifetime in seconds of requests */
					/* in pipe queues.  Also the */
					/* lifetime of requests in the */
					/* arriving state */
extern Mid_t Locmid;			/* Machine-id of local host */
extern char Logfilename[MAX_PATHNAME+1];/* Logfile name */
extern uid_t Mail_uid;			/* User-id of NQS mail account */
extern int Maxcopies;			/* Maximum number of print copies */
extern int Maxextrequests;		/* Maximum number of requests */
					/* that arrived from remote machines */
					/* that can be simultaneously queued */
					/* on this machine */
extern short Maxgblacclimit;		/* Maximum number of simultaneous */
					/* connections that will be */
					/* accepted by the local NQS network */
					/* daemon from the progeny of remote */
					/* NQS daemons */
extern short Maxgblbatlimit;		/* Maximum number of batch requests */
					/* that can simultaneously run on */
					/* the local host */
extern short Maxgblnetlimit;		/* Maximum number of network requests */
					/* that can simultaneously run on */
					/* the local host */
extern short Maxgblpiplimit;		/* Maximum number of pipe requests */
					/* that can simultaneously run on */
					/* the local host */
extern short Maxgblrunlimit;            /* Maximum number of all requests */ 
                                        /* that can simultaneously run on */
                                        /* the local host */
extern int Maxoperet;			/* Maximum device-open retries */
extern int Maxprint;			/* Maximum number of bytes in a */
					/* print file */
extern char Netclient[MAX_SERVERNAME+1];/* Empties network queues */
extern char Netdaemon[MAX_SERVERNAME+1];/* Spawns copies of Netserver */
extern char Loaddaemon[MAX_SERVERNAME+1];/* Sends load information to NQS */
extern int Netdaepid;			/* Process-id of NQS network daemon */
					/* (0 if no daemon present) */
extern char Netserver[MAX_SERVERNAME+1];/* Exec'd over child of Netdaemon */
extern struct nqsqueue *Net_queueset;	/* Network queue set */
extern struct qcomplex *New_qcomplex;   /* Most recently created queue */
                                        /* complex during the NQS boot */
                                        /* rebuild sequence.  (See */
                                        /* nqs_ldconf.c and nqs_updcom.c.) */
extern struct device *New_device;	/* Most recently created device      */
					/* during NQS boot rebuild sequence. */
					/* See nqs_ldconf.c and nqs_upddev.c.*/
extern struct qdestmap *New_qdestmap;	/* Most recently created queue/dest */
					/* mapping during the NQS boot */
					/* rebuild sequence.  (See */
					/* nqs_ldconf.c and nqs_upddev.c.) */
extern struct qdevmap *New_qdevmap;	/* Most recently created queue/device*/
					/* mapping during the NQS boot */
					/* rebuild sequence.  (See */
					/* nqs_ldconf.c and nqs_upddev.c.) */
extern struct nqsqueue *New_queue;		/* Most recently created queue  */
					/* during NQS boot rebuild sequence. */
					/* See nqs_ldconf.c and nqs_updque.c.*/
extern struct nqsqueue *Nonnet_queueset;	/* Non-networked queue set */
extern short Noofdevices;		/* Number of devices presently in */
					/* existence */
extern int Opewai;			/* Number of seconds to wait between */
					/* failed device opens */
extern struct pipeto *Pipetoset;	/* Destination set for all local */
					/* pipe queues */
extern short Plockdae;			/* Plock() status of daemon */
extern struct nqsqueue *Pribatqueset;	/* Priority ordered batch queue set */
extern struct nqsqueue *Prinetqueset;	/* Priority ordered network queue set*/
extern struct nqsqueue *Pripipqueset;	/* Priority ordered pipe queue set */
extern struct qcomplex *Qcomplexset;    /* Queue complex set */
extern struct running *Runvars;		/* Variables for running requests */
extern int Runvars_size;                /* Number of entries in the Runvars */
                                        /* structure */
extern long Seqno_user;			/* Next available seq# for batch and */
					/* device requests */
extern short Shell_strategy;		/* Batch request shell script choice */
					/* strategy = [SHSTRAT_FREE, */
					/* SHSTRAT_FIXED] */
extern short Shutdown;			/* Non-zero if shutdown in progress */
extern short Termsignal;		/* Terminating signal sent to the */
					/* process group of an exiting */
					/* request */
extern long Udbgenparams;		/* Offset of general parameters */
					/* record in the params file */
extern long Udblogfile;			/* Offset of the logfile record */
					/* in the params file */
extern long Udbnetprocs;		/* Offset of the network processes */
					/* record in the params file */
extern struct confd *Devicefile;	/* Device descriptor file */
extern struct confd *Netqueuefile;	/* Network queue descriptor file */
extern struct confd *Qcomplexfile;      /* Queue complex descriptor file */
extern struct confd *Queuefile;		/* Queue descriptor file */
extern struct confd *Qmapfile;		/* Queue/device/destination mapping */
					/* file */
extern struct confd *Pipeqfile;		/* Pipe queue destination file */
extern struct confd *Paramfile;		/* General parameters file */
extern struct confd *Mgrfile;		/* NQS manager access list file */
extern struct confd *Formsfile;		/* NQS forms list file */
extern struct confd *Serverfile;	/* NQS compute server list file */
extern struct device *Devset;		/* Device set */
extern int Read_fifo;			/* Read FIFO descriptor used to */
					/* receive new request packets */
extern int Write_fifo;			/* Write FIFO descriptor.  This file */
					/* descriptor has the FIFO pipe */
					/* open for writing to prevent the */
					/* NQS daemon from exiting when	*/
					/* no processes have the request */
					/* pipe open.  IT IS CLOSED when */
					/* a shutdown request is received */
					/* so that the daemon can shutdown */
					/* when the last requesting proc */
					/* closes the FIFO request pipe */
					/* or exits.  Furthermore, this file */
					/* descriptor is used by pipe queue */
					/* servers and shepherd processes to */
					/* send appropriate message packets */
					/* to the local NQS daemon */
extern Mid_t LB_Scheduler;		/* Mid of system to do load scheduling */
extern int Loaddaepid;			/* Process-id of NQS load daemon */
					/* (0 if no daemon present) */
extern struct loadinfo Loadinfo_head;	/* Head of the loadinfo queue */
