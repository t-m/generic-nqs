/*
 *	Network Queueing System (NQS)
 *  This version of NQS is Copyright (C) 1992  John Roman
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 1, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*++ quolim.h - Network Queueing System
 *
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/libnqs/all-systems/quolim.h,v $
 *
 * DESCRIPTION:
 *
 *
 *	Quota structure definitions file.
 *
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: quolim.h,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:13  nqs
 * Imported sources
 *
 * Revision 1.2  92/06/18  09:46:28  jrroma
 * Added gnu header
 * 
 * Revision 1.1  92/06/18  09:13:22  jrroma
 * Initial revision
 * 
 *
 */

/*
 *	Quota limit units:
 */
#define	QLM_BYTES	011	/* Resource limit in bytes */
#define	QLM_WORDS	012	/* Resource limit in words */
#define	QLM_KBYTES	021	/* Resource limit in kilobytes */
#define	QLM_KWORDS	022	/* Resource limit in kilowords */
#define	QLM_MBYTES	031	/* Resource limit in megabytes */
#define	QLM_MWORDS	032	/* Resource limit in megawords */
#define	QLM_GBYTES	041	/* Resource limit in gigabytes */
#define	QLM_GWORDS	042	/* resource limit in gigawords */


/*
 *	Quota structures:
 */
struct cpulimit {
	unsigned long max_seconds;	/* Maximum CPU time in seconds */
	unsigned long warn_seconds;	/* Warning CPU time in seconds */
	unsigned short max_ms;		/* + max_ms (milliseconds) */
	unsigned short warn_ms;		/* + warn_ms (milliseconds) */
};


struct quotalimit {
	unsigned long max_quota;	/* Max quota limit measured in */
	unsigned long warn_quota;	/* Warn quota limit measured in */
	short max_units;		/* units of type: max_units */
	short warn_units;		/* units of type: warn_units */
};

struct TAG_nqs_limittable
{
  int  iNQSName;			/* NQS name for a limit */
  int  iSALName;			/* libsal name for a limit */
  char cSwitch;				/* qsub switch for the limit */
  char *szDescription;			/* description of the limit */
};

typedef struct TAG_nqs_limittable nqs_limittable;

