/*
 *	Network Queueing System (NQS)
 *  This version of NQS is Copyright (C) 1992  John Roman
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 1, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
*  PROJECT:     Network Queueing System
*  AUTHOR:      John Roman
*
*  Modification history:
*
*       Version Who     When            Description
*       -------+-------+---------------+-------------------------
*       V01.10  JRR                     Initial version.
*       V01.20  JRR     28-Feb-1992	Add Cosmic V2 revisions.
*	V01.3	JRR	17-Jun-1992	Added header.
*/
/*++ mkreqcc.h - Network Queueing System
 *
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/libnqs/all-systems/mkreqcc.h,v $
 *
 * DESCRIPTION:
 *
 *	Mkreq completion code definitions.
 *
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: mkreqcc.h,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:12  nqs
 * Imported sources
 *
 * Revision 1.3  92/06/18  09:46:17  jrroma
 * Added gnu header
 * 
 * Revision 1.2  92/02/28  10:03:50  jrroma
 * Added Cosmic V2 changes.
 * 
 * Revision 1.1  92/02/28  10:01:40  jrroma
 * Initial revision
 * 
 *
 */

#define	MKREQ_SUCCESS	 0	/* Control file successfully created */
#define	MKREQ_NOCREATE	 (-1)	/* Unable to open/create control file */
#define	MKREQ_NOMID	 (-2)	/* Unable to get machine-id of local host */
#define	MKREQ_BADREQTYPE (-3)	/* Bad request type specified */
#define	MKREQ_NOUSERNAME (-4)	/* Could not get username for user-id */
#define	MKREQ_NOCWD	 (-5)	/* Unable to get current working directory */
#define	MKREQ_CWDNEWLINE (-6)	/* Current working directory contained */
				/* a new line character */
#define	MKREQ_NOCHDIRNEW (-7)	/* Unable to chdir() to the NQS new request */
				/* directory */
#define	MKREQ_NOLOCALDAE (-8)	/* Unable to get pipe to local daemon */
#define	MKREQ_NOSETUGID	 (-9)	/* Unable to set [uid,gid] */
#define	MKREQ_NOPARMFILE (-10)	/* Unable to open NQS parameters file */
#define MKREQ_INVALIDACC (-11)  /* Invalid account ID */
