/*
 *	Network Queueing System (NQS)
 *  This version of NQS is Copyright (C) 1992  John Roman
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 1, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
*  PROJECT:     Network Queueing System
*  AUTHOR:      John Roman
*
*  Modification history:
*
*       Version Who     When            Description
*       -------+-------+---------------+-------------------------
*       V01.10  JRR                     Initial version.
*       V01.20  JRR     16-Jan-1992	Added support for RS6000.
*	V01.3   JRR	28-FEb-1992	Added Cosmic V2 changes.
*	V01.4	JRR	17-Jun-1992	Added header.
*	V01.5	JRR	30-Mar-1994	Ranking compute servers.
*/
/*++ netvars.h - Network Queueing System
 *
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/libnqs/all-systems/netvars.h,v $
 *
 * DESCRIPTION:
 *
 *
 *	NQS network daemon global variable definitions file.
 *
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	December 23, 1985.
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: netvars.h,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:12  nqs
 * Imported sources
 *
 * Revision 1.5  1994/09/02  17:37:17  jrroma
 * Version 3.36
 *
 * Revision 1.4  92/06/18  09:46:18  jrroma
 * Added gnu header
 * 
 * Revision 1.3  92/02/28  10:05:30  jrroma
 * Added Cosmic V2 changes.
 * 
 * Revision 1.2  92/01/17  11:36:30  jrroma
 * Added support for RS6000.
 * 
 * Revision 1.1  92/01/17  11:35:50  jrroma
 * Initial revision
 * 
 *
 */

#include "nqsdirs.h"			/* NQS global directory defintions */

char *Argv0;				/* Pointer to argv[0] string for */
					/* the NQS daemon */
int Argv0size;				/* Size of the argv[0] space for */
					/* the NQS daemon */
int Debug;				/* NQS debug level */
Mid_t Locmid;				/* Machine-id of local host */
FILE *NetLogfile;			/* Log process stream */
struct confd *Devicefile;		/* Device descriptor file */
struct confd *Netqueuefile;		/* Network queue descriptor file */
struct confd *Queuefile;		/* Non-network Queue descriptor file */
struct confd *Qmapfile;			/* Queue-device mapping file */
struct confd *Pipeqfile;		/* Pipe queue destination file */
struct confd *Paramfile;		/* General parameters file */
struct confd *Mgrfile;			/* NQS manager access list file */
struct confd *Formsfile;		/* NQS forms list file */
struct confd *Qcomplexfile;             /* Queue complex descriptor file */
struct confd *Serverfile;		/* NQS compute server list file */

