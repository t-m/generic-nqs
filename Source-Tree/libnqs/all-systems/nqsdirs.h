/*
 * libnqs/nqsdirs.h
 * 
 * DESCRIPTION:
 *
 *	NQS daemon directory and file definitions.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 2, 1985.
 */

#if	!HAS_BSD_PIPE
char *Nqs_fifo = "FIFO";		/* NQS interprocess FIFO pipe */
					/* relative pathname */
char *Nqs_ffifo = "new/requests/FIFO";
					/* NQS interprocess FIFO pipe */
					/* full pathname */
#endif

char *Nqs_requests = "new/requests";
					/* NQS new request directory */
char *Nqs_root = "private/root";
					/* NQS root directory */
char *Nqs_scripts = "scripts";
					/* NQS shell-script links directory */
char *Nqs_dump = "dump";
					/* NQS last chance directory */					
/*
 *	All of the following directories/files are relative to Nqs_root.
 */
char *Nqs_control = "control";		/* NQS control directory */
char *Nqs_data = "data";		/* NQS data directory */
char *Nqs_devices = "database/devices";	/* NQS device status file */
char *Nqs_forms = "database/forms";	/* NQS forms file */
char *Nqs_netqueues = "database/netqueues";
					/* NQS network queue/status file */
char *Nqs_queues = "database/queues";	/* NQS non-network queue/status file */
char *Nqs_qmaps = "database/qmaps";	/* NQS queue/device/destination file */
char *Nqs_pipeto = "database/pipeto";	/* NQS pipe destination queues */
char *Nqs_params = "database/params";	/* NQS general parameters file */
char *Nqs_mgracct = "database/managers";/* NQS manager account name file */
char *Nqs_servers = "database/servers"; /* NQS compute servers file */
char *Nqs_seqno = "database/seqno";	/* NQS request seq# file */
char *Nqs_qaccess = "database_qa";	/* NQS queue access directory */
char *Nqs_qorder = "database_qo";	/* NQS queue ordering directory */
char *Nqs_failed = "failed";		/* NQS failed directory */
char *Nqs_inter = "interproc";		/* NQS inter-process comm. dir. */
char *Nqs_output = "output";		/* NQS temporary output file dir. */
char *Nqs_transact = "transact";	/* NQS transaction directory */
char *Nqs_qcomplex = "database/qcomplex";
					/* NQS queue complex status file */
char *Nqs_libdir = "NQS_LIBEXE";	/* NQS default daemon directory */
char *Nqs_nmapdir = "NQS_NMAP";		/* NQS default network map directory */
char *Nqs_spooldir = "NQS_SPOOL";	/* NQS default spooling directory */
