#ifndef __NQS_PROTO_HEADER
#define __NQS_PROTO_HEADER

#ifndef __NQS_LICENSE_HEADER
YOU HAVE FORGOTTEN TO INCLUDE THE LICENSE STATEMENT FOR GENERIC NQS, STU!!!
#endif

/*
 * nqs/proto.h
 * Prototypes for Generic NQS
 */

#include <libnqs/defines.h>
#include <libnqs/types.h>

#include <libsal/types.h>

#include <sys/types.h>
#include <stdio.h>
#include <nqsdaemon/features.h>

void a2s_a2aset ( struct request *req, struct nqsqueue *queue );
void a2s_a2hset ( struct request *req, struct nqsqueue *queue );
void a2s_a2qset ( struct request *req, struct nqsqueue *queue );
void a2s_a2rset ( struct request *req, struct nqsqueue *queue );
void a2s_a2wset ( struct request *req, struct nqsqueue *queue );
unsigned long a6btoul (char *, int);
long aboque ( char *que_name, int wait_time );
int acct_analyze ( char *account_spec, long *ptrtouid, Mid_t *ptrtomid );
long addfor ( char *form );
long addnqsman ( uid_t mgr_uid, Mid_t machine_id, int privilege_bits );
long addqcomque ( char *que_name, char *qcom_name );
long addquedes ( char *local_queue, char *dest_queue, Mid_t dest_mid );
long addquedev ( char *queue_name, char *device_name );
long addquegid ( char *queue_name, gid_t gid );
long addqueuid ( char *queue_name, uid_t uid );
void adsdes_set ( char *quename, int opcode );
void adsdev_set ( char *quename, int opcode );
void adsfor_set ( int opcode );
void adsgid_set ( char *quename, int opcode );
void adsmgr_set ( int opcode );
void adsque_set ( char *qcom_name, int opcode );
void adsuid_set ( char *quename, int opcode );
long allodb (struct confd *, struct gendescr *, int);
long alterreq ( uid_t whomuid, int seqno, Mid_t mid, int priority );
void analyzercm (long, sal_debug_msg_t, char *);
void analyzetcm (long, sal_debug_msg_t, char *);
void append_commandline(const char *);
char *asciierrno ( void );

long blockdb (long);
int bsc_compare (struct nqsqueue *, struct request *, struct request *);
void bsc_reqcom ( struct request *request );
void bsc_resort (void);
int bsc_sched ( struct rawreq *rawreq );
void bsc_spawn ( void );
long bsc_verqueusr ( uid_t uid, struct nqsqueue *queue, char *que_name );
char *bufsockch ( void );
void bufstderr ( void );
void bufstdout ( void );
int buildenv ( void );
int bytesreadable ( int in );
int call_host ( uid_t whomuid, long flags, Mid_t itsmid, char *localname );
int checkinput ( void );
int check_rmt ( char *text, Mid_t *machine_id );
void closedb ( struct confd *file );
long control_daemon ( long action, long daemon );
int convsectime ( time_t secs, int *days, int *hours, int *minutes );
int cpulimhigh ( unsigned long secs, unsigned short ms, unsigned short infinite, unsigned long *new_secsp, unsigned short *new_msp );
int cpulimlow ( unsigned long secs, unsigned short ms, unsigned long *new_secsp, unsigned short *new_msp );
long crebatque ( char *que_name, int que_priority, int que_runlimit, int que_pipeonly, int que_usrlimit );
long crecom ( char *qcom_name );
long credev ( char *dev_name, char *dev_forms, char *dev_fullname, char *dev_server );
long credevque ( char *que_name, int que_priority, int que_pipeonly );
long crepipque ( char *que_name, int que_priority, int que_runlimit, int que_pipeonly, char *que_server, int que_ldb );
#if	HAS_BSD_PIPE
int daepres ( struct confd *runfile );
#else
int daepres ( void );
#endif
long delcom ( char *qcom_name );
long deldev ( char *dev_name );
long delfor ( char *form );
long delnqsman ( uid_t mgr_uid, Mid_t machine_id, int privilege_bits );
long delque ( char *que_name );
long delquedes ( char *local_queue, char *dest_queue, Mid_t dest_mid );
long delquedev ( char *queue_name, char *device_name );
long delquegid ( char *queue_name, gid_t gid );
long delqueuid ( char *queue_name, uid_t uid );
long delreq ( uid_t mapped_uid, long orig_seqno, Mid_t orig_mid, Mid_t target_mid, int sig, int reqquestate );
Mid_t deq_mid ( void );
int deq_priv ( void );
long deq_qual_type ( void );
char *deq_set ( void );
long deq_uid_gid ( void );
char *destacct ( char *remote_account );
char *destdev ( char *remote_device );
char *destpath ( char *remote_path );
char *destqueue ( char *remote_queue );
void diagnose ( long code );
int diagqsuspend ( long code, char *reqid );
long disdev ( char *device_name );
long disque ( char *queue_name );
int docmd ( char *cmdbuf );
void dsc_reqcom ( struct device *device, struct request *request );
int dsc_sched ( struct rawreq *rawreq );
void dsc_spawn ( void );
int dsp_q ( struct confd *file, struct gendescr *que_descr, long flag, short dae_present, struct confd *qmapfile, struct confd *pipeqfile, struct confd *qcomplexfile );
long enadev ( char *device_name );
long enaque ( char *queue_name );
void endgrpnam ( void );
void endmacnam ( void );
void endusenam ( void );
void enf_cpu_limit ( struct cpulimit *cpulimitp, short infinite, long limit_type );
void enf_quo_limit ( struct quotalimit *quotalimitp, short infinite, long limit_type );
void enf_nic_limit ( int nice_value );
int enqueue_set ( char *text, Mid_t machine_id, int uid_gid, short privileges );
long errnototcm ( void );
void errormessage ( int ecode );
int establish ( int packettype, Mid_t targetmid, uid_t cuid, char *cusername, long *transactcc );
void exiting ( void );
int extrasockch ( void );
long filecopyentire ( int from, int to );
long filecopysized ( int from, int to, long bytes );
char *fmtgidname ( gid_t gid );
char *fmtmidname ( Mid_t mid );
char *fmttime ( time_t *timeinsecs );
char *fmtuidname ( uid_t uid );
FILE *fopendata ( long orig_seqno, Mid_t orig_mid, int datano );
void freedb ( struct confd *file, long position );
void fsizedb ( struct confd *file );
void full_q ( struct confd *file, struct gendescr *queue, char * type, struct confd *qmapfile, struct confd *pipeqfile, struct confd *qcomplexfile );
char *getgrpnam ( gid_t gid );
void get_cmplx ( char * name, struct confd *qcomplexfile );
char *getenviron ( char *name );
char *getfilnam ( char *suffix, short dirtype );
int gethdr ( long orig_seqno, Mid_t orig_mid, struct rawreq *rawreq );
long getint ( char *ptr );
int getkmemdata ( char *buf, int bufsize, long n_value );
char *getmacnam ( Mid_t mid );
int getnusers ( void );
int getreq ( long orig_seqno, Mid_t orig_mid, struct rawreq *rawreq );
int getrreq ( long orig_seqno, Mid_t orig_mid, struct rawreq *rawreq );
int getsockch ( void );
char *getusenam ( uid_t uid );
char *getwdir ( void );
int get_host_id ( char *hname, Mid_t *itsmid );
char *get_literal ( void );
int handle_io ( void );
long hdlreq ( int oper, uid_t whomuid, int req_seqno, Mid_t req_mid );
void init_commandline( void );
int init_socket ( void );
long inter ( int packet_type );
void interclear ( void );
int interfmt ( char * );
int interlstr ( int string_i );
int intern32i ( void );
int internstr ( void );
long interr32i ( int i32_i );
int interr32sign ( int i32_i );
unsigned long interr32u ( int u32_i );
int interread ( int (*readfn)(void) );
char *interrstr ( int string_i );
int interscan ( char * );
void interset ( int fd );
void interw32i ( long i32 );
void interw32u ( unsigned long u32 );
void interwbytes ( char *bytes, unsigned n_bytes );
void interwstr ( char *string );
int is6bitstr (char *, int);
int isdecstr ( char *name, int len );
long l_verify_resources(struct quedescr *, struct rawreq *);
long ldbalsrv ( struct rawreq *rawreq, int debug );
void ldparam ( void );
int list ( struct confd *file, struct confd *qcomplexfile, char *name, long flags, char * selector, struct confd *qmapfile, struct confd *pipeqfile );
int listr ( struct confd *file, struct gendescr *que_descr, long flags, char * selector, char * req, short daemon );
int list_nqs_queues ( char *queptr, int count );
int localmid ( Mid_t *machine_id );
long locdae ( void );
int machacct ( char *account_name, Mid_t *machine_id );
int machine_name ( Mid_t mid, char *path );
int machpath ( char *path_name, Mid_t *machine_id );
int machspec ( char *text, Mid_t *machine_id );
int main_dsp ( int argc, char *argv[], long flags, char * selector, char * tname, char * hname );
int mai_outfiles ( struct rawreq *rawreq, struct requestlog *requestlog );
int mai_send ( struct rawreq *rawreq, struct requestlog *requestlog, int suffixcode );
struct passwd *mapuser ( uid_t fromuid, char *fromusername, Mid_t frommid, char *fromhostname, short mapbyuid);
int match ( char *p, char *t );
int matche ( char *p, char *t );
int matchkeyword ( char *maybekeyword, char *testmatch );
int memdump ( char *flags );
long mergertcm ( long rcm_original, long tcm_original );
int mkctrl ( struct rawreq *rawreq, int reqtype, FILE **ptrfile, char *workdir, Mid_t *local_mid );
int mkdata ( FILE **ptrfile );
void mkdefault ( char *cp, char *path, char *reqname, long seqno, char suffix_char );
int mklink ( char *datapath );
long modreq ( uid_t orig_uid, int req_seqno, Mid_t req_mid, int limit_type, long param_value1, long param_value2, short param_infinite );
void modreq_set ( int req_seqno, Mid_t orig_hostid, uid_t whomuid );
long movque ( char *src_qname, char *des_qname );
long movreq ( long orig_seqno, Mid_t orig_mid, char *dest_queue_name );
void movreq_set ( char *dest_qname );
int namop ( const char *name, Mid_t *mid, int mode );
char *namstderr ( long orig_seqno, Mid_t orig_mid );
char *namstdlog ( long orig_seqno, Mid_t orig_mid );
char *namstdout ( long orig_seqno, Mid_t orig_mid );
void new_command ( void );
struct gendescr *nextdb ( struct confd *file );
long nextseqno ( void );
int nqspriv ( uid_t real_uid, Mid_t user_mid, Mid_t local_mid );
long nqsshutdn ( int wait_time );
void nqssleep ( int timetosleep );
void nqs_aboque ( struct nqsqueue *queue, int grace_period, short requeue_flag );
void nqs_abort ( void );
long nqs_alterreq (uid_t uid, int seqno, Mid_t mid, int priority );
long nqs_autoinst ( void );
void nqs_boot ( void );
int nqs_broadcast ( struct rawreq *rawreq, struct requestlog *requestlog, int suffixcode );
int nqs_checkbal(uid_t);
void nqs_complt ( long completion_code, int pid );
int nqs_ctldae ( int action, int daemon );
long nqs_delreq ( uid_t mapped_uid, long orig_seqno, Mid_t orig_mid, Mid_t target_mid, int sig, int state );
void nqs_delrfs ( long orig_seqno, Mid_t orig_mid, short ndatafiles );
void nqs_deque ( struct request *request );
void nqs_disreq ( struct request *request, short deleteflag );
int nqs_enfile ( void );
void nqs_failed ( long orig_seqno, Mid_t orig_mid );
void nqs_family ( int shepherd_pid, int process_family );
struct qcomplex *nqs_fndcomplex ( char *comname );
struct nqsqueue *nqs_fndnnq ( char *quename );
struct request *nqs_fndreq ( long orig_seqno, Mid_t orig_mid, struct request **predecessor, int *state );
int nqs_get_scheduler ( void );
nqs_limittable * nqs_getlimittable(int);
BOOLEAN nqs_getsyslimit(int, sal_syslimit *);
long nqs_holdreq ( uid_t mapped_uid, long orig_seqno, Mid_t orig_mid );
void nqs_ldconf ( void );
long nqs_load ( Mid_t mid, int version, int jobs, char *string );
long nqs_memdump ( int dump_flag );
long nqs_modreq ( uid_t mapped_uid, long orig_seqno, Mid_t orig_mid, long modtype, long param1, long param2, int inf );
long nqs_movque ( char *from_que, char *to_que );
long nqs_movreq ( long orig_seqno, Mid_t orig_mid, char *destque );
long nqs_quereq ( int mode, char *ctrlname, long orig_seqno, Mid_t orig_mid, char *dest, Mid_t frommid );
void nqs_rbuild ( void );
long nqs_relreq ( uid_t mapped_uid, long orig_seqno, Mid_t orig_mid );
#if     IS_POSIX_1 | IS_SYSVr4
void nqs_reqcom ( long orig_seqno, Mid_t orig_mid, int exitcode, struct tms *tms, char *queuename );
#else
#if     IS_BSD
void nqs_reqcom ( long orig_seqno, Mid_t orig_mid, int exitcode, struct rusage *rusage, char *queuename );
#else
 BAD SYSTEM TYPE
#endif
#endif
void nqs_reqexi ( struct request *request, struct transact *transaction, struct rawreq *rawreq, struct requestlog *requestlog, struct device *device );
void nqs_reqser ( struct request *request, struct rawreq *rawreq, int restartflag, struct nqsqueue *queue, struct device *device, struct passwd *passwd );
void nqs_rreqcom ( Mid_t client_mid, Mid_t req_mid, int req_seqno, int no_jobs );
int  nqs_RunPrologueScript(char *);
int  nqs_RunEpilogueScript(char *);
void nqs_spawn ( struct request *request, struct device *device );
long nqs_suspendreq ( uid_t mapped_uid, long orig_seqno, Mid_t orig_mid, Mid_t target_mid, int privs, int do_suspend );
void nqs_valarm ( int );
void nqs_vtimer ( time_t *ptrvtime, void (*fn_to_call)(void) );
void nqs_wakdes ( void );
void nqs_wakreq ( void );
long nsq_enque ( int mode, int cfd, struct rawreq *rawreq, int wasexe, Mid_t frommid, time_t prearrive );
void nsq_spawn ( void );
void nsq_unque ( void );
int opendata ( long orig_seqno, Mid_t orig_mid, int datano );
struct confd *opendb ( char *name, int flags );
int openqacc ( struct confd *queuefile, struct gendescr *descr );
int openqord ( struct confd *queuefile, struct gendescr *descr );
int open_machine ( Mid_t mid, int access );
int open_name_map ( int access );
void pack6name ( char *name, char *dir, int subdirno, char *prefix, long long1, int parcels1, long long2, int parcels2, int salt, int saltparcels );
long parse_args ( int *argc, char *argv[], char * string, char ** selector, char ** tname, char ** hname );
int parseserv ( char *servercmd, char *argv [63 +1] );
char *pipeqdest ( int destno, Mid_t *destmid, long *retrytime );
int pipeqenable ( long code );
void pipeqexitifbad ( long code );
long pipeqfailinfo ( long code );
long pipeqreq ( long destmid, char *dest, struct rawreq *rawreq, int cuid, char *cusername, int *sd );
int pipeqretry ( long code );
long pip_reqreceived ( long orig_seqno, Mid_t orig_mid );
long pip_rmtaccept ( long orig_seqno, Mid_t orig_mid );
long pip_rmtarrive ( long orig_seqno, Mid_t orig_mid );
long pip_rmtdepart ( long orig_seqno, Mid_t orig_mid );
long pip_rmtstasis ( long orig_seqno, Mid_t orig_mid );
long pip_schedulereq ( long orig_seqno, Mid_t orig_mid, long waittime );
void pip_timeout ( void );
int psc_compare (struct nqsqueue *, struct request *, struct request *);
void psc_reqcom ( struct request *request );
void psc_resort (void);
int psc_sched ( struct rawreq *rawreq );
void psc_spawn ( void );
long purque ( char *que_name );
void qcmplx_hdr ( void );
int qdel_by_req ( char *req_pat, uid_t real_uid, short sig, int confirm );
void qdel_diagdel ( long code, char *reqid );
void qstat_hdr ( long flags );
void qstat_q ( struct gendescr *que_descr, short dae_present );
long quereq ( long *transaction_code );
long quereqvlpq ( long orig_seqno, Mid_t orig_mid, char *destq );
int quit_monitor ( void );
int quolimhigh ( unsigned long coeff, short units, short infinite, int limit_type, unsigned long *new_coeffp, short *new_unitsp );
int quolimlow ( unsigned long coeff, short units, int limit_type, unsigned long *new_coeffp, short *new_unitsp );
void rcimsgs ( long code, sal_debug_msg_t, char *prefix, void (*headerfn)(sal_debug_msg_t, char *) );
void rcmmsgs ( long code, sal_debug_msg_t, char *prefix );
struct gendescr *readdb ( struct confd *file );
int readhdr ( int fd, struct rawreq *rawreq );
int readreq ( int fd, struct rawreq *rawreq );
double realtime ( void );
void relfilnam ( char *string );
long remqcomque ( char *que_name, char *qcom_name );
int reqspec ( char *text, long *orig_seqno, Mid_t *machine_id, Mid_t *target_mid );
void rewritedb ( struct confd *file, long position, struct gendescr *descr, int descrtype );
int scan ( void );
int scancpulim ( char *cpulimstr, struct cpulimit *cpulimp, short maxonly );
int scanquolim ( char *quolimstr, struct quotalimit *quolimp, short maxonly );
int scantime ( char *dateandtimestr, time_t *res_time, int default_mode );
char *scan_allinparens ( void );
char *scan_aname ( void );
char *scan_dename ( void );
int scan_destset ( void );
int scan_devset ( void );
char *scan_dfname ( void );
char *scan_dname ( void );
int scan_end ( void );
int scan_equals ( void );
void scan_error ( char *text );
char *scan_fname ( void );
int scan_forset ( void );
int scan_groupset ( void );
int scan_int ( long *ptr_to_long, long low_bound, long high_bound, int int_expected, int bounds_error );
char *scan_logname ( void );
int scan_mgrset ( void );
int scan_opaname ( long *ptr_to_long );
int scan_opdev ( char *devname );
int scan_opqueue ( char *quename );
int scan_opwait ( long *ptr_to_long, long default_value, long low_bound, long high_bound, int int_expected, int bounds_error );
char *scan_qcomname ( void );
char *scan_qname ( void );
int scan_qualifier ( char *qualif_set[] );
int scan_queset ( void );
int scan_reqidname ( long *req_seqno, Mid_t *req_mid );
int scan_reqmodset ( void );
int scan_reqset ( void );
char *scan_servername ( void );
int scan_string ( void );
int scan_userset ( void );
int scnctime ( char *dateandtimestr, time_t *res_time );
int scnftime ( char *dateandtimestr, time_t *res_time );
int scnptime ( char *dateandtimestr, time_t *res_time );
int secgrfir ( unsigned long first_quota, short first_units, unsigned long second_quota, short second_units );
void seekdb ( struct confd *file, long newposition );
void seekdbb ( struct confd *file, long position );
void serexit ( long, const char * );
long setbatpri ( int newpri );
long setbatque ( char *que_name );
long setcomrun ( char *qcom_name, int run_limit );
long setcomuser ( char *qcom_name, int user_limit );
long setdeblev ( int debug_level );
long setdestim ( long retrydelta );
long setdeswai ( long destwait );
long setdevfor ( char *dev_name, char *forms_name );
long setdevpri ( int newpri );
long setdevser ( char *dev_name, char *server_name );
long setfor ( char *form );
long setgbatlim ( int limit );
long setgblpip ( int pip_limit );
long setlife ( long lifetime );
long setdefloadint ( int interval );
long setloaddae ( char *load_daemon_name );
long setlogfil ( char *logfile_name );
long setmaxcop ( long max_copies );
long setmaxopr ( long maxretries );
long setmaxpsz ( long maxprintsize );
int setmtime ( char *path, unsigned long ulongval );
long setndfbat ( void );
long setndffor ( void );
long setndfpri ( void );
long setndp ( char *queue_name, int new_ndp );
long setnetcli ( char *network_client_name );
long setnetdae ( char *network_daemon_name );
long setnetser ( char *network_server_name );
long setnnedae ( void );
long setnoqueacc ( char *queue_name );
long setnoquechar ( char *queue_name, int item );
long setnqsmai ( uid_t nqs_mail_account );
long setnqsman ( void );
long setnqssched (Mid_t);
long setopewai ( long openwait );
long setpeertcm ( long code );
long setpipcli ( char *que_name, char *client_name );
long setpiponl ( char *queue_name );
long setppcore ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setppcput ( char *queue_name, unsigned long new_seconds, short new_ms, short infinite );
long setppdata ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setppmem ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setppnice ( char *queue_name, int new_nice );
long setpppfile ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setppstack ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setpptfile ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setppwork ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setprcput ( char *queue_name, unsigned long new_seconds, short new_ms, short infinite );
long setprifor ( char *formsname );
long setprique ( char *que_name );
long setprmem ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setprpfile ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setprtfile ( char *queue_name, unsigned long new_quota, short new_units, short infinite );
long setquechar ( char *queue_name, int item );
long setquedes ( char *local_queue, char *dest_queue, Mid_t dest_mid );
long setquedev ( char *queue_name, char *device_name );
long setquepri ( char *que_name, int new_priority );
long setquerun ( char *que_name, int run_limit );
long setqueusr ( char *que_name, int usr_limit );
long setservavail ( Mid_t server );
long setservperf ( Mid_t server, int perf );
long setshsfix ( char *shell_name );
long setshsfre ( void );
long setshslog ( void );
void setsockfd ( int fd );
long setunrqueacc ( char *queue_name );
int shoalldev ( struct confd *file );
int shoalllim ( struct confd *file, long flags );
int shoallque ( struct confd *file, long flags, uid_t whomuid, struct reqset *reqs, short daepresent, struct confd *qmapfile, struct confd *pipeqfile, int xrmt, struct confd *qcomplexfile );
int shodbydesc ( struct gendescr *descr );
int shodbyname ( struct confd *devicefile, char *fullname );
int sholbymach ( struct confd *paramfile, char *itsname, long flags );
int shoqbydesc ( struct confd *file, struct gendescr *que_descr, long flags, uid_t whomuid, struct reqset *requests, short dae_present, struct confd *qmapfile, struct confd *pipeqfile, struct confd *qcomplexfile );
int shoqbyname ( struct confd *queuefile, char *fullname, long flags, uid_t whomuid, struct reqset *reqs, short dae_present, struct confd *qmapfile, struct confd *pipeqfile, struct confd *qcomplexfile );
void shoqlims ( struct gendescr * que_descr );
void shoreql ( struct gendescr *, struct rawreq *);
int showcomplex ( struct confd *qfile, char * name, long flags, uid_t whomuid, struct reqset *reqset_ptr, short daepresent, struct confd *qmapfile, struct confd *pipeqfile, struct confd *qcomplexfile );
int showret ( int code );
void show_failed_prefix ( void );
int sighandler ( int signum );
int sizedb ( struct gendescr *descr, int descrtype );
int snap ( char *filename );
long staque ( char *que_name );
long stoque ( char *que_name );
int strchgen ( char *str, char ch, int len );
int strequ ( char *str1, char *str2 );
long suspendreq ( uid_t mapped_uid, long orig_seqno, Mid_t orig_mid, Mid_t target_mid, int privs, int action );
void tcimsgs ( long code, sal_debug_msg_t, char *prefix, void (*headerfn)(sal_debug_msg_t, char *) );
char *tcmident ( long code );
void tcmmsgs ( long code, sal_debug_msg_t, char *prefix );
long telldb ( struct confd *file );
int tid_allocate ( int reserved_tid, int request_is_external );
void tid_deallocate ( int trans_id );
int tra_allocate ( int tid, struct transact *inittransact, uid_t owner_uid, gid_t owner_gid );
int tra_read ( int tid, struct transact *transact );
int tra_release ( int tid );
int tra_setinfo ( int tid, struct transact *transact );
int tra_setstate ( int tid, struct transact *transact );
void udb_addfor ( char *forms );
void udb_addnqsman ( uid_t account_uid, Mid_t account_mid, int privilege_bits );
void udb_addquedes ( struct nqsqueue *queue, struct qdestmap *map );
void udb_addquedev ( struct qdevmap *map );
void udb_crecom ( struct qcomplex *qcomplex );
void udb_credes ( struct pipeto *dest );
void udb_credev ( struct device *device );
void udb_creque ( struct nqsqueue *queue );
void udb_delcom ( struct qcomplex *qcomplex );
void udb_deldes ( struct pipeto *dest );
void udb_deldev ( struct device *device );
void udb_delfor ( char *forms );
void udb_delnqsman ( uid_t account_uid, Mid_t account_mid );
void udb_delque ( struct nqsqueue *queue );
void udb_delquedes ( struct qdestmap *map );
void udb_delquedev ( struct qdevmap *map );
void udb_destination ( struct pipeto *dest );
void udb_device ( struct device *device );
void udb_genparams ( void );
void udb_netprocs ( void );
void udb_qaccess ( struct nqsqueue *queue );
int udb_qapack (int a,unsigned long b[],int c,struct acclist *d );
int udb_qopack (int a, struct qentry b[], int c, struct request *d, int e,short f );
void udb_qorder ( struct nqsqueue *queue );
void udb_quecom ( struct qcomplex *qcomplex );
void udb_queue ( struct nqsqueue *queue );
void udb_reqpgrp ( struct nqsqueue *queue, struct request *req );
void udb_setfor ( char *forms );
void udb_setlogfil ( void );
void udb_setnqsman ( void );
void udb_setservavail ( Mid_t server );
void udb_setservperf ( Mid_t server, int performance );
void udb_useqno ( long seqno );
long unldae ( void );
long upc_addquecom ( char *que_name, char *complx_name );
long upc_crecom ( char *complx_name );
long upc_delcom ( char *complx_name );
long upc_remquecom ( char *que_name, char *complx_name );
long upc_setcomlim ( char *complx_name, long limit );
long upc_setcomuserlim ( char *complx_name, long limit );
long upd_addquedes ( char *lquename, char *rquename, Mid_t rhost_mid );
struct pipeto *upd_credes ( char *rquename, Mid_t rhost_mid );
void upd_deldes ( struct pipeto *pipeto );
long upd_delquedes ( char *lquename, char *rquename, Mid_t rhost_mid );
long upd_destination ( char *rquename, Mid_t rhost_mid, int cmd, long retry_wait );
long upd_machine ( Mid_t rhost_mid, int cmd, long retry_wait );
long upd_setquedes ( char *lquename, char *rquename, Mid_t rhost_mid );
long upf_addfor ( char *newform );
long upf_delfor ( char *form );
int upf_valfor ( char *formname );
long upm_addnqsman ( uid_t account_uid, Mid_t account_mid, int privilege_bits );
long upm_delnqsman ( uid_t account_uid, Mid_t account_mid, int privilege_bits );
long upp_setdefbatque ( char *defbatque );
long upp_setdefprifor ( char *defprifor );
long upp_setdefprique ( char *defprique );
long upp_setgblbatlim ( long limit );
long upp_setgblpiplim ( long limit );
int upp_setgblrunlim ( void );
long upp_setlogfil ( char *newlogname );
long upq_aboque ( char *que_name, int wait_time );
long upq_addqueacc ( char *quename, unsigned long who );
long upq_creque ( char *quename, int quetype, int quepriority, int quendp, int querunlimit, int pipeonly, int queusrlimit, char *servername, int queldb );
long upq_delque ( char *quename );
long upq_delqueacc ( char *quename, unsigned long who );
long upq_disque ( char *quename );
long upq_enaque ( char *quename );
long upq_lowaddacc ( struct nqsqueue *queue, unsigned long who );
long upq_purque ( char *quename );
long upq_setcpulim ( char *quename, unsigned long seconds, short ms, short infinite, long limit_type );
long upq_setniclim ( char *quename, int nice_value );
long upq_setnoqueacc ( char *quename );
long upq_setnoquechar ( char *quename, int item );
long upq_setpipcli ( char *quename, char *clientname );
long upq_setquechar ( char *quename, int item );
long upq_setquendp ( char *quename, int priority );
long upq_setquepri ( char *quename, int priority );
long upq_setquerun ( char *quename, int run_limit );
long upq_setqueusr ( char *quename, int usr_limit );
long upq_setquolim ( char *quename, unsigned long coeff, short units, short infinite, long limit_type );
long upq_setunrqueacc ( char *quename );
long upq_staque ( char *quename );
long upq_stoque ( char *quename );
long ups_shutdown ( int pid, uid_t ruid, int grace_period );
long upserv_setavail ( Mid_t server );
long upserv_setperf ( Mid_t server, int performance );
long upv_addquedev ( char *quename, char *devname );
long upv_credev ( char *devname, char *formsname, char *fullname, char *servername );
long upv_deldev ( char *devname );
long upv_delquedev ( char *quename, char *devname );
long upv_disdev ( char *devname );
long upv_enadev ( char *devname );
long upv_setdevfor ( char *devname, char *formsname );
long upv_setdevser ( char *devname, char *servername );
long upv_setquedev ( char *quename, char *devname );
int verifyhdr ( struct rawreq *rawreq );
int verifyreq ( struct rawreq *rawreq );
long verify_resources ( struct nqsqueue *queue, struct rawreq *rawreq );
void v_aboque ( void );
void v_aboreq ( void );
void v_adddes ( void );
void v_adddev ( void );
void v_addfor ( void );
void v_addgid ( void );
void v_addman ( void );
void v_addque ( void );
void v_adduid ( void );
void v_crebatque ( void );
void v_crecom ( void );
void v_credev ( void );
void v_credevque ( void );
void v_crepipque ( void );
void v_delcom ( void );
void v_deldes ( void );
void v_deldev ( void );
void v_delfor ( void );
void v_delgid ( void );
void v_delman ( void );
void v_delque ( void );
void v_delreq ( void );
void v_deluid ( void );
void v_disdev ( void );
void v_disque ( void );
void v_enadev ( void );
void v_enaque ( void );
void v_exi ( void );
void v_holdreq ( void );
void v_locdae ( void );
void v_memdump ( void );
void v_modreq ( void );
void v_movque ( void );
void v_movreq ( void );
void v_purque ( void );
void v_relreq ( void );
void v_remque ( void );
void v_setcomrunlim ( void );
void v_setcomuserlim ( void );
void v_setdeb ( void );
void v_setecho ( void );
void v_setdefbatpri ( void );
void v_setdefbatque ( void );
void v_setdefdestim ( void );
void v_setdefdeswai ( void );
void v_setdefdevpri ( void );
void v_setdefloadint ( void );
void v_setdefprifor ( void );
void v_setdefprique ( void );
void v_setdes ( void );
void v_setdev ( void );
void v_setdevser ( void );
void v_setfor ( void );
void v_setgbatlim ( void );
void v_setgblpiplim ( void );
void v_setlbin ( void );
void v_setlbout ( void );
void v_setliftim ( void );
void v_setloaddae ( void );
void v_setlogfil ( void );
void v_setmai ( void );
void v_setman ( void );
void v_setmaxcop ( void );
void v_setmaxoperet ( void );
void v_setmaxprisiz ( void );
void v_setndfbatque ( void );
void v_setndfprifor ( void );
void v_setndfprique ( void );
void v_setndp ( void );
void v_setnetcli ( void );
void v_setnetdae ( void );
void v_setnetser ( void );
void v_setnloaddae ( void );
void v_setnnedae ( void );
void v_setnoacc ( void );
void v_setnolbin ( void );
void v_setnolbout ( void );
void v_setnopipeonly ( void );
void v_setnoscheduler ( void );
void v_setopewai ( void );
void v_setpipcli ( void );
void v_setpipeonly ( void );
void v_setppcore ( void );
void v_setppcput ( void );
void v_setppdata ( void );
void v_setppmem ( void );
void v_setppnice ( void );
void v_setpppfile ( void );
void v_setppstack ( void );
void v_setpptfile ( void );
void v_setppwork ( void );
void v_setprcput ( void );
void v_setpri ( void );
void v_setprmem ( void );
void v_setprpfile ( void );
void v_setprtfile ( void );
void v_setrunlim ( void );
void v_setscheduler ( void );
void v_setservavail ( void );
void v_setservnoperf ( void );
void v_setservperf ( void );
void v_setshsfix ( void );
void v_setshsfre ( void );
void v_setshslog ( void );
void v_setunracc ( void );
void v_setusrlim ( void );
void v_shoall ( void );
void v_shodev ( void );
void v_shofor ( void );
void v_sholim ( void );
void v_sholongque ( void );
void v_shoman ( void );
void v_shopar ( void );
void v_shoque ( void );
void v_shoservers ( void );
void v_shutdown ( void );
void v_snap ( void );
void v_staque ( void );
void v_startload ( void );
void v_startnetdae ( void );
void v_startnqs ( void );
void v_stoload ( void );
void v_stonetdae ( void );
void v_stoque ( void );
void v_unldae ( void );
int writehdr ( int fd, struct rawreq *rawreq );
int writereq ( int fd, struct rawreq *rawreq );
void zapreq ( void );

#if	ADD_TAMU | ADD_NQS_TMPDIR
void	nqs_CreateTmpdir (struct rawreq *, struct passwd *, char *, char **, int, int *);
void	nqs_RemoveTmpdir (struct rawreq *);
#endif /* ADD_TAMU | ADD_NQS_TMPDIR */

#endif /* __NQS_PROTO_HEADER */
