/*
 * libnqs/nqsacct.h
 */

#include <SETUP/General.h>
#define NQSACCT_INIT	1		/* job initiation record */
#define NQSACCT_FIN	2		/* job terminiation record */
#define NQSACCT_STARTUP 3		/* NQS startup record */
#define NQSACCT_SHUTDOWN 4		/* NQS shutdown record */
#define NQSACCT_FIN_1	5		/* job terminiation record -- new style */
#define NQSACCT_STAT	6		/* job status record */
#define NQSACCT_INIT_1	7		/* job initiation record -- new style */
#define NQSACCT_SUMM	8		/* Summary record */

/*
 *	NQS account file record header.
 */
struct	nqsacct	{
	int	type;			/* type of record */
	int	length;			/* length of record (bytes) */
	int	jobid;			/* UNICOS job id */
};

/*
 *	NQS account file record for job initiation.
 */
struct	nqsacct_init {
	struct	nqsacct h;      /* NQS account record header */
	char	user[16];		/* user name */
	char	queue[16];		/* NQS queue name */
	int	priority;		/* priority of NQS queue */
	time_t	sub_time;		/* time submitted */
	time_t	start_time;		/* requested start time "-a" */
	time_t	init_time;		/* time initiated */
	Mid_t	orig_mid;		/* originating machine ID */
	char	scp_tid[8];		/* station TID */
	char	scp_mf[2];		/* station mainframe ID */
	long	seqno;			/* original sequence number */
};
/*
 *	New style NQS account file record for job initiation.
 */
struct	nqsacct_init1 {
	struct	nqsacct h;      /* NQS account record header */
	char	user[16];		/* user name */
	char	queue[MAX_QUEUENAME+1];	/* NQS queue name */
	int	priority;		/* priority of NQS queue */
	time_t	sub_time;		/* time submitted */
	time_t	start_time;		/* requested start time "-a" */
	time_t	init_time;		/* time initiated */
	Mid_t	orig_mid;		/* originating machine ID */
	long	seqno;			/* original sequence number */
	char    reqname[MAX_REQNAME+1]; /* request name */
};

/*
 * 	NQS account file record for job termination.
 */
struct 	nqsacct_fin {
	struct	nqsacct h;		/* NQS account record header */
	char	user[16];		/* user name */
	char 	queue[16];		/* NQS queue name */
	long	seqno;			/* Original sequence number */
#if    IS_POSIX_1 | IS_SYSVr4
	long	tms_stime;		/* Time spent by system for this job */
	long	tms_utime;		/* Time spent by user for this job */
#else
#if	IS_BSD
	long	s_sec;			/* System time in seconds */
	long	s_usec;			/* System time in microseconds */
	long	u_sec;			/* User time in seconds */
	long	u_usec;			/* User time in microseconds */
#else
BAD SYSTEM TYPE
#endif
#endif
	Mid_t	orig_mid;		/* Originating Machine ID */
};
/*
 * 	NQS account file record for job termination -- new style
 */
struct 	nqsacct_fin1 {
	struct	nqsacct h;		/* NQS account record header */
	char	user[16];		/* user name */
	char 	queue[16];		/* NQS queue name */
	long	seqno;			/* Original sequence number */
#if    IS_POSIX_1 | IS_SYSVr4
	long	tms_stime;		/* Time spent by system for this job */
	long	tms_utime;		/* Time spent by user for this job */
#else
#if	IS_BSD
	long	s_sec;			/* System time in seconds */
	long	s_usec;			/* System time in microseconds */
	long	u_sec;			/* User time in seconds */
	long	u_usec;			/* User time in microseconds */
#else
BAD SYSTEM TYPE
#endif
#endif
	Mid_t	orig_mid;		/* Originating Machine ID */
	time_t	fin_time;		/* Time completed */
};
/*
 *	NQS account file record for startup.
 */
struct	nqsacct_startup {
	struct	nqsacct h;		/* NQS account record header */
	time_t 	start_time;		/* Time nqs started up */
};
/*
 *	NQS account file record for shutdown.
 */
struct	nqsacct_shutdown {
	struct	nqsacct h;		/* NQS account record header */
	time_t 	down_time;		/* Time nqs shutdown */
};
/*
 * Summary record.
 */
struct nqsacct_summ {
	struct	nqsacct h;		/* NQS account record header */
        char    user[16];               /* user name */
        char    queue[MAX_QUEUENAME+1]; /* NQS queue name */
	char    reqname[MAX_REQNAME+1]; /* Request name */
        int     priority;               /* priority of NQS queue */
        time_t  sub_time;               /* time submitted */
        time_t  start_time;             /* requested start time "-a" */
        time_t  init_time;              /* time initiated */
        time_t  fin_time;               /* Time completed */
        Mid_t   orig_mid;               /* originating machine ID */
        long    seqno;                  /* original sequence number */
	long	u_time;			/* User time */
	long	s_time;			/* System time */
};

/*
 * This record type is only used internally by qacct.
 */
struct acct_record {
	struct acct_record *next;
        char    user[16];               /* user name */
        char    queue[MAX_QUEUENAME+1]; /* NQS queue name */
	char    reqname[MAX_REQNAME+1]; /* Request name */
        int     priority;               /* priority of NQS queue */
        time_t  sub_time;               /* time submitted */
        time_t  start_time;             /* requested start time "-a" */
        time_t  init_time;              /* time initiated */
        time_t  fin_time;               /* Time completed */
        Mid_t   orig_mid;               /* originating machine ID */
        long    seqno;                  /* original sequence number */
#if	IS_POSIX_1 | IS_SYSVr4
        long    tms_stime;              /* Time spent by system for this job */ 
	long    tms_utime;              /* Time spent by user for this job */
#else
#if     IS_BSD
        long    s_sec;                  /* System time in seconds */
        long    s_usec;                 /* System time in microseconds */
        long    u_sec;                  /* User time in seconds */
        long    u_usec;                 /* User time in microseconds */
#else
BAD SYSTEM TYPE
#endif
#endif
};

