#ifndef __NQS_TYPES_HEADER
#define __NQS_TYPES_HEADER

/*
 * nqs/types.h
 * Types for use inside Generic NQS
 */

#include <libnqs/internal/struct.h>
#include <libnqs/internal/mail.h>

#endif /* __NQS_TYPES_HEADER */
