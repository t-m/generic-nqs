/*
 *	Network Queueing System (NQS)
 *  This version of NQS is Copyright (C) 1992  John Roman
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 1, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
*  PROJECT:     Network Queueing System
*  AUTHOR:      John Roman
*
*  Modification history:
*
*       Version Who     When            Description
*       -------+-------+---------------+-------------------------
*       V01.10  JRR                     Initial version.
*       V01.20  JRR     28-Feb-1992	Added Cosmic V2 changes.
*       V01.3   JRR     07-Apr-1992     Added CERN enhancments.
*	V01.4	JRR	17-Jun-1992	Added header.
*	V01.5   JRR	19-Nov-1992	Added load definitions.
*	V01.6   JRR	11-Aug-1993	Added EM_NOSUCHSHELL.
*	V01.7	JRR	06-Apr-1994	Rating compute servers.
*/
/*++ nqsmgr.h - Network Queueing System
 *
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/libnqs/all-systems/nqsmgr.h,v $
 *
 * DESCRIPTION:
 *
 *	NQS Queue manager program (Qmgr) include definitions.
 *
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 *
 *
 * STANDARDS VIOLATIONS:
 *   None.
 *
 * REVISION HISTORY: ($Revision: 1.1.1.1 $ $Date: 1999/01/16 12:30:15 $ $State: Exp $)
 * $Log: nqsmgr.h,v $
 * Revision 1.1.1.1  1999/01/16 12:30:15  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:12  nqs
 * Imported sources
 *
 * Revision 1.7  1994/09/02  17:37:20  jrroma
 * Version 3.36
 *
 * Revision 1.6  93/09/10  13:53:21  jrroma
 * Version 3.35
 * 
 * Revision 1.5  92/12/22  15:45:22  jrroma
 * Version 3.30
 * 
 * Revision 1.4  92/06/18  09:46:24  jrroma
 * Added gnu header
 * 
 * Revision 1.3  92/05/06  10:02:48  jrroma
 * *** empty log message ***
 * 
 * Revision 1.2  92/02/28  10:57:39  jrroma
 * Added Cosmic V2 Changes.
 * 
 * Revision 1.1  92/02/28  10:54:21  jrroma
 * Initial revision
 * 
 *
 */

/*
 *
 *	Configurable parameters.
 */
#define	MAX_LINESIZE	511		/* Max length of a single cmd line */

/*
 *
 *	Token types.
 */
#define	T_ABORT		0		/* Abort command parsing */
#define	T_COMMA		1		/* , seen */
#define	T_EOC		2		/* End of command reached */
#define	T_EOF		3		/* EOF on stdin */
#define	T_EQUALS	4		/* = seen */
#define	T_LITERAL	5		/* Character literal scanned */
#define	T_LPAREN	6		/* ( seen */
#define T_NEG		7		/* - seen */
#define	T_RPAREN	8		/* ) seen */
#define	T_UINT		9		/* Unsigned integer scanned */

/*
 *
 *	Operation types on a set.
 */
#define	ADD_OP		0		/* Add */
#define	DEL_OP		1		/* Delete */
#define	SET_OP		2		/* Set */

/*
 *
 *	NQS syntax and parse Error Message types.
 */
#define	EM_ACCNAMEXP		0	/* Account name expected */
#define	EM_ACCNAMTOOLON		1	/* Account name too long */
#define	EM_ACCSPEEXP		2	/* Account specifier expected */
#define	EM_ACCSPEORLEFPAREXP	3	/* Account specifier or '(' expected */
#define	EM_AMBQUA		4	/* Ambiguous qualifier */
#define EM_BADCPULIMVAL		5	/* Overflow or 10:61 */
#define EM_BADQUOLIMVAL		6	/* Overflow or underflow */
#define EM_CPULIMEXP		7	/* Cpu time limit expected */
#define	EM_DEBVALOUTOFBOU	8	/* Debug value out of bounds */
#define	EM_DESNAMEXP		9	/* Destination name expected */
#define	EM_DESNAMORLEFPAREXP	10	/* Destination name or "(" expected */
#define	EM_DESNAMTOOLON		11	/* Destination name too long */
#define	EM_DESTIMOUTOFBOU	12	/* Destination retry state */
					/* time limit out of bounds */
#define	EM_DESWAIOUTOFBOU	13	/* Destination retry wait time out of */
					/* bounds */
#define	EM_DEVFULNAMEXP		14	/* Device full name expected */
#define	EM_DEVFULNAMTOOLON	15	/* Device full name too long */
#define	EM_DEVNAMEXP		16	/* Device name expected */
#define	EM_DEVNAMOREQUEXP	17	/* Device name or "=" expected */
#define	EM_DEVNAMORLEFPAREXP	18	/* Device name or "(" expected */
#define	EM_DEVNAMTOOLON		19	/* Device name too long */
#define	EM_EQUEXP		20	/* '=' expected */
#define	EM_FORNAMEXP		21	/* Form name expected */
#define	EM_FORNAMTOOLON		22	/* Form name too long */
#define	EM_GENNMAERR		23	/* General NMAP_ error */
#define	EM_GRPSPEORLEFPAREXP	24	/* Group specifier or '(' expected */
#define EM_GRPSPEEXP		25	/* Group specifier expected */
#define	EM_INPARENSEXP		26	/* Something expected within parens */
#define	EM_INPARENSTOOLON	27	/* String in parens too long */
#define	EM_INSNMAPRI		28	/* Insufficient NMAP privilege */
#define	EM_INTDEBVALEXP		29	/* Integer debug value expected */
#define	EM_INTDESTIMEXP		30	/* Integer dest retry limit expected */
#define	EM_INTDESWAIEXP		31	/* Integer dest retry wait time */
					/* expected */
#define	EM_INTLIFTIMEXP		32	/* Integer pipe queue req lifetime */
					/* limit expected */
#define	EM_INTMAXCOPEXP		33	/* Integer max print copies limit */
					/* expected */
#define	EM_INTMAXOPERETEXP	34	/* Max failed device open retry */
					/* limit expected */
#define	EM_INTMAXPRISIZEXP	35	/* Integer max print size limit */
					/* expected */
#define EM_INTNICEEXP		36	/* Integer nice value expected */
#define	EM_INTOPEWAIEXP		37	/* Integer failed device open */
					/* retry wait time expected */
#define	EM_INTPRIEXP		38	/* Integer priority expected */
#define	EM_INTRUNLIMEXP		39	/* Integer run-limit expected */
#define	EM_INTWAITIMEXP		40	/* Integer wait time expected */
#define	EM_INVACCSPESYN		41	/* Invalid account spec. syntax */
#define EM_INVCPULIMSYN		42	/* Invalid cpu time limit syntax */
#define	EM_INVDESSYN		43	/* Invalid destination syntax */
#define	EM_INVDEVFULNAMSPE	44	/* Invalid device full-name specified */
#define	EM_INVDEVNAMSPE		45	/* Invalid device name specified */
#define	EM_INVDEVSYN		46	/* Invalid device name syntax */
#define	EM_INVGRPSPESYN		47	/* Invalid group spec. syntax */
#define EM_INVQUOLIMSYN		48	/* Invalid quota limit syntax */
#define	EM_INVREQIDSYN		49	/* Invalid request-id syntax */
#define	EM_INVMANPRISYN		50	/* Invalid manager privilege syntax */
#define	EM_INVPRICLASPE		51	/* Invalid privilege class specified */
#define	EM_INVQUASPE		52	/* Invalid qualifier specified */
#define	EM_INVQUENAMSPE		53	/* Invalid queue name specified */
#define	EM_INVQUESYN		54	/* Invalid queue name syntax */
#define	EM_REQIDEXP		55	/* Request-id expected */
#define	EM_KEYEXP		56	/* Keyword was expected */
#define	EM_LEFPAREXP		57	/* "(" expected */
#define	EM_LIFTIMOUTOFBOU	58	/* Lifetime value out of bounds */
#define	EM_MACNAMSPEEXP		59	/* Machine-name expected */
#define	EM_MAXCOPOUTOFBOU	60	/* Max copies value out of bounds */
#define	EM_MAXOPERETOUTOFBOU	61	/* Max open retry limit out of */
					/* bounds */
#define	EM_MAXPRISIZOUTOFBOU	62	/* Max print size limit out of */
					/* bounds */
#define	EM_MULDESSET		63	/* Multiple destination set */
					/* specifications */
#define	EM_MULDEVSET		64	/* Multiple device set */
					/* specifications */
#define	EM_MULDEVFOR		65	/* Multiple device forms specified */
#define	EM_MULDEVFULNAM		66	/* Multiple device full name */
					/* specifications */
#define	EM_MULPRI		67	/* Multiple priority specifications */
#define	EM_MULRUNLIM		68	/* Multiple run-limit specifications */
#define	EM_MULSER		69	/* Multiple server specifications */
#define EM_NICEOUTOFBOU		70	/* Nice value out of bounds */
#define	EM_NODEVFORSPE		71	/* No device forms specified */
#define	EM_NODEVFULNAMSPE	72	/* No device full name specified */
#define	EM_NOMANOPEPRISPE	73	/* No manager or operator privileges */
					/* specified */
#define	EM_NOPRISPE		74	/* No priority specified */
#define	EM_NOSERSPE		75	/* No server specified */
#define	EM_NOSUCMAC		76	/* No such machine */
#define	EM_OPEWAIOUTOFBOU	77	/* Open retry wait time out of */
					/* bounds */
#define	EM_PATSPEEXP		78	/* Path specification expected */
#define	EM_PATSPETOOLON		79	/* Path specification too long */
#define	EM_PRIOUTOFBOU		80	/* Priority out of bounds */
#define	EM_QUENAMEXP		81	/* Queue name expected */
#define	EM_QUENAMTOOLON		82	/* Queue name too long */
#define EM_QUOLIMEXP		83	/* Quota limit expected */
#define	EM_REMACCCANTBESPEBYNAM	84	/* Remote account cannot be */
					/* specified by name */
#define	EM_RIGPAREXP		85	/* ")" expected */
#define	EM_RIGPARORCOMEXP	86	/* ")" or "," expected */
#define	EM_RUNLIMOUTOFBOU	87	/* Run-limit out of bounds */
#define	EM_UNECHAPASENDOFVALCOM	88	/* Unexpected characters past end */
					/* of otherwise valid command */
#define	EM_WAITIMOUTOFBOU	89	/* Wait time out of bounds */
#define EM_NDPOUTOFBOU		90	/* Nondegrading priority out of */
					/* bounds -- MONSANTO mod	*/		
#define EM_INTNDPEXP		91	/* Integer NDP expected -- Monsanto */
					/* mod				*/
#define EM_MULUSRLIM		92	/* Multiple user limits */
#define EM_INTUSRLIMEXP		93	/* Integer user limit expected */
#define EM_USRLIMOUTOFBOU	94	/* User limit out of bounds */
#define EM_INTGBATLIMEXP	95	/* Integer global batch limit exp */
#define EM_GBATLIMOUTOFBOU	96	/* Global batch limit out of bounds */
#define EM_INVREQSEQNOSYN       97      /* Invalid request sequence no syntax */
#define EM_INVQCOMNAMSPE        98      /* Invalid queue complex name specfd */
#define EM_MULMEMLIM            99      /* Multiple memory limits */
#define EM_MULNI               100      /* Multiple nice values */
#define EM_MULTIMLIM           101      /* Multiple time limits */
#define EM_NETMAPDBERR         102      /* Network map database error */
#define EM_NETMAPDBINACC       103      /* Network map database inaccessible */
#define EM_NOLIMPARSPE         104      /* No limit parameter specified */
#define EM_QCOMNAMEXP          105      /* Queue complex name expected */
#define EM_QCOMNAMTOOLON       106      /* Queue complex name too long */
#define EM_REQIDTOOLON         107      /* Request identifier too long */
#define EM_REQIDORLEFPAREXP    108      /* Reqst ident or left paren expected */
#define EM_INVKEYWORD          109      /* Invalid keyword */
#define EM_INVKEYSYN           110      /* Invalid keyword = keyvalue syntax */
#define EM_KEYDUP              111      /* Keyword duplicated */
#define EM_KEYVALEXP           112      /* Key value expected */
#define EM_SEQNOOUTOFBOU       113      /* Sequence number out of bounds */
#define EM_REQIDORRIGPAREXP    114      /* Request-id or right paren expected */
#define EM_INVLDBFLAGS         115      /* Invalid load balancing flags */
#define EM_NOSUCHSERVER        116      /* Could not find the server specified */
#define EM_MULSNAPFILE         117      /* Multiple snap files specified */
#define EM_NOSNAPFILE          118      /* No snap file specified */
#define EM_NQSRUNNING          119      /* NQS is already running */
#define EM_NOGOROOT            120      /* Cannot set uid to root */
#define EM_NQSNOTSTART         121      /* System call to start NQS failed */
#define EM_INTLOADINTEXP       122      /* Integer load interval expected */
#define EM_LOADINTOUTOFBOU     123      /* Load interval out of bounds */
#define EM_NOSUCHSHELL	       124      /* No such shell (set shell strategy fixed) */
#define EM_INTPERFEXP	       125	/* Integer performance expected */
#define EM_PERFOUTOFBOU        126	/* Performance out of bounds */
#define EM_SERVNAMEXP	       127	/* Server name expected */
#define EM_SERVNAMTOOLON       128	/* Server name too long */

#define EM_MAX		       128	/* Maximum EM error message */
