#ifndef __NQS_LICENSE_HEADER
#define __NQS_LICENSE_HEADER

/*
 * license.h
 * License information for the Monsanto-NQS source code.
 * 
 * This file is included by all other source code files in
 * Generic NQS.  It is to be modified ONLY by the current  
 * maintainer.
 */

/*
 * Generic NQS
 * The Network Queueing System (NQS)
 * 
 * Copyright (c) 1985 NASA
 * Copyright (c) 1992, 1993, 1994 John Roman, Monsanto Company
 * Copyright (c) 1994, 1995 The University of Sheffield
 * 
 * Portions are the copyright of individual contributors.
 * 
 * Work done by the University of Sheffield is funded by the Joint
 * Information Services Committee New Technologies Initiative grant
 * NTI/48.2, on behalf of all UK Higher Education sites.  More
 * information is available from :
 * 
 * 	http://www.shef.ac.uk/uni/projects/nqs/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This license is to be found in the file COPYING, which is in the
 * top directory of the source code distribution.
 */

#include <assert.h>

#endif /* __NQS_LICENSE_HEADER */

