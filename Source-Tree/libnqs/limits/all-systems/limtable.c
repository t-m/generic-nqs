/*
 * libnqs/limits/limtable.c
 * Translation table; from NQS supported limits to libsal limits.
 *
 * Original Author:
 * ================
 * Stuart Herbert
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libsal/license.h>
#include <libsal/defines.h>
#include <libsal/proto.h>

static nqs_limittable stLimitTable[] =
{
  /* nqs name		libsal name		qsub switch	Description	*/
  { LIM_PPCORE, 	SAL_LIMIT_CORE,		'c',		"Core file size limit"			},
  { LIM_PPCPUT, 	SAL_LIMIT_CPU,		't', 		"Per-process cpu time limit"		},
  { LIM_PRCPUT,		SAL_LIMIT_RCPU,		'T',		"Per-request cpu time limit"		},
  { LIM_PPDATA, 	SAL_LIMIT_DATA,		'd',		"Data segment size limit"		},
  { LIM_PPMEM,		SAL_LIMIT_VMEM,		'm',		"Per-process memory size limit"		},
  { LIM_PRMEM,		SAL_LIMIT_RVMEM,	'M',		"Per-request memory size limit"		},
  { LIM_PPNICE,		SAL_LIMIT_NICE,		'n',		"Nice value"				},
  { LIM_PPPFILE,	SAL_LIMIT_FSIZE,	'f',		"Per-process permanent file size limit"	},
  { LIM_PRPFILE,	SAL_LIMIT_RFSIZE,	'F',		"Per-request permanent file size limit" },
  { LIM_PPSTACK,	SAL_LIMIT_STACK,	's',		"Stack segment size limit"		},
  { LIM_PPWORK,		SAL_LIMIT_RSS,		'w',		"Working set limit"			},
  { 0,			0,			'\0',		NULL					}
};
  
nqs_limittable * nqs_getlimittable(int iLimitType)
{
  nqs_limittable *pstWalker = &stLimitTable[0];
  
  while (pstWalker->szDescription != NULL)
  {
    if (pstWalker->iNQSName == iLimitType)
      return pstWalker;
    pstWalker++;
  }
  return NULL;
}

BOOLEAN nqs_getsyslimit(int iLimitType, sal_syslimit *pstSyslimit)
{
  nqs_limittable *pstNQSLimit = nqs_getlimittable(iLimitType);
  
  assert(pstSyslimit != NULL);
  
  if (pstNQSLimit == NULL)
    return FALSE;
  
  pstSyslimit->stLimitInfo.iName = pstNQSLimit->iSALName;
  if (sal_syslimit_find(pstSyslimit))
    return TRUE;
  else
    return FALSE;
}
