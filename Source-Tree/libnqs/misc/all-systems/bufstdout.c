/*
 * libnqs/misc/bufstdout.c
 *
 * DESCRIPTION:
 *
 *	Block buffer the output to the stdout file stream.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 27, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <stdio.h>


/*** bufstdout
 *
 *
 *	void bufstdout():
 *	Block buffer the output to the stdout file stream.
 */
void bufstdout()
{
	static char stdout_buffer [BUFSIZ];	/* Stdout output buffer */
  	CALL_ONCE_ONLY;

#if	IS_POSIX_1 | IS_SYSVr4
  	setvbuf (stdout, stdout_buffer, _IOFBF, BUFSIZ);
#else
#if	IS_BSD
	setbuffer (stdout, stdout_buffer, BUFSIZ);
#else
BAD SYSTEM TYPE
#endif
#endif
}
