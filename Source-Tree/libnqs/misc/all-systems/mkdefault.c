/*
 * libnqs/misc/mkdefault.c
 * 
 * DESCRIPTION:
 *
 *	Manufacture a default output file name.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

/*** mkdefault
 *
 *
 *	void mkdefault():
 *	Make a default output file name.
 */
void mkdefault (
	register char *cp,		/* Destination for name */
	register char *path,		/* Path for file name */
	register char *reqname,		/* Req name */
	long seqno,			/* Req sequence number */
	char suffix_char)		/* Suffix character: 'e' or 'o' */
{
	register int i;

  	assert (cp      != NULL);
  	assert (path    != NULL);
  	assert (reqname != NULL);
  
	if (*path != '\0') {
		do {
			*cp++ = *path++;
		} while (*path);
		*cp++ = '/';
	}
	for (i=0; i < MAX_REQNAME && *reqname != '\0'; i++) {
		*cp++ = *reqname++;
	}
	*cp++ = '.';
	*cp++ = suffix_char;
	sprintf (cp, "%1ld", seqno);
}
