/*
 * libnqs/misc/destpath.c
 *
 * DESCRIPTION:
 *
 *	Return a pointer to the path-name portion of a remote
 *	path specification:  machine_name:path.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	November 5, 1985.
 */

#include <string.h>
#include <libnqs/license.h>
#include <libnqs/proto.h>			/* NQS types and definitions */

/*** destpath
 *
 *
 *	char *destpath():
 *
 *	Return a pointer to the path-name portion of a remote
 *	path specification:  machine_name:path.
 */
char *destpath (char *remote_path)
{
	static char pathname [MAX_REQPATH+2];
					/* Remote path name of one */
					/* extra byte so that caller can */
					/* tell if the path name is too */
					/* long. */
	register char *local_path;	/* Pointer to local path */
  
  	assert(remote_path != NULL);

	if ((local_path = strchr (remote_path, ':')) == (char *) 0) {
		local_path = remote_path;
	}
	else local_path++;
	strncpy (pathname, local_path, MAX_REQPATH+2);
	pathname [MAX_REQPATH+1] = '\0';	/* Paranoia */
	return (pathname);
}
