/*
 * libnqs/misc/isdecstr.c
 * 
 * DESCRIPTION:
 *
 *	Determine if a string of length n characters contains
 *	ASCII decimal digit characters and nothing but ASCII
 *	decimal digit characters.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

/*** isdecstr
 *
 *
 *	int isdecstr():
 *
 *	Returns:
 *		1: if the specified string of length n characters
 *		   contains decimal digits and nothing but decimal digits;
 *		0: otherwise.
 */
int isdecstr (char *name, int len)
{
  	assert (name != NULL);
  
	if (len <= 0) return (0);
	while (len && *name >= '0' && *name <= '9') {
		len--;
		name++;
	}
	if (len == 0) return (1);
	return (0);
}
