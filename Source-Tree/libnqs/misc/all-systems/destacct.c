/*
 * libnqs/misc/destacct.c
 * 
 * DESCRIPTION:
 *
 *	Return a pointer to the account-name portion of a remote
 *	account specification:  account@machine_name.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	November 5, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

/*** destacct
 *
 *
 *	char *destacct():
 *
 *	Return a pointer to the account-name portion of a remote
 *	account specification:  account@machine_name.
 */
char *destacct (char *remote_account)
{
	static char acctname [MAX_ACCOUNTNAME+2];
					/* Remote account name of one */
					/* extra byte so that caller can */
					/* tell if the account name is too */
					/* long. */
	register char *copyto;		/* Copyto pointer */
	register unsigned size;		/* Copy size */

  	assert (remote_account != NULL);
  
	size = 0;			/* Nothing copied yet */
	copyto = acctname;		/* Where to copy */
	while (*remote_account && *remote_account != '@' &&
	       size < MAX_ACCOUNTNAME+1) {
		*copyto++ = *remote_account++;
		size++;			/* One more character copied */
	}				/* specification */
	*copyto = '\0';			/* Null terminate */
	return (acctname);		/* Return pointer to account name */
}
