/*
 * libnqs/misc/serexit.c
 * 
 * DESCRIPTION:
 *
 *	Server exit routine.  ALL NQS servers should call this routine
 *	WHENEVER they wish to exit.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <string.h>
#include <unistd.h>

/*** serexit
 *
 *
 *	void serexit():
 *
 *	Send a server-complete message to the shepherd process
 *	(parent of the server) on file descriptor 3, and exit(0).
 */
void serexit (long rcm, const char servermssg[256])
{
	struct servermssg ret_packet;

	ret_packet.rcm = rcm;	/* Store req/server completion message */
	if (servermssg == (char *)0) ret_packet.mssg [0] = '\0';
	else strcpy (ret_packet.mssg, servermssg);
	/*
	 *  If you change the file descriptor that the req/server
	 *  completion message is reported on, then you must also
	 *  change nqs_spawn.c.
	 */
	write (3, (char *) &ret_packet, sizeof (ret_packet));
	exit (0);		/* Exit */
}
