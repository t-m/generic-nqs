/*
 * libnqs/misc/grpnam.c
 * 
 * DESCRIPTION:
 *
 *	This module contains the two functions:  getgrpnam()  and
 *	endgrpnam() which return the group-name for the specified
 *	group-id, and discard the group-name cache respectively.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <malloc.h>
#include <string.h>
#include <libnmap/nmap.h>

static struct g_cache *newgroup ( char *name );

/*
 *	Configurable parameters.
 */
#define	MAX_G_CACHESIZE	50	/* We will remember this many group names */


/*
 *	Data structure definitions local to this module.
 */
struct g_cache {
	struct g_cache *prev;	/* Previous group name cache entry */
	struct g_cache *next;	/* Next group name cache entry */
	gid_t gid;		/* Group-id */
	char *name;		/* Group-name */
};


/*
 *	Variables which are global to this module but not public.
 */
static struct g_cache *g_set = (struct g_cache *) 0;
					/* Group-id/name cache */
static int g_count = 0;			/* # of group-id/name cache entries */


/*** getgrpnam
 *
 *
 *	char *getgrpnam():
 *
 *	Return the groupname for the specified group-id on the local
 *	machine.
 */
char *getgrpnam (gid_t gid)
{

	register struct g_cache *scan;	/* Current group cache entry */
	register struct g_cache *prev;	/* Previous group cache entry */
	register char *groupname;	/* Ptr to groupname for local gid */

	prev = (struct g_cache *) 0;
	scan = g_set;			/* Scan group cache */
	while (scan != (struct g_cache *) 0 && scan->gid != gid) {
		prev = scan;
		scan = scan->next;
	}
	if (scan == (struct g_cache *) 0) {
		/*
		 *  The group-id/name was not in the cache.
		 */
		groupname = fmtgidname (gid);	/* Format groupname for gid */
		if (g_count < MAX_G_CACHESIZE) scan = newgroup (groupname);
		while (scan == (struct g_cache *) 0 &&
		       prev != (struct g_cache *) 0) {
			/*
			 *  Discard the least recently used mapping and
			 *  try to add the new mapping to the head of
			 *  the mapping set.
			 */
			free (prev->name);	/* Dispose of LRU name part */
			scan = prev;
			prev = prev->prev;	/* Backup one entry */
			free ((char *) scan);	/* Dispose of LRU entry */
			g_count--;		/* One less entry */
			if (prev != (struct g_cache *) 0) {	/* Disconnect */
				prev->next = (struct g_cache *) 0;
			}
			else {			/* No more entries left */
				g_set = (struct g_cache *) 0;
			}
			scan = newgroup (groupname);
						/* Try to allocate new entry */
		}
		if (scan == (struct g_cache *) 0) {
			/*
			 *  Insufficient memory existed to add the mapping
			 *  cache entry.  g_set points to nothing.
			 */
			return (groupname);
		}
		/*
		 *  Add the new mapping to the head of the mapping cache.
		 */
		if (g_set != (struct g_cache *) 0) g_set->prev = scan;
		scan->prev = (struct g_cache *) 0;
		scan->next = g_set;
		g_set = scan;
		scan->gid = gid;			/* Save group-id */
		strcpy (scan->name, groupname);		/* Save group-name */
	}
	else {
		/*
		 *  The group-id/name pair has been found in the cache.
		 *  Move the entry to the front of the cache to keep track
		 *  of the least-recently used order of the cache entries.
		 */
		if (scan != g_set) {	/* If not already as the front.... */
			if (prev != (struct g_cache *) 0) {
				prev->next = scan->next;
			}
			if (scan->next != (struct g_cache *) 0) {
				scan->next->prev = prev;
			}
			scan->prev = (struct g_cache *) 0;
			scan->next = g_set;
			g_set = scan;
		}
	}
	return (scan->name);	/* Return ptr to groupname */
}


/*** endgrpnam
 *
 *
 *	void endgrpnam():
 *	Clear the group-id/name cache.
 */
void endgrpnam (void)
{
	register struct g_cache *walk;
	register struct g_cache *next;

	walk = g_set;
	while (walk != (struct g_cache *) 0) {
		next = walk->next;
		free (walk->name);
		free ((char *) walk);
		walk = next;
	}
	g_count = 0;			/* Zero group-id/name cache entries */
	g_set = (struct g_cache *) 0;
	sal_closegrdb();			/* Close the group database */
}


/*** newgroup
 *
 *
 *	struct g_cache *newgroup():
 *	Returns:
 *		A pointer to a new group-id/name cache entry if
 *		adequate heap space exists; otherwise a null ptr
 *		is returned.
 */
static struct g_cache *newgroup (char *name)
{
	register struct g_cache *new;

  	assert(name != NULL);
  
	if ((new = (struct g_cache *)
		   malloc (sizeof (struct g_cache))) != (struct g_cache *) 0) {
		/*
		 *  We successfully got a new cache entry.
		 *  Now try to allocate sufficient name space.
		 */
		if ((new->name = (char *) malloc (strlen (name) + 1)) == (char *) 0) {
			/*
			 *  Insufficient heap space for name.
			 */
			free ((char *) new);
			new = (struct g_cache *) 0;
		}
		else g_count++;	/* One more entry */
	}
	return (new);
}
