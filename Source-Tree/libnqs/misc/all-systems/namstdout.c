/*
 * libnqs/misc/namstdout.c
 * 
 * DESCRIPTION:
 *
 *	Return pointer to name of spooled stdout output file for
 *	a batch request.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxdirs.h>
#include <string.h>


/*** namstdout
 *
 *
 *	char *namstdout():
 *
 *	Return pointer to name of spooled stdout output file for
 *	a batch request.
 *
 *	NOTE:	The name is computed relative to the NQS root directory.
 */
char *namstdout (long orig_seqno, Mid_t orig_mid)
{
	static char path [48];		/* Pathname for file.  This size */
					/* may need to be increased.... */

	pack6name (path, Nqs_output, (int) (orig_seqno % MAX_OUTPSUBDIRS),
		  (char *) 0, (long) orig_seqno, 5, (long) orig_mid, 6, 0, 0);
	strcat (path, "o");
	return (path);
}
