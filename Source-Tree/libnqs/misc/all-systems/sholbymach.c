/*
 * libnqs/misc/showlbymach.c
 * 
 * DESCRIPTION:
 *
 *	Print on stdout the limits that are meaningful for a machine.
 *
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	January 13, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <netdb.h>			/* Network database header file; */
#include <libnqs/netpacket.h>		/* NQS remote message packets */
#include <unistd.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*** sholbymach
 *
 *
 *	int sholbymach():
 *
 *	Print on stdout the limits that are meaningful
 *	for the named machine.
 *
 *	Returns: 0 if output was produced.
 *		-1 if no output was produced.
 */
int sholbymach (
	struct confd *paramfile,	/* This pointer useful for */
					/* local case only */
	char *itsname,			/* The machine in question */
	long flags)			/* SHO_? */
{
	Mid_t itsmid;
	Mid_t mymid;
	struct hostent *ent;		/* Host table entry structure */
	struct passwd *passwd;          /* Whose request it is */
        int sd;                         /* Socket descriptor */
        short timeout;                  /* Seconds between tries */
        long transactcc;                /* Holds establish() return value */
	uid_t cuid;		    	/* Mapped owner user-id */
	int extrach;			/* Number of chs already read */
	short output;			/* Boolean */

  	assert (paramfile != NULL);
  	assert (itsname   != NULL);
  
	if ((ent = gethostbyname (itsname)) == (struct hostent *) 0) {
		fprintf (stderr, "Invalid hostname specification: ");
		fprintf (stderr, "%s.\n", itsname);
		return (0);
	}
	switch (nmap_get_mid (ent, &itsmid)) {
	    case NMAP_SUCCESS:		/* Successfully got machine-id */
		break;
	    case NMAP_ENOMAP:		/* What?  No mid defined! */
		fprintf (stderr, "Invalid hostname specification: ");
		fprintf (stderr, "%s.\n", itsname);
		return (0);
	    case NMAP_ENOPRIV:		/* No privilege */
		fprintf (stderr, "Unexpected error in nmap_get_mid(), ");
		fprintf (stderr, "machine = %s.\n", itsname);
		return (0);
	    default:
		fprintf (stderr, "Unexpected error in nmap_get_mid(), ");
		fprintf (stderr, "machine = %s.\n", itsname);
		return (0);
	}
	if (localmid (&mymid) != 0) {
		fprintf (stderr, "Unexpected error in localmid().\n");
		return (0);
	}
	if (itsmid == mymid) {
		if (telldb (paramfile) != -1) {	/* if we have read before, */
			seekdb (paramfile, 0L);	/* seek to beginning */
		}
		return (shoalllim (paramfile, flags));
	} else {
            cuid = getuid();
            if ((passwd = sal_fetchpwuid (cuid)) == (struct passwd *) 0) {
                fprintf (stderr, "Unable to determine caller's username\n");
                exit (1);
            }
	    sal_closepwdb();
	    interclear();
	    interw32i ((long) cuid);
            interwstr (passwd->pw_name);
	    interw32i ((long) flags);
            sd = establish (NPK_QLIMIT, itsmid, cuid, passwd->pw_name, 
				&transactcc);
            if (sd < 0) {
                if (sd == -2) {
                    /*
                     * Retry is in order.
                     */
                    timeout = 1;
                    do {
                        nqssleep (timeout);
                        interclear ();
                        interw32i ((long) cuid);
                        interwstr (passwd->pw_name);
                        interw32i ((long) flags);
                        sd = establish (NPK_QLIMIT, itsmid,
                             cuid, passwd->pw_name, &transactcc);
                        timeout *= 2;
                    } while (sd == -2 && timeout <= 16);
	            /*
	             *  Beyond this point, give up on retry.
	             */
	            if (sd < 0) {
	                analyzetcm (transactcc, SAL_DEBUG_MSG_WARNING, "");
			exit (0);
	            }
		} else {
		    /*
		     *  Retry would surely fail.
		     */
		     /*
		      * We seem to get TCMP_CONNBROKEN here for some reason.
		      * But it gets reported as "Undefined transaction code..."
		      * Hmmmm.
		      * printf("Transact = %o\n",  transactcc);
		      */ 
		     analyzetcm (transactcc, SAL_DEBUG_MSG_WARNING, "");
		     exit (0);
		}
	    }
	    /*
	     *  The server liked us.
	     */
	    output = -1;
	    /*
	     *  First, display any characters that
	     *  were already read inside establish().
	     */
	    if ((extrach = extrasockch ())) {
		output = 0;
		write (STDOUT, bufsockch (), extrach);
	    }
	    /*
	     *  Then display any characters that the server
	     *  may still want to send us.
	     */
	    if (filecopyentire (sd, STDOUT)) return (0);
	    else return (output);
        }
}
