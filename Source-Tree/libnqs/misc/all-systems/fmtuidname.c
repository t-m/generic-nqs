/*
 * libnqs/misc/fmtuidname.c
 * 
 * DESCRIPTION:
 *
 *	Return a pointer to the username of the account
 *	identified by the local user-id.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 29, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libnmap/nmap.h>		/* Mid_t (all OS's) */
					/* Uid_t and gid_t (if not BSD43) */

/*** fmtuidname
 *
 *
 *	char *fmtuidname():
 *
 *	Return a pointer to the username of the account
 *	identified by the local user-id.
 */
char *fmtuidname (uid_t uid)
{
	static char standinbuf [14];	/* Stand-in buffer */

	register char *name;		/* Pointer to name */
	register struct passwd *pw;	/* Password/account entry */

	pw = sal_fetchpwuid ((int) uid);	/* Get account entry */
	if (pw == (struct passwd *) 0) {
		/*
		 *  No such user!  Format a "stand-in" name.
		 */
		sprintf (standinbuf, "[%1d]", (int) uid);
		name = standinbuf;
	}
	else name = pw->pw_name;
	return (name);
}
