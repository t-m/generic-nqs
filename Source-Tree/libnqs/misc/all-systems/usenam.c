/*
 * libnqs/misc/usenam.c
 * 
 * DESCRIPTION:
 *
 *	This module contains the two functions:  getusenam()  and
 *	endusenam() which return the user-name account for the specified
 *	user-id, and discard the user-name cache respectively.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <malloc.h>
#include <string.h>
#include <libnmap/nmap.h>		/* Mid_t (all OS's) */
					/* Uid_t and gid_t (if not BSD43) */

static struct u_cache *newuser ( char *name );

/*
 *	Configurable parameters.
 */
#define	MAX_U_CACHESIZE	400	/* We will remember this many user names */


/*
 *	Data structure definitions local to this module.
 */
struct u_cache {
	struct u_cache *prev;	/* Previous user name cache entry */
	struct u_cache *next;	/* Next user name cache entry */
	uid_t uid;		/* User-id */
	char *name;		/* User-name */
};


/*
 *	Variables which are global to this module but not public.
 */
static struct u_cache *u_set = (struct u_cache *) 0;
					/* User-id/name cache */
static int u_count = 0;			/* # of user-id/name cache entries */


/*** getusenam
 *
 *
 *	char *getusenam():
 *
 *	Return the username for the specified user-id on the local
 *	machine.
 */
char *getusenam (uid_t uid)
{
	register struct u_cache *scan;	/* Current user cache entry */
	register struct u_cache *prev;	/* Previous user cache entry */
	register char *name;		/* Name for local user-id */

	prev = (struct u_cache *) 0;
	scan = u_set;			/* Scan user cache */
	while (scan != (struct u_cache *) 0 && scan->uid != uid) {
		prev = scan;
		scan = scan->next;
	}
	if (scan == (struct u_cache *) 0) {
		/*
		 *  The user-id/name was not in the cache.
		 */
		name = fmtuidname (uid);
		if (u_count < MAX_U_CACHESIZE) scan = newuser (name);
		while (scan == (struct u_cache *) 0 &&
		       prev != (struct u_cache *) 0) {
			/*
			 *  Discard the least recently used mapping and
			 *  try to add the new mapping to the head of
			 *  the mapping set.
			 */
			free (prev->name);	/* Dispose of LRU name part */
			scan = prev;
			prev = prev->prev;	/* Backup one entry */
			free ((char *) scan);	/* Dispose of LRU entry */
			u_count--;		/* One less entry */
			if (prev != (struct u_cache *) 0) {	/* Disconnect */
				prev->next = (struct u_cache *) 0;
			}
			else {			/* No more entries left */
				u_set = (struct u_cache *) 0;
			}
			scan = newuser (name);
						/* Try to allocate new entry */
		}
		if (scan == (struct u_cache *) 0) {
			/*
			 *  Insufficient memory existed to add the mapping
			 *  cache entry.  u_set points to nothing.
			 */
			return (name);
		}
		/*
		 *  Add the new mapping to the head of the mapping cache.
		 */
		if (u_set != (struct u_cache *) 0) u_set->prev = scan;
		scan->prev = (struct u_cache *) 0;
		scan->next = u_set;
		u_set = scan;
		scan->uid = uid;		/* Save user-id */
		strcpy (scan->name, name);	/* Save user-name */
	}
	else {
		/*
		 *  The user-id/name pair has been found in the cache.
		 *  Move the entry to the front of the cache to keep track
		 *  of the least-recently used order of the cache entries.
		 */
		if (scan != u_set) {	/* If not already as the front.... */
			if (prev != (struct u_cache *) 0) {
				prev->next = scan->next;
			}
			if (scan->next != (struct u_cache *) 0) {
				scan->next->prev = prev;
			}
			scan->prev = (struct u_cache *) 0;
			scan->next = u_set;
			u_set = scan;
		}
	}
	return (scan->name);	/* Return ptr to username */
}


/*** endusenam
 *
 *
 *	void endusenam():
 *	Clear the user-id/name cache.
 */
void endusenam ()
{
	register struct u_cache *walk;
	register struct u_cache *next;

	walk = u_set;
	while (walk != (struct u_cache *) 0) {
		next = walk->next;
		free (walk->name);
		free ((char *) walk);
		walk = next;
	}
	u_count = 0;		/* Zero user-id/name cache entries */
	u_set = (struct u_cache *) 0;
	sal_closepwdb();		/* Close the account/password database */
}


/*** newuser
 *
 *
 *	struct u_cache *newuser():
 *	Returns:
 *		A pointer to a new user-id/name cache entry if
 *		adequate heap space exists; otherwise a null ptr
 *		is returned.
 */
static struct u_cache *newuser (char *name)
{
	register struct u_cache *new;

	if ((new = (struct u_cache *)
		   malloc (sizeof (struct u_cache))) != (struct u_cache *) 0) {
		/*
		 *  We successfully got a new cache entry.
		 *  Now try to allocate sufficient name space.
		 */
		if ((new->name = (char *)malloc (strlen (name) + 1)) == (char *) 0) {
			/*
			 *  Insufficient heap space for name.
			 */
			free ((char *) new);
			new = (struct u_cache *) 0;
		}
		else u_count++;	/* One more entry */
	}
	return (new);
}
