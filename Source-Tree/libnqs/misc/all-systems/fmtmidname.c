/*
 * libnqs/misc/fmtmidname.c
 * 
 * DESCRIPTION:
 *
 *	Return a pointer to the principal name of the machine
 *	identified by the specified machine-id.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	April 29, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <sys/types.h>
#include <libnmap/nmap.h>		/* Include network mapping definitions */

/*** fmtmidname
 *
 *
 *	char *fmtmidname():
 *
 *	Return a pointer to the principal name of the machine
 *	identified by the specified machine-id.
 */
char *fmtmidname (Mid_t mid)
{
	static char standinbuf [14];	/* Stand-in buffer */

	register char *name;		/* Pointer to name */

	name = nmap_get_nam (mid);
	if (name == (char *) 0) {
		/*
		 *  No such machine!  Format a "stand-in" name.
		 */
		sprintf (standinbuf, "[%1lu]",  mid);
		name = standinbuf;
	}
	return (name);
}
