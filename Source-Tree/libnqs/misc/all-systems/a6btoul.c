/*
 * libnqs/a6btoul.c
 *
 * DESCRIPTION:
 *
 *
 *	This module contains the functions:
 *
 *		a6btoul(), and
 *		is6bitstr()
 *
 *	which respectively evaluate a 6-bit name returning the long
 *	integer value, and return the boolean truth or falsehood of
 *	the specified name as a 6-bit name.
 *
 *	For more information, see pack6name.c
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	November 19, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

static short ctoi64 [128] = {
	127, 127, 127, 127, 127, 127, 127, 127,
	127, 127, 127, 127, 127, 127, 127, 127,
	127, 127, 127, 127, 127, 127, 127, 127,
	127, 127, 127, 127, 127, 127, 127, 127,
	127, 127, 127, 127, 127, 127, 127, 127,
	127, 127, 127,   0, 127, 127, 127, 127,
	  1,   2,   3,   4,   5,   6,   7,   8,
	  9,  10, 127, 127, 127, 127, 127, 127,
	127,  11,  12,  13,  14,  15,  16,  17,
	 18,  19,  20,  21,  22,  23,  24,  25,
	 26,  27,  28,  29,  30,  31,  32,  33,
	 34,  35,  36, 127, 127, 127, 127,  37,
	127,  38,  39,  40,  41,  42,  43,  44,
	 45,  46,  47,  48,  49,  50,  51,  52,
	 53,  54,  55,  56,  57,  58,  59,  60,
	 61,  62,  63, 127, 127, 127, 127, 127
};


/*** a6btoul
 *
 *
 *	unsigned long a6btoul():
 *
 *	Evaluate the specified 6-bit alphabet name returning the
 *	unsigned long value.
 *
 *	WARNING:
 *		No check is made to enforce the requirement that
 *		the name be constructed from the appropriate 6-bit
 *		alphabet!  Use is6bitstr() if this is required.
 *
 */
unsigned long a6btoul (char *name, int len)
{
	register short ch;
	register unsigned long res;

  	assert(name != NULL);
	res = 0;
	while (len--) {
		ch = *name++;
		ch &= 0x7F;		/* Mask to 7-bits */
		res *= 64;
		res += ctoi64 [ch];
	}
	return (res);
}


/*** is6bitstr
 *
 *
 *	int is6bitstr():
 *
 *	Return non-zero (TRUE), if the specified string is a 6-bit
 *	alphabet name string.  Otherwise return zero (FALSE).
 */
int is6bitstr (char *name, int len)
{
	register short ch;

  	assert(name != NULL);
	if (len <= 0) return (0);	/* Definitely not a 6-bit */
					/* alphabet name string */
	while (len--) {
		ch = *name++;
		ch &= 0x7F;		/* Mask to 7-bits */
		if (ctoi64 [ch] == 127) {
			/*
			 *  The specified name is not a valid
			 *  6-bit alphabet name.
			 */
			return (0);
		}
	}
	/*
	 *  The specified name is a valid 6-bit alphabet name.
	 */
	return (1);
}
