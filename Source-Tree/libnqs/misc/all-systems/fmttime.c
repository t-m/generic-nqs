/*
 * libnqs/misc/fmttime.c
 * 
 * DESCRIPTION:
 *
 *	Format a time string.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <sys/types.h>			/* time_t */
#include <unistd.h>

/*** fmttime
 *
 *
 *	char *fmttime():
 *
 *	Return a pointer to a formatted time string where the time
 *	is specified as a long integer defining the number of seconds
 *	since 00:00:00 GMT, January 1, 1970.
 */
char *fmttime (time_t *timeinsecs)
{
	static char buffer [40];	/* Output buffer */
	char *tzn;			/* Time zone name */
	char *ts;			/* Time string */
	struct tm *tp;			/* Time structure pointer */

#if	GPORT_HAS_SYSV_TIME
	tzset();			/* Get local timezone */
  
  	assert(timeinsecs != NULL);
	tp = localtime (timeinsecs);
	ts = asctime (tp);
	tzn = tzname [tp->tm_isdst];
#else
	struct	timeval	tv;
	struct	timezone tz;

  	assert(timeinsecs != NULL);
	gettimeofday (&tv, &tz);
	tp = localtime (timeinsecs);
	ts = asctime (tp);
	tzn = timezone (tz.tz_minuteswest, tp->tm_isdst);
#endif
	if (tzn == (char *) 0) tzn = "";
	ts [24] = '\0';				/* Zap newline character */
	sprintf (buffer, "%.20s%s%s", ts, tzn, ts+19);
	return (buffer);
}
