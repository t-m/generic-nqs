/*
 * libnqs/misc/bufstderr.c
 * 
 * DESCRIPTION:
 *
 *	Block buffer the output to the stderr file stream.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 27, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <stdio.h>

/*** bufstderr
 *
 *
 *	void bufstderr():
 *	Block buffer the output to the stderr file stream.
 */
void bufstderr(void)
{
	static char stderr_buffer [BUFSIZ];	/* Stderr output buffer */
  	CALL_ONCE_ONLY;

#if	IS_POSIX_1 | IS_SYSVr4
  	setvbuf (stderr, stderr_buffer, _IOFBF, BUFSIZ);
#else
#if	IS_BSD
	setbuffer (stderr, stderr_buffer, BUFSIZ);
#else
BAD SYSTEM TYPE
#endif
#endif
}
