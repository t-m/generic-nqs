/*
 * libnqs/misc/mapuser.c
 * 
 * DESCRIPTION:
 *
 *	Given a user's identity on another host, return
 *	a pointer to that user's password file entry on this host.
 *
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	May 9, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <pwd.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

static int user_access ( char *accessfname, char *fromhostname, struct passwd *passwd);
static int local_finduser ( char *user, char *toname );
static int map_user_name ( char *machine, char *user, char *toname );
static int l_strcmp ( char *str1,  char *str2 );

/*** mapuser
 *
 *
 *	struct passwd *mapuser: Given a user's identity on another host,
 *	return a pointer to that user's password file entry on this host.
 *
 *	mapuser first checks the /etc/hosts.equiv file to see if access
 *	is permitted.  The format of the /etc/hosts.equiv file is
 *
 *	hostname username
 *	fqdn username
 *
 *	Both the hostname and fully qualified domain name may be required.
 *	
 *	If access is not permitted,  the user's .rhosts file is checked.
 *	Its format is the same as the /etc/hosts.equiv above.
 *
 *	If access is still not permitted,  the file /etc/hosts.nqs is
 *	checked.  The details are in the code below,  but at the simplest, 
 *	the file can be the same format at the .rhosts and /etc/hosts.equiv
 *	files.  It is also possible to get more complicated,  and map
 *	a username on a remote machine to a local machine.  An example
 *	is:
 *
 *	(beaker.monsanto.com model7.monsanto.com) jrroma=mgkreb
 *
 *	in this case user jrroma on beaker or model7 is mapped to username
 *	mgkreb on the local machine.
 *
 */
struct passwd *mapuser (
	uid_t fromuid,		/* Client's uid on remote machine */
	char *fromusername,	/* Client username on remote machine */
	Mid_t frommid,		/* Nmap machine id of remote machine */
	char *fromhostname,	/* PRINCIPAL name of remote machine */
	short mapbyuid)		/* Boolean */
{
	uid_t touid;			/* Mapped user-id */
	char dotrhostspath [256];	/* Users .rhost pathname */
	register struct passwd *passwd;	/* Password entry for account */
        static char toname[MAX_ACCOUNTNAME];
        short local_name_exists;

     	/*
	 *  The selected mapping algorithm operates by user-id.
	 */
	switch (nmap_get_uid (frommid, fromuid, &touid)) {
 		case NMAP_SUCCESS:
		case NMAP_DEFMAP:
			return (sal_fetchpwuid (touid));
	}
	/*
	 *  The selected mapping algorithm operates by name.
	 */
	passwd = sal_fetchpwnam (fromusername);
	local_name_exists = 0;
	if (passwd != (struct passwd *) 0) local_name_exists = 1;
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "fromusername >%s<\n",  fromusername);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "fromhostname >%s<\n",  fromhostname);
	if (local_name_exists)
          sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "pwd exists\n");
	else 
          sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "pwd not exist\n");
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW,  "fromuid %d\n", fromuid);
	if ( (fromuid != 0) && (strcmp(fromusername, "root")) ) {
	    /*
	     *  We are not mapping root.
	     *  Examine the /etc/hosts.equiv file for machine
	     *  equivalency.
	     */
	    if (user_access ("/etc/hosts.equiv", fromhostname,
			   (struct passwd *) 0) ) {
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "hosts.equiv allows access from %s\n", fromhostname);
	        return (passwd);	/* Access allowed */
	    }
	}
	/*
	 *  Determine if the user's account specifically allows access.
	 */
        if (local_name_exists) {
	    strcpy (dotrhostspath, passwd->pw_dir);
	    strcat (dotrhostspath, "/.rhosts");
	    if (user_access (dotrhostspath, fromhostname, passwd)) {
	      sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Access allowed for %s from %s\n", fromusername, fromhostname);
	      return (passwd);		/* Access allowed */
	    }
	}
        /*
         * Determine if /etc/hosts.nqs has a mapping
         */
	if ( (fromuid != 0) && (strcmp(fromusername, "root")) ) {
	    switch(map_user_name(fromhostname, fromusername, toname)) {
	    case 0:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "%s@%s -> %s\n", fromusername, fromhostname, toname);
		if ((passwd = sal_fetchpwnam(toname)) != (struct passwd *) 0)
		    return (passwd);
		break;
	    case -1:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Machine %s not found.\n", fromhostname);
		break;
	    case -2:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "User %s not found.\n", fromusername);
		break;
	    case -3:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Cannot open /etc/hosts.nqs\n");
		break;
	    }
	}
	return ((struct passwd *) 0);
}


/*** user_access
 *
 *
 *	int user_access():
 *
 *	Return non-zero if the specified Berkeley style access file
 *	grants access to the specified remote account.
 *
 *	If the password entry parameter:  passwd  is non-null, then
 *	the access equivalency file being scanned MUST contain lines
 *	of the form:
 *
 *		machinename username
 *
 *	and any such access equivalency file MUST be owned by the
 *	account identified by the given password entry.
 */
static int user_access (
	char *accessfname,		/* Access file name */
	char *fromhostname,		/* Name of client machine */
	register struct passwd *passwd)	/* Password file entry */
{
	struct stat statbuf;		/* Stat() buffer */
	char accesshost [256];		/* Access file line buffer */
	register FILE *accessfile;	/* Access file */
	register char *cp, *acp;

	accessfile = fopen (accessfname, "r");
	if (accessfile != (FILE *) 0) {
	    /*
	     * The access file was successfully opened.
	     */
	    if (passwd != (struct passwd *) 0) {
	        /*
	         *  Verify ownership of the .rhosts file, and ensure
	         *  that (on Berkeley systems), that the file is not
	         *  a symbolic link.
	         */
	        if (fstat (fileno (accessfile), &statbuf) == -1) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Fstat of %s failed with errno %d\n", accessfname, errno);
		    return (0);
	        }
	        if (statbuf.st_uid != passwd->pw_uid ||
		       (statbuf.st_mode & S_IFMT) == S_IFLNK) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Uids don't match, errno %d\n", errno);
		    return (0);
	        }
	    }
	    while (fgets (accesshost, sizeof (accesshost),
			      accessfile) != (char *) 0) {
		if (accesshost[0] == '#') continue;
		/*
		 *  We have an access file line to interpret.
		 */
		cp = strchr (accesshost, '\n');
		if (cp != (char *) 0) *cp = '\0';
		if (passwd != (struct passwd *) 0) {
		    /*
		     *  We are inspecting a user account
		     *  .rhost file.  A username field is
		     *  required.
		     */
		    cp = accesshost;
		    /* 
		     * The first token is the hostname.  Scan until
		     * reach whitespace.
		     */
		    while (*++cp && !isspace(*cp) ) {
			;
		    }
		    /*
		     * The second token is the username.  Scan until
		     * reach non-whitespace.
		     */
		    if (*cp) {
		        *cp = '\0';	    /* terminate hostname */
			while (*++cp && isspace(*cp) ) {
			    ;
			}
		    }
		    /*
		     * Scan past the second token until we reach
		     * whitespace or null.
		     */
		    acp = cp;
		    while (*++acp && !isspace(*acp) ) {
			;
		    }
		    if (*acp) *acp = '\0';		    
		    if (cp != (char *) 0) {
			/*
			 *  A username field is present
			 *  (as required).
			 */
			if (!l_strcmp (accesshost, fromhostname)&&
				 !strcmp (cp, passwd->pw_name)) {
					return (1);
			}
		    }
		}
		else if (!l_strcmp (accesshost, fromhostname)) {
		    /*
		     *  Client account is at an equivalent host.
		     */
		    return (1);
		}
#ifdef ADD_NQS_NIS
                 else if ((accesshost[0] == '+') ||
                          (accesshost[0] == '-')) {
                     int isplus;
                     /*
                      * check if the host or group is added or removed
                      */
                     cp = accesshost;
                     isplus = (cp[0] == '+');
                     cp++;
                     if (cp[0] == '@') {
                         /*
                          * it is not a hostname, it is a netgroup
                          */
                         char domain[65];
                         cp++;
                         getdomainname(domain, 65);
                         if (innetgr(cp, fromhostname, (char *) 0, domain)) {
                             return (isplus);
                         }
                     }
                     else if (!l_strcmp(cp, fromhostname)) {
                         return (isplus);
                     }
                 }
#endif /* NQS_NIS */
	    }
	}
	if (passwd != (struct passwd *) 0)
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "No access for user %s from %s\n", passwd->pw_name, fromhostname);
	else 
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "I$No access from %s\n",  fromhostname);
	return (0);			/* No access */
}
/*** map_user_name
 *
 *
 *
 *    int map_user_name()
 *
 *    Return non-zero if the /etc/hosts.nqs file
 *    grants access to the specified remote account
 */
static int map_user_name (
	char *machine,                /* remote machine name */
	char *user,                   /* remote user name */
	char *toname)                 /* mapped user name */
{
      FILE *accessfile;               /* hosts.nqs file */
      static char buffer[BUFSIZ];     /* hosts.nqs line buffer */
      char *cp;                       /* current line */
      char *paren;                    /* paren found */
      int found = 0;                  /* true if name found */

      accessfile = fopen ("/etc/hosts.nqs", "r");
      if (accessfile == (FILE *) 0) return(-3);
      while (fgets(buffer,sizeof(buffer),accessfile)) {
          cp = strchr(buffer, '\n');
          if (cp) *cp = '\0';
          cp = strtok(buffer," \t");
          switch (*cp) {
              case '(':       /* this is a list of machines */
                 cp++;
                  do {
                      paren = strchr(cp,')');
                      if (paren) *paren = '\0';
                      if (l_strcmp(machine, cp) == 0) {
                          found++;
                      }
                      if (!paren) cp = strtok((char *)0," \t");
                  } while (!paren);
                  if (found && local_finduser(user, toname)==0)
                              return(0);
                  break;
              case '*':
                  if (local_finduser(user, toname)==0)
                          return(0);
                  break;
              case '#':
              case '\n':

              case '\0':
                  break;
              default:
                  if (l_strcmp(machine, cp) == 0) {
                      if (local_finduser(user, toname)==0)
                              return(0);
                  }
                  break;
          }
      }
      return (-1);    /* not found */
}


/*** local_finduser
 *
 *
 *    int local_finduser();
 *
 *    Returns the mapped user in toname if the user is found
 *    in the current line
 *
 *    Returns
 *     0:     if the current line grants access
 *    -2:     if the user is not found
 */
static int local_finduser(char *user, char *toname)
{
      char *cp, *paren, *equal;
      int found = 0;
      int userlist = 0;

      while((cp = strtok(NULL, " \t"))) {
          userlist++;
          switch (*cp) {
          case '(':       /* is this a list of users? */
              cp++;
              do {
                  paren = strchr(cp,')');
                  if (paren) *paren = '\0';
                  if (strcmp(user, cp) == 0) found++;
                  if (!paren) cp = strtok((char *)0," \t");
              } while (cp && !paren);
              if (found) {
                  equal = strchr(paren+1,'=');
                  if (equal)
                       strcpy(toname,equal+1);
                  else
                       strcpy(toname,user);
                  return(0);
              }
              break;
          case '*':
              if (*(cp+1) == '=')
                  strcpy(toname,cp+2);
              else
                  strcpy(toname,user);
              return(0);
          default:
              equal = strchr(cp,'=');
              if (equal) *equal = '\0';
              if (strcmp(user, cp) == 0) {
                  if (equal)
                      strcpy(toname,equal+1);
                  else
                      strcpy(toname,user);
                  return(0);
              }
              break;
          }
      }
      if (!found && !userlist) {
          strcpy(toname,user);
          return(0);
      }
      else
         return(-2);
}
/* l_strcmp():
 * 
 * Do string comparison regardless of case.
 * 
 * Returns 0 if the same,  1 otherwise.
 */
static int l_strcmp(char *str1, char *str2)
{
    char c1, c2;
    char *ptr1, *ptr2;
 
    /*
     * If they are different lengths, automatically not the same.
     */
    if ( strlen(str1) != strlen(str2) ) return (1);
    ptr1 = str1;
    ptr2 = str2;
    while (*ptr1 != '\0') {
	c1 = *ptr1++;
	c2 = *ptr2++;
	if ( tolower(c1) != tolower(c2) ) return (1);
    }
    return (0);
}
