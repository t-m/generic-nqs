/*
 * libnqs/misc/fmtgidname.c
 * 
 * DESCRIPTION:
 *
 *	Return a pointer to the groupname of the group
 *	identified by the local group-id.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	May 25, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libnmap/nmap.h>		/* Mid_t (all OS's) */
					/* Uid_t and gid_t (if not BSD43) */
#include <grp.h>			/* Group database definitions */

/*
 *	External functions.
 */


/*** fmtgidname
 *
 *
 *	char *fmtgidname():
 *
 *	Return a pointer to the groupname of the group
 *	identified by the local group-id.
 */
char *fmtgidname (gid_t gid)
{
	static char standinbuf [14];	/* Stand-in buffer */

	register char *name;		/* Pointer to name */
	register struct group *group;	/* Group entry */

	group = sal_fetchgrgid ((int) gid);	/* Get group entry */
	if (group == (struct group *) 0) {
		/*
		 *  No such group!  Format a "stand-in" name.
		 */
		sprintf (standinbuf, "[%1d]", (int) gid);
		name = standinbuf;
	}
	else name = group->gr_name;
	return (name);
}
