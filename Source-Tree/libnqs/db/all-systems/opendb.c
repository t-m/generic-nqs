/*
 * libnqs/db/opendb.c
 * 
 * DESCRIPTION:
 *
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libsal/license.h>
#include <libsal/defines.h>
#include <libsal/proto.h>

#include <errno.h>
#include <sys/stat.h>
#include <malloc.h>
#include <unistd.h>
#define	MAX_CONFIGFILES	10		/* Maximum number of open configur- */
					/* ation files */


/*** opendb
 *
 *
 *	struct confd *opendb():
 *
 *	Open the named file of the NQS database.
 *	Returns:
 *		A pointer to the allocated NQS database file access
 *		structure if successful;
 *		Otherwise NULL is returned (errno has reason).
 */
struct confd *opendb (char *name, int flags)
{
	static short initialized = 0;	/* Tables not initialized */
	static struct confd confiles [MAX_CONFIGFILES];

	struct stat statbuf;		/* Fstat() buffer */
	register struct confd *file;	/* NQS database file descriptor */
	register int descr;

  	assert (name != NULL);
  
	if (!initialized) {
		/*
		 *  We have not yet initialized the configuration
		 *  file descriptor table.
		 */
		initialized = 1;	/* Set initialized flag */
		for (descr = 0; descr < MAX_CONFIGFILES; descr++) {
			confiles [descr].fd = -1;	/* Mark slot as free */
		}
	}
	descr = 0;		/* Search for a free confd descriptor */
	while (descr < MAX_CONFIGFILES && confiles [descr].fd != -1) {
		descr++;
	}
	if (descr >= MAX_CONFIGFILES) {
		/*
		 *  There is not a free confd file descriptor
		 *  to be found.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Too many files in opendb().\n");
		fflush (stdout);
		errno = 0;		/* Not a system call error */
		return (NULL);
	}
	file = &confiles [descr];	/* Get address of free descriptor */
	if (flags & O_CREAT) file->fd = open (name, flags & ~O_EXCL, 0644);
	else file->fd = open (name, flags);
	if (file->fd == -1) return (NULL);	/* The open failed */
	/*
	 *  The open succeeded.  Initialize confd file descriptor.
	 */
	if ((file->cache = (struct rawblock *)
			    malloc (sizeof (struct rawblock))) == NULL) {
		/*
		 *  Insufficient heap space to open database file.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "No heap space in opendb().\n");
		fflush (stdout);
		close (file->fd);	/* Close the file */
		file->fd = -1;		/* Free the descriptor */
		return (NULL);
	}
	file->vposition = 0;		/* Virtual file position */
	file->rposition = 0;		/* Real file position */
	file->lastread = -1;		/* No descriptor has even been read */
	fstat (file->fd, &statbuf);	/* Fstat() file */
	file->size = statbuf.st_size;	/* File size */
	if (file->size % ATOMICBLKSIZ) {
		/*
		 *  The file size is not a multiple of ATOMICBLKSIZ bytes.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Invalid file size in opendb().\n");
		fflush (stdout);
		close (file->fd);	/* Close the file */
		file->fd = -1;		/* Free the descriptor */
		return (NULL);
	}
	/*
	 *  Nature abhors a vacuum.  Load the first block of the file
	 *  into the cache (if the file has 1 or more blocks).
	 */
	if (file->size) {
		errno = 0;		/* No system call error on partial */
					/* reads please */
		if (read (file->fd, file->cache->v.chars,
			  ATOMICBLKSIZ) != ATOMICBLKSIZ) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Read error in opendb().\n");
			fflush (stdout);
			close (file->fd);	/* Close the file */
			file->fd = -1;		/* Free the descriptor */
			return (NULL);
		}
		file->rposition += ATOMICBLKSIZ;
	}
	return (file);
}
