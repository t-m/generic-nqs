/*
 * libnqs/db/sizedb.c
 * 
 * DESCRIPTION:
 *
 *	Return the number of bytes required to represent the specified
 *	generic descriptor structure which must be completely initialized
 *	with the exception of the size field.  The size value returned
 *	does NOT include the bytes required to store the "size" field
 *	of a generic descriptor.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <string.h>
#include <errno.h>

/*** sizedb
 *
 *
 *	int sizedb():
 *
 *	Return the number of bytes required to represent the specified
 *	generic descriptor structure which must be completely initialized
 *	with the exception of the size field.  The size value returned
 *	does NOT include the bytes required to store the "size" field
 *	of a generic descriptor.
 */
int sizedb (struct gendescr *descr, int descrtype)
{
	register int size;

  	assert (descr != NULL);
  
	switch (descrtype) {
        case DSC_QCOMPLEX:
                size = sizeof (descr->v.qcom);
                break;
	case DSC_QUEUE:
		size = sizeof (descr->v.que);
		break;
	case DSC_DEVICE:
		size = sizeof (descr->v.dev) - MAX_PATHNAME
		       + strlen (descr->v.dev.fullname);
		break;
	case DSC_QMAP:
		size = sizeof (descr->v.map);
		break;
	case DSC_PIPEQDEST:
		size = sizeof (descr->v.dest);
		break;
	case DSC_PARAM:
		switch (descr->v.par.paramtype) {
		case PRM_GENPARAMS:
			size = sizeof (descr->v.par.v.genparams);
			break;
		case PRM_LOGFILE:
			size = sizeof (descr->v.par.v.logfile);
			break;
		case PRM_NETPROCS:
			size = sizeof (descr->v.par.v.netprocs);
			break;
		case PRM_NETPEER:
			size = sizeof (descr->v.par.v.netpeer);
			break;
		default:
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Invalid NQS general parameters file descriptor type passed (type was %1d)\n", descr->v.par.paramtype);
		  	return 0;
		}
		/*
		 *  Add in the space for the paramtype field.
		 */
		size += (((char *) &descr->v.par.v) -
			 ((char *) &descr->v.par.paramtype));
		break;
	case DSC_MGR:
		size = sizeof (descr->v.mgr);
		break;
	case DSC_FORM:
		size = sizeof (descr->v.form);
		break;
	case DSC_SERVER:
		size = sizeof (descr->v.cserver);
		break;
	default:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Invalid NQS database descriptor type (type was %1d).\n", descrtype);
	  	return 0;
	}
	return (size);
}
