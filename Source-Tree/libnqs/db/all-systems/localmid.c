/*
 * libnqs/db/localmid.c
 * 
 * DESCRIPTION:
 *
 *	Return the machine-id of the local host.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 11, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <netdb.h>			/* Network database declarations */
#include <libnmap/nmap.h>		/* Network mapping codes */

/*** localmid
 *
 *
 *	int localmid():
 *	Return the machine-id of the local host.
 *
 *	Returns:
 *		0: if successful, in which case the machine_id
 *		   parameter is properly set.
 *
 *	       -2: if the local machine-id is not defined in the
 *		   local host's mapping tables (NMAP_ENOMAP).
 *
 *	       -3: if the Network Mapping Procedures (NMAP_)
 *		   deny access to the caller (NMAP_ENOPRIV).
 *
 *	       -4: if some other general NMAP_ error occurs.
 */
int localmid (Mid_t *machine_id)
{
	static short local_is_known = 0;/* Boolean non-zero if the */
					/* local machine-id is stored */
					/* in local_mid. */
	static Mid_t local_mid;		/* Machine-id of local machine */

  	assert (machine_id != NULL);
  
	if (local_is_known) {
		/*
		 *  The local machine-id is known from a previous
		 *  call.
		 */
		*machine_id = local_mid;
		return (0);		/* Success */
	}
	switch (nmap_get_mid ((struct hostent *) 0, machine_id)) {
	case NMAP_SUCCESS:		/* Successfully got local machine-id */
		local_mid = *machine_id;
		local_is_known = 1;
		return (0);
	case NMAP_ENOMAP:		/* What?  No local mid defined! */
		return (-2);
	case NMAP_ENOPRIV:		/* No privilege */
		return (-3);
	}
	return (-4);			/* General NMAP_ error */
}
