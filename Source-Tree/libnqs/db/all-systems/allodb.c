/*
 * libnqs/db/allodb.c
 *
 * DESCRIPTION:
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <errno.h>
#include <unistd.h>

static int asizedb ( struct gendescr *descr, int nasize );

/*** allodb
 *
 *
 *	long allodb():
 *
 *	Allocate space for, and store a new generic descriptor
 *	structure into, the specified database/configuration file.
 *
 *	The next descriptor returned on a readdb() call will be
 *	the newly created descriptor.
 *
 *	Returns:
 *		The file offset position of the allocated descriptor.
 */
long allodb (
	struct confd *file,		/* Database file */
	struct gendescr *newdescr,	/* New descriptor */
	int descrtype)			/* Descriptor type DSC_ */
{
	register struct gendescr *descr;
	register int need;		/* Total size of new descriptor */
	register int avail = 0;		/* Available free space in entry */
	register long descrpos;
	long origblock;			/* Original block searched */
	int size;			/* Size of new descriptor contents */

  	/* debugging checks added in 3.41.0 */
  	assert(file != NULL);
  	assert(newdescr != NULL);
  
	size = sizedb (newdescr, descrtype);	/* Size of contents */
	need = asizedb (newdescr, size);	/* Aligned size */
	/*
	 *  See if there exists adequate space in an existing descriptor.
	 *  Need has the number of free bytes required to hold the new
	 *  descriptor.
	 *
	 *  We first examine the block presently in the cache (provided
	 *  that the file has at least one block in it).  This speeds up
	 *  the allocation process if many sequential allodb() calls are
	 *  being made (as occurs during the NQS rebuild on startup).
	 *
	 *  If no free space can be found in the current cache block,
	 *  then a sequential search is made from the very beginning of
	 *  the file.
	 *
	 *  If no free space is found, then a new block is added onto
	 *  the end of the file.
	 *
	 *  Obviously, this algorithm assumes only one writer....
	 */
	if (file->size) {
		/*
		 *  The file has bytes in it.  Thus, the cache block
		 *  contains a block of the file.  Start searching at
		 *  the beginning of the current block.
		 */
		file->vposition = file->rposition - ATOMICBLKSIZ;
		origblock = file->vposition;		/* Original search blk*/
		do {
			descrpos = file->vposition;	/* Position of descr */
			descr = readdb (file);		/* 1st descr in block */
			if (descr->size < 0) avail = -(descr->size);
			else avail = descr->size
				   - asizedb (descr, sizedb(descr, descrtype));
			/*
			 *  Keep looping while within the current block,
			 *  and no space big enough is found....
			 */
		} while (avail < need && file->vposition < file->rposition);
		if (avail < need) {
			/*
			 *  No space was found in the current block.
			 *  Search from the very beginning of the
			 *  file.
			 */
			if (origblock != 0) {
				/*
				 *  We have not searched the very first						 *  block of the file.
				 */
				seekdbb (file, 0L);
			}
			/*
			 *  Otherwise, we have already searched the
			 *  first block of the file.  File->rposition
			 *  = file->vposition forcing read() in readdb().
			 */
			do {
				if (file->vposition == origblock) {
					/*
					 *  Skip this block, because we
					 *  have already searched it.
					 */
					seekdbb (file, origblock+ATOMICBLKSIZ);
				}
				descrpos = file->vposition;
				descr = readdb (file);	/* Get descriptor */
				if (descr != NULL) {
					if (descr->size < 0) {
						avail = -(descr->size);
					}
					else avail = descr->size
						   - asizedb (descr,
							      sizedb (
								descr,
								descrtype
							      ));
				}
			} while (avail < need && descr != NULL);
		}
	}
	else {
		descr = NULL;		/* No descriptors exist */
		descrpos = 0;		/* File offset 0 */
	}
	if (descr == NULL) {
		/*
		 *  No existing descriptor has the necessary free space.
		 *  We reached the end of the file.  Format the cache
		 *  block as a new block for later writing.
		 *
		 *  File->vposition, file->rposition, and descrpos
		 *  all equal:  file->size.
		 */
		seekdbb (file, file->size);	/* Zero fill cache */
		descr = (struct gendescr *) file->cache->v.chars;
		descr->size = ATOMICBLKSIZ;
		file->size += ATOMICBLKSIZ;	/* Increase file size */
	}
	else {
		/*
		 *  Space was located in an existing descriptor entry to
		 *  hold the new entry.  Please note that this entry is
		 *  completely contained in the cache block for the file.
		 */
		if (descr->size < 0) {
			/*
			 *  The very first entry in the block has enough
			 *  space.
			 */
			descr->size = -(descr->size);
		}
		else {
			descr->size -= avail;
			descrpos += descr->size;
			descr = (struct gendescr *)
				(((char *) descr) + descr->size);
			descr->size = avail;
		}
		/*
		 *  The contents of the read cache block will be changed
		 *  by the allocation of the descriptor space.  Seek back
		 *  to rewrite the block.
		 */
		lseek (file->fd, (file->rposition -= ATOMICBLKSIZ), 0);
	}
	/*
	 *  Copy the descriptor contents into the block.
	 */
	sal_bytecopy ((void *) &descr->v, (void *) (size_t) &newdescr->v, size);
	/*
	 *  Rewrite the modified block (or add a new formatted block).
	 */
	errno = 0;			/* In case write() works partway */
					/* writing some bytes out, then */
					/* nqs_abort() does not show spurious */
					/* error message */
	if (write (file->fd, file->cache->v.chars,
		   ATOMICBLKSIZ) != ATOMICBLKSIZ) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Unable to write out %d bytes.\n", ATOMICBLKSIZ);
	}
	file->rposition += ATOMICBLKSIZ;	/* Update real position */
	file->vposition = descrpos;		/* Descriptor just created */
	return (descrpos);			/* Return the address of the */
						/* allocated descriptor */
}


/*** asizedb
 *
 *
 *	int asizedb():
 *	Return aligned size for the specified descriptor.
 */
static int asizedb (struct gendescr *descr, int nasize)
{
	register int asize;			/* Aligned size */
	register int residue;			/* Residual */

  	/* debugging check added in 3.41.0 */
  	assert(descr != NULL);
  	
	asize = nasize + (((char *) &descr->v) - ((char *) &descr->size));
	residue = asize % sizeof (ALIGNTYPE);
	if (residue) asize += sizeof (ALIGNTYPE) - residue;
	return (asize);
}
