/*
 * libnqs/db/nextdb.c
 * 
 * DESCRIPTION:
 *
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

/*** nextdb
 *
 *
 *	struct gendescr *nextdb()
 *
 *	Return a pointer to the next allocated descriptor in the
 *	specified database/configuration file;  If no more allocated
 *	descriptors exist in the specified file, then return NULL.
 */
struct gendescr *nextdb (struct confd *file)
{
	register struct gendescr *gendescr;
        short zero=0;   /* used to force signed comparison Intergraph */

  	assert (file != NULL);

	/*
	 *  Loop to get next generic descriptor from the database file
	 *  ignoring unallocated entries.
	 */
	do {
		gendescr = readdb (file);	/* Get next descriptor */
        } while (gendescr != (struct gendescr *)0 && gendescr->size < zero);
	return (gendescr);
}
