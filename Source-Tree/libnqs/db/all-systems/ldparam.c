/*
 * libnqs/db/ldparam.c
 * 
 * DESCRIPTION:
 *
 *	Load the general NQS operating parameters.  This module is used
 *	by the main NQS daemon via ../src/nqs_ldconf.c  and by the Qmgr
 *	program via ../src/mgr_cmd.c.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	October 17, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>
#include <libnqs/nqsxdirs.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*** ldparam
 *
 *
 *	void ldparam():
 *	Load the general NQS operating parameters.
 */
void ldparam (void)
{
	register struct gendescr *gendescr;	/* Database descriptor */
	char	*daemon;			/* Fully quailified name */

	/*
	 *  Read and process the general NQS operating parameters
	 *  from the NQS parameter file.
	 */
	Defbatpri = MAX_RPRIORITY / 2;
	LB_Scheduler = 0;
	Defbatque [0] = '\0';
	Defdesrettim = 3600L * 24 * 3;		/* 3 days of patience */
	Defdesretwai = 300;			/* 5-minute dest retry waits */
	Defdevpri = MAX_RPRIORITY / 2;
	Defnetrettim = 3600L * 24 * 3;		/* 3 days of patience */
	Defnetretwai = 300;			/* 5-minute net retry waits */
	Defloadint = 180;			/* 3-minute load reports */
	Defprifor [0] = '\0';
	Defprique [0] = '\0';
	Fixed_shell [0] = '\0';			/* No fixed shell by default */
	Lifetime = 86400 * 7;			/* 1-week pipe queue lifetime */
	Logfilename [0] = '\0';			/* No default logfile */
        /* TAC Intergraph gave logfile default strcpy(Logfilename,"/usr/lib/nqs                /logfile");     */
	Maxcopies = 2;
	Maxextrequests = MAX_EXTREQUESTS;
	Maxgblacclimit = MAX_GBLACCLIMIT;
	Maxgblbatlimit = MAX_GBLBATLIMIT;
	Maxgblnetlimit = MAX_GBLNETLIMIT;
	Maxgblpiplimit = MAX_GBLPIPLIMIT;
	Maxoperet = 2;
	Maxprint = 1000000;
	Mail_uid = 0;				/* Default mail acct is root */
        daemon = getfilnam ("netdaemon", LIBDIR);
        strcpy (Netdaemon, daemon);             /* Default network daemon */
        relfilnam (daemon);

        daemon = getfilnam ("netserver", LIBDIR);
        strcpy (Netserver, daemon);             /* Default network server */
        relfilnam (daemon);

        daemon = getfilnam ("netclient", LIBDIR);
        strcpy (Netclient, daemon);             /* Default emptier of */
        relfilnam (daemon);                     /* network queues */

        daemon = getfilnam ("loaddaemon", LIBDIR);
        strcpy (Loaddaemon, daemon);             /* Default load daemon */
        relfilnam (daemon);                     
	Opewai = 5;
	Plockdae = 0;				/* Not locked in mem by def */
	Shell_strategy = SHSTRAT_FREE;		/* Free shell-choice */
	Termsignal = TERMSIG_DEFAULT;		/* Default request termsig */
	Udbgenparams = -1;			/* Offset of these three */
	Udbnetprocs = -1;			/* database records is */
	Udblogfile = -1;			/* not known */
	gendescr = nextdb (Paramfile);
	while (gendescr != (struct gendescr *) 0 &&
	       gendescr->v.par.paramtype != PRM_NETPEER) {
	    /*
	     *  This entry defines the general NQS operating parameters
	     *  record, the logfile record, or the network server definition
	     *  record.  These three (3) records ALWAYS appear as the first
 	     *  three records in the parameters file.
	     */
	    switch (gendescr->v.par.paramtype) {
	    case PRM_GENPARAMS:
		Udbgenparams = telldb (Paramfile);
		Mail_uid = gendescr->v.par.v.genparams.mail_uid;
		Maxgblacclimit = gendescr->v.par.v.genparams.maxgblacclimit;
		Maxgblbatlimit = gendescr->v.par.v.genparams.maxgblbatlimit;
		Maxgblnetlimit = gendescr->v.par.v.genparams.maxgblnetlimit;
		Maxgblpiplimit = gendescr->v.par.v.genparams.maxgblpiplimit;
		Plockdae = gendescr->v.par.v.genparams.plockdae;
		Shell_strategy = gendescr->v.par.v.genparams.shell_strategy;
		Termsignal = gendescr->v.par.v.genparams.termsignal;
		sal_debug_SetLevel(gendescr->v.par.v.genparams.debug);
		LB_Scheduler = gendescr->v.par.v.genparams.lb_scheduler;
		Defbatpri = gendescr->v.par.v.genparams.defbatpri;
		Defdevpri = gendescr->v.par.v.genparams.defdevpri;
		Maxcopies = gendescr->v.par.v.genparams.maxcopies;
		Maxextrequests = gendescr->v.par.v.genparams.maxextrequests;
		Maxoperet = gendescr->v.par.v.genparams.maxoperet;
		Maxprint = gendescr->v.par.v.genparams.maxprint;
		Opewai = gendescr->v.par.v.genparams.opewai;
		Defdesrettim = gendescr->v.par.v.genparams.defdesrettim;
		Defdesretwai = gendescr->v.par.v.genparams.defdesretwai;
		Defnetrettim = gendescr->v.par.v.genparams.defnetrettim;
		Defloadint = gendescr->v.par.v.genparams.defloadint;
		Defnetretwai = gendescr->v.par.v.genparams.defnetretwai;
		Lifetime = gendescr->v.par.v.genparams.lifetime;
		strcpy (Defprifor, gendescr->v.par.v.genparams.defprifor);
		strcpy (Defbatque, gendescr->v.par.v.genparams.defbatque);
		strcpy (Defprique, gendescr->v.par.v.genparams.defprique);
		strcpy (Fixed_shell, gendescr->v.par.v.genparams.fixed_shell);
		strcpy (Netdaemon, gendescr->v.par.v.genparams.netdaemon);
		strcpy (Loaddaemon, gendescr->v.par.v.genparams.loaddaemon);
		break;
	    case PRM_LOGFILE:
		Udblogfile = telldb (Paramfile);
		strcpy (Logfilename, gendescr->v.par.v.logfile);
		break;
	    case PRM_NETPROCS:
		Udbnetprocs = telldb (Paramfile);
		strcpy (Netserver, gendescr->v.par.v.netprocs.netserver);
		strcpy (Netclient, gendescr->v.par.v.netprocs.netclient);
		break;
	    }
	    gendescr = nextdb (Paramfile);
	}
}
