/*
 * libnqs/db/telldb.c
 * 
 * DESCRIPTION:
 *
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

/*** telldb
 *
 *
 *	long telldb():
 *
 *	Returns:
 *		The file offset position of the last descriptor
 *		read from the specified NQS database/configuration
 *		file.  A value of -1 indicates that no readdb()
 *		call has ever been performed on the specified file.
 */
long telldb (struct confd *file)
{
  	assert (file != NULL);
	return (file->lastread);
}
