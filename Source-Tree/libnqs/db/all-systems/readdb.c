/*
 * libnqs/db/readdb.c
 * 
 * DESCRIPTION:
 *
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

/*** readdb
 *
 *
 *	struct gendescr *readdb():
 *
 *	Read a gendescr structure from the current position updating
 *	the specified configuration file position to the next gendescr
 *	structure in the file.  Note that the gendescr structure
 *	returned may not contain any data (it may be unallocated).
 *
 *	Returns:
 *		A pointer to the gendescr structure just read if
 *		successful; otherwise a NULL pointer value is returned
 *		(indicating EOF).
 */
struct gendescr *readdb (struct confd *file)
{
	register struct gendescr *gendescr;
	register int size;

  	assert (file != NULL);
  
	if (file->vposition >= file->size) return ((struct gendescr *) 0);
						/* EOF */
	seekdbb (file, file->vposition);
	gendescr = (struct gendescr *) (file->cache->v.chars
		 +  file->vposition % ATOMICBLKSIZ);
	file->lastread = file->vposition;	/* Remember last read address */
	size = gendescr->size;
	if (size < 0) size = -size;		/* Descriptor is unallocated */
	file->vposition += size;		/* Update virtual position */
	return (gendescr);			/* Return ptr to descriptor */
}
