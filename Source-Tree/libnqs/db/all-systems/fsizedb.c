/*
 * libnqs/db/fsizedb.c
 * 
 * DESCRIPTION:
 *
 *	Update the current size of the specified database file.
 *
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <unistd.h>

/*** fsizedb
 *
 *
 *	void fsizedb():
 *	Update the current size of the specified NQS database file.
 */
void fsizedb (struct confd *file)
{
	register long filesize;

  	assert(file != NULL);
  
	filesize = lseek (file->fd, 0L, 2);
	/*
	 *  Seek back to the original position (also reloading the
	 *  contents of the cache buffer, since the cache block could
	 *  be out of date)!!!!
	 */
	file->size = filesize;		/* Update size */
	seekdb (file, file->vposition);	/* Restore file position and */
					/* reload cache block */
}
