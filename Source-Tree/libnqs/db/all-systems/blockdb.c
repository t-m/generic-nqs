/*
 * libnqs/db/blockdb.c
 * 
 * DESCRIPTION:
 *
 *	Return block number for database file offset.
 *
 *	Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/defines.h>
#include <libnqs/types.h>
#include <libnqs/proto.h>			/* Get ATOMICBLKSIZ definition */

/*** blockdb
 *
 *
 *	long blockdb():
 *
 *	Return the block number of the database file for the specified
 *	database file offset.
 *
 *	NOTES:
 *		This function exists ONLY to implement the expression:
 *
 *			blockpos = ((offset / ATOMICBLKSIZ) * ATOMICBLKSIZ);
 *
 *		Unfortunately, we cannot say that directly since smart
 *		C compilers will reduce it to read:
 *
 *			blockpos = offset;
 *
 *		Thus we write (in other modules):
 *
 *			extern long blockdb();
 *				:
 *				:
 *			blockpos = blockdb (offset) * ATOMICBLKSIZ;
 */
long blockdb (long filepos)
{
	return (filepos / ATOMICBLKSIZ);
}
