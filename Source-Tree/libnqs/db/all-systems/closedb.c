/*
 * libnqs/db/closedb.c
 * 
 * DESCRIPTION:
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdlib.h>
#include <unistd.h>

/*** closedb
 *
 *
 *	void closedb():
 *	Close the specified NQS database/configuration file.
 */
void closedb (struct confd *file)
{
  	assert (file != NULL);
  
	if (file->fd >= 0) {
		close (file->fd);	/* Close the file */
		file->fd = -1;		/* Mark confd file descriptor */
					/* as free */
		free ((char *) file->cache);
					/* Free read cache buffer */
	}
}
