/*
 * libnqs/db/rewritedb.c
 * 
 * DESCRIPTION:
 *
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <errno.h>
#include <unistd.h>

/*** rewritedb
 *
 *
 *	void rewritedb():
 *
 *	Rewrite an existing gendescr structure in the specified file
 *	at the specified offset position.  The file position is left
 *	pointing at the descriptor that was just re-written.
 */
void rewritedb (
	struct confd *file,		/* Configuration file */
	long position,			/* File offset position */
	struct gendescr *descr,		/* New version of descriptor */
	int descrtype)			/* Descriptor type */
{
	register struct gendescr *target;

  	assert (file  != NULL);
  	assert (descr != NULL);
  
	seekdbb (file, position);
	/*
	 *  Rewrite the descriptor contents.
	 */
	target = (struct gendescr *) (file->cache->v.chars
	       + position % ATOMICBLKSIZ);
	sal_bytecopy ((char *) &target->v, (char *) &descr->v,
		  sizedb (descr, descrtype));
	/*
	 *  Seek for write().
	 */
	lseek (file->fd, file->rposition - ATOMICBLKSIZ, 0);
	errno = 0;			/* Set to zero so partial writes do */
					/* not show spurious error message */
					/* text in nqs_abort(). */
	if (write (file->fd, file->cache->v.chars,
		   ATOMICBLKSIZ) != ATOMICBLKSIZ) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Unable to write %d bytes.\n", ATOMICBLKSIZ);
	}
}
