/*
 * shlexefai/shlexefai.c
 * 
 * DESCRIPTION:
 *
 *	Inform the shepherd process of a shell process created to
 *	execute an NQS batch request, that the execve() system
 *	call to invoke the actual shell failed.
 *
 *	This program MUST run as setuid to root program, in order
 *	to signal the NQS shepherd process running as root.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	January 22, 1986.
 */

#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include <libnqs/license.h>

/*** main
 *
 *
 *	Invocation from a failed shell execve() call is as follows:
 *
 *	  $(NQS_LIBEXE)/shlexefai <ASCII-errno-code>
 *
 */
int main (int argc, char *argv[])
{
	int parent_pid;			/* Process-id of NQS shepherd */

	parent_pid = getppid();
	if (parent_pid > 1) {
		/*
		 *  The NQS shepherd process for this failed shell
		 *  process has not exited.
		 */
		kill (parent_pid, SIGPIPE);
					/* Send a SIGPIPE signal to the */
					/* NQS shepherd process */
	}
	exit (atoi (argv [1]));	/* Exit */
}
