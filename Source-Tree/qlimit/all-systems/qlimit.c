/*
 * qlimit/qlimit.c
 * 
 * DESCRIPTION:
 *
 *	Display  
 *	1) a list of the meaningful limits and
 *	2) the current shell strategy
 *	for the named machines.
 *	This program must run as a setuid root program.
 *
 * RETURNS:
 *
 *      0       -  if output produced
 *      1       -  if an error occurred. A message is sent to the standard
 *                      output file for every error.
 *
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	January 13, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsdirs.h>		/* NQS files and directories */

#include <unistd.h>

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <SETUP/autoconf.h>

static void sho_version ( void );
static void showhow ( void );

/*
 *	Global variables:
 */
char *Qlimit_prefix = "Qlimit";

/*** main
 *
 *
 *	qlimit [ <machine-name(s)> ]
 */
int main (int argc, char *argv[])
{
	struct confd *paramfile;
	char *argument;
        int exitcode = 0;                       /* Function exit code */
	char *root_dir;                 /* Fully qualified file name */

  	sal_debug_InteractiveInit(1, NULL);
  
	if ( ! buildenv()) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qlimit_prefix);
	    fprintf (stderr, "establish directory independent environment.\n");
	    exit (1);
	}
	root_dir = getfilnam (Nqs_root, SPOOLDIR);
	if (root_dir == (char *)NULL) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qlimit_prefix);
	    fprintf (stderr, "determine root directory name.\n");
	    exit (1);
	}
	if (chdir (root_dir) == -1) {
	    fprintf (stderr, "%s(FATAL): Unable to chdir() to the NQS ",
                 Qlimit_prefix);
	    fprintf (stderr, "root directory.\n");
	    relfilnam (root_dir);
	    exit (1);
	}
	relfilnam (root_dir);
	if ((paramfile = opendb (Nqs_params, O_RDONLY)) == NULL) {
	    fprintf (stderr, "%s(FATAL): Unable to open the NQS ",
			 Qlimit_prefix);
	    fprintf (stderr, "parameters database file.\n");
	    exit (1);
	}
	while (*++argv != NULL && **argv == '-') {
	    argument = *argv;
	    while (*++argument != '\0') {
	        switch (*argument) {
                case 'v':
                    sho_version();
		    exit(1);
                    break;
		default:
		    fprintf (stderr, "Invalid option flag specified.\n");
		    showhow();
		    exit (1);
		}
	    }
	}
	
	/*
	 *  Block buffer stdout for efficiency.
	 */
	bufstdout();
        if (*argv == NULL)  exitcode |= shoalllim (paramfile, SHO_SHS);
	else {
	    while (*argv != NULL) {		/* Show info on a machine */
                exitcode |= sholbymach (paramfile, *argv, SHO_SHS);
		argv++;
	    }
	}
	/*
	 *  Flush output buffers and exit.
	 */
	fflush (stdout);
	fflush (stderr);
	exit (exitcode);
}

static void sho_version(void)
{
     fprintf(stderr, "NQS version is %s\n", NQS_VERSION);
}

static void showhow(void)
{
  fprintf (stderr, "qlimit -- print NQS limits\n");
  fprintf (stderr, "usage:    qlimit [-v] [hostname]\n");
  fprintf (stderr, "-v        print version information\n");
  fprintf (stderr, "hostname  name of host if not current host\n");
  exit (0);
}
