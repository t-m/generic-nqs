/*
 * qmsg/qmsg.c
 * 
 * DESCRIPTION:
 *
 *	Write a message to the stdout or stderr file(s) of a batch request.
 *
 *	Original Author:
 *	-------
 *	Clayton Andreasen, Cray Research, Inc.
 *	July 18, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <errno.h>
#include <netdb.h>			/* Network database files */
#include <libnqs/nqsdirs.h>		/* NQS files and directories */
#include <unistd.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <SETUP/autoconf.h>

static void qmsg_usage ( void );
static void qmsg_version ( void );

char *Qmsg_prefix = "Qmsg";

/*** main
 *
 *
 *	qmsg [ -eo ] <request-id>
 */
int main (int argc, char *argv[])
{
	int	i;
	char	buffer[4096];		/* message buffer */
	int	errflag;		/* write to stderr flag */
	int	outflag;		/* write to stdout flag */
	int	fd_err;			/* stderr file descriptor */
	int	fd_out;			/* stdout file descriptor */
	long	seqno;			/* Sequence# for req */
	Mid_t	mid;			/* Machine-id of originating machine */
	Mid_t	target_mid;		/* Possibly remote node */
	Mid_t	my_mid;			/* Our mid */
	char	*argument;		/* Ptr to cmd line arg text */
	char *root_dir;                 /* Fully qualified file name */	

  	sal_debug_InteractiveInit(1, NULL);
  
	if ( ! buildenv()) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qmsg_prefix);
	    fprintf (stderr, "establish directory independent ");
	    fprintf (stderr, "environment.\n");
	    exit (1);
	}

	root_dir = getfilnam (Nqs_root, SPOOLDIR);
	if (root_dir == (char *)NULL) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qmsg_prefix);
	    fprintf (stderr, "determine root directory name.\n");
	    exit (1);
	}
	if (chdir (root_dir) == -1) {
	    fprintf (stderr, "%s(FATAL): Unable to chdir() to the NQS ",
                 Qmsg_prefix);
	    fprintf (stderr, "root directory.\n");
	    relfilnam (root_dir);
	    exit (1);
	}
	relfilnam (root_dir);

	/*
	 *  Set effective uid to real uid.
	 */
	setuid( getuid() );

	/*
	 *  Interpret flags.
	 */
	while (*++argv != NULL && **argv == '-') {
	    argument = *argv;
	    while (*++argument) {
		switch (*argument) {
		case 'e':		/* write in stderr */
		    errflag = 1;
		    break;
		case 'o':		/* write in stdout */
		    outflag = 1;
		    break;
		case 'v':
		    qmsg_version();
		    break;
		default:
		    qmsg_usage();
		}
	    }
	}
	/*
	 * Default to stderr if neither is specified.
	 */
	if (!( errflag | outflag )) errflag = 1;

	/*
	 *  Validate the request-id.
	 */
	if (*argv == NULL) { 		/* If no request-ids was specified */
		fprintf (stderr, "No request-id specified.\n");
		qmsg_usage();
	}
	switch (reqspec (*argv, &seqno, &mid, &target_mid)) {
	case -1:
	    fprintf (stderr, "Invalid request-id syntax ");
	    fprintf (stderr, "for request-id: %s.\n", *argv);
	    exit (1);
	case -2:
	    fprintf (stderr, "Unknown machine for request-id: %s.\n", *argv);
	    exit (1);
	case -3:
	    fprintf (stderr, "Network mapping database ");
	    fprintf (stderr, "inaccessible.  Seek staff support.\n");
	    exit (1);
	case -4:
	    fprintf (stderr, "Network mapping database error when parsing ");
	    fprintf (stderr, "request-id: %s.  Seek staff support.\n", *argv);
	    exit (1);
	}
	localmid (&my_mid);
	if (target_mid != my_mid) {
	    fprintf (stderr, "Cannot write to remote node.\n");
	    exit (1);
	}
	if (errflag) {
	    fd_err = open(namstderr(seqno, mid), O_WRONLY|O_APPEND);
	    if (fd_err < 0) {
	        /*
	         * We cannot open stderr.  Perhaps it was submitted -eo
		 * If we want to write to stdout as well, then this is
		 * not a problem.
	         */
		errflag = 0;
		if (!outflag) {
		    perror ("Error opening request's stderr file");
		    exit(1);
		}
	    }
	}
	if (outflag) {
	    fd_out = open(namstdout(seqno, mid), O_WRONLY|O_APPEND);
	    if (fd_out < 0) {
		perror ("Error opening request's stdout file");
		exit(1);
	    }
	}
	/*
	 *  Copy the message to stderr and/or stdout.
	 */
	while ((i = read(0, buffer, sizeof(buffer))) > 0) {
	    if (errflag) {
	        if (write(fd_err, buffer, i) < 0) {
	            perror ("Error writing to request's stderr file");
		    exit(1);
		}
	    }
	    if (outflag) {
		if (write(fd_out, buffer, i) < 0) {
		    perror ("Error writing to request's stderr file");
		    exit(1);
		}
	    }
	}
	if (i < 0) {
	    perror ("Error reading message");
	    exit(1);
	}
	exit (0);
}


/*** qmsg_usage
 *
 *
 *	qmsg_usage():
 *	Show how to use this command.
 */
static void qmsg_usage(void)
{
    fprintf (stderr, "qmsg -- write message to NQS request log or error file\n");
    fprintf (stderr, "usage:    qmsg [-e] [-o] [-v] <request-id>\n");
    fprintf (stderr, " -e            append to stderr file (the default)\n");
    fprintf (stderr, " -o            append to stdout file\n");
    fprintf (stderr, " -v            print version information\n");
    fprintf (stderr, " <request-id>  NQS request identifier\n");
    exit (1);
}

/*** qmsg_version
 *
 *
 *	qmsg_version():
 *	Show the current version of this program.
 */
static void qmsg_version(void)
{
    fprintf (stderr, "NQS Version %s\n", NQS_VERSION);
    exit (1);
}
