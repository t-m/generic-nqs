/*
 * nmap/nmapmgr.c
 * 
 * DESCRIPTION:
 *
 *	Provide a very poor version of the real NPSN network mapping
 *	manager program for non-NPSN systems.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */
 
#define VERSION NQS_VERSION

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsdirs.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <libnmap/nmap.h>		/* Mid_t (all OS's) */
					/* Uid_t and gid_t (if not BSD43) */
#include <netdb.h>
#include <SETUP/autoconf.h>

#if GPORT_HAS_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#else
static void using_history(void);
static void add_history(const char *s);
static char *readline(const char *prompt);
#endif

static void nmapmgr_docmd ( char *cmdbuf );
static long nmapmgr_getint ( char *ptr );
static unsigned long getuint ( char *ptr );
static void nmapmgr_showret ( int code );
static int nmapmgr_strequ ( char *str1, char *str2 );

#define	MAX_LINESIZ	255
#define	MAX_CMD		31

/*** main
 *
 *	main (): The main routine of nmapmgr.
 *
 *
 */
int main (int argc, char *argv[])
{
	char cmdbuf [MAX_LINESIZ];
	FILE	*fp = NULL;
	char	*filename;
	char	*readbuf;
	int	status;		    /* status from open_name_map */

  	sal_debug_InteractiveInit(1, NULL);
  
        /*
         *  Set up the environment variables necessary for directory
         *  independent installation.  If these variables do not exist,
         *  NQS will use the compiled defaults.  The $NQS_HOME/nqs.config
         *  file is read to create environment variables that define where
         *  major NQS directories reside.  Major directories are as
         *  follows:
         *
         *    NQS_LIBEXE - NQS daemon programs and qmgr help file.
         *    NQS_LIBUSR - NQS user interface programs.
         *    NQS_MAN - NQS man pages directory.
         *    NQS_NMAP - NQS network map directory.
         *    NQS_SPOOL - NQS spooling directories.
         */
        if ( ! buildenv()) {
                fprintf (stderr, "Unable to establish directory ");
                fprintf (stderr, "independent environment.\n");
                exit(1);                /* Exit */
        }
	umask (0);			/* CRITICAL */
	/*
	 * Does the machine database already exist?  If not,  go ahead
	 * and create it.  There isn't much we can do without it.
	 */
	status = open_name_map (O_RDONLY);
	if (status != NMAP_SUCCESS ) {
	    status = nmap_create();
	    if (status == NMAP_EUNEXPECT) {
		printf("NMAPMGR unexpected error creating MID database, \n");
		exit (1);
	    } else if (status == NMAP_ENOPRIV) {
		printf("NMAPMGR error creating MID database -- no privilege.\n");
		exit (1);
	    } else if  (status == NMAP_ECONFLICT) {
		printf ("NMAPMGR error creating MID database -- already exists.\n");
		exit (1);
	    }
	}
	if (argc > 1) {
	    if (*argv[1] == '@') {
	        filename = argv[1];
	        filename++;
	        if ((fp = fopen(filename, "r")) == NULL ) {
		    printf("NMAPMGR error opening input file %s\n", filename);
		    exit (1);
	        }
		for (;;) {
		    if (fgets (cmdbuf, MAX_LINESIZ, fp) == NULL) {
			putchar ('\n');
			if (feof (fp)) exit (0);
			printf ("NMAPMGR error reading command input.\n");
			exit (1);
		    }
		    if (cmdbuf [strlen (cmdbuf)-1] != '\n') {
			printf ("NMAPMGR command line too long.\n");
			exit (1);
		    }
		    cmdbuf [strlen (cmdbuf)-1] = '\0';	/* Zap '\n' */
		    if (strlen(cmdbuf) == 0) continue;
		    printf("%s\n", cmdbuf);
		    nmapmgr_docmd (cmdbuf);
		}
	    }
	    else { /* command is on the command-line */
		int i = 1;
		int templen = MAX_LINESIZ-1;
		cmdbuf[0] = '\0';
		while (i < argc) {
		    if (templen > 0) {
		        strncat(cmdbuf, argv[i], templen);
		        strcat(cmdbuf, " ");
		        templen -= strlen(cmdbuf);
		    }
		    i++;
		}
		nmapmgr_docmd (cmdbuf);
	    }
	} else { /* Interactive use */
	    using_history();
	    while ((readbuf = readline("NMAPMGR>: ")) != NULL) {
		putchar ('\n');
		if (strlen(readbuf) != 0) {
		    add_history(readbuf);
		    nmapmgr_docmd (readbuf);
		}
		free(readbuf);
	    }
	    putchar ('\n');
	}
	exit(0);
}


/*** nmapmgr_docmd
 *
 *	static nmapmgr_docmd (): Do command.
 *
 *
 */
static void nmapmgr_docmd (char *cmdbuf)
{

	struct hostent hostent;
	char *arg [MAX_CMD+1];
	int tokens;
	char *ch;
	int res;
	int nfds;
	Mid_t from_mid;
	uid_t from_uid;
	gid_t from_gid;
	Mid_t to_mid;
	uid_t to_uid;
	gid_t to_gid;
	Mid_t mid;
	uid_t defuid;
	gid_t defgid;

	/*
 	 *  Break command into tokens.
	 */
	for (tokens = 0; tokens <= MAX_CMD; tokens++) arg [tokens] = NULL;
	tokens = 0;
	ch = cmdbuf;
	while (*ch && tokens < MAX_CMD) {
		while (*ch && isspace (*ch)) ch++;
		if (*ch) {
			arg [tokens++] = ch;	/* Save ptr to arg */
			while (*ch && !(isspace (*ch))) ch++;
			if (*ch) *ch++ = '\0';	/* Null terminate */
		}
	}
        /* Bail out if no data */
        if (tokens == 0)
                return;
	/*
	 *  Perform the command.
	 */
	if (nmapmgr_strequ (arg [0], "Add")) {
		if (tokens == 1) printf ("Missing verb modifier.\n");
		else if (nmapmgr_strequ (arg [1], "Alias")) {
                        if (tokens >= 4) {
                                nmapmgr_showret (nmap_add_alias (arg [2], arg [3]));
                        }
                        else printf ("Missing arguments.\n");
                }
		else if (nmapmgr_strequ (arg [1], "Uid")) {
			if (tokens >= 5) {
			  		from_mid = getuint(arg[2]);
				  	from_uid = nmapmgr_getint (arg[3]);
				  	to_uid = nmapmgr_getint (arg[4]);
					nmapmgr_showret (nmap_add_uid (from_mid,
							       from_uid,
							       to_uid));
			}
			else printf ("Missing arguments.\n");
		}
		else if (nmapmgr_strequ (arg [1], "Gid")) {
			if (tokens >= 5) {
				  	from_mid = getuint (arg[2]);
			    		from_gid = nmapmgr_getint (arg[3]);
					to_gid = nmapmgr_getint (arg[4]);
					nmapmgr_showret (nmap_add_gid (from_mid,
							       from_gid,
							       to_gid));
			}
			else printf ("Missing arguments.\n");
		}
                else if (nmapmgr_strequ (arg [1], "Host")) {
                        if (tokens >= 3) {
                                nmapmgr_showret (nmap_add_host (arg [2]));
                        }
                        else printf ("Missing argument.\n");
                }
		else if (nmapmgr_strequ (arg [1], "Name")) {
			if (tokens >= 4) {
				  	to_mid = getuint (arg[3]);
					nmapmgr_showret (nmap_add_nam (arg [2],to_mid));
			}
			else printf ("Missing arguments.\n");
		}
		else if (nmapmgr_strequ (arg [1], "Mid")) {
			if (tokens >= 4) {
			    mid = getuint(arg[2]);
			    if (mid > 0) 
				nmapmgr_showret (nmap_add_mid (mid, arg [3]));
			    else
				printf ("Mid must be greater than 0.\n");
			}
			else printf ("Missing arguments.\n");
		}
		else printf ("Unrecognized command verb modifier.\n");
	}
	else if (nmapmgr_strequ (arg [0], "CHange")) {
		if (tokens == 1) printf ("Missing verb modifier.\n");
		else if (nmapmgr_strequ (arg [1], "Name")) {
			if (tokens >= 4) {
				  	mid = getuint(arg[2]);
					nmapmgr_showret (nmap_chg_mid (mid, arg [3]));
			}
			else printf ("Missing arguments.\n");
		}
		else printf ("Unrecognized command verb modifier.\n");
	}
	else if (nmapmgr_strequ (arg [0], "CReate")) {
		nmapmgr_showret (nmap_create());
	}
	else if (nmapmgr_strequ (arg [0], "Delete")) {
		if (tokens == 1) printf ("Missing verb modifier.\n");
		else if (nmapmgr_strequ (arg [1], "Uid")) {
			if (tokens >= 4) {
   					from_mid = getuint(arg[2]);
   					from_uid = nmapmgr_getint (arg[3]);
					nmapmgr_showret (nmap_del_uid (from_mid,
							       from_uid));
			}
			else printf ("Missing arguments.\n");
		}
		else if (nmapmgr_strequ (arg [1], "Gid")) {
			if (tokens >= 4) {
				  	from_mid = getuint(arg[2]);
				  	from_gid = nmapmgr_getint(arg[3]);
					nmapmgr_showret (nmap_del_gid (from_mid,
							       from_gid));
			}
			else printf ("Missing arguments.\n");
		}
		else if (nmapmgr_strequ (arg [1], "DEFUid")) {
			if (tokens >= 3) {
				  	from_mid = getuint (arg[2]);
					nmapmgr_showret (nmap_del_defuid (from_mid));
			}
			else printf ("Missing <mid>.\n");
		}
		else if (nmapmgr_strequ (arg [1], "DEFGid")) {
			if (tokens >= 3) {
				  	from_mid = getuint(arg[2]);
					nmapmgr_showret (nmap_del_defgid (from_mid));
			}
			else printf ("Missing <mid>.\n");
		}
                else if (nmapmgr_strequ (arg [1], "Alias")) {
                        if (tokens >= 3) {
                                nmapmgr_showret (nmap_del_nam (arg [2]));
                        }
                        else printf ("Missing <name>.\n");
                }
                else if (nmapmgr_strequ (arg [1], "Host")) {
                        if (tokens >= 3) {
                                nmapmgr_showret (nmap_del_host (arg [2]));
                        }
                        else printf ("Missing <name>.\n");
                }
		else if (nmapmgr_strequ (arg [1], "Name")) {
			if (tokens >= 3) {
				nmapmgr_showret (nmap_del_nam (arg [2]));
			}
			else printf ("Missing <name>.\n");
		}
		else if (nmapmgr_strequ (arg [1], "Mid")) {
			if (tokens >= 3) {
				  	mid = getuint(arg[2]);
					nmapmgr_showret (nmap_del_mid (mid));
			}
			else printf ("Missing <mid>.\n");
		}
		else printf ("Unrecognized command verb modifier.\n");
	}
	else if (nmapmgr_strequ (arg [0], "Exit")) exit (0);
	else if (nmapmgr_strequ (arg [0], "Get")) {
		if (tokens == 1) printf ("Missing verb modifier.\n");
		else if (nmapmgr_strequ (arg [1], "Uid")) {
			if (tokens >= 4) {
 					from_mid = getuint (arg[2]);
					from_uid = nmapmgr_getint (arg[3]);
					res = nmap_get_uid (from_mid, from_uid,
							    &to_uid);
					nmapmgr_showret (res);
					if (res >= NMAP_SUCCESS) {
						printf ("Mapped uid = %d.\n",
							to_uid);
					}
			}
			else printf ("Missing arguments.\n");
		}
		else if (nmapmgr_strequ (arg [1], "Gid")) {
			if (tokens >= 4) {
				  	from_mid = getuint (arg[2]);
   					from_gid = nmapmgr_getint (arg[3]);
					res = nmap_get_gid (from_mid, from_gid,
							    &to_gid);
					nmapmgr_showret (res);
					if (res >= NMAP_SUCCESS) {
						printf ("Mapped gid = %d.\n",
							to_gid);
					}
			}
			else printf ("Missing arguments.\n");
		}
		else if (nmapmgr_strequ (arg [1], "Mid")) {
			if (tokens >= 3) {
				hostent.h_name = arg [2];
				hostent.h_aliases = arg+3;
				res = nmap_get_mid (&hostent, &mid);
				nmapmgr_showret (res);
				if (res == NMAP_SUCCESS) {
					printf ("Mid = %lu.\n", (unsigned long) mid);
				}
			}
			else printf ("Missing <name(s)>.\n");
		}
		else if (nmapmgr_strequ (arg [1], "Name")) {
			if (tokens >= 3) {
				  	mid = getuint (arg[2]);
					arg [0] = nmap_get_nam (mid);
					if (arg [0] == NULL) {
						printf ("No such mid.\n");
					}
					else printf ("Name = %s.\n", arg[0]);
			}
			else printf ("Missing <mid>.\n");
		}
		else printf ("Unrecognized command verb modifier.\n");
	}
	else if (nmapmgr_strequ (arg [0], "Help")) {
		putchar ('\n');
		printf ("Commands:\n");
		printf ("  Add Uid <from_mid> <from_uid> <to_uid>\n");
		printf ("  Add Gid <from_mid> <from_gid> <to_gid>\n");
                printf ("  Add Alias <name> <principal-name>\n");
                printf ("  Add Host <principal-name>\n");
		printf ("  Add Name <name> <to_mid>\n");
		printf ("  Add Mid <mid> <principal-name>\n");
		printf ("  CHange Name <mid> <new-name>\n");
		printf ("  CReate\n");
		printf ("  Delete Uid <from_mid> <from_uid>\n");
		printf ("  Delete Gid <from_mid> <from_gid>\n");
		printf ("  Delete DEFUid <from_mid>\n");
		printf ("  Delete DEFGid <from_mid>\n");
                printf ("  Delete Alias <name>\n");
                printf ("  Delete Host <principal-name>\n");
		printf ("  Delete Name <name>\n");
		printf ("  Delete Mid <mid>\n");
		printf ("  Exit\n");
		printf ("  Get Uid <from_mid> <from_uid>\n");
		printf ("  Get Gid <from_mid> <from_gid>\n");
		printf ("  Get Mid <name>\n");
		printf ("  Get Name <mid>\n");
		printf ("  Help\n");
		printf ("  List\n");
		printf ("  Quit\n");
		printf ("  Set Nfds <#-of-file-descrs>\n");
		printf ("  Set DEFUid <from_mid> <defuid>\n");
		printf ("  Set DEFGid <from_mid> <defgid>\n");
                printf ("  Show\n");
                printf ("  Version\n");
		printf (" ^D (to exit nmapmgr.\n");
	}
	else if (nmapmgr_strequ (arg [0], "List")) {
	    nmap_show();                    /* I like show better now */
	                                    /* This used to be nmap_list_nam() */
	}
	else if (nmapmgr_strequ (arg [0], "Quit")) exit (0);
	else if (nmapmgr_strequ (arg [0], "Set")) {
		if (tokens == 1) printf ("Missing verb modifier.\n");
		else if (nmapmgr_strequ (arg [1], "Nfds")) {
			if (tokens >= 3) {
				if ((nfds = nmapmgr_getint (arg [2])) >= 0) {
					nmap_ctl (NMAP_OPNOFD, nfds);
				}
			}
			else printf ("Missing <#-of-file-descrs>.\n");
		}
		else if (nmapmgr_strequ (arg [1], "DEFUid")) {
			if (tokens >= 4) {
   					from_mid = getuint (arg[2]);
   					defuid = nmapmgr_getint (arg[3]);
					nmapmgr_showret (nmap_set_defuid (from_mid,
							          defuid));
			}
			else printf ("Missing arguments.\n");
		}
		else if (nmapmgr_strequ (arg [1], "DEFGid")) {
			if (tokens >= 4) {
   					from_mid = getuint (arg[2]);
   					defgid = nmapmgr_getint (arg[3]);
					nmapmgr_showret (nmap_set_defgid (from_mid,
								  defgid));
			}
			else printf ("Missing arguments.\n");
		}
		else printf ("Unrecognized command verb modifier.\n");
	}
	else if (nmapmgr_strequ (arg[0], "Show")) {
	    nmapmgr_showret (nmap_show());
	}
	else if (nmapmgr_strequ (arg[0], "Version")) {
	    printf("Nmapmgr version: %s\n", VERSION);
	}
	else if (nmapmgr_strequ ("#", arg[0])) {
	    /* comment -- ignore */
	}
	else if (tokens >= 1) printf ("Unrecognized command verb: %s\n",
				      arg [0]);
}


/*** nmapmgr_strequ
 *
 *	int nmapmgr_strequ (str1, str2)
 *	char *str1;
 *	char *str2;
 *
 *	Return 1 if the two null-terminated strings are equal.
 *	Otherwise return 0.  This comparison however uses uppercase
 *	characters in str2 to denote acceptable abbreviations.
 *	Thus:
 *
 *		nmapmgr_strequ ("defg", "DEFGid") = 1
 *
 *	while:
 *
 *		nmapmgr_strequ ("def", "DEFGid") = 0
 */
static int nmapmgr_strequ (str1, str2)
char *str1;
char *str2;
{
	register char ch1;
	register char ch2;

	while ((ch1 = *str1++)) {
		ch2 = *str2++;
		if (isupper (ch1)) ch1 += 'a' - 'A';
		if (isupper (ch2)) ch2 += 'a' - 'A';
		if (ch1 != ch2) return (0);	/* Strings not equal */
	}
	if (isupper (*str2)) return (0);/* Strings not equal */
	return (1);			/* Strings equal */
}
 

/*** nmapmgr_getint
 *
 *	long nmapmgr_getint (ptr)
 *	char *ptr;
 *
 *	Return integer value of unsigned sequence of decimal ASCII
 *	digits referenced by ptr.  Print-out an error message and
 *	return a -1 if no sequence of decimal digits exists.
 */
static long nmapmgr_getint (ptr)
char *ptr;
{
	register int i;

	if (isdigit (*ptr)) {
		i = 0;
		while (isdigit (*ptr)) {
			i *= 10;
			i += *ptr++ - '0';
		}
		if (i < 0) {
			printf ("Integer overflow.\n");
			return (-1);
		}
		return (i);
	}
	else {
		printf ("Unsigned integer number expected.\n");
		return (-1);
	}
}


/*** getuint
 *
 *      long getuint (ptr)
 *      char *ptr;
 *
 *      Return integer value of unsigned sequence of decimal ASCII
 *      digits referenced by ptr.  Print-out an error message and
 *      return a -1 if no sequence of decimal digits exists.
 */
static unsigned long getuint (char *ptr)
{
        register unsigned long i;

        if (isdigit (*ptr)) {
                i = 0;
                while (isdigit (*ptr)) {
                        i *= 10;
                        i += *ptr++ - '0';
                }
                return (i);
        }
        else {
                printf ("Unsigned integer number expected.\n");
                return (-1);
        }
}

/*** nmapmgr_showret
 *
 *	nmapmgr_showret (code)
 *	int code;
 *
 *	Identify the return code from the NMAP library module.
 */
static void nmapmgr_showret (int code)
{
	switch (code) {
	case NMAP_SUCCESS:
		printf ("NMAP_SUCCESS:  Successful completion.\n");
		break;
	case NMAP_DEFMAP:
		printf ("NMAP_DEFMAP:  Successful completion using ");
		printf ("default mapping.\n");
		break;
	case NMAP_EUNEXPECT:
		printf ("NMAP_EUNEXPECT:  Fatal error in mapping software.\n");
		break;
	case NMAP_ENOPRIV:
		printf ("NMAP_ENOPRIV:  No privilege for operation.\n");
		break;
	case NMAP_ECONFLICT:
		printf ("NMAP_ECONFLICT:  Already exists.\n");
		break;
	case NMAP_ENOMAP:
		printf ("NMAP_ENOMAP:  No such mapping.\n");
		break;
	case NMAP_ENOMID:
		printf ("NMAP_ENOMID:  No such machine.\n");
		break;
	case NMAP_EBADNAME:
		printf ("NMAP_EBADNAME:  Null name, name too long, ");
		printf ("or undefined host.\n");
		break;
	default:
		printf ("Invalid NMAP return code.\n");
		break;
	}
}

/* Simulate (poorly) the readline library routines that I use. */
/* The only trick is that readline(2) returns a string that must be */
/* free-ed.  This means my readline must do the same.  If I was really */
/* feeling ambitous, I'd allow longer command lines and continuation */
/* characters '\' at the end of line.  Maybe for qmgr! */
#if !GPORT_HAS_READLINE

static void using_history(void)
{
}

static void add_history(const char *s)
{
}

static char *readline(const char *prompt)
{
    char *s;
    char buf[256];
    
    printf(prompt);
    fflush(stdout);
    if (fgets (buf, 256, stdin) == NULL) {
	putchar ('\n');
	if (feof (stdin))
	    return(NULL);
	printf ("NMAPMGR error reading command input.\n");
	exit (1);
    }
    
    if (buf[strlen(buf)-1] != '\n') {
	printf ("NMAPMGR command line too long.\n");
	exit (1);
    }
    
    buf[strlen(buf)-1] = '\0';	/* Zap '\n' */

    /* allocate a free-able string and return it */
    s = (char *)malloc(sizeof(char)*(strlen(buf)+1));
    if (s == NULL) {
	printf ("NMAPMGR memory allocation failure.\n");
	exit (1);
    }
    strcpy(s,buf);
    return s;
}
#endif
