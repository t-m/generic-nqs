/*
 * nqsd/nqs_wakdes.c
 * 
 * DESCRIPTION:
 *
 *	Re-enable pipe queue destinations that are presently in retry
 *	mode, whose retry wait time has elapsed.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	May 30, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>	       	/* Global vars and directories */
#include <time.h>

/*** nqs_wakdes
 *
 *
 *	void nqs_wakdes():
 *
 *	Re-enable pipe queue destinations that are presently in retry
 *	mode, whose retry wait time has elapsed.
 */
void nqs_wakdes(void)
{
	struct pipeto *dest;	/* Pipe queue destination */
	time_t timenow;			/* Current GMT time */

	time (&timenow);		/* Get current time */
	dest = Pipetoset;		/* Global pipe queue destination set */
	while (dest != (struct pipeto *) 0) {
	    if (dest->status == DEST_RETRY) {
		/*
		 *  This destination is in the retry state, and
		 *  has NOT been re-enabled.
		 */
		if (dest->retry_at > timenow) {
		    /*
		     *  Set alarm for destination retry in
		     *  the future.
		     */
		    nqs_vtimer (&dest->retry_at, nqs_wakdes);
		} else {
		    /*
		     *  This destination must be enabled
		     *  (leaving the retry bit set)!
		     */
		    dest->status |= DEST_ENABLED;
	            udb_destination (dest);	/* Update database */
		}
	    }
	    dest = dest->next;	/* Examine next destination */
	}
	/*
	 *  Maybe spawn some pipe queue requests, and update and
	 *  pipe queue request-ordering files that need to be
	 *  modified.
	 */
	psc_spawn();			/* Maybe spawn some pipe requests */
}
