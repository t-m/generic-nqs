/*
 * nqsd/nqs_disreq.c
 * 
 * DESCRIPTION:
 *
 *	Dispose of an NQS request or subrequest.  This can
 *	entail deleting the control and data files for the
 *	request (not a subrequest), as well as the memory-
 *	resident version of the request or subrequest structure,
 *	and associated transaction descriptor (not a subrequest).
 *	The Extreqcount global var is also updated as appropriate.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	March 30, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>		/* NQS external vars and dirs  */
#include <malloc.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*** nqs_disreq
 *
 *
 *	void nqs_disreq():
 *
 *	Dispose of an NQS request or subrequest.  This can
 *	entail deleting the control and data files for the
 *	request (not a subrequest), as well as the memory-
 *	resident version of the request or subrequest structure,
 *	and associated transaction descriptor (not a subrequest).
 *	The Extreqcount global var is also updated as appropriate.
 */
void nqs_disreq (struct request *request, short deleteflag)
{
	register int tid;		/* Transaction-id */

	/*
	 *  MOREHERE someday to delete child subrequest(s) as well.
	 */
	if ((request->status & RQF_SUBREQUEST) == 0) {
		/*
		 *  This request is not a subrequest.
		 */
		if (request->status & RQF_EXTERNAL) {
			/*
			 *  The request was queued from an external source.
			 *  Update the number of externally queued requests
			 *  as necessary.
			 */
			Extreqcount--;	/* One less externally queued */
		}			/* request */
		tid = request->v1.req.trans_id;
		/*
		 *  This request still has a transaction-id/descriptor
		 *  assigned to it.  Free up the transaction-id so that
		 *  it can be used by another request.
		 */
		tid_deallocate (tid);
		/*
		 *  Update the permanent memory NQS database to record
		 *  that the corresponding transaction descriptor is now
		 *  unallocated.
		 *
		 *  WARNING:  It is CRITICAL that the transaction-id/descr
		 *	      for a request be released PRIOR to actually
		 *	      deleting any files associated with the
		 *	      request.  This temporal sequence is
		 *	      CRITICAL and ESSENTIAL for the correct
		 *	      operation of nqs_rbuild.c.
		 */
		if (tra_release (tid) == -1) {
			/*
			 *  Something went wrong updating the database.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Tra_release() error in nqs_disreq.\n");
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "%s.\n", asciierrno());
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Transaction-id was: %1d.\n", tid);
		}
		if (deleteflag) {
			/*
			 *  The request is NOT being placed in the
			 *  failed directory for analysis.  The request
			 *  control and data files are to be deleted.
			 */
			nqs_delrfs (request->v1.req.orig_seqno,
				    request->v1.req.orig_mid,
				    request->v1.req.ndatafiles);
		}
	}
	free ((char *) request);	/* Delete the request structure */
}
