/*
 * nqsd/nqs_upp.c
 * 
 * $Source: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/Source-Tree/nqsdaemon/all-systems/nqs_upp.c,v $
 *
 * DESCRIPTION:
 *
 *	NQS general parameters state update module.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	June 16, 1986.
 */


#include <signal.h>
#include <errno.h>
#include <string.h>
#include <malloc.h>

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libnqs/nqsxvars.h>
#include <libnqs/transactcc.h>		/* Transaction completion codes */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

static void upp_brokenpipe ( int );

/*** upp_setdefbatque
 *
 *
 *	long upp_setdefbatque():
 *	Set the default batch-request queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful;
 *		TCML_NOSUCHQUE:	if the named queue does not exist.
 */
long upp_setdefbatque (char *defbatque)
{
	struct nqsqueue *queue;

	if (defbatque [0] != '\0') {		/* Setting a default */
		queue = nqs_fndnnq (defbatque);
		if (queue == (struct nqsqueue *)0) {
			return (TCML_NOSUCHQUE);/* No such queue */
		}
	}
	strcpy (Defbatque, defbatque);		/* Save new batch queue name */
	udb_genparams();			/* Update NQS database */
	return (TCML_COMPLETE);			/* Success */
}


/*** upp_setdefprifor
 *
 *
 *	long upp_setdefprifor():
 *	Set the default print-forms value.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHFORM: if the named form is not defined in the
 *				 local NQS forms set.
 */
long upp_setdefprifor (char *defprifor)
{
	if (defprifor [0] != '\0') {	/* Setting a default */
		if (!upf_valfor (defprifor)) {
			return (TCML_NOSUCHFORM);	/* No such form */
		}
	}
	strcpy (Defprifor, defprifor);	/* Save default print-form */
	udb_genparams();		/* Update NQS database */
	return (TCML_COMPLETE);		/* Success */
}


/*** upp_setdefprique
 *
 *
 *	long upp_setdefprique():
 *	Set the default print-request queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful;
 *		TCML_NOSUCHQUE:	if the named queue does not exist.
 */
long upp_setdefprique (char *defprique)
{
	struct nqsqueue *queue;

	if (defprique [0] != '\0') {	/* Setting a default */
		queue = nqs_fndnnq (defprique);
		if (queue == (struct nqsqueue *)0) {
			return (TCML_NOSUCHQUE);	/* No such queue */
		}
	}
	strcpy (Defprique, defprique);	/* Save new print-queue name */
	udb_genparams();		/* Update NQS database */
	return (TCML_COMPLETE);		/* Success */
}


/*** upp_setlogfil
 *
 *
 *	long upp_setlogfil():
 *
 *	Switch error and message logging to a new logfile.
 *	If the new logfile cannot be opened for writing, then
 *	do not switch!
 *
 *	A message is left on the old logfile if the switch is
 *	successful (or unsuccessful).
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful;
 *		TCML_LOGFILERR:	if unsuccessful.
 */
long upp_setlogfil (char *newlogname)
{
  /* Generic NQS 3.41.0
   * 
   * This is a no-op, supported for backwards compatibility.
   * Log file support has been removed from NQS, and left to
   * libsal to handle.
   */
  
  return (TCML_COMPLETE);
}


/*** upp_brokenpipe
 *
 *
 *	void upp_brokenpipe ():
 *
 *	This function serves as the signal catcher for SIGPIPE signals
 *	sent in case the NQS log process is not running anymore.
 *
 *	This function sets errno = EPIPE
 */
static void upp_brokenpipe ( int unused )
{
	signal (SIGPIPE, upp_brokenpipe);
	errno = EPIPE;
}

/*** upp_setgblrunlim
 *
 *
 *	int upp_setgblrunlim():
 *
 *	Set the global run limit for all requests.  This may require us to
 *	allocate memory for a bigger Runvars structure.
 *
 *	Returns:
 *		 0:	if successful.
 *		-1:	if unable to allocate memory for the Runvars structure.
 */
int upp_setgblrunlim (void)
{
	int i;
	int newgblrunlimit;
	struct running *runvars;

	newgblrunlimit = Maxgblbatlimit + Maxgblpiplimit + Maxgblnetlimit
		 /* + Maxnoofdevices */;
	if (newgblrunlimit > Runvars_size) {
		if (Runvars_size == 0) {
			/*
			 *  Create Runvars structure for the first time.
			 */
			Runvars = (struct running *) malloc (newgblrunlimit *
				sizeof (struct running));
			if (Runvars ==  (struct running *) 0) {
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "upp_setgblrunlim(): Can't malloc Runvars\n");
				return (-1);
			}
		} else {
			/*
			 *  Increase the number of entries in the existing
			 *  Runvars structure.
			 */
			runvars = (struct running *) realloc ((char *) Runvars,
				newgblrunlimit * sizeof (struct running));
			if (runvars ==  (struct running *) 0) {
				/*
				 *  We're assuming that realloc won't damage
				 *  the original Runvars structure.
				 */
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "upp_setgblrunlim(): Can't realloc Runvars\n");
				return (-1);
			}
			Runvars = runvars;
		}
		/*
		 *  Clear the allocated flag in all of the newly created
		 *  Runvars entries.
		 */
		for (i = Runvars_size; i < newgblrunlimit; i++)
			(Runvars + i)->allocated = 0;
		Runvars_size = newgblrunlimit;
	}
	Maxgblrunlimit = newgblrunlimit;
	return (0);
}


/*** upp_setgblbatlim
 *
 *
 *	long upp_setgblbatlim():
 *
 *	Set the global runlimit for batch requests.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful.
 *		TCML_NOMOREMEM:	if unable to allocate more memory for the
 *				Runvars structure.
 */
long upp_setgblbatlim (long limit)
{
	long prevlim;	 		/* Previous batch request runlimit */

	/*
	 *  Set the new global batch run limit which in turn affects the global
	 *  run limit, so set it too.  Call the spawning routine to possibly
	 *  spawn new batch requests if the limit has increased.
	 */
	prevlim = Maxgblbatlimit;
	Maxgblbatlimit = limit;
	if (upp_setgblrunlim () == -1) {
		/*
		 *  We were unable to set the global run limit due to an
		 *  inability to allocate more memory for the Runvars structure,
		 *  so we'll put the global batch limit back the way it was.
		 */
		Maxgblbatlimit = prevlim;
		return (TCML_NOMOREMEM);
	}
	if (Booted) {
		udb_genparams ();
	}
	if (limit > prevlim) {
		bsc_spawn ();
	}
	return (TCML_COMPLETE);		/* Successful completion */
}


/*** upp_setgblpiplim
 *
 *
 *	long upp_setgblpiplim():
 *
 *	Set the global route limit for pipe requests.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful.
 *		TCML_NOMOREMEM:	if unable to allocate more memory for the
 *				Runvars structure.
 */
long upp_setgblpiplim (long limit)
{
	long prevlim;			/* Previous pipe request route limit */

	/*
	 *  Set the new global pipe route limit which in turn affects the global
	 *  run limit, so set it too.  Call the spawning routine to possibly
	 *  spawn new pipe requests if the limit has increased.
	 */
	prevlim = Maxgblpiplimit;
	Maxgblpiplimit = limit;
	if (upp_setgblrunlim () == -1) {
		/*
		 *  We were unable to set the global run limit due to an
		 *  inability to allocate more memory for the Runvars structure,
		 *  so we'll put the global pipe limit back the way it was.
		 */
		Maxgblpiplimit = prevlim;
		return (TCML_NOMOREMEM);
	}
	if (Booted) {
		udb_genparams ();
	}
	if (limit > prevlim) {
		psc_spawn ();
	}
	return (TCML_COMPLETE);		/* Successful completion */
}


