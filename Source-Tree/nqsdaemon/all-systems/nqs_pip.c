/*
 * nqsd/nqs_pip.c
 * 
 * DESCRIPTION:
 *
 *	Pipe-queue server, network-queue server, and network server
 *	interaction update module.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	May 31, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>
#include <libnqs/transactcc.h>			/* Transaction completion codes */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <unistd.h>

static void abortrequest ( struct request *request );

/*** pip_reqreceived
 *
 *
 *	long pip_reqreceived():
 *
 *	Perform the necessary updates for the specified request
 *	that has now been completely received from the local machine,
 *	or from a remote machine.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful.
 *		TCML_NOSUCHREQ:	if the specified request does not
 *				exist on this machine.
 *
 */
long pip_reqreceived (long orig_seqno, Mid_t orig_mid)
{
	struct request *predecessor;	/* Predecessor in request set in */
					/* queue */
	struct request *req;		/* Ptr to request structure for */
					/* located req, if found */
	int state;			/* Request state */
	register time_t timenow;	/* Current time */
	register struct nqsqueue *queue;	/* Queue containing request */

	/*
	 *  Search for the request.
	 */
	state = RQS_ARRIVING;		/* Search only arriving state */
	if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) {
	    /*
	     *  The request was not found in any of the local queues.
	     */
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pip_reqreceived: Couldn't find %d.%lu in arriving state.\n", orig_seqno, orig_mid);
	    state = -1;     /* look in all states */
	    if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) 
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_reqreceived: Couldn't find anywhere.\n");
	    else 
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_reqreceived: Found in state %d\n", state);
	    return (TCML_NOSUCHREQ);
	}
	/*
	 *  The request has been located.
	 */
	timenow = time ((time_t *) 0);
	queue = req->queue;		/* Containing queue */
	/*
	 *  It is necessary to dequeue the request from its current
	 *  position in the arriving set of the queue.
	 */
	if (predecessor == (struct request *) 0) {
		queue->arriveset = req->next;
	}
	else predecessor->next = req->next;
	queue->q.arrivecount--;		/* One less request in the arrive */
					/* state */
	/*
	 *  The request is now significantly changing state.
	 *  Place the request in the appropriate state set
	 *  within the same queue.
	 */
	req->v1.req.state_time=timenow;	/* Adjust request state time */
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "pip_reqreceived: Setting state_time\n");
	if ((req->status & RQF_OPERHOLD) || (req->status & RQF_USERHOLD)) {
		/*
		 *  Add the request to the hold set for this queue,
		 *  and update the NQS database image.
		 */
		a2s_a2hset (req, queue);
		udb_qorder (queue);	/* Update NQS database image */
	}
	else if (req->start_time > timenow) {
		/*
		 *  The request has a start time in the future,
		 *  and update the NQS database image.
		 */
		a2s_a2wset (req, queue);
		udb_qorder (queue);	/* Update NQS database image */
	}
	else {
		/*
		 *  Place the request in the eligible to run set.
		 *  The QUE_UPDATE bit is set by a2s_a2qset().
		 */
		a2s_a2qset (req, queue);
		switch (queue->q.type) {
		case QUE_BATCH:
			bsc_spawn();	/* Maybe spawn the request */
			break;
		case QUE_DEVICE:
			dsc_spawn();	/* Maybe spawn the request */
			break;
		case QUE_PIPE:
			psc_spawn();	/* Maybe spawn the request */
			break;
		}
		if (queue->q.status & QUE_UPDATE) {
			/*
			 *  No requests were spawned from the queue in
			 *  which the request was just completely received.
			 *  Update the NQS database image of the queue.
			 */
			udb_qorder (queue);
		}
	}
	return (TCML_COMPLETE);		/* Return complete */
}


/*** pip_rmtaccept
 *
 *
 *	long pip_rmtaccept():
 *
 *	Record that the remote queue destination for a pipe queue
 *	request has tentatively accepted the request (transaction
 *	state = RTS_PREDEPART).
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful.
 *		TCML_NOSUCHREQ:	if the specified request does not
 *				exist on this machine.
 *
 */
long pip_rmtaccept (long orig_seqno, Mid_t orig_mid)
{
	struct request *predecessor;	/* Predecessor in request set in */
					/* queue */
	struct request *req;		/* Ptr to request structure for */
					/* located req, if found */
	int state;			/* Request state */

	/*
	 *  Search for the request.
	 */
	state = RQS_RUNNING;		/* Search only running state */
	if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) {
	    /*
	     *  The request was not found in any of the local queues.
	     */
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pip_rmtaccept: Couldn't find %d.%lu in running state.\n", orig_seqno, orig_mid);
	    state = -1;     /* look in all states */
	    if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) 
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_rmtaccept: Couldn't find anywhere.\n");
	    else 
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_rmtaccept: Found in state %d\n", state);
	    return (TCML_NOSUCHREQ);
	}
	/*
	 *  The request has been located.
	 */
	req->status |= RQF_PREDEPART;	/* Set the pre-depart flag */
	return (TCML_COMPLETE);		/* Return complete */
}


/*** pip_rmtarrive
 *
 *
 *	long pip_rmtarrive():
 *
 *	The specified tentatively queued request has been committed
 *	(transaction state = RTS_ARRIVE).
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful.
 *		TCML_NOSUCHREQ:	if the specified request does not
 *				exist on this machine.
 *
 */
long pip_rmtarrive (long orig_seqno, Mid_t orig_mid)
{
	struct request *predecessor;	/* Predecessor in request set in */
					/* queue */
	struct request *req;		/* Ptr to request structure for */
					/* located req, if found */
	int state;			/* Request state */

	/*
	 *  Search for the request.
	 */
        fflush(stdout);
	state = RQS_ARRIVING | RQS_WAITING;		
	                   /* Search only arriving state (and also waiting)*/
	if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) {
	    /*
	     *  The request was not found in any of the local queues.
	     */
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pip_rmtarrive: Couldn't find %d.%lu in arriving or waiting state.\n", orig_seqno, orig_mid);
	    state = -1;     /* look in all states */
	    if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) 
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_rmtarrive: Couldn't find anywhere.\n");
	    else 
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_rmtarrive: Found in state %d\n", state);
	    return (TCML_NOSUCHREQ);
	}
	/*
	 *  The request has been located.
	 */
	req->status &= ~RQF_PREARRIVE;	/* Clear the pre-arrive flag */
	return (TCML_COMPLETE);		/* Return complete */
}


/*** pip_rmtdepart
 *
 *
 *	long pip_rmtdepart():
 *
 *	Record that the remote queue destination for the specified
 *	pipe queue request has committed the request for arrival.
 *
 *	****TRIAGE****
 *	Until network queues are implemented, this procedure is
 *	a no-op.  When network queues are implemented, this
 *	procedure will clear the pre-depart flag for the request,
 *	and move the request into the depart set of the containing
 *	pipe queue.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful.
 *		TCML_NOSUCHREQ:	if the specified request does not
 *				exist on this machine.
 *
 */
long pip_rmtdepart (long orig_seqno, Mid_t orig_mid)
{
	struct request *predecessor;	/* Predecessor in request set in */
					/* queue */
	struct request *req;		/* Ptr to request structure for */
					/* located req, if found */
	int state;			/* Request state */

	/*
	 *  Search for the request.
	 */
	state = RQS_RUNNING;		/* Search only routing state */
	if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) {
	    /*
	     *  The request was not found in any of the local queues.
	     */
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pip_rmtdepart: Couldn't find %d.%lu in running state.\n", orig_seqno, orig_mid);
	    state = -1;     /* look in all states */
	    if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) 
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_rmtdepart: Couldn't find anywhere.\n");
	    else 
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_rmtdepart: Found in state %d\n", state);
	    return (TCML_NOSUCHREQ);
	}
	/*
	 *  The request has been located.
	 *
 	 *  ****TRIAGE****
	 *  Until network queues are implemented, this procedure is
	 *  a no-op.  When network queues are implemented, this
	 *  procedure will clear the pre-depart flag for the request,
	 *  and move the request into the depart set of the containing
	 *  pipe queue.
	 */
	return (TCML_COMPLETE);		/* Return complete */
}


/*** pip_rmtstasis
 *
 *
 *	long pip_rmtstasis():
 *
 *	Record that the remote queue destination for a pipe queue
 *	request has been aborted.  The request is now free to be
 *	targeted to any destination.  The request state WAS
 *	RTS_PREDEPART, and has now been restored to RTS_STASIS.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful.
 *		TCML_NOSUCHREQ:	if the specified request does not
 *				exist on this machine.
 *
 */
long pip_rmtstasis (long orig_seqno, Mid_t orig_mid)
{
	struct request *predecessor;	/* Predecessor in request set in */
					/* queue */
	struct request *req;		/* Ptr to request structure for */
					/* located req, if found */
	int state;			/* Request state */

	/*
	 *  Search for the request.
	 */
	state = RQS_RUNNING;		/* Search only running state */
	if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) {
	    /*
	     *  The request was not found in any of the local queues.
	     */
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pip_rmtstasis: Couldn't find %d.%lu in running state.\n", orig_seqno, orig_mid);
	    state = -1;     /* look in all states */
	    if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) 
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_rmtstasis: Couldn't find anywhere.\n");
	    else 
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "D$pip_rmtstasis: Found in state %d\n", state);
	    return (TCML_NOSUCHREQ);
	}
	/*
	 *  The request has been located.
	 */
	req->status &= (~RQF_PREDEPART); /* Clear the pre-depart flag */
	return (TCML_COMPLETE);
}


/*** pip_schedulereq
 *
 *
 *	long pip_schedulereq():
 *
 *	Adjust the in-memory version of the start-after time for the
 *	specified request WITHOUT modifying the start-after time
 *	defined in the request control file.  This allows pipe and
 *	network queues to delay delivery of a request until a more
 *	appropriate time.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful.
 *		TCML_NOSUCHREQ:	if the specified request does not
 *				exist on this machine.
 *
 */
long pip_schedulereq (long orig_seqno, Mid_t orig_mid, long waittime)
{
	struct request *predecessor;	/* Predecessor in request set in */
					/* queue */
	struct request *req;		/* Ptr to request structure for */
					/* located req, if found */
	int state;			/* Request state */
	struct rawreq rawreq;		/* Raw request structure. */
	int rawreq_fd;			/* Fd left when rawreq opened */
  
	/*
	 *  Search for the request.
	 */
	state = RQS_RUNNING;		/* Search only routing state */
	if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) {
	    /*
	     *  The request was not found in any of the local queues.
	     */
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pip_schedulereq: Couldn't find %d.%lu in running state.\n", orig_seqno, orig_mid);
	    state = -1;     /* look in all states */
	    if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) 
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_schedulereq: Couldn't find anywhere.\n");
	    else 
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "pip_schedulereq: Found in state %d\n", state);
	  
	    return (TCML_NOSUCHREQ);
	}
	/*
	 *  The request has been located.
 	 *  Set the start-after time
	 */
	if (req->status & RQF_AFTER) {
	    if ( (rawreq_fd = getrreq(orig_seqno,  orig_mid, &rawreq)) == -1) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Error reading rawreq,  Mid: %d,  Seqno: %d\n", orig_mid,  orig_seqno);
		req->start_time = time ((time_t *) 0) + waittime;
	    } else {
		req->start_time = rawreq.start_time;
		close (rawreq_fd);
	    }
	} else
	    req->start_time = time ((time_t *) 0) + waittime;

  	return (TCML_COMPLETE);		/* Return complete */
}


/*** pip_timeout
 *
 *
 *	void pip_timeout():
 *	Abort uncommitted timed-out remote transactions.
 */
void pip_timeout (void)
{

	register struct nqsqueue *queue;	/* Queue set walking */
	register struct request *preq;	/* Previous request */
	register struct request *req;	/* Request set walking */
	register struct request *next;	/* next request */
	register time_t timenow;	/* Current GMT time */
	time_t abortat;			/* Time to abort transaction at */

	queue = Nonnet_queueset;	/* Non-network queue set */
	timenow = time ((time_t *) 0);	/* Get current time */
	while (queue != (struct nqsqueue *) 0) {
	    preq = (struct request *) 0;
	    req = queue->arriveset;	/* Requests in the arriving set */
	    while (req != (struct request *) 0) {
		next = req->next;
		if (req->status & RQF_PREARRIVE) {
		    /*
		     *  This request is in the pre-arrive state.
		     */
		    abortat = req->v1.req.state_time+TRANS_TIMEOUT;
		    if (abortat <= timenow) {
			/*
			 *  This request was not committed for
			 *  arrival within the specified time
			 *  limits.  Abort the transaction.
			 */
		        abortrequest(req);
			if (preq == (struct request *) 0) {
			    queue->arriveset = req->next;
			}
			else preq->next = req->next;
			queue->q.status |= QUE_UPDATE;
					/* Database image is */
					/* now out of date */
			queue->q.arrivecount--; 	/* Dequeue request */
			nqs_disreq (req, 1);	        /* Delete data files */
		    }			                /* as well */
		    else {
			/*
			 *  Unless this request is committed
			 *  before the transaction timeout
			 *  time sometime in the future, this
			 *  request transaction will have to
			 *  be aborted.
			 */
			nqs_vtimer (&abortat, pip_timeout);
			preq = req;	/* Remember prev req */
		    }
		}
		else preq = req;/* Remember previous request */
		req = next;	/* Check the next request */
	    }
	    if (queue->q.status & QUE_UPDATE) {
		/*
		 *  The NQS database image of this queue is now
		 *  out of date, and needs to be updated.
		 */
		udb_qorder (queue);
	    }
	    queue = queue->next;	/* Check the next queue */
	}
}


/*** abortrequest
 *
 *
 *	void abortrequest():
 *
 *	Display debug diagnostic to report aborted remote request queueing
 *	transaction.
 */
static void abortrequest (struct request *request)
{
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Remote queueing transaction of %1ld.%s aborted.\n",
		request->v1.req.orig_seqno,
		fmtmidname (request->v1.req.orig_mid));
}
