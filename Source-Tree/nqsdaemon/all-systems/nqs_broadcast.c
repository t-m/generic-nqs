/*
 * nqsd/nqs_broadcast.c
 * 
 * DESCRIPTION:
 *
 *	This module contains the function:
 *
 *		nqs_broadcast()
 *
 *	which delivers a broadcast message either at the start of execution
 *	of an NQS job or the completion.
 *
 *	Original Author:
 *	-------
 *	John Roman, Monsanto Company.
 *	April 2, 1992.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <errno.h>
#include <unistd.h>			/* For getuid() */
#include <pwd.h>
#include <string.h>
#include <time.h>
#include <malloc.h>
#include <libnqs/requestcc.h>		/* NQS request completion codes */
#include <libnqs/nqsxvars.h>		/* Global vars */
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <SETUP/General.h>

/*** nqs_broadcast
 *
 *
 *	int nqs_broadcast()
 *
 *	Broadcast message regarding a specific NQS request and WAIT for
 *	the broadcast process to exit.
 *
 *	Similarities with nqs_mai are not a figment of your imagination.
 *	I started with that file to create this one.
 *
 *	WARNING:
 *		Nqs_broadcast() must NEVER be called at any time
 *		by the NQS daemon after the rebuild operation
 *		is complete, since this broadcast procedure waits
 *		for the spawned broadcast process to exit, which
 *		could result in the waiting for an exited
 *		request shepherd process, rather than the
 *		broadcast process.
 *
 *	Returns:
 *		0: if broadcast sent (does not guarantee delivery);
 *	       -1: if unsuccessful (diagnostic text is written
 *		   to the logfile).
 */
int nqs_broadcast (
	struct rawreq *rawreq,		/* Raw request structure for req */
	struct requestlog *requestlog,	/* Mail request log structure */
					/* (Can be NIL if suffixcode is */
					/*  one of MSX_BEGINNING or */
					/*  MSX_RESTARTING) */
	int suffixcode)			/* Subject suffix code */
{
	static char *suffixtab []= {
		"aborted",		/* MSX_ABORTED */
		"aborted for NQS shutdown",
					/* MSX_ABORTSHUTDN */
		"beginning",		/* MSX_BEGINNING */
		"deleted",		/* MSX_DELETED */
		"delivered",		/* MSX_DELIVERED */
		"ended",		/* MSX_ENDED */
		"failed",		/* MSX_FAILED */
		"requeued",		/* MSX_REQUEUED */
		"restarting",		/* MSX_RESTARTING */
		"status"		/* Bad MSX value maps to here */
	};
        FILE *domainfile;      		/* Domain file */
	char domainfile_name[64];	/* Name of the domain file */
        char hostname [256];          	/* Domain file line buffer */
	int found_orig_host = 0;	/* Flag make sure broadcast */
					/* to original host */

	char message [128];	/* Message to broadcast */
	int i;			/* Work var */
	struct tm *tp;		/* Break down time into parts */
	struct passwd *my_passwd;
	char *cp;
	int pid;		/* Process-id of child */
	time_t timeofday;	/* Time of day */
	char orig_host[64];     /* Name of original host */
	char *argv [100];	/* Arguments to shell */
	char *envp [5];		/* Environment variables for process that */
				/* will be used to send NQS mail */
	char home [64];		/* HOME=........................ */
#if	!HAS_BSD_ENV
	char logname [30];	/* LOGNAME=..................... */
#else
	char user [30];		/* USER=........................ */
#endif


	/*
	 *  Build message line.
	 */
	if (suffixcode < 0 || suffixcode > MSX_MAXMSX) {
		suffixcode = MSX_MAXMSX+1;
	}
	time (&timeofday);
	strcpy(orig_host, fmtmidname(rawreq->orig_mid) );
	tp = localtime(&timeofday);
	sprintf (message, "NQS request: %s (%1ld.%s) %s at %d:%2.2d on %s",
		rawreq->reqname,
		(long) rawreq->orig_seqno, 
		orig_host,
		suffixtab [suffixcode],
		tp->tm_hour, tp->tm_min, 
      		fmtmidname (Locmid));
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_broadcast: mid is %u\n", rawreq->orig_mid);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_broadcast: message is %s\n", message);
	if ((pid = fork()) == 0) {
		setsid();	/* set our own session */
		/*
		 *  We are child process.
		 *
		 *  Simulate enough of a login account to cause the
		 *  msg  program to operate in the necessary fashion.
		 */
		sprintf (home, "HOME=/" );
		envp [0] = home;	/* Record the home directory */
		envp [1] = "SHELL=/bin/sh";
					/* Default shell */
#if		!HAS_BSD_ENV
		envp [2] = "PATH=:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin";
		sprintf (logname, "LOGNAME=%s", rawreq->username);
		envp [3] = logname;
		envp [4] = (char *) 0;	/* No more environment variables */
#else
		envp [2] = "PATH=:/usr/ucb:/bin:/usr/bin:/usr/local/bin";
		sprintf (user, "USER=%s", rawreq->username);
		envp [3] = user;
		envp [4] = (char *) 0;	/* No more environment variables */
#endif
		argv [0] = "msg";
		argv [1] = "-s";               /* silently, silently */
		argv [2] = "-m";
		argv [3] = message;

		/* TAMU BUG FIX */
		i = 4;

		domainfile = (FILE *) 0;
                my_passwd = sal_fetchpwnam(rawreq->username);
		if ( my_passwd == NULL ) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_broadcast: unable to get passwd for %s\n",
			    rawreq->username);
		} else {
                    sprintf(domainfile_name,  "%s/.nqs-domain", my_passwd->pw_dir);
                    domainfile = fopen (domainfile_name, "r");
                }
		if (domainfile == (FILE *) 0) {
		    sprintf(domainfile_name, "%s/nqs-domain", NQS_LIBEXE);
		    domainfile = fopen (domainfile_name, "r");
		}
        	if (domainfile != (FILE *) 0) {
		     while ( (fgets(hostname, sizeof(hostname), domainfile) ) != 
			(char *) 0 ) { 
		        cp = strchr(hostname, '\n');
		        if (cp != (char *) 0 ) *cp = '\0';
		        if (hostname[0] == '#') continue;
			if ( !strcmp(hostname,  orig_host)) found_orig_host++;
		        cp = (char *)malloc(strlen(rawreq->username)+strlen(hostname)+2);
		        if (cp == NULL) {
			    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "nqs_broadcast:  malloc failed.\n");
		        }
		        sprintf(cp, "%s@%s", rawreq->username, hostname);
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_broadcast: sending to %s\n", cp);
		        argv [i] = cp;
		        i++;
		    }
		    fclose (domainfile);
		}
		/*
		 * If we did not find the original host, add it to list.
		 */
		if (!found_orig_host) {
		    cp = (char *)malloc(strlen(rawreq->username)+strlen(orig_host)+2);
		    if (cp == NULL) {
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "nqs_broadcast:  malloc failed.\n");
		    }
		    sprintf(cp, "%s@%s", rawreq->username, orig_host);
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_broadcast: sending to %s\n", cp);
		    argv [i] = cp;
		    i++;
      		}
		argv [i] = (char *) 0;
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_broadcast: here are the arguments:\n");
		for (i = 0; argv[i] != '\0'; i++) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_broadcast: argv[%d] is %s\n", i, argv[i]);
		}
		execve ("/usr/local/bin/msg", argv, envp);
					/* Execve() broadcast program */
		_exit (1);
	}
	else if (pid == -1) return (-1);	/* Fork failed */
	/*
	 *  The broadcast process was successfully created.
         *  Don't wait for the broadcast program to exit.
         */
        /*  while (wait ((int *)0) == -1 && errno == EINTR) 
         *       ;
	 */
	return (0);				/* Success */
}
