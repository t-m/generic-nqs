/*
 * nqsd/nqs_delrfs.h
 * 
 * DESCRIPTION
 * 
 *	Delete the control and data files for the specified
 *	request.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxdirs.h>			/* NQS global variables */
#include <string.h>
#include <unistd.h>

/*** void nqs_delrfs()
 *
 *
 *	void nqs_delrfs():
 *	Delete the specified control and data request files.
 */
void nqs_delrfs (
	long orig_seqno,	/* Request seq# */
	Mid_t orig_mid,		/* Request machine-id */
	short ndatafiles)	/* #of request data files */
{
	char path [MAX_PATHNAME+1];

	while (ndatafiles) {
		/*
		 *  Loop to delete data files first.
		 */
		--ndatafiles;
		pack6name (path, Nqs_data, (int) (orig_seqno % MAX_DATASUBDIRS),
			  (char *) 0, (long) orig_seqno, 5,
			  (long) orig_mid, 6, ndatafiles, 3);
		unlink (path);		/* Unlink request */
	}
	/*
	 *  Now, unlink the request control file.
	 */
	pack6name (path, Nqs_control, (int) (orig_seqno % MAX_CTRLSUBDIRS),
		  (char *) 0, (long) orig_seqno, 5, (long) orig_mid, 6, 0, 0);
	unlink (path);
        /*
         *  Now, unlink log file if it exists.
         */
        pack6name (path, Nqs_control, (int) (orig_seqno % MAX_CTRLSUBDIRS),
                  (char *) 0, (long) orig_seqno, 5, (long) orig_mid, 6, 0, 0);
        strcat (path,"l");
        unlink (path);
}
