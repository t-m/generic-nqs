/*
 * nqsd/nqs_bsc.c
 * 
 * DESCRIPTION:
 *
 *	This module contains the 3 functions:
 *
 *		bsc_reqcom()
 *		bsc_sched()
 *		bsc_spawn()
 *
 *	which control the scheduling, and spawning of NQS batch requests.
 *	This module can be modified to implement appropriate scheduling
 *	algorithms for a particular installation as necessary.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>		/* NQS global variables */
#include <libnqs/transactcc.h>          /* Transaction completion codes */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <nqsdaemon/features.h>

static int check_complex ( struct nqsqueue *queue, struct request *walkreq );
static int complex_userlimit ( uid_t uid, struct qcomplex *qcomplex );
static int userlimit ( uid_t uid, struct nqsqueue *queue );
static int userrun ( uid_t uid, struct nqsqueue *queue );
static int ver_check_complex ( struct nqsqueue *queue, uid_t uid );

/*** bsc_reqcom
 *
 *
 *	void bsc_reqcom():
 *
 *	This function is invoked whenever a running batch request completes
 *	execution.  This function is free to spawn more batch requests from
 *	ANY batch queue.
 *
 *	All queues modified by the invocation of this procedure have
 *	had their corresponding NQS database queue state image updated
 *	(to match the internal queue states) upon return from this
 *	procedure.
 */
void bsc_reqcom (struct request *request)
{

	/*
	 *  For the moment, we do not use any of the knowledge imparted
	 *  to us by the request that just finished execution.  Instead,
	 *  we spawn as many batch requests as we can, within the limits
	 *  configured.
	 */
	bsc_spawn();			/* Spawn all batch requests */
        /*
         *      Intergraph addition:
         *      should we spawn a device request
         */
        if (request->queue->v1.batch.qcomplex[0] != (struct qcomplex *) 0)
                dsc_spawn();
}					/* within configured boundaries */


/*** bsc_sched
 *
 *
 *	int bsc_sched():
 *
 *	This function is invoked whenever a batch req must be evaluated
 *	and assigned a priority by the NQS batch req scheduling policies
 *	(which are implemented by this function).
 *
 *	The priority assigned to the req must be in the interval [0..32767].
 *	All batch reqs with priority >= N within a given batch queue, will,
 *	depending upon the precise scheduling criteria defined in:
 *
 *		bsc_spawn()  and
 *		bsc_reqcom()
 *
 *	be spawned before any batch reqs in the same queue with a priority
 *	value < N.
 *	
 *	Returns:
 *		The assigned priority value for the specified batch request.
 */
int bsc_sched (struct rawreq *rawreq)
{
#if	ADD_NQS_CHECKBAL
	/* Call local accounting routine - if returns neg, set pri to 0 */
	if (nqs_checkbal(rawreq->orig_uid) < 0) {
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "sched: checkbal returned -1\n");
	    return(0);
	}
#endif
	if (rawreq->rpriority == -1) {
		/*
		 *  The user did not specify a priority; assign a
		 *  default value.
		 */
		return (Defbatpri);
	}
	return (rawreq->rpriority);	/* For now, just the intra-queue */
}					/* req priority */
#if	ADD_TAMU | ADD_NQS_DYNAMICSCHED

/*** bsc_resort
 *
 *   TAMU MOD - normally nqs does only a one time insertion sort
 *              as new requests are queued.  This new routine is
 *              called just before trying to spawn new requests
 *              (eg after a new request is queued, or a running
 *              job exits.) This provides for locally tailored
 *              dynamic scheduling.
 *              The sort priority is set by the local bsc_compare()
 */
void bsc_resort(void)
{
	register struct nqsqueue *queue;	/* Batch queue set walking */
	struct	 request *preq;			/* pointer to a request */
	struct	 request *nextp;		/* pointer to a request */
	struct	 request *lastp;		/* pointer to a request */
	int	done;

	/* for all batch queues */
	for ( queue = Pribatqueset;
              queue != (struct nqsqueue *) 0;
              queue = queue->v1.batch.nextpriority) {

	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "bsc_resort: Starting loop for queue %s.\n", queue->q.namev.name);

	    /* simple bubble sort - walk queue until no more changes */
	    for (;;) {
		done = 1;
	        /* for all waiting requests in the queue */
	        for ( preq = queue->queuedset,lastp = (struct request *) 0;
                      preq != (struct request *) 0;
		      preq = preq->next) {
		    if (preq->next != (struct request *) 0) {
		        if (bsc_compare(queue,preq,preq->next)) {
		            done=0;
			    queue->q.status |= QUE_UPDATE;
			    /* exchange requests */
			    nextp = preq->next;
			    if (lastp != (struct request *)0)
			        lastp->next = nextp;
			    else {
				/* preq was pointing to the head of the queue */
				queue->queuedset = nextp;
			    }
			    preq->next = nextp->next;
			    nextp->next = preq;
		        }
		    }
		    lastp = preq;
	        }
		if (done)
		    break;
	    }

	    /* if queue updated, write it out */
	    if (queue->q.status & QUE_UPDATE) {
		udb_qorder (queue);	/* Update image and clear */
	    }				/* the QUE_UPDATE bit */
	}

}

/*
 *	compare two requests and return relative priority (p2 - p1)
 *
 *	the original insertion sort simply places a new request at the
 *      bottom of all other requests of equal or higher priority.
 *      This comparison deprecates all "excess" requests within
 *      the same priority class (where excess is defined as more requests
 *      than the user_limit for the queue.)
 */
int bsc_compare(struct nqsqueue *queue, struct request *p1, struct request *p2)
{
	struct	 request *preq;		/* pointer to a request */
	int 	 runlimit;		/* more than this are "excess" */
	int 	 jobcount1;		/* count of user's jobs higher */
	int 	 jobcount2;		/* count of user's jobs higher */
  	int	 i;

	/* simple cases - differing priorities */
	if (p1->v1.req.priority > p2->v1.req.priority)
	    return (0);
	if (p1->v1.req.priority < p2->v1.req.priority)
	   return (1);

	/* priorities are same, so check "excess" status */
	runlimit = queue->q.v1.batch.userlimit;
	/* check the queue complexes this queue belongs to, to ensure
	 * that they don't have a lower user_limit */
  	for (i=0; i < MAX_COMPLXSPERQ; i++)
  	{
	  if (queue->v1.batch.qcomplex[i] != (struct qcomplex *) 0)
	    if (queue->v1.batch.qcomplex[i]->userlimit < runlimit)
	      runlimit = queue->v1.batch.qcomplex[i]->userlimit;
	}
  
	/* get jobcount for p1 */
	jobcount1 = 1;  /* count request we are checking */
	/* count user's jobs running in queue */
	for ( preq = queue->runset;
	      preq != (struct request *) 0;
	      preq = preq->next) {
	    if (preq->v1.req.uid == p1->v1.req.uid)
		jobcount1++;
	}
	/* count user's jobs queued higher in queue */
	for ( preq = queue->queuedset;
	      (preq != (struct request *) 0) && (preq != p1);
	      preq = preq->next) {
	    if (preq->v1.req.uid == p1->v1.req.uid)
		jobcount1++;
	}

	/* get jobcount for p2 */
	jobcount2 = 1;  /* count request we are checking */
	/* count user's jobs running in queue */
	for ( preq = queue->runset;
	      preq != (struct request *) 0;
	      preq = preq->next) {
	    if (preq->v1.req.uid == p2->v1.req.uid)
		jobcount2++;
	}
	/* count user's jobs queued higher in queue */
	for ( preq = queue->queuedset;
	      (preq != (struct request *) 0) && (preq != p1);
	      preq = preq->next) {
	    if (preq->v1.req.uid == p2->v1.req.uid)
		jobcount2++;
	}

	/* p1 is excess, p2 is not, so swap */
	if ((jobcount1 > runlimit)&&(jobcount2 <= runlimit))
	    return (1);

	/* p2 is excess, p1 is not, so leave */
	if ((jobcount2 > runlimit)&&(jobcount1 <= runlimit))
	    return (0);

	return (0);
}
#endif /* TAMU | NQS_DYNAMICSCHED */

/*** bsc_spawn
 *
 *
 *	void bsc_spawn():
 *
 *	This function is invoked whenever request activity indicates that
 *	it MAY be possible to spawn a batch request.  It is up to the
 *	discretion of the batch request scheduling/spawning algorithm
 *	implemented here to determine whether or not batch req(s) should
 *	be spawned.
 *
 *	All queues modified by the invocation of this procedure have
 *	had their corresponding NQS database queue state image updated
 *	(to match the internal queue states) upon return from this
 *	procedure.
 */
/* TAMU NOTES -- this is called following queueing a new request or completion
 *               of a running request. This is the logical place to put
 *               locally tailored dynamic queue sorting
 */
void bsc_spawn(void)
{
	register struct nqsqueue *queue;	/* Batch queue set walking */
	register int prevbatcount;	/* Prev loop value of Gblbatcount */
	struct	 request *preq;		/* pointer to a request */
  	struct   request *savenext;	/* ensure all requests get spawned */

	/*
	 *  Note that we are very careful to make sure that all batch
	 *  queues with higher priorities that also have batch requests
	 *  that can run, get to spawn first.  This becomes critical when
	 *  the number of batch requests that can run exceeds the maximum
	 *  number of batch requests that are allowed to simultaneously
	 *  execute.
	 */
	if (Shutdown) return;		/* Do not spawn any requests if */
					/* NQS is shutting down */
#if	ADD_TAMU | ADD_NQS_DYNAMICSCHED
	/* TAMU MOD -- add call to resort queues before spawning */
	bsc_resort();
#endif

	queue = Pribatqueset;		/* Prioritized batch queue set */
	prevbatcount = Gblbatcount - 1;	/* Make loop go at least once */
	for ( ; queue != (struct nqsqueue *) 0 &&
	    Maxgblbatlimit > Gblbatcount && prevbatcount != Gblbatcount;
	    queue = queue->v1.batch.nextpriority) {
	    /*
	     *  Spawn as many batch requests as we can for this queue.
	     */
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "bsc_spawn: Starting loop for queue %s.\n", queue->q.namev.name);
	    if (!(queue->q.status & QUE_RUNNING)) continue;
	    for (preq = queue->queuedset; preq != (struct request *) 0;
			preq = savenext) {
	        savenext = preq->next;
		if (queue->q.v1.batch.runlimit <= queue->q.runcount ) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "bsc_spawn: Rqst not scheduled due to queue runlimit.\n");
		    continue;
		}
		if ( Maxgblbatlimit <= Gblbatcount) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "bsc_spawn: Rqst not scheduled due to Gblbatcount.\n");
                    continue; 
                }
		if (check_complex (queue, preq)) continue;        /* and not break */
            	if (queue->q.v1.batch.userlimit <= 
		            userlimit (preq->v1.req.uid, queue)) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "bsc_spawn: Rqst not scheduled due to batch userlimits.\n");
		    continue;
		}
		/*
		 *  There is a batch request that can be spawned.
		 *  Try to spawn it.
		 *
		 *  Note that when spawning a batch request,
		 *  subrequests must be created and queued in
		 *  the appropriate network queues--which
		 *  themselves may have to be created.  It is
		 *  therefore possible that the successful
		 *  spawning of a batch request would require
		 *  more memory, than there is available.  In
		 *  such a situation, the spawn quietly fails.
		 *  By watching Gblbatcount, this procedure
		 *  can tell when such an event has occurred....
		 */
		prevbatcount = Gblbatcount;
		nqs_spawn (preq, (struct device *) 0);
	    }
	    if (queue->q.status & QUE_UPDATE) {
		/*
		 *  The database image of this queue must be
		 *  updated.
		 */
		udb_qorder (queue);	/* Update image and clear */
	    }				/* the QUE_UPDATE bit */
	}
}

static
int userlimit(uid_t uid, struct nqsqueue *queue)
{
        struct nqsqueue *walkqueue;
        struct request *walkreq;
        int number_running = 0;

        walkqueue = Pribatqueset;
        while (walkqueue != (struct nqsqueue *) 0) {
            if (queue == (struct nqsqueue *) 0 || strcmp (queue->q.namev.name,
			walkqueue->q.namev.name) == 0) {
                for (walkreq = walkqueue->runset;
                        walkreq != (struct request *) 0;
                        walkreq = walkreq->next)  {
                   if (walkreq->v1.req.uid == uid) number_running++; 
                }
            }
            walkqueue = walkqueue->v1.batch.nextpriority;
        }
        return(number_running);
}


/*** bsc_verqueusr
 *
 *
 *	long bsc_verqueusr():
 *	Verify user-limit for a queue.
 *
 *	This function is invoked whenever queue activity indicates that
 *	it MAY be possible to spawn a batch request. It is the main part
 *	of the load balancing mechanism. Note that the queue may be
 *	specified by a pointer to its structure or by name (requests
 *	coming through the network).
 *
 *	Returns:
 *		TCML_SUBMITTED if successful. Otherwise TCML_QUEBUSY.
 */
long bsc_verqueusr (
	uid_t uid,			/* User id */
	struct nqsqueue *queue,		/* Queue in question */
	char *que_name)			/* Queue name in question */
{
	struct nqsqueue *walkque;			/* Batch queue set walking */
	int count;

	/*
	 *  Find the queue structure if needed.
	 */
	if ((walkque = queue) == (struct nqsqueue *) 0)
		walkque = nqs_fndnnq (que_name);
	/*
	 *  Is there a free initiator ?
	 */
	if (Maxgblbatlimit <= Gblbatcount) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "bsc_verqueuser: Rqst not accepted due to global batch count\n");
	    return (TCML_QUEBUSY);
	}
	
	if ( walkque->q.v1.batch.runlimit <= (walkque->q.runcount +
	/*
	 *  All the arriving requests have to be counted as
	 *  running because with the load-balancing mechanism, on the local
	 *  batch queues, all of them should be running...
	 */
		walkque->q.arrivecount 
	/*
	 * Don't count the queued requests.  They are not running and
	 * may have a good reason for not running, like the other user
	 * has hit her run limit...
	 */
	    /* + walkque->q.queuedcount	*/
					) ) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "bsc_verqueuser: Rqst not accepted due to queue runlimit\n");
	    return (TCML_QUEBUSY);
	}
	count = userrun (uid,  walkque);
        if (walkque->q.v1.batch.userlimit <= count ) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "bsc_verqueuser: Rqst not accepted due to queue user limit\n");
	    return (TCML_QUEBUSY);
	}
	if ( !ver_check_complex(walkque, uid) ) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "bsc_verqueuser: Rqst not accepted due to complex limit\n");
	    return (TCML_QUEBUSY);
	}
	return (TCML_SUBMITTED);
}

/*** userrun
 *
 *      int userrun():
 *      Return the global or queue run count of the specified user.
 *
 */
static
int userrun (uid_t uid, struct nqsqueue *queue)
{
	register struct request *walkreq;	/* Queue request set walking */
	register int run_count;

	run_count = 0;
	for (walkreq = queue->runset; walkreq != (struct request *) 0;
	     walkreq = walkreq->next) {
		if (walkreq->v1.req.uid == uid)
			run_count++;		/* A match ! */
	}
	/*
	 * And also check the arriving requests as well.
	 */
	for (walkreq = queue->arriveset; walkreq != (struct request *) 0;
	     walkreq = walkreq->next) {
		if (walkreq->v1.req.uid == uid)
			run_count++;		/* A match ! */
	}
	return (run_count);
}
/*
 *   check_complex()
 *
 *   Check complex limits to see if this request can run.
 *
 *   Return TRUE if cannot, FALSE otherwise.
 *
 */
static
int check_complex(
	struct nqsqueue *queue,
	struct request *walkreq)
{
    int i;
    struct qcomplex *qcomplex;

    for (i = MAX_COMPLXSPERQ; --i >= 0;) {
        qcomplex = queue->v1.batch.qcomplex[i];
        if (qcomplex == (struct qcomplex *)0) continue;
        if (qcomplex->runlimit <= qcomplex->runcount) {
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "bsc_spawn: Rqst not scheduled due to complex run limits.\n");
	    return (1);
	}
	if (qcomplex->userlimit <= complex_userlimit(walkreq->v1.req.uid, qcomplex)){
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "bsc_spawn: Rqst not scheduled due to complex user limits.\n");
	    return (1);
	}
    }
    return (0);
}
/*
 *   ver_check_complex()
 *
 *   Check complex limits to see if this request can run.  This version
 *   is especially for bsc_verqueusr,  above.
 *
 *   Return TRUE if can, FALSE otherwise.
 *
 */
static
int ver_check_complex(struct nqsqueue *queue, uid_t uid)
{
    int i;
    struct qcomplex *qcomplex;

    for (i = MAX_COMPLXSPERQ; --i >= 0;) {
        qcomplex = queue->v1.batch.qcomplex[i];
        if (qcomplex == (struct qcomplex *)0) continue;
        if (qcomplex->runlimit <= qcomplex->runcount) {
	    return (0);
	}
	if (qcomplex->userlimit <= complex_userlimit(uid, qcomplex)){
	    return (0);
	}
    }
    return (1);
}
/*
 * complex_userlimit()
 *
 * Count the number of requests running by this user in this queue complex.
 * Return the number of running requests in this complex.
 *
 */
static
int complex_userlimit(uid_t uid, struct qcomplex *qcomplex)
{
    int i;
    int run_count;

    run_count = 0;
    for ( i = 0; i < MAX_QSPERCOMPLX; i++) {
	if (qcomplex->queue[i] != (struct nqsqueue *) 0)
	    run_count += userlimit(uid, qcomplex->queue[i]);
    }
    return (run_count);
}
