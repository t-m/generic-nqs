/*
 * nqsd/nqs_mai.c
 * 
 * DESCRIPTION:
 *
 *	This module contains the two functions:
 *
 *		mai_send(),	and
 *		mai_outfiles()
 *
 *	which respectively send an NQS mail message (for request completion
 *	events), and diagnose the need to send mail concerning the disposition
 *	of output file events (for batch requests).
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	May 2, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>
#include <string.h>
#include <libnqs/requestcc.h>			/* NQS request completion codes */
#include <libnqs/nqsxvars.h>			/* Global vars */
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#define	MAX_SUBJECT_SUFFIX	50	/* Maximum length of subject suffix */

/*** mai_send
 *
 *
 *	int mai_send():
 *
 *	Send mail regarding a specific NQS request
 *
 *	WARNING:
 *		Nqs_mail() must NEVER be called at any time
 *		by the NQS daemon after the rebuild operation
 *		is complete, since this mail procedure waits
 *		for the spawned mail process to exit, which
 *		could result in the waiting for an exited
 *		request shepherd process, rather than the
 *		mail process.
 *              This is no longer true (I think).  The nqs_mail()
 *              no longer waits, instead its SIGNAL gets caught
 *              by child_catcher installed in nqs_boot.
 *
 *	Returns:
 *		0: if mail sent (does not guarantee delivery);
 *	       -1: if unsuccessful (diagnostic text is written
 *		   to the logfile).
 */
int mai_send (
	struct rawreq *rawreq,		/* Raw request structure for req */
	struct requestlog *requestlog,	/* Mail request log structure */
					/* (Can be NIL if suffixcode is */
					/*  one of MSX_BEGINNING or */
					/*  MSX_RESTARTING) */
	int suffixcode)			/* Subject suffix code */
{
	static char no_mailsent[]= "No mail sent.\n";
	static char lostbycrash[]= "information lost due to a system crash.\n";
	static char *suffixtab []= {
		"aborted",		/* MSX_ABORTED */
		"aborted for NQS shutdown",
					/* MSX_ABORTSHUTDN */
		"beginning",		/* MSX_BEGINNING */
		"deleted",		/* MSX_DELETED */
		"delivered",		/* MSX_DELIVERED */
		"ended",		/* MSX_ENDED */
		"failed",		/* MSX_FAILED */
		"requeued",		/* MSX_REQUEUED */
		"restarting",		/* MSX_RESTARTING */
		"status"		/* Bad MSX value maps to here */
	};
	static char stderr_dest[]  = "  Destination: -e %s:%s\n";
	static char stderr_dispos[]= "\n  Stderr return disposition ";
	static char stderr_status[]= "\n  Stderr file staging event status:\n";
	static char stdout_dest[]  = "  Destination: -o %s:%s\n";
	static char stdout_dispos[]= "\n  Stdout return disposition ";
	static char stdout_status[]= "\n  Stdout file staging event status:\n";

	char maildest [MAX_ACCOUNTNAME+1+MAX_MACHINENAME+1];
	char subject [19 + MAX_MACHINENAME + MAX_SUBJECT_SUFFIX + 2];
				/* Subject line for mail */
				/* 13 chars for "NQS request: "; */
				/* 5  chars for request-id #; */
				/* 1  char for "." between  */
				/*    request-id# and the machine */
				/*    name; */
				/* MAX_MACHINENAME chars for */
				/*    machine name; */
				/* MAX_SUBJECT_SUFFIX chars for */
				/*    the subject suffix; */
				/* 1  char for trailing "."; */
				/*    and */
				/* 1  delimiting null char. */
	long i;			/* Work var */
	char *machinename;	/* Machine name */
	FILE *mailfile;		/* Mail file */
	uid_t mail_account;	/* Account used to send mail */
	int p[2];		/* Read/write pipe descriptors */
	int pid;		/* Process-id of child */
	struct passwd *pw_entry;/* Password file entry for NQS mail account */
	time_t timeofday;	/* Time of day */
	char *argv [3];		/* Arguments to shell */
	char *envp [5];		/* Environment variables for process that */
				/* will be used to send NQS mail */
	char home [64];		/* HOME=........................ */
#if	!HAS_BSD_ENV
	char logname [30];	/* LOGNAME=..................... */
/*
 *	char mail [30];*/		/* MAIL=/usr/mail/.............. */
/**/
#else
/*
 *	char mail [35];*/		/* MAIL=/usr/spool/mail/........ */
/**/
	char user [30];		/* USER=........................ */
#endif


	/*
	 *  Build subject line.
	 */
	if (suffixcode < 0 || suffixcode > MSX_MAXMSX) {
		suffixcode = MSX_MAXMSX+1;
	}
	sprintf (subject, "NQS request:  %1ld.%s %s.",
		(long) rawreq->orig_seqno, fmtmidname (rawreq->orig_mid),
		suffixtab [suffixcode]);
	/*
	 *  Determine mail machine address.
	 */
	if (Locmid == rawreq->mail_mid) {
		/*
		 *  We are going to be sending mail to the same
		 *  machine.
		 */
		sprintf (maildest, "%s", rawreq->mail_name);
	}
	else {
		/*
		 *  Get the name of the machine to which ALL mail should be
		 *  sent concerning the NQS request, where the mail machine
		 *  DIFFERS from the local machine.
		 */
		machinename = nmap_get_nam (rawreq->mail_mid);
					/* Get hostname as viewed from */
					/* local machine */
		if (machinename == (char *) 0) {
			/*
			 *  Cannot determine host name for mid.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to get hostname for mid: %1u\n%s", (long) rawreq->mail_mid, no_mailsent);
			return (-1);
		}
		sprintf (maildest, "%s@%s", rawreq->mail_name, machinename);
	}
	if (pipe (p) == -1) return (-1);
	fflush (stdout);	/* Purge any diagnostic messages prior to */
	fflush (stderr);	/* flush */
	if ((pid = fork()) == 0) {
		/*
		 *  We are child process.
		 *
		 *  Simulate enough of a login under the NQS mail
		 *  account, to cause the mail program to operate
		 *  in the necessary fashion.
		 */
		mail_account = Mail_uid;
		if ((pw_entry = sal_fetchpwuid ((int) mail_account))
			== (struct passwd *) 0) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Non-existent mail account encountered by mai_send().\nUsing root account to send mail.\n");
			mail_account = 0;	/* Use root account */
			if ((pw_entry = sal_fetchpwuid ((int) mail_account))
				== (struct passwd *) 0) {
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "No password entry for root account in mai_send().\n%s", no_mailsent);
			}
		  	return -1;
		}
		/*
		 *  Change directory to the home directory of the
		 *  NQS mail account (or default mail account).
		 */
		if (chdir (pw_entry->pw_dir) == -1) {
			/*
			 *  We were unable to chdir() to the home
			 *  directory of the NQS mail account.
			 */
			if (mail_account == 0)
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to chdir to root account home directory in mai_send().\n%s", no_mailsent);
			else 
		    		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to chdir() to NQS mail account home directory in mai_send().\n%s", no_mailsent);
			return -1;
		}
		sprintf (home, "HOME=%s", pw_entry->pw_dir);
		envp [0] = home;	/* Record the home directory */
		envp [1] = "SHELL=/bin/sh";
					/* Default shell */
#if		!HAS_BSD_ENV
		envp [2] = "PATH=:/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/sbin:/usr/local/bin";
		sprintf (logname, "LOGNAME=%s", pw_entry->pw_name);
		envp [3] = logname;
		envp [4] = (char *) 0;	/* No more environment variables */
#else
		envp [2] = "PATH=:/usr/ucb:/bin:/usr/bin";
		sprintf (user, "USER=%s", pw_entry->pw_name);
		envp [3] = user;
		envp [4] = (char *) 0;	/* No more environment variables */
#endif
		/*
		 *  Set our user-id to the account used to send
		 *  NQS mail.
		 */
		if (setuid ((int) mail_account) == -1) {
			/*
			 *  Unable to set-uid to the NQS mail account.
			 *  No mail will be sent.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to setuid() in mai_send().\n%s", no_mailsent);
		  	return -1;
		}
		close (p[1]);			/* Close writing descr */
		close (0);			/* Close stdin */
		if (fcntl (p[0], F_DUPFD, 0) == -1) _exit (1);
		/*
		 *  Note that we leave stdout and stderr open.
		 *  If mail decides to be vocal, then the logfile
		 *  will get some bad messages.
		 *
		 *  We leave stdout and stderr open anyway, because
		 *  some versions of mail(1) wrap themselves up into
		 *  a marvelous infinite loop if stdout or stderr
		 *  is closed.  Sigh....
		 */
		i = sysconf ( _SC_OPEN_MAX );	/* #of file descrs per proc */
		while (--i >= 3) close (i);	/* Close all other files */
		argv [0] = "mail";
		argv [1] = maildest;
		argv [2] = (char *) 0;
		execve ("/bin/mail", argv,	/* Execve() mail program */
			envp);
		_exit (1);
	}
	else if (pid == -1) return (-1);	/* Fork failed */
	close (p[0]);				/* Close read descr */
	if ((mailfile = fdopen (p[1], "w")) == (FILE *) 0) {
		/*
		 *  The mail process was successfully created,
		 *  but we were unable to create a buffered
		 *  mailfile FILE stream.
		 *
		 *  Mail program now gets caught by SIGCHLD catch routine
		 *  that is installed in nqs_boot(), so we just return.
		 */
		close (p[1]);			/* Close write descr */
		return (-1);			/* Failure */
	}
	fprintf (mailfile, "Subject:  %s\n\n", subject);
	fprintf (mailfile, "Request name:   %s\n", rawreq->reqname);
	fprintf (mailfile, "Request owner:  %s\n", rawreq->username);
	time (&timeofday);
	fprintf (mailfile, "Mail sent at:   %s\n\n", fmttime (&timeofday));
	if (requestlog != (struct requestlog *) 0) {
		/*
		 *  We are sending non-trivial mail.
		 */
		if (requestlog->reqrcmknown) {
			analyzercm (requestlog->svm.rcm, SAL_DEBUG_MSG_WARNING, "");
		}
		else {
			fputs ("Request completion", mailfile);
			fputs (lostbycrash, mailfile);
		}
		if (requestlog->svm.mssg [0] != '\0') {
			/*
			 *  A server message is also present.
			 */
			fprintf (mailfile, "\n%s\n", requestlog->svm.mssg);
		}
		if (rawreq->type == RTYPE_BATCH &&
		    mai_outfiles (rawreq, requestlog)) {
			/*
			 *  We are sending mail about a batch request, and
			 *  output return transaction results should be
			 *  discussed.
			 *  
			 *  Note that for the moment, the general
			 *  implementation of file staging is not
			 *  complete....
			 */
			if (rawreq->v.bat.stdout_acc & OMD_SPOOL) {
			    /*
			     *  It was necessary to return the stdout file
			     *  produced by the batch request.
			     *
			     *  Remember:	Any stdout return transaction
			     *			is file output staging event
			     *			30.
			     */
			    if (requestlog->outrcmknown & (1L << 30)) {
				/*
				 *  The results of the stdout return
				 *  transaction are known.
				 */
				if (requestlog->outrcm [30] != RCM_STAGEOUT) {
				    /*
				     *  The return of the stdout file was
				     *  not entirely successful.
				     */
				    fputs (stdout_status, mailfile);
				    fprintf (mailfile, stdout_dest,
					     fmtmidname (
						rawreq->v.bat.stdout_mid
					     ),
					     rawreq->v.bat.stdout_name);
				    analyzercm (requestlog->outrcm [30],
						SAL_DEBUG_MSG_WARNING, "    ");
				}
			    }
			    else {
				fputs (stdout_dispos, mailfile);
				fputs (lostbycrash, mailfile);
			    }
			}
			if ((rawreq->v.bat.stderr_acc & OMD_SPOOL) &&
			   !(rawreq->v.bat.stderr_acc & OMD_EO)) {
			    /*
			     *  It was necessary to return the stderr file
			     *  produced by the batch request.
			     *
			     *  Remember:	Any stderr return transaction
			     *			is file output staging event
			     *			31.
			     */
			    if (requestlog->outrcmknown & 0x8000000) {
				/*
				 *  The results of the stderr return
				 *  transaction are known.
				 */
				if (requestlog->outrcm [31] != RCM_STAGEOUT) {
				    /*
				     *  The return of the stderr file was
				     *  not entirely successful.
				     */
				    fputs (stderr_status, mailfile);
				    fprintf (mailfile, stderr_dest,
					     fmtmidname (
						rawreq->v.bat.stderr_mid
					     ),
					     rawreq->v.bat.stderr_name);
				    analyzercm (requestlog->outrcm [31],
						SAL_DEBUG_MSG_WARNING, "    ");
				}
			    }
			    else {
				fputs (stderr_dispos, mailfile);
				fputs (lostbycrash, mailfile);
			    }
			}
		}
	}
        /* Intergraph change Bill Mar   12/08/89 TAC */
        if (rawreq->type == RTYPE_DEVICE) {
                char * logfile;
                struct stat sbuf;
                int fd;

                logfile = namstdlog(rawreq->orig_seqno, rawreq->orig_mid);
                if ((stat(logfile,&sbuf) != -1) &&
                    (sbuf.st_size != 0) &&
                    ((fd=open(logfile, O_RDONLY)) != -1)) {
                        fprintf(mailfile,
                                "\nThe job produced the following errors:\n\n");
                        fflush(mailfile);
                        filecopyentire(fd,fileno(mailfile));
                }
        }

	fclose (mailfile);			/* Close pipe */
	/*
	 *  The mail program exit should  get caught by the SIGCHLD
	 *  handler installed in nqs_boot().  So just exit.
	 */
	return (0);				/* Success */
}


/*** mai_outfiles
 *
 *
 *	int mai_outfiles():
 *
 *	Return non-zero if there is output file information to be
 *	analyzed.
 */
int mai_outfiles (
	struct rawreq *rawreq,			/* Raw request */
	struct requestlog *requestlog)		/* Request log */
{
	if (rawreq->type != RTYPE_BATCH) {
		/*
		 *  Only batch requests have any kind of output file
		 *  information.
		 */
		return (0);
	}
	/*
	 *  The request is a batch request.
	 */
	switch (requestlog->svm.rcm) {
	case RCM_ABORTED:
	case RCM_EXITED:
	case RCM_MIDUNKNOWN:
	case RCM_NOACCAUTH:
	case RCM_NORESTART:
	case RCM_SHEXEF2BIG:
	case RCM_SHUTDNABORT:
	case RCM_SSHBRKPNT:
	case RCM_SSHEXEFAI:
	case RCM_UNABLETOEXE:
	case RCM_UNCRESTDERR:
	case RCM_UNCRESTDOUT:
	case RCM_USHBRKPNT:
	case RCM_USHEXEFAI:
		if (rawreq->v.bat.stdout_acc & OMD_SPOOL) {
			/*
			 *  It was necessary to return the stdout file
			 *  produced by the batch request.
			 *
			 *  Remember:	Any stdout return transaction
			 *		is file output staging event 31.
			 */
			if (requestlog->outrcmknown & (1L << 30)) {
				/*
				 *  The results of the stdout return
				 *  transaction are known.
				 */
				if (requestlog->outrcm [30] != RCM_STAGEOUT) {
					/*
					 *  Something went wrong with the
					 *  return of the stdout file.
					 */
					return (1);
				}
			}
			else {
				/*
				 *  Information concerning the return
				 *  of the stdout file was lost.
				 */
				return (1);
			}
		}
		if ((rawreq->v.bat.stderr_acc & OMD_SPOOL) &&
		   !(rawreq->v.bat.stderr_acc & OMD_EO)) {
			/*
			 *  It was necessary to return the stderr file
			 *  produced by the batch request.
			 *
			 *  Remember:	Any stderr return transaction
			 *		is file output staging event 31.
			 */
			if (requestlog->outrcmknown & 0x80000000) {
				/*
				 *  The results of the stderr return
				 *  transaction are known.
				 */
				if (requestlog->outrcm [31] != RCM_STAGEOUT) {
					/*
					 *  Do not say anything unless
					 *  something went wrong.
					 */
					return (1);
				}
			}
			else {
				/*
				 *  Information concerning the return
				 *  of the stderr file was lost.
				 */
				return (1);
			}
		}
		break;
	}
	return (0);			/* No mail was sent */
}
