/*
 * nqsd/nqs_reqcom.c
 * 
 * DESCRIPTION:
 *
 *	Process an NQS request completion event.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <errno.h>
#include <pwd.h>
#include <string.h>
#include <libnqs/nqsxvars.h>		/* NQS global variables */
#include <libnqs/nqsacct.h>
#include <libnqs/netpacket.h>		/* For NPK_RREQCOM */
#include <libnqs/transactcc.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <unistd.h>

static int get_req_count ( void );
static void requeue ( struct request *req );
static void stopallqueues ( struct nqsqueue *serverq );
static void stopdev ( struct device *device, struct nqsqueue *serverq );
static void tell_nqs_scheduler ( int orig_seqno, Mid_t orig_mid, struct request *request );

/*** nqs_reqcom
 *
 *
 *	void nqs_reqcom():
 *	Process an NQS request completion event.
 */
#if	IS_POSIX_1 | IS_SYSVr4
void nqs_reqcom (
 	long orig_seqno,		/* Request original sequence# */
 	Mid_t orig_mid,			/* Request original machine-id */
 	int exitcode,			/* Exit code */
 	struct tms *tms,		/* CPU time usage by request */
 	char *queuename)		/* Queuename for which request */
#else
#if	IS_BSD
void nqs_reqcom (
 	long orig_seqno,		/* Request original sequence# */
 	Mid_t orig_mid,			/* Request original machine-id */
 	int exitcode,			/* Exit code */
 	struct rusage *rusage,		/* Resource utilization by request */
 	char *queuename)		/* Queuename for which request */
#else
BAD SYSTEM TYPE
#endif
#endif
{

	short free_request_files = 0;	/* BOOLEAN delete files associated */
					/* with the request flag.  This */
					/* flag is NEVER true without */
					/* free_request_struct ALSO being */
					/* true */
	short free_request_struct = 0;	/* BOOLEAN delete request struct */
					/* for request flag */
	register struct nqsqueue *serverq;	/* Server was handling this queue */
	register struct request *req = NULL;	/* Request structure */
	struct request *predecessor;	/* Predecessor in request set in */
					/* queue */
	register struct device *device = NULL;	/* Device handling device request */
        register struct qcomplex *qcomplex; /* Queue complex pointer */
        register int i;
        int fd_acct;                    /* Accounting file descriptor */
        struct nqsacct_fin1 acct_fin1;  /* Accounting structure to report */
                                        /* Cpu usage                      */


	/*
	 *  Locate the queue containing the completed request.
	 */
	serverq = nqs_fndnnq (queuename);
	if (serverq == (struct nqsqueue *) 0) {
		/*
		 *  Bad queuename reported by shepherd!
		 */
	  	errno = 0;
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Bad queue name '%s' reported by request shepherd.\n", queuename);
	}
	/*
	 *  Update resource usage statistics.
	 */
#if	IS_POSIX_1 | IS_SYSVr4
	/*
	 *  Update system call time.
	 */
	serverq->q.ru_stime += tms->tms_stime;
	serverq->q.ru_stime += tms->tms_cstime;
	/*
	 *  Update user-space time.
	 */
	serverq->q.ru_utime += tms->tms_utime;
	serverq->q.ru_utime += tms->tms_cutime;
#else
#if	IS_BSD
	/*
	 *  Update system call time.
	 */
  	/* Generic NQS 3.50.0
	 * 
	 * For some reason unknown to me, accessing the contents of
	 * rusage on SunOS 4 results in a BUS error (alignment error).
	 * To prevent this happening, the symbol HAS_RUSAGE_BUSERROR
	 * has been added to prevent this code from being used.
	 */
  
#ifndef	HAS_RUSAGE_BUSERROR
	serverq->q.ru_stime_usec += rusage->ru_stime.tv_usec;
	if (serverq->q.ru_stime_usec > 1000000) {
		serverq->q.ru_stime_usec -= 1000000;
		serverq->q.ru_stime += 1;
	}
	serverq->q.ru_stime += rusage->ru_stime.tv_sec;
	/*
	 *  Update user-space time.
	 */
	serverq->q.ru_utime_usec += rusage->ru_utime.tv_usec;
	if (serverq->q.ru_utime_usec > 1000000) {
		serverq->q.ru_utime_usec -= 1000000;
		serverq->q.ru_utime += 1;
	}
	serverq->q.ru_utime += rusage->ru_utime.tv_sec;
#endif
#else
BAD SYSTEM TYPE
#endif
#endif
	/*
	 *  If the request was routed by a pipe queue, then we must
	 *  do quite a bit of extra work....
	 */
	if ((exitcode & 0003) == 3) {
		/*
		 *  The request is presently in the arriving state and
		 *  was successfully routed by a local pipe queue.
		 *  Locate the request, and modify the state of the
		 *  request as appropriate (provided that the request
		 *  has not been deleted).
		 */
		pip_reqreceived (orig_seqno, orig_mid);
	}
	else {
		/*
		 *  The request is presently still in the running state.
		 */
		predecessor = (struct request *) 0;
		req = serverq->runset;
		while (req != (struct request *) 0 &&
		      (req->v1.req.orig_seqno != orig_seqno ||
		       req->v1.req.orig_mid != orig_mid)) {
			/*
			 *  Keep searching.
			 */
			predecessor = req;	/* Remember predecessor */
			req = req->next;	/* Examine the next request */
		}
		if (req == (struct request *) 0) {
			/*
			 *  Request not found!
			 */
		  	errno = 0;
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Request not found in nqs_reqcom().\n");
		}
		/*
		 *  Remove the request from its current position in the
		 *  queue.
		 */
		if (predecessor == (struct request *) 0) {
			serverq->runset = req->next;
		}
		else predecessor->next = req->next;
		serverq->q.runcount--;		/* One less running request */
                if (serverq->q.type == QUE_BATCH) {
                        for (i = MAX_COMPLXSPERQ; --i >= 0;) {
                                qcomplex = serverq->v1.batch.qcomplex[i];
                                if (qcomplex == (struct qcomplex *)0) continue;
                                qcomplex->runcount--;
                        }
                }
                /* addition by Intergraph Bill Mar - 122/08/89 TAC */
                if (serverq->q.type == QUE_DEVICE) {
                        for (i = MAX_COMPLXSPERQ; --i >= 0;) {
                                qcomplex = serverq->v1.device.qcomplex[i];
                                if (qcomplex == (struct qcomplex *)0) continue;
                                qcomplex->runcount--;
                        }
                }
	  	/* addition by SLH for GNQS v3.50.0 */
                if (serverq->q.type == QUE_PIPE) {
                        for (i = MAX_COMPLXSPERQ; --i >= 0;) {
                                qcomplex = serverq->v1.pipe.qcomplex[i];
                                if (qcomplex == (struct qcomplex *)0) continue;
                                qcomplex->runcount--;
                        }
                }

		serverq->q.status |= QUE_UPDATE;/* Database update required */
		/*
		 *  Free up the allocated entry in the Runvars[] array for the
		 *  recently completed request.
		 */
		(Runvars + req->reqindex)->allocated = 0;
	}
	/*
	 *  Req points to the request structure for the request that
	 *  just sent its request completion packet to us.
	 *
	 *  Serverq points to the queue structure within which a
	 *  completed request is queued (or was queued).
	 */
        /* --------------------------------------
         * Report cpu, usage etc. for a batch queue;
         */
        if (serverq -> q.type == QUE_BATCH ) {
                fd_acct = open(NQSACCT_FILE, O_WRONLY|O_APPEND|O_CREAT, 0644);
                if (fd_acct < 0) {
                        sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Error opening NQS account file;  Errno = %d\n", errno);
                } else {
                        sal_bytezero((char *)&acct_fin1, sizeof(acct_fin1));
                        acct_fin1.h.type = NQSACCT_FIN_1;
                        acct_fin1.h.length = sizeof(acct_fin1);
                        acct_fin1.h.jobid = (Runvars + req->reqindex)->process_family;
                        strncpy(acct_fin1.user,getusenam(req->v1.req.uid),
                                sizeof(acct_fin1.user));
                        strncpy(acct_fin1.queue, serverq->q.namev.name,
                                sizeof(acct_fin1.queue));
#if     IS_POSIX_1 | IS_SYSVr4
                        acct_fin1.tms_stime = tms->tms_stime + tms->tms_cstime;
                        acct_fin1.tms_utime = tms->tms_utime + tms->tms_cutime;
#else
#if     IS_BSD
                        acct_fin1.s_sec = rusage->ru_stime.tv_sec;
                        acct_fin1.s_usec = rusage->ru_stime.tv_usec;
                        acct_fin1.u_sec = rusage->ru_utime.tv_sec;
                        acct_fin1.u_usec = rusage->ru_utime.tv_usec;
#else
BAD SYSTEM TYPE
#endif
#endif
                        acct_fin1.orig_mid = req->v1.req.orig_mid;
			acct_fin1.seqno = req->v1.req.orig_seqno;
			acct_fin1.fin_time = time ((time_t *) 0);
                }
                write(fd_acct, &acct_fin1, sizeof(acct_fin1));
                close(fd_acct);
        }
        /*-----------------------*/

	if (serverq->q.type == QUE_DEVICE) {
		/*
		 *  Identify the device that was servicing the recently
		 *  completed request.  The internal representation of
		 *  the device structure is updated to indicate that
		 *  the device is inactive, and that no current request
		 *  is being serviced by the device.  However, the NQS
		 *  database image for the device is NOT updated.
		 */
		device = Devset;
		while (device != (struct device *) 0 &&
		       device->curreq != req) {
			device = device->next;
		}
		if (device == (struct device *) 0) {
			/*
			 *  We did not find the device serving what
			 *  supposedly was a completed device req!
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "NQS internal error.\nUnable to find device that handled completed device request.\n");
		}
		device->status &= ~DEV_ACTIVE;	/* Device no longer active */
		device->curreq = (struct request *) 0;
						/* No current request */
		device->curque = (struct nqsqueue *) 0;
						/* No current queue */
	}
	/*
	 *  Based upon the request exitcode status, we must do
	 *  several things.  The exitcode bits have the following
	 *  definitions:
	 *
	 *  .---------------.
 	 *  |   |     | |   |	Request server shepherd process
	 *  |7|6|5|4|3|2|1|0|	exit code (of 8 bits)
	 *  `---------------'
	 *
	 *  Bits 1..0: contain the request disposition code:
	 *		0: Delete the request, provided that bit 2
	 *		   is clear, or the request is not to be
	 *		   requeued upon signal termination, or the
	 *		   request is not restartable.  Otherwise,
	 *		   the request must be requeued for restart.
	 *		1: Place the request in the failed directory;
	 *		2: Requeue the request for retry.
	 *		3: Request queued via local pipe queue.
	 *
	 *  Bit  2:    exit status:
	 *		0: Request exited via exit();
	 *		1: Request aborted by receipt of signal.
	 *
	 *  Bits 5..3: contain additional action bits which can
	 *	       be or'ed together as necessary.
	 *		1: Stop the device, that the request was
	 *		   running on, marking the device as
	 *		   failed;
	 *		2: Stop the queue that the request was
	 *		   (or is) residing in;
	 *		4: Stop all queues.
	 *
	 *  Bits 7..6: RESERVED for future use.
	 *
	 */
	switch (exitcode & 0003) {	/* Bits 1..0 */
	case 0: /*
		 *  Request completed normally.
		 */
		if ((req->status & RQF_SIGREQUEUE) &&
		    (req->status & RQF_RESTARTABLE) &&
		    (exitcode & 0004)) {
			/*
			 *  Requeue the request as ready to run.
			 *  The request must not be deleted.
			 */
			free_request_files = 0;
			free_request_struct = 0;
			requeue (req);
		}
		else {
			/*
			 *  The request should be deleted--later
			 *  (see below).
			 */
			free_request_files = 1;
			free_request_struct = 1;
		}
		break;
	case 1:	/*
		 *  The request is to be placed in the failed
		 *  directory.  The files associated with the
		 *  the request should be saved, but the memory-
		 *  resident information on the request should
		 *  be discarded.
		 */
		free_request_files = 0;		/* Save request files but */
		free_request_struct = 1;	/* free request structure */
		break;				/* Place req. in failed dir */
	case 2:	/*
		 *  The request is to be requeued for retry.
		 */
		free_request_files = 0;		/* The request must not */
		free_request_struct = 0;	/* be deleted. */
		requeue (req);			/* Requeue */
		break;
	case 3:	/*
		 *  The request is to be simply left-alone.
		 */
		free_request_files = 0;		/* The request must not */
		free_request_struct = 0;	/* be deleted. */
		break;
	}
	/*
	 *  Check for extra operations to be done depending
	 *  on the values of bits [5..3].
	 */
	if (exitcode & 0010) {
		/*
		 *  Stop the device that was servicing the request
		 *  and mark it as failed.
		 */
		if (serverq->q.type != QUE_DEVICE) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Shepherd process specified device shutdown for non-device request.\n");
		}
		else stopdev (device, serverq);
	}
	if (exitcode & 0020) {
		/*
		 *  Stop the queue that the request was in (or is
		 *  residing in).
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Stopping queue: %s.\n", serverq->q.namev.name);
		serverq->q.status &= ~QUE_RUNNING;
	}
	if (exitcode & 0040) {			/* Stop all queues */
		stopallqueues (serverq);	/* but don't update */
						/* database image of */
	}					/* the server queue */
	/*
	 * Check to see if we need to do an auto-install.
	 * If this was the only running request, then check further.
	 */
	if (Gblbatcount == 1) nqs_autoinst();
	/*
	 *  Inform proper scheduler of the request completion.
	 */
	switch (serverq->q.type) {
	case QUE_BATCH:			/* The req was a batch req */
		Gblbatcount--;		/* One less batch request */
		/* bsc_reqcom (req); */	/* Notify batch scheduler */
		nqs_wakreq();		/* See if there is something to run */
		tell_nqs_scheduler(orig_seqno, orig_mid, req);
		break;
	case QUE_DEVICE:		/* The req was a device req */
		/*
		 *  Delete the request and remove it from the queue.
		 */
		udb_device (device);	/* Update database image */
		if (device->status & DEV_FAILED) {
			/*
			 *  The device failed to successfully handle
			 *  the request.  However, it may be possible
			 *  for another device serving the same queue
			 *  to handle the request.
			 */
			dsc_spawn();	/* Maybe spawn it again */
		}			/* on a different device */
		else {
			/*
			 *  Notify device schedule that the device
			 *  has become available.
			 */
			dsc_reqcom (device, req);
		}
		break;
	case QUE_NET:			/* MOREHERE */
		Gblnetcount--;		/* One less network request */
		break;
	case QUE_PIPE:			/* The req was a pipe req */
		if ((exitcode & 0003) != 3) {
			/*
			 *  This will have already been done if the request
			 *  was queued from a local pipe queue into another
			 *  local queue.
			 */
			Gblpipcount--;	/* One less pipe request */
			psc_reqcom(req);/* Notify pipe scheduler */
		}
		break;
	}
	if (serverq->q.status & QUE_UPDATE) {
		/*
		 *  No requests from the specified queue were
		 *  activated (and therefore the NQS database
		 *  image for the queue has not been updated).
		 */
		udb_qorder (serverq);	/* Update queue ordering */
	}
	/*
	 *  It is now safe to free up resources associated with the
	 *  request, as indicated by the free_ flags.
	 */
	if (free_request_struct) {
		/*
		 *  Dispose of the request, as appropriate.
		 */
		nqs_disreq (req, free_request_files);
		if (!free_request_files) {
			/*
			 *  The request files are to be placed in the
			 *  failed directory, for the mystification of
			 *  the NQS maintainers.
			 */
			nqs_failed (req->v1.req.orig_seqno,
				    req->v1.req.orig_mid);
		}
	}

}


/*** requeue
 *
 *
 *	void requeue():
 *
 *	Requeue the request that just completed, because of some sort
 *	of failure that warrants a retry effort.
 *
 *	The NQS database image of the server queue is NOT updated
 *	by this function.
 */
static void requeue (struct request *req)
{

	register struct nqsqueue *serverq;		/* Request queue */

	serverq = req->queue;			/* Containing queue */
	req->status &= (~RQF_SIGQUEUED & ~RQF_SIGREQUEUE);
						/* No queued signal; no */
						/* requeue on abort */
	if (req->status & RQF_OPERHOLD) {
		/*
		 *  The request has been placed on operator hold.
		 */
		a2s_a2hset (req, req->queue);	/* Add to holding set */
	} else if (req->start_time > time ((time_t *) 0)) {
		/*
		 *  Place the request in the waiting set for the queue
		 *  (a2s_a2wset() calls nqs_vtimer()).
		 */
		a2s_a2wset (req, serverq);
	}
	else {
		/*
		 *  The request can start again as soon as possible.
		 */
		a2s_a2qset (req, serverq);	/* Add to queued set */
	}
	fflush (stdout);
}


/*** stopallqueues
 *
 *
 *	void stopallqueues():
 *	Stop all NQS queues.
 */
static void stopallqueues (struct nqsqueue *serverq)
{
	register struct nqsqueue *queue;

	queue = Nonnet_queueset;		/* Non-network queues */
	while (queue != (struct nqsqueue *) 0) {
		queue->q.status &= ~QUE_RUNNING;	/* Stop the queue */
		if (serverq != queue) {
			/*
			 *  Only update the queue database image if the
			 *  the queue is NOT the server queue (which we
			 *  will update later).
			 */
			udb_queue (queue);	/* Update queue header */
		}
		queue = queue->next;
	}
	queue = Net_queueset;			/* Network queues */
	while (queue != (struct nqsqueue *) 0) {
		queue->q.status &= ~QUE_RUNNING;	/* Stop the queue */
		if (serverq != queue) {
			/*
			 *  Only update the queue database image if the
			 *  the queue is NOT the server queue (which we
			 *  will update later).
			 */
			udb_queue (queue);	/* Update queue header */
		}
		queue = queue->next;
	}
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "All queues stopped.\n");
}


/*** stopdev
 *
 *
 *	void stopdev():
 *
 *	Stop and mark the specified device as failed.
 *	Note, that the server queue will be effectively stopped if the
 *	device was the last device enabled in the queue/device set
 *	for the server queue.
 */
static void stopdev (
	struct device *device,		/* Device to be marked as failed */
	struct nqsqueue *serverq)	/* Queue that was being handled by */
					/* the device */
{
	register struct qdevmap *map;	/* Walk queue/device mappings */

	/*
	 *  Inform.
	 */
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Stopping and marking device: %s as failed.\n", device->name);
	/*
	 *  Mark device as failed.
	 */
	device->status = DEV_FAILED;
	udb_device (device);		/* Update NQS image */
	/*
	 *  Determine if the device queue has any more devices
	 *  to which reqs can be sent which are enabled.  If
	 *  not, then the queue is effectively stopped and we
	 *  want to print a warning message.
	 */
	map = serverq->v1.device.qdmaps;
	while (map != (struct qdevmap *) 0 &&
	      (map->device->status & DEV_ENABLED) == 0) {
		map = map->nextqd;
	}
	if (map == (struct qdevmap *) 0) {
		/*
		 *  The server queue has no enabled devices for
		 *  it to use.  Warn that the queue is effectively
		 *  stopped.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "W$Queue: %s is effectively stopped since no enabled devices remain in its queue/device mapping set.\n", serverq->q.namev.name);
	}
}
/*
 * A remote system has reported a request has completed.  First lets
 * check if there is some load information on this node.  If so, 
 * adjust the number of jobs. Then, lets kick the
 * scheduler to see if anything can run on that system.
 */
void
nqs_rreqcom(
	Mid_t	client_mid,	/* Mid that is reporting completion */
	Mid_t	req_mid,	/* Mid of request which completed */
	int	req_seqno,	/* Seqno of request which completed */
	int	no_jobs)	/* Remaining number of jobs */
{
    struct loadinfo *load_ptr;       /* Load information pointer */

    load_ptr = Loadinfo_head.next;
    while (load_ptr != (struct loadinfo *) NULL) {
        if (load_ptr->mid == client_mid) {
	    if (load_ptr->no_jobs >= 0) load_ptr->no_jobs = no_jobs;
	    break;
        }
        load_ptr = load_ptr->next;
    }
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Nqs_rreqcom: Client mid is %d\n",  client_mid);
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Nqs_rreqcom: Req_mid is %d\n",  req_mid);
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Nqs_rreqcom: Req_seqno is %d\n",  req_seqno);
    if (load_ptr != (struct loadinfo *) NULL) 
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Nqs_rreqcom: Remaining jobs %d\n", load_ptr->no_jobs);
    
    nqs_wakreq();
}
/*
 * Send a message back to the NQS scheduler node that we have completed
 * a request.
 */
static void tell_nqs_scheduler(
	int orig_seqno,
	Mid_t orig_mid,
	struct request *request)
{

    struct passwd *whompw;           /* Whose request it is */
    int sd;                          /* Socket descriptor */
    short timeout;                   /* Seconds between tries */
    long transactcc;                 /* Holds establish() return value */
    Mid_t my_mid;		     /* Local mid */
    int nqs_jobs;		     /* Number of NQS jobs */
    int status;		             /* Status from close */	
	
    localmid( &my_mid );
    if ( (LB_Scheduler == 0) || (LB_Scheduler == my_mid) ) return;
    whompw = sal_fetchpwuid (request->v1.req.uid);
    if (whompw == (struct passwd *) 0) {
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "tell_nqs_scheduler: Can't find password for uid %d\n", request->v1.req.uid);
	return;
    }
    nqs_jobs = get_req_count();
    interclear();
    interw32i (my_mid);
    interw32i (orig_seqno);         /* Original sequence number */
    interw32i ((long) orig_mid);    /* Original machine id */
    interw32i ((long) nqs_jobs);    /* Jobs left on the system */
 
    sd = establish (NPK_RREQCOM, LB_Scheduler, request->v1.req.uid, 
			whompw->pw_name, &transactcc);
    fflush(stdout);
    if (sd == -2) {
        /*
         * Retry is in order.
         */
        timeout = 1;
        do {
            nqssleep (timeout);
            interclear ();
            interw32i (my_mid);
            interw32i (orig_seqno);
            interw32i ((long) orig_mid);
	    interw32i ((long) nqs_jobs);    
            sd = establish (NPK_RREQCOM, LB_Scheduler,
                        request->v1.req.uid, whompw->pw_name, &transactcc);
            timeout *= 2;
        } while (sd == -2 && timeout <= 16);
    }
    status = close (sd);		/* We don't need it anymore */
    if (status != 0) {
        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "tell_nqs_scheduler: errno from close == %d\n",errno);
    }
    return;

}
/**** get_req_count:
 * 
 * Get the count of running and arriving requests on the system.
 * Returns either -1 for a failure, or 0 or positive number for count
 * of jobs on the system.
 */
static int get_req_count()
{
    register struct nqsqueue *queue;   /* Queue set to traverse */
    register struct request *req;
    int req_count;

    queue = Nonnet_queueset;        /* The request will always be */
                                    /* located in a non-network queue */
    req_count = 0;
    while (queue != (struct nqsqueue *) 0) {
       req = queue->runset;
       while (req != (struct request *) 0 ) {
	    req_count++;
            req = req->next;
       }
       req = queue->arriveset;
       while (req != (struct request *) 0 ) {
	    req_count++;
            req = req->next;
       }
       /*
        *  Examine the next queue in the queue set.
        */
       queue = queue->next;
    }
    return(req_count);
}
