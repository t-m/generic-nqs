/*
 * nqsd/nqs_abort.c
 * 
 * DESCRIPTION:
 *
 *	Abort NQS daemon execution.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libsal/debug/internal.h>

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>			/* NQS global vars */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <errno.h>


/*** nqs_abort
 *
 *
 *	void nqs_abort():
 *
 *	Abort execution of the NQS daemon writing a message to
 *	the NQS log process.
 * 
 * 	This function should NOT be called directly - it is called by
 * 	libsal on receipt of a fatal error message.
 */
void nqs_abort (void)
{
	if (errno) 
    	  sal_debug_dprintf (SAL_DEBUG_MSG_ERRORAUTHOR, "%s.\n", asciierrno());
	sal_debug_dprintf (SAL_DEBUG_MSG_ERRORAUTHOR, "Execution aborted.\n");
	exit (1);
}
