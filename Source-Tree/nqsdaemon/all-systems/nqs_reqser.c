/*
 * nqsd/nqs_reqser.c
 * 
 * DESCRIPTION:
 *
 *	This module contains all of the logic associated with the
 *	request server created for each spawned NQS request.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	May 5, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <malloc.h>
#include <time.h>
#include <grp.h>
#include <limits.h>
#include <signal.h>			/* Signal definitions */
#include <libnqs/nqspacket.h>		/* NQS local message packet types */
#include <libnqs/nqsxvars.h>		/* NQS global variables and dirs */
#include <libnqs/requestcc.h>	       	/* NQS request completion codes */
#include <libnqs/nqsacct.h>             /* Accounting file structures */
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <nqsdaemon/features.h>

#if IS_SGI
#include <sys/schedctl.h>
#endif

static char *get_the_path ( struct passwd *passwd );
static int mkspool ( struct rawreq *rawreq, char *(*namefn)(long int, Mid_t), struct passwd *passwd, int filemask );
static void seracknowledge ( int );
static void serexecute ( void );

/*
 *	Variables local to this module.
 */
static short Stateset;			/* Semaphore */
static long jid;                        /* job ID */

/*
 *      Internal data structure for ordering pipe destinations.
 */
struct pipe_dest {
    short status;                   /* Current pipe destination status */
                                        /* bits: DEST_ */
    time_t retry_at;                /* If in retry mode, then this value */                                        /* determines when the destination */
                                        /* should be reenabled */
    time_t retrytime;               /* If in retry mode, then this value */                                        /* records the time at which the */
                                        /* destination first failed */
    long retrydelta;                /* RESERVED for future use */
    char rqueue [MAX_QUEUENAME+1];  /* Name of destination queue */
    Mid_t rhost_mid;                /* Machine-id of remote destination */
    struct pipe_dest *next;         /* Ptr to next dest "pipeto" descr */
    time_t  time;                   /* When we got this information */
    int no_jobs;                    /* Number of NQS jobs currently running*/
    float load1;                    /* 1 minute load average */
    float load5;                    /* 5 minute load average */
    float load15;                   /* 15 minute load average */
    int rap;			    /* Relative Application Performance */
    float rap_per_job;		    /* RAP/no_jobs */
    float rap_per_la;		    /* RAP/load5 */
};

#define lnull(c) ((c)!=NULL ? strlen(c) : 0)
#define lgetnull(c) ((c)!=NULL ? lnull(getenv(c)) : 0)

/*** nqs_reqser
 *
 *
 *	void nqs_reqser():
 *
 *	We are the request server or shell process.
 *	We are the child of the shepherd process created
 *	in nqs_spawn.c.
 *
 *	  At this point:
 *		File descriptor 0: Control file (O_RDWR).
 *		File descriptor 1: Stdout writing to log process.
 *		File descriptor 2: Stderr writing to log process.
 *		File descriptor 3: Write file descriptor for sending
 *				   back request completion messages
 *				   from the server to the shepherd.
 *		File descriptor 4: Write file descriptor to NQS daemon
 *				   request pipe, this is enforced
 *				   by startup code in nqs_boot.c!
 *
 *	  If we are spawning a batch request with no specific shell,
 *	  and the shell strategy is FREE, then
 *
 *		File descriptor 5: Read file descriptor from the
 *				   shepherd process to the server
 *				   shell process containing the
 *				   user-process visible shell
 *				   script link name.
 *
 *	  otherwise:
 *
 *		File descriptor 5: is closed.
 *
 *	  In all cases:
 *
 *		File descriptors [6.. sysconf ( _SC_OPEN_MAX )-1] are closed.
 *
 *
 *
 *	One of two distinct situations now exists:
 *
 *	  Case 1:  We are spawning a batch request with a shell
 *		   strategy of FREE.
 *
 *	.-----------------------.
 *	|  NQS daemon process	|<============================================.
 *	|			|<==.					     ||
 *	`-----------------------'  || Named-pipe	Named-pipe connection||
 *		    |		   || connected to	to the local NQS     ||
 *		    V		   || the NQS daemon	daemon request/event ||
 *	.-----------------------.  || request/event	pipe.		     ||
 *	|  Server shepherd	|>==' pipe.				     ||
 *	|  process.		|>=======================================.   ||
 *	|			|<==.					||   ||
 *	`-----------------------'  ||			Pipe from	||   ||
 *		    |		   || Serexit() pipe	shepherd process||   ||
 *		    V		   || TO shepherd	to shell with	||   ||
 *	.-----------------------.  || from shell.	scriptfile name.||   ||
 *	|  Shell process.	|>=='					||   ||
 *	|			|<======================================='   ||
 *	|			|>============================================'
 *	`-----------------------'
 *
 *
 *
 *	  Case 2:  We are spawning a batch request with a shell
 *		   strategy of FIXED or LOGIN (or the request
 *		   specifies a particular shell), or we are spawning
 *		   a device, network, or pipe request.
 * 
 *	.-----------------------.
 *	|  NQS daemon process	|<============================================.
 *	|			|<==.					     ||
 *	`-----------------------'  || Named-pipe	Named-pipe connection||
 *		    |		   || connected to	to the local NQS     ||
 *		    V		   || the NQS daemon	daemon request/event ||
 *	.-----------------------.  || request/event	pipe.		     ||
 *	|  Server shepherd	|>==' pipe.				     ||
 *	|  process.		|<==.					     ||
 *	`-----------------------'  ||					     ||
 *		    |		   || Serexit() pipe			     ||
 *		    V		   || TO shepherd			     ||
 *	.-----------------------.  || from shell.			     ||
 *	|  Shell process.	|>=='					     ||
 *	|			|>============================================'
 *	`-----------------------'
 *
 *
 */
void nqs_reqser (
	struct request *request,	/* Request structure */
	struct rawreq *rawreq,		/* Raw request header from */
						/* control file */
	int restartflag,		/* BOOLEAN request is restarting */
					/* execution */
	struct nqsqueue *queue,		/* Queue being served */
	struct device *device,		/* Device to use (can be NIL) */
	struct passwd *passwd)		/* Password entry for request */
{

#if	MAX_QUEUENAME > MAX_DEVNAME
	char argv0buffer [MAX_QUEUENAME + 8];
#else
	char argv0buffer [MAX_DEVNAME + 8];
#endif
					/* Space for argv [0] name of */
					/* queue or device name + " server" */
	char *environment;		/* Space for environment */
	char *server = NULL;		/* Server and/or server arguments, */
					/* or pathname of batch shell */
	char *argv [MAX_SERVERARGS+1];	/* Execve() argument pointers */
	int argc = 0;			/* Number of arguments to execve() */
	char **envp;			/* Execve() environment pointers */
	struct qdestmap *mapdest;	/* Used to walk destset for pipe-q */
	int fd;				/* File descriptor for device */
	int envpcount;			/* Number of environment vars */
	int envpsize;			/* Size of environment in chars */
	int filemask = 0;			/* = O_CREAT if restartflag is TRUE */
					/* = O_CREAT | O_TRUNC if */
					/*   restartflag is FALSE */
	long i, j;			/* Iteration counter & scratch var */
	char *cp;	   		/* Character pointer */
	int persist;			/* Persistent pipe queue request */
	/*
	 *  Variables specific to the spawning of a batch request.
	 */
	char minusname [257];		/* Name of shell (path removed) */
					/* prefixed with a "-" character */
	char buffer [MAX_REQPATH+2];	/* Batch request control file line */
					/* buffer */
	char buffer2 [1025];		/* buffer for sys_job filename */
	char *hostname = NULL;		/* Name of submitter's host */
	time_t timeval;			/* Current time */
	unsigned long retry_time;	/* Retry time */
	int script = -1;		/* Shell script file descriptor */
	int standout = -1;		/* Standard output file */
	int standerr = -1;		/* Standard error file */
        int fd_acct;                    /* Accounting file descriptor */
        struct nqsacct_init1 acct_init1;  /* Accounting record */
	Mid_t mymid;			/* what is my machine id? */
	int dest_number;		/* Counter for destinations */
        int openflags;
        int cmask = -1;
        struct stat sbuf;
        char fullname [MAX_REQPATH+2];
        char *retrymsg = "Device offline (%s:%s). retries = %d\n";
	int number_of_dests;		 /* Number of destinations */
	int empty_dests;	       	 /* TRUE if empty destinations */
	int loadinfo_dests;		 /* Destinations with load information */
	int start_destination;		 /* Destination number to start at */
	struct pipe_dest pipe_dest_header; /* Header of list of pipe destinations */
	struct pipe_dest *pd_ptr;	 /* Pipe destination pointer */
	struct pipe_dest *pd_last;	 /* Pipe destination pointer */
	struct pipe_dest *pd_curr;	 /* Pipe destination pointer */
	struct loadinfo *load_ptr;	 /* Load information pointer */
	struct pipe_dest *pd_list[MAX_QDESTS];	/* Ordered list of pointers */
	time_t	now_now;		 /* The time now */
        int ngroups;                     /* Number of groups in the group set */
        gid_t gidset[NGROUPS_MAX];       /* The concurrent group set */

	/* Dynamic sizing variables */
	char *dbuffer;
	int c;
	off_t f_pos;
	int envpcountmax;
	int envpsizemax;
	int linesize;
	int linesizemax;

	char *filename;
	char *dirname;
	char *szVarNameEnd = NULL;	/* used when checking env vars */
	int   iIndex = 0;		/* used when checking env vars */
	int   iOverwrite = 0;		/* used when checking env vars */

#if HAS_PSET_IRIX_DPLACE
	FILE * fp = NULL;		/* file handle for placement file */
	char fp_buffer[2048];		/* filename of placement file */
#endif 

	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "nqs_reqser: entering for %d\n", rawreq->orig_seqno);

	envpcount = 0;			/* No environment vars exist yet */
	envpsize = 0;

	/* Size the environment variables for malloc() */
	envpcountmax = 8;
	envpsizemax = lnull(Nqs_nmapdir) + lgetnull(Nqs_nmapdir) + 2
	    + lnull(Nqs_libdir) + lgetnull(Nqs_libdir) + 2
	    + lnull(Nqs_spooldir) + lgetnull(Nqs_spooldir) + 2
	    + 12 + lnull(NQS_LIBEXE)
	    + 9 + 15 + 15;
	switch(queue->q.type) {
	case QUE_BATCH:
	    envpcountmax += 17;
	    envpsizemax += 6 + lnull(passwd->pw_dir)
		+ 11 + lnull(Fixed_shell) + 7 
		+ lnull(passwd->pw_shell) + lnull(rawreq->v.bat.shell_name)
		+ 12 + lnull(queue->q.namev.name)
		+ 11 + lnull(passwd->pw_name)
		+ 18
		+ 1 + lnull(get_the_path(passwd))
#if !HAS_BSD_ENV
		+ 9 + lnull(passwd->pw_name)
		+ 16 + lnull(passwd->pw_name)
		+ 4 + lgetnull("TZ")
#else
		+ 6 + lnull(passwd->pw_name)
		+ 22 + lnull(passwd->pw_name)
#endif
		+ 11 + lnull(hostname)
		+ 23 + lnull(hostname)
		+ 14 + lnull(rawreq->reqname)
		+ 14
		+ 12 + 2*lnull(rawreq->v.bat.stdout_name)
		+ 12 + lnull(rawreq->v.bat.stderr_name)
		+ 15 + 32;
	    f_pos = lseek(fileno(stdin), 0L, SEEK_CUR);
	    fseek(stdin, f_pos, SEEK_SET);
	    linesize = linesizemax = 0;
	    while ((c = fgetc(stdin)) != EOF) {
		linesize++;
		envpsizemax++;
		if (c == '\n') {
		    envpcountmax++;
		    if (linesize > linesizemax)
			linesizemax = linesize;
		    linesize = 0;
		}
	    }
	    linesizemax++;  /* Allow for NUL char */
	    lseek (fileno(stdin), f_pos, SEEK_SET);
	    fseek (stdin, f_pos, SEEK_SET);
	    break;
	case QUE_DEVICE:
	    break;
	case QUE_NET:
	    envpcountmax += 8;
	    envpsizemax += 10 + 10 + strlen(passwd->pw_name)
#if	IS_POSIX_1 | IS_SYSVr4
		+ 4 + lgetnull("TZ")
#endif
		+ 31
		+ 31
		+ 21
		+ 5
		+ 7 + 11;
	    break;
	case QUE_PIPE:
	    envpcountmax += (6 + MAX_QDESTS);
	    envpsizemax += 10 + 10 + strlen(passwd->pw_name)
#if	IS_POSIX_1 | IS_SYSVr4
		+ 4 + lgetnull("TZ")
#endif
		+ 31
		+ 31
		+ (MAX_QDESTS*(8+11+11+MAX_QUEUENAME+1))
		+ 10;
	    break;
	}
	/* Allocate the buffers */
	environment = (char *)malloc(envpsizemax*sizeof(char));
	if (environment == NULL) {
	    serexit(RCM_INSUFFMEM, "Unable to allocate environment memory in reqser()\n");
	}
	envp = (char **)malloc(envpcountmax*sizeof(char *));
	if (envp == NULL) {
	    serexit(RCM_INSUFFMEM, "Unable to allocate environment pointer memory in reqser()\n");
	}

        /*
         * If the current environment defines directory independent
         * pathnames, pass them into the new environment.
         */
        if ((dirname = getenv (Nqs_nmapdir)) != (char *)NULL) {
                cp = environment + envpsize;
                envp [envpcount++] = cp;
                sprintf (cp, "%s=%s", Nqs_nmapdir, dirname);
                envpsize += strlen (cp) + 1;
        }
        if ((dirname = getenv (Nqs_libdir)) != (char *)NULL) {
                cp = environment + envpsize;
                envp [envpcount++] = cp;
                sprintf (cp, "%s=%s", Nqs_libdir, dirname);
                envpsize += strlen (cp) + 1;
        }
        if ((dirname = getenv (Nqs_spooldir)) != (char *)NULL) {
                cp = environment + envpsize;
                envp [envpcount++] = cp;
                sprintf (cp, "%s=%s", Nqs_spooldir, dirname);
                envpsize += strlen (cp) + 1;
        }

	cp = environment + envpsize;
	envp [envpcount++] = cp;
	sprintf (cp, "NQS_LIBEXE=%s", NQS_LIBEXE);
	envpsize += strlen (cp) + 1;

	cp = environment + envpsize;
	envp [envpcount++] = cp;
	sprintf (cp, "DEBUG=%1d", sal_debug_GetLevel());
	envpsize += strlen (cp) + 1;

	cp = environment + envpsize;
	envp [envpcount++] = cp;
	sprintf (cp, "QSUB_UID=%d", passwd->pw_uid);
	envpsize += strlen (cp) + 1;
	cp = environment + envpsize;
	envp [envpcount++] = cp;
	sprintf (cp, "QSUB_GID=%d", passwd->pw_gid);
	envpsize += strlen (cp) + 1;

	if (queue->q.type != QUE_BATCH) {
		if (queue->q.type != QUE_DEVICE) {
			/*
			 *  Network queue and pipe queue servers are
			 *  spawned with a real AND effective user-id
			 *  of root.  This is necessary so that the
			 *  ../lib/establish.c module can bind to a
			 *  system socket port (<1024).
			 *
			 *  These servers however must know the account
			 *  upon whose behalf they are operating.  This
			 *  information is passed to them through the
			 *  environment.
			 */
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf (cp, "UID=%d", passwd->pw_uid);
			envpsize += strlen (cp) + 1;
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf (cp, "USERNAME=%s", passwd->pw_name);
			envpsize += strlen (cp) + 1;
#if	IS_POSIX_1 | IS_SYSVr4
			/*
			 *  Network queue and pipe queue servers for
			 *  System V based implementations of NQS always
 			 *  have a TZ (timezone) environment variable
			 *  (for ../lib/establish.c as invoked by network
			 *  and pipe queue servers).  (Please note that
			 *  the TZ environment variable is guaranteed
			 *  to be present, since nqs_boot.c checks for
			 *  it.)
			 *
			 *  Berkeley style NQS implementations do not
			 *  need or use TZ.
			 */
		  	
		  	/* Generic NQS 3.41.0
			 * 
			 * Although TZ is 'guarenteed' to be there, there
			 * have been a number of cases where it wasn't,
			 * and nqsd segv'd at this point.
			 * 
			 * To deal with this, I've added a paranoia test
			 * which forces NQS to abort, rather than have it
			 * segv.
			 */
		  
		  	if (getenv("TZ") == NULL)
		    		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Unable to locate environment variable TZ.  Please ensure that TZ is set before starting NQS.\n");
		  
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf (cp, "TZ=%s", getenv ("TZ"));
			envpsize += strlen (cp) + 1;
#else
#if	IS_BSD
#else
BAD SYSTEM TYPE
#endif
#endif
		}
	}
	switch (queue->q.type) {
	case QUE_BATCH:
		/*
		 *  We are spawning a batch request.
		 */
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "nqs_reqser(): Request is type BATCH\n");
		filemask = O_CREAT;
		if (!restartflag) {
			/*
			 *  This request is being spawned for the very
			 *  first time.  Set filemask to also include
			 *  O_TRUNC.
			 */
			filemask |= O_TRUNC;
		}
		/*
		 *  Open the shell script file.
		 */
		if (Shell_strategy == SHSTRAT_FREE &&
		    rawreq->v.bat.shell_name [0] == '\0') {
			/*
			 *  The batch request is being run with a shell
			 *  strategy of FREE.  File descriptor #5 has the
			 *  the user-process visible link name of the
			 *  shell script file for the request.
			 */
			script = 5;	/* Read on file-descr #5 */
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM,
			"nqs_reqser: no shell specified\n");
		}
		else {
			/*
			 *  The batch request is being run with a shell
			 *  strategy of FIXED or LOGIN (or the request
			 *  specifies a particular shell).
			 */
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM,
			"nqs_reqser: shell specified\n");
			if ((script = opendata (rawreq->orig_seqno,
						rawreq->orig_mid, 1)) == -1) {
				/*
				 *  We were unable to open the shell script
				 *  file.
				 */
				if (errno == ENFILE) {
					serexit (RCM_ENFILERUN, (char *) 0);
				}
				serexit (RCM_BADCDTFIL,
					"Unable to open shell script file.");
			}
		}
		/*
		 *  Determine the principal name of the request owner's
		 *  machine as seen from the execution machine.
		 */
		errno = 0;		/* Clear errno so that we can tell */
					/* if a system call error occurred */
					/* in the nmap_get_nam() function */
		hostname = nmap_get_nam (rawreq->orig_mid);
		if (hostname == (char *) 0) {
		    /*
		     *  Unable to determine the name of the
		     *  request owner's machine.
		     */
		    if (errno == ENFILE) {
			serexit (RCM_ENFILERUN, (char *) 0);
		    }
		    /*
		     *  The request owner's machine-id is no
		     *  longer known to the execution machine.
		     */
		    serexit (RCM_MIDUNKNOWN, (char *) 0);
		}
		/*
		 *  Build the login environment and select the proper
		 *  shell.
		 */
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "HOME=%s", passwd->pw_dir);
		envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;

		if (rawreq->v.bat.shell_name [0] == '\0') {
		    /*
		     *  The batch request does not explicitly
		     *  specify a shell.  NQS must choose the
		     *  shell used to execute the batch request
		     *  based upon the configured shell
		     *  strategy of the local system.
		     */
		    if (Shell_strategy == SHSTRAT_FIXED) {
			/*
			 *  A shell strategy of FIXED is in
			 *  effect.
			 */
			sprintf (cp, "NQS_SHELL=%s", Fixed_shell);
		    }
		    else {
			/*
			 *  A shell strategy of FREE of LOGIN
			 *  is presently in effect.
			 */
			if (passwd->pw_shell == (char *) 0 ||
			    passwd->pw_shell [0] == '\0') {
			    /*
			     *  The default shell is the Bourne shell.
			     */
			    sprintf (cp, "NQS_SHELL=%s", "/bin/sh");
			}
			else {
			    /*
			     *  The password file entry for the owner
			     *  identifies a shell to be used.
			     */
			    sprintf (cp, "NQS_SHELL=%s", passwd->pw_shell);
			}
		    }
		}
		else {
		    /*
		     *  The request specifies that a specific shell
		     *  be used.
		     */
		    sprintf (cp, "NQS_SHELL=%s", rawreq->v.bat.shell_name);
		}
		envpsize += strlen (cp) + 1;
		/*
		 *  At this time, the character pointer variable: cp
		 *  refers to the SHELL= environment variable.
		 */

		/* Generic NQS v3.51.0-pre6:
		 *
		 * Ignoring processor set support for the moment ...
		 *
		 * What we want to do is, as root, to execute the following:
		 *
		 *	/bin/sh -c $NQS_LIBEXE/sys_job
		 *
		 * This allows the system administrator to let nqsjobscript
		 * run as any type of scripting file as he wishes.
		 *
		 * It is the responsibility of the nqsjobscript to start
		 * the user's job, by calling
		 *
		 *	$NQS_LIBEXE/nqsexejob
		 *
		 * with no parameters.  nqsexejob will take what it needs
		 * from the environment.
		 */
#if HAS_PSET_IRIX_DPLACE

		sprintf(fp_buffer,"%s/%s", NQS_DPLACE_DIR, queue->q.namev.name);

		if ((fp = fopen(fp_buffer, "r")) != NULL)
		{
			/* okay - we know the file exists ... */
			fclose(fp);
			argv[argc] = "dplace";
			argc++;
			argv[argc] = "-p ";
			argc++;
			argv[argc] = fp_buffer;
			argc++;
			argv[argc] = "/bin/sh";
			argc++;

			server = "/usr/sbin/dplace";

			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH,
                            "using placement file %s for queue %s\n",
			    &fp_buffer[0], queue->q.namev.name);
		}
		else
		{
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH,
			    "no placement file %s for queue %s - not using dplace\n",
			    &fp_buffer[0], queue->q.namev.name);
			server = "/bin/sh";
			
		}
#else
		server = "/bin/sh";
#endif
		/*
		 *  Build the remaining login environment variable
		 *  set.
		 */
                cp = environment + envpsize;
                envp [envpcount++] = cp;
                sprintf (cp, "QSUB_QUEUE=%s", queue->q.namev.name);
                envpsize += strlen(cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "QSUB_USER=%s", passwd->pw_name);
		envpsize += strlen(cp) + 1;
                cp = environment + envpsize;
                envp [envpcount++] = cp;
		sprintf (cp, "ENVIRONMENT=BATCH");
		envpsize += strlen(cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		strcpy(cp, get_the_path(passwd) );
		envpsize += strlen(cp) + 1;				 
#if	!HAS_BSD_ENV
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "LOGNAME=%s", passwd->pw_name);
	        envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "MAIL=/usr/mail/%s", passwd->pw_name);
	        envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
                sprintf (cp, "TZ=%s", getenv ("TZ"));
	        envpsize += strlen (cp) + 1;
#else
		envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "USER=%s", passwd->pw_name);
		envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "MAIL=/usr/spool/mail/%s", passwd->pw_name);
		envpsize += strlen (cp) + 1;
#endif								       
		/*
		 *  Construct the proper argv array.
		 */
		argv [argc] = "/bin/sh";
		argc++;
		argv [argc] = "-c";
		argc++;
		sprintf(buffer2, "%s/sys_job", NQS_LIBEXE);
		argv [argc] = buffer2;
		argc++;
		argv [argc] = (char *) 0;
		argc++;
		/*
		 *  Read the varying portion of the control file as a
		 *  sequence of lines terminated with newline characters,
		 *  adding environment variables as appropriate.
		 */
		/* fseek (stdin, lseek (fileno (stdin), 0L, SEEK_CUR), SEEK_SET); */
		linesizemax++;   /* allow for NUL byte */
		dbuffer = (char *)malloc(linesizemax*sizeof(char));
		if (dbuffer == NULL) {
		    serexit(RCM_INSUFFMEM, "Unable to allocate memory in reqser()\n");
		}
		while (!feof (stdin) &&
			fgets (dbuffer, linesizemax, stdin) != (char *) 0) {
		    i = strlen (dbuffer) - 1;	/* Get length of buffer -1 */
		    if (dbuffer [i] != '\n') {
			/*
			 *  We did not see a new line character.
			 *  The control file line is too long.
			 */
			serexit (RCM_BADCDTFIL,
				 "Varying control file line too long.");
		    }
		    dbuffer [i] = '\0';		/* Delete the newline char */
		    if (dbuffer [0] == 'D') {
			/*
			 *  This line has the value for the environment
			 *  variable:  QSUB_WORKDIR.
			 */
			i--;		/* i now equals the length of the */
					/* QSUB_WORKDIR directory string*/
			/*
			 *  Check to see if the environment variables:
			 *  QSUB_HOST, QSUB_REQID, QSUB_REQNAME,
			 *  and QSUB_WORKDIR will fit.
			 */
			if (envpcount + 4 >= envpcountmax ||
			    strlen (hostname) * 2 + envpsize + 59 + i +
			    strlen (rawreq->reqname) > (size_t) envpsizemax) {
				/*
				 *  There is not sufficient space in
				 *  the environment array for all of
				 *  this information.
				 */
				serexit (RCM_2MANYENVARS, (char *) 0);
			}
			/*
			 *  Create the "QSUB_HOST=" environment variable.
			 *  QSUB_HOST=hostname
			 */
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf (cp, "QSUB_HOST=%s", hostname);
			envpsize += strlen (cp) + 1;
			/*
			 *  Create the "QSUB_REQID=" environment variable.
			 *  QSUB_REQID=nnnnn.hostname
			 */
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf (cp, "QSUB_REQID=%1ld.%s",
					    (long) rawreq->orig_seqno,
					     hostname);
			envpsize += strlen (cp) + 1;
			/*
			 *  Create the "QSUB_REQNAME=" environment variable.
			 *  QSUB_REQNAME=reqname
			 */
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf (cp, "QSUB_REQNAME=%s", rawreq->reqname);
			envpsize += strlen (cp) + 1;
			/*
			 *  Create the "QSUB_WORKDIR=" environment variable.
			 *  QSUB_WORKDIR=workdir
			 */
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf (cp, "QSUB_WORKDIR=%s", dbuffer+1);
			envpsize += strlen (cp) + 1;
		    }
		    else if (dbuffer [0] == 'E') {
			/*
			 *  This is a simple environment variable.
			 *  Get space for the environment variable
			 *  (i has the length of the entire environment
			 *  string + 1), and store the environment
			 *  string.
			 */
			if (envpcount >= envpcountmax ||
			    envpsize + i > envpsizemax) {
				/*
				 *  There is not sufficient space in
				 *  the environment array.
				 */
				serexit (RCM_2MANYENVARS, (char *) 0);
			}
			/*
			 * Generic NQS 3.50.8:
			 *
			 * We must check to make sure that we are not
			 * trying to overwrite a variable that we
			 * have already set
			 */

		        szVarNameEnd = strchr(&dbuffer[1], '=');
			iIndex       = 0;
			iOverwrite   = 0;

			while (iIndex < envpcount)
			{
				if (strncmp(&dbuffer[1], envp[iIndex], szVarNameEnd - &dbuffer[0]) == 0)
				{
					iOverwrite = 1;
					sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_reqser: Rejecting environment variable %s from control file - possible security attack?\n", &dbuffer[1]);
				}
				iIndex++;
			}

			if (iOverwrite == 0)
			{
				cp = environment + envpsize;
				envp [envpcount++] = cp;
				strcpy (cp, dbuffer+1);
				envpsize += i;
			}
		    }
		    /*
		     *  File staging event specifications: [I,O]
		     *  are, and should always be, ignored here.
		     *  Their implementation will always be done
		     *  elsewhere.
		     */
		}
		free(dbuffer);
		/*
		 *  If the output file is to be spooled, then create the
		 *  temporary version of it in the NQS output spooling
		 *  directory BEFORE we chdir() to the user's home
		 *  directory.
		 *
		 *  It is possible that NQS will someday be linked
		 *  with a Newcastle or equivalent distributed file
		 *  system library, which intercepts system calls always
		 *  passing ABSOLUTE file paths to the kernel on the
		 *  appropriate machine.  Since the spool files reside
		 *  within the NQS protected file hierarchy, it is
		 *  necessary to be running as root, when a spool file
		 *  is being created under Newcastle-like file system
		 *  implementations.
		 */
		if (rawreq->v.bat.stdout_acc & OMD_SPOOL) {
		    /*
		     *  The output file is to be spooled.
		     */
		    standout = mkspool (rawreq, namstdout, passwd,
					    filemask);
		    if (standout == -1) {
		        /*
			 *  Alas, we failed!
		         */
			if (errno == ENFILE) {
			    /*
			     *  The system file table is full.
			     */
			    serexit (RCM_ENFILERUN, (char *) 0);
			 }
			 if (errno == ENOSPC) {
			    /*
			     *  Insufficient space.
			     */
			    serexit (RCM_ENOSPCRUN, (char *) 0);
			 }
			 sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_reqser: Unable to create spooled stdout\nerrno = %d\n,", errno);
			 serexit (RCM_UNAFAILURE,
					"Unable to create spooled stdout.");
		    }
		}
		/*
		 *  If the error file is to be spooled, then create the
		 *  temporary version of it in the NQS output spooling
		 *  directory BEFORE we chdir() to the user's home
		 *  directory.
		 */
		if ((rawreq->v.bat.stderr_acc & OMD_SPOOL) &&
		    (rawreq->v.bat.stderr_acc & OMD_EO) == 0) {
		    /*
		     *  The error file is to be spooled.
		     */
		    standerr = mkspool (rawreq, namstderr, passwd,
					    filemask);
		    if (standerr == -1) {
			/*
			 *  Alas, we failed!
			 */
			if (errno == ENFILE) {
			    /*
			     *  The system file table is full.
			     */
			    serexit (RCM_ENFILERUN, (char *) 0);
			}
			if (errno == ENOSPC) {
			    /*
			     *  Insufficient space.
			     */
			    serexit (RCM_ENOSPCRUN, (char *) 0);
			}
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_reqser: Unable to create spooled stderr, errno %d.\n", errno);
			serexit (RCM_UNAFAILURE,
					"Unable to create spooled stderr.");
		    }
		}
		break;
	case QUE_DEVICE:
		/*
		 *  The request is a device request.  Try to open the device
		 *  for reading AND writing.  If this fails, then try to
		 *  open the device for writing only.  If that fails, then
		 *  go through a wait/retry process until the retry limit
		 *  is reached.
		 */
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "nqs_reqser(): Request is type DEVICE\n");
		close (1);		/* Stdout is going to become the */
					/* device leaving only stderr for */
					/* catastrophic error reporting */
		i = 0;
                openflags = 0;
                if ((stat (device->fullname, &sbuf)) == -1) {
                        serexit(RCM_DEVOPEFAI, asciierrno());
                }
                if ((sbuf.st_mode & S_IFMT) == S_IFDIR) {
                    sprintf(fullname,"%s/%ld.%s",device->fullname,
                                (long) rawreq->orig_seqno,
                                getmacnam(rawreq->orig_mid));
                    cmask = umask(0177);
                    openflags = O_CREAT;
                } else
                        strcpy(fullname, device->fullname);
		do {
                    if ((fd = open (fullname, O_RDWR |
                        openflags, 0777)) == -1) {
			/*
			 *  Would not open for read and write.
			 *  Try just write.
			 */
                        if ((fd = open (fullname, O_WRONLY |
                                    openflags,0777)) == -1) {
			    if (i < Maxoperet) {
                                fprintf(stderr,retrymsg, device->name,
                                            fullname,i+1);
                                fflush(stderr);
				if (Opewai == 0) nqssleep (1);
				else nqssleep (Opewai);
			    }
			    i++;	/* Retry count */
			}
		    }
		} while (i <= Maxoperet && fd == -1);
		if (fd == -1) serexit (RCM_DEVOPEFAI, asciierrno ());
                if ((sbuf.st_mode & S_IFMT) == S_IFDIR) {
                    /*
                     * Make sure the user pays for the spooled
                     * disk space (it was created while running
                     * under root).
                     */
                    umask (cmask);
                    chown(fullname, passwd->pw_uid, passwd->pw_gid);
                }
		/*
		 *  Make sure that the device is open as stdout (fd#1).
		 */
		if (fd != 1) {
		    fcntl (fd, F_DUPFD, 1);
		    close (fd);
		}
		/*
		 *  We have the device open.
		 *  Get the name of the server and args to execve.
		 */
		server = device->server;
		break;
	case QUE_NET:
		/*
		 *  We are spawning a network queue request.
		 */
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "nqs_reqser(): Request is type NET\n");
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "DEFAULT_RETRYTIME=%1ld", (long) Defnetrettim);
		envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "DEFAULT_RETRYWAIT=%1ld", (long) Defnetretwai);
		envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "ELAPSED_RETRYTIME=0");
		envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "OP=O");		/* Output file transaction */
		envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "EVENT=%1d", request->v1.subreq.event);
		envpsize += strlen (cp) + 1;
		server = queue->q.v1.network.server;
		break;				/* Get server/args to execve */
	case QUE_PIPE:
		/*
		 *  We are spawning a pipe queue request.
		 *  Get the server, and format the destination set
		 *  into the environment for the pipe queue server.
		 */
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "nqs_reqser(): Request is type PIPE\n");
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "DEFAULT_RETRYTIME=%1ld", (long) Defdesrettim);
		envpsize += strlen (cp) + 1;
		cp = environment + envpsize;
		envp [envpcount++] = cp;
		sprintf (cp, "DEFAULT_RETRYWAIT=%1ld", (long) Defdesretwai);
		envpsize += strlen (cp) + 1;
		/*
		 * ================================================
		 * Build list of destinations
		 * ================================================
		 */
		pipe_dest_header.next = (struct pipe_dest *) NULL;
		pd_last = &pipe_dest_header;
		i = 0;				/* Destination# */
		mapdest = queue->v1.pipe.destset;
		while (mapdest != (struct qdestmap *) 0) {
		    pd_ptr = (struct pipe_dest *) malloc ( sizeof( pipe_dest_header ));
		    if (pd_ptr == (struct pipe_dest *) NULL ) {
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Nqs_reqser: Cannot allocate memory!\n");
			serexit (RCM_INSUFFMEM, (char *) 0);
		    }
		    sal_bytezero(pd_ptr, sizeof(pipe_dest_header) );
		    pd_ptr->status = mapdest->pipeto->status;
		    pd_ptr->retry_at = mapdest->pipeto->retry_at;
		    pd_ptr->retrytime = mapdest->pipeto->retrytime;
		    pd_ptr->retrydelta = mapdest->pipeto->retrydelta;
		    strcpy(pd_ptr->rqueue, mapdest->pipeto->rqueue);
		    pd_ptr->rhost_mid = mapdest->pipeto->rhost_mid;
		    pd_ptr->no_jobs = LOAD_NO_JOBS;
						/* Marker for no load information */
		    pd_ptr->rap = 1;		/* Default RAP */	
		    /*
		     * Find the load information record (if any)
		     * associated with this mid.
		     */
		    time(&now_now);		    /* What time is it now? */
		    load_ptr = Loadinfo_head.next;
		    while (load_ptr != (struct loadinfo *) 0) {
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_reqser: load mid %u pd mid %u.\n", load_ptr->mid, pd_ptr->rhost_mid);
			if (load_ptr->mid == pd_ptr->rhost_mid) {
			    pd_ptr->rap = load_ptr->rap;
			    /*
			     * If the load record is less than 15 minutes
			     * old, use it,  o/w ignore.
			     */
			    if ( (now_now - load_ptr->time) < 15*60) {
				pd_ptr->time = load_ptr->time;
				pd_ptr->no_jobs = load_ptr->no_jobs;
				pd_ptr->load1 = load_ptr->load1;
				pd_ptr->load5 = load_ptr->load5;
				pd_ptr->load15 = load_ptr->load15;
			    }
			    break;
			}
			load_ptr = load_ptr->next;
		    }
		    mapdest = mapdest->next;    /* Ordered destinations */
		    pd_last->next = pd_ptr;     /* The previous last now points here */
		    pd_last = pd_ptr;		/* This is now the previous last */
		    i++;			/* One more destination */
		}
		number_of_dests = i;		/* Remember how many */
	  
		i = 0;
		pd_ptr = pipe_dest_header.next;
		while (pd_ptr != (struct pipe_dest *) NULL) {
		    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_reqser: Dest %d mid %u jobs %d\n",
		      	  	 i, pd_ptr->rhost_mid,  pd_ptr->no_jobs);
		    pd_ptr = pd_ptr->next;
		    i++;
		}
		/*
		 * Now set up the proper order of destinations.
		 */
		for (i = 0; i < MAX_QDESTS; i++ ) {
		    pd_list[i] = (struct pipe_dest *) NULL;
		}
		if (queue->q.status & QUE_LDB_OUT) { 
		    /*
		     * Load balanced ... Set up round robin order.
		     */
		    persist = 1;			/* Unassuming request */
		    start_destination = rawreq->orig_seqno % number_of_dests;
		    pd_ptr = pipe_dest_header.next;
		    for ( i = 0; i < start_destination; i++) {
			pd_ptr = pd_ptr->next;
		    }
		    i = 0;
		    while (pd_ptr != (struct pipe_dest *) NULL ) {
			pd_list[i++] = pd_ptr;
			pd_ptr = pd_ptr->next;
		    }
		    pd_ptr = pipe_dest_header.next;
		    for ( j = 0; j < start_destination; j++) {
		        pd_list[i++] = pd_ptr;
			pd_ptr = pd_ptr->next;
		    }
		    /*
		     * Do any of the destinations have no jobs or do we
		     * have load information for all destinations?
		     */
		    empty_dests = 0;       /* Start with no empty dests */
		    loadinfo_dests = 0;    /* Count of dests with load info */
		    for ( i = 0; i < number_of_dests; i++) {
		        if ( pd_list[i]->no_jobs == 0) empty_dests++;
			if ( pd_list[i]->no_jobs != LOAD_NO_JOBS) 
					loadinfo_dests++;
			/*
			 * Calculate performance.  If there are no jobs, 
			 * the rap_per_job is 10 times the rap. O/W it is
			 * rap/no_jobs.
			 */
			if (pd_list[i]->no_jobs == 0) 
				pd_list[i]->rap_per_job = 10. * pd_list[i]->rap;
			else
			    pd_list[i]->rap_per_job = (float) pd_list[i]->rap /
					(float) pd_list[i]->no_jobs;
			if (pd_list[i]->load5 == 0.0) pd_list[i]->load5 = 0.001;
			pd_list[i]->rap_per_la = (float) pd_list[i]->rap /
					pd_list[i]->load5;
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_reqser: Dest %d mid %u jobs %d\n",
				  i, pd_list[i]->rhost_mid,  pd_list[i]->no_jobs);
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_reqser: rap-per-job %f rap-per-load %f\n",
				  pd_list[i]->rap_per_job,  pd_list[i]->rap_per_la);
		    }
		    if (empty_dests  || (loadinfo_dests == number_of_dests) ) {
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_reqser: Reordering dests.\n");
		        /*
			 * We have at least one empty destination -or- load
			 * information for all the destinations.  Order them
			 * based on rap per job and the rap per 5 min load 
			 * avgerage.
			 */
			for (i = 0; i < number_of_dests - 1; i++) {
			    for (j = number_of_dests - 1 ; i < j; --j) {
			       /*
			        * Sort first on rap per job.
			        */
			       if (pd_list[j-1]->rap_per_job < 
				         pd_list[j]->rap_per_job) {
				    fflush(stderr);
				    pd_ptr = pd_list[j];
				    pd_list[j] = pd_list[j-1];
				    pd_list[j-1] = pd_ptr;
			        } else if ( pd_list[j-1]->rap_per_job ==
			                pd_list[j]->rap_per_job) {
				    /*
				     * Sort second on rap per 5 min load 
				     * average.
				     */
				    if (pd_list[j-1]->rap_per_la <
				         pd_list[j]->rap_per_la ) {
					pd_ptr = pd_list[j];
					pd_list[j] = pd_list[j-1];
					pd_list[j-1] = pd_ptr;
				    }
					
				}
			     } 
			 }
		    }
		} else {
		    /*
		     * Not load balanced ... Just create the list.
		     */
		    persist = 0;
		    pd_ptr = pipe_dest_header.next;
		    for (i = 0; i < number_of_dests; i++) {
			pd_list[i] = pd_ptr;
			pd_ptr = pd_ptr->next;
		    }
		}
		/*
		 * So what does it look like?
		 */
		for (i = 0; i < number_of_dests; i++) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_reqser: dest %d, mid %u.\n",
				i, pd_list[i]->rhost_mid);
		}
		timeval = time ((time_t *) 0);	/* Current time */
		/*
		 * Go through the list of destinations creating
		 * the environment variables which indicate the
		 * destinations to try (and the order).
		 */
		dest_number = 0;
		for (i = 0; i < number_of_dests; i++) {
		    if (pd_list[i]->status & DEST_ENABLED) {
			/*
			 *  The destination is enabled.
			 *  Add this destination to the
			 *  set of pipe queue destinations
			 *  in the environment.
			 *
			 *  Note that no explicit checks are
			 *  made here to see if we are running
			 *  off the end of the environment arrays!
			 *
			 *  We are depending on the #defines in
			 *  nqs.h to have been honestly defined.
			 */
			retry_time = 0;
			if ((pd_list[i]->status & DEST_RETRY) &&
			     timeval > pd_list[i]->retrytime) {
			    retry_time = (unsigned long) timeval
				- (unsigned long) pd_list[i]->retrytime;
			}
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf (cp, "D%03d=%1lu %1lu %s", dest_number++,
				(long) pd_list[i]->rhost_mid,
				 retry_time, pd_list[i]->rqueue);
			envpsize += strlen (cp) + 1;
		     } else {
			/*
			 *  A destination exists that is presently disabled
			 *  for this pipe queue.  Since not all destinations
			 *  are therefore available to the pipe queue server
			 *  (pipeclient), the request should be allowed to
			 *  persist even if none of the enabled destinations
			 *  accept it, since it is not known that all possible
			 *  destinations for request have been tried.
			 */
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_reqser: mid %u disabled\n",
				    pd_list[i]->rhost_mid);
			persist = 1;		/* Let the request persist */
		    }				/* ENDif enabled */
		}
		if (persist) {
		    /*
		     *  The request should be allowed to persist, even
		     *  if no destination accepts it this time.
		     */
		    cp = environment + envpsize;
		    envp [envpcount++] = cp;
		    strcpy (cp, "PERSIST=Y");
		    envpsize += strlen (cp) + 1;
		}
		/*
		 * Now free up memory.
		 */
		pd_ptr = pipe_dest_header.next;
		while (pd_ptr != (struct pipe_dest *) NULL) {
		    pd_curr = pd_ptr;
		    pd_ptr = pd_ptr->next;
		    free (pd_curr);
		}
		server = queue->q.v1.pipe.server;
	}					/* Get server/args to execve */
	/*
	 *  Place the NIL pointer at the end of the environment var
	 *  list for the server.
	 */
	envp [envpcount] = (char *) 0;
	/*
	 *  In ALL cases, the current working directory is the NQS
	 *  root directory, and the appropriate environment has been
	 *  constructed for the server.
	 */
	if (queue->q.type != QUE_BATCH) {
		/*
		 *  A device, network, or pipe queue request is
		 *  being spawned.
		 */
		if (parseserv (server, argv) == -1) {	/* Break into args */
			/*
			 *  Too many server arguments in server command line.
			 */
			serexit (RCM_2MANYSVARGS, (char *) 0);
		}
		server = argv [0];	/* Get ptr to program to execve */
		if (queue->q.type == QUE_DEVICE) cp = device->name;
		else if (queue->q.type == QUE_NET) {
			cp = fmtmidname (queue->q.namev.to_destination);
		}
		else cp = queue->q.namev.name;
		argv [0] = argv0buffer;
		sprintf (argv [0], "%s %s", cp, "server");
	}
	/*
	 *  Send mail (and broadcast messages) as necessary notifying the 
	 *  user that their request is beginning or restarting execution.
	 */
	if (queue->q.type == QUE_BATCH || queue->q.type == QUE_DEVICE) {
		if (restartflag) {	/* Restarting execution */
			if (rawreq->flags & RQF_RESTARTMAIL) {
				mai_send (rawreq, (struct requestlog *) 0,
					  MSX_RESTARTING);
			}
		}
		else {			/* Beginning execution for the first */
			if (rawreq->flags & RQF_BEGINMAIL) {	/* time */
				mai_send (rawreq, (struct requestlog *) 0,
					  MSX_BEGINNING);
			}
			if (rawreq->flags & RQF_BEGINBCST) {
				nqs_broadcast (rawreq, (struct requestlog *) 0,
					  MSX_BEGINNING);
			}
		}
	}
        /*
         *  Set nice value for batch requests.
         */
        if (queue->q.type == QUE_BATCH) {
	    	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "nqs_reqser: setting nice level\n");

	  	if (rawreq->v.bat.ppnice > queue->q.v1.batch.ppnice)
                  enf_nic_limit (rawreq->v.bat.ppnice);
	  	else
	    	  enf_nic_limit (queue->q.v1.batch.ppnice);
        }
	/*
	 *  Set ourselves in our very own process group/family so that
	 *  the NQS daemon can kill us (and our children) as a group if
	 *  necessary.
	 */
	jid = getpid();
	setsid();
  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM,"nqs_reqser: Established new process group\n");

#if IS_UNICOS
	/* set up UNICOS job */
	{
  		int ujid;
	  	ujid = setjob((int) passwd->pw_uid, 0);
	    	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM,"nqs_reqser: Established new job %d\n",ujid);
	}
#endif /* UNICOS */

	if (queue->q.type == QUE_BATCH) {
		/*
		 *  We are spawning a batch request.
		 *  Enforce resource limits on the process or process group
		 *  while we're still root.
		 */
		enf_quo_limit (&rawreq->v.bat.ppcoresize,
			    rawreq->v.bat.infinite & LIM_PPCORE, LIM_PPCORE);
		enf_quo_limit (&rawreq->v.bat.ppdatasize,
			    rawreq->v.bat.infinite & LIM_PPDATA, LIM_PPDATA);
		enf_quo_limit (&rawreq->v.bat.pppfilesize,
			    rawreq->v.bat.infinite & LIM_PPPFILE, LIM_PPPFILE);
	    	enf_quo_limit (&rawreq->v.bat.prpfilespace,
			    rawreq->v.bat.infinite & LIM_PRPFILE, LIM_PRPFILE);
		enf_quo_limit (&rawreq->v.bat.ppstacksize,
			    rawreq->v.bat.infinite & LIM_PPSTACK, LIM_PPSTACK);
		enf_quo_limit (&rawreq->v.bat.ppworkset,
			    rawreq->v.bat.infinite & LIM_PPWORK, LIM_PPWORK);
#if 1
		enf_cpu_limit (&rawreq->v.bat.ppcputime,
			    rawreq->v.bat.infinite & LIM_PPCPUT, LIM_PPCPUT);
#endif
		enf_cpu_limit (&rawreq->v.bat.prcputime,
			    rawreq->v.bat.infinite & LIM_PRCPUT, LIM_PRCPUT);
		enf_quo_limit (&rawreq->v.bat.ppmemsize,
			    rawreq->v.bat.infinite & LIM_PPMEM, LIM_PPMEM);
		enf_quo_limit (&rawreq->v.bat.prmemsize,
			    rawreq->v.bat.infinite & LIM_PRMEM, LIM_PRMEM);
        }

        if (queue->q.type == QUE_BATCH) {
                /*
                *  Write an entry in an NQS accounting file.
                */
                fd_acct = open(NQSACCT_FILE,
                        O_WRONLY|O_APPEND|O_CREAT, 0644);
                if (fd_acct < 0) {
                        sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Error opening NQS account file;  Errno = %d\n", errno);
                } else {
                        sal_bytezero((char *)&acct_init1, sizeof(acct_init1));
                        acct_init1.h.type = NQSACCT_INIT_1;
                        acct_init1.h.length = sizeof(acct_init1);
                        acct_init1.h.jobid = jid;
                        strncpy(acct_init1.user, rawreq->username,
                                sizeof(acct_init1.user));
                        strncpy(acct_init1.queue, rawreq->quename,
                                sizeof(acct_init1.queue));
                        acct_init1.priority = queue->q.priority;
                        acct_init1.sub_time = rawreq->create_time;
                        acct_init1.start_time = rawreq->start_time;
                        acct_init1.init_time = time ((time_t *) 0);
                        acct_init1.orig_mid = rawreq->orig_mid;
			acct_init1.seqno = rawreq->orig_seqno;
                        strncpy(acct_init1.reqname, rawreq->reqname,
                                sizeof(acct_init1.reqname));
                        write(fd_acct, (char *)&acct_init1, sizeof(acct_init1));
			close (fd_acct);
                }
		localmid (&mymid);
		
        }

        if (queue->q.type == QUE_BATCH) {
#if 	HAS_PSET_IRIX_PSET
                char psetcmd[128];
        	/* TAMU MOD to set processor set */
                sprintf(psetcmd,"/sbin/pset -p %d %s >/dev/null",
                        getpid(),rawreq->quename);
                system(psetcmd);
#else
#if	HAS_PSET_DECOSF
	  	char psetcmd[128];
	  	/* SLH mod to set processor set */
	  	sprintf(psetcmd, "/usr/sbin/pset_assign_pid %s %d >/dev/null", &rawreq->quename[1], getpid());
	  	system(psetcmd);
#else
#if	HAS_PSET_SOLARIS
		char psetcmd[128];
		/* PXL mod to set processor set.
		 * Solaris processor sets are numbered increasing from 1,
		 * and NQS does not allow numbered queue names, so if we
		 * name the NQS queues 'q1', 'q2', etc. this will work
		 * (a la DECOSF processor set support).
		 */
		sprintf(psetcmd, "/usr/sbin/psrset -b %s %d >/dev/null", &rawreq->quename[1], getpid());
		system(psetcmd);
#endif /* HAS_PSET_SOLARIS */
#endif /* HAS_PSET_IRIX_PSET */
#endif /* HAS_PSET_DECOSF */
	
#if ADD_NQS_TMPDIR
		nqs_CreateTmpdir(rawreq, passwd, environment, envp, envpcount, &envpsize);
#endif
                /*
                 *  Display start information.  Note that we have to use
                 *  stderr, since stdout is used for devices....
                 */
                sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "nqs_reqser(): start %ld.%s (%s for %s) in %s\n",
                        rawreq->orig_seqno,
                        rawreq->quename,
			rawreq->reqname,
			rawreq->username,
                        hostname);
        }

	if (queue->q.type == QUE_BATCH || queue->q.type == QUE_DEVICE) {
		/* 
		 * For SGI set priority down low out of way of interactives
		 * if set for queue.
		 */
#if	IS_SGI
		if (queue->q.ndp != 0) {
			schedctl(NDPRI, 0, (int) queue->q.ndp);
		}
#endif
		/* Generic NQS v3.51.0-pre6
		 *
		 * We used to set the UID and GID to the owner of the
		 * job.  This is now done in nqsexejob(1) for batch jobs
		 */

		/*
		 *  Batch and device requests must now set their real and
		 *  effective user and group-ids to that of the request
		 *  owner.
		 *
		 *  Network queue and pipe queue servers must NOT do this.
		 *  Network queue and pipe queue servers must be spawned
		 *  as root.
		 */
		if (queue->q.type == QUE_DEVICE)
		{
			initgroups (passwd->pw_name, passwd->pw_gid);
			ngroups = getgroups (NGROUPS_MAX, gidset);
			setgroups (ngroups, gidset);
			setgid ((int) passwd->pw_gid);	/* Set login group-id */
			setuid ((int) passwd->pw_uid);	/* Set mapped user-id */
		}
	}
	/*
	 *  Explicitly set the signal receipt actions to their defaults so
	 *  that the server gets a completely "clean" signal environment.
	 */
	signal (SIGHUP, SIG_DFL);
	signal (SIGINT, SIG_DFL);
	signal (SIGQUIT, SIG_DFL);
	signal (SIGILL, SIG_DFL);
	signal (SIGTRAP, SIG_DFL);
	signal (SIGIOT, SIG_DFL);
#if !IS_LINUX
	signal (SIGEMT, SIG_DFL);
#endif
	signal (SIGFPE, SIG_DFL);
	signal (SIGBUS, SIG_DFL);
	signal (SIGSEGV, SIG_DFL);
#if !IS_LINUX
	signal (SIGSYS, SIG_DFL);
#endif
	signal (SIGPIPE, SIG_DFL);
	signal (SIGALRM, SIG_DFL);
	signal (SIGTERM, SIG_DFL);
#if	IS_SGI
	signal (SIGXCPU, SIG_DFL);
	signal (SIGXFSZ, SIG_DFL);
#endif
#if	( IS_POSIX_1 | IS_SYSVr4 ) && ! ( IS_ULTRIX )
	signal (SIGUSR1, SIG_DFL);
	signal (SIGUSR2, SIG_DFL);
#if	GPORT_HAS_SIGPWR
	signal (SIGPWR, SIG_DFL);
#endif
#else
#if	IS_BSD | IS_ULTRIX
	signal (SIGURG, SIG_DFL);
	signal (SIGSTOP, SIG_DFL);
	signal (SIGTSTP, SIG_DFL);
	signal (SIGCONT, SIG_DFL);
	signal (SIGTTIN, SIG_DFL);
	signal (SIGTTOU, SIG_DFL);
	signal (SIGIO, SIG_DFL);
	signal (SIGXCPU, SIG_DFL);
	signal (SIGXFSZ, SIG_DFL);
	signal (SIGVTALRM, SIG_DFL);
	signal (SIGPROF, SIG_DFL);
#else
BAD SYSTEM TYPE
#endif
#endif
	/*
	 *  ***TRIAGE***
	 *
	 *  Extreme schedule constraints have forced us to leave
	 *  network queues unimplemented (software implementation
	 *  by triage).  When the day arrives that network queues
	 *  are a reality, then the obvious code section below
	 *  should be changed to read:
	 */  
	 	/*
	 	 *  Report our "process-family" to the NQS daemon.
	 	 */
		/*   interclear();		      */
		/*   interw32i ((long) getppid()); */ /* Shepherd process-id */
		/*   interw32i ((long) getpid());  */ /* Our process-id */
		/*   inter (PKT_FAMILY);	      */
	/*
	 *  WITHOUT the surrounding "if" statement that PREVENTS
	 *  network queue requests from sending PKT_FAMILY packets
	 *  to the local NQS daemon.
	 */
	if (queue->q.type != QUE_NET) {		/* THIS IF STATEMENT SHOULD */
						/* EVENTUALLY BE REMOVED */
						/* (SEE ABOVE COMMENTS) */
		/*
		 *  Report our "process-family" to the NQS daemon.
		 */
		interclear();
		interw32i ((long) getppid());	/* Shepherd process-id */
		interw32i ((long) jid);		/* Our process-id */
		inter (PKT_FAMILY);
	}					/* (SEE ABOVE COMMENTS) */
	/*
	 *  Batch and device requests are not allowed to retain their
	 *  connection to the NQS daemon event/request pipe.  Only pipe
	 *  and network queue servers need to be able to talk directly to
	 *  the NQS daemon.
	 */
	if (queue->q.type == QUE_BATCH || queue->q.type == QUE_DEVICE) {
		close (Write_fifo);
	}
	/*
	 *  Batch requests are handled differently....
	 */
	if (queue->q.type == QUE_BATCH) {
		/*
		 *  Change directory to the user's home directory.
		 */
		if (chdir (passwd->pw_dir) == -1) {
			/*
			 *  We were unable to chdir() to the user's
			 *  home directory.
			 */
			serexit (RCM_UNABLETOEXE,
				 "Unable to chdir() to user home directory.");
		}
		/*
		 *  Set the file creation mask to the value that it
		 *  had when the user first submitted the request, with
		 *  exception that the umask will NOT be allowed to
		 *  disable WRITE access for the request owner.
		 *
		 *  We do this so that any stdout or stderr output file
		 *  created at this point, will be created such that it
		 *  will be writeable by the request owner.  This is
		 *  required if the request is to be restartable, and
		 *  a stdout or stderr file exists from a previously
		 *  interrupted execution of the batch request.
		 *
		 *  The umask will be set precisely later on.
		 */
		umask (rawreq->v.bat.umask & 0577);	/* Set partial umask */
		if ((rawreq->v.bat.stdout_acc & OMD_SPOOL) == 0) {
			/*
			 *  The standard-output file is to be accessed
			 *  directly.  We had to wait until after the chdir()
			 *  because the pathname could be relative.
			 */
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf(cp, "NQS_STDOUT=%s", rawreq->v.bat.stdout_name);
			envpsize += strlen(cp) + 1;
			/*
			 * If we have the -eo flag set, we assign the same
			 * name to the NQS_STDERR as NQS_STDOUT.  This is
			 * an indicator to sys_job to merge the two output
			 * pipes.
			 */
			if (rawreq->v.bat.stderr_acc & OMD_EO) {
			    cp = environment + envpsize;
			    envp [envpcount++] = cp;
			    sprintf(cp, "NQS_STDERR=%s", rawreq->v.bat.stdout_name);
			    envpsize += strlen(cp) + 1;
			}
		}
		if ((rawreq->v.bat.stderr_acc & OMD_SPOOL) == 0 &&
		    (rawreq->v.bat.stderr_acc & OMD_EO) == 0) {
			/*
			 *  The standard-error file is to be accessed
			 *  directly.  We had to wait until after the
			 *  chdir() because the pathname may have been
			 *  specified as a relative pathname.
			 */
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf(cp, "NQS_STDERR=%s", rawreq->v.bat.stderr_name);
			envpsize += strlen(cp) + 1;
		}
		if (rawreq->v.bat.stderr_acc & OMD_EO) {	/* -eo */
			/*
			 *  The stderr output of the request is to be directed
			 *  to the stdout file.
			 */
			standerr = fcntl (standout, F_DUPFD, 0);
			fcntl (standerr, F_SETFL, O_APPEND);
		}
		/*
		 *  Set the file creation mask to the value that it
		 *  had when the user first submitted the request.
		 */
		umask (rawreq->v.bat.umask);	/* Set precise umask */
		if (restartflag) {
			/*
			 *  This batch request is being restarted.
			 *  Write an appropriate message on the stdout
			 *  file indicating this fact.
			 */
			time (&timeval);	/* Get current time */
			cp = environment + envpsize;
			envp [envpcount++] = cp;
			sprintf(cp, "NQS_RESTART='%s'", fmttime(&timeval));
			envpsize += strlen(cp) + 1;
		}
	}
	/*
	 *  Place the NIL pointer at the end of the environment var
	 *  list for the server.
	 */
	envp [envpcount] = (char *) 0;
	/*
	 *  Display debug information.  Note that stdout is currently in use
	 *  by a device at this time ...
	 */
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Execve'ing server: %s.\n", server);
	
	i = 0;
	while (argv [i] != (char *) 0) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "argv[%d]=%s.\n", i, argv [i]);
	    i++;
	}
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Environment values:\n");
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "  envpcountmax = %d\n",envpcountmax);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "  envpcount    = %d\n",envpcount);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "  envpsizemax  = %d\n",envpsizemax);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "  envpsize     = %d\n",envpsize);
	if (envpcount >= envpcountmax ||
	    envpsize > envpsizemax) {
	    /*
	     *  There is not sufficient space in
	     *  the environment arrays.
	     */
	    serexit (RCM_2MANYENVARS, "Dynamically allocated environment size too small\n");
	}
        i = 0;
	while (envp [i] != (char *) 0) {
	    sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "envp[%d]=%s.\n", i, envp [i]);
	    i++;
	}
  
	if (queue->q.type == QUE_BATCH) {
		/*
		 *  Mark the request as executing, if the request is
		 *  being executed for the first time.
		 *
		 *  WARNING:  We must do this now, BEFORE rearranging
		 *	      the file descriptor associations for the
		 *	      shell (see below).  Otherwise, file
		 *	      descriptor #3 will no longer be open
		 *	      as the request message/completion pipe
		 *	      back to the shepherd process.
		 */
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "nqs_reqser: executing batch process\n");
		serexecute ();
		/*
		 *  Force the shell script file to be on file descriptor
		 *  #0.  Force the stdout and stderr files to be on file
	 	 *  descriptors #1 and #2 respectively.
		 */
		if (script != 0) {
			close (0);
			fcntl (script, F_DUPFD, 0);
			close (script);
		}
		if (standout != 1) {
			close (1);
			fcntl (standout, F_DUPFD, 1);
			close (standout);
		}
		if (standerr != 2) {
			close (2);
			fcntl (standerr, F_DUPFD, 2);
			close (standerr);
		}
		/*
		 *  Close all remaining file descriptors.
		 */
		i = sysconf ( _SC_OPEN_MAX );	/* #of file descrs per proc */
		while (--i >= 3) close (i);	/* Close all other descrs */
		/*
		 *  Exec the proper shell.
		 */
		execve (server, argv, envp);
		/*
		 *  Fall through if execve() fails.
		 *
		 *  We have severed all connections to the shepherd
		 *  process that created us.  We must invoke a setuid
		 *  root program (shlexefai), to signal the shepherd
		 *  that the execve() of the shell, failed.
		 */
                filename = getfilnam ("shlexefai", LIBDIR);
                if (filename != (char *)NULL) {
                        /*
                         * getfilnam allocates memory for filename.  Copy
                         * filename into local buffer and free memory.
                         */
                        strcpy (buffer, filename);
                        relfilnam (filename);
                        /*
                         *  Format errno as argv [1] for shlexefai.
                         */
                        strcpy (argv0buffer, "shlexefai");
                        sprintf (minusname, "%1d", errno);
                        /*
                         *  Write shell execve() diagnostic on the stderr
                         *  file of the batch request.
                         */
                        fprintf (stderr,
                                 "FATAL:  Execve() of shell: %s failed.\n",
                                 server);
                        fprintf (stderr, "INFO:   %s.\n", asciierrno());
                        fflush (stdout);
                        fflush (stderr);
                        /*
                         *  Execve() shlexefai to inform the shepherd about
                         *  the shell execve() failure.
                         */
                        argv [0] = argv0buffer;
                        argv [1] = minusname;
                        argv [2] = (char *) 0;
                        envp [0] = (char *) 0;
                        execve (buffer, argv, envp);
                }
		/*
		 *  We are really having a bad day!  Nothing works!
		 *  Commit seppuku.  We cannot even display a diagnostic
		 *  to the NQS log file because our connection with the
		 *  NQS log process has been severed.
		 */
		kill (getpid(), SIGKILL);
	}
	/*
	 *  If the request is a device request, then the request must
	 *  be marked as executing.
	 */
	if (queue->q.type == QUE_DEVICE ) serexecute ();
	/*
	 *  We are spawning a device, network, or pipe queue request.
	 *  Execve() the appropriate server.
	 */
	execve (server, argv, envp);
	/*
	 *  Execve() failed.
	 */
	serexit (RCM_SEREXEFAI, asciierrno());
}


/*** mkspool
 *
 *
 *	int mkspool():
 *
 *	Make a standard error or standard output file for a batch
 *	request in the NQS output spooling directory.
 *
 *	WARNING!!!!!!!!!!!!!!!!
 *		The EXTREMELY serious student will note that this
 *		function is invoked while the calling shell process
 *		is still running as root.
 *
 *		This is done because someday, NQS may be linked
 *		with the Newcastle distributed file access library.
 *		Since the Newcastle library converts ALL file paths
 *		to ABSOLUTE paths on the appropriate machine when
 *		invoking the requisite system call, NQS must still
 *		be running as root to safely create the spool output
 *		files in the spooling directory, since the spooling
 *		directory resides BELOW the NQS private directory,
 *		which only allows root access.
 *
 *	Returns:
 *	      >=0: if successful, as the opened file descriptor
 *		   for the stderr file;
 *	       -1: if an error occurs in which case errno is set.
 */
static int mkspool (
	struct rawreq *rawreq,		/* Rawreq structure for request */
	char *(*namefn)(long int, Mid_t),		/* Ptr to fn returning ptr to char */
	struct passwd *passwd,		/* Password file entry for owner */
	int filemask)			/* File create/truncate flag */
{
	int fd;		/* File descriptor */
	char *path;		/* Ptr to temp file path */
	int cmask;		/* umask */

	/*
	 *  Generate name of temporary spooled output file.
	 */
	path = (*namefn) (rawreq->orig_seqno, rawreq->orig_mid);
	/*
	 *  Set the file creation mask to the value that it
	 *  had when the user first submitted the request, with
	 *  the exception that the umask will NOT be allowed to
	 *  disable WRITE access for the request owner.
	 *
	 *  We do this so that the output return algorithm on
	 *  retry will not have to worry about being rebuffed
	 *  at the output file destination by a lack of write-
	 *  access.  (Note that the output return algorithm
	 *  replicates the permissions of the original file
	 *  for the destination file.)
	 */
	cmask = umask (rawreq->v.bat.umask & 0577);	/* Set partial umask */
	if ((fd = open (path, filemask | O_WRONLY | O_APPEND, 0777)) == -1) {
		return (-1);
	}
	umask (cmask);			/* Restore the file create mask */
	/*
	 *  Make sure that the user pays for the spooled disk space.
	 *  (The spool file was created while we were running as root).
	 */
	chown (path, passwd->pw_uid, passwd->pw_gid);
	return (fd);
}


/*** seracknowledge
 *
 *
 *	void seracknowledge():
 *
 *	Catch SIGPIPE request state set to executing acknowledgement
 *	from the server shepherd process.
 */
static void seracknowledge( int unused )
{
	Stateset = 1;			/* Request state has been set */
}

 
/*** serexecute
 *
 *
 *	void serexecute():
 *
 *	Tell the shepherd process to set the request state to
 *	executing.
 */
static void serexecute ()
{
	void (*prevsigfn)(int);		/* Previous signal function */
	struct servermssg msg_packet;	/* Message packet */

	msg_packet.rcm = RCM_EXECUTING;
        msg_packet.id = jid;
	msg_packet.mssg [0] = '\0';
	/*
	 *  Set signal handler to catch acknowledge signal from the
	 *  server shepherd process.
	 */
	Stateset = 0;			/* Clear semaphore */
	prevsigfn = signal (SIGPIPE, seracknowledge);
	/*
	 *  WARNING:	The file descriptor below (#3) must agree with
	 *		../src/nqs_spawn and ../lib/serexit.c.
	 */
	if (write (3, (char *) &msg_packet, sizeof (msg_packet)) !=
		sizeof (msg_packet)) {
		/*
		 * We cannot communicate with the shepherd through
		 * the pipe. Exit so the shepherd's read() will return.
		 */
		exit (0);
	}
	/*
	 *  Loop waiting for acknowlege signal from shepherd process.
	 */
	while (!Stateset) pause();	/* Wait for signal */
	signal (SIGPIPE, prevsigfn);	/* Restore SIGPIPE handler */
}
/*
 * This routine determines the path.  It first checks a file in /etc called
 * environment.  This file is present on AIX,  and can be added to other
 * systems.  The routine checks the lines of the file for a line starting
 * with "PATH=" and if found, uses the rest of the line as the path (without
 * checking it!).  Otherwise,  it uses guess for the proper path.
 */
static char *get_the_path(struct passwd *passwd)
{
    static char    the_path[MAX_PATHNAME+1];
    FILE    *environment_file;
    char    env_line[128];
    char    *cp;
    
    environment_file = fopen("/etc/environment",  "r");
    if ( environment_file != (FILE *) 0 ) {
	while ( (fgets (env_line, sizeof(env_line), environment_file )
		!= (char *) 0) ) {
	    if (env_line[0] == '#') continue;
	    cp = strchr( env_line,  '\n');
            if (cp != (char *) 0 ) *cp = '\0';
	    if ( !strncmp(env_line,  "PATH=",  5) ) {
		 strcpy(the_path,  env_line);
		 fclose ( environment_file );
		 return (the_path); 
	    }   
	}
        fclose ( environment_file );
    }
    /*
     * The file was not found, or the path was not in it.
     */
    if (passwd->pw_uid == 0) {
	/*
	 *  We are running as root.  The default path
	 *  is slightly different.
	 */
#if	IS_POSIX_1 | IS_SYSVr4
	strcpy (the_path,  "PATH=/bin:/etc:/usr/bin:/sbin:/usr/sbin:/usr/local/sbin:/usr/local/bin");
     } else {
	/*
	 *  The request is not owned by root.
	 */
	strcpy (the_path,  "PATH=:/bin:/usr/bin");
#else
#if	IS_BSD
	strcpy (the_path, "PATH=/usr/ucb:/bin:/etc:/usr/bin");
     } else {
	/*
	 *  The request is not owned by root.
	 */
	strcpy (the_path, "PATH=:/usr/ucb:/bin:/usr/bin");
#else
BAD SYSTEM TYPE
#endif
#endif
    }
    return (the_path);
}
