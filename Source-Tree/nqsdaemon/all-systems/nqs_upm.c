/*
 * nqsd/nqs_upf.c
 * 
 * DESCRIPTION:
 *
 *	NQS manager set update module.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	June 16, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>
#include <libnqs/transactcc.h>		/* Transaction completion codes */

static int validmgr ( uid_t account_uid, Mid_t account_mid );

/*** upm_addnqsman
 *
 *
 *	long upm_addnqsman():
 *	Add an NQS manager account to the NQS manager set.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful;
 *		TCML_ALREADEXI:	if the specified account is already
 *				present in the NQS manager set.
 */
long upm_addnqsman (
	uid_t account_uid,		/* NQS manager account user-id */
	Mid_t account_mid,		/* NQS manager account machine-id */
	int privilege_bits)		/* Privilege bits */
{
	if (validmgr (account_uid, account_mid)) {
		/*
		 *  The specified account to add to the NQS manager
		 *  set is already present in the set.
		 */
		return (TCML_ALREADEXI);
	}
	udb_addnqsman (account_uid, account_mid, privilege_bits);
	return (TCML_COMPLETE);		/* Add the account to the */
					/* NQS manager set */
}


/*** upm_delnqsman
 *
 *
 *	long upm_delnqsman():
 *	Delete an NQS manager account from the NQS manager set.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful;
 *		TCML_NOSUCHMAN:	if the specified manager account was not
 *				present at the correct privilege level
 *				in the NQS manager set.
 *		TCML_ROOTINDEL:	if an attempt is made to delete "root"
 *				from the NQS manager set.
 */
long upm_delnqsman (
	uid_t account_uid,		/* NQS manager account user-id */
	Mid_t account_mid,		/* NQS manager account machine-id */
	int privilege_bits)		/* Privilege bits */
{
	if (account_uid == 0 && account_mid == Locmid) {
		/*
		 *  The "root" account can never be deleted from the NQS
		 *  manager set.
		 */
		return (TCML_ROOTINDEL);
	}
	if (validmgr (account_uid, account_mid) == privilege_bits) {
		/*
		 *  The specified account to delete from the NQS manager
		 *  set is present at the specified privilege level.
		 */
		udb_delnqsman (account_uid, account_mid);
		return (TCML_COMPLETE);	/* Delete the account from the */
					/* NQS manager set */
	}
	return (TCML_NOSUCHMAN);	/* No such manager account */
}


/*** validmgr
 *
 *
 *	validmgr():
 *
 *	Return the (non-zero) privilege bits if the specified
 *	manager account is defined in the NQS manager set.
 *	Otherwise return 0.
 */
static int validmgr (
	uid_t account_uid,		/* Manager account user-id */
	Mid_t account_mid)		/* Manager account machine-id */
{
	struct gendescr *descr;

	seekdbb (Mgrfile, 0L);		/* Seek to the beginning of the NQS */
					/* manager list file */
	/*
	 *  Search for the entry to be added, to see if it is already
	 *  present.
	 */
	descr = nextdb (Mgrfile);
	while (descr != (struct gendescr *) 0) {
		if (descr->v.mgr.manager_uid == account_uid &&
		    descr->v.mgr.manager_mid == account_mid) {
			/*
			 *  He or she is a manager.
			 */
			return (descr->v.mgr.privileges);
		}
		else descr = nextdb (Mgrfile);
	}
	return (0);			/* He or she is not a manager. */
}
