/*
 * nqsd/nqs_upf.c
 *
 * DESCRIPTION 
 *
 *	NQS forms set update module.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	June 16, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>
#include <libnqs/transactcc.h>		/* Transaction completion codes */
#include <string.h>

/*** upf_addfor
 *
 *
 *	long upf_addfor():
 *	Add a form to the local NQS forms set.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful;
 *		TCML_ALREADEXI:	if the specified form name is already
 *				present in the NQS forms set.
 */
long upf_addfor (char *newform)
{
	if (upf_valfor (newform)) {
		/*
		 *  The specified form is already present in the forms
		 *  set.
		 */
		return (TCML_ALREADEXI);
	}
	udb_addfor (newform);		/* Add the form to the forms set */
	return (TCML_COMPLETE);
}


/*** upf_delfor
 *
 *
 *	long upf_delfor():
 *	Delete a form from the local NQS forms set.
 *
 *	Returns:
 *		TCML_COMPLETE:	 if successful;
 *		TCML_NOSUCHFORM: if the specified form did not exist
 *				 in the NQS forms set.
 */
long upf_delfor (char *form)
{
	if (upf_valfor (form)) {
		/*
		 *  The specified form to delete is present in the forms
		 *  set.
		 */
		udb_delfor (form);	/* Delete the form from the forms */
					/* set */
		return (TCML_COMPLETE);
	}
	return (TCML_NOSUCHFORM);	/* No such form */
}


/*** upf_valfor
 *
 *
 *	upf_valfor():
 *
 *	Return non-zero if the specified form is defined in the
 *	local NQS forms set.  Otherwise return 0.
 */
int upf_valfor (char *formname)
{
	struct gendescr *descr;

	seekdbb (Formsfile, 0L);	/* Seek to the beginning of the NQS */
					/* forms list file */
	/*
	 *  Search for the entry to be added, to see if it is already
	 *  present.
	 */
	descr = nextdb (Formsfile);
	while (descr != (struct gendescr *) 0) {
		if (strcmp (descr->v.form.forms, formname) == 0) {
			/*
			 *  The forms exists.
			 */
			return (1);	/* Form is valid */
		}
		else descr = nextdb (Formsfile);
	}
	return (0);			/* Form is not valid */
}
