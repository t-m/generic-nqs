/*
 * nqsd/nqs_vtimer.c
 * 
 * DESCRIPTION:
 *
 *	This module contains the two public functions of:
 *
 *		nqs_vtimer(), and
 *		nqs_valarm()
 *
 *	which respectively set virtual timers, and enable virtual
 *	timers.  Nqs_valarm() also catches the SIGALRM signals
 *	used to implement the virtual timer mechanism.
 *
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <signal.h>
#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>		/* Global vars */
#include <errno.h>
#include <time.h>
#include <unistd.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

static int setalarm ( void );

#define	MAX_ALARMS	4	/* Max number of alarms handled */
				/* 1 for -a request activation; */
				/* 1 for pipe queue expiration; */
				/* 1 for pre-arrive expiration; */
				/* 1 for nqs_aboque.c; */


/*
 *	Variables local to this module.
 */
static short n_vtimers = 0;	/* Number of alarms in effect */
static struct {
	time_t vtime;		/* GMT time at which fn should be called */
	void (*fn_to_call)(void);	/* Function to call */
} vtimer [MAX_ALARMS];

/*
 *	Variables external to this module.
 */

extern void(*nqs_tfunc)(void);

/*** nqs_vtimer
 *
 *
 *	void nqs_vtimer():
 *
 *	Set a "virtual timer" at which time the specified function
 *	is to be called with no arguments.  The timer set by this call
 *	however will NOT be enabled until an nqs_valarm() call is made.
 */
void nqs_vtimer (
	time_t *ptrvtime,		/* GMT time at which fn should go */
	void (*fn_to_call)(void))	/* Function to call when time is up */
{
	int i;			/* Index var */
	int vtime;		/* GMT time at which fn should go */

	vtime = *ptrvtime;		/* Get time */
	i = 0;
	while (i < n_vtimers && vtimer [i].fn_to_call != fn_to_call) {
		i++;
	}
	if (i >= n_vtimers) {
		/*
		 *  The specified function to be activated is not
		 *  already listed in the call table.
		 */
		if (n_vtimers >= MAX_ALARMS) {
			/*
			 *  Cannot set alarm.  Table is full.
			 */
		  	errno = 0;
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "Virtual timer table full in call to to nqs_vtimer().\n");
		}
		else {
			/*
			 *  Add this function to the table.
			 */
			vtimer [n_vtimers].vtime = vtime;
			vtimer [n_vtimers].fn_to_call = fn_to_call;
			n_vtimers++;		/* One more timer going */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_vtimer(): queued function for later use\n");
		}
	}
	else {
		/*
		 *  The specified function was already in the table
		 *  to be activated at the proper time.
		 */
		if (vtimer [i].vtime > vtime) vtimer [i].vtime = vtime;
	}
}


/*** nqs_valarm
 *
 *
 *	nqs_valarm():
 *
 *	Enable ALL virtual timers as set by the nqs_vtimer() call and
 *	also catch a SIGALRM signal to implement the virtual timer schema.
 */
void nqs_valarm ( int unused )
{
	int i;
	time_t timenow;	/* Current GMT time */

	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_valarm(): Entered\n");
#if 0
	signal (SIGALRM, nqs_valarm);	/* Re-instate SIGALRM handler */
#endif
	if (n_vtimers) {		/* Timer(s) were set */
		/*
		 *  Loop until all remaining timers are set to
		 *  go off in the future.
		 */
		do {
			i = 0;
			timenow = time ((time_t *)0);
			/*
			 *  Loop to service timers which have gone off.
			 */
			while (i < n_vtimers) {
				if (vtimer [i].vtime > timenow) {
					/*
					 *  This timer has not gone off.
					 */
					i++;	/* Examine next timer */
					continue;
				}
				/*
				 *  This timer has gone off.
				 */
				(*vtimer [i].fn_to_call)();
				n_vtimers--;
				if (i < n_vtimers) {
					/*
					 *  Another timer follows the
					 *  one that just went off.
					 *  Copy the succeeding timer
					 *  state into the one being
					 *  deleted.
					 */
					vtimer [i].vtime
						= vtimer [n_vtimers].vtime;
					vtimer [i].fn_to_call
						= vtimer [n_vtimers].fn_to_call;
				}
			}
		} while (setalarm() == -1);
	}
}


/*** setalarm
 *
 *
 *	int setalarm():
 *	Set next closest alarm.
 *
 *	Returns:
 *		0: if an alarm was successfully set (or no timers
 *		   existed);
 *	       -1: if a timer exists which has gone off and needs to
 *		   be handled immediately.
 */
static int setalarm (void)
{
	int i;			/* Index */
	time_t min_time;
	time_t timenow;

	if (n_vtimers) {
		/*
		 *  Now, search for the alarm that should go off the
		 *  soonest.
		 */
		time (&timenow);	/* Get current time */
		min_time = vtimer [0].vtime;
		for (i=1; i < n_vtimers; i++) {
			if (vtimer [i].vtime < min_time) {
				min_time = vtimer [i].vtime;
			}
		}
		if (min_time <= timenow) {
			/*
			 *  We have a timer that should go off NOW.
			 */
			return (-1);		/* No timer set */
		}
		min_time -= timenow;		/* Get alarm duration */
		if (min_time > 32767) {
			/*
			 *  On some machines, integers are 16-bits.  NQS
			 *  may find itself scheduling a timer that is
			 *  several days in duration.  That many seconds
			 *  cannot be represented in a 16-bit integer.
			 *  For such alarms, we set as many smaller
			 *  alarms as necessary to reach the proper
			 *  time.
			 */
			min_time = 32767;
		}
#if 0
		alarm ((unsigned) min_time);	/* Set the alarm */
#endif
	}
	return (0);				/* Success */
}
