/*
 * nqsd/nqs_delreq.c
 *
 * DESCRIPTION
 *
 *	Delete an NQS request.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <errno.h>
#include <libnqs/transactcc.h>		/* Transaction completion codes */
#include <signal.h>
#include <libnqs/nqsxvars.h>		/* NQS external vars and dirs  */
#include <libnqs/nqsacct.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <string.h>
#include <unistd.h>

#if	IS_SGI	
#include <sys/syssgi.h>
#include <stdlib.h>
#include <time.h>
#include <sys/sysmp.h>
#include <sys/proc.h>
#include <sys/var.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#if   IS_IRIX5 | IS_IRIX6
#include <sys/fault.h>
#include <sys/syscall.h>
#include <sys/procfs.h>
#endif
int	memfd;
int	procsize;
int	do_count;
int	total_time;	/* total cpu time */
time_t	start_time;
#endif	/* SGI */

#if	IS_IBMRS
#define	    MAX_PROC	1000
#include <nlist.h>
#include <procinfo.h>	
#include <stdlib.h>
#include <time.h>
#include <sys/proc.h>
#include <sys/user.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
int	memfd;
struct nlist locnlist;
int	do_count;
int	total_time;	/* total cpu time */
time_t	start_time;
#endif /* IBMRS */

#if	IS_SGI | IS_IBMRS
static int get_node ( struct proc *pointer, int pid_oi );
static void summary ( pid_t pid_oi );
#endif
static void write_acct_record ( struct request *req );
#if	IS_IBMRS
static get_node_finduser( long pid,  struct user *user );
#endif

/*** nqs_delreq
 *
 *
 *	long nqs_delreq():
 *	Delete an NQS request.
 *
 *	Returns:
 *		TCML_NOSUCHREQ:	 if the specified request does not
 *				 exist on this machine.
 *		TCML_NOSUCHSIG:  if the specified request was running
 *				 or in transit, and a signal trans-
 *				 mission was specified for a signal
 *				 not recognized by the supporting
 *				 UNIX implementation.
 *		TCML_NOTREQOWN:	 if the mapped user-id does not match
 *				 the current mapped user-id of the
 *				 request owner.
 *		TCML_PEERDEPART: if the specified request is presently
 *				 being routed by a pipe queue.
 *		TCML_REQDELETE:	 if the specified request was deleted.
 *		TCML_REQRUNNING: if the specified request is running,
 *				 and no signal was specified.
 *		TCML_REQSIGNAL:	 if the request was signalled (or will
 *				 be signalled immediately upon receipt
 *				 of the process-family group.
 *
 */
long nqs_delreq (
	uid_t mapped_uid,		/* Mapped owner user-id */
	long orig_seqno,		/* Req sequence number */
	Mid_t orig_mid,			/* Machine-id of request */
	Mid_t target_mid,		/* Target Machine-id of request */
	int sig,			/* Signal to send to processes */
					/* in a running request.  Sig = 0 */
					/* if no signal should be sent to */
					/* the request processes if running */
	int state)			/* Request queue state: RQS_ */
{
	struct request *predecessor;	/* Predecessor in request set in */
					/* queue */
	struct request *req;		/* Ptr to request structure for */
					/* located req, if found */
	short reqindex;			/* Request index in Runvars if */
					/* the request is running, or */
					/* index in Runvars if subrequest */
					/* is transporting parent request */
					/* to its destination via a pipe/ */
					/* network queue pair */

	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_delreq: mapped_uid = %d.\n", mapped_uid);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_delreq: orig_seqno = %d.\n", orig_seqno);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_delreq: orig_mid = %d.\n", orig_mid);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_delreq: target_mid = %d.\n", target_mid);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_delreq: sig = %d.\n", sig);
        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_delreq: state = %d.\n", state);
  
	/*
	 *  Search for the request.
	 */
	if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) {
		/*
		 *  The request was not found in any of the local queues.
		 */
		return (TCML_NOSUCHREQ);
	}
        if (req->v1.req.uid != mapped_uid && mapped_uid != 0) {
		/*
		 *  This request cannot be affected by the client.
		 */
		return (TCML_NOTREQOWN);
	}
	if (state == RQS_DEPARTING) {
		/*
		 *  MOREHERE someday.  The batch request is returning
		 *  output files to the proper locations.  Send SIGTERM.
		 *
		 *	-- OR --
		 *
		 *  A request may be in the very process of transiting
		 *  to another machine.  Send SIGINT.
		 *
		 *  But for now, this ****TRIAGE**** implementation simply
		 *  returns peer-depart.
 		 */
		return (TCML_PEERDEPART);
	}
	if (state == RQS_RUNNING) {	/* The request is running, or */
					/* is being routed by a pipe queue */
		if (req->queue->q.type == QUE_PIPE ||
		   (req->status & RQF_PREDEPART)) {	/* ****TRIAGE**** */
			/*
			 *  The request is presently being routed by a
			 *  pipe queue.
			 *
			 *  This ****TRIAGE**** implementation does not
			 *  allow the deletion of requests presently
			 *  being routed by a pipe queue.
	 		 */
			return (TCML_PEERDEPART);
		}
		if (sig) {
			/*
			 *  The request is presently running, and is NOT
			 *  being routed by a pipe queue.
			 */
			if (sig < 1 || sig > GPORT_SIG_SIGPWR) {
				/*
				 *  No such signal.
				 */
				return (TCML_NOSUCHSIG);
			}
			/*
			 * Write out the accounting record in case ...
			 */
			write_acct_record(req);
			/*
			 *  Send the signal.  The signal may not necessarily
			 *  kill the request (unless it is of the SIGKILL (9)
			 *  variety).
			 */
			reqindex = req->reqindex;
			if ( (Runvars+reqindex)->process_family == 0) {
				/*
				 *  The process group/family of the server
				 *  has not yet been reported in....  Queue
				 *  the signal so that it can be sent later
				 *  upon receipt of the process group/family
				 *  packet for this request.
				 */
				if ((req->status & RQF_SIGQUEUED) == 0) {
					/*
					 *  We are NOT allowed to overwrite
					 *  any previously pending signal!
					 *  For example, nqs_aboque() may
					 *  have queued a signal for this
					 *  request.  Nqs_aboque() always wins.
					 */
					req->status |= RQF_SIGQUEUED;
					(Runvars+reqindex)->queued_signal = sig;
				}
			}
			else {
				/*
				 *  The process group/family of the server
				 *  is known.  Send the signal.
				 */
				kill (-(Runvars+reqindex)->process_family, sig);
				
				sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "nqs_delreq: sent %d signal to %d process family.\n", sig, Runvars[reqindex].process_family);
			}
			return(TCML_REQSIGNAL);	/* Req has been signalled, */
						/* or req will be signalled*/
						/* immediately upon receipt*/
						/* of the process-family */
		}
		return (TCML_REQRUNNING);	/* Request not deleted */
	}
	/*
	 *  The request is not running.
	 *  Simply delete the request.
	 */
	nqs_deque (req);		/* Remove the request from the queue */
					/* setting the QUE_UPDATE bit */
	nqs_disreq (req, 1);		/* Dispose of request */
	udb_qorder (req->queue);	/* Update queue image in database */

#if	ADD_TAMU | ADD_NQS_DYNAMICSCHED
	/* TAMU MOD -- resort batch queues */
	bsc_resort();
#endif

	return (TCML_REQDELETE);	/* Request deleted */
}
/*
 * Write out the accounting record -- in case the shepard drops the ball!
 */
static void write_acct_record(struct request *req)
{
        int fd_acct;                    /* Accounting file descriptor */
        struct nqsacct_fin1 acct_fin1;  /* Accounting structure to report */
                                        /* Cpu usage                      */

        fd_acct = open(NQSACCT_FILE,
                        O_WRONLY|O_APPEND|O_CREAT, 0644);
        if (fd_acct < 0) {
            sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Error opening NQS account file;  Errno = %d\n", errno);
        } else {
            sal_bytezero((char *)&acct_fin1, sizeof(acct_fin1));
            acct_fin1.h.type = NQSACCT_STAT;
            acct_fin1.h.length = sizeof(acct_fin1);
            acct_fin1.h.jobid = (Runvars + req->reqindex)->process_family;
            strncpy(acct_fin1.user,getusenam(req->v1.req.uid),
                                sizeof(acct_fin1.user));
            strncpy(acct_fin1.queue, req->queue->q.namev.name,
                                sizeof(acct_fin1.queue));
#if     IS_SGI | IS_IBMRS
	    summary((Runvars + req->reqindex)->process_family);
            acct_fin1.tms_stime = 0;
            acct_fin1.tms_utime = total_time;
#else
/* TAMU FIXME - need to add per job summary for UNICOS */
#if     !IS_BSD
	    /*
	     * No way to check these,  so just give as zero.
	     */
            acct_fin1.tms_stime = 0;
            acct_fin1.tms_utime = 0;
#else
#if     IS_BSD
            acct_fin1.s_sec = 0;
            acct_fin1.s_usec = 0;
            acct_fin1.u_sec = 0;
            acct_fin1.u_usec = 0;
#endif
#endif
#endif
             acct_fin1.orig_mid = req->v1.req.orig_mid;
	     acct_fin1.seqno = req->v1.req.orig_seqno;
	     acct_fin1.fin_time = time ((time_t *) 0);
        }
        write(fd_acct, &acct_fin1, sizeof(acct_fin1));
        close(fd_acct);
  }


#if 	IS_IRIX6
#include <sys/types.h>
#include <sys/dir.h>
	/* for IRIX 6, avoid 64 bit KMEM , and go straight to /proc */
static void summary( pid_t	proc_family)
{
	int	status;
        int     fd;
        int     retval;
	prpsinfo_t prpsinfo;
        char char_pid[32];
	pid_t	sesid;
	DIR 	*dirp;
	struct direct *dir;

	total_time = start_time = 0;

	/* use /proc to read leader info directly */
        sprintf(char_pid, "/proc/pinfo/%05d", proc_family);
        fd = open (char_pid, O_RDONLY);
        if (fd != -1) {
	    retval = ioctl(fd, PIOCPSINFO, &prpsinfo);
	    start_time = prpsinfo.pr_start.tv_sec;
	    sesid = prpsinfo.pr_sid;
            close (fd);
        }

	/* walk /proc, accumulating time for all procs in sesid */
	dirp = opendir("/proc/pinfo");
	while ((dir = readdir(dirp)) != NULL) {
		sprintf(char_pid, "/proc/pinfo/%s",dir->d_name);
		fd = open(char_pid,O_RDONLY);
		if (fd != -1) {
			retval = ioctl(fd,PIOCPSINFO, &prpsinfo);
			if (prpsinfo.pr_sid == sesid) {
#ifdef IS_IRIX5
				total_time += prpsinfo.pr_time.tv_sec;
#else
				total_time += prpsinfo.pr_time.tv_sec
					   +  prpsinfo.pr_ctime.tv_sec;
#endif
			}
			close(fd);
		}
	}
}


#else

#if	IS_SGI

static void summary(pid_t pid_oi)
{

	struct	tm  *start_tm;
	int	status;
	struct proc	*poffset;
	struct	proc   proc_buff;

	total_time = 0;
	start_time = 0;
	/* printf( "pid is %d\n", pid_oi); */
	procsize = sysmp(MP_KERNADDR,  MPKA_PROCSIZE);
	/*printf ("Procsize is %d\n", procsize); */
	poffset = (struct proc *) sysmp(MP_KERNADDR,  MPKA_PROC);
	/*printf ("pointer is %x (or %d)\n", poffset, poffset); */
	memfd = open ("/dev/kmem", O_RDONLY);
	if (memfd == -1) {
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_delreq: Error opening /dev/kmem, errno: %d\n", errno);
	}
	poffset =  (struct proc *) ((int) poffset & ~0x80000000);
	/* printf ("pointer is %x (or %d)\n", poffset, poffset); */
	status = lseek(memfd, (int) poffset, SEEK_SET);
	if (status != (int) poffset) {
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_delreq: Error in lseek, errno: %d\n", errno);
	}
	status = read (memfd, &proc_buff, procsize);
	if (status != procsize) {
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_delreq: Error in read, errno: %d\n", errno);
	}
	poffset =  proc_buff.p_child;
	do_count = 0;
	get_node(poffset, pid_oi);
}
static get_node(struct proc *pointer, int pid_oi)
{
	int	status;
	struct  user  *uptr;
	struct user   auser;
	struct	proc   proc_buff;
#if     IS_IRIX5
        int     fd;
        int     retval;
        prstatus_t prstatus;
        char char_pid[16];
#endif


	if (pointer == 0) return(0);
        pointer =  (struct proc *) ( (int )pointer & ~0x80000000);
        status = lseek(memfd, (int) pointer, SEEK_SET);
        if (status != (int) pointer) {
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_delreq: Error in lseek, errno: %d\n", errno);
	    return (0);
        }
        status = read (memfd, &proc_buff, procsize);
        if (status != procsize) {
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_delreq: Error in lseek, errno: %d\n", errno);
	    return (0);
        }
	if (proc_buff.p_pid == pid_oi) do_count++;
        /* if (do_count) printf(">>>>>>>>Pid is %d\n", proc_buff.p_pid); */
	uptr = &auser;
	status = syssgi (SGI_RDUBLK, proc_buff.p_pid, uptr, sizeof(auser) );
#if   IS_IRIX5
        if (do_count) {
            sprintf(char_pid, "/proc/%05d", proc_buff.p_pid);
            fd = open (char_pid, O_RDONLY);
            if (fd != -1) {
                retval = ioctl(fd, PIOCSTATUS, &prstatus);
                total_time += 100 * (prstatus.pr_cutime.tv_sec +
                                prstatus.pr_cstime.tv_sec +
                                prstatus.pr_utime.tv_sec +
                                prstatus.pr_stime.tv_sec);
                close (fd);
            }
        }
#else

	if (do_count) total_time += uptr->u_utime+uptr->u_stime
			+uptr->u_cutime+uptr->u_cstime;
#endif
	if (do_count) {
	    if (start_time == 0) start_time = uptr->u_start;
	    else if (start_time > uptr->u_start) start_time = uptr->u_start;
	}
        status = get_node(proc_buff.p_child,pid_oi);
	if (proc_buff.p_pid == pid_oi) do_count = 0;
	status = get_node(proc_buff.p_sibling, pid_oi);
}
#endif
#endif


#if	IS_IBMRS

static void summary(int pid_oi)
{

	struct	tm  *start_tm;
	int	status;
	struct proc *poffset;
	ulong   procstart;
	struct	proc   myproc;
	
	total_time = 0;
	memfd = open ("/dev/kmem", O_RDONLY);
	if (memfd == -1) {
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_delreq: Error in lseek, errno: %d\n", errno);
	    return;
	}
	locnlist.n_name = "proc";
	knlist(&locnlist, 1, sizeof(struct nlist));
	procstart = locnlist.n_value;
	status = findproc( procstart,  &myproc);
	poffset = myproc.p_child;
	do_count = 0;
	get_node(poffset, pid_oi);

}
static get_node(struct proc *pointer, int pid_oi)
{
	int	status;
	struct  user  myuser;
	struct	proc   myproc;

	if (pointer == 0) return(0);
	status = findproc(pointer,  &myproc);
	if (myproc.p_pid == pid_oi) do_count++;
	status = get_node_finduser(myproc.p_pid,  &myuser);
#if	IS_AIX4
	if (do_count) total_time += myuser.U_utime+myuser.U_stime
			+myuser.U_cutime+myuser.U_cstime;
#else
	if (do_count) total_time += myuser.u_utime+myuser.u_stime
			+myuser.u_cutime+myuser.u_cstime;
#endif
	if (do_count) {
#if	IS_AIX4
	    if (start_time == 0) start_time = myuser.U_start;
	    else if  (start_time > myuser.U_start) start_time = myuser.U_start;
#else
	    if (start_time == 0) start_time = myuser.u_start;
	    else if  (start_time > myuser.u_start) start_time = myuser.u_start;
#endif
	}
        status = get_node(myproc.p_child,pid_oi);
	if (myproc.p_pid == pid_oi) do_count = 0;
	status = get_node(myproc.p_siblings, pid_oi);
}
static findproc(ulong address,  struct proc *myproc)
{
    int	    rtn;
    ulong   offset;
    offset = address & 0x7fffffff;
    rtn = lseek(memfd,  offset,  0);
    if (rtn == -1) return (0);
    rtn = readx(memfd, (char *)myproc, sizeof(struct proc),  1);
    if (rtn == -1) return (0);
    return(1);
}
get_node_finduser(long pid,  struct user *user)
{
    struct procinfo pinfo[MAX_PROC];
    int	proc_no,  nproc;
    int status;
    
    nproc = getproc(&pinfo[0],  MAX_PROC,  sizeof(struct procinfo) );
    for (proc_no = 0; proc_no < nproc; proc_no++) {
	if (pid == pinfo[proc_no].pi_pid) break;
    }
    status = getuser(&pinfo[proc_no],  sizeof(struct procinfo),  user, 
		    sizeof(struct user) );
    return (status);
}
#endif
