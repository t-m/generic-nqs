/*
 * nqsd/nqs_failed.c
 * 
 * DESCRIPTION:
 *
 *	Move the named request control file to the NQS hold directory
 *	for failed requests.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <string.h>
#include <unistd.h>
#include <libnqs/nqsxvars.h>		/* Global vars and directories */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#if	IS_BSD
#include <sys/dir.h>			/* Include directory walking defs */
#else
#if	IS_HPUX8
#include <ndir.h>
#else
#if     IS_POSIX_1 | IS_SYSVr4 | IS_HPUX9
#include <dirent.h>
#else
BAD SYSTEM TYPE
#endif
#endif
#endif

/*** nqs_failed
 *
 *
 *	void nqs_failed():
 *
 *	Move the named request control file from the control directory
 *	to the holding directory for failed requests.
 */
void nqs_failed (long orig_seqno, Mid_t orig_mid)
{
	char ctrlname [15];		/* Name of control file in control */
					/* directory */
	char path [MAX_PATHNAME+1];	/* Control/data subdirectory path */
	char datapath [MAX_PATHNAME+1];	/* Data subdirectory path */
	char holdpath [MAX_PATHNAME+1];	/* Failed directory path */
	DIR *dir;			/* Open queue order dir structure */
#if	IS_POSIX_1 | IS_SYSVr4
	struct dirent *dirent;		/* Ptr to directory entry */
#else
	struct direct *dirent;		/* Ptr to directory entry */
#endif

	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Request being placed in failed directory.\n");
	pack6name (path, Nqs_control, (int) (orig_seqno % MAX_CTRLSUBDIRS),
		  (char *) 0, (long) orig_seqno, 5, (long) orig_mid, 6, 0, 0);
	pack6name (ctrlname, (char *) 0, -1, (char *) 0, (long) orig_seqno, 5,
		  (long) orig_mid, 6, 0, 0);
	sprintf (holdpath, "%s/%s", Nqs_failed, ctrlname);
	link (path, holdpath);		/* Establish link in failed dir */
	unlink (path);			/* Remove control subdirectory entry */
	/*
	 *  Traverse the appropriate data subdirectory to grab all data
	 *  files associated with the failed request.
	 */
	pack6name (datapath, Nqs_data, (int) (orig_seqno % MAX_DATASUBDIRS),
		  (char *) 0, 0L, 0, 0L, 0, 0, 0);
	if ((dir = opendir (datapath)) == (DIR *) 0) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to open the data dir. in nqs_failed().\nData files for failed request not moved to failed directory.\n");
		return;
	}
#if	IS_POSIX_1 | IS_SYSVr4
	while ((dirent = readdir (dir)) != (struct dirent *) 0) {
#else
	while ((dirent = readdir (dir)) != (struct direct *) 0) {
#endif
		/*
		 *  Determine if this data file is for the request being
		 *  placed on hold.
		 */
		if (strlen (dirent->d_name) == 14 &&
		    is6bitstr (dirent->d_name, 14) &&
		    strncmp (dirent->d_name, ctrlname, 11) == 0) {
			/*
			 *  This data file is for the request being placed
			 *  in the failed directory.
			 */
			sprintf (path, "%s/%s", datapath, dirent->d_name);
			sprintf (holdpath, "%s/%s", Nqs_failed,
				 dirent->d_name);
			link (path, holdpath);	/*Establish link to data file*/
			unlink (path);		/*Remove from data dir */
		}
	}
	closedir (dir);			/* Close the data file directory */
}
