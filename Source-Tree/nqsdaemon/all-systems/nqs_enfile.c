/*
 * nqsd/nqs_enfile.c
 * 
 * DESCRIPTION:
 *
 *	Determine if the last file open error operation was caused
 *	by a lack of file descriptors, and if so--display the proper
 *	error output.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <stdio.h>
#include <errno.h>
#ifdef	IS_POSIX_1
#include <string.h>
#endif

/*** nqs_enfile
 *
 *
 *	int nqs_enfile():
 *	Diagnose a shortage of file descriptors.
 *
 *	Returns:
 *		0: if errno is NOT either EMFILE or ENFILE.
 *		1: if errno is either EMFILE or ENFILE in which
 *		   case error output is generated.
 */
int nqs_enfile (void)
{
	if (errno == ENFILE || errno == EMFILE) {
		if (errno == ENFILE) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to open file.\nSystem file table full.\n");
			return (1);
		}
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to open file.\nMax open files per-process limit reached.\n");
		return (1);
	}
  	else
  	{
	  	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unknown filing failure.\nError code %d : %s\n", errno, libsal_err_strerror(errno));
	}
	return (0);		/* Nope, not ENFILE or EMFILE. */
}
