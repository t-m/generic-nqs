/*
 * nqsd/nqs_fnddnq.c
 * 
 * DESCRIPTION:
 *
 *	Return pointer to queue structure for the named non-network
 *	queue.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	January 23, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>		/* External variables */
#include <string.h>


/*** nqs_fndnnq
 *
 *
 *	struct nqsqueue *nqs_fndnnq():
 *
 *	Return pointer to queue structure for the named non-network
 *	queue.
 *
 *	Returns:
 *		A pointer to the queue structure for the named non-
 *		network queue, if the named non-network queue exists;
 *		otherwise (struct nqsqueue *)0 is returned.
 */
struct nqsqueue *nqs_fndnnq (char *quename)
{
	register struct nqsqueue *queue;
	register int res;

  	assert (quename != NULL);
  
	/*
	 *  We take advantage of the fact that the non-network queue set
	 *  is lexicographically ordered.
	 */
	queue = Nonnet_queueset;
	while (queue != (struct nqsqueue *)0) {
		if ((res = strcmp (queue->q.namev.name, quename)) < 0) {
			/*
			 *  We have not yet found the queue, but we should
			 *  keep on searching.
			 */
			queue = queue->next;
		}
		else if (res == 0) return (queue);	/* Found! */
		else return ((struct nqsqueue *)0);	/* Not found */
	}
	return (queue);					/* Not found */
}
