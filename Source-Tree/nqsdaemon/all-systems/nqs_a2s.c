/*
 * nqsd/nqs_a2s.c
 * 
 * DESCRIPTION:
 *
 *	This module contains 7 externally visible procedures which
 *	add the given request to the appropriate queue/request set:
 *
 *		a2s_a2aset():	Add to arriving set;
 *		a2s_a2dset():	Add to departing/exiting
 *				(staging-out) set.
 *		a2s_a2hset():	Add to holding set;
 *		a2s_a2qset():	Add to queued set;
 *		a2s_a2rset():	Add to running set;
 *		a2s_a2sset():	Add to staging-in set;
 *		a2s_a2wset():	Add to waiting set;
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	March 13, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>		/* NQS global vars and dirs */

static void a2s_a2eset ( struct request *req, struct nqsqueue *queue, struct request **addtoset, short *setcount );
static void a2s_a2pset ( struct request *req, struct nqsqueue *queue, struct request **addtoset, short *setcount );

/*** a2s_a2aset
 *

 *
 *	void a2s_a2aset():
 *
 *	Add the given request to the arrive set in the specified
 *	queue.  The QUE_UPDATE bit is set for the queue.
 */
void a2s_a2aset (
	struct request *req,		/* Request */
	struct nqsqueue *queue)		/* The queue */
{
	/*
	 *  In a load balanced outbound queue, if there is a waiting request, we
	 *  want to follow the order of submission. So the new request
	 *  is not queued but puts on wait.
	 */
	if (queue->waitset != (struct request *)0 &&
	    ( queue->q.status & QUE_LDB_OUT ) ) {
		/*
		 * If not really waiting, fixup the start time. 
		 */
		if ( !(req->status & RQF_AFTER) )
			req->start_time = req->v1.req.state_time + Defdesretwai;
		a2s_a2wset (req, queue);
	} else
		a2s_a2pset (req, queue, &queue->arriveset,
						&queue->q.arrivecount);
}

/*** a2s_a2hset
 *
 *
 *	void a2s_a2hset():
 *
 *	Add the given request to the hold set in the specified
 *	queue.  The QUE_UPDATE bit is set for the queue.
 */
void a2s_a2hset (
	struct request *req,		/* Request */
	struct nqsqueue *queue)		/* The queue */
{

	a2s_a2pset (req, queue, &queue->holdset, &queue->q.holdcount);
}


/*** a2s_a2qset
 *
 *
 *	void a2s_a2qset():
 *
 *	Add the given request to the queued set in the specified
 *	queue.  The QUE_UPDATE bit is set for the queue.
 */
void a2s_a2qset (
	struct request *req,		/* Request */
	struct nqsqueue *queue)		/* The queue */
{

	a2s_a2pset (req, queue, &queue->queuedset, &queue->q.queuedcount);
}


/*** a2s_a2rset
 *
 *
 *	void a2s_a2rset():
 *
 *	Add the given request to the running set in the specified
 *	queue.  The QUE_UPDATE bit is set for the queue.
 *
 *      Added Device type as per Intergraph Bill Mar    TAC
 */
void a2s_a2rset (
	struct request *req,		/* Request */
	struct nqsqueue *queue)		/* The queue */
{
        register int i;
        struct qcomplex *qcomplex;      /* Queue complex pointer */

        if (queue->q.type == QUE_BATCH) {
                for (i = MAX_COMPLXSPERQ; --i >= 0;) {
                        qcomplex = queue->v1.batch.qcomplex[i];
                        if (qcomplex == (struct qcomplex *)0) continue;
                        qcomplex->runcount++;
                }
        }
        else if (queue->q.type == QUE_DEVICE) {
                for (i = MAX_COMPLXSPERQ; --i >= 0;) {
                        qcomplex = queue->v1.device.qcomplex[i];
                        if (qcomplex == (struct qcomplex *)0) continue;
                        qcomplex->runcount++;
                }
        }
        else if (queue->q.type == QUE_PIPE) {
                for (i = MAX_COMPLXSPERQ; --i >= 0;) {
                        qcomplex = queue->v1.pipe.qcomplex[i];
                        if (qcomplex == (struct qcomplex *)0) continue;
                        qcomplex->runcount++;
                }
        }
        a2s_a2eset (req, queue, &queue->runset, &queue->q.runcount);
}

/*** a2s_a2wset
 *
 *
 *	void a2s_a2wset():
 *
 *	Add the given request to the waiting set in the specified
 *	queue.  The QUE_UPDATE bit is set for the queue.
 */
void a2s_a2wset (
	struct request *req,		/* Request */
	struct nqsqueue *queue)		/* The queue */
{
	register struct request *prevreq;
	register struct request *walkreq;
	register short pri;

	/*
	 *  Insert the request by -a time and priority.
	 */
	prevreq = (struct request *) 0;
	walkreq = queue->waitset;
	
	if ( !(queue->q.status & QUE_LDB_OUT) ) {
		while (walkreq != (struct request *)0 &&
		       walkreq->start_time < req->start_time) {
	    		prevreq = walkreq;
	    		walkreq = walkreq->next;
		}
	} else {
		/*
		 *  In a load balanced outbound queue, in order to follow the
		 *  order of submission, we have to delay the execution
		 *  of requests that have been submitted after.
		 */
		while (walkreq != (struct request *)0 &&
		       walkreq->v1.req.state_time < req->v1.req.state_time) {
	    		prevreq = walkreq;
	    		walkreq = walkreq->next;
		}
		/*
		 * Just to make sure that the req will get looked at sometime.
		 */
		if (!(req->status & RQF_AFTER) )req->start_time += Defdesretwai;	
	}
	if (walkreq != (struct request *)0) {
	    if (walkreq->start_time == req->start_time) {
		if (queue->q.type == QUE_NET) {
		    /*
		     *  The request is being placed in a network queue,
		     *  and therefore it MUST be a subrequest.  Its priority
		     *  is inherited from its parent.
		     *
		     *  Search to add the request into the -a time
		     *  set by priority.
		     */
		    pri = req->v1.subreq.parent->v1.req.priority;
		    while (walkreq != (struct request *)0 &&
			   walkreq->start_time == req->start_time &&
			   walkreq->v1.subreq.parent->v1.req.priority >= pri) {
			prevreq = walkreq;
			walkreq = walkreq->next;
		    }
		}
		else {
		    /*
		     *  The request is not being place in a network queue,
		     *  and is therefore NOT a subrequest.
		     *
		     *  Search to add the request into the -a time
		     *  set by priority.
		     */
		    pri = req->v1.req.priority;
		    while (walkreq != (struct request *)0 &&
			   walkreq->start_time == req->start_time &&
			   walkreq->v1.req.priority >= pri) {
			prevreq = walkreq;
			walkreq = walkreq->next;
		    }
		}
	    }
	}
	/*
	 *  Add to set.
	 */
	if (prevreq == (struct request *)0) {
	    req->next = queue->waitset;
	    queue->waitset = req;
	}
	else {
	    req->next = prevreq->next;
	    prevreq->next = req;
	}
	queue->q.waitcount++;		/* One more request in the wait set */
	queue->q.status |= QUE_UPDATE;	/* Database image needs to be */
					/* updated */
	req->queue = queue;		/* Each request always has a pointer */
					/* to the queue in which it resides */
	/*
	 *  Set virtual timer to wakeup request.
	 */
	nqs_vtimer (&req->start_time, nqs_wakreq);
}


/*** a2s_a2eset
 *
 *
 *	void a2s_a2eset():
 *
 *	Add the request to the specified set in the given queue.
 *	The requests in the given request set are ordered by
 *	event time (the time at which this procedure is called).
 *
 *	NOTE:	This procedure can only be used for adding requests
 *		to a request set in the DEPARTING/EXITING, RUNNING/ROUTING,
 *		or STAGING states.
 *
 */
static
void a2s_a2eset (
	struct request *req,		/* Request */
	struct nqsqueue *queue,		/* The queue */
	struct request **addtoset,	/* Ptr to the chain of requests into */
					/* into which 'req' must be inserted */
	short *setcount)		/* Ptr to short integer tallying the */
					/* number of requests in the set into*/
					/* 'req' is being inserted */
{
	register struct request *prevreq;
	register struct request *walkreq;

	/*
	 *  Insert the request by event time.
	 */
	prevreq = (struct request *) 0;
	walkreq = *addtoset;
	while (walkreq != (struct request *) 0) {
	    prevreq = walkreq;
	    walkreq = walkreq->next;
	}
	/*
	 *  Add to set.
	 */
	if (prevreq == (struct request *) 0) {
		req->next = *addtoset;
		*addtoset = req;
	}
	else {
		req->next = prevreq->next;
		prevreq->next = req;
	}
	++*setcount;			/* One more request in this set */
	req->queue = queue;		/* Each request always has a pointer */
					/* to the queue in which it resides */
	queue->q.status |= QUE_UPDATE;	/* Database image needs to be */
}					/* updated */


/*** a2s_a2pset
 *
 *
 *	void a2s_a2pset():
 *
 *	Add the request to the specified set in the given queue.
 *	The requests in the given request set are ordered by:
 *
 *		1. priority	AND
 *		2. queued-time (recorded in state_time).
 *
 *	NOTE:	This procedure can only be used for adding requests
 *		to a request set in the QUEUED, HOLDING or ARRIVING
 *		states.
 *
 */
static
void a2s_a2pset (
	struct request *req,		/* Request */
	struct nqsqueue *queue,		/* The queue */
	struct request **addtoset,	/* Ptr to the chain of requests into */
					/* into which 'req' must be inserted */
	short *setcount)		/* Ptr to short integer tallying the */
					/* number of requests in the set into*/
					/* 'req' is being inserted */
{
	register struct request *prevreq;
	register struct request *walkreq;
	register short pri;
	register time_t sta;

	/*
	 *  Insert the request by priority and queued-time (as
	 *  recorded in state_time).
	 */
	prevreq = (struct request *) 0;
	walkreq = *addtoset;
	if (queue->q.type == QUE_NET) {
	    /*
	     *  The request is being placed in a network queue,
	     *  and therefore it MUST be a subrequest.  Its priority
	     *  is inherited from its parent.
	     */
	    pri = req->v1.subreq.parent->v1.req.priority;
	    while (walkreq != (struct request *)0 &&
		   walkreq->v1.subreq.parent->v1.req.priority > pri) {
		prevreq = walkreq;
		walkreq = walkreq->next;
	    }
	    if (walkreq != (struct request *)0) {
		if (walkreq->v1.subreq.parent->v1.req.priority == pri) {
		    /*
		     *  Search to add the request into this
		     *  priority set by the last changed state
		     *  time of its parent.
		     */
		    sta = req->v1.subreq.parent->v1.req.state_time;
		    while (walkreq != (struct request *)0 &&
			   walkreq->v1.subreq.parent->v1.req.priority == pri &&
			   walkreq->v1.subreq.parent->v1.req.state_time <=sta){
			prevreq = walkreq;
			walkreq = walkreq->next;
		    }
		}
	    }
	}
	else {
	    /*
	     *  The request is NOT a subrequest.
	     */
	    pri = req->v1.req.priority;
	    while (walkreq != (struct request *)0 &&
		   walkreq->v1.req.priority > pri) {
		prevreq = walkreq;
		walkreq = walkreq->next;
	    }
	    if (walkreq != (struct request *)0) {
		if (walkreq->v1.req.priority == pri) {
		    /*
		     *  Search to add the request into this
		     *  priority set by its last changed state
		     *  time.
		     */
		    sta = req->v1.req.state_time;
		    while (walkreq != (struct request *)0 &&
			   walkreq->v1.req.priority == pri &&
			   walkreq->v1.req.state_time <= sta) {
			prevreq = walkreq;
			walkreq = walkreq->next;
		    }
		}
	    }
	}
	/*
	 *  Add to set.
	 */
	if (prevreq == (struct request *)0) {
		req->next = *addtoset;
		*addtoset = req;
	}
	else {
		req->next = prevreq->next;
		prevreq->next = req;
	}
	++*setcount;			/* One more request in this set */
	req->queue = queue;		/* Each request always has a pointer */
					/* to the queue in which it resides */
	queue->q.status |= QUE_UPDATE;	/* Database image needs to be */
}					/* updated */
