/*
 * nqsd/nqs_memdump.c
 * 
 * DESCRIPTION:
 *
 *	NQS dump internal data structures module.
 *
 *	Original Author:
 *	-------
 *	John Roman, Monsanto Company.
 *	August 24, 1992.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>
#include <libnqs/transactcc.h>		/* Transaction completion codes */
#include <time.h>
#include <string.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

static int dump_load ( void );
static int dump_req ( void );

/*** nqs_memdump
 *
 *
 *		TCML_COMPLETE:	if successful;
 */
long nqs_memdump (int dump_flag)
{
    
    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Nqs_memdump: Entering with flags %d.\n", dump_flag);
  
    switch (dump_flag) {
	case MEMDUMP_REQ:
	    dump_req();
	    break;
	case MEMDUMP_LOAD:
	    dump_load();
	    break;
	default:
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unknown dump flag %d\n",  dump_flag);
    }
    return (TCML_COMPLETE);
}
/*** dump_req
 *
 *
 *	int dump_req():
 *
 */
static int dump_req ()
{
	register struct nqsqueue *queue;	/* Queue set to traverse */
	register struct request *req;
	register short set;		/* Request set [0..6] */

	queue = Nonnet_queueset;	/* The request will always be */
					/* located in a non-network queue */
	while (queue != (struct nqsqueue *) 0) {
	    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Nqs_memdump: Queue: %s.\n",  queue->q.namev.name);
	    for (set = 0; set <= 6; set++) {
		/*
		 *  Search each
		  set for the request.
		 */
		req = (struct request *) 0;
		switch (set) {
		    case 0:
		       req = queue->departset;
		       break;
		    case 1:
		       req = queue->runset;
		       break;
		    case 2: 
			req = queue->stageset;
			break;
		    case 3:
			req = queue->queuedset;
			break;
		    case 4:
		    	req = queue->waitset;
			break;
		    case 5:
		    	req = queue->holdset;
			break;
		    case 6: 
			req = queue->arriveset;
			break;
		}
		while (req != (struct request *) 0 ) {
		    sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Request %d name is %s\n",  req->v1.req.orig_seqno, 
				req->v1.req.reqname); 
		    req = req->next;
		}
	    }
	    /*
	     *  Examine the next queue in the queue set.
	     */
	    queue = queue->next;
	}
	return ( 0);	
}
/*
 * Dump the load information data structures.
 */
static int 
dump_load(void)
{
    struct loadinfo *loadinfo_ptr;
    char  *time_ptr,  *cp;

    /*
     * Look through the list of loadinfos printing as we go.
     */
    loadinfo_ptr = Loadinfo_head.next;
    if (loadinfo_ptr == (struct loadinfo *) 0) {
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Nqs_memdump: No load data structures.\n");
	return (0);
    }
    while (loadinfo_ptr != (struct loadinfo *) 0) {
        time_ptr = ctime(&loadinfo_ptr->time);
	cp = strchr(time_ptr,  '\n');
	if (cp != NULL ) *cp = '\0';
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Nqs_memdump: Mid %u Time %s Jobs %d RAP %d\n", loadinfo_ptr->mid, 
			    time_ptr,  loadinfo_ptr->no_jobs,
			    loadinfo_ptr->rap);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "NQS_memdump: Load %f %f %f\n",  loadinfo_ptr->load1,
			    loadinfo_ptr->load5, loadinfo_ptr->load15);
        loadinfo_ptr = loadinfo_ptr->next;
    }
  return 0;
}
