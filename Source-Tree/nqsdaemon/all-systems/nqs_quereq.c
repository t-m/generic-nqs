/*
 * nqsd/nqs_quereq.c
 * 
 * DESCRIPTION:
 *
 *	Queue an NQS request for execution.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>

#include <string.h>
#include <libnqs/informcc.h>		/* NQS information completion */
					/* codes and masks */
#include <libnqs/transactcc.h>		/* NQS transaction completion codes */
#include <libnqs/nqsxvars.h>		/* NQS global vars and directories */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <unistd.h>

static int flag_upd ( struct request *req, short setflags, short clflags );
static int nqs_resolvepath ( struct rawreq *rawreq, Mid_t *mid_resolve, char *path_resolve, short access_mode, int length, char suffixchar );
static void subs_seqno ( char *cp, int seqno );

/*** nqs_quereq
 *
 *
 *	long nqs_quereq():
 *	Queue an NQS request for execution.
 *
 *	Returns:
 *		TCML_SUBMITTED: if successful in queueing the request;
 *		otherwise, a failure transaction code is returned (TCMx_).
 *		The information completion code portion of the return
 *		value is 0.
 */
long nqs_quereq (
	int mode,			/* Queueing mode:  0=new; 1=local */
					/* pipe queue; 2=remote pipe queue */
	char *ctrlname,			/* The name of the req control file */
					/* (Only meaningful if mode = 0) */
	long orig_seqno,		/* Original request sequence# */
					/* (Only meaningful if mode > 0) */
	Mid_t orig_mid,			/* Original request machine-id */
					/* (Only meaningful if mode > 0) */
	char *destqueue,		/* Destination queue */
					/* (Only meaningful if mode > 0) */
	Mid_t frommid)			/* Machine delivering request */
{					/* (Only meaningful if mode = 2) */

	char newpath [MAX_PATHNAME+1];	/* New pathname */
	char oldpath [MAX_PATHNAME+1];	/* Old pathname */
	struct rawreq rawreq;		/* Raw request structure */
	int cfd;			/* Control file file-descriptor */
	long insqueres;			/* nsq_enque() result code */
	register int i;			/* Counter */
        char *requests_dir;             /* Fully qualified file name */
	char *cp;			/* Utility character pointer */
	int iHold = 0;			/* do we want to hold the request? */
	struct request * req = NULL;
	struct request * predecessor = NULL;
	int		 state = RQS_DEPARTING | RQS_RUNNING | RQS_STAGING | RQS_QUEUED | RQS_WAITING | RQS_HOLDING | RQS_ARRIVING;

	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "nqs_quereq: Entering with seqno %d.\n", orig_seqno);

	switch(mode)
	{
	 case 0:
		/*
		 *  This request is a brand new request, with its
		 *  associated files in the new local request
		 *  directory.
		 */
                requests_dir = getfilnam (Nqs_requests, SPOOLDIR);
                if (requests_dir == (char *)NULL) return (TCML_INTERNERR);
                sprintf (newpath, "%s/%s", requests_dir, ctrlname);
                relfilnam (requests_dir);
		break;
	 case 2:
		/*
		 *  The request is being routed by a remote pipe
		 *  queue.
		 */

		/* SLH: 2000/02/11
		 *
		 * Before we queue the request, we must make sure that
		 * the request does not already exist on the system.
		 *
		 * This will prevent a race condition.
		 */

		if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,&state)) != NULL)
		{
			/* the request already exists */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "nqs_quereq: request '%lu' from mid '%lu' already exists\n", orig_seqno, orig_mid);
			return (TCML_ERRORRETRY);
		}

		/* fall through */

	default:
		/*
		 *  The request is being routed by a local pipe
		 *  queue.
		 */

		pack6name (newpath, Nqs_control,
			  (int) (orig_seqno % MAX_CTRLSUBDIRS), (char *) 0,
			  (long) orig_seqno, 5, (long) orig_mid, 6, 0, 0);
	}
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Attempting to open file %s\n", newpath);
	if ((cfd = open (newpath, O_RDWR)) == -1) {
		/*
		 *  We are unable to open the control file!
		 */
		if (nqs_enfile ()) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to accept new request because of file descriptor shortage.\n");
			return (TCML_ENFILE);
		}
		return (TCML_UNAFAILURE);
	}
	if (readreq (cfd, &rawreq) == -1) {
		/*
		 *  Request is corrupt.
		 */
		if (mode == 0) {
			rawreq.tcm = TCML_UNAFAILURE;
			writereq (cfd, &rawreq);
		}
		close (cfd);			/* Close request file */
		return (TCML_UNAFAILURE);	/* Failure */
	}
	/*
	 *  Assign sequence number to request.
	 */
	if (mode == 0) {
		/*
		 *  The request is being submitted for the first time and
		 *  does not have a sequence number or transaction-id
		 *  assigned to it.
		 */
		rawreq.trans_id = 0;		/* No arguments about this */
		time (&rawreq.create_time);
		rawreq.tcm = TCML_SUBMITTED;	/* Might overwrite this below */
		rawreq.orig_seqno = Seqno_user;	/* Seq# the request will have */
	}					/* if it queues successfully */
	else {
		/*
		 *  The request is being routed by a pipe queue.
		 *  Copy the destination queue name into the queue
		 *  field of the rawreq structure for the request.
		 */
		strcpy (rawreq.quename, destqueue);
		if (mode == 2) {
			/*
			 *  Set values out of paranoia (that should
			 *  already be set) for requests that are
			 *  being queued from a remote machine.
			 */
			rawreq.trans_id = 0;	/* No transaction-id */
			rawreq.flags |= RQF_EXTERNAL;
		}
	}
	/*
	 *  Assign queue entry time to request.
	 */
	time (&rawreq.enter_time);
	/*
	 *  Manufacture default output file pathnames for batch request
	 *  stderr, stdlog, and stdout files as necessary.
	 */
	if (rawreq.type == RTYPE_BATCH) {
		i = strlen (rawreq.v.bat.stderr_name);
		if ((rawreq.v.bat.stderr_acc & OMD_EO) == 0 &&
		    (i == 0 || rawreq.v.bat.stderr_name [i-1] == '/')) {
			/*
			 *  The stderr file name needs to be resolved.
			 *  The user did NOT specify the pathname for
			 *  the stderr file, and the stderr file is to
			 *  be placed on the execution machine, or
			 *  returned to the submitting machine as
			 *  determined by the OMD_M_KEEP modifier bit.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Stderr name needs resolution.\n");
		  
			if (nqs_resolvepath (&rawreq, &rawreq.v.bat.stderr_mid,
					 rawreq.v.bat.stderr_name,
					 rawreq.v.bat.stderr_acc, i,
					 'e') == -1) {
				/*
				 *  The resolved pathname will be too long.
				 */
				if (mode == 0) {
					rawreq.tcm = TCML_PATHLEN;
					writereq (cfd, &rawreq);
				}
				close (cfd);		/* Close request file */
				return (TCML_PATHLEN);
			}
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM,
				     "Resolved stderr name = %s.\n", rawreq.v.bat.stderr_name);
		} else {
		    /*
		     * Does the name include a hash token indicating that
		     * the request number should be substituted?
		     */
		    cp = strrchr (rawreq.v.bat.stderr_name,  '/');
		    if (cp == NULL) cp = rawreq.v.bat.stderr_name;
		    else cp++;
		    if (strchr( cp, SEQNO_TOKEN) ) {
			subs_seqno (rawreq.v.bat.stderr_name, rawreq.orig_seqno);
		    }
		}
		i = strlen (rawreq.v.bat.stdlog_name);
		if (i == 0 || rawreq.v.bat.stdlog_name [i-1] == '/') {
			/*
			 *  The stdlog file name needs to be resolved.
			 *  The user did NOT specify the pathname for
			 *  the stdlog file, and the stdlog file is to
			 *  be placed on the execution machine, or
			 *  returned to the submitting machine as
			 *  determined by the OMD_M_KEEP modifier bit.
			 */
			
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Stdlog name needs resolution.\n");
		  
			if (nqs_resolvepath (&rawreq, &rawreq.v.bat.stdlog_mid,
					 rawreq.v.bat.stdlog_name,
					 rawreq.v.bat.stdlog_acc, i,
					 'l') == -1) {
				/*
				 *  The resolved pathname will be too long.
				 */
				if (mode == 0) {
					rawreq.tcm = TCML_PATHLEN;
					writereq (cfd, &rawreq);
				}
				close (cfd);		/* Close request file */
				return (TCML_PATHLEN);
			}
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Resolved stdlog name = %s.\n", rawreq.v.bat.stdlog_name);
		} else {
		    /*
		     * Does the name include a hash token indicating that
		     * the request number should be substituted?
		     */
		    cp = strrchr (rawreq.v.bat.stdlog_name,  '/');
		    if (cp == NULL) cp = rawreq.v.bat.stdlog_name;
		    else cp++;
		    if (strchr( cp, SEQNO_TOKEN) ) {
			subs_seqno (rawreq.v.bat.stdlog_name, rawreq.orig_seqno);
		    }
		}
		i = strlen (rawreq.v.bat.stdout_name);
		if (i == 0 || rawreq.v.bat.stdout_name [i-1] == '/') {
			/*
			 *  The stdout file name needs to be resolved.
			 *  The user did NOT specify the pathname for
			 *  the stdout file, and the stdout file is to
			 *  be placed on the execution machine, or
			 *  returned to the submitting machine as
			 *  determined by the OMD_M_KEEP modifier bit.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Stdout name needs resolution.\n");
		  
			if (nqs_resolvepath (&rawreq, &rawreq.v.bat.stdout_mid,
					 rawreq.v.bat.stdout_name,
					 rawreq.v.bat.stdout_acc, i,
					 'o') == -1) {
				/*
				 *  The resolved pathname will be too long.
				 */
				if (mode == 0) {
					rawreq.tcm = TCML_PATHLEN;
					writereq (cfd, &rawreq);
				}
				close (cfd);		/* Close request file */
				return (TCML_PATHLEN);
			}
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Resolved stdout name = %s.\n", rawreq.v.bat.stdout_name);
		} else {
		    /*
		     * Does the name include a hash token indicating that
		     * the request number should be substituted?
		     */
		    cp = strrchr (rawreq.v.bat.stdout_name,  '/');
		    if (cp == NULL) cp = rawreq.v.bat.stdout_name;
		    else cp++;
		    if (strchr( cp, SEQNO_TOKEN) ) {
			subs_seqno (rawreq.v.bat.stdout_name, rawreq.orig_seqno);
		    }
		}
	}
	/* Generic NQS v3.51.0-pre5
	 * 
	 * If the job is marked to be held, make a note of it, and clear
	 * the flag.
	 */

	if (rawreq.flags & RQF_USERHOLD)
	{
		rawreq.flags &= ~RQF_USERHOLD;
		iHold = 1;
	}

	/*
	 *  The request has been assigned a sequence number, and any
	 *  unresolved output return pathnames have also been resolved.
	 *
	 *  Try to insert the request into the internal queues of NQS.
	 */
	insqueres = nsq_enque (mode, cfd, &rawreq, 0, frommid,
			       rawreq.enter_time);
	if ((insqueres & XCI_FULREA_MASK) != TCML_SUBMITTED) {
		/*
		 *  The request was NOT submitted.
		 */
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Nqs_quereq: Request not submitted, reason = %o\n", insqueres);
		if (mode == 0) {
			rawreq.tcm = insqueres;	/* Store transaction and */
						/* information completion */
						/* codes */
			writereq (cfd, &rawreq);/* Update request header */
		}
		close (cfd);			/* Close request file */
		return (insqueres);		/* Return transaction code */
	}
	/*
	 *  The request was successfully enqueued.  Make sure to increment
	 *  the next available request sequence number, if necessary.
	 */
	if (mode == 0) {
		rawreq.tcm = insqueres;	/* Store transaction and information */
					/* completion codes */
		/*
		 *  Update the next available sequence number.
		 */
		if (++Seqno_user > MAX_SEQNO_USER) Seqno_user = 0;
		udb_useqno (Seqno_user);	/* Update */
		/*
		 *  A Sequence number and transaction-id has been assigned
		 *  to the request, and the request has been successfully
		 *  enqueued.  Form links in the NQS data directory to all
		 *  data files that are a part of the request.
		 *
		 *  Note however that the control file has NOT been updated
		 *  with the assigned sequence number or transaction-id.  If
		 *  the system crashes after the links have been made, but
		 *  with the control file un-updated, then nqs_rbuild.c will
		 *  have to catch it later.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Trying to link in files.\n");
	  
		for (i=0; i < rawreq.ndatafiles; i++) {
			pack6name (newpath, Nqs_data,
				  (int) (rawreq.orig_seqno % MAX_DATASUBDIRS),
				  (char *) 0, (long) rawreq.orig_seqno, 5,
				  (long) rawreq.orig_mid, 6, i, 3);
                        requests_dir = getfilnam (Nqs_requests, SPOOLDIR);
                        if (requests_dir == (char *)NULL)
                                return (TCML_INTERNERR);
                        pack6name (oldpath, requests_dir, -1,
                                   ctrlname, 0L, 0, 0L, 0, i, 3);
                        relfilnam (requests_dir);
			if (link (oldpath, newpath) == -1) {
				/*
				 *  The link failed!
				 */
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to form link to new request data file.\nlink (%s, %s) failed;\n%s.\n", oldpath, newpath, asciierrno());
				/*
				 *  Unlink any previous links to data files
				 *  for the new request.
				 */
				nqs_delrfs (rawreq.orig_seqno,
					    rawreq.orig_mid, i);
				rawreq.tcm = TCML_UNAFAILURE;
				writereq (cfd, &rawreq);
				close (cfd);	/* Close request file */
				nsq_unque();	/* Unqueue the request */
				return (TCML_UNAFAILURE);	/* Failure */
			}
			unlink (oldpath);	/* Unlink original */
		}
		/*
		 *  Form the link to the control file for the new request.
		 */
		pack6name (newpath, Nqs_control,
			  (int) (rawreq.orig_seqno % MAX_CTRLSUBDIRS),
			  (char *) 0, (long) rawreq.orig_seqno, 5,
			  (long) rawreq.orig_mid, 6, 0, 0);
                requests_dir = getfilnam (Nqs_requests, SPOOLDIR);
                sprintf (oldpath, "%s/%s", requests_dir, ctrlname);
                relfilnam (requests_dir);
		if (link (oldpath, newpath) == -1) {
			/*
			 *  We failed.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to form link to new request control file.\nlink (%s, %s) failed;\n%s.\n", oldpath, newpath, asciierrno());
			/*
			 *  Unlink all of the data files and control files
			 *  for the new request.
			 */
			nqs_delrfs (rawreq.orig_seqno, rawreq.orig_mid,
				    rawreq.ndatafiles);
			rawreq.tcm = TCML_UNAFAILURE;
			writereq (cfd, &rawreq);
			close (cfd);		/* Close request file */
			nsq_unque();		/* Unqueue the request */
			return (TCML_UNAFAILURE);
		}				/* Failure */
		unlink (oldpath);		/* Unlink original */
	}
	/*
	 *  The request has been assigned a sequence number and a transaction
	 *  descriptor.  The request has also been queued, and all of the
	 *  control and data files have been linked in.  Rawreq.tcm holds
	 *  the transaction completion code.
	 */
	writereq (cfd, &rawreq);		/* Update request header */
	close (cfd);				/* Close request file */

	/* Generic NQS v3.51.0-pre5
	 *
	 * If the job needs to be held on submission, hold it.
	 * We've just submitted the request, so it should never fail.
	 */

	if (iHold)
		nqs_holdreq(0, rawreq.orig_seqno, rawreq.orig_mid);
		
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "nqs_quereq: Exiting with seqno %d.\n", orig_seqno);
	return (insqueres);			/* Return transaction code */
}


/*** nqs_resolvepath
 *
 *
 *	int nqs_resolvepath():
 *	Resolve batch request output file path (when necessary).
 *
 *	Returns:
 *		 0: Path resolved successfully.
 *		-1: Resolved path is too long.
 */
static int nqs_resolvepath (
	struct rawreq *rawreq,		/* Raw request structure for request */
	Mid_t *mid_resolve,		/* Machine-id requiring resolution */
	register char *path_resolve,	/* Path requiring resolution */
	short access_mode,		/* Access mode */
	register int length,		/* Len of path requiring resolution */
	char suffixchar)		/* Suffix char for default */
{
	char dirpath [MAX_REQPATH+1];	/* Output file pathname */

	if (length + 14 > MAX_REQPATH) {
		/*
		 *  The resolved pathname will be
		 *  too long.
		 */
		rawreq->tcm = TCML_PATHLEN;
		return (-1);		/* Too long */
	}
	strcpy (dirpath, path_resolve);
	if (length) dirpath [length-1] = '\0';
	/*
	 *  Manufacture proper default pathname.
	 */
	mkdefault (path_resolve, dirpath, rawreq->reqname,
		  (long) rawreq->orig_seqno, suffixchar);
	if (access_mode & OMD_M_KEEP) {
		/*
		 *  The file must be left on the execution machine.
		 */
		*mid_resolve = Locmid;
	}
	return (0);			/* Success */
}

/*** nqs_holdreq
 *
 *
 *	long nqs_holdreq()
 *
 *	Place a hold on an NQS request.
 *
 *	Returns:
 *		TCML_COMPLETE:	if the request was successfully held.
 *		TCML_NOSUCHREQ:	if the specified request does not exist
 *				on this machine.
 *		TCML_NOTREQOWN:	if the mapped user-id does not match
 *				the current mapped user-id of the
 *				request owner.
 */
long nqs_holdreq (
	uid_t mapped_uid,	/* Mapped owner user-id */
	long orig_seqno,	/* Original request sequence# */
	Mid_t orig_mid)		/* Original machine-id of req */
{
	struct request *predecessor;	/* Predecessor in req set in queue */
	int state;			/* Request queue state: RQS_ */
	register struct nqsqueue *queue;   /* Queue in which req is placed */
	register struct request *req;	/* Request struct allocated for req */

	/*
	 *  Locate the request.
	 */
	state = RQS_STAGING | RQS_QUEUED | RQS_WAITING | RQS_HOLDING |
		RQS_RUNNING;
	if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0) {
		/*
		 *  The request was not found in any of the local queues.
		 */
		return (TCML_NOSUCHREQ);
	} else if (state & RQS_RUNNING)
		return (TCML_REQRUNNING);	/* Request already running */
	if (mapped_uid && req->v1.req.uid != mapped_uid)
		/*
		 *  This request cannot be affected by the client.
		 */
		return (TCML_NOTREQOWN);
	queue = req->queue;
	switch (state) {
	case RQS_QUEUED:
		/*
		 *  The request is queued, so remove the request from the
		 *  queued set.
		 */
		if (predecessor == (struct request *) 0)
			queue->queuedset = req->next;
		else
			predecessor->next = req->next;
		queue->q.queuedcount--;		/* One less request queued */
		break;
	case RQS_WAITING:
		/*
		 *  The request is waiting to be started, so remove the request
		 *  from the waiting set.
		 */
		if (predecessor == (struct request *) 0)
			queue->waitset = req->next;
		else
			predecessor->next = req->next;
		queue->q.waitcount--;		/* One less request waiting */
		break;
	case RQS_HOLDING:
		/*
		 *  This request is already held.
		 */
		return (TCML_COMPLETE);
	}
	/*
	 *  Add the request to the holding set.
	 */
	a2s_a2hset (req, queue);
	req->status |= RQF_OPERHOLD;		/* Set Flag */
	/*
	 *  Set flags to indicate that the operator is holding this request.
	 */
	if (flag_upd (req, RQF_OPERHOLD, 0) != 0)
		return (TCML_UNAFAILURE);
	/*
	 *  The NQS database queue image needs to be updated.
	 */
	udb_qorder (queue);
	return (TCML_COMPLETE);
}


/*** nqs_relreq
 *
 *
 *	long nqs_relreq()
 *
 *	Remove a hold on an queued NQS request.
 *
 *	Returns:
 *		TCML_COMPLETE:	if the request was successfully released.
 *		TCML_NOSUCHREQ:	if the specified request does not exist
 *				on this machine.
 *		TCML_NOTREQOWN:	if the mapped user-id does not match
 *				the current mapped user-id of the
 *				request owner.
 *		TCML_REQNOTHELD:if the specified request is not held.
 */
long nqs_relreq (
	uid_t mapped_uid,	/* Mapped owner user-id */
	long orig_seqno,	/* Original request sequence# */
	Mid_t orig_mid)		/* Original machine-id of req */
{
	struct request *predecessor;	/* Predecessor in req set in queue */
	int state;			/* Request queue state: RQS_ */
	register struct nqsqueue *queue;   /* Queue in which req is placed */
	register struct request *req;	/* Request struct allocated for req */
	/*
	 *  Locate the request.
	 */
	state = RQS_STAGING | RQS_QUEUED | RQS_WAITING | RQS_HOLDING |
		RQS_RUNNING;
	if ((req = nqs_fndreq (orig_seqno, orig_mid, &predecessor,
			       &state)) == (struct request *) 0)
		/*
		 *  The request was not found in any of the local queues.
		 */
		return (TCML_NOSUCHREQ);
	if (mapped_uid && req->v1.req.uid != mapped_uid)
		/*
		 *  This request cannot be affected by the client.
		 */
		return (TCML_NOTREQOWN);
	if (!(state & RQS_HOLDING))
		/*
		 *  This request is not on hold.
		 */
		return (TCML_REQNOTHELD);
	queue = req->queue;
	/*
	 *  Remove the request to the holding set.
	 */
	if (predecessor == (struct request *) 0)
		queue->holdset = req->next;
	else
		predecessor->next = req->next;
	queue->q.holdcount--;			/* One less request held */
	req->status &= ~RQF_OPERHOLD;		/* Clear Flag */
	/*
	 *  Clear RQF_OPERHOLD.
	 */
	if (flag_upd (req, 0, RQF_OPERHOLD) != 0)
		return (TCML_UNAFAILURE);
	if (req->start_time > time ((time_t *) 0)) {
		/*
		 *  The request has a future start time, so add the request to
		 *  the waiting set.
		 */
		a2s_a2wset (req, queue);
	} else {
		/*
		 *  Add the request to the queued set.
		 */
		a2s_a2qset (req, queue);
		/*
		 *  Maybe spawn a request.
		 */
		switch (queue->q.type) {
		case QUE_BATCH:
			bsc_spawn ();
			break;
		case QUE_DEVICE:
			dsc_spawn ();
			break;
		case QUE_PIPE:
			psc_spawn ();
			break;
		}
	}
	/*
	 *  The NQS database queue image needs to be updated.
	 */
	udb_qorder (queue);
	return (TCML_COMPLETE);
}


/***    flag_upd
 *
 *
 *	int flag_upd()
 *
 *	Update the flags field in a raw request in a control file
 *
 *	Returns:
 *		 0:	if successful.
 *		-1:	if failure.
 */
static int flag_upd (
	struct request *req,		/* Ptr to request structure */
	short setflags,			/* Flags to set */
	short clflags)			/* Flags to clear */
{
	int cfd;			/* File descriptor for control file */
	struct rawreq rawreq;		/* Raw request structure */

	/*
	 *   Locate and read raw request header.
	 */
	if ((cfd = gethdr (req->v1.req.orig_seqno, req->v1.req.orig_mid,
	     &rawreq)) == -1 ) {
		printf ("E$flag_upd(): Read of request header failed.");
		fflush (stdout);
		return (-1);
	}
	/*
	 *  Update flags.
	 */
	rawreq.flags |= setflags;
	rawreq.flags &= ~clflags;
	/*
	 *  Rewrite raw request to control file.
	 */
	if (writehdr (cfd, &rawreq) == -1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "flag_upd(): Write of request header failed.");
		fflush (stdout);
		close (cfd);
		return (-1);
	}
	close (cfd);
	return (0);
}
/*
 * This routine is used to take the file name and substitue tokens with
 * the ascii sequence number.
 */
static
void subs_seqno (char *name, int seqno)
{
    char *cp, *acp, *dcp;		/* Utility character pointers */
    char dirpath [MAX_REQPATH+1];	/* Output file pathname */
    char ascii_seqno[16];		/* Ascii form of seqno */
    
    sprintf(ascii_seqno, "%d", seqno);
    strcpy (dirpath, name);
    dcp = name;
    acp = strrchr (dirpath, '/');
    if (acp != NULL) {
	dcp = strrchr (name,  '/');
	dcp++; acp++;
    } else acp = dirpath;
    while (*acp) {
	if (*acp == SEQNO_TOKEN) {
	    acp++;			/* Skip token */	  
	    cp = ascii_seqno;		/* And put in ascii sequence number */
	    while (*cp) {
		*dcp++ = *cp++;
	    }
	} else *dcp++ = *acp++;
    }
    *dcp = '\0';
}
