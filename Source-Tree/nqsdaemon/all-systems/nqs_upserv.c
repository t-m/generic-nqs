/*
 * nqsd/upserv.c
 * 
 * DESCRIPTION:
 *
 *	NQS compute server set update module.
 *
 *	Original Author:
 *	-------
 *	John Roman, Monsanto Company.
 *	April 8, 1994.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>
#include <libnqs/transactcc.h>		/* Transaction completion codes */


/*** upserv_setperf
 *
 *
 *	long upserv_setperf():
 *	Set the performance level for a compute server.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful;
 */
long upserv_setperf (Mid_t server, int performance)
{
	udb_setservperf (server, performance);
	return (TCML_COMPLETE);		/* and we are done */
}


/*** upserv_setavail
 *
 *
 *	long upserv_setavail():
 *	Set a server available.
 *
 *	Returns:
 *		TCML_COMPLETE:	if successful;
 */
long upserv_setavail (Mid_t server)
{
	udb_setservavail (server);
	return (TCML_COMPLETE);		/* and we are done */
}
