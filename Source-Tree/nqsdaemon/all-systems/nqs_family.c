/*
 * nqsd/nqs_family.c
 * 
 * DESCRIPTION:
 *
 *	Process a "report process family" packet from a shepherd process.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libnqs/nqsxvars.h>			/* NQS global variables */

#include <unistd.h>
#include <signal.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

/*** nqs_family
 *
 *
 *	void nqs_family():
 *	Process a "report process family" packet from a shepherd process.
 */
void nqs_family (
	int shepherd_pid,	/* Process-id of shepherd process */
	int process_family)	/* "Process family" of server */
{
	struct nqsqueue *serverque;	/* Server was handling this queue */
	struct nqsqueue *queue;		/* Ptr to queue structure */
	int runcount;			/* Reqs running in a queue */
	struct request *req = NULL;		/* Request structure */
	short reqindex = -1;			/* Index in Runvars of the running */
					/* structure allocated for the */
					/* request under inspection */

	queue = Nonnet_queueset;		/* First queue in queue set */
	serverque = (struct nqsqueue *)0;	/* Request queue not found */
	while (queue != (struct nqsqueue *)0 && serverque == (struct nqsqueue *)0) {
		runcount = queue->q.runcount;	/* # of requests running */
		req = queue->runset;		/* First run request */
		while (runcount--) {
			reqindex = req->reqindex;
			if ( (Runvars+reqindex)->shepherd_pid == shepherd_pid) {
				/*
				 *  We have found the request!
				 */
				serverque = queue;
				runcount = 0;	/* Exit inner loop */
			}
			else req = req->next;
		}
		queue = queue->next;		/* Get next queue */
	}
	if (serverque == (struct nqsqueue *) 0) {
		/*
		 *  The request was not located in the non-network
		 *  queue set.  Scan the network queue set for the
		 *  request.
		 */
		queue = Net_queueset;		/* First queue in queue set */
		serverque = (struct nqsqueue *)0;	/* Request queue not found */
		while (queue != (struct nqsqueue *) 0 &&
		       serverque == (struct nqsqueue *) 0) {
			runcount = queue->q.runcount;
						/* # of requests running */
			req = queue->runset;	/* First run request */
			while (runcount--) {
				reqindex = req->reqindex;
				if ( (Runvars+reqindex)->shepherd_pid
					== shepherd_pid) {
					/*
					 *  We have found the request!
					 */
					serverque = queue;
					runcount = 0;
						/* Exit inner loop */
				}
				else req = req->next;
			}
			queue = queue->next;	/* Get next queue */
		}
	}
	/*
	 *  Determine whether or not we found the request, and if it's
	 *  valid.
	 */
	if (serverque == (struct nqsqueue *) 0 ||	/* Request not found */
	    (Runvars+reqindex)->process_family != 0 || /*Family known already!*/
	    process_family <= 0) {
		/*
		 *  Bad family reported.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Bad process family reported.\nprocess family = %d\nknown family =%d\n", process_family, (Runvars+reqindex)->process_family);
		if (serverque == (struct nqsqueue *) 0) sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "req not found\n");
		return;
	}
	/*
	 *  Req points to the request structure for the request whose
	 *  "process family" is being reported.
	 *
	 *  Serverque points to the queue structure for the queue
	 *  containing the request.
	 */
	if (req->status & RQF_SIGQUEUED) {
		/*
		 *  We have a queued "kill -<signal> request".
		 *  Kill the server process group/family.
		 */
		kill (-process_family, (Runvars+reqindex)->queued_signal);
		req->status &= ~RQF_SIGQUEUED;	/* Queued signal sent */
	}
	else {
		(Runvars+reqindex)->process_family = process_family;
						/* Store process-family */
		udb_reqpgrp (serverque, req);	/* Update the proper qentry */
	}					/* to show process group */
}



