/*
 * nqsd/nqs_checkbal.c
 * 
 * DESCRIPTION
 * 
 *     hook to check user accounting information.
 *     returning a negative value will set the job's priority
 *     to zero (ie "goodwill" job).
 * 
 * 	Original Author
 * 	===============
 * 	Dave Safford (saff@tamu.edu)
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <sys/types.h>

int nqs_checkbal(uid_t uid)
{
     return (0);
}
