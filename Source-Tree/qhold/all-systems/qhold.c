/*
 * qhold/qhold.c
 * 
 * DESCRIPTION:
 *
 *	Hold a queued or waiting request.
 *
 *	Original Author:
 *	-------
 *	Christian Boissat, CERN, Geneva, Switzerland.
 *	January 6, 1992.
 */

#define	MAX_REQS	100		/* Maximum number of reqs that */
					/* can be held at a time. */
#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <signal.h>			/* Signal definitions */
#include <netdb.h>			/* Network database header file; */
#include <libnqs/nqsdirs.h>		/* NQS files and directories */
#include <libnqs/transactcc.h>		/* Transaction completion codes */
#include <unistd.h>

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <SETUP/autoconf.h>

static void qhold_cleanup ( int sig );
static void qhold_diaghold ( long code, char *reqid );
static void qhold_showhow ( void );
static void show_version ( void );

/*
 *	Global variables:
 */
char *Qhold_prefix = "Qhold";

/*** main
 *
 *
 *	qhold [ -u <username> ] [-v] <request-id(s)>
 */
int main (int argc, char *argv[])
{

	int n_reqs;			/* Number of reqs to hold. */
	struct {
		long orig_seqno;	/* Sequence# for req */
		Mid_t machine_id;	/* Machine-id of originating machine */
		Mid_t target_mid;       /* target mid */
	} reqs [MAX_REQS];		/* Reqs to hold */
	struct passwd *passwd;		/* Password structure ptr */
	char **scan_reqs;		/* Scan reqs */
	char *argument;			/* Ptr to cmd line arg text */
	uid_t real_uid = -1;			/* Real user-id */
	Mid_t local_mid;		/* local machine-id */
	char *root_dir;                 /* Fully qualified file name */

  	sal_debug_InteractiveInit(1, NULL);
  
	/*
	 *  Catch 4 common household signals:  SIGINT, SIGQUIT,SIGHUP, 
	 *  and SIGTERM.  This is quite important, because we do not want
	 *  to leave useless inter-process communication files hanging
	 *  around in the NQS directory hierarchy.  However, if we do,
	 *  it is not fatal, it will eventually be cleaned up by NQS.
	 */
	signal (SIGINT, qhold_cleanup);
	signal (SIGQUIT, qhold_cleanup);
	signal (SIGHUP, qhold_cleanup);
	signal (SIGTERM, qhold_cleanup);
	if ( ! buildenv()) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qhold_prefix);
	    fprintf (stderr, "establish directory independent environment.\n");
	    exit (1);
	}
	root_dir = getfilnam (Nqs_root, SPOOLDIR);
	if (root_dir == (char *)NULL) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qhold_prefix);
	    fprintf (stderr, "determine root directory name.\n");
	    exit (1);
	}
	if (chdir (root_dir) == -1) {
	    fprintf (stderr, "%s(FATAL): Unable to chdir() to the NQS ",
                 Qhold_prefix);
	    fprintf (stderr, "root directory.\n");
	    relfilnam (root_dir);
	    exit (1);
	}
	relfilnam (root_dir);
	/*
	 *  On systems with named pipes, we get a pipe to the local
	 *  daemon automatically the first time we call inter().
	 */
#if	HAS_BSD_PIPE
	if (interconn () < 0) {
		fprintf (stderr, "%s(FATAL): Unable to get ", Qhold_prefix);
		fprintf (stderr, "a pipe to the local daemon.\n");
		exit (1);
	}
#endif
	passwd = NULL;			/* No -u flag seen */
	while (*++argv != NULL && **argv == '-') {
		argument = *argv;
		switch (*++argument) {
		    case'u':	     /* User-name specification */
			if (*++argv == NULL) {
				fprintf (stderr, "Missing username.\n");
				exit (1);
			}
			if (passwd != NULL) {
				fprintf (stderr,
					"Multiple -u specifications.\n");
				exit (1);
			}
			if ((passwd = sal_fetchpwnam (*argv)) == NULL) {
				fprintf (stderr, "No such user on this ");
				fprintf (stderr, "machine.\n");
				exit (1);
			}
			if (localmid (&local_mid) != 0) {
				fprintf (stderr, "%s(FATAL): ", Qhold_prefix);
				fprintf (stderr, "Unable to get machine-id");
				fprintf (stderr, "of local host.\n");
				exit(1);
			}
			if ((nqspriv (getuid(), local_mid, local_mid)
				& QMGR_OPER_PRIV) ||
			    passwd->pw_uid == getuid()) {
				/*
				 *  We have NQS operator privileges, or we
				 *  are just going after our own requests.
				 */
				real_uid = passwd->pw_uid;
			}
			else {
				fprintf (stderr, "Insufficient privilege ");
				fprintf (stderr, "for -u specification.\n");
				exit (1);
			}
			break;
		    case 'v':
			show_version();
			break;
		    default:
			fprintf (stderr, "Invalid option flag specified.\n");
			qhold_showhow();
			exit(1);
		}
	}
	if (passwd == NULL) {
		/*
		 *  No username specified.  We assume the invoker.
		 */
		real_uid = getuid();		/* Get real user-id */
	}
	else sal_closepwdb();		/* Close account/password database */
	/*
	 *  Build the set of reqs to be held.
	 */
	if (*argv == NULL) {
		/*
		 *  No request-ids were specified.
		 */
		fprintf (stderr, "No request-id(s) specified.\n");
		qhold_showhow();
	}
	else {
		n_reqs = 0;			/* #of reqs to hold */
		scan_reqs = argv;		/* Set req scan pointer */
		while (*scan_reqs != NULL &&	/* Loop to hold reqs */
		       n_reqs < MAX_REQS) {
			switch (reqspec (*scan_reqs, &reqs [n_reqs].orig_seqno,
					 &reqs [n_reqs].machine_id,
					 &reqs [n_reqs].target_mid)) {
			case -1:
				fprintf (stderr, "Invalid request-id syntax ");
				fprintf (stderr, "for request-id: %s.\n",
					*scan_reqs);
				exit (1);
			case -2:
				fprintf (stderr, "Unknown machine for");
				fprintf (stderr, "request-id: %s.\n",
					*scan_reqs);
				exit (1);
			case -3:
				fprintf (stderr, "Network mapping database ");
				fprintf (stderr, "inaccessible.  Seek staff ");
				fprintf (stderr, "support.\n");
				exit (1);
			case -4:
				fprintf (stderr, "Network mapping database ");
				fprintf (stderr, "error when parsing ");
				fprintf (stderr, "request-id: %s.\n",
					*scan_reqs);
				fprintf (stderr, "Seek staff support.\n");
				exit (1);
			}
			/* If reqspec returns null in machine id, force to
			 * local machine id.
			 */
			if (reqs[n_reqs].machine_id == 0) 
				localmid(&reqs [n_reqs].machine_id);
			scan_reqs++;		/* One more req */
			n_reqs++;
		}
		if (*scan_reqs != NULL) {
			/*
			 *  Too many reqs were specified to be held.
			 */
			fprintf (stderr, "Too many requests given to ");
			fprintf (stderr, "hold.\n");
			exit (1);
		}
		/*
		 *  Now that everything has been parsed and legitimized,
		 *  hold the specified set of requests.
		 */
		n_reqs = 0;
		while (*argv != NULL) {		/* Loop to hold reqs */
			qhold_diaghold (hdlreq (0, real_uid, 
				 reqs [n_reqs].orig_seqno,
				 reqs [n_reqs].machine_id), *argv);
			argv++;			/* One more req */
			n_reqs++;
		}
	}
	exiting();			/* Delete our comm. file */
	exit (0);
}


/*** qhold_cleanup
 *
 *
 *	Catch certain signals, and delete the inter-process
 *	communication file we have been using.
 */
static void qhold_cleanup (int sig)
{
	signal (sig, SIG_IGN);		/* Ignore multiple signals */
	exiting();			/* Delete our comm. file */
}


/*** qhold_diaghold
 *
 *
 *	qhold_diaghold():
 *	Diagnose hdlreq() completion code.
 */
static void qhold_diaghold (long code, char *reqid)
{
	switch (code) {
	case TCML_INTERNERR:
		printf ("Internal error.\n");
		exiting();		/* Delete communication file */
		exit (1);
	case TCML_NOESTABLSH:
		printf ("Unable to establish inter-process communications ");
		printf ("with NQS daemon.\n");
		printf ("Seek staff support.\n");
		exiting();		/* Delete communication file */
		exit (1);
	case TCML_NOLOCALDAE:
		printf ("The NQS daemon is not running.\n");
		printf ("Seek staff support.\n");
		exiting();		/* Delete communication file */
		exit (1);
	case TCML_NOSUCHREQ:
		printf ("Request %s does not exist.\n", reqid);
		break;
	case TCML_NOTREQOWN:
		printf ("Not owner of request %s.\n", reqid);
		break;
	case TCML_PEERDEPART:
		printf ("Request %s is presently being routed by a ", reqid);
		printf ("pipe queue,\n");
		printf ("and this NQS implementation does not support the ");
		printf ("handling of a\n");
		printf ("request under such conditions.\n");
		break;
	case TCML_PROTOFAIL:
		printf ("Protocol failure in inter-process communications ");
		printf ("with NQS daemon.\n");
		printf ("Seek staff support.\n");
		exiting();		/* Delete communication file */
		exit (1);
	case TCML_COMPLETE:
		printf ("Request %s has been held.\n", reqid);
		break;
	case TCML_REQRUNNING:
		printf ("Request %s is running.\n", reqid);
		break;
	default:
		printf ("Unexpected completion code from NQS daemon.\n");
		exiting();		/* Delete communication file */
		exit (1);
	}
}


/*** qhold_showhow
 *
 *
 *	qhold_showhow():
 *	Show how to use this command.
 */
static void qhold_showhow()
{
	fprintf (stderr, "qhold -- hold queued NQS requests\n");
	fprintf (stderr, "usage:    qhold [ -u <username> ] [-v] <request-id(s)>\n");
	fprintf (stderr, " -u username   username of request identifier (if not yourself)\n");
	fprintf (stderr, " -v            print version information\n");
	fprintf (stderr, " <request-id>  NQS request identifier\n");	
	exit (0);
}

static void show_version()
{
	fprintf (stderr, "NQS version is %s.\n", NQS_VERSION);
}
