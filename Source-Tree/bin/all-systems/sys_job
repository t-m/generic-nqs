#!/bin/sh
#
# sys_job	Perform prologue/epilogue processing for Generic NQS
#		This script runs as root
#
#		All output from this script will be written to the stdout
#		and stderr staging files for the current batch job
#
# Here is what we know ...
#
#	DEBUG
#		Current GNQS debugging level
#
#	ENVIRONMENT
#		Always set to BATCH
#
#	HOME
#		Current user's home directory
#
#
#	Nqs_nmapdir
#	Nqs_libdir
#	Nqs_spooldir
#
#	QSUB_GID
#		GID of the owner of the current batch job
#
#	QSUB_HOST
#		Name of the current host
#
#	QSUB_QUEUE
#		Name of the current batch queue
#
#	QSUB_REQID
#		Request ID of the current batch job
#
#	QSUB_REQNAME
#		Name of the current batch job
#
#	QSUB_SHELL
#		Shell to use to execute the current batch job
#	
#	QSUB_UID
#		UID of the owner of the current batch job
#
#	QSUB_USER
#		Name of the owner of the current batch job
#
#	QSUB_WORKING
#		Directory the job was submitted in?
#		Don't rely on this until thoroughly tested!

# ///////////////////////////////////////////////////////////////////////////
#	PROLOGUE SECTION
# ///////////////////////////////////////////////////////////////////////////

# we need to redirect stdout and stderr before we do anything else!
# If we cannot create the file, we abort.
# If root cannot exec into the file, we remove the file and let user_job
#  handle the file creation.
if [ "$NQS_STDOUT+" != "+" ]; then
	$NQS_LIBEXE/nqsmkfile $QSUB_UID $QSUB_GID mk "$NQS_STDOUT"

	case $? in
		0)	;;
		*)	exit 254 ;;
	esac
	if [ "$NQS_STDERR+" = "$NQS_STDOUT+" ]; then
	    exec > $NQS_STDOUT 2>&1

	    case $? in
		0)      ;;
		*)      $NQS_LIBEXE/nqsmkfile $QSUB_UID $QSUB_GID rm "$NQS_STDOUT"
	    esac
	else
	    exec > $NQS_STDOUT

	    case $? in
		0)      ;;
		*)      $NQS_LIBEXE/nqsmkfile $QSUB_UID $QSUB_GID rm "$NQS_STDOUT"
	    esac
	fi
fi

if [ "$NQS_STDERR+" != "+" -a "$NQS_STDERR+" != "$NQS_STDOUT+" ]; then
	$NQS_LIBEXE/nqsmkfile $QSUB_UID $QSUB_GID mk "$NQS_STDERR"

	case $? in
		0)	;;
		*)	exit 253 ;;
	esac
	exec 2> $NQS_STDERR

	case $? in
	    0)      ;;
	    *)      $NQS_LIBEXE/nqsmkfile $QSUB_UID $QSUB_GID rm "$NQS_STDERR"
	esac
fi

# place your local modifications here

# ///////////////////////////////////////////////////////////////////////////
#	RUN CURRENT BATCH JOB SECTION
#
#	This section is supplied with Generic NQS - do not play with
#	unless you know what you are doing
#
#	WARNING :- your user's batch job will not execute if you break
#		   this section of the script
# ///////////////////////////////////////////////////////////////////////////

$NQS_LIBEXE/nqsexejob

# ///////////////////////////////////////////////////////////////////////////
#	EPILOGUE SECTION
# ///////////////////////////////////////////////////////////////////////////

# place your local modifications here
