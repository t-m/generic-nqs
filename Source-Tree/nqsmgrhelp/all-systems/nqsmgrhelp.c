/*
 * utility/nqsmgrhelp.c
 * 
 * DESCRIPTION:
 *
 *	Construct Qmgr help include constant file:
 *
 *		../h/nqsmgrhelp.h
 *
 *	WARNING:
 *		This program must be executed with the current
 *		working directory of: "../proto".
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	September 9, 1985.
 */

#include <stdio.h>
#include <sys/types.h>			/* Get time_t definition */
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include <libsal/license.h>
#include <libsal/libsal.h>

/*** main
 *
 *
 *	int main():
 */
int main (void)
{
	static char temp[] = "XQMGR_TEMP.H";
					/* Temporary version of nqsmgrhelp.h */
	FILE *helpfile;			/* NQS qmgr help file */
	FILE *includefile;		/* Nqsmgrhelp.h include file under */
					/* construction */
	int fd;				/* Integer file descriptor */
	long offset;			/* Help file offset */
	register short prevch;		/* Previous character */
	register short ch;		/* Current character */

	if ((fd = creat (temp, 0444)) == -1) {
		/*
		 *  Unable to open temporary file.
		 */
		printf ("Unable to open temp file: %s.\n", temp);
		printf ("Reason: %s.\n", libsal_err_strerror(errno));
		exit (1);
	}
	includefile = fdopen (fd, "w");
	if ((helpfile = fopen ("../qmgr/qmgr.hlp", "r")) == NULL) {
		/*
		 *  Unable to open source help file.
		 */
		printf ("Unable to open qmgr help file: ../qmgr/qmgr.hlp.\n");
		printf ("Reason: %s.\n", libsal_err_strerror (errno));
		exit (2);
	}
	fputs ("/*++ nqsmgrhelp.h - Network Queueing System\n", includefile);
	fputs (" *\n", includefile);
	fputs (" * $Sou", includefile);	/* Break-up $ Source$ so RCS doesn't */
	fputs ("rce$\n", includefile);	/* do us an unwanted favor. */
	fputs (" *\n", includefile);
	fputs (" * DESCRIPTION:\n", includefile);
	fputs (" *\n", includefile);
	fputs (" *	NQS qmgr help file offset definitions file.\n",
		includefile);
	fputs (" *\n", includefile);
 	fputs (" *	Author:\n", includefile);
	fputs (" *	-------\n", includefile);
	fputs (" *	Brent A. Kingsbury, Sterling Software Incorporated.\n",
		includefile);
	fputs (" *\n", includefile);
	fputs (" * STANDARDS VIOLATIONS:\n", includefile);
	fputs (" *	None.\n", includefile);
	fputs (" *\n", includefile);
	fputs (" * REVISION HISTORY: ($Revi", includefile);	/* We break */
	fputs ("sion$ $Da", includefile);	/* Break-up the RCS strings: */
	fputs ("te$ $Sta", includefile);	/* $ Revision$, $ Date$, */
	fputs ("te$)\n", includefile);		/* $ State$, and $ Log$ to */
	fputs (" * $Lo", includefile);		/* prevent RCS wrecking */
	fputs ("g$\n", includefile);		/* this program during */
	fputs (" */\n", includefile);		/* checkout. */
	fputs ("\n", includefile);
	/*
	 *  The header for the nqsmgrhelp file has now been written.
	 *  Scan the help source file to generate the proper #define
	 *  constants.
	 */
	offset = 0;
	prevch = '\n';
	while ((ch = getc (helpfile)) != EOF) {
		offset++;			/* One more character */
		if (ch == '#' && prevch == '\n') {
			/*
			 *  A '#' has been found at the beginning of a line
			 *  in the help file.  This marks the beginning of a
			 *  help page.
			 */
			fputs ("#define	", includefile);
			while ((ch = getc (helpfile)) != EOF && ch != '\n') {
				offset++;	/* One more character */
				/*
				 *  Scan the help page name.
				 */
				putc (ch, includefile);
			}
			offset++;		/* Newline character read */
			fprintf (includefile, "\t%1ldL\n", offset);
		}
		prevch = ch;			/* Remember prev char */
	}
	/*
	 *  We are done generating the #include file for the NQS qmgr
	 *  help file offsets.  Unlink the old nqsmgrhelp.h file and
	 *  link in the new version.
	 */
	fclose (includefile);
	fclose (helpfile);
	unlink ("nqsmgrhelp.h");
	if (link (temp, "nqsmgrhelp.h") == -1) {
		/*
		 *  Unable to establish link to new version of the
		 *  #include NQS qmgr help file offsets file.
		 */
		printf ("Nqsmgrhelp.h #include file update failed.\n");
		printf ("Reason: %s.\n", libsal_err_strerror(errno));
		exit (3);
	}
	unlink (temp);
	/*
	 *  Return successful completion to the caller.
	 */
	exit (0);
}
