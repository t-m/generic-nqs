/*
 * finish_autoinst/finish_autoinst.c
 * 
 * DESCRIPTION:
 *
 *  This program completes the auto-install process.  It does the following:
 *	1. executes the script NQS_STAGE_LOC/install_nqs to move the
 *	   various files to the appropriate locations.
 *	2. It starts NQS back up again.
 *	3. (Optional) It sends a mail message to the desired address.
 *
 *	This program MUST run as setuid to root program, in order
 *	to signal the NQS shepherd process running as root.
 *
 *	Original Author:
 *	-------
 *	John Roman, Monsanto Company.
 *	July 21,  1993.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdlib.h>
#include <string.h>
#include <SETUP/General.h>

/*** main
 *
 *
 *	Invocation is as follows:
 *
 *	  $(NQS_LIBEXE)/finish_autoinst  $(NQS_LIBEXE)
 *
 */
int main (int argc, char *argv[])
{

    char buffer[512];
    
    /*
     * System the install_nqs script.
     */
    strcpy (buffer, NQS_STAGE_LOC);
    strcat (buffer, "/install_nqs");
    
    system (buffer);
    
    /*
     * Startup NQS...
     */
     
    sprintf(buffer, "%snqsdaemon > /dev/null &", argv[1]);

    system (buffer);

    /*
     * Currently commented out -- modify if desired...
     *
     * sprintf(buffer, "Mail -s \"NQS automatically installed\" root </dev/null");
     *
     * system (buffer);
     */
  
  return 0;
}
