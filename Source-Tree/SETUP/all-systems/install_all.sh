# -*- sh -*-
#
# Script to install Generic NQS onto a single node or a cluster server

umask 022
. ./General.conf.def
. ./SETUP.def

. ./install.xxx

mknqsdirs
mknqsspooldir
copynqsfiles ../bin
copynqsmanpages
mkspoolfiles
