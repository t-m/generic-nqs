#! /bin/ksh
#
# config - interactively configure a file
#
# Version 1.5
# 
# Stuart Herbert
# Academic Computing Services
# University of Sheffield
#
# Copyright (c) 1994.  All rights reserved
#
############################################################################
#
# Functions for use in config files
#
# Boolean <prompt> <variable> <y|n>
# Comment <comment>
# Define  <variable> <value>
# Integer <prompt> <variable> <default value>
# String  <prompt> <variable> <default value>
#
############################################################################
# Boolean() - select yes or no
# $1 - prompt for the user
# $2 - define to set
# $3 - boolean value

Boolean () {
  sm_last="Boolean"
  
  GetString "$1 [y/n]" "Boolean_x" "$3" "$2"
  if [ "$Boolean_x" = "Y" ]; then
    Boolean_x="y"
  elif [ "$Boolean_x" != "y" ]; then
    Boolean_x="n"
  fi
  
  case "$Boolean_x" in
    'y')
      Define "$2" "y" "1"
      ;;
    'n')
      Undefine "$2"
      ;;
    *)
      echo "PANIC: This line should not be reached!!!"
      exit 1
      ;;
  esac
  UpdateConfig "\"$1\" $2 $3" "\"$1\" $2 $Boolean_x"
}

############################################################################
# CleanSlate - delete all existing generated files
# $1 - part we're configuring

CleanSlate () {
  if [ "$CONFIG_RO" = "y" ]; then
    return
  fi
  
  for CleanSlate_x in $szOutputHeader ; do
    if [ -f $CleanSlate_x ]; then
      rm -f $CleanSlate_x
    fi
  done
  
  if [ -n "$szOutputHeader" ]; then
    echo "#ifndef __${szConfigHeader}_CONFIG_HEADER"	>  $szOutputHeader
    echo "#define __${szConfigHeader}_CONFIG_HEADER"	>> $szOutputHeader
    echo						>> $szOutputHeader
    echo "/*"						>> $szOutputHeader
    echo " * This is an automatically-generated file,"	>> $szOutputHeader
    echo " * which is included by the C source code."	>> $szOutputHeader
    echo " *"						>> $szOutputHeader
    echo " * Edit at your own risk."			>> $szOutputHeader
    echo " */"						>> $szOutputHeader
    echo						>> $szOutputHeader
  fi
}

############################################################################
# CleanStateDef() - start a clean definitions file
# no parameters

CleanStateDef () {
  if [ "$CONFIG_RO" = "y" ]; then
    return
  fi
  
  echo "#"						>  $szConfig.def
  echo "# This is an automatically-generated file,"	>> $szConfig.def
  echo "# which is included by config,sh and make"	>> $szConfig.def
  echo "#"						>> $szConfig.def
  echo "# Edit at your own risk."			>> $szConfig.def
  echo "#"						>> $szConfig.def
  echo							>> $szConfig.def
  sm_last="Comment"
}

############################################################################
# Comment() - Put a comment into the header
# $@ - the comment

Comment () {
  if [ "$CONFIG_RO" = "n" ]; then
    if [ -n "$szOutputHeader" ]; then
      if [ "$sm_last" != "Comment" ]; then
        echo						>> $szOutputHeader
      fi
      echo "/*"						>> $szOutputHeader
      for Comment_x in "$@" ; do
        echo " * $Comment_x"				>> $szOutputHeader
      done
      echo " */"					>> $szOutputHeader
      echo 						>> $szOutputHeader
    fi
  
    if [ "$sm_last" != "Comment" ]; then
      echo						>> $szConfig.def
    fi
    for Comment_x in "$@" ; do
      echo "# $Comment_x"				>> $szConfig.def
    done
    echo 						>> $szConfig.def
  fi
  
  if [ "$sm_last" != "Comment" ]; then
    echo
  fi
  echo "#"
  for Comment_x in "$@" ; do
    echo "# $Comment_x"
  done
  echo "#"
  echo

  sm_last="Comment"
}

############################################################################
# Define() - define a variable in the header
# $1 - variable to define
# $2 - value of variable in software
# $3 - value of variable in header (optional)

Define () {
  if [ "$CONFIG_RO" = "n" ]; then
    if [ -n "$szOutputHeader" ]; then
      if [ -n "$3" ]; then
        echo "#define $1 $3" 				>> $szOutputHeader
      else
        echo "#define $1 $2"				>> $szOutputHeader
      fi
    fi
  
    echo "$1=$2"					>> $szConfig.def
  fi
  
  eval "$1=\"`eval echo \"$2\"`\""
}

############################################################################
# FinishOff() - add any extra bumphf to the header
# no parameters

FinishOff () {
  if [ "$CONFIG_RO" = "y" ]; then
    return
  fi
  
  if [ -n "$szOutputHeader" ]; then
    echo 							>> $szOutputHeader
    echo "#endif /* __${szConfigHeader}_CONFIG_HEADER */"	>> $szOutputHeader
  fi
}

############################################################################
# GetString() - handle keyboard input

GetString () {
  if [ "$SILENT" = "TRUE" ]; then
    eval "$2=\"`eval echo \"$3\"`\""
    echo "$1 ($4) [$3] : $3"
  else
    ReadLine "$1" "$2" "$3" "$4"
  fi
}

############################################################################
# Integer() - define an integer in the header
# $1 - message to print
# $2 - variable
# $3 - default value

Integer () {
  GetString "$1" "Integer_x" "$3" "$2"

  Define "$2" "$Integer_x"
  UpdateConfig '"$1" $2 $3' '"$1" $2 $Integer_x'
  sm_last="Integer"
}

Set_Global_Options () {
  echo > /dev/null
}

############################################################################
# String() - define a string in the header
# $1 - message to print
# $2 - variable
# $3 - default string

String () {
  GetString "$1" "String_x" "$3" "$2"

  Define "$2" "\"$String_x\""
  UpdateConfig "$2 \"$3\"" "$2 \"$String_x\""
  
  sm_last="String"
}

############################################################################
# Undefine - undef a variable in the header
# $1 - variable to undef

Undefine () {
  if [ "$CONFIG_RO" = "n" ]; then
    echo "#undef $1" 					>> $szOutputHeader
    echo "$1=n"						>> $szConfig.def
  fi
  eval "$1=n"
}

############################################################################
# UpdateConfig - record the user's selection
# $1 - old config line
# $2 - new config line

UpdateConfig () {
# This routine should be needed no more, but is left in for now

echo > /dev/null			# NO-OP

#  if [ "$1" != "$2" ]; then
#   sed "/$1/s//$2/" $szConfig > $szConfig.new
#    mv $szConfig.new $szConfig
#  fi
}

############################################################################
# main - the action starts here
# $@ - command line parameters

main () {
  # we create and parse $tmpfile because bash(1) loses the value of any
  # variable you set inside a while loop ;-(
  
  AUTO=FALSE

  if [ "$1" = "--no-write" ]; then
    CONFIG_RO="y"
    shift
  else
    CONFIG_RO="n"
  fi
  
  szInput="$1"
  szOutputHeader="`basename $1 .conf`.h"
  
  szCwd="`pwd`"
  szConfigOrig="`basename $1 .conf`.def"
  szConfigHeader="`basename $1 .conf`"
  
  CleanSlate
  
  for szConfig in $szInput ; do
    if [ -f "$szConfig" ]; then
      if [ -f "$szConfig.def" ]; then
        . $szConfig.def
      fi
      CleanStateDef
      . $szConfig
    else
      echo "Configure.sh:ERROR:File $szConfig not found."
      exit 1
    fi
  done
  
  FinishOff
}

############################################################################

. ../SETUP/libSETUP.sh
. ../../../../SETUPDir/GSetup-Scripts/GSetup-lib.sh
. ../../../../SETUPDir/Product/DESC
. ../SETUP/SETUP.def
main "$@"
