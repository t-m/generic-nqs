#ifndef __GPORT_TRANS_H
#define __GPORT_TRANS_H

/*
 * SETUP/all-systems/GPort_Trans.h
 * 		Header to translate #defines created by the GPort script
 *		into something GNQS expects
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1999 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <SETUP/GPort.h>

#if	GPORT_HAS_BSD_LIMITS
#undef	HAS_BSD_LIMITS
#define	HAS_BSD_LIMITS 1
#endif

#if	GPORT_HAS_LIMIT_LIMITS
#undef	HAS_LIMIT_LIMITS
#define	HAS_LIMIT_LIMITS 1
#endif

#if	GPORT_HAS_NICE_LIMITS
#undef	HAS_NICE_LIMITS
#define	HAS_NICE_LIMITS 1
#endif

#if	GPORT_HAS_RLIM_T
#undef	HAS_RLIM_T
#define	HAS_RLIM_T 1
#endif

#if	GPORT_HAS_SETPRIORITY_LIMITS
#undef	HAS_PRIORITY_LIMITS
#define	HAS_PRIORITY_LIMITS 1
#endif

/*
 * This is temporary, until full signal handling is added to this copy
 * of GPort
 */

#include <signal.h>
#ifndef SIGCLD
#define SIGCLD SIGCHLD
#endif

/*
 * Tests to make sure timezone handling is consistent
 */

#undef  GPORT_HAS_SYSV_TIME
#undef  GPORT_HAS_BSD_TIME

#if GPORT_HAS_TZNAME && GPORT_HAS_TZ_TIMEZONE && GPORT_HAS_TZSET
#define GPORT_HAS_SYSV_TIME 1
#define GPORT_HAS_BSD_TIME 0
#else
#define GPORT_HAS_SYSV_TIME 0
#define GPORT_HAS_BSD_TIME 1
#endif

#endif /* __GPORT_TRANS_H */
