# -*- sh -*-
#
# Script to install NQS files on a clustered workstation

umask 022
. ./General.conf.def
. ./SETUP.def

. ./install.xxx

for x in nqsmkdirs nqsmktrans ; do
	if [ ! -f ../bin/$x ]; then
		cp $1/$x ../bin
	fi
done

mknqsspooldir
mkspoolfiles
