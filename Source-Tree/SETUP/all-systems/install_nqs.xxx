# Script to install NQS on a moment's notice.
#
# This script depends on a make stage having been done previously
# to place the binaries of interest in the area $NQS_STAGE_LOC. 
# This script then checks the staging area and installs any files found
# there.  This could be run when the system boots up, before NQS
# is started.  

if [ -r  $NQS_STAGE_LOC/xlpserver ] 
then
    cp $NQS_STAGE_LOC/xlpserver $NQS_LIBEXE/lpserver
    chown $NQS_OWNER $NQS_LIBEXE/lpserver
    chgrp $NQS_GROUP $NQS_LIBEXE/lpserver
    chmod 755 $NQS_LIBEXE/lpserver
fi

if [ -r  $NQS_STAGE_LOC/xnetclient ] 
then
    cp $NQS_STAGE_LOC/xnetclient $NQS_LIBEXE/netclient
    chown $NQS_OWNER $NQS_LIBEXE/netclient
    chgrp $NQS_GROUP $NQS_LIBEXE/netclient
    chmod 700 $NQS_LIBEXE/netclient
fi

if [ -r  $NQS_STAGE_LOC/xpipeclient ] 
then
    cp $NQS_STAGE_LOC/xpipeclient $NQS_LIBEXE/pipeclient
    chown $NQS_OWNER $NQS_LIBEXE/pipeclient
    chgrp $NQS_GROUP $NQS_LIBEXE/pipeclient
    chmod 700 $NQS_LIBEXE/pipeclient
fi

if [ -r  $NQS_STAGE_LOC/xshlexefai ] 
then
    cp $NQS_STAGE_LOC/xshlexefai $NQS_LIBEXE/shlexefai
    chown $NQS_OWNER $NQS_LIBEXE/shlexefai
    chgrp $NQS_GROUP $NQS_LIBEXE/shlexefai
    chmod 6755 $NQS_LIBEXE/shlexefai
fi

if [ -r  $NQS_STAGE_LOC/xfinish_autoinst ] 
then
    cp $NQS_STAGE_LOC/xfinish_autoinst $NQS_LIBEXE/finish_autoinst
    chown $NQS_OWNER $NQS_LIBEXE/finish_autoinst
    chgrp $NQS_GROUP $NQS_LIBEXE/finish_autoinst
    chmod 6755 $NQS_LIBEXE/finish_autoinst
fi

if [ -r  $NQS_STAGE_LOC/xqcmplx ] 
then
    cp $NQS_STAGE_LOC/xqcmplx $NQS_USREXE/qcmplx
    chown $NQS_OWNER $NQS_USREXE/qcmplx
    chgrp $NQS_GROUP $NQS_USREXE/qcmplx
    chmod 6755 $NQS_USREXE/qcmplx
fi

if [ -r  $NQS_STAGE_LOC/xqdel ] 
then
    cp $NQS_STAGE_LOC/xqdel $NQS_USREXE/qdel
    chown $NQS_OWNER $NQS_USREXE/qdel
    chgrp $NQS_GROUP $NQS_USREXE/qdel
    chmod 6755 $NQS_USREXE/qdel
fi

if [ -r  $NQS_STAGE_LOC/xqsuspend ] 
then
    rm -f $NQS_USREXE/qresume
    cp $NQS_STAGE_LOC/xqsuspend $NQS_USREXE/qsuspend
    chown $NQS_OWNER $NQS_USREXE/qsuspend
    chgrp $NQS_GROUP $NQS_USREXE/qsuspend
    chmod 6755 $NQS_USREXE/qsuspend
    ln -s $NQS_USREXE/qsuspend $NQS_USREXE/qresume
fi

if [ -r  $NQS_STAGE_LOC/xqdev ] 
then
    cp $NQS_STAGE_LOC/xqdev $NQS_USREXE/qdev
    chown $NQS_OWNER $NQS_USREXE/qdev
    chgrp $NQS_GROUP $NQS_USREXE/qdev
    chmod 6755 $NQS_USREXE/qdev
fi

if [ -r  $NQS_STAGE_LOC/xqlimit ] 
then
    cp $NQS_STAGE_LOC/xqlimit $NQS_USREXE/qlimit
    chown $NQS_OWNER $NQS_USREXE/qlimit
    chgrp $NQS_GROUP $NQS_USREXE/qlimit
    chmod 6755 $NQS_USREXE/qlimit
fi

if [ -r  $NQS_STAGE_LOC/xqpr ] 
then
    cp $NQS_STAGE_LOC/xqpr $NQS_USREXE/qpr
    chown $NQS_OWNER $NQS_USREXE/qpr
    chgrp $NQS_GROUP $NQS_USREXE/qpr
    chmod 6755 $NQS_USREXE/qpr
fi

if [ -r  $NQS_STAGE_LOC/xqhold ] 
then
    cp $NQS_STAGE_LOC/xqhold $NQS_USREXE/qhold
    chown $NQS_OWNER $NQS_USREXE/qhold
    chgrp $NQS_GROUP $NQS_USREXE/qhold
    chmod 6755 $NQS_USREXE/qhold
fi

if [ -r  $NQS_STAGE_LOC/xqmsg ] 
then
    cp $NQS_STAGE_LOC/xqmsg $NQS_USREXE/qmsg
    chown $NQS_OWNER $NQS_USREXE/qmsg
    chgrp $NQS_GROUP $NQS_USREXE/qmsg
    chmod 6755 $NQS_USREXE/qmsg
fi

if [ -r  $NQS_STAGE_LOC/xqrls ] 
then
    cp $NQS_STAGE_LOC/xqrls $NQS_USREXE/qrls
    chown $NQS_OWNER $NQS_USREXE/qrls
    chgrp $NQS_GROUP $NQS_USREXE/qrls
    chmod 6755 $NQS_USREXE/qrls
fi

if [ -r  $NQS_STAGE_LOC/xqjob ] 
then
    cp $NQS_STAGE_LOC/xqjob $NQS_USREXE/qjob
    chown $NQS_OWNER $NQS_USREXE/qjob
    chgrp $NQS_GROUP $NQS_USREXE/qjob
    chmod 6755 $NQS_USREXE/qjob
fi

if [ -r  $NQS_STAGE_LOC/xqstatc ] 
then
    cp $NQS_STAGE_LOC/xqstatc $NQS_USREXE/qstatc
    chown $NQS_OWNER $NQS_USREXE/qstatc
    chgrp $NQS_GROUP $NQS_USREXE/qstatc
    chmod 6755 $NQS_USREXE/qstatc
fi

if [ -r  $NQS_STAGE_LOC/xqstat ] 
then
    cp $NQS_STAGE_LOC/xqstat $NQS_USREXE/qstat
    chown $NQS_OWNER $NQS_USREXE/qstat
    chgrp $NQS_GROUP $NQS_USREXE/qstat
    chmod 6755 $NQS_USREXE/qstat
fi

if [ -r  $NQS_STAGE_LOC/xqsub ] 
then
    cp $NQS_STAGE_LOC/xqsub $NQS_USREXE/qsub
    chown $NQS_OWNER $NQS_USREXE/qsub
    chgrp $NQS_GROUP $NQS_USREXE/qsub
    chmod 6755 $NQS_USREXE/qsub
fi

if [ -r  $NQS_STAGE_LOC/xnqsdaemon ] 
then
    mv -f $NQS_LIBEXE/nqsdaemon $NQS_LIBEXE/OLDnqsdaemon
    cp $NQS_STAGE_LOC/xnqsdaemon $NQS_LIBEXE/nqsdaemon
    chown $NQS_OWNER $NQS_LIBEXE/nqsdaemon
    chgrp $NQS_GROUP $NQS_LIBEXE/nqsdaemon
    chmod 2755 $NQS_LIBEXE/nqsdaemon
fi

if [ -r  $NQS_STAGE_LOC/xloaddaemon ] 
then
    mv -f $NQS_LIBEXE/loaddaemon $NQS_LIBEXE/OLDloaddaemon
    cp $NQS_STAGE_LOC/xloaddaemon $NQS_LIBEXE/loaddaemon
    chown $NQS_OWNER $NQS_LIBEXE/loaddaemon
    chgrp $NQS_GROUP $NQS_LIBEXE/loaddaemon
    chmod 700 $NQS_LIBEXE/loaddaemon
fi

if [ -r  $NQS_STAGE_LOC/xnetdaemon ] 
then
    mv -f $NQS_LIBEXE/netdaemon $NQS_LIBEXE/OLDnetdaemon
    cp $NQS_STAGE_LOC/xnetdaemon $NQS_LIBEXE/netdaemon
    chown $NQS_OWNER $NQS_LIBEXE/netdaemon
    chgrp $NQS_GROUP $NQS_LIBEXE/netdaemon
    chmod 700 $NQS_LIBEXE/netdaemon
fi

if [ -r  $NQS_STAGE_LOC/xnetserver ] 
then
    mv -f $NQS_LIBEXE/netserver $NQS_LIBEXE/OLDnetserver
    cp $NQS_STAGE_LOC/xnetserver $NQS_LIBEXE/netserver
    chown $NQS_OWNER $NQS_LIBEXE/netserver
    chgrp $NQS_GROUP $NQS_LIBEXE/netserver
    chmod 755 $NQS_LIBEXE/netserver
fi

if [ -r  $NQS_STAGE_LOC/xqmgr ] 
then
    cp $NQS_STAGE_LOC/xqmgr $NQS_USREXE/qmgr
    chown $NQS_OWNER $NQS_USREXE/qmgr
    chgrp $NQS_GROUP $NQS_USREXE/qmgr
    chmod 6755 $NQS_USREXE/qmgr
    if [ -r $NQS_STAGE_LOC/qmgr.hlp ]
    then
        cp $NQS_STAGE_LOC/qmgr.hlp $QMGR_HELPFILE
        chown $NQS_OWNER $QMGR_HELPFILE
        chgrp $NQS_GROUP $QMGR_HELPFILE
        chmod 0644 $QMGR_HELPFILE
    fi
fi

if [ -r  $NQS_STAGE_LOC/xnmapmgr ] 
then
    cp $NQS_STAGE_LOC/xnmapmgr $NQS_USREXE/nmapmgr
    chown $NQS_OWNER $NQS_USREXE/nmapmgr
    chgrp $NQS_GROUP $NQS_USREXE/nmapmgr
    chmod 755 $NQS_USREXE/nmapmgr
fi

if [ -r  $NQS_STAGE_LOC/xqacct ] 
then
    cp $NQS_STAGE_LOC/xqacct $NQS_USREXE/qacct
    chown $NQS_OWNER $NQS_USREXE/qacct
    chgrp $NQS_GROUP $NQS_USREXE/qacct
    chmod 755 $NQS_USREXE/qacct
fi

if [ -r  $NQS_STAGE_LOC/xqcat ] 
then
    cp $NQS_STAGE_LOC/xqcat $NQS_USREXE/qcat
    chown $NQS_OWNER $NQS_USREXE/qcat
    chgrp $NQS_GROUP $NQS_USREXE/qcat
    chmod 6755 $NQS_USREXE/qcat
fi

