#		-*- sh -*-
#
# SETUP v3
# Source Tree Builder And Compiler
# Developed for Generic NQS v3.50.0
#
# Copyright (c) 1995 The University of Sheffield
# Released under v2 of the GNU GPL
#
# Author(s) :
#
# Stuart Herbert
# Academic Computing Services
# The University of Sheffield
#
############################################################################
# WARNING
#
# To use SETUP, your computer's Bourne Shell (/bin/sh) must support shell
# scripts that define functions.  If your computer's Bourne Shell doesn't
# do this, you should modify the first line of this script to point to
# either /bin/ksh (Korn Shell) or to bash (GNU shell).
#
############################################################################

if [ -f ./SETUPDir/GSetup-Scripts/GSetup-lib.sh ]; then
	. ./SETUPDir/GSetup-Scripts/GSetup-lib.sh
elif [ -f ../../../../SETUPDir/GSetup-Scripts/GSetup-lib.sh ]; then
	. ../../../../SETUPDir/GSetup-Scripts/GSetup-lib.sh
else
	echo "Cannot find GSetup-lib.sh - giving up"
	exit 255
fi

############################################################################
# GLOBAL VARIABLES
############################################################################

case "$TERM" in
  ansi*|linux|vt*|xterm*|dtterm*)
  COLOUR_NORMAL="[0m"
    COLOUR_BOLD="[1m"
    COLOUR_BLINK="[5m"
    ;;
  *)
    COLOUR_NORMAL=""
    COLOUR_BOLD=""
    COLOUR_BLINK=""
    ;;
esac

############################################################################
# SCREEN FUNCTIONS
############################################################################

endpage () { 
  echo
  separator
  pause
}

# $1 - Title of new page
newpage () {
  clear
  showbold "$1"
  separator
  echo
}

pause () {
  showbold "Press ENTER/RETURN to continue, or CTRL-C to quit."
  read x < /dev/tty
}

sh_printf () {
  F_ECHO "$@"
}

separator () {
  echo "--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--"
}

showblink () {
  echo "${COLOUR_BLINK}$@${COLOUR_NORMAL}"
}

showbold () {
  echo "${COLOUR_BOLD}$@${COLOUR_NORMAL}"
}

shownormal () {
  echo "${COLOUR_NORMAL}$@"
}

############################################################################
# PRODUCT DESCRIPTION FUNCTIONS
############################################################################

AUTHOR () {
  AUTHOR="$1"
}

AUTHOR_EMAIL () {
  AUTHOR_EMAIL="$1"
}

BUILDNO () {
  BUILDNO="$1"
  BUILDSTRING="$PRODUCT-$VERSION-pre$BUILDNO"
  VERSION="$VERSION.$1"
}

COPYRIGHT () {
  if [ "$COPYRIGHT_VAR" = "x" ]; then
    COPYRIGHT_VAR="  Copyright (c) $1\\n"
  else
    COPYRIGHT_VAR="$COPYRIGHT_VAR  Copyright (c) $1\\n"
  fi
}

COPYRIGHTEND () {
  COPYRIGHTEND_VAR="$1"
}

LASTRELEASE () {
  LASTRELEASE="$1"
}

PRODUCT () {
  PRODUCT="$1"
}

RELEASEDATE () {
  RELEASEDATE="$1"
}

VERSION () {
  VERSION="$1"
  BUILDSTRING="$PRODUCT-$1"
  BUILDNO="0"
}

##########################################################################
# INPUT ROUTINES
##########################################################################

GetString () {
  ReadLine "$1" "$2" "$3" "$4" "$5"
}

# $1 - prompt
# $2 - variable to set
# $3 - default value
# $4 - (optional) - variable name that will eventually get set
# $5 - (optional) - path to config docs directory
ReadLine() {
  ReadLine_Done="n"
  
  while [ "$ReadLine_Done" = "n" ]; do
    ReadLine_y="$3"

    sh_printf "$1 ($ReadLine_y): "
    read ReadLine_x

    if [ -z "$ReadLine_x" ]; then
      eval "$2=\"`eval echo \"$ReadLine_y\"`\""
      ReadLine_Done="y"
    elif [ "$ReadLine_x" = "?" ]; then
      if [ -f "${5}ConfigDocs/$4" ]; then
        echo
        more "${5}ConfigDocs/$4"
        echo
      else
        echo "  *** Sorry, no help available for this topic."
      fi
    else
      eval "$2=\"`eval echo \"$ReadLine_x\"`\""
      ReadLine_Done="y"
    fi
  done
}
