GPORT_PLATFORM_ID="i586-pc-linux2.2.0-pre6"

GPORT_PLATFORM_HARDWARE="i586"

GPORT_PLATFORM_VENDOR="pc"

GPORT_PLATFORM_OS="LINUX"

GPORT_PLATFORM_OS_VERSION="2.2.0-PRE6"

GPORT_PLATFORM_OS_MVERS="2"

GPORT_PLATFORM_OS_RVERS="2_2"

GPORT_IS_LINUX=1

GPORT_IS_LINUX_2=1

GPORT_IS_LINUX_2_2=1

GPORT_EXE_AR="/usr/bin/ar cr"

GPORT_EXE_AR=

GPORT_EXE_HOSTNAME="/bin/hostname"

GPORT_EXE_MKDIR="/bin/mkdir -p"

GPORT_EXE_RANLIB="/usr/bin/ranlib"

GPORT_EXE_RM="/bin/rm"

GPORT_EXE_UNAME="/bin/uname"

GPORT_CC="/usr/bin/cc"

GPORT_HAS_EGCS=0

GPORT_HAS_GCC=1

GPORT_CC_FLAGS_DEBUG="-Wpointer-arith -Wbad-function-cast -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Werror"

GPORT_CC_FLAGS_OPTIMISE="-O6 -fno-strength-reduce"

GPORT_CC_FLAGS_COMMON="-Wall"

GPORT_CC_FLAGS_DEBUG="-Wpointer-arith -Wbad-function-cast -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Werror -g3"

GPORT_SIZEOF_UNSIGNED_LONG="4"

GPORT_SIZEOF_INT="4"

GPORT_HAS_sys_wait_h=1

GPORT_HAS_POSIX_ZOMBIES=0

GPORT_HAS_BSD_ZOMBIES=0

