# -*- sh -*- 
# Set of functions used to install Generic NQS

mkspooldir () {
  if [ ! -d "$NQS_SPOOL/$1" ]; then
    mkdir "$NQS_SPOOL/$1"
  fi
  chmod $2 $NQS_SPOOL/$1
}

mkspoolfile () {
  touch $NQS_SPOOL/$1
  chmod 644 $NQS_SPOOL/$1
}

copynqsfiles () {
  for x in $1/finish_autoinst $1/lpserver $1/netclient $1/pipeclient $1/shlexefai $1/sys_job $1/user_job $1/nqssetargv $1/nqsexejob $1/nqsmkfile $1/getuid ; do
    echoline "  o  Copying `basename $x`"
    cp $x $NQS_LIBEXE/`basename $x`
    chown $NQS_OWNER $NQS_LIBEXE/`basename $x`
    chgrp $NQS_GROUP $NQS_LIBEXE/`basename $x`
    chmod 755 $NQS_LIBEXE/`basename $x`
  done

  for x in $1/loaddaemon $1/netdaemon $1/netserver $1/nqsdaemon $1/nqsmktrans $1/nqsmkdirs ; do
    echoline "  o  Copying `basename $x`"
    if [ -f $NQS_LIBEXE/`basename $x` ]; then
      mv -f $NQS_LIBEXE/`basename $x` $NQS_LIBEXE/OLD`basename $x`
    fi
    cp $x $NQS_LIBEXE/`basename $x`
    chown $NQS_OWNER $NQS_LIBEXE/`basename $x`
    chgrp $NQS_GROUP $NQS_LIBEXE/`basename $x`
    chmod 755 $NQS_LIBEXE/`basename $x`
  done

  for x in $1/nqs.prologue $1/nqs.epilogue ; do
    if [ -f $x ]; then
      echoline "  o  Copying `basename $x`"
      cp $x $NQS_LIBEXE/`basename $x`
      chown $NQS_OWNER $NQS_LIBEXE/`basename $x`
      chgrp $NQS_GROUP $NQS_LIBEXE/`basename $x`
      chmod 755 $NQS_LIBEXE/`basename $x`
    fi
  done

  for x in $1/qalter $1/qcat $1/qdel $1/qdev $1/qhold $1/qlimit $1/qmgr $1/qmsg $1/qpr $1/qrls $1/qstat $1/qsub $1/qsuspend ; do
    echoline "  o  Copying `basename $x`"
    cp $x $NQS_USREXE/`basename $x`
    chown $NQS_OWNER $NQS_USREXE/`basename $x`
    chgrp $NQS_GROUP $NQS_USREXE/`basename $x`
    chmod 6111 $NQS_USREXE/`basename $x`
  done

  for x in $1/nmapmgr $1/qacct ; do
    echoline "  o  Copying `basename $x`"
    cp $x $NQS_USREXE/`basename $x`
    chown $NQS_OWNER $NQS_USREXE/`basename $x`
    chgrp $NQS_GROUP $NQS_USREXE/`basename $x`
    chmod 755 $NQS_USREXE/`basename $x`
  done

  if [ -f "$NQS_USREXE/qresume" ]; then
    rm -f $NQS_USREXE/qresume
  fi
  ln -s $NQS_USREXE/qsuspend $NQS_USREXE/qresume

  if [ -f $1/qmgr.hlp ]; then
    cp $1/qmgr.hlp $QMGR_HELPFILE
  else
    cp ../qmgr/qmgr.hlp $QMGR_HELPFILE
  fi
  chown $NQS_OWNER $QMGR_HELPFILE
  chgrp $NQS_GROUP $QMGR_HELPFILE
  chmod 644        $QMGR_HELPFILE

  for x in $1/GNQS.VERSION ../SETUP/General.conf.def ; do
	cp    $x         $NQS_HOME/`basename $x`
	chown $NQS_OWNER $NQS_HOME/`basename $x`
	chgrp $NQS_GROUP $NQS_HOME/`basename $x`
	chmod 644        $NQS_HOME/`basename $x`
  done
}

copynqsmanpages () {
  # now we copy the man pages
  cd ../man
  for man_x in * ; do
    if [ -d "$man_x" ]; then
      if [ ! -d "$NQS_MAN/$man_x" ]; then
        echoline "  o  Creating directory '$man_x'"
        mkdir $NQS_MAN/$man_x ;
	chown $NQS_OWNER $NQS_MAN/$man_x
	chgrp $NQS_GROUP $NQS_MAN/$man_x
	chmod 755 $NQS_MAN/$man_x
      fi
      
      cd $man_x
      for man_y in * ; do
        echoline "  o  Installing manual page '$man_y'"
        cp $man_y $NQS_MAN/$man_x
	chown $NQS_OWNER $NQS_MAN/$man_x/$man_y
	chgrp $NQS_GROUP $NQS_MAN/$man_x/$man_y
	chmod 644 $NQS_MAN/$man_x/$man_y
      done
      cd ..
    fi
  done
  
  cd ../SETUP
}

mkspoolfiles () {
  mkspooldir dump 777
  mkspooldir new  700
  mkspooldir new/requests 777
  mkspooldir private 700
  mkspooldir private/root 777
  mkspooldir private/root/control 777
  mkspooldir private/root/data 777
  mkspooldir private/root/database 777
  mkspooldir private/root/database_qa 777
  mkspooldir private/root/database_qo 777
  mkspooldir private/root/failed 777
  mkspooldir private/root/interproc 777
  mkspooldir private/root/output 777
  mkspooldir private/root/transact 777
  mkspooldir scripts 711

  mkspoolfile private/root/database/devices
  mkspoolfile private/root/database/forms
  mkspoolfile private/root/database/managers
  mkspoolfile private/root/database/servers
  mkspoolfile private/root/database/netqueues
  mkspoolfile private/root/database/params
  mkspoolfile private/root/database/pipeto
  mkspoolfile private/root/database/qmaps
  mkspoolfile private/root/database/queues
  mkspoolfile private/root/database/qcomplex
  mkspoolfile private/root/database/servers

  ../bin/nqsmkdirs
  ../bin/nqsmktrans
}

echoline () {
  if [ "$CONFIG_NQS_INTERACTIVE" = "y" ]; then
    echo "$1"
  fi
}

mknqsdirs () {
  for x in $NQS_LIBEXE $NQS_HOME $NQS_MAN $NQS_USREXE $NQS_STAGE_LOC $NQS_NMAP ; do
    echoline "  o  Checking ... $x"
    if [ ! -d $x ]; then
      echoline "     Not found.  Creating directory."
      mkdir -p $x
    fi
    echoline "     Setting permissions to 755."
    chmod 755 $x
  done
}

mknqsspooldir ()
{
  echoline "  o  Checking ... $NQS_SPOOL"
  if [ ! -d $NQS_SPOOL ]; then
    echoline "     Not found.  Creating directory."
    mkdir -p $NQS_SPOOL
  fi
  echoline "     Setting permissions to 711"
  chmod 711 $NQS_SPOOL
}

mkstagefiles () {      
  for x in ../bin/* ; do
    echoline "  o  Copying `basename $x`"
    cp $x $NQS_STAGE_LOC/`basename $x`
  done

  echoline "  o  Copying qmgr.hlp"
  cp ../qmgr/qmgr.hlp $NQS_STAGE_LOC/qmgr.hlp

  echoline "  o  Making VERSION file"
  echo \"`cat ../bin/GNQS.VERSION`\"	> $NQS_STAGE_LOC/VERSION

  if [ -f "$NQS_STAGE_LOC/install_nqs" ]; then
    rm -f $NQS_STAGE_LOC/install_nqs
  fi

  echo "#! /bin/sh"					>  $NQS_STAGE_LOC/install_nqs
  echo "# Perform an upgrade to $PRODUCT $VERSION"	>> $NQS_STAGE_LOC/install_nqs
  echo "umask 022"					>> $NQS_STAGE_LOC/install_nqs
  cat ./General.conf.def 				>> $NQS_STAGE_LOC/install_nqs
  cat ./SETUP.def	 				>> $NQS_STAGE_LOC/install_nqs
  cat ./install.xxx					>> $NQS_STAGE_LOC/install_nqs
  echo "copynqsfiles $NQS_STAGE_LOC >/dev/null 2>&1"	>> $NQS_STAGE_LOC/install_nqs

  chmod 755 $NQS_STAGE_LOC/install_nqs
}
