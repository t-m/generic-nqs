list_nqs_queues subroutine call

PURPOSE:
	To list all available NQS queues

SYNTAX:
	int	list_nqs_queues ( queue_ptr, queue_count );

PARAMETERS:

	char	*queue_ptr	
		Pointer to buffer at least queue_count * 32 bytes in size to receive
		list of available NQS queues.

	int	queue_count
		Maximum number of queue names to return;

DESCRIPTION:

	This subroutine returns the name of all available NQS queues in a character
	buffer, where each entry takes up 32 bytes.

	The libraries /usr/lib/nqs/libnqs.a and /usr/lib/nqs/libnmap.a must be loaded
	with the program.

RETURN VALUES:

	If there is no error, the routine returns the number of queues, or queue_count,
	which ever is smaller.  If there is an error, a negative number will be returned.

EXAMPLE USE:

	Here is an example of its use:


#include <stdio.h>

int list_nqs_queues();
/*
 * Compiled using the command line:
 *
 *	cc  -o list_queues ../src/list_queues.c /usr/lib/nqs/libnqs.a \
 *		/usr/lib/nqs/libnmap.a -lc_s
 *
 */

main (argc, argv)
int argc;
char *argv[];
{
    char    buffer[32*100];
    char    i,  count;
    char    *cptr;
    
    count = list_nqs_queues(buffer,  1);
    printf("Returned count is %d\n",  count);
    cptr = buffer;
    for (i = 0; i < count; i++ ) {
	printf("Queue %d is %s\n",  i,  cptr);
	cptr += 32;
    }
}
