.TH QSTAT 1
\"
\"	Network Queueing System (NQS)
\"  This version of NQS is Copyright (C) 1992  John Roman
\"
\"  This program is free software; you can redistribute it and/or modify
\"  it under the terms of the GNU General Public License as published by
\"  the Free Software Foundation; either version 1, or (at your option)
\"  any later version.
\"
\"  This program is distributed in the hope that it will be useful,
\"  but WITHOUT ANY WARRANTY; without even the implied warranty of
\"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\"  GNU General Public License for more details.
\"
\"  You should have received a copy of the GNU General Public License
\"  along with this program; if not, write to the Free Software
\"  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
\"
.SH NAME
qstat \- display status of \s-1NQS\s+1 queue(s)
.SH SYNOPSIS
.B qstat
.RB [ \-a ]
.RB [ \-d ]
.RB [ \-l ]
.RB [ \-m ]
.RB [ \-o ]
.RB [ \-s ]
.RB [ \-u " user-name ]"
.RB [ \-v ]
[ queue-name@host-name ... ]
.br
.B qstat
.RB [ \-x ] 
.RB [ \-b ] 
.RB [ \-l ]
[ queue-name ... ]
.br
.B qstat
.RB [ \-c ] 
.RB [ \-b ] 
.RB [ \-l ]
[ complex-name ... ]
.br
.SH DESCRIPTION
.I Qstat\^
displays the status of Network Queueing System (\s-1NQS\s+1) queues.
.PP
If no queues are specified, then the current state of all \s-1NQS\s+1
queues on the local host are displayed.
Otherwise, information is displayed for the specified queues only.
Queues may be specified either as
.I queue-name\^
or
.IR queue-name@host-name\^ .
In the absence of a
.I host-name\^
specifier, the local host is assumed.
.PP
The various 
.I qstat\^
switches are separated into several groups according to which jobs are
selected and how the output is formatted:
.TP
Jobs selected by owner:
.TP
\f3Default\fP
Shows only the current user's jobs.
.TP
\f3-a\fP
Shows all requests.
.TP
\f3-u \f2user-name\^\fP
Shows only those requests belonging to
.IR user-name\^ .
.TP
Jobs selected by machine:
.TP
\f3Default\fP
Shows only jobs on the current machine.
.TP
\f3-d\fP
Shows all jobs in machines within the local NQS domain.
.TP
\f3queue\fP
Shows jobs in a particular queue.
.TP
\f3@machine\fP
Shows jobs on a specific machine.
.TP
\f3queue@machine\fP
Shows jobs on a specific queue on a particular machine.
.TP
Jobs selected by origin:
.TP
\f3Default\fP
Shows jobs which originated from anywhere.
.TP
\f3-o\fP
Shows jobs that originated from the current machine.
.TP
The format of the output is determined by other switches:
.TP
\f3Default\fP
Shows jobs in the Monsanto summary format.
.TP
\f3-m\fP
Synonym for the default (Monsanto summary format).
.TP
\f3-l\fP
Standard NQS format with further detail.
.TP
\f3-s\fP
Standard NQS format.
.PP
The 
.I -x\^
switch gives greater detail on the queue header, and is modified
by the 
.I -b\^ 
or 
.I -l\^ 
switches which give lesser or greater detail, respectively,
of the queue header.
The
.I queue header\^
always includes the queue-name, queue type,
queue status (see below), an indication of whether or not the queue is
.I pipeonly\^
(accepts requests from pipe queues only),
and the number of requests in the queue.
An extended queue header goes on to display the priority and run limit
of a queue, as well as the access restrictions,
cumulative use statistics,
server and destinations (if a pipe queue),
queue to device mappings (if a device queue),
and resource limits (if a batch queue).
.PP
The 
.I -c\^ 
switch gives information about queue complexes and the display is
modified by the 
.I -b\^ 
and 
.I -l\^ 
switches to give lesser or greater detail,
respectively.  The display for the 
.I -b\^ 
switch gives only the complex name.
The default is to print the complex name and the run limit of that complex.
The 
.I -l\^
switch causes the name, the queue run limit, and the members of that
complex to be printed.
.PP
By default,
.I qstat\^
displays the following information about
a request: the
.IR request-name\^ ,
the
.IR request-id\^ ,
the owner, the queuename, the start time, the per-process
request time, the total time the request has been running, and
the status of the request.  The 
.I -m\^ 
switch also gives this
format.
.PP
\f2Qstat -s\fP shows the
.IR request-name\^ ,
the
.IR request-id\^,
the owner, the relative request priority, and
the current request state (see below).
For running requests, the process group is also shown,
as soon as this information becomes
available to the local \s-1NQS\s+1 daemon.
.PP
\f2Qstat -l\fP
shows the time at which the request was created, an indication of
whether or not mail will be sent, where mail
will be sent, and
the username on the originating machine.
If a batch queue is being examined, resource limits, planned
disposition of stderr and stdout, any advice concerning
the command interpreter, and the umask value are shown.
If a device queue is being examined, the requested forms are shown.
.PP
It must be understood that the relative ordering of requests within a queue
does not always determine the order in which the requests will be run.
The \s-1NQS\s+1 request scheduler is allowed to make exceptions to the request
ordering for the sake of efficient machine resource usage.  However, requests
appearing near the beginning of the queue have higher
priority than requests appearing later, and will usually be run before
requests appearing later on in the queue.
.PP
\f2Qstat -d\fP first checks the user's default directory for a file called .qstat,
which is a list of machines to be polled for the NQS status.
If this file is not present, then the system-wide file in
/usr/lib/nqs/nqs-domain is used. In this manner one may override
the system default and choose to get the status of a different list
of machines. 
The format of these files is a list of machines with
a single machine name on each line.  Lines with a # in the first column are
considered comments and ignored.
.SH QUEUE STATE
The general state of a queue is defined by two principal properties of
the queue.
.PP
The first property determines whether or not requests can be submitted to
the queue.  If they can, then the queue is said to be
.IR "enabled\^" .
Otherwise the queue is said to be
.IR "disabled\^" .
One of the words \f3\s-1CLOSED\s+1\fR, \f3\s-1ENABLED\s+1\fR, or
\f3\s-1DISABLED\s+1\fR will appear
in the queue status field to indicate the respective queue
states of: enabled (with no local \s-1NQS\s+1 daemon), enabled (and local
\s-1NQS\s+1 daemon
is present), and disabled.  Requests can only be submitted to the queue if
the queue is enabled, and the local \s-1NQS\s+1 daemon is present.
.PP
The second principal property of a queue determines if requests which are
ready to run, but are not now presently running, will be allowed to run
upon the completion of any currently running requests, and whether any requests
are presently running in the queue.
.PP
If queued requests not already running are blocked from running, and no
requests are presently executing in the queue, then the queue is said to be
.IR "stopped\^" .
If the same situation exists with the difference that at least one request
is running, then the queue is said to be
.IR "stopping\^" ,
where the requests presently executing will be allowed to complete
execution, but no new requests will be spawned.
.PP
If queued requests ready to run are only prevented from doing so by the
\s-1NQS\s+1 request scheduler, and one or more requests are presently running
in the queue, then the queue is said to be
.IR "running\^" .
If the same circumstances prevail with the exception that no requests are
presently running in the queue, then the queue is said to be
.IR "inactive\^" .
Finally, if the \s-1NQS\s+1 daemon for the local host upon which the queue
resides is not running, but the queue would otherwise be in the state
of
.I running\^
or
.IR "inactive\^" ,
then the queue is said to be
.IR "shutdown\^" .
The queue states describing the second principal property of a queue
are therefore respectively displayed
as \f3\s-1STOPPED\s+1\fR, \f3\s-1STOPPING\s+1\fR, \f3\s-1RUNNING\s+1\fR,
\f3\s-1INACTIVE\s+1\fR, and \f3\s-1SHUTDOWN\s+1\fR.
.SH REQUEST STATE
The state of a request may be
.IR arriving\^ ,
.IR holding\^ ,
.IR waiting\^ ,
.IR queued\^ ,
.IR staging\^ ,
.IR routing\^ ,
.IR running\^ ,
.IR departing\^ ,
or
.IR exiting\^ .
A request is said to be
.I arriving\^
if it is being enqueued from a remote host.
.I Holding\^
indicates that the request is presently prevented from entering any other
state (including the
.I running\^
state), because a
.I hold\^
has been placed on the request.
A request is said to be
.I waiting\^
if it was submitted with the constraint that it not run before a certain
date and time, and that date and time have not yet arrived.
.I Queued\^
requests are eligible to proceed (by
.I routing\^
or
.IR running\^ ).
When a request reaches the head of a pipe queue and receives
service there, it is
.IR routing\^ .
A request is
.I departing\^
from the time the pipe queue turns to other work until the request has
arrived intact at its destination.
.I "Staging\^"
denotes a
.I batch\^
request that has not yet begun execution, but for which input files are
being brought on to the execution machine.
A
.I running\^
request has reached its final destination queue, and is actually executing.
Finally,
.I "exiting\^"
describes a batch request that has completed execution, and will exit from
the system after the required output files have been returned (to possibly
remote machines).
.PP
Imagine a batch request originating on a workstation,
destined for the batch queue of a computation engine,
to be run immediately.
That request would first go through the states
.IR queued\^ ,
.IR routing\^ ,
and
.I departing\^
in a local pipe queue.
Then it would disappear from the pipe queue.
From the point of view of a queue on the computation engine,
the request would first be
.IR arriving\^ ,
then
.IR queued\^ ,
.I staging\^
(if required by the batch request),
.IR running\^ ,
and finally
.IR exiting\^ .
Upon completion of the
.I exiting\^
phase of execution, the batch request would disappear from the batch queue.
.SH "CAVEATS"
\s-1NQS\s+1 is not finished, and continues to undergo development.
Some of the request states shown above may or may not be supported
in your version of NQS.
.SH "EXAMPLES"
.PP
.nf
.ft CR
$ qstat -x
batch@beaker.monsanto.com;  type=BATCH;  [ENABLED, INACTIVE];  pri=16  lim=1
  0 exit;   0 run;   0 stage;   0 queued;   0 wait;   0 hold;   0 arrive;
  User run limit= 1

cray@beaker.monsanto.com;  type=PIPE;  [ENABLED, INACTIVE];  pri=16  lim=1
  0 depart;   0 route;   0 queued;   0 wait;   0 hold;   0 arrive;
  Destset = {batch@cray};

$ qstat -xl
batch@beaker.monsanto.com;  type=BATCH;  [ENABLED, INACTIVE];  pri=16  lim=1
  0 exit;   0 run;   0 stage;   0 queued;   0 wait;   0 hold;   0 arrive;
  User run limit= 1
  Cumulative system space time = 1.98 seconds
  Cumulative user space time = 0.80 seconds
  Unrestricted access
  Per-process core file size limit = 32 megabytes <DEFAULT>
  Per-process data size limit = 32 megabytes <DEFAULT>
  Per-process permanent file size limit = 500 megabytes <DEFAULT>
  Queue nondegrading priority =    0 
  Per-process execution nice value = 0 <DEFAULT>
  Per-process stack size limit = 32 megabytes <DEFAULT>
  Per-process CPU time limit = 360000.0 <DEFAULT>
  Per-process working set limit = 32 megabytes <DEFAULT>

cray@beaker.monsanto.com;  type=PIPE;  [ENABLED, INACTIVE];  pri=16  lim=1
  0 depart;   0 route;   0 queued;   0 wait;   0 hold;   0 arrive;
  Cumulative system space time = 0.00 seconds
  Cumulative user space time = 0.00 seconds
  Unrestricted access
  Queue server: /usr/lib/nqs/pipeclient
  Destset = {batch@cray};


$ qsub -eo -q batch -r example idle.nqs

$ qstat
Request         I.D.  Owner    Queue    Start Time   Time Limit  Total Time St
-------------- ------ -------- -------- -----------  ----------  ---------- --
example          130  jrroma   batch    4/30 11:40   4 04:00:00  0 00:00:00 R

$ qstat -s
batch@beaker.monsanto.com;  type=BATCH;  [ENABLED, RUNNING];  pri=16  lim=1
  0 exit;   1 run;   0 stage;   0 queued;   0 wait;   0 hold;   0 arrive;
  User run limit= 1

         REQUEST NAME        REQUEST ID            USER  PRI    STATE     PGRP
    1:        example       130.beaker            jrroma  31  RUNNING     8022
cray@beaker.monsanto.com;  type=PIPE;  [ENABLED, INACTIVE];  pri=16  lim=1
  0 depart;   0 route;   0 queued;   0 wait;   0 hold;   0 arrive;

$ qstat -l
batch@beaker.monsanto.com;  type=BATCH;  [ENABLED, RUNNING];  pri=16  lim=1
  0 exit;   1 run;   0 stage;   0 queued;   0 wait;   0 hold;   0 arrive;
  User run limit= 1

  Request    1:  Name=example
  Id=130.beaker     Owner=jrroma  Priority=31  RUNNING  Pgrp=8022  
  Created at Thu Apr 30 11:39:57 CDT 1992
  Mail = [NONE]
  Mail address = jrroma@beaker
  Owner user name at originating machine = jrroma
  Request is notrestartable, notrecoverable.
  Broadcast = [NONE]
  Per-proc. core file size limit= [32 megabytes, 32 megabytes]<DEFAULT>
  Per-proc. data size limit= [32 megabytes, 32 megabytes]<DEFAULT>
  Per-proc. permanent file size limit= [500 megabytes, 500 megabytes]<DEFAULT>
  Per-proc. execution nice priority = 0 <DEFAULT>
  Per-proc. stack size limit= [32 megabytes, 32 megabytes]<DEFAULT>
  Per-proc. CPU time limit= [360000.0, 360000.0]<DEFAULT>
  Per-proc. working set limit= [32 megabytes, 32 megabytes]<DEFAULT>
  Standard-error access mode = EO
  Standard-output access mode = SPOOL
  Standard-output name = beaker:/usr2/jrroma/tmp/example.o130
  Shell = DEFAULT
  Umask =  22

cray@beaker.monsanto.com;  type=PIPE;  [ENABLED, INACTIVE];  pri=16  lim=1
  0 depart;   0 route;   0 queued;   0 wait;   0 hold;   0 arrive;

.fi
.ft TR
.SH "BUGS"
One cannot yet get information on a remote queue complex.
.SH "SEE ALSO"
qdel(1), qdev(1), qlimit(1), qpr(1), and qsub(1)
.br
in the \f2\s-1NPSN\s+1 \s-1UNIX\s+1 System Programmer Reference Manual\fR.
.br
qmgr(1M) in the
\f2\s-1NPSN\s+1 \s-1UNIX\s+1 System Administrator Reference Manual\fR.
.SH "NPSN HISTORY"
Origin: Sterling Software Incorporated
.PP
.I "August 1985 \- Brent Kingsbury, Sterling Software"
.br
Original release.
.br
.sp 1
.I "May 1986"
.br
Second release.
.br
.sp 1
.I "August 1994 \- John Roman, Monsanto Company"
.br
Release 3.36
