


()                                                             ()



























































                                                                1





QCAT(1)                                                   QCAT(1)


NNAAMMEE
       qcat - display NQS request output.

SSYYNNOOPPSSIISS
       qqccaatt [ --ee ] [ --oo ] [ --vv ] request-id

DDEESSCCRRIIPPTTIIOONN
       _Q_c_a_t  displays  the  spooled stdout or stderr file for the
       NQS request whose _r_e_q_u_e_s_t_-_i_d  is  listed  on  the  command
       line.

       An  NQS  request  is  always  uniquely  identified  by its
       _r_e_q_u_e_s_t_-_i_d, no matter where it is in the  network  of  the
       machines  comprising  the NPSN.  A _r_e_q_u_e_s_t_-_i_d is always of
       the form: _s_e_q_n_o or _s_e_q_n_o_._h_o_s_t_n_a_m_e where  _h_o_s_t_n_a_m_e  identi-
       fies  the  machine  from whence the request was originally
       submitted,  and  _s_e_q_n_o  identifies  the  sequence   number
       assigned  to  the request on the originating host.  If the
       _h_o_s_t_n_a_m_e portion of a  _r_e_q_u_e_s_t_-_i_d  is  omitted,  then  the
       local host is always assumed.

       The  _r_e_q_u_e_s_t_-_i_d  of  any NQS request is displayed when the
       request is first submitted  (unless  the  _s_i_l_e_n_t  mode  of
       operation  for  the given NQS command was specified).  The
       user can also obtain the _r_e_q_u_e_s_t_-_i_d of any request through
       the use of the _q_s_t_a_t(1) command.

       The  _-_e  switch  indicates  that  the stderr file is to be
       shown rather than the stdout file.  The  _-_o  switch  indi-
       cates  that  the  stdout file is to be shown.  This is the
       default.  The _-_v switch will list the version of the file.
       All other switches will print a indication of the usage of
       the program.

BBUUGGSS
       One cannot see the output of a job on a remote machine.

SSEEEE AALLSSOO
       qsub(1), qstat(1).
       qmgr(1M) in the _N_P_S_N _U_N_I_X _S_y_s_t_e_m  _A_d_m_i_n_i_s_t_r_a_t_o_r  _R_e_f_e_r_e_n_c_e
       _M_a_n_u_a_l.

NNPPSSNN HHIISSTTOORRYY
       Origin: Monsanto

       _J_u_n_e _1_9_9_2 _- _J_o_h_n _R_o_m_a_n_, _M_o_n_s_a_n_t_o _C_o_m_p_a_n_y
       Original release.
       Release 3.20.









                                                                1


