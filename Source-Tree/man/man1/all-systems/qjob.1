.TH QJOB 1
\"
\"	Network Queueing System (NQS)
\"  This version of NQS is Copyright (C) 1992  John Roman
\"
\"  This program is free software; you can redistribute it and/or modify
\"  it under the terms of the GNU General Public License as published by
\"  the Free Software Foundation; either version 1, or (at your option)
\"  any later version.
\"
\"  This program is distributed in the hope that it will be useful,
\"  but WITHOUT ANY WARRANTY; without even the implied warranty of
\"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\"  GNU General Public License for more details.
\"
\"  You should have received a copy of the GNU General Public License
\"  along with this program; if not, write to the Free Software
\"  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
\"
.SH NAME
qjob \- display status of \s-1NQS\s+1 queues in a networked environment
.SH SYNOPSIS
.B qjob
.SH DESCRIPTION
.I Qjob\^
displays the status of queues known to the Network Queueing System
(\s-1NQS\s+1) as remote ones. The
.I /etc/batchservers\^
file should contain host names for
machines
.I currently\^
in connection with the local host via pipe queues.
Each entry consists of a line specifying
the name of the machine. This file is normally created and maintained by
NQS queue managers.
.PP
The current state of each NQS queue in the list of hosts is displayed.
For each selected queue,
.I qjob\^
displays a
.I queue header\^
(information about the queue itself) followed by information
about requests in the queue.
.I qjob\^
shows only those  requests  belonging  to  the  invoker.
.PP
The
.I queue header\^
always includes the queue-name, queue type,
queue status (see below), an indication of whether or not the queue is
.I pipeonly\^
(accepts requests from pipe queues only),
and the number of requests in the queue.
.PP
.I Qjob\^
displays the following information about
a request: the
.IR request-name\^ ,
the
.IR request-id\^ ,
the owner, the relative request priority, and
the current request state (see below).
For running requests, the process group is also shown,
as soon as this information becomes
available to the local \s-1NQS\s+1 daemon.
.PP
It must be understood that the relative ordering of requests within a queue
does not always determine the order in which the requests will be run.
The \s-1NQS\s+1 request scheduler is allowed to make exceptions to the request
ordering for the sake of efficient machine resource usage.  However, requests
appearing near the beginning of the queue have higher
priority than requests appearing later, and will usually be run before
requests appearing later on in the queue.
.SH QUEUE STATE
The general state of a queue is defined by two principal properties of
the queue.
.PP
The first property determines whether or not requests can be submitted to
the queue.  If they can, then the queue is said to be
.IR "enabled\^" .
Otherwise the queue is said to be
.IR "disabled\^" .
One of the words \f3\s-1CLOSED\s+1\fR, \f3\s-1ENABLED\s+1\fR, or
\f3\s-1DISABLED\s+1\fR will appear
in the queue status field to indicate the respective queue
states of: enabled (with no local \s-1NQS\s+1 daemon), enabled (and local
\s-1NQS\s+1 daemon
is present), and disabled.  Requests can only be submitted to the queue if
the queue is enabled, and the local \s-1NQS\s+1 daemon is present.
.PP
The second principal property of a queue determines if requests which are
ready to run, but are not now presently running, will be allowed to run
upon the completion of any currently running requests, and whether any requests
are presently running in the queue.
.PP
If queued requests not already running are blocked from running, and no
requests are presently executing in the queue, then the queue is said to be
.IR "stopped\^" .
If the same situation exists with the difference that at least one request
is running, then the queue is said to be
.IR "stopping\^" ,
where the requests presently executing will be allowed to complete
execution, but no new requests will be spawned.
.PP
If queued requests ready to run are only prevented from doing so by the
\s-1NQS\s+1 request scheduler, and one or more requests are presently running
in the queue, then the queue is said to be
.IR "running\^" .
If the same circumstances prevail with the exception that no requests are
presently running in the queue, then the queue is said to be
.IR "inactive\^" .
Finally, if the \s-1NQS\s+1 daemon for the local host upon which the queue
resides is not running, but the queue would otherwise be in the state
of
.I running\^
or
.IR "inactive\^" ,
then the queue is said to be
.IR "shutdown\^" .
The queue states describing the second principal property of a queue
are therefore respectively displayed
as \f3\s-1STOPPED\s+1\fR, \f3\s-1STOPPING\s+1\fR, \f3\s-1RUNNING\s+1\fR,
\f3\s-1INACTIVE\s+1\fR, and \f3\s-1SHUTDOWN\s+1\fR.
.SH REQUEST STATE
The state of a request may be
.IR arriving\^ ,
.IR holding\^ ,
.IR waiting\^ ,
.IR queued\^ ,
.IR staging\^ ,
.IR routing\^ ,
.IR running\^ ,
.IR departing\^ ,
or
.IR exiting\^ .
A request is said to be
.I arriving\^
if it is being enqueued from a remote host.
.I Holding\^
indicates that the request is presently prevented from entering any other
state (including the
.I running\^
state), because a
.I hold\^
has been placed on the request.
A request is said to be
.I waiting\^
if it was submitted with the constraint that it not run before a certain
date and time, and that date and time have not yet arrived.
.I Queued\^
requests are eligible to proceed (by
.I routing\^
or
.IR running\^ ).
When a request reaches the head of a pipe queue and receives
service there, it is
.IR routing\^ .
A request is
.I departing\^
from the time the pipe queue turns to other work until the request has
arrived intact at its destination.
.I "Staging\^"
denotes a
.I batch\^
request that has not yet begun execution, but for which input files are
being brought on to the execution machine.
A
.I running\^
request has reached its final destination queue, and is actually executing.
Finally,
.I "exiting\^"
describes a batch request that has completed execution, and will exit from
the system after the required output files have been returned (to possibly
remote machines).
.PP
Imagine a batch request originating on a workstation,
destined for the batch queue of a computation engine,
to be run immediately.
That request would first go through the states
.IR queued\^ ,
.IR routing\^ ,
and
.I departing\^
in a local pipe queue.
Then it would disappear from the pipe queue.
From the point of view of a queue on the computation engine,
the request would first be
.IR arriving\^ ,
then
.IR queued\^ ,
.I staging\^
(if required by the batch request),
.IR running\^ ,
and finally
.IR exiting\^ .
Upon completion of the
.I exiting\^
phase of execution, the batch request would disappear from the batch queue.
.SH "CAVEATS"
\s-1NQS\s+1 is not finished, and continues to undergo development.
Some of the request states shown above may or may not be supported
in your version of NQS.
.SH "SEE ALSO"
qstat(1)
.SH "NPSN HISTORY"
Origin: CERN
.PP
.I "August 1991 \- Christian Boissat, CERN"
.br
Original release.
