.TH QPR 1
\"
\"	Network Queueing System (NQS)
\"  This version of NQS is Copyright (C) 1992  John Roman
\"
\"  This program is free software; you can redistribute it and/or modify
\"  it under the terms of the GNU General Public License as published by
\"  the Free Software Foundation; either version 1, or (at your option)
\"  any later version.
\"
\"  This program is distributed in the hope that it will be useful,
\"  but WITHOUT ANY WARRANTY; without even the implied warranty of
\"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\"  GNU General Public License for more details.
\"
\"  You should have received a copy of the GNU General Public License
\"  along with this program; if not, write to the Free Software
\"  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
\"
.SH NAME
qpr \- submit a hardcopy print request to \s-1NQS\s+1
.SH SYNOPSIS
.B qpr
.RB [ \-a " date-time ]"
.RB [ \-f " form-name ]"
.RB [ \-mb ]
.RB [ \-me ]
.br
.RB [ \-mu " user-name ]"
.RB [ \-n " number-of-copies ]"
.RB [ \-p " priority ]"
.br
.RB [ \-q " queue-name ]"
.RB [ \-r " request-name ]"
.RB [ \-z ]
[ files ]
.br
.SH DESCRIPTION
.I Qpr\^
places the named files in a
.I Network Queueing System\^
(\s-1NQS\s+1)
queue to be printed by a device such as a line printer
or laser printer.
If no files are specified,
.I qpr\^
will read from the standard input.
.PP
In the absence of the
.B \-z
flag,
.I qpr\^
will print a
.I request-id\^
on the standard
output, upon successful queueing of a request.  This
.I request-id\^
can be compared with
what is reported by
.IR qdev\^ (1)
and
.IR qstat\^ (1)
to find out what happened to a request, and given
as an argument to
.IR qdel\^ (1)
to delete a request.
A
.I request-id\^
is always of the form:
.I "seqno\^.hostname\^"
where
.I seqno\^
refers to the sequence number assigned to the request by \s-1NQS\s+1, and
.I hostname\^
refers to the name of originating local machine.
This identifier is used throughout \s-1NQS\s+1 to uniquely identify the
request, no matter where it is in the network.
.PP
The following options
to
.I qpr\^
may appear
in any order
and may be intermixed
with file names.
.TP "\w'\-n\|number\ \ 'u"
.BI \-a " date-time\^"
Submit at the specified date and/or time.
In the absence of
this flag,
.I qpr\^
will submit the request immediately.
.IP
If a
.I "date-time\^"
specification is comprised
of two or more tokens separated by whitespace characters, then the
.I "date-time\^"
specification must be placed within double quotes as in:
.B \-a
\f2"July, 4, 2026 12:31-EDT\^"\fP, or otherwise escaped such that the
shell will interpret the entire
.I "date-time\^"
specification as a single lexical token.
.IP
The syntax accepted for the 
.I "date-time\^"
parameter is relatively flexible.
Unspecified date and time values default to an appropriate value (e.g.
if no date is specified, then the current month, day, and year are
assumed).
.IP
A date can be specified as a month and day (current year assumed).
The year can also be explicitly specified.  It is also possible to
specify the date as a weekday name (e.g. "Tues"), or as one of the
strings "today" or "tomorrow".  Weekday names and month names can be
abbreviated by any three character (or longer) prefix of the actual
name.  An optional period can follow an abbreviated month or day name.
.IP
Time of day specifications can be given using a twenty-four hour clock,
or "am" and "pm" specifications may be used alternatively.  In the
absence of a meridian specification, a twenty-four hour clock is assumed.
.IP
It should be noted that the time of day specification is interpreted
using the precise meridian definitions whereby "12am" refers to the
twenty-four hour clock time of 0:00:00, "12m" refers to noon, and
"12-pm" refers to 24:00:00.  Alternatively, the phrases "midnight" and
"noon" are accepted as time of day specifications, where "midnight"
refers to the time of 24:00:00.
.IP
A timezone may also appear at any point in the
.I "date-time\^"
specification.
Thus, it is legal to say: "April 1, 1987 13:01-PDT".  In the absence
of a timezone specification, the local timezone is assumed, with daylight
savings time being inferred when appropriate, based on the date specified.
.IP
All alphabetic comparisons are performed in a case insensitive fashion
such that both "WeD" and "weD" refer to the day of Wednesday.
.IP
Some valid
.I "date-time\^"
examples are:
.sp 1
.RS
.RS
01-Jan-1986 12am, PDT
.br
Tuesday, 23:00:00
.br
11pm tues.
.br
tomorrow 23-MST
.RE
.RE
.TP
.BI \-f " form-name\^"
Limit the set of acceptable devices to those devices which are loaded
with the forms:
.IR "form-name\^" .
In the absence of this flag,
.I "qpr\^"
will submit the request only to a device that is loaded with the
.I default\^
forms.
If there is no
.I default\^
forms defined, the request will be submitted
to the appropriate output device without regard to the forms configured
for the device.
.IP
In any case, only those devices associated
with the chosen queue will be considered.
.TP
.B \-mb
Send mail to the user on the originating machine
when the request begins execution.
If the
.B \-mu
flag is also present, then mail is sent to the user specified for the
.B \-mu
flag instead of to the invoking user.
.TP
.B \-me
Send mail to the invoker on the originating machine when the request
has ended execution.
If the
.B \-mu
flag is also present, then mail is sent to the user specified for the
.B \-mu
flag instead of to the invoking user.
.TP
.BI \-mu " user-name\^"
Specify that any mail concerning the request should be delivered to the user
.IR user-name\^ .
.I User-name\^
may be formatted either as
.I user\^
(containing no `@' characters),
or as
.IR user@machine\^ .
In the absence of this flag, any mail concerning the request will
be sent to the invoker on the originating machine.
.TP
.BI \-n " number-of-copies\^"
Print
.I number-of-copies\^
copies.
The default is one.
.TP
.BI \-p " priority\^"
Assign an intra-queue priority to this request.
The specified
.I priority\^
must be an integer, and must be in the range [0..63], inclusive.
A value of 63 defines the highest
.I intra-queue\^
request priority, while a value of 0 defines the lowest.
This priority does
.B not
determine the execution priority of the request.  This priority is only used to
determine the relative ordering of requests within a queue. 
.IP
When a request is added to a queue, it is placed at a specific position
within the queue such that it appears
ahead of all existing requests whose priority is less than the priority of
the new request.  Similarly, all requests with a higher priority will remain
ahead of the new request when the queueing process is complete.
When the priority of the new request is equal to the priority
of an existing request, the existing request takes precedence over the
new request.
.IP
If no
.I intra-queue\^
priority is chosen by the user, then \s-1NQS\s+1 assigns a default value.
.TP
.BI \-q " queue-name\^"
Specify the queue to which the device request is to be submitted.  If no
.BI \-q " queue-name\^"
specification is given, then the user's environment variable set is searched
for the variable: \f3\s-1QPR_QUEUE\s+1\fP.  If this environment variable is
found, then the character string value for \f3\s-1QPR_QUEUE\s+1\fP is
presumed to name the queue to which the request should be submitted.  If
the \f3\s-1QPR_QUEUE\s+1\fP environment variable is not found, then the
request will be submitted to the default device request queue,
.I "if\^"
defined by the local system administrator.
Otherwise, the request cannot be queued,
and an appropriate error message is displayed to this effect.
.TP
.BI \-r " request-name\^"
Assign a name to this request.
In the absence of an explict
.BI \-r " request-name\^"
specification, the
.I "request-name\^"
defaults to the name of the
first print file
(leading path name removed) specified on the command line.
If no
print files were
specified, then the default
.I "request-name\^"
assigned to the request is
.BR "STDIN" .
.IP
In all cases, if the
.I "request-name\^"
is found to begin with a digit, then the character 'R' is pre-pended
to prevent a
.I "request-name\^"
from beginning with a digit.
All
.I "request-names\^"
are truncated to a maximum length of 15 characters.
.IP
Be sure not to confuse
.I "request-name\^"
with
.IR "request-id\^" .
.TP
.B \-z
Submit the request silently.
If the request is submitted successfully, nothing will
be written to stdout or stderr.
.SH QUEUE ACCESS
\s-1NQS\s+1 supports queue access restrictions.  For each
queue of queue type other than
.IR network\^ ,
access may be either
.I unrestricted\^
or
.IR restricted\^ .
If access is
.IR unrestricted\^ ,
any request may enter the queue.
If access is
.IR restricted\^ ,
a request can only enter the queue if the requester or the
requester's login group has been given access to that queue
(see \f2qmgr\^\fP(1M)).
Requests submitted by root are an exception; they are always
queued, even if root has not explicitly been given access.
.PP
Use \f2qstat\^\fP(1) to determine who has access to a particular queue.
.SH "SEE ALSO"
mail(1), qdel(1), qdev(1), qlimit(1), qstat(1), and qsub(1)
.br
in the \f2\s-1NPSN\s+1 \s-1UNIX\s+1 System Programmer Reference Manual\fR.
.br
qmgr(1M) in the
\f2\s-1NPSN\s+1 \s-1UNIX\s+1 System Administrator Reference Manual\fR.
.SH "NPSN HISTORY"
Origin: Sterling Software Incorporated
.PP
.I "May 1986 \- Robert Sandstrom, Sterling Software"
.br
Original release.
