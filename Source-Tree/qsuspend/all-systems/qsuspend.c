/*
 * qsuspend/qsuspend.c
 * 
 * DESCRIPTION:
 *
 *	Suspend or resume a running request.
 *
 * RETURNS:
 *      0    -  all requests suspended/resumed succesfully
 *      -1   -  error in calling sequence
 *      n    -  number of requests not suspended/resumed
 *
 *      In all error cases, messages are sent to the standard output file.
 *
 *	Author:
 *	-------
 *	John Roman, Monsanto Company.
 *	March 20, 1992.
 */

#define MAX_USRRETTIM	15		/* Maximum user retry times */
#define	MAX_REQS	100		/* Maximum number of reqs that */
					/* can be suspended/resumed at */
					/* a time. */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>			/* Network database header file; */
#include <libnqs/nqsdirs.h>		/* NQS files and directories */
#include <libnqs/transactcc.h>		/* Transaction completion codes */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <SETUP/autoconf.h>
#include <unistd.h>

static void qsuspend_cleanup ( int sig );
static void qsuspend_showhow ( void );
static void selrequest (struct qentry* qentry, struct gendescr *que_descr); 
static void show_version ( void );
static void suspendallreq ( void );

/*
 *	Global variables:
 */

int   SUSPEND;				/* True if suspend, false if resume */
int   DEBUG;				/* DEBUG flag */
char *Qsuspend_prefix;			/* Pointer to string of type of action*/
Mid_t Locmid;                           /* Caller's host machine-id */
char *chostname;                        /* Caller's host machine name */
uid_t cuid;                             /* Caller's userid */
char *cusername;                        /* Caller's username */
char *whom;                             /* Whom we are interested in */
struct passwd *whompw;                  /* Password entry of "whom" */
int Mgr_priv;                           /* NQS manager privilege bits */
int monsanto_header;			/* Apparently we call some sho_ rtn */

/*** main
 *
 *
 *	qsuspend [-a] [-u username] [-v] <request-ids>
 *	qresume [-a] [-u username] [-v] <request-ids>
 */
int main (int argc, char *argv[])
{

        int n_errs;                     /* Number of requests not processed. */
	int n_reqs;			/* Number of reqs to suspend/resume. */
	struct {
		long orig_seqno;	/* Sequence# for req */
		Mid_t machine_id;	/* Machine-id of originating machine */
		Mid_t target_mid;	/* target mid */
	} reqs [MAX_REQS];		/* Reqs to suspend/resume */
	char **scan_reqs;		/* Scan reqs */
	char *argument;			/* Ptr to cmd line arg text */
	char *cp;			/* Scanning character ptr */
        struct passwd *passwd;          /* Pointer to password info */
	char	buffer[64];
	int	suspendall;		/* Flag true to suspend all */
	char *root_dir;                 /* Fully qualified file name */

  	sal_debug_InteractiveInit(1, NULL);
  
	/*
	 *  Catch 4 common household signals:  SIGINT, SIGQUIT,SIGHUP, 
	 *  and SIGTERM.  This is quite important, because we do not want
	 *  to leave useless inter-process communication files hanging
	 *  around in the NQS directory hierarchy.  However, if we do,
	 *  it is not fatal, it will eventually be cleaned up by NQS.
	 */
	signal (SIGINT, qsuspend_cleanup);
	signal (SIGQUIT, qsuspend_cleanup);
	signal (SIGHUP, qsuspend_cleanup);
	signal (SIGTERM, qsuspend_cleanup);
	/*
	 * Check to see if this is a suspend or resume.
	 */
	cp = strrchr(argv[0], '/');	/* Find last slash */
	if (cp != NULL) cp++;		/* Get to start of binary name */
	else (cp = argv[0]);		/* Otherwise use entire name */
	SUSPEND = 0;			/* Assume is a resume */
	Qsuspend_prefix = "Qresume";
	if ( !(strcmp( cp, "qsuspend")) || !(strcmp (cp, "xqsuspend")) ) {
	    SUSPEND = 1;
	    Qsuspend_prefix = "Qsuspend";
	}
        /*
         * Check to see if we want to print some debugging information.
         */
        DEBUG = 0;
        cp = getenv ("NQS_DEBUG");
        if ( (cp != NULL) && (!strcmp(cp, "yes") ) ) {
            DEBUG = 1;
            fprintf( stderr, "NQS version %s\n", NQS_VERSION);
        }
	if ( ! buildenv()) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qsuspend_prefix);
	    fprintf (stderr, "establish directory independent ");
	    fprintf (stderr, "environment.\n");
	    exit (1);
	}

	root_dir = getfilnam (Nqs_root, SPOOLDIR);
	if (root_dir == (char *)NULL) {
	    fprintf (stderr, "%s(FATAL): Unable to ", Qsuspend_prefix);
	    fprintf (stderr, "determine root directory name.\n");
	    exit (1);
	}
	if (chdir (root_dir) == -1) {
	    fprintf (stderr, "%s(FATAL): Unable to chdir() to the NQS ",
                 Qsuspend_prefix);
	    fprintf (stderr, "root directory.\n");
	    relfilnam (root_dir);
	    exit (1);
	}
	relfilnam (root_dir);
	/*
	 *  On systems with named pipes, we get a pipe to the local
	 *  daemon automatically the first time we call inter().
	 */
#if	HAS_BSD_PIPE
	if (interconn () < 0) {
	    fprintf (stderr, "%s(FATAL): Unable to get ", Qsuspend_prefix);
	    fprintf (stderr, "a pipe to the local daemon.\n");
	    exit (-1);
	}
#endif
        if (localmid (&Locmid) != 0) {
            fprintf (stderr, "Unable to get machine-id of local host.\n");
            exit(-1);
        }
        if ((chostname = nmap_get_nam (Locmid)) == (char *) 0)    {
            fprintf (stderr, "Unable to determine name of local host.\n");
            exit (-1);
        }

        cuid = getuid();
        if ((passwd = sal_fetchpwuid (cuid)) == (struct passwd *) 0) {
            fprintf (stderr, "Unable to determine caller's username\n");
            exit (-1);
        }
	sal_closepwdb();
        cusername = passwd->pw_name;
        Mgr_priv = nqspriv (cuid, Locmid, Locmid);

	suspendall = 0;			/* By default, do not suspend/etc. all*/
	whom = NULL;			/* No -u flag seen */
	while (*++argv != NULL && **argv == '-') {
	    argument = *argv;
	    switch (*++argument) {
	    case 'a':
		suspendall++;
		break;
	    case 'v':
		show_version();
		break;
	    case 'u':		/* User-name specification */
		if (*++argv == NULL) {
		    fprintf (stderr, "Missing username.\n");
		    exit (-1);
		}
		if (whom != NULL) {
		    fprintf (stderr, "Multiple -u specifications.\n");
		    exit (-1);
		}
		whom = *argv;
		break;
	    default:
		fprintf (stderr, "Invalid option flag specified.\n");
		qsuspend_showhow();
	    }
	}
        if (whom == NULL ) {
	    strcpy(buffer, cusername);
	    whom = buffer;
	}
        whompw = sal_fetchpwnam (whom);

	if (!suspendall &&(*argv == NULL)) {
	    /*
	     *  No request-ids were specified.
	     */
	    fprintf (stderr, "No request-id(s) specified.\n");
	    qsuspend_showhow();
	}
	if (suspendall) {
	    suspendallreq();
	    exit (0);
	}
	/*
	 *  Build the set of reqs to be suspended/resumed.
	 */
	n_reqs = 0;			/* #of reqs to suspend/resume */
	scan_reqs = argv;		/* Set req scan pointer */
	while (*scan_reqs != NULL &&	/* Loop to delete reqs */
	   n_reqs < MAX_REQS) {
	    switch (reqspec (*scan_reqs, &reqs [n_reqs].orig_seqno,
				 &reqs [n_reqs].machine_id,
				 &reqs [n_reqs].target_mid)) {
	    case -1:
		fprintf (stderr, "Invalid request-id syntax ");
		fprintf (stderr, "for request-id: %s.\n", *scan_reqs);
		exit (-1);
	    case -2:
		fprintf (stderr, "Unknown machine for ");
		fprintf (stderr, "request-id: %s.\n",
				*scan_reqs);
		exit (-1);
	    case -3:
		fprintf (stderr, "Network mapping database ");
		fprintf (stderr, "inaccessible.  Seek staff support.\n");
			exit (-1);
	    case -4:
		fprintf (stderr, "Network mapping database ");
		fprintf (stderr, "error when parsing ");
		fprintf (stderr, "request-id: %s.\n",
				*scan_reqs);
		fprintf (stderr, "Seek staff support.\n");
		exit (-1);
	    }
	    /* If reqspec returns null in machine id, force to
	     * local machine id.
	     */
	    if (reqs[n_reqs].machine_id == 0) 
			localmid(&reqs [n_reqs].machine_id);
	    scan_reqs++;		/* One more req */
	    n_reqs++;
	}
	if (*scan_reqs != NULL) {
	    /*
	     *  Too many reqs were specified to be suspended/etc.
	     */
	    fprintf (stderr, "Too many requests given to ");
	    if (SUSPEND) fprintf (stderr, "suspend.\n");
	    else fprintf (stderr, "resume.\n");
	    exit (-1);
	}
	/*
	 *  Now that everything has been parsed and legitimized,
	 *  take care of the specified set of requests.
	 */
        n_errs = 0;
	n_reqs = 0;
	while (*argv != NULL) {		/* Loop to delete reqs */
            if (whompw == NULL &&  ((reqs[n_reqs].target_mid == (Mid_t) 0)||
                       	(reqs[n_reqs].target_mid == Locmid))) {
                fprintf (stderr, "Unknown user %s on local host.\n", whom);
                fflush (stderr);
            } else {
		if (reqs[n_reqs].target_mid != Locmid) {
		    if (SUSPEND) fprintf (stderr, " Cannot suspend");
		    else fprintf (stderr, "Cannot resume");
		    fprintf (stderr, " remote request %s.\n", *argv);
		} else {
		    diagqsuspend (suspendreq (
			whompw->pw_uid,		/* Whom to do */
		 	reqs[n_reqs].orig_seqno,/* Request */
                       	reqs[n_reqs].machine_id, 
			reqs[n_reqs].target_mid, 
                       	Mgr_priv, 		/* Our privs */
			SUSPEND),		/* What to do */
			*argv);			/* Text to print */
		}
	    	argv++;			/* One more req */
	        n_reqs++;
	    }
	}
	exiting();			/* Delete our comm. file */
	exit (n_reqs);
}

/*** qsuspend_cleanup 
 * 
 * 
 *	Catch certain signals, and delete the inter-process
 *	communication file we have been using.
 */
static void qsuspend_cleanup (int sig)
{
	signal (sig, SIG_IGN);		/* Ignore multiple signals */
	exiting();			/* Delete our comm. file */
}

/*** qsuspend_showhow
 *
 *
 *	qsuspend_showhow():
 *	Show how to use this command.
 */
static void qsuspend_showhow(void)
{
    if (SUSPEND) {
	fprintf (stderr, "qsuspend -- suspend running NQS requests\n");
	fprintf(stderr, "qsuspend [-a] [-u username] [-v] <request-ids>\n");
	fprintf (stderr, " -a            suspend all requests of user\n");
	fprintf (stderr, " -u username   username of request owner (if not yourself)\n");
	fprintf (stderr, " -v            print version information\n");
    } else {
	fprintf (stderr, "qresume -- resume suspended NQS requests\n");
	fprintf(stderr, "qresume [-a] [-u username] [-v] <request-ids>\n");
	fprintf (stderr, " -a            resume all requests of user\n");
	fprintf (stderr, " -u username   username of request owner (if not yourself)\n");
	fprintf (stderr, " -v            print version information\n");
	}
    exit (0);
}

static void show_version(void)
{
	fprintf (stderr, "NQS version is %s.\n", NQS_VERSION);
}
/*
 * Suspend (or resume) all possible requests.
 */
static void suspendallreq (void)
{
	struct confd *queuefile;  
        struct gendescr *que_descr;
        int fd;                         /* Queue ordering file descriptor */
        struct qentry cache [QOFILE_CACHESIZ];
                                        /* Queue ordering file cache buffer */
        int cacheindex;                 /* Current buffer cache index */
        int cachesize;                  /* Number of queue entries in read */
                                        /* cache buffer */
	int i;
	
	if ((queuefile = opendb (Nqs_queues, O_RDONLY)) == NULL) {
                fprintf (stderr, "%s(FATAL): Unable to open the NQS queue ",
                         Qsuspend_prefix);
                fprintf (stderr, "definition database file.\n");
                exit (-1);
        }
	que_descr = nextdb (queuefile);
        while (que_descr != (struct gendescr *)0) {
            fd = openqord (queuefile, que_descr);
            /*
             *  fd >= 0 if the queue has requests, and we successfully
             *          opened the queue ordering file.
             *  fd = -1 if the queue has no requests.
             *  fd = -2 if the queue has requests, but an error prevented
             *          us from opening the queue ordering file for
             *          the queue.
             *  fd = -3 if the queue was deleted.
             */
            if (fd < -1) break;       /* Error or queue was deleted */
	    cachesize = 0;                          /* Mark read cache as invalid*/
	    cacheindex = 0;
	    lseek (fd, (long) (que_descr->v.que.departcount *
                        sizeof (struct qentry)), 0);
            /*
             * Find running requests.
             */
            for (i = 0; i < que_descr->v.que.runcount; i++) {
                if (cacheindex >= cachesize) {
                    cachesize = read (fd, (char *) cache,
                                                  sizeof (cache));
                    cachesize /= sizeof (struct qentry);
                    cacheindex = 0;
                }

                selrequest (&cache [cacheindex], que_descr);
		cacheindex++;
            }
            que_descr = nextdb (queuefile);  /* Get the next queue */
	}

}

static void selrequest ( 
	struct qentry *qentry,          /* Queue entry describing request */
	struct gendescr *que_descr)     /* Queue file queue-descriptor */
{
	struct rawreq rawreq;
	int	cfd;
	char	fake_argv[64];
	
	/*
	 * If this request is owned by me, or I am a manager, then I can 
	 * affect this request.
	 */
	if ( (qentry->uid != whompw->pw_uid) && !(Mgr_priv & QMGR_MGR_PRIV) ) return;
	cfd = getreq ( (long) qentry->orig_seqno,  qentry->orig_mid, &rawreq);
	if (cfd == -1) {
            fprintf (stderr, "%s(FATAL): Unable to open raw request for ",
                         Qsuspend_prefix);
            fprintf (stderr, "request %ld.\n", qentry->orig_seqno);
            exit (-1);	    
	}
	if (qentry->orig_mid == Locmid)
		sprintf(fake_argv, "%ld", qentry->orig_seqno);
	else
		sprintf(fake_argv, "%ld.%s", qentry->orig_seqno, 
				getmacnam(qentry->orig_mid) );
	    
	if ( SUSPEND ) {
	    if (rawreq.flags & RQF_SUSPENDED) {
		printf("Request %s is already suspended.\n",  fake_argv);
		return;
	    }
	}
	if ( !SUSPEND ) {
	    if (!(rawreq.flags & RQF_SUSPENDED)) {
		printf("Request %s is already running.\n", fake_argv);
		return;
	    }
	}
        diagqsuspend (suspendreq (
			whompw->pw_uid,		/* Whom to do */
		 	qentry->orig_seqno,	/* Request */
                       	qentry->orig_mid,	/* originating mid */
			0,			/* local machine */
                       	Mgr_priv, 		/* Our privs */
			SUSPEND),		/* What to do */
			fake_argv);		/* Text to print */
	    
}
