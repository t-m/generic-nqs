/*
 * qstat/qstat.c
 * 
 * DESCRIPTION:
 *
 *	Display queue status information.
 *	This program must run as a setuid program.
 *
 *	Original Author:
 *	-------
 *	Brent A. Kingsbury, Sterling Software Incorporated.
 *	August 12, 1985.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdlib.h>
#include <libnqs/nqsvars.h>			/* NQS global variables */
#include <libsal/license.h>
#include <libsal/proto.h>
#include <unistd.h>
#include <SETUP/autoconf.h>

static void sho_version ( void );
static void showhow ( void );

/*
 *	Global variables:
 */
int  monsanto_header = 0;
char *Qstat_prefix = "Qstat";
int DEBUG;				/* Debug flag for interactive pgms. */

/*** main
 *
 *
 *      qstat [ -a ]            all users jobs
 *            [ -d ]            jobs in the local nqs domain
 *	      [ -h ]		force header with monsanto style
 *            [ -l ]            long format
 *            [ -o ]            originating from this node
 *            [ -s ]            original standard format
 *            [ -u username ]   particular user's requests
 *            [ -v ]            version
 *            [ <queue-name(s)> ]
 *
 *      Default is monsanto standard format for only the current user
 *      on the local machine.
 *
 *  To get information about queues:
 *
 *      qstat -x
 *            [ -b ]            Brief format
 *            [ -l ]            Long format
 *
 *
 *  To get information about complexes:
 *
 *      qstat -c
 *            [ -b ]            Brief format
 *            [ -l ]            Long format
 */
int main (int argc, char *argv[])
{
	struct confd *queuefile;	/* Queue description file */
	struct confd *qcomplexfile;	/* Queue complex description file */
	struct confd *qmapfile;		/* Queue/device/destination mapping */
					/* file */
	struct confd *pipeqfile;	/* Pipe queue destination file */
	long flags;
	char *argument;
	short daepresent;		/* Boolean non-zero if the local */
					/* NQS daemon is present and running */
	char *whom;			/* Whom we are interested in */
	struct passwd *whompw;		/* Password entry of "whom" */
	uid_t whomuid;			/* Uid of whom we are interested in */
	struct reqset *reqset_ptr;	/* pointer to the request set */
	struct reqset reqset;		/* request set we are interested in */
	Mid_t	mymid;
	char	*root_dir;
	int	exit_status;		/* Status at exit */

  	sal_debug_InteractiveInit(1, NULL);
  
        if ( ! buildenv()) {
                fprintf (stderr, "%s(FATAL): Unable to establish directory ", 
			Qstat_prefix);
                fprintf (stderr, "independent environment.\n");
		exit(1);
        }
	root_dir = getfilnam (Nqs_root,  SPOOLDIR);
	if (root_dir == (char *) NULL) {
		fprintf (stderr, "%s(FATAL): Unable to determine the NQS ",
			 Qstat_prefix);
		fprintf (stderr, "root directory.\n");
		exit (1);
	}
	if (chdir (root_dir) == -1) {
		fprintf (stderr, "%s(FATAL): Unable to chdir() to the NQS ",
			 Qstat_prefix);
		fprintf (stderr, "root directory.\n");
		exit (1);
	}
	relfilnam (root_dir);
	if ((queuefile = opendb (Nqs_queues, O_RDONLY)) == NULL) {
		fprintf (stderr, "%s(FATAL): Unable to open the NQS queue ",
			 Qstat_prefix);
		fprintf (stderr, "definition database file.\n");
		exit (1);
	}
	if ((qmapfile = opendb (Nqs_qmaps, O_RDONLY)) == NULL) {
		fprintf (stderr, "%s(FATAL): Unable to open the NQS queue/",
			 Qstat_prefix);
		fprintf (stderr, "device/destination\n");
		fprintf (stderr, "%s(FATAL): mapping database file.\n",
			 Qstat_prefix);
		exit (1);
	}
	if ((pipeqfile = opendb (Nqs_pipeto, O_RDONLY)) == NULL) {
		fprintf (stderr, "%s(FATAL): Unable to open the NQS pipe-",
			 Qstat_prefix);
		fprintf (stderr, "queue destination database file.\n");
		exit (1);
	}
	if ((qcomplexfile = opendb (Nqs_qcomplex, O_RDONLY)) == NULL) {
		fprintf (stderr, "%s(FATAL): Unable to open the NQS queue ",
			 Qstat_prefix);
		fprintf (stderr, "complex definition database file.\n");
		exit (1);
	}
	/*
	 *  Determine if the local NQS daemon is present and running, and
	 *  set the daepresent boolean flag accordingly.
	 */
#if	HAS_BSD_PIPE
	daepresent = daepres (queuefile);
#else
	daepresent = daepres();
#endif
	reqset_ptr = NULL;
	reqset.v.reqid.orig_mid = 0;
	reqset.v.reqid.orig_seqno = 0;
	reqset.selecttype = 0;
	reqset.next = NULL;
	whomuid = (uid_t)NULL;			/* Start with no one */
	flags = SHO_R_MONSANTO;		/* Assume short request format */
	while (*++argv != NULL && **argv == '-') {
	    argument = *argv;
	    while (*++argument != '\0') {
	        switch (*argument) {
		case 'a':
		    flags |= SHO_R_ALLUID;
		    break;
                case 'b':
                    flags |= SHO_R_BRIEF;
                    break;
                case 'c':
                    flags |= SHO_R_COMPLEX;
		    flags &= ~(SHO_R_STANDARD | SHO_R_MONSANTO);
                    break;
                case 'd':
                    flags |= SHO_R_DEST;
                    break;
                case 'h':
                    flags |= SHO_R_HEADER;
                    break;
		case 'l':
		    flags |= SHO_R_LONG;
		    flags &= ~(SHO_R_STANDARD | SHO_R_MONSANTO);
		    break;
                case 'm':	/* no-op -- synonym for default */
                    break;
                case 'o':
                    flags |= SHO_R_ORIGIN;
		    localmid(&mymid);
	            reqset.v.reqid.orig_mid = mymid;
		    reqset.v.reqid.orig_seqno = 0;
		    reqset.selecttype = SEL_REQID;
		    reqset.next = NULL;
		    reqset_ptr = &reqset;
                    break;
                case 's':
                    flags |= SHO_R_STANDARD;
                    flags &= ~(SHO_R_MONSANTO);
                    break;
		case 'u':
		    whom = *++argv;
		    if (whom == NULL) {
		        fprintf (stderr, "User name required ");
		        fprintf (stderr, "after -u.\n");
		        exit (5);
		    }
		    if ((whompw = sal_fetchpwnam (whom)) ==
				        (struct passwd *) 0) {
		        fprintf (stderr, "Unknown user %s.\n", whom);
		        exit (6);
		    }
		    whomuid = whompw->pw_uid;
		    break;
                case 'v':
                    sho_version();
		    exit(0);
                    break;
		case 'x':
		    flags |= (SHO_R_QUEUE);
		    flags &= ~(SHO_R_STANDARD | SHO_R_MONSANTO);
		    break;
	        default:
		    fprintf (stderr, "Invalid option flag ");
		    fprintf (stderr, "specified.\n");
		    showhow();
		    exit (7);
		}
	    }
	}
	if (flags & SHO_R_QUEUE ) {
		if ( (flags & SHO_R_ALLUID) || (flags & SHO_R_ORIGIN) ||
			(flags & SHO_R_STANDARD) || (flags & SHO_R_HEADER) ||
			(whomuid != (uid_t)NULL) ) {
			fprintf(stderr, "Incompatable switch with -x.\n");
			exit(1);
		}
	}
	if (flags & SHO_R_COMPLEX ) {
		if ( (flags & SHO_R_ALLUID) || (flags & SHO_R_ORIGIN) ||
			(flags & SHO_R_STANDARD) || (flags & SHO_R_HEADER) ||
			(whomuid != (uid_t)NULL) || (flags & SHO_R_QUEUE) ) {
			fprintf(stderr, "Incompatable switch with -c.\n");
			exit(1);
		}
	}
	if ( (flags & SHO_R_BRIEF) && (flags & SHO_R_LONG) ) {
		fprintf(stderr, "Cannot have -b and -l on same ");
		fprintf(stderr, "command line.\n");
		exit(1);
	}
	if (flags & SHO_R_HEADER) {
		if (!(flags & SHO_R_MONSANTO) ) {
			fprintf(stderr, "-h switch cannot be used here.\n");
			exit(1);
		}
	}
	if ( (flags & SHO_R_ALLUID) && (whomuid != (uid_t)NULL) ) {
		fprintf(stderr, "Cannot use -a and -u switches together.\n");
		exit(1);
	}
	if (whomuid == (uid_t)NULL) whomuid = getuid();
						/* Assume asking about self */
	/*
	 *  For this preliminary version of the Qstat command, show all
	 *  requests in the queue, regardless of state.
	 */
	flags |= (SHO_RS_EXIT | SHO_RS_RUN | SHO_RS_STAGE |
		  SHO_RS_QUEUED | SHO_RS_WAIT | SHO_RS_HOLD | SHO_RS_ARRIVE);

        /*
         * If we just want short queue information, we just want the
         * destination.  If we want the long, give us all.  Medium gets just
         * some.
         */
        if ( flags & SHO_R_QUEUE) {
            flags &= ~(SHO_R_MONSANTO);
            if (flags & SHO_R_BRIEF) 
				;
            else if ( flags & SHO_R_LONG)
                flags |= (SHO_H_DEST | SHO_H_ACCESS | SHO_H_LIM | SHO_H_MAP |
                            SHO_H_RUNL | SHO_H_SERV | SHO_H_TIME );
            else flags |= (SHO_H_DEST);
        }
	exit_status = 0;			/* Assume success */
	if ( flags & SHO_R_COMPLEX) {
	    if ( *argv != NULL ) {
                while (*argv != NULL) {         /* Show info on a complex */
		    exit_status += showcomplex( queuefile, *argv, flags, whomuid, 
		        reqset_ptr, daepresent, qmapfile, pipeqfile, qcomplexfile);
                    argv++;
		}
            } else {
		exit_status += showcomplex( queuefile, NULL, flags, whomuid, 
			reqset_ptr, daepresent, qmapfile, pipeqfile, qcomplexfile);
	    }
	} else {   				/* Show info on a queue */
	    if (*argv != NULL) {
	        if (flags & SHO_R_DEST) {
		    fprintf(stderr, "Incompatable Qstat switches.\n");
		    exit(0);
	        }
	        while (*argv != NULL) {		/* Show info on a queue */
		    exit_status += shoqbyname (queuefile, *argv, flags, whomuid,
				    reqset_ptr, daepresent, qmapfile,
				    pipeqfile, qcomplexfile);
		    argv++;
	        }
	    } else {
		exit_status += shoallque (queuefile, flags, whomuid, reqset_ptr,
			   daepresent, qmapfile, pipeqfile, 0, qcomplexfile);
	    }
	}
	/*
	 *  Flush any output buffers and exit.
	 */
	fflush (stdout);
	fflush (stderr);
	if (exit_status) exit(1);
	exit (0);
}

static void sho_version(void)
{
     fprintf(stderr, "NQS version is %s\n", NQS_VERSION);
}

static void showhow(void)
{
    fprintf (stderr, "Command format is:\n\n");
    fprintf (stderr, "    qstat [-a] [-d] [-h] [-l] [-m] [-o] [-s] \n");
    fprintf (stderr, "          [-u username] [-v]");
    fprintf (stderr, " [ <queue-name(s)> ]\n\n");
    fprintf (stderr, "    qstat -x [-b] [-l] [ <queue-name(s)> ]\n\n ");
    fprintf (stderr, "    qstat -c [-b] [-l] [ <complex-name(s)> ]\n");
    exit (0);
}
