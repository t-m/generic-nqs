/*
 * pipeclient/pipeclient.c
 * 
 * DESCRIPTION:
 *
 *	NQS pipe client.  Enqueues requests at pipe queue destinations.
 *
 *	Original Author:
 *	--------
 *	Brent A. Kingsbury, Robert W. Sandstrom.
 *	Sterling Software Incorporated.
 *	May 24, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <libnqs/nqspacket.h>		/* Packets to the local daemon */
#include <libnqs/netpacket.h>		/* Packets to remote machines */
#include <libnqs/informcc.h>		/* Information completion codes */
#include <libnqs/requestcc.h>		/* Request completion codes */
#include <libnqs/transactcc.h>		/* Transaction completon codes */
#include <libnqs/nqsdirs.h>		/* For nqslib library functions */

#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>

#include <unistd.h>

static int badfailure ( long transactcc );
static long commit ( int sd );
static long deleteold ( Mid_t destmid, struct rawreq *rawreq, int cuid, char *cusername );
static long deliver ( Mid_t destmid, struct rawreq *rawreq, int cuid, char *cusername );
static void evalandexit ( long transactcc, char *quename, Mid_t destmid, long wait, long basercm, struct rawreq *rawreq );
static void evalmaybeexit ( long transactcc, char *quename, Mid_t destmid, long wait, short *thereshope, long *finalrcm, struct rawreq *rawreq);
static void ourserexit ( long rcm, char *servermssg );
static void pipe_brokenpipe ( int );
static void reportdep ( struct rawreq *rawreq );
static void reportenable ( Mid_t mid );
static void reportpredep ( struct rawreq *rawreq, Mid_t destmid, char *destqueue );
static void reportschdes ( char *quename, Mid_t destmid, long wait );
static void reportschreq ( struct rawreq *rawreq, long wait );
static void reportstasis ( struct rawreq *rawreq );
static void catch_sigpipe (int);

static void * pipe_buffer_file (char *, size_t);

/*** main
 *
 *	main():
 *
 *	This process is exec'd by the child of a shepherd process (child of
 *	the NQS daemon) with the following file descriptors open as described:
 *
 *		File descriptor 0: Control file (O_RDWR).
 *		File descriptor 1: Stdout writes to the NQS log process.
 *		File descriptor 2: Stderr writes to the NQS log process.
 *		File descriptor 3: Write file descriptor for serexit()
 *				   back to the shepherd process.
 *		File descriptor 4: Connected to the local NQS daemon
 *				   request FIFO pipe.
 *		File descriptors [5.._NFILE] are closed.
 *
 *
 *	At this time, this process is running with a real AND effective
 *	user-id of root!
 *
 *	The current working directory of this process is the NQS
 *	root directory.
 *
 *	All signal actions are set to SIG_DFL.
 *
 *	The environment of this process contains the environment string:
 *
 *		DEBUG=n
 *
 *	where n specifies (in integer ASCII decimal format), the debug
 *	level in effect when this client was exec'd over the child of
 *	an NQS shepherd.
 *
 *	The user upon whose behalf this work is being performed (the
 *	owner of the request), is identified by the environment variables:
 *
 *		UID=n
 *		USERNAME=username-of-request-owner
 *
 *	where n specifies (in integer ASCII decimal format), the integer
 *	user-id of the request owner.
 *
 *	For System V based implementations of NQS, the environment
 *	variable:
 *
 *		TZ=timezonename
 *
 *	will also be present.  Berkeley based implementations of NQS will
 *	not contain this environment variable.
 *
 *	The environment of this process also contains the default retry
 *	state tolerance time, and the default retry wait time recommenda-
 *	tions (in seconds):
 *
 *		DEFAULT_RETRYWAIT=n
 *		DEFAULT_RETRYTIME=n
 *
 *	where n is an ASCII decimal format long integer number.
 *
 *	Additional environment vars define the possible destination set
 *	for the request:
 *
 *		Dnnn=mmm rrr queuename
 *
 *	where:
 *
 *		nnn	  specifies the destination number (in ASCII
 *			  decimal) in the range [0..#of destinations-1];
 *		mmm	  specifies the machine-id of the destination
 *			  (in ASCII decimal).
 *		rrr	  specifies the amount of time that this
 *			  destination has spent in a retry mode.
 *		queuename specifies the name of the destination queue
 *			  at the destination machine.
 *
 *	Lastly, the presence of the environment variable:
 *
 *		PERSIST=Y
 *
 *	indicates that the request should be requeued, even if all of the
 *	destinations indicate that they will never accept the request.  The
 *	presence of the PERSIST environment variable is a signal to the
 *	pipe queue server (pipeclient), that there are presently disabled
 *	destination(s) for the associated pipe queue, that MIGHT accept the
 *	request at a later time.
 */

int main (int argc, char * argv[], char * envp[])
{
	char *cp;			/* Ascii value of env var */
	int cuid;			/* Client user id */
	char *cusername;		/* Client username */
	/* long dretrytime; */		/* Default seconds before we give up */
	long dretrywait;		/* Default seconds per wait */
	struct rawreq rawreq;		/* What we are about to send */
	struct transact transact;	/* Holds output of tra_read() */
	short thereshope;		/* Boolean */
	long finalrcm;			/* Or several rcms into here */
	register int destno;		/* Destination number */
	long transactcc;		/* Transaction completion code */
	long quereqinfo;		/* Info from NPK_QUEREQ transaction */
	register char *destqueue;	/* Destination queue */
	Mid_t destmid;			/* Destination machine-id */
	long retrytime;			/* The amount of time that the */
					/* current destination has spent */
					/* in a retry mode */
	int sd;				/* Socket descriptor */

  	/* we close stdout, so that the debugging code opens it */
  	close(1);
  	sal_debug_Init(1, "NQS Pipeclient", NULL);
	/*
	 * We must be root to bind to a reserved port.
	 */
	if (getuid() != 0 || geteuid() != 0) {
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pipeclient: unable to get uid or euid, UNAFAILURE.\n");
		ourserexit (RCM_UNAFAILURE, (char *) 0);
	}
	if ((cp = getenv ("DEBUG")) != (char *) 0)
		sal_debug_SetLevel(atoi(cp));
	
	interset (4);		/* Use this file descriptor to communicate */
				/* with the local NQS daemon */
	if ((cp = getenv ("UID")) == (char *) 0) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to locate environment variable UID\n");
		ourserexit (RCM_BADSRVARG, (char *) 0);
	}
	cuid = atoi (cp);
	if ((cusername = getenv ("USERNAME")) == (char *) 0) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to locate environment variable USERNAME\n");
		ourserexit (RCM_BADSRVARG, (char *) 0);
	}
	if ((cp = getenv ("DEFAULT_RETRYTIME")) == (char *) 0) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to locate environment variable DEFAULT_RETRYTIME\n");
		ourserexit (RCM_BADSRVARG, (char *) 0);
	}
	/* dretrytime = atoi (cp); */
	if ((cp = getenv ("DEFAULT_RETRYWAIT")) == (char *) 0) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to locate environment variable DEFAULT_RETRYWAIT\n");
		ourserexit (RCM_BADSRVARG, (char *) 0);
	}
	dretrywait = atoi (cp);
	while (*envp != (char *) 0) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient envp: %s\n", *envp);
		envp++;
	}
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "End of pipeclient envp\n");
  
	/*
	 * Readreq leaves the file pointer pointing to the first
	 * byte beyond the header.
	 */
	if (readreq (STDIN, &rawreq) == -1) {
	  	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "readreq() returned -1\n");
		ourserexit (RCM_BADCDTFIL, (char *) 0);
	}
	/*
	 * Find out whether we are starting from scratch.
	 */
	if (tra_read (rawreq.trans_id, &transact) == -1) {
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pipeclient, tra_read failed for %d UNAFAILURE\n", rawreq.orig_seqno);
		ourserexit (RCM_UNAFAILURE, (char *) 0);
	}
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: initial transaction state is %d\n", transact.state);
	switch (transact.state) {
	/*
	 * After network queues are implemented,
	 * it will no longer be possible for pipeclient
	 * to called on a request in RTS_DEPART state.
	 */
	case RTS_DEPART:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: transaction state is RTS_DEPART");
		transactcc = deliver (transact.v.peer_mid, &rawreq,
				      cuid, cusername);
		tcmmsgs (transactcc, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: redeliver ");
		if (pipeqenable (transactcc)) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: calling reportenable()\n");
			reportenable (transact.v.peer_mid);
		}
		switch (transactcc) {
		case TCMP_COMPLETE:
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: request delivered, exiting\n");
			ourserexit (RCM_DELIVERED, (char *) 0);
		case TCMP_NOSUCHREQ:
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: no such request, exiting\n");
			ourserexit (RCM_PIPREQDEL, (char *) 0);
		default:
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: delivery failure, exiting\n");
			evalandexit (transactcc, rawreq.trans_quename,
				transact.v.peer_mid, dretrywait,
				RCM_DELIVERFAI, &rawreq);
		}
	case RTS_PREDEPART:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: transaction state is RTS_PREDEPART");
		transactcc = deleteold (transact.v.peer_mid, &rawreq,
					cuid, cusername);
		tcmmsgs (transactcc, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: deleteold ");
		if (transactcc != TCMP_CONTINUE &&
		    transactcc != TCMP_NOSUCHREQ) {
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "routing failure, exiting\n");
			evalandexit (transactcc, rawreq.trans_quename,
				     transact.v.peer_mid, dretrywait,
				     RCM_ROUTEFAI, &rawreq);
		}
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: calling reportenable()\n");
		reportenable (transact.v.peer_mid);
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: calling reportstasis()\n");
		reportstasis (&rawreq);
		/*
		 * Now we have a clean slate.
		 * FALL THROUGH.
		 */
	case RTS_ARRIVE:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: transaction state is RTS_ARRIVE");
	case RTS_STASIS:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: transaction state is RTS_STASIS");
		/*
		 * Our assumptions, until better information can be obtained:
		 * (finalrcm): We don't know WHY any unretryable destinations
		 * are unretryable.
		 * (thereshope): There are NO known retryable destinations.
		 */
		finalrcm = RCM_ROUTEFAI;
		thereshope = 0;
		destno = 1;		/* Try destination #1 first */

		if ((destqueue = pipeqdest (destno, &destmid,
					    &retrytime)) == (char *) 0) {
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to route request to first destination\n");
			if (getenv ("PERSIST") != (char *) 0) {
				/*
				 *  The request should be allowed to persist.
				 */
			  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Scheduling request for retry at a later time\n");
				reportschreq (&rawreq, dretrywait);
				sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Request rescheduled; exiting\n");
				ourserexit (RCM_RETRYLATER, (char *) 0);
			}
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "routing failure, exiting\n");
			ourserexit (RCM_ROUTEFAI, (char *) 0);
		}

		/* Generic NQS v3.50.5-pre2
		 *
		 * At this point, we have decided that the pipe queue does
		 * have a destination.  We have not yet attempted to route
		 * the request to that destination.
		 *
		 * We now remember to reschedule the request
		 */

		do {
			/*
			 *  Another destination exists.  Try it.
			 */
			 sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: dest=%1d; mid=%1u; queue=%s\n", destno, destmid, destqueue);
			/*
			 * Pipeqreq() returns when 
			 * 1) the request is in pre-arrive state
			 *	on the remote machine, or
			 * 2) the request has been submitted
			 *	on the local machine.
			 */
			transactcc = pipeqreq (destmid, destqueue, &rawreq,
					       cuid, cusername, &sd);
			tcmmsgs (transactcc, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: pipeqreq ");
			if (pipeqenable (transactcc)) reportenable (destmid);
			if (transactcc != TCMP_CONTINUE &&
			   (transactcc & XCI_FULREA_MASK) != TCML_SUBMITTED) {
				sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: may be exiting\n");
				evalmaybeexit (transactcc, destqueue,
					destmid, dretrywait,
					&thereshope, &finalrcm, &rawreq);
			}
		} while (
			(transactcc != TCMP_CONTINUE) &&
			((transactcc & XCI_FULREA_MASK) != TCML_SUBMITTED) &&
			((destqueue = pipeqdest (++destno, &destmid,
				&retrytime)) != (char *) 0)
			);
		tcmmsgs (transactcc, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: pipeqdest ");
		if ((transactcc & XCI_FULREA_MASK) == TCML_SUBMITTED) {
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: request routed to LOCAL host, exiting\n");
			signal (SIGPIPE, pipe_brokenpipe);
			ourserexit (RCM_ROUTEDLOC, (char *) 0);
		}
		if (destqueue == (char *) 0) {
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to deliver request %d to a destination\n", rawreq.orig_seqno);
			if (thereshope == 1 ||
			    getenv ("PERSIST") != (char *) 0) {
				/*
				 *  The request should be allowed to persist.
				 */
			  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Msg #2:Scheduling request for retry at a later time\n");
				reportschreq (&rawreq, dretrywait);
				sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Msg #2:Request rescheduled; exiting\n");
				ourserexit (RCM_RETRYLATER, (char *) 0);
			}
			else {
				sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: anything could be happening - exiting\n");
				ourserexit (finalrcm, (char *) 0);
			}
		}
		/*
		 * Only TCMP_CONTINUE can reach this point.
		 * Tell the local daemon that the remote machine liked packet 2.
		 */
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: before reppredep ");
		reportpredep (&rawreq, destmid, destqueue);
		break;
	default:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pipeclient: Unknown transaction state %d UNAFAILURE; exiting\n", transact.state);
		ourserexit (RCM_UNAFAILURE, (char *) 0);
	}
	/*
	 * At this point, the request is in the RTS_PREDEPART state,
	 * we know the destination, and we have a socket open
	 * to the destination machine.  Send packet 3.
	 */
	transactcc = commit (sd);
	tcmmsgs (transactcc, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: commit ");
	if ((transactcc & XCI_FULREA_MASK) != TCMP_SUBMITTED) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Unable to commit, exiting\n");
		evalandexit (transactcc, rawreq.trans_quename,
			transact.v.peer_mid, dretrywait,
			RCM_ROUTEFAI, &rawreq);
	}
	/*
	 * Remember the information bits, so that we can tell
	 * our parent when we exit.
	 * Tell the local daemon that the remote machine liked packet 3.
	 */
	quereqinfo = transactcc & XCI_INFORM_MASK;
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: calling reportdep()\n");
	reportdep (&rawreq);
	/*
	 * After network queues are implemented, we will exit here
	 * with RCM_ROUTED, passing any information bits (quereqinfo)
	 * back to our caller.
	 */
	/*
	 * Start a new conversation.  Send the tail of the
	 * control file. Send all of the data files.
	 * Eventually, CODE BELOW THIS POINT WILL BECOME PART OF A
	 * DIFFERENT PROCESS, hence the duplication of effort.
	 * That different process will have cuid and cusername as
	 * environmental variables.
	 * Readreq leaves the file pointer pointing to the first
	 * byte beyond the header.
	 */
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: reading request file\n");
	if (readreq (STDIN, &rawreq) == -1) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "readreq() returned -1\n");
		ourserexit (RCM_BADCDTFIL, (char *) 0);
	}
	/*
	 * Find out whether we are starting from scratch.
	 */
	if (tra_read (rawreq.trans_id, &transact) == -1) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "tra_read() returned -1\n");
		ourserexit (RCM_BADCDTFIL, (char *) 0);
	}
	if (transact.state != RTS_DEPART) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Transaction not in departing state!\n");
		ourserexit (RCM_BADCDTFIL, (char *) 0);
	}

	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: calling deliver()\n");
	transactcc = deliver (transact.v.peer_mid, &rawreq, cuid, cusername);
	tcmmsgs (transactcc, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient: deliver ");
	switch (transactcc) {
	case TCMP_COMPLETE:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Request delivered, exiting\n");
		ourserexit (RCM_DELIVERED | quereqinfo, (char *) 0);
	case TCMP_NOSUCHREQ:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Request to be deleted, exiting\n");
		ourserexit (RCM_PIPREQDEL, (char *) 0);
	default:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Delivery failure, exiting\n");
		evalandexit (transactcc, rawreq.trans_quename,
			transact.v.peer_mid, dretrywait,
			RCM_DELIVERFAI, &rawreq);
	}

  sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "End of main() reached; should never happen\n");
  return 0;
}


/*** badfailure
 *
 *	int badfailure:
 *
 *	Return 1 if this is a bad failure.
 *	Return 0 if this is a success or a mild failure.
 */
static int badfailure (transactcc)
long transactcc;
{	
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "badfailure() called with code %d\n", transactcc);
	return (transactcc == TCML_NOLOCALDAE || transactcc == TCML_PROTOFAIL);
}


/*** commit
 *
 *	long commit:
 *
 *	Send a packet to the remote machine committing enqueueing
 *	at the previously attempted destination.
 *	This is the 2nd phase of the 2 phase commit.
 *	Returns: a TCM.
 */
static long commit (sd)
int sd;						/* Socket descriptor */
{
	char packet [MAX_PACKET];
	int packetsize;
	int integers;
	int strings;
	
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "commit() entered\n");
	interclear ();
	interw32i (NPK_COMMIT);
	if ((packetsize = interfmt (packet)) == -1) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Packetsize wrong\n");
		return (TCML_INTERNERR);
		
	}
	if (write (sd, packet, packetsize) != packetsize) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Failed to write %d bytes\n", packetsize);
		return (TCML_INTERNERR);
	}
	switch (interread (getsockch)) {
	case 0:
		break;
	case -1:
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "interread() returned -1\n");
		return (TCMP_CONNBROKEN);
	case -2:
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "interread() returned -2\n");
		return (TCMP_PROTOFAIL);
	}
	integers = intern32i ();
	strings = internstr ();
	if (integers == 1 && strings == 0) {
		return (interr32i (1));
	}
	else 
  	{
	  sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Expected 1 integer and 0 strings; received %d integers and %d strings\n", integers, strings);
	  return (TCMP_PROTOFAIL);
	}
}

/*** deleteold
 *
 *	long deleteold:
 *
 *	This function establishes a fresh connection with
 *	destmid.  We tell destmid that it should delete
 *	the request in question.
 *
 */
static long deleteold (destmid, rawreq, cuid, cusername)
Mid_t destmid;				/* Destination machine id */
struct rawreq *rawreq;			/* Request info */
int cuid;				/* Client user id */
char *cusername;			/* Client username */
{
	int sd;				/* Socket descriptor */
	short timeout = 0;		/* Seconds between tries */
	long transactcc;		/* Holds establish() return value */

	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "deleteold() entered\n");
	interclear ();
	interw32i ((long) cuid);		/* Client uid */
	interwstr (cusername);			/* Client username */
	interw32i (rawreq->orig_seqno);		/* Original sequence number */
	interw32u (rawreq->orig_mid);		/* Original machine id */
	interw32i (0L);				/* Signal (none) */
	interw32i ((long) RQS_ARRIVING);	/* Request queueing state */
	/*
	 * Send the first and only packet of the deleteold connection.
	 */
	sd = establish (NPK_DELREQ, destmid, cuid, cusername, &transactcc);
	if (sd == -2) {
		/*
		 * Retry is in order.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "establish() returned -2\n");
		timeout = 1;
		do {
			nqssleep (timeout);
			interclear ();
			interw32i ((long) cuid);
			interwstr (cusername);
			interw32i (rawreq->orig_seqno);
			interw32u (rawreq->orig_mid);
			interw32i (0L);
			interw32i ((long) RQS_ARRIVING);
			sd = establish (NPK_DELREQ, destmid,
				cuid, cusername, &transactcc);
			timeout *= 2;
		} while (sd == -2 && timeout <= 16);
	}
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "leaving deleteold()\n");
	return (transactcc);
}


/*** deliver
 *
 *	long deliver:
 *
 *	This function establishes a fresh connection with
 *	destmid. We send over the tail of the control file.
 *	We send over all data files.
 *	In the near future, the network queue client
 *	take the place of deliver().
 *
 */
static long deliver (destmid, rawreq, cuid, cusername)
Mid_t destmid;				/* Destination machine id */
struct rawreq *rawreq;			/* Request info */
int cuid;				/* Client user id */
char *cusername;			/* Client username */
{
	int sd;				/* Socket descriptor */
	short timeout;			/* Seconds between tries */
	long transactcc;		/* Holds establish() return value */
	long headersize;		/* Bytes in control file header */
	long totalsize;			/* Bytes in control file */
	char packet [MAX_PACKET];	/* Buffer area */
	int packetsize;			/* Bytes in this packet */
	int strings;			/* Number of strings */
	int integers;			/* Number of integers */
	int i;				/* Loop index */
	char packedname [29];		/* Dir:14, file:14, slash:1 */
	struct stat statbuf;		/* Holds stat() output */

	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "deliver() entered\n");
	interclear ();
	/*
	 * Send the first packet of the delivery connection.
	 */
	sd = establish (NPK_REQFILE, destmid, cuid, cusername, &transactcc);
	if (sd < 0) {
		if (sd == -2) {
			/*
			 * Retry is in order.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "establish() returned -2\n");
			timeout = 1;
			do {
				nqssleep (timeout);
				interclear ();
				sd = establish (NPK_REQFILE, destmid,
					cuid, cusername, &transactcc);
				timeout *= 2;
			} while (sd == -2 && timeout <= 16);
			/*
			 * Beyond this point, give up on retry.
			 */
			if (sd < 0) {
				return (transactcc);
			}
		}
		else {
			/*
			 * Retry would surely fail.
			 */
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "deliver(): abandoning timeout loop\n");
			return (transactcc);
		}
	}
	/*
	 * The server liked us.  The next step is to determine
	 * the length of the tail of the control file.
	 * We count on the file pointer being undisturbed from above.
	 */
	if ((headersize = lseek (STDIN, 0L, 1)) == -1) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "deliver(): unable to determine header size\n");
		return (errnototcm());
	}
	if ((totalsize = lseek (STDIN, 0L, 2)) == -1) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "deliver(): unable to determine total size\n");
		return (errnototcm());
	}
	interclear();
	interw32i ((long) NPK_REQFILE);
	interw32i ((long) -1);		/* "-1" means "the control file" */
	interw32i ((long) totalsize - headersize);
	interw32i ((long) rawreq->orig_seqno);
	interw32u ((long) rawreq->orig_mid);
	if ((packetsize = interfmt (packet)) == -1) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Incorrect packet size\n");
		return (TCML_INTERNERR);
	}
	if (write (sd, packet, packetsize) != packetsize) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to write %d bytes\n", packetsize);
		return (TCMP_CONNBROKEN);
	}
	/*
	 * See if the server got packet 2 of the delivery connection.
	 */
	switch (interread (getsockch)) {
	case 0:
		break;
	case -1:
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "interread() returned -1\n");
		return (TCMP_CONNBROKEN);
	case -2:
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "interread() returned -2\n");
		return (TCML_PROTOFAIL);
	}
	integers = intern32i();
	strings = internstr();
	if (integers != 1 || strings != 0) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Expected 1 integer and 0 strings; received %d integers and %d strings\n", integers, strings);
		return (TCML_PROTOFAIL);
	}
	switch (transactcc = interr32i (1)) {
	case TCMP_CONTINUE:
		break;
	case TCMP_NOSUCHREQ:			/* Deleted or already ran */
	case TCMP_COMPLETE:			/* Already delivered */
	default:
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "deliver(): bailing out with transactcc %d\n", transactcc);
		return (transactcc);
	}
	/*
	 * Send the tail of the control file as the beginning of
	 * packet 3 of the delivery connection.
	 */
	lseek (STDIN, (long) headersize, 0);
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient (deliver): about to send tail\n");
	if (filecopyentire (STDIN, sd) != totalsize - headersize) {
		return (errnototcm ());
	}
	for (i = 0; i < rawreq->ndatafiles; i++) {
		
		void * pvoBuffer = NULL;

		pack6name (packedname, Nqs_data,
			(int) rawreq->orig_seqno % MAX_DATASUBDIRS,
			(char *) 0, rawreq->orig_seqno, 5,
			(Mid_t) rawreq->orig_mid, 6, i, 3);
		if (stat (packedname, &statbuf) == -1) {
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to stat() file %s\n", packedname);
			return (TCML_INTERNERR);
		}

		/* SLH - 2000/02/10
		 *
		 * To prevent the race condition happening, we pull the
		 * datafile contents into memory before sending the
		 * NQS_REQFILE packet.
		 */

		pvoBuffer = pipe_buffer_file(packedname, statbuf.st_size);
		if (pvoBuffer == NULL)
			return (TCML_INTERNERR);

		interclear ();
		interw32i ((long) NPK_REQFILE);
		interw32i ((long) i);
		interw32i (statbuf.st_size);
		if ((packetsize = interfmt (packet)) == -1) {
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Incorrect packet size\n");
			free (pvoBuffer);
			return (TCML_INTERNERR);
		}
		if (write (sd, packet, packetsize) != packetsize) {
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to write %d bytes\n", packetsize);
			free (pvoBuffer);
			return (TCMP_CONNBROKEN);
		}
		switch (interread (getsockch)) {
		case 0:
			break;
		case -1:
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "interread() returned -1\n");
			free (pvoBuffer);
			return (TCMP_CONNBROKEN);
		case -2:
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "interread() returned -2\n");
			free (pvoBuffer);
			return (TCML_PROTOFAIL);
		}
		integers = intern32i();
		strings = internstr();
		if (integers == 1 && strings == 0) {
			if ((transactcc = interr32i (1)) != TCMP_CONTINUE) {
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "deliver(): bailing out of datafile loop with transactcc %d\n", transactcc);
				free (pvoBuffer);
				return (transactcc);
			}
		}
		/*
		 * Send the data file here
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Pipeclient (deliver): about to send data #%d\n", i);
		if (write(sd, pvoBuffer, statbuf.st_size) != statbuf.st_size)
		{
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "deliver(): unable to write datafile; exiting\n");
			free (pvoBuffer);
			return (errnototcm ());
		}
		free(pvoBuffer);
		pvoBuffer = NULL;
	}			/* End of for loop */
	/*
	 * We have already put a file into this packet.
	 * Tack a note onto the end saying that there are no more files.
	 */
	interclear ();
	interw32i ((long) NPK_DONE);
	interw32i ((long) 0);
	interw32i ((long) 0);
	if ((packetsize = interfmt (packet)) == -1) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Incorrect packet size\n");
		return (TCML_INTERNERR);
	}
	if (write (sd, packet, packetsize) != packetsize) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to write %d bytes\n", packetsize);
		return (TCMP_CONNBROKEN);
	}
	switch (interread (getsockch)) {
	case 0:
		break;
	case -1:
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "interread() returned -1\n");
		return (TCMP_CONNBROKEN);
	case -2:
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "interread() returned -2\n");
		return (TCML_PROTOFAIL);
	}
	integers = intern32i();
	strings = internstr();
	if (integers == 1 && strings == 0) {
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Expected 1 integer and 0 strings; received %d integers and %d strings\n", integers, strings);
		return (interr32i (1));
	}
	return (TCML_PROTOFAIL);
}


/*** evalandexit
 *
 *	void evalandexit:
 *
 *	Evaluate a transaction completion code, and act on any retry
 *	information.  Then exit, passing a request completion code to
 *	the shepherd process.
 */
static void evalandexit (transactcc, quename, destmid, wait, basercm, rawreq)
long transactcc;			/* Transaction completion code */
char *quename;				/* Name without @machine */
Mid_t destmid;				/* Destination machine id */
long wait;				/* Seconds before next try */
long basercm;				/* Generalized reason for failure */
struct rawreq *rawreq;			/* Pointer to request header info */
{
	
	switch (pipeqretry (transactcc)) {
	case 1:
		reportschdes (quename, destmid, wait);
		/* reportschreq (rawreq, wait); */
		ourserexit (RCM_RETRYLATER, (char *) 0);
	case 0:
		pipeqexitifbad (transactcc);
		ourserexit (basercm | pipeqfailinfo (transactcc), (char *) 0);
	case -1:
		reportschdes (quename, destmid, wait);
/*	  	reportschreq (rawreq, wait); */
		ourserexit (basercm | pipeqfailinfo (transactcc), (char *) 0);
	}
}


/*** evalmaybeexit
 *
 *
 *	void evalmaybeexit:
 *
 *	Evaluate a transaction completion code.
 *	If it indicates that a destination retry is in order, then tell the
 *	local daemon when to schedule the destination retry event.
 *	If it indicates that retry is not in order, or in a reason into
 *	finalrcm.  If you want to customize the pipeclient, this is a good
 *	place to start.
 */
static void evalmaybeexit (transactcc, quename, destmid, wait, thereshope,
			   finalrcm, rawreq)
long transactcc;			/* Transaction completion code */
char *quename;				/* Name without @machine */
Mid_t destmid;				/* Machine id */
long wait;				/* Seconds before next try */
short *thereshope;			/* Pointer to boolean */
long *finalrcm;				/* Pointer to composite of reasons */
					/* for failure */
struct rawreq *rawreq;			/* request structure */
{
	
	switch (pipeqretry (transactcc)) {
	case 1:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "evalmaybeexit(): pipeqretry() returned 1\n");
		/* *thereshope = 1; */
		reportschdes (quename, destmid, wait);
/*	  	reportschreq (rawreq, wait); */
/*		ourserexit (RCM_RETRYLATER, (char *) 0); */
	  	break;
	case 2:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "evalmaybeexit(): pipeqretry() returned 2\n");
		*thereshope = 1;
		break;
	case 0:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "evalmaybeexit(): pipeqretry() returned 0\n");
		pipeqexitifbad (transactcc);
		*finalrcm |= pipeqfailinfo (transactcc);
		break;
	case -1:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "evalmaybeexit(): pipeqretry() returned -1\n");
		reportschdes (quename, destmid, wait);
	  	reportschreq (rawreq, wait);
		ourserexit (RCM_RETRYLATER, (char *) 0);
	}
}


/*** ourserexit
 *
 *	void ourserexit:
 *
 *	Give up any IPC file, close the FIFO pipe, and
 *	then tell the shepherd process what happened.
 */
static void ourserexit (rcm, servermssg)
long rcm;				/* Request completion code */
char *servermssg;			/* Additional text */
{
	exiting ();
	serexit (rcm, servermssg);
}


/*** reportdep
 *
 *	void reportdep:
 *
 *	Report that the request is ready to go to the departed state.
 *	Exit if inter() fails badly.
 */
static void reportdep (rawreq)
struct rawreq *rawreq;			/* Request info */
{
	long transactcc;
	struct transact transact;
	
	interclear ();
	interw32i (rawreq->orig_seqno);
	interw32u (rawreq->orig_mid);
	transactcc = inter (PKT_RMTDEPART);
	if (badfailure (transactcc)) {
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pipeclient: inter failed with oct %o UNAFAILURE\n", transactcc);
		ourserexit (RCM_UNAFAILURE, (char *) 0);
	}
	if (transactcc == TCML_NOSUCHREQ)  {
		/*
		 * Should never happen.
		 */
		ourserexit (RCM_PIPREQDEL, (char *) 0);
	}
	/*
	 * Synchronously leave a record in non-volatile memory.
	 */
	tra_read (rawreq->trans_id, &transact);
	transact.state = RTS_DEPART;
	tra_setstate (rawreq->trans_id, &transact);
}


/*** reportenable
 *
 *	void reportenable:
 *
 *	Report that all destinations at a machine should be reenabled.
 *	Exit if inter() fails badly.
 */
static void reportenable (mid)
Mid_t mid;				/* Machine id */
{
	interclear ();
	interw32u ( mid);
	if (badfailure (inter (PKT_RMTENAMAC))) {
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pipeclient: Nolocaldae or Protofail UNAFAILURE\n");
		ourserexit (RCM_UNAFAILURE, (char *) 0);
	}
}


/*** reportpredep
 *
 *	void reportpredep:
 *
 *	Report that the request is ready to go to the predeparted state.
 */
static void reportpredep (rawreq, destmid, destqueue)
struct rawreq *rawreq;			/* Pointer to request header info */
Mid_t destmid;				/* Destination machine id */
char *destqueue;			/* Destination queue name */
{
	long transactcc;
	struct transact transact;
	
	interclear ();
	interw32i (rawreq->orig_seqno);
	interw32u (rawreq->orig_mid);
	transactcc = inter (PKT_RMTACCEPT);
	if (badfailure (transactcc)) {
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pipeclient: RMTACCEPT failed with nolocaldae or protofail UNAFAILURE\n");
		fflush(stdout);
		ourserexit (RCM_UNAFAILURE, (char *) 0);
	}
	if (transactcc == TCML_NOSUCHREQ)  {
		ourserexit (RCM_PIPREQDEL, (char *) 0);
	}
	strncpy (rawreq->trans_quename, destqueue, MAX_QUEUENAME + 1);
	writehdr (STDIN, rawreq);
	/*
	 * BEWARE!
	 * The machine might crash after the tra_setstate() call has
	 * changed the state to RTS_PREDEPART, but before the write
	 * buffer containing the new trans_quename has been written
	 * to disk.
	 */
	tra_read (rawreq->trans_id, &transact);
	transact.v.peer_mid = destmid;
	transact.state = RTS_PREDEPART;
	/*
	 * The order of update must guarantee that: IF the state has
	 * already been set, then the info fields have also been set.
	 */
	tra_setinfo (rawreq->trans_id, &transact);
	tra_setstate (rawreq->trans_id, &transact);
}


/*** reportschdes
 *
 *	void reportschdes:
 *
 *	Report that the destination should be retried after
 *	retrywait seconds.
 */
static void reportschdes (quename, destmid, wait)
char *quename;				/* Queue name missing @machine */
Mid_t destmid;				/* Destination machine id */
long wait;				/* Seconds before next retry */
{
	interclear ();
	interwstr (quename);
	interw32u (destmid);
	interw32i (wait);
	if (badfailure (inter (PKT_RMTSCHDES))) {
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pipeclient:  RMTSCHDES failed with nolocaldae or protofail UNAFAILURE\n");
		ourserexit (RCM_UNAFAILURE, (char *) 0);
	}
}

/* =-=-=-=-=
 * 	void catch_sigpipe()
 * 
 * 	We occaisonally get a SIGPIPE when trying to talk to the
 * 	NQSdaemon.  Our solution is to wait a while and try again.
 */

int iPipe = 0;

static void catch_sigpipe(int iDummy)
{
	iPipe++;
	signal(SIGPIPE, catch_sigpipe);
}

/*** reportschreq
 *
 *	void reportschreq:
 *
 *	Report that the request should be retried after
 *	retrywait seconds.
 */
static void reportschreq (rawreq, wait)
struct rawreq *rawreq;			/* Pointer to request header info */
long wait;				/* Seconds before next retry */
{
	/* Generic NQS v3.50.5
	 * 
	 * We have a small problem, in that we believe that this function
	 * is causing the odd SIGPIPE.  Unfortunately, I can't reproduce
	 * it myself, so guessing is all I have to try.
	 * 
	 * We ensure that we have the right pipe open to the NQSdaemon
	 * by telling inter() to open the pipe for itself.  We do this
	 * by calling interset() with -1.
	 */

	signal(SIGPIPE, catch_sigpipe);

	do {
		interset(-1);
		interclear ();
		interw32i (rawreq->orig_seqno);
		interw32u (rawreq->orig_mid);
		interw32i (wait);
	
		iPipe = 0;
	        if (badfailure (inter (PKT_SCHEDULEREQ))) {
	        	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pipeclient:  SCHEDULEREQ failed with nolocaldae or protofail UNAFAILURE\n");
			ourserexit (RCM_UNAFAILURE, (char *) 0);
		}
		if (iPipe != 0)
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "reportschreq(): Recovering from SIGPIPE\n");
	} while (iPipe != 0);
	
}


/*** reportstasis
 *
 *	void reportstasis:
 *
 *	Report that the request should go back to the stasis state.
 */
static void reportstasis (rawreq)
struct rawreq *rawreq;			/* Pointer to request header info */
{
	struct transact transact;
	register long transactcc;
	
	/*
	 * Synchronously leave a record in non-volatile memory.
	 */
	tra_read (rawreq->trans_id, &transact);
	transact.v.peer_mid = 0;
	transact.state = RTS_STASIS;
	/*
	 * The order of update must guarantee that: IF the state has
	 * already been set, then the info fields have also been set.
	 */
	tra_setinfo (rawreq->trans_id, &transact);
	tra_setstate (rawreq->trans_id, &transact);
	/*
	 *  Tell the local NQS daemon, that the request is now back
	 *  in the stasis state.
	 */
	interclear ();
	interw32i (rawreq->orig_seqno);
	interw32u (rawreq->orig_mid);
	transactcc = inter (PKT_RMTSTASIS);
	if (badfailure (transactcc)) {
	        sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "pipeclient:  RMTSTASIS failed with nolocaldae or protofail UNAFAILURE\n");
		ourserexit (RCM_UNAFAILURE, (char *) 0);
	}
	if (transactcc == TCML_NOSUCHREQ)  {
		ourserexit (RCM_PIPREQDEL, (char *) 0);
	}
}
static void
pipe_brokenpipe( int unused )
{
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "pipeclient: caught SIGPIPE!!!!\n");
	exit (0);
}

/* SLH - 2000/02/10
 *
 * There is a race condition in NQS, whereby a datafile we are sending can
 * be sent back to us before we have finished with it.
 *
 * The proposed solution is to read the entire datafile into memory first,
 * close the file, and *then* send the REQFILE packet and then the contents
 * over the network.
 *
 * This function will slurp the datafile up into a single memory buffer,
 * ready to be sent.
 */
 
static void * pipe_buffer_file (char * szFilename, size_t ulFilesize)
{
	int 	iFd       = 0;
	void *	pvoBuffer = NULL;

	if ((iFd = open(szFilename, O_RDONLY)) < 0)
	{
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to open file %s\n", szFilename);
		return NULL;
	}

	pvoBuffer = malloc(ulFilesize);
	if (pvoBuffer == NULL)
	{
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to allocate %lu bytes for file %s\n", ulFilesize, szFilename);
		close (iFd);
		return NULL;
	}

	if (read(iFd, pvoBuffer, ulFilesize) != ulFilesize)
	{
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Unable to read %lu bytes from file %s\n", ulFilesize, szFilename);
		close (iFd);
		free (pvoBuffer);
		return NULL;
	}

	close (iFd);

	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "read %lu bytes from file %s\n", ulFilesize, szFilename);

	return pvoBuffer;
}

