/*
 * nqssetargv
 *		Run an arbitrary command, setting argv[0] ... of that
 *		command to argv[1] ... of this process
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <unistd.h>

int main (int argc, char * argv[], char * env[])
{
	return execve(argv[0], &argv[1], env);
}
