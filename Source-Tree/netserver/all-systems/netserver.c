/*
 * netserver/netserver.c
 * 
 * DESCRIPTION:
 *
 *	NQS network server. Exec'd when necessary by the network daemon.
 *
 *	
 *	Original Author:
 *	-------
 *	Robert W. Sandstrom, Sterling Software Incorporated.
 *	April 23, 1986.
 */

#include <libnqs/license.h>
#include <libnqs/proto.h>
#include <stdlib.h>
#include <errno.h>			/* System call error numbers */
#include <signal.h>			/* For SIGPIPE */
#include <pwd.h>			/* Password stuff */
#include <sys/stat.h>			/* So we can call stat() */
#include <libnqs/nqspacket.h>		/* To talk to the local daemon */
#include <libnqs/netpacket.h>		/* Network packet types */
#include <libnqs/informcc.h>		/* Information completion codes */
#include <libnqs/requestcc.h>		/* Request completion codes */
#include <libnqs/transactcc.h>		/* Transaction completion codes */
#include <libnqs/nqsdirs.h>		/* Pathnames of directories */
#include <libsal/license.h>
#include <libsal/proto.h>
#include <libsal/defines.h>
#include <string.h>
#include <unistd.h>

static unsigned long enf_max ( unsigned long quota );
static void mvoutfileserver ( void );
static void quereqserver ( Mid_t clientmid );
static int receivehdr ( struct rawreq *rawreq );
static void reqfileserver ( void );
static void sendtcm ( long tcm );
static void server_exit ( void );
static int unpackheader ( struct rawreq *rawreq );

#define MAXIMUM_UNSIGNED32 2147483647

/*
 *	Variables global to this module.
 */

Mid_t Locmid;                   /* Our mid */
static char *Argv0;
static int Argv0size;

/*** main
 *
 *
 *	main():
 *
 *	This process is exec'd by the child of the network daemon
 *	with the following file descriptors open as described:
 *
 *	File descriptor 0: The new socket.
 *	File descriptor 1: The NQS log process.
 *	File descriptor 2: The NQS log process.
 *	File descriptor 3: Connected to the local NQS daemon request
 *			   FIFO pipe.
 *	File descriptor 4: The NQS log process.
 *	File descriptors [5.._NFILE] are closed.
 *
 *	The current working directory of this process is the NQS
 *	root directory.
 *
 *	All signal actions are set to SIG_DFL.
 *
 *	The environment of this process contains the environment string:
 *
 *		DEBUG=nnn
 *
 *	where nnn specifies (in integer ASCII decimal format), the
 *	debug level in effect when this server was exec'd over
 *	the child of the NQS netdaemon.
 *
 *	The environment of this process also contains the following
 *	environment variables:
 *
 *		OP=opstring
 *		CLIENTMID=mid
 *
 *	where opstring is one of:
 *
 *		OP=NPK_MVOUTFILE
 *			Catch one or more staged-out files
 *		OP=NPK_QUEREQ
 *			Catch the header of a control file, and
 *			attempt to put a request in the arrive state in
 *			a queue on this machine
 *		OP=NPK_REQFILE
 *			Catch the tail of a control file, and all
 *			of the data files associated with this request
 *
 *	and 	mid	is the ASCII decimal integer value of
 *			the client's machine-id.
 *
 */
int main (int argc, char *argv[], char *envp[])
{
	char *cp;
	Mid_t clientmid;
	
	Argv0 = argv [0];
	Argv0size = strlen (Argv0);
#if	TEST
	sprintf (Argv0, "%-*s", Argv0size, "xNQS netserver");
#else
	sprintf (Argv0, "%-*s", Argv0size, "NQS netserver");
#endif
  
  	/* Initialise the debugging support */
  	close(1);
  	sal_debug_Init(1, Argv0, server_exit);

	if ((cp = getenv ("DEBUG")) != NULL)
  	  sal_debug_SetLevel(atoi(cp));
  
	if ((cp = getenv ("CLIENTMID")) == (char *) 0) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORAUTHOR, "Unable to locate environment variable CLIENTMID\n");
	}
	clientmid = atol (cp);
	if ((cp = getenv ("OP")) == (char *) 0) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORAUTHOR, "Unable to locate environment variable OP\n");
	}
  
	while (*argv != NULL) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver argv: %s\n", *argv);
		argv++;
	}
	while (*envp != NULL) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver envp: %s\n", *envp);
		envp++;
	}
  
	/*
	 * We ignore SIGPIPE. We MUST check the return value of all
	 * write() calls against the number we sought to write,
	 * not against -1.
	 */
	signal (SIGPIPE, SIG_IGN);
	/*
	 * Mvoutfileserver, quereqserver, and reqfileserver exit() when done.
	 */
	if (!strcmp (cp, "NPK_MVOUTFILE")) 
          mvoutfileserver ();
	else if (!strcmp (cp, "NPK_QUEREQ")) 
          quereqserver (clientmid);
	else if (!strcmp (cp, "NPK_REQFILE")) 
          reqfileserver ();
  	else
    	  NEVER;
  
  	return 0;
}


/*** mvoutfileserver
 *
 *
 *	void mvoutfileserver():
 *
 *	Catch one or more files that are being staged out.
 *	If our work is aborted before it is finished, do not
 *	unlink any of the files that are already here.
 *	
 */
static void mvoutfileserver ()
{

	struct passwd *passwd;		/* Pointer to password entry */
	int integers;			/* Number of integers */
	int strings;			/* Number of strings */
	char *path;			/* Where to put the file */
	int fd;				/* File descriptor */
	short done;			/* Boolean */

	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "netserver: starting mvoutfileserver()\n");

	if ((passwd = sal_fetchpwuid (getuid())) == (struct passwd *) 0) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "netserver: unable to get passwd struct for uid %d\n", getuid());
		sendtcm (TCMP_NOACCAUTH);
		server_exit ();
	}

	if (chdir (passwd->pw_dir) == -1) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "netserver: unable to chdir to %s for uid %d\n", passwd->pw_dir, getuid());
		sendtcm (setpeertcm (errnototcm ()));
		server_exit ();
	}

	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGLOW, "netserver: sending TCMP_CONTINUE");

	interset (0);
	sendtcm (TCMP_CONTINUE);
	/*
	 * We are now ready to work for the client
	 */
	setsockfd (0);			/* Tell interread we'll use fd #0 */
	/*
	 *  The client sends the following packets:
	 *  1: the daemon already saw this packet
	 *
	 *  2: this packet contains:
	 *	a) NPK_MVOUTFILE
	 *	b) mode
	 *	c) size
	 *	d) destination pathname
	 *
	 *  3 thru n-1: these optional packets contain:
	 *	a) the file mentioned previously
	 *	b) NPK_MVOUTFILE
	 *	c) mode
	 *	d) size
	 *	e) destination pathname
	 *
	 *  n: this packet contains:
	 *	a) the file mentioned previously
	 *	b) NPK_DONE
	 *	c) a place holder
	 *	d) a place holder
	 *	e) a place holder
	 *
	 *  We first process packet 2 and part of 3,
	 *  then the rest of packet 3 and part of 4,
	 *		   ...
	 *  then the rest of packet n-1 and all of n.
	 *
	 *  Get size information.
	 */
	errno = 0;			/* Set errno to zero for below */
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "netserver: waiting for packet\n");

	switch (interread (getsockch)) {
	case 0:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "netserver: interread() returned 0");
		break;
	case -1:
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "interread() returned -1\n");
	case -2:
		if (errno)
	          sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Netserver errno: %s.\n", asciierrno());
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "interread() returned -2\n");
	}
	integers = intern32i ();
	strings = internstr ();
	done = 0;
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "netserver: received packet: %d integers and %d strings\n", integers, strings);

	if (integers != 3 || strings != 1) {
		sendtcm (TCMP_PROTOFAIL);
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Expecting 3 integers and one string; received %d strings and %d integers\n", strings, integers);
	}
	for ( ;interr32i (1) == NPK_MVOUTFILE; ) {
		/*
		 * The client has told us about a file.  Get that file.
		 * int 1: NPK_MVOUTFILE
		 * int 2: mode (not umask)
		 * int 3: size
		 * str 1: destination
		 */
			       
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: mode = %d\n", interr32i (2));
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: size = %d\n", interr32i (3));
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: path = %s\n", interrstr (1));
	  
		path = interrstr (1);
		if ((fd = open (path, O_WRONLY | O_CREAT | O_TRUNC,
			interr32i (2) & 0777)) == -1) {
			sendtcm (setpeertcm (errnototcm ()));
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Unable to open file %s\nErrno = %d\n", path, errno);
		}
		sendtcm (TCMP_CONTINUE);
		errno = 0;
		if (filecopysized (0, fd, interr32i(3)) != interr32i (3)) {
			sendtcm (setpeertcm (errnototcm ()));
			unlink (path);
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "Exiting on bad return value from filecopysized()\n");
		}
		errno = 0;		/* Set errno to zero for below */
		switch (interread (getsockch)) {
		case 0:
			break;
		case -1:
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "interread() returned -1\n");
		case -2:
			if (errno) {
			  sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_INFO, "Netserver errno: %s.\n", asciierrno());
			}
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "interread() returned -2\n");
		}
		integers = intern32i ();
		strings = internstr ();
		if (integers != 3 || strings != 1) {
			sendtcm (TCMP_PROTOFAIL);
			server_exit();
		}
	}				/* End of for loop */
	sendtcm (TCMP_COMPLETE);
	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netserver sending TCMP_COMPLETE\n");
	server_exit ();
}


/*** quereqserver
 *
 *
 *	void quereqserver ():
 *
 *	Catch a request from a pipe queue.
 *	If our work is aborted before it is finished, do not
 *	unlink any of the files that are already here.
 */
static void quereqserver (Mid_t clientmid)
{

	char packedname[MAX_PATHNAME+1];/* Holds relative path name */
	struct rawreq rawreq;		/* To hold most of control file */
	struct stat statbuf;		/* Holds output of stat() */
	struct rawreq rawreq2;		/* Header already on server machine */
	long quereqtcm;			/* Transaction completion code */
	int ctrlfd;			/* Control file file descriptor */
	int integers;			/* Number of integers */
	int strings;			/* Number of strings */
	struct transact transact;	/* In-core transaction descriptor */
	long transactcc;		/* Transaction completion code */
	long ldbaltcm;                  /* Transaction completion code */

	interset (3);			/* This "inter" is unlike the others */
	sendtcm (TCMP_CONTINUE);
	/*
	 * We are now ready to work for the client
	 */
	setsockfd (0);			/* Tell interread we'll use fd #0 */
	/*
	 * The client sends the following packets
	 * as part of the enqueueing connection:
	 * 1: The daemon already saw this packet
	 * 2: This packet contains the control file header
	 *	An immense amount of information is received.
	 * 3: This packet commits the two phase commit
	 *
	 * The client sends the following packets
	 * as part of the request file delivery connection:
	 * 1: The daemon will see this packet later
	 * 2: reqfileserver will see this packet later
	 * 3 thru n-1: reqfileserver will see these packets later
	 * n: reqfileserver will see this packet later
	 */
	if (receivehdr (&rawreq) == -1) {
		sendtcm (TCMP_PROTOFAIL);
	  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "receivehdr() returned -1\n");
	}
	if (((ldbaltcm = ldbalsrv (&rawreq, sal_debug_GetLevel()))
		& XCI_FULREA_MASK) != TCML_SUBMITTED) {
		tcmmsgs (ldbaltcm, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: ldbaltcm ");
		sendtcm (setpeertcm (ldbaltcm));
		server_exit();
	}
	pack6name (packedname, Nqs_control,
		(int) (rawreq.orig_seqno % MAX_CTRLSUBDIRS),
		(char *) 0, rawreq.orig_seqno, 5, (long) rawreq.orig_mid,
		6, 0, 0);
	if (stat (packedname, &statbuf) == -1) {
		if (errno != ENOENT) {
			sendtcm (setpeertcm (errnototcm ()));
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORRESOURCE, "stat() on %s returned -1\nerrno is %d\n", packedname, errno);
		}
	}
	else if (statbuf.st_uid != getuid ()) {
		sendtcm (TCMP_REQCOLLIDE);
		server_exit ();
	}
	/*
	 * At this point, there are two possibilities:
	 * a) the file does not exist
	 * b) the file does exist, and we own it
	 */
	if ((ctrlfd = open (packedname, O_RDWR | O_CREAT | O_EXCL, 0744))
		== -1) {
		if (errno != EEXIST) {
			sendtcm (setpeertcm (errnototcm ()));
		  	sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_ERRORDATA, "open() on %s returned -1\nerrno = %d\n", packedname, errno);
		}
		/*
		 * There is a control file already here, and we own it.
		 */
		ctrlfd = getrreq (rawreq.orig_seqno, rawreq.orig_mid, &rawreq2);
		if (ctrlfd == -1) {
			if (errno != 0) {
				/*
				 * We are prevented from learning
				 * about the old request.
				 */
				sendtcm (setpeertcm (errnototcm ()));
				server_exit ();
			}
		}
		else {
			if (rawreq.create_time != rawreq2.create_time) {
				sendtcm (TCMP_REQCOLLIDE);
				server_exit ();
			}
		}
		/*
		 * The old request is either corrupt, or a duplicate of the
		 * request we are now working on. Delete the old request.
		 */
		switch (delreq (getuid (), rawreq.orig_seqno,
				rawreq.orig_mid, 0, 0, RQS_ARRIVING)) {
		case TCML_NOSUCHREQ:
			unlink (packedname);
			break;
		case TCML_REQDELETE:
			break;
		default:
			unlink (packedname);
			sendtcm (TCMP_UNAFAILURE);
			server_exit ();
		}
		/*
		 * The server machine has completely obliterated all
		 * traces of the request that was in conflict with us.
		 */
		if ((ctrlfd = open ( packedname,
			O_RDWR | O_CREAT | O_EXCL, 0744)) == -1) {
			sendtcm (setpeertcm (errnototcm ()));
			server_exit ();
		}
	}
	/*
	 * At this point, we have an open file descriptor,
	 * and any old request that might have been at the same
	 * path name is gone.
	 */
	if (writereq (ctrlfd, &rawreq) != 0) {
		unlink (packedname);
		sendtcm (setpeertcm (errnototcm ()));
		server_exit ();
	}
	/*
	 * At this point, the control file header is
	 * in the file system, and is not corrupt.
	 */
	interclear ();
	interw32i (rawreq.orig_seqno);
	interw32u (rawreq.orig_mid);
	interw32u (clientmid);
	/*
	 * Rawreq.quename was filled in with the name of the destination
	 * queue in receivehdr().
	 */
	interwstr (rawreq.quename);
	if (((quereqtcm = inter (PKT_RMTQUEREQ)) & XCI_FULREA_MASK)
		!= TCML_SUBMITTED) {
		tcmmsgs (quereqtcm, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: rmtquereq ");
		sendtcm (setpeertcm (quereqtcm));
		server_exit ();
	}
	/*
	 * The local daemon has just allocated a transaction descriptor
	 * and set the state of the transaction to RTS_PREARRIVE.
	 * Find out which transaction descriptor.
	 */
	if (readhdr (ctrlfd, &rawreq) != 0) {
		unlink (packedname);
		sendtcm (setpeertcm (errnototcm ()));
		server_exit ();
	}
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: sending TCMP_CONTINUE\n");
	sendtcm (TCMP_CONTINUE);
	/*
	 * Get packet 3 of the enqueueing connection.
	 */
	errno = 0;			/* Set errno to zero for below */
	switch (interread (getsockch)) {
	case 0:
		break;
	case -1:
		server_exit ();
	case -2:
		if (errno) 
	    		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netserver errno: %s.\n", asciierrno());
		server_exit();
	}
	integers = intern32i ();
	strings = internstr ();
	if (integers != 1 || strings != 0) {
		sendtcm (TCMP_PROTOFAIL);
		server_exit();
	}
	if (interr32i (1) != NPK_COMMIT) {
		sendtcm (TCMP_PROTOFAIL);
		server_exit ();
	}
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: after reading NPK_COMMIT\n");
	interclear ();
	interw32i (rawreq.orig_seqno);
	interw32u (rawreq.orig_mid);
	if ((transactcc = inter (PKT_RMTARRIVE)) != TCML_COMPLETE) {
	    tcmmsgs (transactcc, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: not complete ");
	    sendtcm (setpeertcm (transactcc));
	    server_exit ();
	}
	tra_read (rawreq.trans_id, &transact);
	transact.state = RTS_ARRIVE;
	tra_setstate (rawreq.trans_id, &transact);
	tcmmsgs (quereqtcm, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: state is arrive ");
	sendtcm (setpeertcm (quereqtcm));
	server_exit ();
}


/*** receivehdr 
 *
 *
 *	int receivehdr():
 *
 *	Receive NQS request header file from a remote client connected
 *	via a stream socket connection.
 *
 *	Returns:
 *		 0: if successful;
 *		-1: otherwise (connection severed).
 */
static int receivehdr (struct rawreq *rawreq)
{
	typedef unsigned long longcard;	/* Less typing */

	
	register short i;		/* Loop and temp var */
	int n_integers;			/* Number of packet integers */
	int n_strings;			/* Number of packet strings */

	i = interread (getsockch);
	if (i < 0) {
		/*
		 *  The client process terminated, or sent us a bad
		 *  packet (i == -1 is EOF).
		 */
		if (i == -2) {
			/*
			 *  Bad message packet received from client.
			 *  Inform client.
			 */
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netserver: Inconsistent packet recv'd.\n");
		}
		return (-1);		/* No request */
	}
	/*
	 *  We got a self-consistent packet.
	 */
	n_integers = intern32i();	/* #of packet integers */
	n_strings = internstr();	/* #of packet strings */
	if (n_integers == 16 + MAX_DEVPREF && n_strings == 5 + MAX_DEVPREF) {
		/*
		 *  The request had better be a device request!
		 */
		if (unpackheader (rawreq) >= 0 &&
		    rawreq->type == RTYPE_DEVICE && !interr32sign (16)) {
			/*
			 *  The common request header was successfully
			 *  unpacked, and no fields in the device variant
			 *  have been detected as invalid (yet).
			 */
			strncpy (rawreq->v.dev.forms, interrstr (5),
				 MAX_FORMNAME+1);
			rawreq->v.dev.copies = interr32i (13);
			rawreq->v.dev.reserved1 = interr32i (14);
			rawreq->v.dev.reserved2 = interr32i (15);
			rawreq->v.dev.size = interr32u (16);
			for (i = 0; i < MAX_DEVPREF; i++) {
				strncpy (rawreq->v.dev.devprefname [i],
					 interrstr (6+i), MAX_DEVNAME+1);
				rawreq->v.dev.devprefmid [i] = interr32u(17+i);
			}
			if (verifyreq (rawreq) >= 0) {
				/*
				 *  The request is valid.
				 */
				return (0);	/* Success */
			}
		}
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netserver: Invalid device request packet.\n");
		return (-1);		/* No request */
	}
	else if (n_integers != 84 + MAX_INSTAPERREQ + MAX_OUSTAPERREQ ||
		n_strings != 8 + MAX_PREDECESSOR) {
		/*
		 *  We have a protocol failure.  The packet is invalid.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netserver: non-batch, non-device packet.\n");
		return (-1);		/* No request */
	}
	/*
	 *  The request had better be a batch request!
	 */
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Receivehdr: Rawreq type is %d\n", rawreq->type);
  
	if (unpackheader (rawreq) >= 0 && rawreq->type == RTYPE_BATCH) {
		/*
		 *  The common request header was successfully unpacked,
		 *  and no fields in the batch variant have been detected
		 *  as invalid (yet).
		 *
		 */
		/*
		 *  No quota coefficients were detected to be out of
		 *  range.  Unpack the batch request variant fields.
		 */
		rawreq->v.bat.umask = interr32i (13);
		strncpy (rawreq->v.bat.shell_name, interrstr (5),
				 MAX_SHELLNAME+1);
		rawreq->v.bat.explicit = interr32i (14);
		rawreq->v.bat.infinite = interr32i (15);
		/*
		 *  Unpack per-process corefile size limits.
		 */
		rawreq->v.bat.ppcoresize.max_quota = enf_max(interr32u (16));
		rawreq->v.bat.ppcoresize.max_units = interr32i (17);
		rawreq->v.bat.ppcoresize.warn_quota = enf_max(interr32u (18));
		rawreq->v.bat.ppcoresize.warn_units = interr32i (19);
		/*
		 *  Unpack per-process data-segment size limits.
		 */
		rawreq->v.bat.ppdatasize.max_quota = enf_max(interr32u (20));
		rawreq->v.bat.ppdatasize.max_units = interr32i (21);
		rawreq->v.bat.ppdatasize.warn_quota = enf_max(interr32u (22));
		rawreq->v.bat.ppdatasize.warn_units = interr32i (23);
		/*
		 *  Unpack per-process permanent file size limits.
		 */
		rawreq->v.bat.pppfilesize.max_quota = enf_max(interr32u (24));
		rawreq->v.bat.pppfilesize.max_units = interr32i (25);
		rawreq->v.bat.pppfilesize.warn_quota = enf_max(interr32u (26));
		rawreq->v.bat.pppfilesize.warn_units = interr32i (27);
		/*
		 *  Unpack per-request permanent file space limits.
		 */
		rawreq->v.bat.prpfilespace.max_quota = enf_max(interr32u (28));
		rawreq->v.bat.prpfilespace.max_units = interr32i (29);
		rawreq->v.bat.prpfilespace.warn_quota = enf_max(interr32u (30));
		rawreq->v.bat.prpfilespace.warn_units = interr32i (31);
		/*
		 *  Unpack per-process quick file size limits.
		 */
		rawreq->v.bat.ppqfilesize.max_quota = enf_max(interr32u (32));
		rawreq->v.bat.ppqfilesize.max_units = interr32i (33);
		rawreq->v.bat.ppqfilesize.warn_quota = enf_max(interr32u (34));
		rawreq->v.bat.ppqfilesize.warn_units = interr32i (35);
		/*
		 *  Unpack per-request quick file space limits.
		 */
		rawreq->v.bat.prqfilespace.max_quota = enf_max(interr32u (36));
		rawreq->v.bat.prqfilespace.max_units = interr32i (37);
		rawreq->v.bat.prqfilespace.warn_quota = enf_max(interr32u (38));
		rawreq->v.bat.prqfilespace.warn_units = interr32i (39);
		/*
		 *  Unpack per-process temporary file size limits.
		 */
		rawreq->v.bat.pptfilesize.max_quota = enf_max(interr32u (40));
		rawreq->v.bat.pptfilesize.max_units = interr32i (41);
		rawreq->v.bat.pptfilesize.warn_quota = enf_max(interr32u (42));
		rawreq->v.bat.pptfilesize.warn_units = interr32i (43);
		/*
		 *  Unpack per-request temporary file space limits.
		 */
		rawreq->v.bat.prtfilespace.max_quota = enf_max(interr32u (44));
		rawreq->v.bat.prtfilespace.max_units = interr32i (45);
		rawreq->v.bat.prtfilespace.warn_quota = enf_max(interr32u (46));
		rawreq->v.bat.prtfilespace.warn_units = interr32i (47);
		/*
		 *  Unpack per-process memory size limits.
		 */
		rawreq->v.bat.ppmemsize.max_quota = enf_max(interr32u (48));
		rawreq->v.bat.ppmemsize.max_units = interr32i (49);
		rawreq->v.bat.ppmemsize.warn_quota = enf_max(interr32u (50));
		rawreq->v.bat.ppmemsize.warn_units = interr32i (51);
		/*
		 *  Unpack per-request memory size limits.
		 */
		rawreq->v.bat.prmemsize.max_quota = enf_max(interr32u (52));
		rawreq->v.bat.prmemsize.max_units = interr32i (53);
		rawreq->v.bat.prmemsize.warn_quota = enf_max(interr32u (54));
		rawreq->v.bat.prmemsize.warn_units = interr32i (55);
		/*
		 *  Unpack per-process stack-segment size limits.
		 */
		rawreq->v.bat.ppstacksize.max_quota = enf_max(interr32u (56));
		rawreq->v.bat.ppstacksize.max_units = interr32i (57);
		rawreq->v.bat.ppstacksize.warn_quota = enf_max(interr32u (58));
		rawreq->v.bat.ppstacksize.warn_units = interr32i (59);
		/*
		 *  Unpack per-process working set size limits.
		 */
		rawreq->v.bat.ppworkset.max_quota = enf_max(interr32u (60));
		rawreq->v.bat.ppworkset.max_units = interr32i (61);
		rawreq->v.bat.ppworkset.warn_quota = enf_max(interr32u (62));
		rawreq->v.bat.ppworkset.warn_units = interr32i (63);
		/*
		 *  Unpack per-process CPU time limits.
		 */
		rawreq->v.bat.ppcputime.max_seconds = enf_max(interr32u (64));
		rawreq->v.bat.ppcputime.max_ms = interr32i (65);
		rawreq->v.bat.ppcputime.warn_seconds = enf_max(interr32u (66));
		rawreq->v.bat.ppcputime.warn_ms = interr32i (67);
		/*
		 *  Unpack per-request CPU time limits.
		 */
		rawreq->v.bat.prcputime.max_seconds = enf_max(interr32u (68));
		rawreq->v.bat.prcputime.max_ms = interr32i (69);
		rawreq->v.bat.prcputime.warn_seconds = enf_max(interr32u (70));
		rawreq->v.bat.prcputime.warn_ms = interr32i (71);
		/*
		 *  Unpack remaining batch request fields.
		 */
		rawreq->v.bat.ppnice = interr32i (72);
		rawreq->v.bat.prdrives = interr32i (73);
		rawreq->v.bat.prncpus = interr32i (74);
		for (i=0; i < MAX_PREDECESSOR; i++) {
			strncpy (rawreq->v.bat.predecessors [i],
				 interrstr (i + 6), MAX_REQNAME+1);
		}
		rawreq->v.bat.stderr_acc = interr32i (75);
		rawreq->v.bat.stdlog_acc = interr32i (76);
		rawreq->v.bat.stdout_acc = interr32i (77);
		rawreq->v.bat.stderr_mid = interr32u (78);
		rawreq->v.bat.stdlog_mid = interr32u (79);
		rawreq->v.bat.stdout_mid = interr32u (80);
		strncpy (rawreq->v.bat.stderr_name,
			 interrstr (MAX_PREDECESSOR+6), MAX_REQPATH+1);
		strncpy (rawreq->v.bat.stdlog_name,
			 interrstr (MAX_PREDECESSOR+7), MAX_REQPATH+1);
		strncpy (rawreq->v.bat.stdout_name,
			 interrstr (MAX_PREDECESSOR+8), MAX_REQPATH+1);
		rawreq->v.bat.instacount = interr32i (81);
		rawreq->v.bat.oustacount = interr32i (82);
		rawreq->v.bat.instahiermask = interr32i (83);
		rawreq->v.bat.oustahiermask = interr32i (84);
		for (i=0; i < MAX_INSTAPERREQ; i++) {
			rawreq->v.bat.instamid [i] = interr32u (i+85);
		}
		for (i=0; i < MAX_OUSTAPERREQ; i++) {
			rawreq->v.bat.oustamid [i]
				= interr32u (i + MAX_INSTAPERREQ + 85);
		}
		if (verifyreq (rawreq) >= 0) {
			/*
			 *  The request is valid.
			 */
			return (0);	/* Success */
		} else {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Receivehdr: verifyreq failed!!!\n");
		}

	}
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netserver: Invalid batch request packet.\n");
	return (-1);			/* No request to queue */
}


/*** reqfileserver
 *
 *
 *	void reqfileserver ():
 *
 *	Catch the tail of a control file and all
 *	the data files associated with a request.
 *	If our work is aborted before it is finished, do not
 *	unlink any of the files that are already here.
 */
static void reqfileserver (void)
{
 
	int integers;			/* Number of integers */
	int strings;			/* Number of strings */
	struct rawreq rawreq;		/* To hold most of control file */
	char packedname [29];		/* Holds relative path name */
					/* dir:14, file:14, slash:1 */
	struct stat statbuf;		/* Holds stat() output */
	int ctrlfd;			/* Control file file descriptor */
	long headersize;		/* Bytes in control file header */
	int datafd;			/* Data file file descriptor */
	struct transact transact;	/* In-core transaction descriptor */
	long transactcc;		/* Transaction completion code */

	interset (3);			/* This "inter" is unlike the others */
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "reqfileserver: sending TCMP_CONTINUE in response to first NPK_REQFILE packet\n");
	sendtcm (TCMP_CONTINUE);
	/*
	 * We are now ready to work for the client
	 */
	setsockfd (0);			/* Tell interread we'll use fd #0 */
	/*
	 *  The client sends the following packets in the
	 *  enqueueing connection:
	 *  1: The daemon already saw this packet
	 *  2: quereqserver already saw this packet
	 *  3: quereqserver already saw this packet
	 *
	 *  The client sends the following packets in the delivery
	 *  connection:
	 *  1: The daemon already saw this packet
	 *  2: This packet contains 
	 *	a) packet type (NPK_REQFILE)
	 *	b) ordinal number of file (-1 == tail of control file)
	 *	c) number of bytes in file
	 *	d) original sequence number
	 *	e) original mid
	 *
	 *  3 thru n-1: These optional packets contain:
	 *	a) the file mentioned previously
	 *	b) packet type (NPK_REQFILE)
	 *	c) ordinal number of file
	 *	d) number of bytes in file
	 *
	 *  n: This packet contains:
	 *	a) the file mentioned previously
	 *	b) packet type (NPK_DONE)
	 *	c) a place holder
	 *	d) a place holder
	 *
	 *  Get packet 2 of the deliver connection.
	 */
	errno = 0;			/* Set to zero for below */
	switch (interread (getsockch)) {
	case 0:
		break;
	case -1:
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: interread() returned -1 when looking for packet #2\n");
		server_exit ();
		break;
	case -2:
		if (errno) 
	    		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netserver errno: %s.\n", asciierrno());
		else
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: interread() returned -2 when looking for packet #2\n");
		server_exit();
	}
	integers = intern32i ();
	strings = internstr ();
	if (integers != 5 || strings != 0) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: expected 5 integers and 0 strings; received %d integers and %d strings\n", integers, strings);
		sendtcm (TCMP_PROTOFAIL);
		server_exit();
	}
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: NPK = %d\n", interr32i (1));
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: data# = %d\n", interr32i (2));
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: bytes = %d\n", interr32i (3));
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: seqno = %d\n", interr32i (4));
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: mid = %u\n", interr32u (5));
  
	pack6name (packedname, Nqs_control,
		(int) (interr32i (4) % MAX_CTRLSUBDIRS), (char *) 0,
		interr32i (4), 5, interr32u (5), 6, 0, 0);
	if (stat (packedname, &statbuf) == -1) {
		/*
		 * It might be that a previous delivery worked, the
		 * client machine crashed before hearing the news,
		 * and the batch request has already run to completion.
		 */
		if (errno == ENOENT) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: could not find request seqno %d mid %d\n", interr32i(4), interr32u(5) );
			sendtcm (TCMP_NOSUCHREQ);
		}
		else
		{
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: unable to stat control file of request seqno %d mid %d\n", interr32i(4), interr32u(5));
			sendtcm (setpeertcm (errnototcm ()));
		}
		server_exit ();
	}
	if (statbuf.st_uid != getuid ()) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: control file is owned by uid '%d' - should be owned by uid '%d'\n", statbuf.st_uid, getuid());
		sendtcm (TCMP_REQCOLLIDE);
		server_exit ();
	}
	/*
	 * At this point, we know that the control file exists,
	 * and that we own it.
	 */
	if ((ctrlfd = open (packedname, O_RDWR)) == -1) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: unable to open control file '%s'\n", packedname);
		sendtcm (setpeertcm (errnototcm ()));
		server_exit ();
	}
	if (readreq (ctrlfd, &rawreq) == -1) {
		sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: unable to read control file '%s'\n", packedname);
		sendtcm (TCMP_BADCDTFIL);
		server_exit ();
	}
	/*
	 * It might be that a previous delivery worked, and
	 * the client machine crashed before hearing the news.
	 */
	tra_read (rawreq.trans_id, &transact);
	if (transact.state != RTS_ARRIVE) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "Netserver: caught 2nd delivery.\n");
		sendtcm (TCMP_COMPLETE);
		server_exit ();
	}
	/*
	 * Below here, ctrlfd is still open
	 */
	for ( ;interr32i (1) == NPK_REQFILE; ) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "Netserver: catch %d bytes\n", interr32i (3));
		if (interr32i (2) == -1) {
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "reqfileserver: packet is control file tail\n");
			/*
			 * Tail of control file
			 */
			headersize = ((char *) &rawreq.v - (char *) &rawreq);
			if (rawreq.type == RTYPE_DEVICE) {
				headersize += sizeof (struct rawdevreq);
			}
			else {
				headersize += sizeof (struct rawbatreq);
			}
			lseek (ctrlfd, (long) headersize, 0);
			sendtcm (TCMP_CONTINUE);
			errno = 0;
			if (filecopysized (0, ctrlfd, interr32i(3))
				!= interr32i (3)) {
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: filecopysized() failed whilst writing control file tail\n");
				sendtcm (setpeertcm (errnototcm ()));
				server_exit ();
			}
		}
		else {
			/*
			 * Data file
			 * If there are n data files, interr32i(2)
			 * will range from [0..n-1].
			 */

			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM, "reqfileserver: packet is data file #%d\n", interr32i(2));
			if (interr32i (2) >= rawreq.ndatafiles) {
				sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: too many datafiles (this request only has %d)\n", rawreq.ndatafiles);
				sendtcm (TCMP_BADCDTFIL);
				server_exit ();
			}
			pack6name (packedname, Nqs_data,
				(int) (rawreq.orig_seqno % MAX_DATASUBDIRS),
				(char *) 0, rawreq.orig_seqno, 5,
				(long) rawreq.orig_mid, 6, interr32i (2), 3);
			errno = 0;
			if ((datafd = creat (packedname, 0777)) < 0) {
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: unable to open datafile '%s'\n", packedname);
				sendtcm (setpeertcm (errnototcm ()));
				server_exit ();
			}
			else sendtcm (TCMP_CONTINUE);
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGMEDIUM,
					"netserver: datafilename is %s\n",
					packedname);
			errno = 0;
			if (filecopysized (0, datafd, interr32i(3))
				!= interr32i (3)) {
				sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: filecopysized() failed whilst writing datafile '%s'\n", packedname);
				sendtcm (setpeertcm (errnototcm ()));
				server_exit ();
			}
		}
		errno = 0;		/* Set to zero for below */
		switch (interread (getsockch)) {
		case 0:
			break;
		case -1:
			sal_dprintf(SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: interread() returned -1 whilst waiting for next control file/datafile packet\n");
			server_exit ();
		case -2:
			if (errno) {
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netserver errno: %s.\n",
					 asciierrno());
			}
			else
				sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfilename: interread() returned -2 whilst waiting for next control file/datafile packet\n");
			server_exit();
		}
		integers = intern32i ();
		strings = internstr ();
		if (integers != 3 || strings != 0) {
			sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfile: expected 3 integers and 0 strings; received %d integers and %d strings\n", integers, strings);
			sendtcm (TCMP_PROTOFAIL);
			server_exit();
		}
	}
	/*
	 * We just saw NPK_DONE.
	 */
	interclear ();
	interw32i (rawreq.orig_seqno);
	interw32u (rawreq.orig_mid);
	if ((transactcc = inter (PKT_RMTRECEIVED)) != TCML_COMPLETE) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "reqfileserver: remote destination does not report TCML_COMPLETE - reports %d instead\n", transactcc);
		sendtcm (setpeertcm (transactcc));
		server_exit ();
	}
	transact.state = RTS_STASIS;
	tra_setstate (rawreq.trans_id, &transact);
	sendtcm (TCMP_COMPLETE);
	server_exit ();
}


/*** sendtcm
 *
 *	void sendtcm:
 *
 *	Send on file descriptor 0 a packet containing nothing but a
 *	transaction completion code.  If there is trouble, exit.
 */
static void sendtcm (long tcm)
{
	char packet [MAX_PACKET];	/* To hold our reply */
	int packetsize;			/* Bytes in our reply */
	int actual_ps;			/* Actual size of write */
	
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "sendtcm: called with TCM code: %X\n", tcm);
	interclear ();
	interw32i (tcm);
	if ((packetsize = interfmt (packet)) == -1) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netserver: interfmt.\n");
		server_exit ();
	}
	actual_ps = write (0, packet, packetsize);
	if (actual_ps != packetsize) {
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "Netserver: sending %o, write %d != %d.\n",
			tcm, packetsize, actual_ps);
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "E$Netserver: errno = %d.\n", errno);
		server_exit ();
	}
}


/*** server_exit
 *
 *	void server_exit:
 *
 *	Exit gracefully.
 */
static void server_exit ()
{
	sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_DEBUGHIGH, "server_exit: called\n");
	exiting ();			/* Unlink IPC file, close FIFO */
	exit (0);
}


/*** unpackheader
 *
 *
 *	int unpackheader():
 *	Unpack common request header from message packet.
 *
 *	Returns:
 *		 0: if successful;
 *		-1: otherwise.
 */
static int unpackheader (struct rawreq *rawreq)
{
	if (interr32sign (2) || interr32sign (3) || interr32sign (10)) {
		/*
		 *  Negative values encountered where only positive
		 *  numbers ( >= 0 ) are allowed.
		 */
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "unpackheader: negative values 2/3/10\n");
		sal_dprintf (SAL_DEBUG_INFO, SAL_DEBUG_MSG_WARNING, "%d / %d / %d\n",
			interr32sign (2),
			interr32sign (3),
			interr32sign (10) );
		return (-1);
	}
	rawreq->magic1 = interr32i (1);
	rawreq->trans_id = 0;
	rawreq->reserved1 = 0;
	rawreq->reserved2 = 0;
	rawreq->reserved3 = 0;
	rawreq->reserved4 = 0;
	rawreq->reserved5 = 0;
	rawreq->reserved6 = 0;
	strncpy (rawreq->trans_quename, "", MAX_QUEUENAME + 1);
	rawreq->create_time = interr32u (2);
	rawreq->enter_time = interr32u (3);
	strncpy (rawreq->quename, interrstr (1), MAX_QUEUENAME+1);
	rawreq->type = interr32i (4);
	rawreq->orig_uid = interr32i (5);
	rawreq->orig_mid = interr32u (6);
	rawreq->tcm = TCML_UNDEFINED;
	rawreq->orig_seqno = interr32i (7);
	rawreq->rpriority = interr32i (8);
	rawreq->flags = (interr32i (9) | RQF_EXTERNAL);
	rawreq->start_time = interr32u (10);
	rawreq->ndatafiles = interr32i (11);
	strncpy (rawreq->reqname, interrstr (2), MAX_REQNAME+1);
	strncpy (rawreq->username, interrstr (3), MAX_ACCOUNTNAME+1);
	strncpy (rawreq->mail_name, interrstr (4), MAX_ACCOUNTNAME+1);
	rawreq->mail_mid = interr32u (12);
	return (0);			/* No errors detected */
}

/*
 * This routine checks to make sure the quota sent over from
 * the remote system is no more than the maximum unsigned number
 * in a 32 bit number.  If the quota exceeds that value, the
 * maximum value is returned.
 */
static unsigned long 
enf_max ( quota )
unsigned long quota;
{
    if (quota > MAXIMUM_UNSIGNED32 ) return ( MAXIMUM_UNSIGNED32 );
    else return (quota);
}
