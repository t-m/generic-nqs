#ifndef __LIBGNQS_NETNQS1_MESSAGES_HEADER
#define __LIBGNQS_NETNQS1_MESSAGES_HEADER

/*
 * libgnqs/net-nqs1/messages.h
 *		List of all "messages" which the GNQS process can receive
 *
 *		Part of Generic NQS.
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1999 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * List of all messages for NQS Network Protocol v1
 *
 * The NQS Network Protocol v1 consists of two distinct sets of message
 * types:
 *
 *	NQSNET1_MSG_I_<name>
 *		are messages which can only be received on the local
 *		UNIX domain socket
 *
 *		i.e. these messages are only valid when sent from command-line
 *		utilities to the main GNQS daemon.
 *
 *	NQSNET1_MSG_E_<name>
 *		are messages which can be received on the well-known TCP
 *		socket
 *
 *		i.e. these messages are only valid when sent from one GNQS
 *		daemon to another.
 *
 * _I_ and _E_ messages can have the same ID number, provided that all _I_
 * values are different, and that all _E_ numbers are different.
 *
 * HINT:
 *
 *	There is a validation table (in tables.c) for each message type 
 *	supported by the NQS Network Protocol v1, and for each internal
 *	message type too.
 *
 *	The validation table includes a description of what the message
 *	is for.  This can be output to the GNQS log file for debugging
 *	purposes, to aid developers new to the source base.  These
 *	messages appear when:
 *
 *		DEBUG_NQSNET1		is set to 'high'
 *		DEBUG_NQSNET1_MSGDESC	is set to 'on'
 */

/* the following message types are historical */

#define NQSNET1_MSG_E_QUEREQ		0
#define NQSNET1_MSG_E_DELREQ		1
#define NQSNET1_MSG_E_COMMIT		2
#define NQSNET1_MSG_E_DONE		100
#define NQSNET1_MSG_E_MKDIR		101
#define NQSNET1_MSG_E_REQFILE		102
#define NQSNET1_MSG_E_CPINFILE		103
#define NQSNET1_MSG_E_CPOUTFILE		104
#define NQSNET1_MSG_E_MVINFILE		105
#define NQSNET1_MSG_E_MVOUTFILE		106
#define NQSNET1_MSG_E_QSTAT		203
#define NQSNET1_MSG_E_REQCOM		205

/*
 * the following message types have been added during the drafting of
 * the NQS Network Protocol (v1) RFC document
 */

#define NQSNET1_MSG_E_VENDORINFO	254
#define NQSNET1_MSG_E_VENDOREXT		255

/*
 * all other message types are invalid as of GNQS v3.51
 */

/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * So-called "internal" packet types
 *
 * This is the list of messages which the GNQS daemon will send (and accept)
 * on the well-known UNIX domain socket it listens on.
 *
 * Compatibility from version to version is NOT guaranteed!!
 *
 * In particular, it's quite likely this list will shorten as v2 of the NQS
 * Network Protocol is introduced ...
 */

#define NQSNET1_MSG_I_ABOQUE		0
#define NQSNET1_MSG_I_ADDFOR		1
#define NQSNET1_MSG_I_ADDNQSMAN		2
#define NQSNET1_MSG_I_ADDQUEDES		3
#define NQSNET1_MSG_I_ADDQUEDEV		4
#define NQSNET1_MSG_I_ADDQUEGID		5
#define NQSNET1_MSG_I_ADDQUEUID		6
#define NQSNET1_MSG_I_CREDEV		7
#define NQSNET1_MSG_I_CREBATQUE		8
#define NQSNET1_MSG_I_CREDEVQUE		9
#define NQSNET1_MSG_I_CREPIPQUE		10
#define NQSNET1_MSG_I_DELDEV		11
#define NQSNET1_MSG_I_DELFOR		12
#define NQSNET1_MSG_I_DELNQSMAN		13
#define NQSNET1_MSG_I_DELQUE		14
#define NQSNET1_MSG_I_DELQUEDES		15
#define NQSNET1_MSG_I_DELQUEDEV		16
#define NQSNET1_MSG_I_DELQUEGID		17
#define NQSNET1_MSG_I_DELQUEUID		18
#define NQSNET1_MSG_I_DELREQ		19
#define NQSNET1_MSG_I_DISDEV		20
#define NQSNET1_MSG_I_DISQUE		21
#define NQSNET1_MSG_I_ENADEV		22
#define NQSNET1_MSG_I_ENAQUE		23
#define NQSNET1_MSG_I_FAMILY		24
#define NQSNET1_MSG_I_LOCDAE		25
#define NQSNET1_MSG_I_MODREQ		26
#define NQSNET1_MSG_I_MOVREQ		27
#define NQSNET1_MSG_I_PURQUE		28
#define NQSNET1_MSG_I_QUEREQ		29
#define NQSNET1_MSG_I_QUEREQVLPQ	30
#define NQSNET1_MSG_I_REQCOM		31
#define NQSNET1_MSG_I_RMTACCEPT		32
#define NQSNET1_MSG_I_RMTARRIVE		33
#define NQSNET1_MSG_I_RMTDEPART		34
#define NQSNET1_MSG_I_RMTENADES		35
#define NQSNET1_MSG_I_RMTENAMAC		36
#define NQSNET1_MSG_I_RMTFAIDES		37
#define NQSNET1_MSG_I_RMTFAIMAC		38
#define NQSNET1_MSG_I_RMTQUEREQ		39
#define NQSNET1_MSG_I_RMTRECEIVED	40
#define NQSNET1_MSG_I_RMTSCHDES		41
#define NQSNET1_MSG_I_RMTSCHMAC		42
#define NQSNET1_MSG_I_RMTSTASIS		43
#define NQSNET1_MSG_I_SCHEDULEREQ	44
#define NQSNET1_MSG_I_SENSEDAEMON	45
#define NQSNET1_MSG_I_SETDEB		46
#define NQSNET1_MSG_I_SETDEFBATPR	47
#define NQSNET1_MSG_I_SETDEFBATQU	48
#define NQSNET1_MSG_I_SETDEFDESTI	49
#define NQSNET1_MSG_I_SETDEFDESWA	50
#define NQSNET1_MSG_I_SETDEFDEVPR	51
#define NQSNET1_MSG_I_SETDEFPRIFO	52
#define NQSNET1_MSG_I_SETDEFPRIQU	53
#define NQSNET1_MSG_I_SETDEVFOR		54
#define NQSNET1_MSG_I_SETDEVSER		55
#define NQSNET1_MSG_I_SETFOR		56
#define NQSNET1_MSG_I_SETLIFE		57
#define NQSNET1_MSG_I_SETLOGFIL		58
#define NQSNET1_MSG_I_SETMAXCOP		59
#define NQSNET1_MSG_I_SETMAXOPERE	60
#define NQSNET1_MSG_I_SETMAXPRISI	61
#define NQSNET1_MSG_I_SETNDFBATQU	62
#define NQSNET1_MSG_I_SETNDFPRIFO	63
#define NQSNET1_MSG_I_SETNDFPRIQU	64
#define NQSNET1_MSG_I_SETNETCLI		65
#define NQSNET1_MSG_I_SETNETDAE		66
#define NQSNET1_MSG_I_SETNETSER		67
#define NQSNET1_MSG_I_SETNONETDAE	68
#define NQSNET1_MSG_I_UNDEFINED1	69
#define NQSNET1_MSG_I_SETNOQUEACC	70
#define NQSNET1_MSG_I_SETNQSMAI		71
#define NQSNET1_MSG_I_SETNQSMAN		72
#define NQSNET1_MSG_I_SETOPEWAI		73
#define NQSNET1_MSG_I_SETPIPCLI		74
#define NQSNET1_MSG_I_SETPIPONL		75
#define NQSNET1_MSG_I_SETPPCORE		76
#define NQSNET1_MSG_I_SETPPCPUT		77
#define NQSNET1_MSG_I_SETPPDATA		78
#define NQSNET1_MSG_I_SETPPMEM		79
#define NQSNET1_MSG_I_SETPPNICE		80
#define NQSNET1_MSG_I_SETPPPFILE	81
#define NQSNET1_MSG_I_SETPPQFILE	82
#define NQSNET1_MSG_I_SETPPSTACK	83
#define NQSNET1_MSG_I_SETPPTFILE	84
#define NQSNET1_MSG_I_SETPPWORK		85
#define NQSNET1_MSG_I_SETPRCPUT		86
#define NQSNET1_MSG_I_SETPRDRIVES	87
#define NQSNET1_MSG_I_SETPRMEM		88
#define NQSNET1_MSG_I_SETPRNCPUS	89
#define NQSNET1_MSG_I_SETPRPFILE	90
#define NQSNET1_MSG_I_SETPRQFILE	91
#define NQSNET1_MSG_I_SETPRTFILE	92
#define NQSNET1_MSG_I_SETQUEDES		93
#define NQSNET1_MSG_I_SETQUEDEV		94
#define NQSNET1_MSG_I_SETQUEPRI		95
#define NQSNET1_MSG_I_SETQUERUN		96
#define NQSNET1_MSG_I_SETSHSFIX		97
#define NQSNET1_MSG_I_SETSHSFRE		98
#define NQSNET1_MSG_I_SETSHSLOG		99
#define NQSNET1_MSG_I_SETUNRQUEAC	100
#define NQSNET1_MSG_I_SHUTDOWN		101
#define NQSNET1_MSG_I_STAQUE		102
#define NQSNET1_MSG_I_STOQUE		103
#define NQSNET1_MSG_I_UNLDAE		104
#define NQSNET1_MSG_I_SETQUENDP		105
#define NQSNET1_MSG_I_SETGBATLIM	106
#define NQSNET1_MSG_I_SETQUEUSR		107
#define NQSNET1_MSG_I_ADDQUECOM		108
#define NQSNET1_MSG_I_CRECOM		109
#define NQSNET1_MSG_I_DELCOM		110
#define NQSNET1_MSG_I_MOVQUE		111
#define NQSNET1_MSG_I_REMQUECOM		112
#define NQSNET1_MSG_I_SETCOMLIM		113
#define NQSNET1_MSG_I_SETCPUACC		114
#define NQSNET1_MSG_I_SUSPENDREQ	115
#define NQSNET1_MSG_I_HOLDREQ		116
#define NQSNET1_MSG_I_RELREQ		117
#define NQSNET1_MSG_I_SETPIPLIM		118
#define NQSNET1_MSG_I_VERQUEUSR		119
#define NQSNET1_MSG_I_SETQUECHAR	120
#define NQSNET1_MSG_I_SETNOQUECHAR	121
#define NQSNET1_MSG_I_SETCOMUSERLIM	122
#define NQSNET1_MSG_I_MEMDUMP		123
#define NQSNET1_MSG_I_LOAD		124
#define NQSNET1_MSG_I_RREQCOM		125
#define NQSNET1_MSG_I_SETNQSSCHED	126
#define NQSNET1_MSG_I_SETDEFLOADINT	127
#define NQSNET1_MSG_I_CTLDAE		128
#define NQSNET1_MSG_I_SETLOADDAE	129
#define NQSNET1_MSG_I_ALTERREQ		130
#define NQSNET1_MSG_I_SETSERVPERF	131
#define NQSNET1_MSG_I_SETSERVAVAIL	132

/* the following messages have all been added for GNQS v4.0-pre ... */

#define NQSNET1_MSG_I_END		133

#endif /* __LIBGNQS_NETNQS1_MESSAGES_HEADER */
