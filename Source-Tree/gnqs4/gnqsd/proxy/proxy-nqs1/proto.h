#ifndef __LIBGNQS_NETNQS1_PROTO_HEADER
#define __LIBGNQS_NETNQS1_PROTO_HEADER

/*
 * libgnqs/net-nqs1/all-systems/proto.h
 *		ANSI C prototypes for NQS network protocol v1 functions
 *
 *		Part of Generic NQS
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1999 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#endif /* __LIBGNQS_NETNQS1_PROTO_HEADER */
