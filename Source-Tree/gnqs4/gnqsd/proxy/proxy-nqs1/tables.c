/*
 * libgnqs/net-nqs1/tables.c
 *		Tables describing the NQS network protocol v1
 *
 *		Part of Generic NQS
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1999 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <libgnqs/license.h>
#include <libgnqs/libgnqs.h>

/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Validation Tables
 *
 * We use these tables to describe how to validate each individual
 * network message which we accept.
 */

/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Packet Table
 *
 * This table lists all of the messages which we can receive, where we
 * can receive them from, and the validation table to use for each message.
 */

struct libgnqs_netnqs1_pkttable stTable {
	/* PKT NUMBER		| TYPE		| VALIDATION		*/
	{ NQSNET1_MSG_E_QUEREQ,		NQSNET_EXT,	},
	{ NQSNET1_MSG_E_DELREQ,		NQSNET_EXT,	},
	{ NQSNET1_MSG_E_COMMIT,		NQSNET_EXT,	},
	{ NQSNET1_MSG_E_DONE,		NQSNET_EXT,	},
	{ NQSNET1_MSG_E_MKDIR,		NQSNET_EXT,	},
	{ NQSNET1_MSG_E_REQFILE,	NQSNET_EXT,	},
	{ NQSNET1_MSG_E_CPINFILE,	NQSNET_EXT,	},
	{ NQSNET1_MSG_E_CPOUTFILE,	NQSNET_EXT,	},
	{ NQSNET1_MSG_E_MVINFILE,	NQSNET_EXT,	},
	{ NQSNET1_MSG_E_MVOUTFILE,	NQSNET_EXT,	},
	{ NQSNET1_MSG_E_QSTAT,		NQSNET_EXT,	},
	{ NQSNET1_MSG_E_REQCOM,		NQSNET_EXT,	},
	{ NQSNET1_MSG_E_VENDORINFO,	NQSNET_EXT,	},
	{ NQSNET1_MSG_E_VENDOREXT,	NQSNET_EXT,	},
};
