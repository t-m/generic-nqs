/*
 * libgnqs/net-nqs1/all-systems/inet.c
 *		Support for sending messages between hosts
 *
 *		Part of Generic NQS
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1999 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <libgnqs/license.h>
#include <libgnqs/internal.h>
#include <libgnqs/libgnqs.h>

nqsnet1_create_socket ();

