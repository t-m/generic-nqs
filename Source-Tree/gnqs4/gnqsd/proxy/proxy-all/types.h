#ifndef __LIBGNQS_NETALL_TYPES_HEADER
#define __LIBGNQS_NETALL_TYPES_HEADER

/*
 * libgnqs/net-all/types.h
 *		Types defined for the networking layer
 *
 *		Part of Generic NQS
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1999 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

/* =-=-=-=-=
 * struct libgnqs_netall_oper
 *
 * Set of operations supported by network interfaces
 *
 *	open		- open the network interface
 *	read		- read a message from the network interface
 *	write		- write a message to the network interface
 */
 
struct libgnqs_netall_oper {
	void (*open)(void);
	void (*read)(void);
	void (*write)(void);
};


/* =-=-=-=-=
 * struct libgnqs_netall_interface
 *
 * Overall structure to register a network interface
 *
 *	pstDesc		- table describing the network interface
 *	pstFuncs	- table of operations supported on the network 
 *			  interface
 */

struct libgnqs_netall_interface {
	struct libgnqs_netall_desc *pstDesc;
	struct libgnqs_netall_oper *pstFuncs;
};

#endif /* __LIBGNQS_NETALL_TYPES_HEADER */
#endif /* __LIBGNQS_NETALL_TYPES_HEADER */
