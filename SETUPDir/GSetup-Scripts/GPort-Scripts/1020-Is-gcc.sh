#!/bin/sh
#
# Is-gcc.sh
#		Is our chosen C compiler GNU's gcc (or egcs)?
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of GNU GPL

. $GSLIB

# =-=-=-=-=
#	check_for_egcs ()
#
#	Is this compiler egcs?

check_for_egcs ()
{
	if ( $GPORT_CC --version | grep egcs- ) > /dev/null 2>&1 ; then
		return 0
	else
		return 1
	fi
}

# =-=-=-=-=
#	check_for_gcc ()
#
#	Is this compiler gcc?

check_for_gcc ()
{
	F_COMPILE_SCRIPT compile 01.c
	case $? in
		0)	return 0 ;;
		*)	return 1 ;;
	esac
}

main ()
{
	F_TEST_TYPE OTHER
	F_ECHO_RESULT "Is ANSI C compiler egcs ... "

	if check_for_egcs ; then
		F_ECHO_LN "compiler is egcs"
		F_DEFINE_BOOLEAN GPORT_HAS_EGCS 1
		F_TEST_PASSED
	else
		F_ECHO_LN "compiler not egcs"
		F_DEFINE_BOOLEAN GPORT_HAS_EGCS 0
		F_TEST_FAILED
	fi

	F_TEST_TYPE OTHER
	F_ECHO_RESULT "Is ANSI C compiler gcc ..."
	if check_for_gcc ; then
		F_ECHO_LN "compiler is gcc"
		F_DEFINE_BOOLEAN GPORT_HAS_GCC 1
		F_TEST_PASSED
	else
		F_ECHO_LN "compiler not gcc"
		F_DEFINE_BOOLEAN GPORT_HAS_GCC 0
		F_TEST_FAILED
	fi

	if [ "$GPORT_HAS_EGCS" = 1 ]; then
		F_TEST_TYPE BLANK
		F_ECHO_RESULT "  Compiler switches set for ..."
		F_ECHO_LN     "egcs"

		F_DEFINE_STRING  GPORT_CC_FLAGS_DEBUG "-Wpointer-arith -Wbad-function-cast -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations"
		F_DEFINE_STRING  GPORT_CC_FLAGS_OPTIMISE "-O6 -fno-strength-reduce"
		F_DEFINE_STRING  GPORT_CC_FLAGS_COMMON "-Wall"
	elif [ "$GPORT_HAS_GCC" = 1 ]; then
		F_TEST_TYPE BLANK
		F_ECHO_RESULT "  Compiler switches set for ..."
		F_ECHO_LN     "gcc"

		F_DEFINE_STRING  GPORT_CC_FLAGS_DEBUG "-Wpointer-arith -Wbad-function-cast -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations"
		F_DEFINE_STRING  GPORT_CC_FLAGS_OPTIMISE "-O6 -fno-strength-reduce"
		F_DEFINE_STRING  GPORT_CC_FLAGS_COMMON "-Wall"
	fi
}

main "$@"
