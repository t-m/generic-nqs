#!/bin/sh
#
# Has-Malloc-H.sh
#		Check for <malloc.h>
#		Is this a BSD header file?  It isn't POSIX.1 ...
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER malloc.h

exit 0
