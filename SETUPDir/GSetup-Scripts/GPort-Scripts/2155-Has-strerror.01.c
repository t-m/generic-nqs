/*
 * Has-strerror.01.c
 *		Test for strerror() defined in <string.h>
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <string.h>

int main (void)
{
	char * s = strerror(1);
	return 0;
}
