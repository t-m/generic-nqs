#!/bin/sh
#
# Has-Osync.sh
#		Check on support for O_SYNC flag
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Support for O_SYNC ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_OSYNC 1
		F_TEST_PASSED
		exit 0
		;;
esac

F_COMPILE_SCRIPT link 02.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_OSYNC 1
		F_TEST_PASSED
		exit 0
		;;
esac

F_ECHO_LN "not found"
F_DEFINE_BOOLEAN GPORT_HAS_OSYNC 0
F_TEST_FAILED
