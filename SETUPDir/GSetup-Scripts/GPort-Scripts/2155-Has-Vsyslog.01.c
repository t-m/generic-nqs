/*
 * Has-Vsyslog.01.c
 *		Test support for vsyslog() function
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <syslog.h>
#include <stdarg.h>

int main(void)
{
	va_list vTmp;

	vsyslog(LOG_INFO, "%s", vTmp);

	return 0;
}
