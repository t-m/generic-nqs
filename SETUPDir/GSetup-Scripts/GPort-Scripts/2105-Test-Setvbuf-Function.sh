#! /bin/sh
#
# Test-Setvbuf-Function
# 		Tests to see if this is a BSD system
#
# Version	4.0
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_ECHO_RESULT "Support for setvbuf() ... "
F_COMPILE_SCRIPT link 01.c

case $? in
	0) F_ECHO_LN "found it"
	   F_DEFINE_BOOLEAN "GPORT_HAS_SETVBUF" 1
	   F_TEST_PASSED
	   ;;
	*) F_ECHO_LN "not found"
	   F_DEFINE_BOOLEAN "GPORT_HAS_SETVBUF" 0
	   F_TEST_FAILED
	   ;;
esac
