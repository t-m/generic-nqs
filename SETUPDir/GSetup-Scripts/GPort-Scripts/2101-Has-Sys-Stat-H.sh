#!/bin/sh
#
# Has-Sys-Stat-H.sh
#		Check for POSIX.1 <sys/stat.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_CHECK_HEADER sys/stat.h

exit 0
