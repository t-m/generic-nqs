/*
 * Has-Grent.01.c
 *		Test for support of get/setgrent(3) functions
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <grp.h>
#include <sys/types.h>

int main(void)
{
	struct group * pstGroup;

	setgrent();
	pstGroup = getgrent();
	endgrent();
}

