/*
 * Has-Syslog.01.c
 *		Test for syslog(3) support
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <syslog.h>

int main(void)
{
	openlog ("gSETUP", LOG_CONS | LOG_NDELAY | LOG_PID, LOG_LOCAL0);
	syslog (LOG_INFO, "%s", "gSETUP");
	closelog();

	return 0;
}
