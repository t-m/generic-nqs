/*
 * Has-Ioctl.02.c
 *		Where is the ioctl() function call
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <sys/ioctl.h>
#include <sgtty.h>

int main(void)
{
	ioctl(0,TIOCGETP);
	ioctl(0,TIOCEXCL);
	ioctl(0,TIOCSETP);
	ioctl(0,TIOCLBIC);
	ioctl(0,TIOCSETP);
	return 0;
}
