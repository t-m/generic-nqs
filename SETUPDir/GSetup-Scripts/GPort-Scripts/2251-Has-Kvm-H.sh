#!/bin/sh
#
# Has-Kvm-H.sh
#		Check for Solaris 2.x <kvm.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER kvm.h

exit 0
