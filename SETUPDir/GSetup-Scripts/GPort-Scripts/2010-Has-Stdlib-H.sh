#!/bin/sh
#
# Has-Stdlib-H.sh
#		Check for ANSI C <stdlib.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE ANSIC
F_CHECK_HEADER stdlib.h

exit 0
