#!/bin/sh
#
# Has-Uptime-Code.sh
#		Figure out how to get system load information
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE BLANK
F_ECHO_RESULT_LN "How do I get system load info ..."

F_TEST_TYPE OTHER
F_ECHO_RESULT "  Via IRIX-like sysmp(2) ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_SYSLOAD_SYSMP 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_SYSLOAD_SYSMP 0
		F_TEST_FAILED
		;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT "  Via Linux-like /proc/uptime ..."
if [ -f /proc/loadavg ]; then
	F_ECHO_LN "found it"
	F_DEFINE_BOOLEAN GPORT_HAS_SYSLOAD_PROC 1
	F_TEST_PASSED
else
	F_ECHO_LN "not found"
	F_DEFINE_BOOLEAN GPORT_HAS_SYSLOAD_PROC 0
	F_TEST_FAILED
fi

F_TEST_TYPE OTHER
F_ECHO_RESULT "  Via Solaris-like libkvm ..."
F_COMPILE_SCRIPT link 98.c "" -lkvm

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_SYSLOAD_KVM 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_SYSLOAD_KVM 0
		F_TEST_FAILED
		;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT "Via BSD-like /dev/kmem and <nlist.h> ..."
F_COMPILE_SCRIPT link 99.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_SYSLOAD_NLIST 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_SYSLOAD_NLIST 0
		F_TEST_FAILED
		;;
esac
