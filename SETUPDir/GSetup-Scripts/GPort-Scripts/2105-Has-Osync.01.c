/*
 * Has-Osync.01.c
 *		Test for O_SYNC option to open(2)
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <fcntl.h>

int main(void)
{
	int i = open ("/dev/null", O_RDWR | O_SYNC);

	return 0;
}
