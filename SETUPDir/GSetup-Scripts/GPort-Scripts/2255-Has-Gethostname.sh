#!/bin/sh
#
# Has-Gethostname.sh
#		Check for the BSD gethostname(2) function
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE BSD43
F_ECHO_RESULT "Support for gethostname() ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_GETHOSTNAME 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_GETHOSTNAME 0
		F_TEST_FAILED
		;;
esac
