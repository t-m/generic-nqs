#!/bin/sh
#
# Has-NetInet-In-H.sh
#		Check for BSD4.3 <netinet/in.h> socket header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE BSD43
F_CHECK_HEADER netinet/in.h

exit 0
