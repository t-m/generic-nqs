#!/bin/sh
#
# Has-Memcpy.sh
#		Check for the POSIX.1 mem*() function set.
#		If the entire function set isn't found, then users are
#		recommended to use BSD bcopy() et al.
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_ECHO_RESULT "Support for mem*(3) routines ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_POSIX_MEMCPY 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_POSIX_MEMCPY 0
		F_TEST_FAILED
		;;
esac

F_TEST_TYPE BSD43
F_ECHO_RESULT "Support for bcopy() et al ..."
F_COMPILE_SCRIPT link 02.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_BSD_BCOPY 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_BSD_BCOPY 0
		F_TEST_FAILED
		;;
esac
