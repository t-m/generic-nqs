#!/bin/sh
#
# Has-pw-comment
#		Does the password field have the pw_comment field?
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Does struct passwd have pw_comment ... "
F_COMPILE_SCRIPT compile 01.c

case $? in
	0)	F_ECHO_LN "yes it does"
		F_DEFINE_BOOLEAN GPORT_HAS_PW_COMMENT 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "no it doesn't"
		F_DEFINE_BOOLEAN GPORT_HAS_PW_COMMENT 0
		F_TEST_FAILED
		;;
esac

exit 0
