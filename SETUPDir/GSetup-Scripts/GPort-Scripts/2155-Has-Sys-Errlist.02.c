/*
 * Has-Sys-Errlist.02.c
 *		Test for sys_errlist defined in <errno.h>
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <errno.h>
#include <stdio.h>

int main (void)
{
	printf ("%s\n", sys_errlist[0]);
	return 0;
}
