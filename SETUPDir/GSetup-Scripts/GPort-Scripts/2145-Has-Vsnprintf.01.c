/*
 * Has-Vsnprintf.01.c
 *		Test for vsnprintf(3) library call
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <stdio.h>
#include <stdarg.h>

int main(void)
{
	char    * szBuffer;
	size_t    gSize;
	va_list   gList;

	vsnprintf(szBuffer, gSize, "%s", gList);
	return 0;
}
