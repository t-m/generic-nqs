/*
 * Need--g.01.c
 *		ANSI C program to compile when testing for -g switch
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

int main(void)
{
	return 0;
}
