/*
 * Has-File-Locking.02.c
 *		Check for file locking support
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <sys/file.h>

#ifndef LOCK_SH
#define LOCK_SH 1
#endif

#ifndef LOCK_EX
#define LOCK_EX 2
#endif

#ifndef LOCK_NB
#define LOCK_NB 4
#endif

#ifndef LOCK_UN 
#define LOCK_UN 8
#endif

int main(void)
{
	flock(0, LOCK_UN);
	flock(0, LOCK_SH + LOCK_NB);
	flock(0, LOCK_EX + LOCK_NB);

	return 0;
}
