#!/bin/sh
#
# Has-Rlimit-64.sh
#		Check for support for 64-bit BSD process limits
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Support for get/setrlimit64() ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN "GPORT_HAS_RLIMIT_64" 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN "GPORT_HAS_RLIMIT_64" 0
		F_TEST_FAILED
		;;
esac
