#!/bin/sh
#
# Has-Sys-Session-H.sh
#		Check for UNICOS <sys/session.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/session.h

exit 0
