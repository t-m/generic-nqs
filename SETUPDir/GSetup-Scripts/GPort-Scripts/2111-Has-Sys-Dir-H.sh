#!/bin/sh
#
# Has-Sys-Dir-H.h
#		Check for BSD 4.3 <sys/dir.h>, or <ndir.h>
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE BSD43
F_ECHO_RESULT "Checking for <sys/dir.h> ... "

F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_SYS_DIR_H 1
		has_result=1
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_SYS_DIR_H 0
		;;
esac

F_TEST_TYPE BSD43
F_ECHO_RESULT "Checking for <ndir.h> ..."

F_COMPILE_SCRIPT link 02.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_NDIR_H 1
		has_result=1
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_NDIR 0
		;;
esac

if [ "$has_result" = 1 ]; then
	F_TEST_PASSED
else
	F_TEST_FAILED
fi
