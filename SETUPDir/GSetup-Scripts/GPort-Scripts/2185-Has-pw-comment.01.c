/*
 * Has-pw-comment.01.c
 *		Test to see if struct passwd has the pw_comment field or
 *		not
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <pwd.h>
#include <sys/types.h>
#include <stdio.h>

int main (void)
{
	struct passwd pw;
	printf ("%s", pw.pw_comment);
	return 0;
}
