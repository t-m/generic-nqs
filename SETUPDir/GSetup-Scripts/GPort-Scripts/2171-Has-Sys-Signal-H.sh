#!/bin/sh
#
# Has-Sys-Signal-H.sh
#		Check for <sys/signal.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/signal.h

exit 0
