/*
 * Has-Gethostname.01.c
 *		Check for the BSD gethostname() function
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <unistd.h>

int main(void)
{
	char * szBuffer;

	gethostname (szBuffer, 10);
	
	return 0;
}
