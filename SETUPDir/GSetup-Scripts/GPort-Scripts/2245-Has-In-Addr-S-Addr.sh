#!/bin/sh
#
# Has-In-Addr-S-Addr.sh
#		Does the in_addr structure have the s_addr field?
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Does struct in_addr have s_addr ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_IN_ADDR_S_ADDR 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_IN_ADDR_S_ADDR 0
		F_TEST_FAILED
		;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT "Does struct in_addr have S_un.S_addr ..."
F_COMPILE_SCRIPT link 02.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_IN_ADDR_S_UN 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_IN_ADDR_S_UN 0
		F_TEST_FAILED
		;;
esac
