/*
 * Has-Getpw.01.c
 *		Test for getpw*(3) functions
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <pwd.h>

int main(void)
{
	struct passwd * pstPw;

	pstPw = getpwuid(0);
	pstPw = getpwnam("root");

	return 0;
}
