#!/bin/sh
#
# Has-Time-T.sh
#		Check for time_t data type
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_ECHO_RESULT "Checking definition of time_t ..."

F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "in <time.h>"
		F_DEFINE_BOOLEAN GPORT_HAS_TIME_T          1
		F_DEFINE_BOOLEAN GPORT_HAS_TIME_HEADER     1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not in <time.h>"
		F_DEFINE_BOOLEAN GPORT_HAS_TIME_HEADER	   0
		F_TEST_FAILED
		;;
esac

F_TEST_TYPE BSD43
F_ECHO_RESULT
F_COMPILE_SCRIPT link 02.c

case $? in
	0)	F_ECHO_LN "in <sys/time.h>"
		F_DEFINE_BOOLEAN GPORT_HAS_TIME_T          1
		F_DEFINE_BOOLEAN GPORT_HAS_SYS_TIME_HEADER 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not in <sys/time.h>"
		F_DEFINE_BOOLEAN GPORT_HAS_SYS_TIME_HEADER 0
		F_TEST_FAILED
		;;
esac

if [ "$GPORT_HAS_TIME_T" != 1 ]; then
	F_DEFINE_BOOLEAN GPORT_HAS_TIME_T 0
fi
