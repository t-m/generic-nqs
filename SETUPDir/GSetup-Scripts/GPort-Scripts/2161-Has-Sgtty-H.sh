#!/bin/sh
#
# Has-Sgtty-H.sh
#		Check for BSD4.3 <sgtty.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE BSD43
F_CHECK_HEADER sgtty.h

exit 0
