/*
 * Test-Times-Function.01.c
 *		Test for POSIX.1 times.h header file
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <sys/times.h>

int main(void)
{
	clock_t c;
	struct tms buf;
	
	c = times(&buf);
	
	return 0;
}
