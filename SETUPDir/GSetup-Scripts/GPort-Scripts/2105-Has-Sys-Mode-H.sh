#!/bin/sh
#
# Has-Sys-Mode-H.sh
#		Check for AIX <sys/mode.h> header
#		Not sure what's in this header - may have nothing to
#		do with files!
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/mode.h

exit 0
