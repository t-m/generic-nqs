/*
 * Has-BSD-Limits.01.c
 *		Test for BSD resource limits API
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#ifndef RLIMIT_CPU
#define RLIMIT_CPU 0
#endif

int main (void)
{
	struct rlimit stLimit;

	getrlimit(RLIMIT_CPU, &stLimit);
	setrlimit(RLIMIT_CPU, &stLimit);

	return 0;
}
