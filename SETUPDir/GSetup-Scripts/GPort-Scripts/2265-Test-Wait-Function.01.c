/*
 * Test-Wait-Function.01.c
 *		Test for POSIX.1 wait() function
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright 	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main (void)
{
	switch (fork())
	{
		case 0:
			/* we are the child */
			sleep(1);
			return 0;
			break;
		case -1:
			/* unable to fork - oh hell */
			return 127;
			break;
			
		default:
			/* we are the parent */
			wait(NULL);
			break;
	 }
	 
	 return 0;
}
