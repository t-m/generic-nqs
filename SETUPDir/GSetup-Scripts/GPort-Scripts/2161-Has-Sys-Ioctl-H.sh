#!/bin/sh
#
# Has-Sys-Ioctl-H.sh
#		Check for POSIX.1 and BSD 4.3 <sys/ioctl.h>
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/ioctl.h

exit 0
