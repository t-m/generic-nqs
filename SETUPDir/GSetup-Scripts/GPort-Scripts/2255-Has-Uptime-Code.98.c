/*
 * Has-Uptime-Code.02.c
 *		Figure out how to read uptime information
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

/*
 * This is the method for Solaris 2.5+ ... 
 * Maybe it'll work with older Solaris, and other SVr4 systems ...
 */

#include <nlist.h>
#include <sys/sysinfo.h>
#include <sys/param.h>
#include <kvm.h>

int main(void)
{
	int i;
	kvt_m * kvt_handle;
	struct nlist nl[] = {
		{ "avenrun" },
		{ 0 }
	};

	kvm_handle = kvm_open(NULL, NULL, NULL, O_RDONLY, NULL);
	if (kvm_handle == NULL || kvm_nlist(kvm_handle, nl) != 0)
	{
		fprintf(stderr, "error from kvm_open()");
		exit (1);
	}

	if ((i = kvm_read(kvm_handle, nl[0].n_value, (void *) avenrun, sizeof (avenrun))) != sizeof (avenrun))
	{
		fprintf(stderr, "error from kvm_read()");
		exit (1);
	}

	kvm_close(kvm_handle);
}
