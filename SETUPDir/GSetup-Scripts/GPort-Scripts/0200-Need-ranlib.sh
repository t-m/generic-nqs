#!/bin/sh
#
# Need_ranlib
#		Script to check for the 'ranlib' utility
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER

F_FIND_PATH ranlib
case $? in
	0)	F_TEST_PASSED
		F_DEFINE_STRING "GPORT_EXE_RANLIB" "$GSLIB_V_ANSWER"
		;;
	*)	F_TEST_FAILED
		;;
esac
