/*
 * Has-Tzset-01.c
 *		Check out support for tzset()
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <time.h>

int main(void)
{
	tzset();

	return 0;
}
