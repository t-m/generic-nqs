#!/bin/sh
#
# Has-SetPriority-Limits.sh
#		Does this machine support setpriority(2)?
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Looking for get/setpriority(2) ... "

F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_SETPRIORITY_LIMITS 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_SETPRIORITY_LIMITS 0
		F_TEST_FAILED
		;;
esac

exit 0
