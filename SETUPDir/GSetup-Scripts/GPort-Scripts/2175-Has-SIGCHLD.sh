#!/bin/sh
#
# HAS_SIGCHLD	Test for the SYSV SIGCHLD signal ...
#
# Version	4.0
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_SIGNAL SIGCHLD
exit 0
