#!/bin/sh
#
# Has-Sys-Errno
#		Work out whether strerror() is supported on this platform
#		or not
#
# Version	4.0
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Checking definition of strerror ... "

F_COMPILE_SCRIPT compile 01.c

case $? in
	0)	F_ECHO_LN "defined in <string.h>"
		F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_STRERROR y
		F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_STRERROR_STRING y
		F_TEST_PASSED
		exit 0
		;;
	*)	F_ECHO_LN "not in <stdio.h> ..."
		;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT
F_COMPILE_SCRIPT compile 02.c

case $? in
	0)	F_ECHO_LN "defined in <errno.h>"
		F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_STRERROR y
		F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_STRERROR_ERRNO y
		F_TEST_PASSED
		exit 0
		;;
	*)	F_ECHO_LN "not in <errno.h> ..."
		;;
esac

F_ECHO_LN "not found ... oh well"
F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_STRERROR n
F_TEST_FAILED
