#!/bin/sh
#
# HAS_SIGPWR	Test for the SYSV SIGPWR signal ...
#		We assume that SIGPROF is preferred is SIGPWR cannot be found
#
# Version	4.0
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_SIGNAL SIGPWR

case $? in
	0)	F_DEFINE_LITERAL "GPORT_SIG_SIGPWR" "SIGPWR" ;;
	*)	F_ECHO_LN "  I'll use SIGPROF instead"
		F_DEFINE_LITERAL "GPORT_SIG_SIGPWR" "SIGPROF" ;;
esac
