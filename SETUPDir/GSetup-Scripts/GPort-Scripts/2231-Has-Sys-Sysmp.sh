#!/bin/sh
#
# Has-Sys-Sysmp.sh
#		Check for IRIX-like <sys/sysmp.h> header
#		Should be found on IRIX 5.x, 6.x, and Linux w/ pset patch
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/sysmp.h

exit 0
