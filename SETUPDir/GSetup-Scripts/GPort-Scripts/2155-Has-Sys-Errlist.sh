#!/bin/sh
#
# Has-Sys-Errno
#		Work out what the local declaration of sys_errlist is
#
# Version	4.0
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Checking definition of sys_errlist ... "

F_COMPILE_SCRIPT compile 01.c

case $? in
	0)	F_ECHO_LN "defined in <stdio.h>"
		F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_ERRLIST_STDIO y
		F_TEST_PASSED
		exit 0
		;;
	*)	F_ECHO_LN "not in <stdio.h> ..."
		;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT
F_COMPILE_SCRIPT compile 02.c

case $? in
	0)	F_ECHO_LN "defined in <errno.h>"
		F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_ERRLIST_ERRNO y
		F_TEST_PASSED
		exit 0
		;;
	*)	F_ECHO_LN "not in <errno.h> ..."
		;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT
F_COMPILE_SCRIPT compile 03.c 

case $? in
	0)	F_ECHO_LN "char * []"
		F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_ERRLIST y
		F_DEFINE_LITERAL GPORT_ERRLIST_TYPE "char * sys_errlist[]"
		F_TEST_PASSED
		exit 0
		;;
	*)	F_ECHO_LN "not char * []" ;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT
F_COMPILE_SCRIPT compile 04.c

case $? in
	0)	F_ECHO_LN "char *"
		F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_ERRLIST y
		F_DEFINE_LITERAL GPORT_ERRLIST_TYPE "char * sys_errlist"
		F_TEST_PASSED
		exit 0
		;;
	*)	F_ECHO_LN "not char *" ;;
esac

F_ECHO_LN "not found ... oh well"
F_DEFINE_BOOLEAN GPORT_HAS_ERRNO_ERRLIST n
F_TEST_FAILED
