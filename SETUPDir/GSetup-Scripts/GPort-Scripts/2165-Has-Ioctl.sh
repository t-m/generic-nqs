#!/bin/sh
#
# Has-Ioctl.sh
#		Check for ioctl(2) support
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_ECHO_RESULT "Support for ioctl() ..."

F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "POSIX.1 found"
		F_DEFINE_BOOLEAN GPORT_HAS_POSIX_IOCTL 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "POSIX.1 not found"
		F_DEFINE_BOOLEAN GPORT_HAS_POSIX_IOCTL 0
		F_TEST_FAILED
		;;
esac

F_TEST_TYPE BSD43
F_ECHO_RESULT
F_COMPILE_SCRIPT link 02.c

case $? in
	0)	F_ECHO_LN "BSD found"
		F_DEFINE_BOOLEAN GPORT_HAS_BSD_IOCTL 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "BSD not found"
		F_DEFINE_BOOLEAN GPORT_BAS_BSD_IOCTL 0
		F_TEST_FAILED
		;;
esac
