#!/bin/sh
#
# Need-cc	Attempt to locate an ANSI C compiler
#
# Version	5.0
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL
#

. $GSLIB

# =-=-=-=-=
#	is_ansi ()
#
#	Is $1 an ANSI C compiler?

is_ansi ()
{
	F_TEST_TYPE BLANK
	F_ECHO_RESULT_LN "  Is $1 an ANSI C compiler ..."

	# taken from GNU autoconfig ...
	#
	# AIX			-qlanglvl=ansi
	# Ultrix & OSF/1	-std1
	# HP-UX			-Aa -D_HPUX_SOURCE
	# SVR4			-Xc -D__EXTENSIONS__
	#
	# additionally ... tests for 64-bit compilers
	#
	# IRIX 6.x		-64 -mips4
	# IRIX 6.5?		-ansi
	# also			-ansi -64 -mips4
	
	for is_ansi_x in "-64 -mips4" "-ansi -64 -mips4" "" "-qlanglvl=ansi" "-std1" "-Aa -D_HPUX_SOURCE" "-Xc -D__EXTENSIONS__" "-ansi" ; do

		GPORT_CC="$1 $is_ansi_x"
		F_TEST_TYPE BLANK
		F_ECHO_RESULT "    '$is_ansi_x' ..."
		F_COMPILE_SCRIPT compile 01.c

		case $? in
			0) 	GPORT_CC=
				is_ansi_y="$1"
				if [ "$is_ansi_x+" != "+" ]; then
					is_ansi_y="$1 $is_ansi_x"
				fi
				F_DEFINE_STRING "GPORT_CC" "$is_ansi_y"
				F_ECHO_LN "ANSI compliant"
				return 0 
				;;
			*)	F_ECHO_LN "not ANSI or invalid switches"
				;;
		esac
	done
	return 1
}

# =-=-=-=-=
#	main ()
#
#	It all happens *here*

main ()
{
	# special case - CC variable
	F_TEST_TYPE ANSIC
	F_ECHO_RESULT "Is \$CC set to a C compiler ..."

	if [ "$SETUP_CC+" = "+" ]; then
		F_ECHO_LN "\$CC is unset"
		F_TEST_TYPE BLANK
		F_ECHO_RESULT 
		F_ECHO_LN "I'll find it myself"
	else
		F_ECHO_LN "\$CC is set to $SETUP_CC"
		if is_ansi "$SETUP_CC" ; then
			return 0
		else
			F_ECHO_LN
			F_ERROR "    Hrm ... \$CC is *not* set to an ANSI C compiler."
			F_ERROR "    Please fix \$CC, or unset it!"
			exit 1
		fi
	fi

	for x in acc cc gcc ; do
		F_TEST_TYPE BLANK
		F_FIND_PATH $x is_ansi
		case $? in
			0)	F_TEST_PASSED ; return 0 ;;
		esac
	done
	
	F_ECHO_LN
	F_ERROR "cannot find an ANSI C compiler!"
	F_TEST_FAILED
	exit 1
}

main "$@"
