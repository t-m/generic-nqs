#!/bin/sh
#
# HAS_BSD_PIPE	Test to see if named pipes are supported or not
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_ECHO_RESULT "Support for mkfifo() ... "
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_MKFIFO 0
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_MKFIFO 1
		F_TEST_FAILED
		;;
esac
