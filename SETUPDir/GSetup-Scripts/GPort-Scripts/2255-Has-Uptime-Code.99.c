/*
 * Has-Uptime-Code.03.c
 *		Figure out how to read system load
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

/*
 * This method is supposed to work on HP-UX 8.x and 9.x ...
 * Oh, and Digital UNIX too strangely enough ...
 * And on older versions of Solaris ...
 *
 * I guess this must be the BSD way of doing things ...
 *
 * I'm hoping the newer Solaris method will work for HP-UX v10 upwards ;-)
 */

#include <nlist.h>
#include <stdio.h>
#include <fcntl.h>

int main(void)
{
	int kmem;

	struct nlist nl[] = {
#ifdef __hppa	/* series 700 & 800 */
	{ "avenrun" },
#else
	{ "_avenrun" },
#endif
	{ 0 }
	};

	nlist("/hp-ux", nl);
	if (nl[0].n_type == 0)
	{
		fprintf(stderr, "namelist error\n");
		exit (1);
	}

	kmem = open ("/dev/kmem", 0);

	return 0;
}
