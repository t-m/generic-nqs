#!/bin/sh
#
# Readline-Libs
#		Does readline need other libraries?
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Looking for GNU readline library ... "

for libs in "-lreadline" "-lreadline -ltermcap"
do
    F_COMPILE_SCRIPT link 01.c ${libs}

    case $? in
	0)	F_ECHO_LN "using ${libs}"
		F_DEFINE_STRING GPORT_READLINE_LIBS "${libs}"
		F_TEST_PASSED
		exit 0
		;;
    esac
done

F_ECHO_LN "not found"
F_DEFINE_BOOLEAN GPORT_HAS_READLINE 0
F_TEST_FAILED

exit 0
