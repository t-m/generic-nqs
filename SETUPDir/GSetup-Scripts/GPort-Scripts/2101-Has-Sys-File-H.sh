#!/bin/sh
#
# Has-Sys-File-H.sh
#		Check for BSD <sys/file.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE BSD43
F_CHECK_HEADER sys/file.h

exit 0
