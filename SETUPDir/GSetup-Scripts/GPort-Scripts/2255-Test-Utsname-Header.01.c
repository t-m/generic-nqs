/*
 * Test-Utsname-Header.01.c
 *		Test for POSIX.1 utsname.h and uname function
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <sys/utsname.h>

int main (void)
{
	struct utsname name;
	
	uname(&name);
	
	return 0;
}
