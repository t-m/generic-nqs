#!/bin/sh
#
# Has-Zombies.sh
#		Test whether the local o/s creates zombies or not
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Problems with zombies ..."
x="`F_COMPILE_SCRIPT run 01.c`"

case "$x" in
	"zombie problem")	F_ECHO_LN "has POSIX.1 zombies"
				F_DEFINE_BOOLEAN GPORT_HAS_POSIX_ZOMBIES 1
				F_TEST_PASSED
				;;
	*)			F_ECHO_LN "no  POSIX.1 zombies"
				F_DEFINE_BOOLEAN GPORT_HAS_POSIX_ZOMBIES 0
				F_TEST_FAILED
				;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT
x="`F_COMPILE_SCRIPT run 02.c`"

case "$x" in
	"zombie problem")	F_ECHO_LN "has BSD zombies"
				F_DEFINE_BOOLEAN GPORT_HAS_BSD_ZOMBIES 1
				F_TEST_PASSED
				;;
	*)			F_ECHO_LN "no  BSD zombies"
				F_DEFINE_BOOLEAN GPORT_HAS_BSD_ZOMBIES 0
				F_TEST_FAILED
				;;
esac
