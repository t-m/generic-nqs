#!/bin/sh
#
# Need--g.sh
#		Does the C compiler support the -g switch?
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

check_for_g_switch ()
{
	F_COMPILE_SCRIPT compile 01.c $1
	return $?
}

# =-=-=-=-=
#	main ()
#
#	The action starts here

main ()
{
	F_TEST_TYPE OTHER
	F_ECHO_RESULT "Does the C compiler accept -g switch ..."
	if check_for_g_switch -g ; then
		F_ECHO_LN "it seems to"
		if test $GPORT_HAS_EGCS = 1 ; then
			F_TEST_TYPE BLANK
			F_ECHO_RESULT "  Does egcs support debugging levels ..."
			if check_for_g_switch -g3 ; then
				F_ECHO_LN "yes it does"
				F_DEFINE_STRING GPORT_CC_FLAGS_DEBUG "$GPORT_CC_FLAGS_DEBUG -g3"
			else
				F_ECHO_LN "no it doesn't"
				F_DEFINE_STRING GPORT_CC_FLAGS_DEBUG "$GPORT_CC_FLAGS_DEBUG -g"
			fi
		elif test $GPORT_HAS_GCC = 1 ; then
			F_TEST_TYPE BLANK
			F_ECHO_RESULT "  Does GCC support debugging levels ... "
			if check_for_g_switch -g3 ; then
				F_ECHO_LN "yes it does"
				F_DEFINE_STRING GPORT_CC_FLAGS_DEBUG "$GPORT_CC_FLAGS_DEBUG -g3"
			else
				F_ECHO_LN "no it doesn't"
				F_DEFINE_STRING GPORT_CC_FLAGS_DEBUG "$GPORT_CC_FLAGS_DEBUG -g"
			fi
		else
			F_DEFINE_STRING GPORT_CC_FLAGS_DEBUG "$GPORT_CC_FLAGS_DEBUG -g"
		fi
		F_TEST_PASSED
	else
		F_ECHO_LN "no it doesn't"
	fi
}

main "$@"
