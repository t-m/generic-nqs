/*
 * Has-In-Addr-S-Addr.01.c
 *		Check that the internet address structure has the 
 *		s_addr field
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

int main(void)
{
	struct in_addr stAddr;

	stAddr.s_addr = 0L;

	return 0;
}
