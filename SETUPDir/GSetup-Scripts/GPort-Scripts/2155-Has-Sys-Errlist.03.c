/*
 * Has-Sys-Errlist.01.c
 *		Test for sys_errlist defined in <errno.h> or <stdio.h>
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <errno.h>
#include <stdio.h>

extern char * sys_errlist[];

int main (void)
{
	return 0;
}
