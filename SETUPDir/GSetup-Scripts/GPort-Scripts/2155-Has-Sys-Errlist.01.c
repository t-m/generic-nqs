/*
 * Has-Sys-Errlist.01.c
 *		Test for sys_errlist defined in <stdio.h>
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <stdio.h>

int main (void)
{
	printf ("%s\n", sys_errlist[0]);
	return 0;
}
