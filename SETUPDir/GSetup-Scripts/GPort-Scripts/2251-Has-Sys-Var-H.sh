#!/bin/sh
#
# Has-Sys-Var-H.sh
#		Check for IRIX-like <sys/var.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/var.h

exit 0
