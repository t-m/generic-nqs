/*
 * Has-Ioctl.01.c
 *		Where is the ioctl() function call
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <sys/ioctl.h>
#include <termios.h>

int main(void)
{
	ioctl(0,TCGETA);
	ioctl(0,TCSETA);
	ioctl(0,TCSETAW);
	return 0;
}
