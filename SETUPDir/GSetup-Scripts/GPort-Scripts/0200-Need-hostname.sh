#!/bin/sh
#
# Need_hostname
#		Script to check for the 'hostname' utility
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER

F_FIND_PATH hostname
case $? in
	0)	F_TEST_PASSED
		F_DEFINE_STRING "GPORT_EXE_HOSTNAME" "$GSLIB_V_ANSWER" 
		;;
	*)	F_TEST_FAILED
		F_DEFINE_STRING GPORT_EXE_HOSTNAME
		F_ERROR "'hostname' is not in your PATH.  Please fix your PATH, and try again"
		exit 1
		;;
esac
