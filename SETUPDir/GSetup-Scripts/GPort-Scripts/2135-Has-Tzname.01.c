/*
 * Has-Tzname.01.c
 *		Check on support for the tzname variable
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <time.h>
#include <stdio.h>

int main(void)
{
	printf("%s", tzname[0]);
	return 0;
}
