/*
 * Has-File-Locking.01.c
 *		Check for file locking support
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <unistd.h>
#include <fcntl.h>

int main(void)
{
	struct flock *pstLock;

	fcntl(0, F_GETLK,  pstLock);
	fcntl(0, F_SETLK,  pstLock);
	fcntl(0, F_SETLKW, pstLock);

	return 0;
}
