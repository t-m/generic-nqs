/*
 * 25-Test-Setvbuf-Function.01.c
 *		ANSI C script to test whether local o/s supports the POSIX.1
 *		setvbuf() function or not.
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <stdio.h>

int main(void)
{
	char c[1024];
	
	FILE * fp;
	/* it doesn't have to run - it only has to compile */
	/* we don't cast to size_t to help SVr1-3 */
	setvbuf(fp, &c[0], 0, 1024);
	
	return 0;
}
