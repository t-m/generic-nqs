#!/bin/sh
#
# Has-Sys-Types-H.sh
#		Check for POSIX.1 / BSD4.3 <sys/types.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/types.h

exit 0
