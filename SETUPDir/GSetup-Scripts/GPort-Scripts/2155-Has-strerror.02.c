/*
 * Has-strerror.02.c
 *		Test for strerror() defined in <errno.h>
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <errno.h>

int main (void)
{
	char * s = strerror(1);
	return 0;
}
