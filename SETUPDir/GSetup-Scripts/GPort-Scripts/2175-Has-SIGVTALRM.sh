#!/bin/sh
#
# HAS_SIGVTALRM	Test for the SYSV SIGVTALRM signal ...
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_SIGNAL SIGVTALRM
exit 0
