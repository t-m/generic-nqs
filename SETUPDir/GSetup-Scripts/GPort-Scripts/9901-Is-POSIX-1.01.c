/*
 * Is-POSIX.01.c
 *		Program to output current POSIX.1 support level
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 * 
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#define _POSIX_SOURCE 1
#include <stdio.h>
#include <unistd.h>
int main()
{
  printf ("%ld\n", _POSIX_VERSION);
  return 0;
}
