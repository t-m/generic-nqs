#! /bin/sh
#
# HAS_E_ELOOP	Test to see if the error value ELOOP is supported on this
#		operating system or not.
#
# Version	4.0
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		All rights reserved

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Checking for ELOOP error code ... "
F_COMPILE_SCRIPT compile 01.c

case $? in
	0)	F_ECHO_LN "ELOOP supported"
		F_DEFINE_BOOLEAN "GPORT_HAS_E_ELOOP" 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "ELOOP not supported"
		F_DEFINE_BOOLEAN "GPORT_HAS_E_ELOOP" 0
		F_TEST_FAILED
		;;
esac
