#!/bin/sh
#
# Has-BSD-Limits
#		Does this operating system support get/setrlimit()?
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE BSD43
F_ECHO_RESULT "Support for get/setrlimit() ... "

F_COMPILE_SCRIPT link 01.c
case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN "GPORT_HAS_BSD_LIMITS" 1
		F_TEST_PASSED
		;;
	1)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN "GPORT_HAS_BSD_LIMITS" 0
		F_TEST_FAILED
		;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT "  Is rlim_t defined ... "
F_COMPILE_SCRIPT compile 02.c
case $? in
	0)	F_ECHO_LN "yes"
		F_DEFINE_BOOLEAN GPORT_HAS_RLIM_T 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "no (fix libc, someone!)"
		F_DEFINE_BOOLEAN GPORT_HAS_RLIM_T 0
		F_TEST_FAILED
		;;
esac
