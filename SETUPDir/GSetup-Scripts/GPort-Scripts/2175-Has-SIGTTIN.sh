#!/bin/sh
#
# HAS_SIGTTIN	Test for the SYSV SIGTTIN signal ...
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_SIGNAL SIGTTIN
exit 0
