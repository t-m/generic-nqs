#!/bin/sh
#
# Has-Sys-Sysinfo-H.sh
#		Check for IRIX? <sys/sysinfo.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/sysinfo.h

exit 0
