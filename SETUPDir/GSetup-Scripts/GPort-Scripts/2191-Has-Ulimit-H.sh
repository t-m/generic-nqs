#!/bin/sh
#
# Has-Ulimit-H.sh
#		Check for SVr4 <ulimit.h> header file
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER ulimit.h

exit 0
