#! /bin/sh
#
# Has_Size_Int	How big is an integer on this operating system?
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Checking for integer size ... "

x="`F_COMPILE_SCRIPT run 01.c`"
case $? in
	0)	F_DEFINE_LITERAL GPORT_SIZEOF_UNSIGNED_LONG "`echo $x | awk '{ print $1 }'`"
		F_DEFINE_LITERAL GPORT_SIZEOF_INT           "`echo $x | awk '{ print $2 }'`"
		F_ECHO_LN "ulongs are $GPORT_SIZEOF_UNSIGNED_LONG bytes"
		F_TEST_TYPE BLANK
		F_ECHO_RESULT
		F_ECHO_LN "ints are $GPORT_SIZEOF_INT bytes"
		F_TEST_PASSED
		;;
	*)	F_DEFINE_LITERAL GPORT_SIZEOF_INT 4
		F_DEFINE_LITERAL GPORT_SIZEOF_UNSIGNED_LONG 4
		F_ECHO_LN "assuming 4 bytes"
		F_TEST_FAILED
		;;
esac
