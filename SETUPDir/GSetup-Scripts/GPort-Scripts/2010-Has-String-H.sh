#!/bin/sh
#
# Has-String-H.sh
#		Check for ANSI C <string.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE ANSIC
F_CHECK_HEADER string.h

exit 0
