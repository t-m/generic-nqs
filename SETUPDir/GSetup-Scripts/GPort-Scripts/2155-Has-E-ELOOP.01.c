/*
 * Has-E-ELOOP.01.c
 *		Test for ELOOP errno value
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <errno.h>
#include <stdio.h>

int main (void)
{
	printf ("%d\n", ELOOP);
        return 0;
}
