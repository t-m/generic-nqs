#!/bin/sh
#
# Choose-Port.sh
#		Select whether we are going to be a POSIX.1 port or a BSD43
#		port
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB
. ./.gslib_tests

F_ECHO_LN
F_ECHO_SEPARATOR
F_ECHO_LN

F_TEST_TYPE BLANK
F_ECHO_RESULT "POSIX.1 passed ..."
F_ECHO_LN "$V_POSIX1_P"

F_TEST_TYPE BLANK
F_ECHO_RESULT "POSIX.1 failed ..."
F_ECHO_LN "$V_POSIX1_F"

F_ECHO_LN

F_TEST_TYPE BLANK
F_ECHO_RESULT "POSIX.2 passed ..."
F_ECHO_LN "$V_POSIX2_P"

F_TEST_TYPE BLANK
F_ECHO_RESULT "POSIX.2 failed ..."
F_ECHO_LN "$V_POSIX2_F"

F_ECHO_LN

F_TEST_TYPE BLANK
F_ECHO_RESULT "BSD 4.3 passed ..."
F_ECHO_LN "$V_BSD43_P"

F_TEST_TYPE BLANK
F_ECHO_RESULT "BSD 4.3 failed ..."
F_ECHO_LN "$V_BSD43_F"

F_ECHO_LN

F_TEST_TYPE BLANK
F_ECHO_RESULT "BSD 4.4 passed ..."
F_ECHO_LN "$V_BSD44_P"

F_TEST_TYPE BLANK
F_ECHO_RESULT "BSD 4.4 failed ..."
F_ECHO_LN "$V_BSD44_F"

F_ECHO_LN

F_TEST_TYPE BLANK
F_ECHO_RESULT "Other tests passed ..."
F_ECHO_LN "$V_OTHER_P"

F_TEST_TYPE BLANK
F_ECHO_RESULT "Other tests failed ..."
F_ECHO_LN "$V_OTHER_F"

F_ECHO_LN

F_TEST_TYPE BLANK

if [ "$V_POSIX1_F" = 0 ]; then
	if [ "$V_BSD43_F" = 0 ]; then

		# here we have a problem
		#
		# the operating system supports both POSIX.1 and BSD 4.3
		# without any special libraries such as -lbsd
		#
		# which do we choose?
		#
		# we could define both the POSIX.1 and BSD4.3 symbols, and
		# leave it up to the app to decide its preference
		#
		# guess we have to make an ideological statement :(

		F_ECHO_RESULT_LN "Both ports should work."
		F_TEST_TYPE BLANK
	fi

	F_ECHO_RESULT_LN "Recommended port is POSIX.1 ..."
	F_DEFINE_BOOLEAN GPORT_IS_POSIX_1 1
	F_DEFINE_BOOLEAN GPORT_IS_BSD43   0

elif [ "$V_BSD43_F" = 0 ]; then
	F_ECHO_RESULT_LN "Recommended port is BSD 4.3 ..."
	F_DEFINE_BOOLEAN GPORT_IS_BSD_43  1
	F_DEFINE_BOOLEAN GPORT_IS_POSIX_1 0
fi

if [ "$V_POSIX2_P" != 0 ]; then
	if [ "$V_VPOSIX2_F" = 0 ]; then
		F_TEST_TYPE BLANK
		F_ECHO_RESULT_LN "UNIX environment is POSIX.2 ..."
		F_DEFINE_BOOLEAN GPORT_IS_POSIX_2 1
	else
		F_DEFINE_BOOLEAN GPORT_IS_POSIX_2 0
	fi
fi

if [ "$GPORT_IS_BSD43" = 1 ]; then
	if [ "$V_BSD44_P" != 0 ]; then
		if [ "$V_BSD_44_F" = 0 ]; then
			F_TEST_TYPE BLANK
			F_ECHO_RESULT_LN "BSD 4.4 extensions are also available"
			F_DEFINE_BOOLEAN GPORT_IS_BSD_44 1
		fi
	else
		F_DEFINE_BOOLEAN GPORT_IS_BSD_44 0
	fi
else
	F_DEFINE_BOOLEAN GPORT_IS_BSD_44 0
fi
