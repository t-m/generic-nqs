#!/bin/sh
#
# HAS_SIGALRM	Test for the SIGALRM signal ...
#
# Version	4.0
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_SIGNAL SIGALRM
exit 0
