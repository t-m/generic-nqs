#!/bin/sh
#
# Need_uname
#		Script to check for the 'uname' utility
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_FIND_PATH uname
case $? in
	0)	F_DEFINE_STRING "GPORT_EXE_UNAME" "$GSLIB_V_ANSWER"
		if [ "$GPORT_EXE_HOSTNAME+" = "+" ]; then
			F_ECHO_LN "  Using '$GSLIB_V_ANSWER -n' for hostname"
			F_DEFINE_STRING "GPORT_EXE_HOSTNAME" "$GSLIB_V_ANSWER -n"
		fi
		exit 0 ;;
	*)	exit 1 ;;
esac
