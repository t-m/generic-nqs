#!/bin/sh
#
# Has-Utime.sh
#		Check for POSIX.1 utime() function
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_ECHO_RESULT "Support for utime() ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_UTIME 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_UTIME 0
		F_TEST_FAILED
		;;
esac
