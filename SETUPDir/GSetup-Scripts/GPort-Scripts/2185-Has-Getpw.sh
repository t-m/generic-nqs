#!/bin/sh
#
# Has-Getpw.sh
#		Test for getpw*(3) functions
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_ECHO_RESULT "Support for getpw*(3) ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_GETPW 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_GETPW 0
		F_TEST_FAILED
		;;
esac
