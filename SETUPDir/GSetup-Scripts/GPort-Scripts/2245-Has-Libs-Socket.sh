#
# Has-Libs-Socket
#		Does the platform need the SVr4 socket and nsl libraries?
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Do we need socket libraries ... "

F_COMPILE_SCRIPT link 01.c
case $? in
	0)	F_ECHO_LN "no"
		F_TEST_PASSED
		exit 0
		;;
esac

# yes - but which way round?
	
F_ECHO_LN "yes ... "
F_TEST_TYPE BLANK
F_ECHO_RESULT

F_COMPILE_SCRIPT link 01.c "" "-lnsl -lsocket -lelf"

case $? in
	0)	F_ECHO_LN "-lnsl -lsocket -lelf"
		F_DEFINE_LITERAL GPORT_LD_LIBRARIES_LAST "-lnsl -lsocket -lelf $GPORT_LD_LIBRARIES_LAST"
		F_TEST_PASSED
		have_result=1
		exit 0
		;;
	*)	F_ECHO_LN "not -lnsl -lsocket -lelf"
		;;
esac

F_TEST_TYPE BLANK
F_ECHO_RESULT

F_COMPILE_SCRIPT link 01.c "" "-lsocket -lnsl -lelf"
case $? in
	0) 	F_ECHO_LN "-lsocket -lnsl -lelf"
		F_DEFINE_LITERAL GPORT_LD_LIBRARIES_LAST "-lsocket -lnsl -lelf $GPORT_LD_LIBRARIES_LAST"
		F_TEST_PASSED
		exit 0
		;;
	*)	F_ECHO_LN "not -lsocket -lnsl -lelf"
		;;
esac

F_ECHO_LN
F_ERROR "don't know how to link BSD socket code on this platform!"
