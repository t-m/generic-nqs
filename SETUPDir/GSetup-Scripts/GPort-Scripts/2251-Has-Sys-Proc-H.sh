#!/bin/sh
#
# Has-Sys-Proc-H.sh
#		Check for IBM <sys/proc.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/proc.h

exit 0
