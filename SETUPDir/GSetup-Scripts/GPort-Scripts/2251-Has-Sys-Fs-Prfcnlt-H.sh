#!/bin/sh
#
# Has-Sys-Fs-Prfcntl-H.sh
#		Check for UNICOS <sys/fs/prfcntl.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/fs/prfcntl.h

exit 0
