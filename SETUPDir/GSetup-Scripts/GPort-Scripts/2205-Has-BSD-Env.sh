#! /bin/sh
#
# HAS_BSD_ENV	Test to see whether we have a SVr4-style environment, or
#		whether we really have a BSD environment.
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_ECHO_RESULT "Checking out the environment ... "

has_sysv_env=y
has_bsd_env=y

if [ "$LOGNAME+" != "+" ]; then
	F_ECHO_LN "SYSV style ... fine"
	F_DEFINE_BOOLEAN GPORT_HAS_SYSV_ENV 1
else
	F_ECHO_LN "not SYSV style"
	F_DEFINE_BOOLEAN GPORT_HAS_SYSV_END 0
fi

F_TEST_TYPE BSD43
F_ECHO_RESULT
if [ "$USER+" != "+" ]; then
	F_ECHO_LN "BSD style ... fine"
	F_DEFINE_BOOLEAN GPORT_HAS_BSD_ENV 1
else
	F_ECHO_LN "not BSD style"
	F_DEFINE_BOOLEAN GPORT_HAS_BSD_ENV 0
fi

if [ "$GPORT_HAS_BSD_ENV" = 0 ]; then
	if [ "$GPORT_HAS_SYSV_ENV" = 0 ]; then
		F_ECHO_LN "I don't like it"
		F_ECHO_LN
		F_ERROR "  Neither LOGNAME nor USER is defined ..."
		exit 1
	fi
fi
