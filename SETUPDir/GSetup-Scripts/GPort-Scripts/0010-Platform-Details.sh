#!/bin/sh
#
# Platform-Details.sh
#		Wrapper around the standard GNU config.guess.
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

# okay, here's the gig :-
#
# GNU's script produces output in this format :
#
#	hardware-manufacturer-osVersion
#
# this simply produces too many different answers to be useful as-is for
# portability (*you* try maintaining code based on this - been there,
# done that, never again ;-)
#
# from what I've seen to date, the most useful things for applications
# would be
#
#	os-version
#	os-majorversion
#	hardware-os
#	os
#
# in that order. 
#
# I've asked GNU to provide additional output from their script, but they're
# happy with things as they stand - they obviously don't use this script much!
# Anyway, I don't want to modify the actual script itself - that way, I can
# take future versions of the script and just drop them in.  So we're left with
# my script, which runs the GNU script, and adapts the output accordingly.
#
# The only interesting bit is splitting up the 'os' and 'version' parts.
# Some operating systems have a digit or two in their name (e.g. svr4)
#
# For the moment, we try the following ...
#
# (I wouldn't be surprised if there are one or two problems with the main
#  sed script - I don't claim to be an expert of any description!)

F_TEST_TYPE BLANK
F_ECHO_RESULT "What type of computer is this ..."
	
V_GNU_OUTPUT="`$GPORT_V_DIR/config.guess`"
	
V_HARDWARE="`echo $V_GNU_OUTPUT | cut -d- -f1`"
V_VENDOR="`echo $V_GNU_OUTPUT | cut -d- -f2`"
V_OS="`echo $V_GNU_OUTPUT | cut -d- -f3- | tr 'abcdefghijklmnopqrstuvwxyz' 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'`"

V_VERSNO="`echo $V_OS | sed -n -e 's/[^0-9]*\([0-9]*\..*\)/\1/p;'`"
if [ "$V_VERSNO+" != "+" ]; then
	V_OS="`echo $V_OS | sed -e \"s/$V_VERSNO//g;\"`"
	V_OS_VERSION="${V_VERSNO}"
	V_OS_MVERS="`echo $V_VERSNO | cut -d. -f1`"
	V_OS_RVERS="`echo $V_VERSNO | cut -d. -f1-2 | tr '.' '_'`"
else
	V_OS_VERSION="$V_OS"
	V_OS_MVERS="$V_OS"
	V_OS_RVERS="$V_OS"
fi

# this leaves us with the following :
#
#	V_GNU_OUTPUT	- hardware-vendor-os[Version]	eg i386-unknown-linux2.1.76
#	V_HARDWARE	- computer hardware description	eg i386
#	V_VENDOR	- computer supplier		eg unknown
#	V_OS		- operating system type		eg LINUX
#	V_OS_VERSION	- version number for the os	eg 2.1.76
#	V_OS_MVERS	- major version number		eg 2
#	V_OS_RVERS	- version number we'll assign	eg 2_1

F_DEFINE_STRING "GPORT_PLATFORM_ID" 		"$V_GNU_OUTPUT"
F_DEFINE_STRING "GPORT_PLATFORM_HARDWARE"	"$V_HARDWARE"
F_DEFINE_STRING "GPORT_PLATFORM_VENDOR"		"$V_VENDOR"
F_DEFINE_STRING "GPORT_PLATFORM_OS"		"$V_OS"
F_DEFINE_STRING "GPORT_PLATFORM_OS_VERSION"	"$V_OS_VERSION"
F_DEFINE_STRING "GPORT_PLATFORM_OS_MVERS"	"$V_OS_MVERS"
F_DEFINE_STRING "GPORT_PLATFORM_OS_RVERS"	"$V_OS_RVERS"

F_DEFINE_BOOLEAN "GPORT_IS_${GPORT_PLATFORM_OS}" 1
F_DEFINE_BOOLEAN "GPORT_IS_${GPORT_PLATFORM_OS}_${GPORT_PLATFORM_OS_MVERS}" 1
F_DEFINE_BOOLEAN "GPORT_IS_${GPORT_PLATFORM_OS}_${GPORT_PLATFORM_OS_RVERS}" 1

F_ECHO_LN "$V_GNU_OUTPUT"

F_TEST_TYPE BLANK
F_ECHO_RESULT "  Hardware is ..."
F_ECHO_LN "$GPORT_PLATFORM_HARDWARE"

F_TEST_TYPE BLANK
F_ECHO_RESULT "  Operating system vendor is ..."
F_ECHO_LN "$GPORT_PLATFORM_VENDOR"

F_TEST_TYPE BLANK
F_ECHO_RESULT "  Operating system is ..."
F_ECHO_LN "$GPORT_PLATFORM_OS"

F_TEST_TYPE BLANK
F_ECHO_RESULT "  Operating system version is ..."
F_ECHO_LN "$GPORT_PLATFORM_OS_VERSION"

F_TEST_TYPE BLANK
F_ECHO_RESULT "  Operating system major version is ..."
F_ECHO_LN "$GPORT_PLATFORM_OS_RVERS"

