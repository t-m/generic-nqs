#!/bin/sh
#
# Has-Time-H.sh
#		Check for POSIX.1 <time.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE POSIX1
F_CHECK_HEADER time.h

exit 0
