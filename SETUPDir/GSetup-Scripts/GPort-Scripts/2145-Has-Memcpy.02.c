/*
 * Has-Memcpy.02.c
 *		Test for BSD bcopy() et al
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <string.h>

int main(void)
{
	void   * pvoVoid1;
	void   * pvoVoid2;
	int      iSize;

	bcopy(pvoVoid1, pvoVoid2, iSize);
	bzero(pvoVoid1, iSize);

	return 0;
}
