#!/bin/sh
#
# Need_ar
#		Script to check for the 'ar' utility
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

for x in ar bld ; do
	F_TEST_TYPE OTHER
	F_FIND_PATH $x

	case $? in
		0)	F_TEST_PASSED
			F_DEFINE_STRING "GPORT_EXE_AR" "$GSLIB_V_ANSWER cr" 
			exit 0
			;;
		*)	F_TEST_FAILED
			F_DEFINE_STRING GPORT_EXE_AR 
			F_ERROR "'ar' is not in your PATH.  Please fix your PATH and try again"
			exit 1
			;;
	esac
done
