#!/bin/sh
#
# Has-Float-H.sh
#		Check for ANSI C <float.h> header file
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE ANSIC
F_CHECK_HEADER float.h

exit 0
