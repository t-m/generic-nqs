#!/bin/sh
#
# Has-Timezone
#		Work out whether the external variables 'daylight',
#		'timezone', and 'tzname[]' are supported, and what
#		types they are.
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

# =-=-=-=-=
#	F_TestDaylight ()
#
#	Is the daylight variable supported?

F_TestDaylight ()
{
	F_TEST_TYPE OTHER
	F_ECHO_RESULT "Is the 'daylight' variable supported ..."
	F_COMPILE_SCRIPT link 01.c

	case $? in
	0)	F_ECHO_LN "yes"
		F_DEFINE_BOOLEAN GPORT_HAS_TZ_DAYLIGHT 1
		F_TEST_PASSED
		return 0
		;;
	*)	F_ECHO_LN "no - not a problem"
		F_DEFINE_BOOLEAN GPORT_HAS_TZ_DAYLIGHT 0
		F_TEST_FAILED
		return 0
		;;
	esac
}

# =-=-=-=-=
#	F_TestTimezone ()
#
#	Is the timezone variable supported?

F_TestTimezone ()
{
	F_TEST_TYPE OTHER
	F_ECHO_RESULT "Is the 'timezone' variable supported ..."
	F_COMPILE_SCRIPT link 02.c
	case $? in
	0)	F_ECHO_LN "yes, type is time_t"
		F_DEFINE_BOOLEAN GPORT_HAS_TZ_TIMEZONE 1
		F_DEFINE_LITERAL GPORT_TZ_TYPE time_t
		F_TEST_PASSED
		return 0
		;;
	esac

	F_COMPILE_SCRIPT link 03.c
	case $? in
	0)	F_ECHO_LN "yes ... type is long"
		F_DEFINE_BOOLEAN GPORT_HAS_TZ_TIMEZONE 1
		F_DEFINE_LITERAL GPORT_TZ_TYPE long
		F_TEST_PASSED
		return 0
		;;
	esac

	F_ECHO_LN "no"
	F_DEFINE_BOOLEAN GPORT_HAS_TZ_TIMEZONE 0
	F_TEST_FAILED
	return 0
}

# =-=-=-=-=
#	main

main ()
{
	F_TestDaylight
	F_TestTimezone
}

main "$@"
