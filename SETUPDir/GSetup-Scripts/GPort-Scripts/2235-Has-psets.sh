#!/bin/sh
#
# Has-psets
#		Does this o/s support processor sets?
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL
#

. $GSLIB

F_TEST_TYPE BLANK
F_ECHO_RESULT_LN "Do you have processor sets ..."

F_TEST_TYPE OTHER
F_ECHO_RESULT "  IRIX-like /sbin/pset ..."
if [ -f /sbin/pset ]; then
	F_ECHO_LN "found it"
	F_DEFINE_BOOLEAN GPORT_HAS_PSET_IRIX_PSET 1
	F_TEST_PASSED
else
	F_ECHO_LN "not found"
	F_DEFINE_BOOLEAN GPORT_HAS_PSET_IRIX_PSET 0
	F_TEST_FAILED
fi

F_TEST_TYPE OTHER
F_ECHO_RESULT "  Digital UNIX-like pset_assign_pid ..."

if [ -f /usr/sbin/pset_assign_pid ]; then
	F_ECHO_LN "found it"
	F_DEFINE_BOOLEAN GPORT_HAS_PSET_DECOSF 1
	F_TEST_PASSED
else
	F_ECHO_LN "not found"
	F_DEFINE_BOOLEAN GPORT_HAS_PSET_DECOST 0
	F_TEST_FAILED
fi

F_TEST_TYPE OTHER
F_ECHO_RESULT "  IRIX 6.4-like dplace ..."

if [ -f /sbin/dplace ]; then
	F_ECHO_LN "found it"
	F_DEFINE_BOOLEAN GPORT_HAS_PSET_IRIX_DPLACE 1
	F_TEST_PASSED
else
	F_ECHO_LN "not found"
	F_DEFINE_BOOLEAN GPORT_HAS_PST_IRIX_DPLACE 0
	F_TEST_FAILED
fi

F_TEST_TYPE OTHER
F_ECHO_RESULT "  Solaris-like psrset ..."

if [ -f /usr/sbin/psrset ]; then
	F_ECHO_LN "found it"
	F_DEFINE_BOOLEAN GPORT_HAS_PSET_SOLARIS 1
	F_TEST_PASSED
else
	F_ECHO_LN "not found"
	F_DEFINE_BOOLEAN GPORT_HAS_PSET_SOLARIS 0
	F_TEST_FAILED
fi
