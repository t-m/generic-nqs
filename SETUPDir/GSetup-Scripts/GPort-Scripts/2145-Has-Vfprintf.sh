#!/bin/sh
#
# Has-Vfprintf.sh
#		Check on support for vfprintf(3)
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE ANSIC
F_ECHO_RESULT "Support for vfprintf() ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_BOOLEAN GPORT_HAS_VFPRINTF 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "not found"
		F_DEFINE_BOOLEAN GPORT_HAS_VFPRINTF 0
		F_TEST_FAILED
		;;
esac
