#!/bin/sh
#
# HAS_SIGURG	Test for the SYSV SIGURG signal ...
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_SIGNAL SIGURG
exit 0
