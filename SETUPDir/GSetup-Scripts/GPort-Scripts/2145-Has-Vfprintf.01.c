/*
 * Has-Vfprintf.01.c
 *		Check for vfprintf(3) support
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

int main(void)
{
	FILE    * fp;
	va_list   vTmp;

	vfprintf(fp, "%s", vTmp);

	return 0;
}
