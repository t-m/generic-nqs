#!/bin/sh
#
# Need_rm
#		Script to check for the 'rm' utility
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_FIND_PATH rm
case $? in
	0)	F_DEFINE_STRING "GPORT_EXE_RM" "$GSLIB_V_ANSWER"
		exit 0 ;;
	*)	exit 1 ;;
esac
