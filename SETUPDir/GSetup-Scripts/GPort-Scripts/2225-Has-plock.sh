#! /bin/sh
#
# HAS_PLOCK	Does this operating system have <sys/lock.h> (used to
#		lock a process in memory)?
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Checking for process locking support ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "found it"
		F_DEFINE_LITERAL "GPORT_HAS_PLOCK" 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "can't find it"
		F_DEFINE_LITERAL "GPORT_HAS_PLOCK" 0
		F_TEST_FAILED
		;;
esac
