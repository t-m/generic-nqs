/*
 * Has-Utime.01.c
 *		Check on the utime() function
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <sys/types.h>
#include <utime.h>

int main(void)
{
	struct utimbuf stBuf;

	utime("/dev/null", &stBuf);

	return 0;
}
