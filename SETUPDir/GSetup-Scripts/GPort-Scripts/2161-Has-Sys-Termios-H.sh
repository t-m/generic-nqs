#!/bin/sh
#
# Has-Sys-Termios-Header-H.sh
#		Check for <sys/termios.h> header file
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/termios.h

exit 0
