#!/bin/sh
#
# Has-Syslog.sh
#		Check on support for the syslog() stuff
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_ECHO_RESULT "Support for syslog() ..."
F_COMPILE_SCRIPT link 01.c

case $? in
	0)	F_ECHO_LN "full support found"
		F_DEFINE_BOOLEAN "GPORT_HAS_SYSLOG_FULL" 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "full support not found"
		F_DEFINE_BOOLEAN GPORT_HAS_SYSLOG_FULL 0
		F_TEST_FAILED
		;;
esac

F_TEST_TYPE OTHER
F_ECHO_RESULT
F_COMPILE_SCRIPT link 02.c

case $? in
	0)	F_ECHO_LN "simplified support found"
		F_DEFINE_BOOLEAN "GPORT_HAS_SYSLOG_SIMPLE" 1
		F_TEST_PASSED
		;;
	*)	F_ECHO_LN "simplified support not found"
		F_DEFINE_BOOLEAN GPORT_HAS_SYSLOG_SIMPLE 0
		F_TEST_FAILED
		;;
esac
