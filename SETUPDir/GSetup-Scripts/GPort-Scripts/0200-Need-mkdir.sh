#!/bin/sh
#
# Need_mkdir
#		Script to check for the 'hostname' utility
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

check_mkdir ()
{
	F_TEST_TYPE BLANK
	F_ECHO_RESULT "  Does '$1' support -p ... "
	mkdir -p ./test1/test2 > /dev/null 2> /dev/null
	y=$?
	rm -rf ./test1 > /dev/null 2> /dev/null

	if [ "$y" = "0" ]; then
		F_ECHO_LN "apparently so"
		F_TEST_TYPE BLANK
		F_ECHO_RESULT "  Decided to use ..."
		F_ECHO_LN "'$1 -p'"
		return 0
	fi
	F_ECHO_LN "apparently not"
	return 1
}

F_TEST_TYPE OTHER

F_FIND_PATH mkdir check_mkdir
case $? in
	0)	F_TEST_PASSED
		F_DEFINE_STRING "GPORT_EXE_MKDIR" "$GSLIB_V_ANSWER -p"
		;;
	*)	F_TEST_FAILED
		;;
esac
