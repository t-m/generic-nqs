/*
 * Is-gcc.01.c
 *		Test for the GNU C compiler
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

int main(void)
{
#if __GNUC__
	return 0;
}
#endif
