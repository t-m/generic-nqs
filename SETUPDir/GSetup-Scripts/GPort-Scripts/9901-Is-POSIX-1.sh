#! /bin/sh	# -*- sh -*-
#
# IS_POSIX	Test for POSIX.1-1990 compatibility or not
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

#	ISO/IEC 9945-1 : 1990 (E) (IEEE Std 1003.1)
#	
#	This is the POSIX.1 standard which Generic NQS supports.
#
#	This is also the POSIX.1 standard which fails to define any way
#	to test whether an operating system supports POSIX.1 or not.
#
#	I can't even lift any tests from GNU autoconf, as that doesn't test
#	for POSIX.1 at all ;-(
#
#	And, to cap it all, I can't find anything clever in Perl 5's
#	Configure script either.
#
#	So, just how *do* we test for a POSIX.1 system?  Try this for size ...
#
#		We'll include <unistd.h>, and see if the macro
#		_POSIX_VERSION is defined.
#
#	Our biggest danger is trying to use POSIX.1 on a BSD system, such
#	as SunOS v4.x.

F_TEST_TYPE POSIX1
F_ECHO_RESULT "Do we have POSIX.1 support ..."

POSIX_VERSION="`F_COMPILE_SCRIPT run 01.c`"

if [ "$POSIX_VERSION+" != "+" ]; then
	F_ECHO_LN "POSIX.1-$POSIX_VERSION"
	F_TEST_PASSED
else
	F_ECHO_LN "apparently not"
	F_TEST_FAILED
fi
