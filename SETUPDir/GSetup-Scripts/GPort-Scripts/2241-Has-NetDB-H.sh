#!/bin/sh
#
# Has-NetDB-H.sh
#		Check for BSD4.3 <netdb.h> sockets header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE BSD43
F_CHECK_HEADER netdb.h

exit 0
