#!/bin/sh
#
# HAS_SIGUSR1	Test for the SIGUSR1 signal ...
#
# Author	Stuart Herbert
#		(S.Herbert@sheffield.ac.uk)
#
# Copyright	(c) 1997 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_SIGNAL SIGUSR1
exit 0
