/*
 * Has-Memcpy.01.c
 *		Test for support of the POSIX.1 memcpy-style functions
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <stdlib.h>
#include <string.h>

int main (void)
{
	void * pvoVoid1;
	void * pvoVoid2;
	int    iInt;
	size_t gSize;

	memchr  (pvoVoid1, iInt, gSize);
	memcmp  (pvoVoid1, pvoVoid2, gSize);
	memcpy  (pvoVoid1, pvoVoid2, gSize);
	memmove (pvoVoid1, pvoVoid2, gSize);
	memset  (pvoVoid1, iInt, gSize);

	return 0;
}
