/*
 * Need-cc.01.c
 *		Test for an ANSI C compiler
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

int fred(int x);

#ifdef __STDC__
int fred(int x)
#endif
{
	return x;
}
