/*
 * Has-Syslog.02.c
 *		Test for syslog(3) support
 *		ULTRIX for one has a cut-down version
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

#include <syslog.h>

int main(void)
{
	openlog ("gSETUP", LOG_PID);
	syslog (LOG_INFO, "%s", "gSETUP");
	closelog();

	return 0;
}
