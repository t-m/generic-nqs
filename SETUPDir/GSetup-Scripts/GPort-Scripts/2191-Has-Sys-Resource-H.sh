#!/bin/sh
#
# Has-Sys-Resource-H.sh
#		Check for <sys/resource.h> header
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_HEADER sys/resource.h

exit 0
