#!/bin/sh
#
# Has-SIGCLD.sh
#		Do we have the SIGCLD signal, and is it different to
#		SIGCHLD
#
# Author	Stuart Herbert
#		(stuart@gnqs.org)
#
# Copyright	(c) 1998 Stuart Herbert
#		Released under v2 of the GNU GPL

. $GSLIB

F_TEST_TYPE OTHER
F_CHECK_SIGNAL SIGCLD

case $? in
	0) ;;
	*) F_DEFINE_LITERAL SIGCLD SIGCHLD ;;
esac 

exit 0
