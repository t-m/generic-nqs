/*
 * Has-Uptime-Code.01.c
 *		Check how to read uptime info
 *
 * Author	Stuart Herbert
 *		(stuart@gnqs.org)
 *
 * Copyright	(c) 1998 Stuart Herbert
 *		Released under v2 of the GNU GPL
 */

/*
 * This method is supposed to work on IRIX ...
 *
 * I bet I've not got all the headers right ...
 */

#include <sys/sysinfo.h>
#include <sys/syssgi.h>
#include <sys/sysmp.h>
#include <fcntl.h>

int main (void)
{
	unsigned long	kernaddr;
	int		kmem;

	kernaddr = sysmp(MP_KERNADDR, MPKA_AVENRUN) & 0x7fffffff;
	kmem     = open("/dev/kmem", O_RDONLY);

	return 0;
}
