$!
$! Command procedure to make msgd.  Assumes you have Wollongong
$! TCP/IP.
$!
$!	if P1 == D, then it will build a debug version. (Probably
$!	should undefine INETD if debugging at the terminal.
$!
$ set verify
$ write sys$output p1
$ cc_debug = ""
$ link_debug = ""
$ if ("''P1'" .eqs. "D")
$    then 
$       cc_debug = "/debug/noopt"
$	link_debug = "/debug"
$ endif
$ cc'cc_debug' /define="INETD"/include=(twg$tcp:[netdist.include],[-.include])  msgd
$ cc'cc_debug' /include=(twg$tcp:[netdist.include],[-.include]) server
$ cc'cc_debug' /include=(twg$tcp:[netdist.include],[-.include]) [-.common]answerback
$ cc'cc_debug' /include=(twg$tcp:[netdist.include],[-.include]) [-.common]deliver
$ cc'cc_debug' /include=(twg$tcp:[netdist.include],[-.include]) [-.common]errtext
$ cc'cc_debug' /include=(twg$tcp:[netdist.include],[-.include]) [-.common]misc
$ cc'cc_debug' /include=(twg$tcp:[netdist.include],[-.include]) [-.common]savemsg
$link'link_debug' msgd,server,answerback,deliver,errtext,misc,savemsg,-
	twg$tcp:[netdist.lib]twglib.olb/lib,-
	sys$login:c/opt
$set noverify

