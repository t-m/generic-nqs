/* msg.h
   header file for msg and msgd
*/

/*
 * $Header: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/contrib/msgd/Include/msg.h,v 1.1.1.1 1999/01/16 12:30:17 nqs Exp $
 * 
 */

/* Options */
#include "msgoptions.h"

#include <stdio.h>
#include <sys/types.h>
#if	vms
#include stat
#else
#include <utmp.h>
#include <sys/stat.h>
#define UTMP	"/etc/utmp"	/* why isn't this in utmp.h?? */
#ifdef USER_PROCESS
#define SYSV_UTMP
#endif
#endif
#define ALLOW	((S_IWRITE>>3)|(S_IWRITE>>6))
#ifdef VARARGS
#include <varargs.h>
#endif

#define DEV	"/dev/"
#if vms
#define MSGFILE	"sys$login:$msg."	/* where to save messages, append userid */
#define PMSGFILE "sys$login:$pmsg." /* where to save outgoing messages */
#define ANSBACK	"sys$login:$mesg."	/* where asnwerback message might be */
#else
#define MSGFILE	"/usr/tmp/msg."	/* where to save messages, append userid */
#define PMSGFILE "/usr/tmp/pmsg." /* where to save outgoing messages */
#define ANSBACK	"/usr/tmp/mesg."	/* where asnwerback message might be */
#endif

#define private		static

extern char hostname[];
extern char *syserr();

/*
 * Following are in network byte order.
 */
typedef struct header {
    int	slen;	/* sender length */
    int	ulen;	/* userid length */
    int	mlen;	/* message length */
} header;

/* This version header was added after the original version of msg had
   been installed and was running.  To provide compatibility with versions
   of msg that do not send a version as the first information, this
   version struct must be twelve bytes long, and the first int is always
   a 0, because if it is non-zero you can assume that it is a sender
   length in a header, and not a version struct.
*/
typedef union version {
    header h;
    struct v {
	int zero;
	int vnum;	/* version number */
	int filler;
    } v;
} version;
#define VERSION		(1)	/* current version */


extern char	*errmessages[];
/* error codes for message - msgtext.c relies on this ordering */
#define OK		(0)
#define VERSION_OK	OK	/* got a version, and we accept it */
#define NOT_ON		(1)	/* user not signed on */
#define NO_WRITE	(2)	/* couldn't write to terminal */
#define BAD_HOST	(3)	/* host doesn't exist */
#define NO_UTMP		(4)	/* couldn't open /etc/utmp */
#define BAD_ARGS	(5)	/* bad arguments */
#define NO_CONNECT	(6)	/* couldn't connect to remote host */
#define FATAL		(7)	/* fatal error */
#define NO_ACK		(8)	/* no result from remote host */
#define REMOTE_WRITE	(9)	/* write error to remote host */
#define REMOTE_READ	(10)	/* read error from remote host */
#define MESG_OFF	(11)	/* dest user has mesg n set */
#define NO_REPLY	(12)	/* can't get replies */
#define TIMEOUT		(13)	/* timeout while writing to user's terminal */
#define ANSWER		(14)	/* asnwerback message */
#define ANS_WARN	(15)	/* user has answerback msg set */
#define NO_TTY		(16)	/* user is not on a tty */
#define	HOWEVER		(17)	/* However, another tty was writable. */
#define NO_STAT		(18)	/* couldn't stat tty */
#define BAD_VERSION	(19)	/* client newer than server */

#define ERROR_MESSAGE	(99)	/* error message being returned from msgd */

#define FPNULL		((FILE *)NULL)
#define CPNULL		((char *)NULL)
#define TRUE		(1)
#define FALSE		(0)
