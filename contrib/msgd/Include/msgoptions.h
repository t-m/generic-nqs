/* msgoptions.h
   separate options file, so I can #inlcude it in an Imakefile
*/

/*
 * $Header: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/contrib/msgd/Include/msgoptions.h,v 1.1.1.1 1999/01/16 12:30:17 nqs Exp $
 * 
 */

/* Options */
/* define PMBECOME if you want to use the Waterloo pmbecome() routine
   which allows you to seteuid() to a user, and seteuid() back to root
   later.  Only needed if the MSGFILE is on a partition that root can't
   write, such as on pre 4.0 SunOS diskless clients. */
/* #define PMBECOME */

/* define SAVEMSG to save received messages in MSGFILE */
#define SAVEMSG

/* define ANSWERBACK to allow the use of answerback files set up by
   mesg to send back to the sender when a message is received. */
#define ANSWERBACK

/* define REMOTE to always use the daemon to deliver instead of doing
   local delivery when possible.  This means msg doesn't have to be
   setuid/setgid, but it's also slower. */
/* #define REMOTE */

/* define VARARGS if you have the varargs stuff, including vprintf and
   friends. */
#define VARARGS

/* define DUMBSHORT if you're silly enough to have long userids in the
   passwd file, but short userids everywhere else.  We were silly enough
   to be doing this on our MIPS machines ... */
/* #define DUMBSHORT */

/* define REALUSER if you want to use the MFCF realuser() routine to find
   out your "real" campus-wide userid and use that as the return address.
   This won't be useful outside Waterloo, and I hope I remembered to remove
   the "-luw" from the Makefile before sending it into the outside world. */
/* #define REALUSER */
