#ifndef lint
static char *RCSid = "$Header: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/contrib/msgd/Common/misc.c,v 1.1.1.1 1999/01/16 12:30:17 nqs Exp $";
#endif

#if 0
/*
 * $Log: misc.c,v $
 * Revision 1.1.1.1  1999/01/16 12:30:17  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:16  nqs
 * Imported sources
 *
 * Revision 1.1  87/08/06  19:06:34  sahayman
 * Initial revision
 * 
 */
#endif

/* misc.c
   miscellaneous routines
*/


extern int sys_nerr, errno;
extern char *sys_errlist[], *sprintf();

char *
syserr()
{
	static char buf[80];

	return( (errno<sys_nerr) ? sys_errlist[errno] :
		sprintf(buf,"Unknown error %d", errno) );
}
