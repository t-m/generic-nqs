#ifndef lint
static char *RCSid = "$Header: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/contrib/msgd/Common/errtext.c,v 1.1.1.1 1999/01/16 12:30:17 nqs Exp $";
#endif

#if 0
/*
 * $Log: errtext.c,v $
 * Revision 1.1.1.1  1999/01/16 12:30:17  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:16  nqs
 * Imported sources
 *
 * Revision 1.3  89/01/08  03:16:36  jmsellens
 * Changed BADHOST error message to not mention hosts file, since
 * we don't use it any longer.
 * 
 * Revision 1.2  88/09/25  02:40:49  jmsellens
 * added message for BAD_VERSION
 * 
 * Revision 1.1  87/08/06  19:06:32  sahayman
 * Initial revision
 * 
 */
#endif

/* msgtext.c
   Text of error messages produced by the system
*/

#include "msg.h"

char *errmessages[] = {
    /* OK */
    "",
    /* NOT_ON - user, host */
    "%s@%s is not signed on",
    /* NO_WRITE - user, host, ut.u_line, syserr() */
    "couldn't write to %s@%s (%s): %s",
    /* BAD_HOST - host */
    "host '%s' is unknown",
    /* NO_UTMP - host, syserr() */
    "couldn't open utmp on host '%s': %s",
    /* BAD_ARGS - not used I think */
    "bad argument(s)",
    /* NO_CONNECT - host syserr() */
    "couldn't connect to daemon on host '%s': %s (not running?)",
    /* FATAL - not used I think */
    "fatal error",
    /* NO_ACK - host */
    "didn't receive acknowledgement from host '%s'",
    /* REMOTE_WRITE - host, syserr() */
    "error writing to host '%s': %s",
    /* REMOTE_READ - host, syserr() */
    "error writing from host '%s': %s",
    /* MESG_OFF user, host, ut.u_line */
    "%s@%s (%s) is not receiving messages",
    /* NO_REPLY user, ut.u_line */
    "WARNING: Your terminal (%s on %s) cannot be written to",
    /* TIMEOUT user, host, ut.u_line */
    "timed out writing to %s@%s (%s) - terminal paging set?",
    /* ANSWER user, host, line, answerbackmessage */
    "%s@%s (%s): %s",
    /* ANS_WARN user, line, answerbackmessage */
    "%s on %s: Answerback message set to '%s'",
    /* NO_TTY */
    "WARNING: You are not on a terminal",
    /* HOWEVER */
    "However, %s@%s (%s) received the message",
    /* NO_STAT - terminal, user, host, syserr() */
    "Couldn't stat '%s' of %s@%s: %s - message not delivered",
    /* BAD_VERSION - remote version, our version */
    "Client version (%d) newer than server (%d) - can't understand you",
    0
};
