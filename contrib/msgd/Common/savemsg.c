#ifndef lint
static char *RCSid = "$Header: /home/cvs/Generic/GNQS/Generic-NQS-3.50.5/contrib/msgd/Common/savemsg.c,v 1.1.1.1 1999/01/16 12:30:17 nqs Exp $";
#endif

#if 0
/*
 * $Log: savemsg.c,v $
 * Revision 1.1.1.1  1999/01/16 12:30:17  nqs
 * Imported Source
 *
 * Revision 1.1.1.1  1998/10/28 11:07:16  nqs
 * Imported sources
 *
 * Revision 1.8  91/08/13  23:57:35  jmsellens
 * s/eprintf/errprintf/ to avoid libuw clash
 * 
 * Revision 1.7  91/08/13  23:57:05  jmsellens
 * realuser stuff
 * 
 * Revision 1.6  90/12/29  22:01:03  jmsellen
 * Avoid PMBECOME, and handle DUMBSHORT machines
 * 
 * Revision 1.5  88/09/25  03:03:29  jmsellens
 * call syserr() is pmbecome() fails
 * 
 * Revision 1.4  87/10/19  18:29:11  sahayman
 * Use pmbecome() to become different users, since the simple
 * seteuid( old_euid ) after you seteuid (someone) won't work.
 * 
 * Revision 1.2  87/08/06  20:17:31  sahayman
 * Write /usr/tmp/msg.user file by doing a seteuid(user) first, so that
 * it has a chance of working when /usr/tmp is an NFS partition.
 * No longer writes ~/.msg.
 * 
 * Revision 1.1  87/08/06  19:06:40  sahayman
 * Initial revision
 * 
 */
#endif

#include "msg.h"
#include <pwd.h>
#ifdef REALUSER
#include <mfcf/libuw/standard.h>
#endif


savemsg( uid, recipient, sender, msg )
int uid;
char *recipient, *sender, *msg;
{
    char fname[100];	/* lots o' room */
    FILE *fp;
    struct passwd *pwd;
#ifdef PMBECOME
    int old_euid = geteuid();
#endif /* PMBECOME */

    /*
     * Original code used to write into ~/.msg.  No more.
     */

#if vms

#else

    /* truncate the recipient for lookup */
    if ( strlen(recipient) > sizeof(((struct utmp *)NULL)->ut_name) )
	recipient[sizeof(((struct utmp *)NULL)->ut_name)] = '\0';
    if ((pwd = getpwnam(recipient)) == (struct passwd *) NULL) {
#ifdef DUMBSHORT
	/* loop through the whole passwd file looking for a
	   truncated match */
	struct utmp *ut;	/* just for sizeof() */
	setpwent();
	while ( (pwd = getpwent()) != (struct passwd *) NULL ) {
	    if ( strncmp(recipient,pwd->pw_name,sizeof(ut->ut_name)) == 0 )
		break;
	}
	if ( pwd == (struct passwd *) NULL )
	    return;
	recipient = pwd->pw_name;
#else
	return;
#endif
    }
#ifdef REALUSER
    recipient = realuser( pwd );
#endif
    

    /* now, put it where it really belongs, in MSGFILE */

#ifdef PMBECOME
    /*
     * This seteuid() business is so that this can work
     * when /usr/tmp is an NFS thingy, and root on a client
     * can only creat things there owned by -2, so the old
     * scheme of opening the file and then chowning it wouldn't work.
     */

#ifdef old
    if ( seteuid( pwd->pw_uid ) == -1 ) {
	errprintf("savemsg: Cannot seteuid(%d)", pwd->pw_uid);
#else
    if ( pmbecome( pwd->pw_uid ) == -1 ) {
	errprintf("savemsg: Cannot pmbecome(%d): %s", pwd->pw_uid, syserr());
#endif

	/*
	 * Keep going, it might work...
	 */
    }
#endif /* PMBECOME */

#endif	/* vms */
    strcpy( fname, MSGFILE );
    strcat( fname, recipient );
#if vms
    strcat( fname, ";1" );
    (void) delete( fname );	/* in case it already exists */
#else
    (void) unlink( fname );	/* in case it already exists */
#endif

    if ( (fp = fopen( fname, "w" ) ) != NULL ) {
#if vms
	(void) chmod( fname, 0600 );
#else
	(void) fchmod( fileno(fp), 0600 );
	(void) fchown( fileno(fp), pwd->pw_uid, -1 );
#endif
	fprintf( fp, "%s: %s\n", sender, msg );
	(void) fclose( fp );
    }

#ifdef PMBECOME
    if ( pmbecome(0) == -1 )
	errprintf("savemsg: Cannot pmbecome(0): %s", syserr());
#endif /* PMBECOME */
}
