
/*  check for root permissions (euid = 0)				*/

/*  02-02-95	Ingo Schneider, ReZe, RWTH Aachen, Germany		*/
/*		E-mail schneider@rz.RWTH-Aachen.DE			*/

#define version "1.00"

#include <stdio.h>

int main (int argc, char **argv)
{

char *pname;
int  silent = 0, verbose = 0;

   pname = *argv;
   while (*(++argv) != NULL) {
      if (strcmp (*argv, "-h") == 0 || strcmp (*argv, "-?") == 0) {
         fprintf (stderr, "\n%s checks for root permissions (euid = 0).\n\nvalid options are:\n      -?, -h   display this help\n      -f       run in full display mode\n               (also display a message when you have root permissions)\n      -s       run in silent mode (just give a return code)\n      -v       display Version Info\n\npossible return codes are:\n      -1       no root permissions\n       0       root permissions\n       1       help output displayed\n\n\nby Ingo Schneider, ReZe, RWTH Aachen, Germany\nE-mail schneider@rz.RWTH-Aachen.DE\n\n", pname);
         return (1);
      }
      if (strcmp (*argv, "-f") == 0)
         verbose = -1;
      if (strcmp (*argv, "-s") == 0)
         silent = -1;
      if (strcmp (*argv, "-v") == 0) {
         fprintf (stderr, "\n%s Version: %s\n\n", pname, version);
         return (1);
      }
   }
   if (geteuid() != 0) {
      if (! silent)
         fprintf (stderr, "You do NOT have root permissions.\n");
      return (-1);
   }
   if (verbose)
      fprintf (stderr, "You DO have root permissions.\n");
   return (0);
}
