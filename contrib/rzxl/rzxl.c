/*
    This C-programm is used to call compilers on RS/6000 under AIX.

    It changes the data segment size limit to "unlimited" before 
    starting the original compiler driver program.

    For reason and history s. README.

    The name of this binary is passed to the compiler as its name
    so you can make hardlinks for this binary under different names
    (e.g.: f77, xlf, ...) and the compiler uses the right library
    definition etc. (see man pages for xlf stanzas).		

    To change the limits effective rights of root are needed.
    Therefor the binary needs s-bit for user and must belong to root.

    For security reasons you should NOT try to let this program decide	
    upon its own name which compiler is to be executed because symbolic
    links could be used to cheat this program. This might result in an
    other program (eg. sh) running with unlimited memory access.
*/

/*  01-02-95	Ingo Schneider, ReZe, RWTH Aachen, Germany		*/
/*		E-mail schneider@rz.RWTH-Aachen.DE			*/

#include <stdio.h>
#include <sys/stat.h>
#include <ulimit.h>

#ifndef execprg
#define execprg "/usr/bin/xlf"
#endif

int main (int argc, char *argv[])
{
   char cname[] = execprg;		/* compiler to be started	*/

   if (geteuid() != 0) {		/* is the effective uid = root ?*/
      fprintf (stderr, "unable to change limits for execution of %s\n", cname);
      return (-1);
   }

   ulimit (SET_DATALIM, -1);		/* data limit in kB (-1 = unlimited) */
   ulimit (SET_STACKLIM, 100000);	/* stack limit in kB		*/

   setuid (getuid(), 0);		/* switch back to users uid	*/

   execv (cname, argv);			/* execute compiler		*/

/*   you'll never come here when no error has occured			*/

   fprintf (stderr, "execution of %s denied!\n", cname);

   return (-1);
}
