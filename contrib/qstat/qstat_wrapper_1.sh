#!/bin/sh
# Wrapper for qstat -d.   Gives better tabular output, with 
# the full job id.
# Michel Proulx, RPSO/NRC, 1995

if test $# -eq 0 
then
 opt=-ald
else
 opt="-al $*"
fi

qstat $opt | nawk '
BEGIN {hprint=0}
/^  Request is/ {next}
/^  Request / {
         junk=sub("(.*Name=)",""); 
         request=$1
         if (hprint == 0) {
                          hprint = 1
                          printf("\n%-8s %-8s %-11s %-8s %2s %8s %-28.28s\n", "Host", "Queue", "Request-id", "Owner", "St", "Proc/prio", "Request-name")}
         if (oqhost == qhost) { 
          if (oqueue == queue) pqueue=""
          else pqueue=queue
          pqhost=""}
         else {pqhost=qhost; pqueue=queue}
         oqueue=queue; oqhost=qhost
         printf("%-8.8s %-8.8s ", pqhost, pqueue)
         next }
         
/^  Id/ {junk=sub("(Priority=)","");
         junk=sub("(Id=)","");
         junk=sub("(Owner=)","");
         junk=sub("(Pgrp=)","");
         n=split($0,qstat)
         if (junk == 0 ) qstat[5] = qstat[3]
         printf("%-11s %-8s %2.1s %8s %-28.28s\n", qstat[1], qstat[2], qstat[4], qstat[5], request )
         next }

/^  /   {next}

/^$/    {next}

/^Destination/ {next}

/Try later/     { print "NQS is down on node", qhost ; print ; next }

        {junk=sub("(; .*type=.*)",":");
         n=split($0,nqueue,"([@,:])")
         queue=nqueue[1]
         qhost=nqueue[2] }

END     { if (hprint == 1) printf("\n")}'
